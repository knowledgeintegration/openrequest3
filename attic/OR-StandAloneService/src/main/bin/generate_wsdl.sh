export CLASSPATH=../etc
for lib in `ls ../lib/*.jar`
do
  export CLASSPATH=$CLASSPATH:$lib
done

export CLASSPATH=.:$CLASSPATH

export OPTS="-Xmx256m -Xms256m"

# echo Starting RMI registry
# $JAVA_HOME/bin/rmiregistry &
# 
# sleep 4
# 
echo Generating wsdl
/usr/local/axis2-1.1/bin/java2wsdl.sh -cn com.k_int.openrequest.integration.provider.openrequest.StatelessRequestManagerImpl -p2n all,"http://k-int.com/openrequest/webservice/1.1/"
