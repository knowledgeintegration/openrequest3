package com.k_int.openrequest.integration.backend.iface;

public interface ILLSymbolDAO {
  
  public static final int SYMBOL_STATUS_INVALID = 1;
  public static final int SYMBOL_STATUS_REQUEST_ONLY = 2;
  public static final int SYMBOL_STATUS_RESPOND_ONLY = 3;
  public static final int SYMBOL_STATUS_ALL_OK = 4;

  /**
   * Attempt to look up the given symbol in whatever directory the host system
   * uses.
   * @param symbol the Symbol to lookup
   * @return a
   */
  public LocalSymbolInfoDTO lookupSymbol(String symbol);

  /**
   * here we assume that the symbol is already known to OpenRequest (Even if only
   * because a previous call to lookupSymbol), and we are asking a host system
   * to validate the symbol. This function can be used when a host system database
   * might dis-allow requesting for a given location, and OpenRequest should not
   * automatically assume that just because a location is known, requests should
   * be acceped from it.
   * @param symbol The location symbol to resolve
   * @return appropriate enum from this class, SYMBOL_STATUS_INVALID,SYMBOL_STATUS_REQUEST_ONLY,SYMBOL_STATUS_RESPOND_ONLY,SYMBOL_STATUS_ALL_OK
   */
  public int validateSymbolStatus(String symbol);
}
