package com.k_int.openrequest.integration.backend.iface;

import java.util.Map;

public class LocalSymbolInfoDTO {

  public String private_symbol_id;
  public String name;
  public Map additional_properties;

  public LocalSymbolInfoDTO() {
  }

  public LocalSymbolInfoDTO(String private_symbol_id, String name, Map additional_properties) {
    this.private_symbol_id = private_symbol_id;
    this.name = name;
    this.additional_properties = additional_properties;
  }

  public String getPrivateSymbolId() {
    return private_symbol_id;
  }

  public void setPrivateSymbolId(String private_symbol_id) {
    this.private_symbol_id = private_symbol_id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Map getAdditionalProperties() {
    return additional_properties;
  }

  public void setAdditionalProperties(Map additional_properties) {
    this.additional_properties = additional_properties;
  }
}
