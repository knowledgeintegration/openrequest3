package com.k_int.openrequest.integration.api.dbc;
import java.lang.reflect.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class Assertions {

  public static void PRE(boolean cond, String label){
    if (!cond)  {throw new  PRECONDITION_EXCEPTION(label);}
  }

  public static void CHECK(boolean cond, String label){
    if (!cond)  {throw new  CHECK_EXCEPTION(label);}
  }

  public static void BE_CHECK(boolean cond, String label){
    if (!cond)  {throw new  BE_EXCEPTION(label);}
  }

/*
  public static void notNull (Object o, String label) {
    if (o == null) {throw new NullPointerException ("NULL Object: " + label);}
  }

  public static void notEmpty (String s, String label) {
    if (s.length() == 0) {throw new PRECONDITION_EXCEPTION("EMPTY String: " + label);}
  }

  public static void notNullOrEmpty (String s, String label) {
    notNull (s, label);
    notEmpty (s, label);
  }

  public static void notZeroLengthArray (Object a, String label){
      if (Array.getLength(a) == 0) {throw new IllegalArgumentException("ZERO LENGTH Array: " + label);}
  }

  public static void check (boolean exp, String label) {
    if (!exp) {throw new NullPointerException ("Assertion Violation: " + label);}
  }
  */
}