package com.k_int.openrequest.integration.api.dbc;

/**
 * Title:
 * Description: Exception raised when illengine returns inconsistent results
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class BE_EXCEPTION extends RuntimeException{

  public BE_EXCEPTION(String label) {
    super ("<" + label + ">");
  }
}

