package com.k_int.openrequest.integration.api.dbc;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class PRECONDITION_EXCEPTION extends RuntimeException{

  public PRECONDITION_EXCEPTION(String label) {
    super ("<" + label + ">");
  }
}