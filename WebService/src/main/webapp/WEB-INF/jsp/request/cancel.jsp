<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/pojoform.tld" prefix="pf" %><%
  String context_id = (String) request.getAttribute("codbif2.context.id");
  String pojo_id = (String) request.getAttribute("codbif2.pojo.id");
  String base_dir = request.getContextPath();
  String username = request.getRemoteUser();
%>
<div id="LeftColumn">
</div>

<div id="RightColumn">
  <tiles:insert definition="location.actions.weblet"/>
  </div>

  <div id="MainContent">
    <div id="ORPanel">
      <div id="Weblet">
        <h1>Action Requests - Cancel</h1>

        <pf:Form action="storeRequestActionForm">
          <input type="hidden" name="codbif2.context.id" value="<%=context_id%>"/>
          <input type="hidden" name="codbif2.pojo.id" value="<%=pojo_id%>"/>
          <table>
            <tr><td align="right">Message :</td><td><pf:TextField property="messageDTO.cancel.note" editable="true"/></td></tr>
            <tr><td colspan="2"><pf:Submit name="codbif2.action:forward:submit" label="Submit Request"/></td></tr>
          </table>
        </pf:Form>
      </div>
    </div>
  </div>
</div>


