<%@ taglib uri="/WEB-INF/pojoform.tld" prefix="pf" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%
  String context_id = (String) request.getAttribute("codbif2.context.id");
  String pojo_id = (String) request.getAttribute("codbif2.pojo.id");
  String base_dir = request.getContextPath();
  // String action=base_dir+"/secure/processNewRequestPage";
%>

<div id="LeftColumn">
</div>

<div id="RightColumn">
  <tiles:insert definition="location.actions.weblet"/>
</div>

<div id="MainContent">
  <div id="ORPanel">
    <div id="Weblet">
      <h1>Create New Request - Request Rota</h1>

<pf:Form action="processNewRequestPage">
  <input type="hidden" name="codbif2.context.id" value="<%=context_id%>"/>
  <input type="hidden" name="codbif2.pojo.id" value="<%=pojo_id%>"/>
  <table>
    <tr>
      <td colspan="2">
        <input type="submit" name="codbif2.action:forward:general" value="General"/>
        <input type="submit" name="codbif2.action:forward:rota" value="Rota"/>
        <input type="submit" name="codbif2.action:forward:service" value="Service"/>
        <input type="submit" name="codbif2.action:forward:requester" value="Requester"/>
      </td>
    </tr>

    <pf:PojoContext property="reqData" autoCreate="com.k_int.openrequest.webservices.generated.RequesterViewDTO">
      <pf:PojoContext property="ILLRequest" autoCreate="com.k_int.openrequest.webservices.generated.ILLRequestMessageDTO">
        <pf:PojoContext property="itemId" autoCreate="com.k_int.openrequest.webservices.generated.ItemIdDTO">
          <tr>
            <td align="Right">Title :</td><td><pf:TextField property="title" editable="true"/></td>
          </tr>
        </pf:PojoContext>
      </pf:PojoContext>
    </pf:PojoContext>

    <tr>
      <td align="right" valign="top">Rota :</td>
      <td><table>
          <tr><th>Symbol</th><th>TQ</th><th>Protocol</th></tr>
        <pf:List property="rota">
          <tr>
            <td><pf:TextField property="symbol" editable="false"/></td>
            <td><pf:TextField property="mandatoryTransactionQualifier" editable="false"/></td>
            <td><pf:TextField property="protocol" editable="false"/></td>
          </tr>
        </pf:List>
        <tr>
          <td colspan="3">
            <input type="text" name="new_rota_symbol"/>
            <input type="submit" name="codbif2.action:forward:addRotaEntry" value="Add To Rota"/>
          </td>
        </tr>
        </table>
      </td>
    </tr>

    <tr>
      <td colspan="2">
        <pf:Submit name="codbif2.action:forward:submit" label="Submit Request"/>
      </td>
    </tr>
  </table>
</pf:Form>

</div>
</div>
</div>
