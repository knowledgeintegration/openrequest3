<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/pojoform.tld" prefix="pf" %><%
  String context_id = (String) request.getAttribute("codbif2.context.id");
  String pojo_id = (String) request.getAttribute("codbif2.pojo.id");
  String base_dir = request.getContextPath();
  String username = request.getRemoteUser();
  // Use helper method to get list of locations this user is allowed to request from.
%>
<div id="LeftColumn">
</div>

<div id="RightColumn">
  <tiles:insert definition="location.actions.weblet"/>
</div>

<div id="MainContent">
  <div id="ORPanel">
    <div id="Weblet">
      <h1>Create New Request - General</h1>

<pf:Form action="processNewRequestPage">
  <input type="hidden" name="codbif2.context.id" value="<%=context_id%>"/>
  <input type="hidden" name="codbif2.pojo.id" value="<%=pojo_id%>"/>
  <table>
    <tr>
      <td colspan="2">
        <input type="submit" name="codbif2.action:forward:general" value="General"/>
        <input type="submit" name="codbif2.action:forward:rota" value="Rota"/>
        <input type="submit" name="codbif2.action:forward:service" value="Service"/>
        <input type="submit" name="codbif2.action:forward:requester" value="Requester"/>
      </td>
    </tr>
    <tr>
      <td align="right">Requesting Location :</td>
      <td><pf:TextField property="requesterSymbol" editable="false"/></td>
    </tr>
    <pf:PojoContext property="reqData" autoCreate="com.k_int.openrequest.webservices.generated.RequesterViewDTO">
      <pf:PojoContext property="ILLRequest" autoCreate="com.k_int.openrequest.webservices.generated.ILLRequestMessageDTO">
        <tr>
          <td align="right">Transaction Type :</td>
          <td>
            <pf:StaticSelectList property="transactionType">
              <pf:StaticOption key="simple" value="Simple"/>
              <pf:StaticOption key="chained" value="Chained"/>
              <pf:StaticOption key="partitioned" value="Partitioned"/>
            </pf:StaticSelectList>
          </td>
        </tr>
        <tr>
          <td align="right">Requested Service Type :</td>
          <td>
            <pf:StaticSelectList property="requestedServiceType">
              <pf:StaticOption key="1" value="Loan"/>
              <pf:StaticOption key="2" value="Copy Non Returnable"/>
              <pf:StaticOption key="3" value="Locations"/>
              <pf:StaticOption key="4" value="Estimate"/>
              <pf:StaticOption key="5" value="Responder-Specific"/>
            </pf:StaticSelectList>
          </td>
        </tr>
        <pf:PojoContext property="itemId" autoCreate="com.k_int.openrequest.webservices.generated.ItemIdDTO">
          <tr>
            <td align="right">Item Type :</td>
            <td>
              <pf:StaticSelectList property="itemType">
                <pf:StaticOption key="1" value="Monograph"/>
                <pf:StaticOption key="2" value="Serial"/>
                <pf:StaticOption key="3" value="Other"/>
              </pf:StaticSelectList>
            </td>
          </tr>
          <tr> <td align="right">Author :</td><td><pf:TextField property="author" editable="true"/></td> </tr>
          <tr> <td align="right">Author Of Article :</td><td><pf:TextField property="authorOfArticle" editable="true"/></td> </tr>
          <tr> <td align="right">Call Number :</td><td><pf:TextField property="callNumber" editable="true"/></td> </tr>
          <tr> <td align="right">Edition :</td><td><pf:TextField property="edition" editable="true"/></td> </tr>
          <tr>
            <td align="right">Held Medium Type :</td>
            <td>
              <pf:StaticSelectList property="heldMediumType">
                <pf:StaticOption key="1" value="Printed"/>
                <pf:StaticOption key="3" value="Microform"/>
                <pf:StaticOption key="4" value="Film Or Video Recording"/>
                <pf:StaticOption key="5" value="Audio Recording"/>
                <pf:StaticOption key="6" value="Machine Readable"/>
                <pf:StaticOption key="7" value="Other"/>
              </pf:StaticSelectList>
            </td>
          </tr>
          <tr> <td align="right">ISBN :</td><td><pf:TextField property="isbn" editable="true"/></td> </tr>
          <tr> <td align="right">ISSN :</td><td><pf:TextField property="issn" editable="true"/></td> </tr>
          <tr> <td align="right">Pagination :</td><td><pf:TextField property="pagination" editable="true"/></td> </tr>
          <tr> <td align="right">Place of Publication :</td><td><pf:TextField property="placeOfPublication" editable="true"/></td> </tr>
          <tr> <td align="right">Publication Date :</td><td><pf:TextField property="publicationDate" editable="true"/></td> </tr>
          <tr> <td align="right">Publication Date Of Component:</td><td><pf:TextField property="publicationDateOfComp" editable="true"/></td> </tr>
          <tr> <td align="right">Publisher:</td><td><pf:TextField property="publisher" editable="true"/></td> </tr>
          <tr> <td align="right">Series Title Number:</td><td><pf:TextField property="seriesTitleNumber" editable="true"/></td> </tr>
          <tr> <td align="right">Sponsoring Body:</td><td><pf:TextField property="sponsoringBody" editable="true"/></td> </tr>
          <tr> <td align="right">Title :</td><td><pf:TextField property="title" editable="true"/></td> </tr>
          <tr> <td align="right">Subtitle:</td><td><pf:TextField property="subTitle" editable="true"/></td> </tr>
          <tr> <td align="right">Title Of Article :</td><td><pf:TextField property="titleOfArticle" editable="true"/></td> </tr>
          <tr> <td align="right">Verification Reference Source :</td><td><pf:TextField property="verificationReferenceSource" editable="true"/></td> </tr>
          <tr> <td align="right">Volume/Issue :</td><td><pf:TextField property="volumeIssue" editable="true"/></td> </tr>
        </pf:PojoContext>
      </pf:PojoContext>
    </pf:PojoContext>

    <tr>
      <td colspan="2">
        <pf:Submit name="codbif2.action:forward:submit" label="Submit Request"/>
      </td>
    </tr>

  </table>
</pf:Form>
    </div>
  </div>
</div>
