<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/pojoform.tld" prefix="pf" %><%
  String context_id = (String) request.getAttribute("codbif2.context.id");
  String pojo_id = (String) request.getAttribute("codbif2.pojo.id");
  String base_dir = request.getContextPath();
  String username = request.getRemoteUser();
%>
<div id="LeftColumn">
</div>

<div id="RightColumn">
  <tiles:insert definition="location.actions.weblet"/>
  </div>

  <div id="MainContent">
    <div id="ORPanel">
      <div id="Weblet">
        <h1>Action Requests - Shipped</h1>

        <pf:Form action="storeRequestActionForm">
          <input type="hidden" name="codbif2.context.id" value="<%=context_id%>"/>
          <input type="hidden" name="codbif2.pojo.id" value="<%=pojo_id%>"/>
          <table>
            <tr><td align="right">Shipped Service Type :</td><td>
              <pf:StaticSelectList property="messageDTO.shipped.shippedServiceType">
                <pf:StaticOption key="1" value="Loan"/>
                <pf:StaticOption key="2" value="Copy"/>
                <pf:StaticOption key="3" value="Locations"/>
                <pf:StaticOption key="4" value="Estimate"/>
                <pf:StaticOption key="5" value="Responder Specific"/>
              </pf:StaticSelectList>
            </td></tr>
            <tr><td align="right">Message :</td><td><pf:TextField property="messageDTO.shipped.note" editable="true"/></td></tr>
            <tr><td colspan="2"><pf:Submit name="codbif2.action:forward:submit" label="Submit Request"/></td></tr>
          </table>
        </pf:Form>
      </div>
    </div>
  </div>
</div>


