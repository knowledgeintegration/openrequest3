<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%
  String base_dir = request.getContextPath();
%>
<!-- thanks http://raibledesigns.com/page/rd/20020828 -->
<!--taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" -->
<!--tiles:insert definition="site.badlogin.page" flush="true" -->
<h2>Login Failed</h2>
<b><font color="red">The username or password was not valid.</font></b>
