<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %><%
  String base_dir = request.getContextPath();
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache"); //HTTP 1.0
  response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
  response.setContentType("text/html;charset=utf-8");
  String username = request.getRemoteUser();

  String location_id = (String) request.getAttribute("com.k_int.openrequest.location_id");
%>
<html:html locale="en" xhtml="true">

  <head>
    <title>OpenRequest 3.0.0 Web Services Module</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="<%=base_dir%>/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="<%=base_dir%>/js/prototype.js" type="text/javascript"></script>
    <script src="<%=base_dir%>/js/scriptaculous.js" type="text/javascript"></script>
  </head>

  <body>
    <div id="header">

      <div id="navlogo">
        OpenRequest 3.0.0
      </div>

      <div id="idbadge">
        <% if ( request.getUserPrincipal() != null ) { %>
        <div id="idContent">

          <div id="badgeTop">
            <ul>
              <li> <a href="<%=base_dir%>/secure/home" class="subscriberIcon"><span><%=request.getUserPrincipal().getName()%></span></a> </li>
            </ul>
          </div>

          <div id="badgeNav">
            <a class="settings" href="<%=base_dir%>/secure/preferences/" accesskey="p">preferences</a>
            <a class="logout" href="<%=base_dir%>/logout">logout</a>
          </div>
        </div>
        <% } else { %>
          Have an account? <a href="<%=base_dir%>/secure/home">Login</a> in or 
          <a href="<%=base_dir%>/register">register here</a>
        <% } %>
      </div>

      <div id="navigation">
        <ul>
          <li><a href="<%=base_dir%>/secure/home" title="Home">Home</a></li>

      <% 
         if ( location_id != null ) {
           %><li><a href="<%=base_dir%>/secure/location/<%=location_id%>/general" title="Location Home Page">Location Home Page</a></li><%
         }

         if ( request.getUserPrincipal() != null ) {
           // If the user is logged in, offer the site level actions available to the users role
           %>
              <li><a href="<%=base_dir%>/logout" title="Logout">Logout</a></li>
           <%
           // if ( request.isUserInRole("com.k_int.apps.colws.user") ) {
           // if (  request.isUserInRole("com.k_int.apps.colws.sysadmin") ) {
         } 
      %>
        </ul>
      </div>

    </div>

    <div id="content">
      <tiles:insert attribute="active_content"/>
    </div>

    <div id="footer">
      <span>
        Copyright <a href="http://www.k-int.com">knowledge integration</a> 2006.<br/>
        Best viewed in resolutions of 1024x768 and above.
      </span>
    </div>
  </body>
</html:html>
