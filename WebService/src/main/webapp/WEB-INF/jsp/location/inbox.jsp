<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="org.hibernate.*" %>
<%@ page import="org.hibernate.type.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.k_int.openrequest.webservices.generated.*" %>
<div id="LeftColumn">
</div>

<div id="RightColumn">
  <tiles:insert definition="location.actions.weblet"/>
</div>

<div id="MainContent">
<table>
<tr>
  <th>Message Type</th>
  <th>Event Code</th>
  <th>Date</th>
  <th>Read</th>
  <th>MessageId</th>
  <th>FromState</th>
  <th>ToState</th>
  <th>Additional Info</th>
<%
   BriefMessageInfoDTO[] result = (BriefMessageInfoDTO[]) request.getAttribute("unreadMessages");
   if ( result != null ) {
     for ( int i=0; i<result.length; i++ ) {
       BriefMessageInfoDTO bmi = (BriefMessageInfoDTO) result[i];
%>
<tr>
  <td><%=bmi.getType()%></td>
  <td><%=bmi.getEventCode()%></td>
  <td><%=bmi.getServiceDateTime()%></td>
  <td><%=bmi.isRead()%></td>
  <td><%=bmi.getMessageId()%></td>
  <td><%=bmi.getFromState()%></td>
  <td><%=bmi.getToState()%></td>
  <td><%=bmi.getAdditional()%></td>
</tr>
<%
    }
  }
%>
</table>
</div>
