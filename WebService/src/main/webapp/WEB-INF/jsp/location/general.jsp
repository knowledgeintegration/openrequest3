<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %><%
  String base_dir = request.getContextPath();
  String username = request.getRemoteUser();
  String location_id = (String) request.getAttribute("com.k_int.openrequest.location_id");

  org.springframework.web.context.WebApplicationContext ctx =
    org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext(application);

  org.hibernate.Session sess = null;

  try {
    org.hibernate.SessionFactory factory = (org.hibernate.SessionFactory) ctx.getBean("OpenRequestSessionFactory");
    sess = factory.openSession();
    sess.clear();

     org.hibernate.Query q = sess.createQuery("Select l from com.k_int.openrequest.db.Location.Location l where l.id = ?");

    q.setString(0,location_id);
    com.k_int.openrequest.db.Location.Location location = (com.k_int.openrequest.db.Location.Location)q.uniqueResult();

    if ( location != null ) {
%>
<div id="LeftColumn">
  <div id="ORPanel">
    <div id="Weblet">
      <h1>Location Symbols</h1><ul>
<%
  for ( java.util.Iterator i = location.getAliases().iterator(); i.hasNext(); ) {
    com.k_int.openrequest.db.Location.LocationSymbol symbol = (com.k_int.openrequest.db.Location.LocationSymbol) i.next();
      %><li><%=symbol%><%
  }
%>
    </ul></div>
  </div>
</div>

<div id="RightColumn">
  <tiles:insert definition="location.actions.weblet"/>
</div>

<div id="MainContent">
  <div id="ORPanel">
    <div id="Weblet">
      <h1><%=location.getName()%> - Location Home</h1>
      <p>
        Status report for this location, pending queues, etc, etc.
      </p>
    </div>
  </div>
</div>

<%
    }
  }
  catch ( org.hibernate.HibernateException he ) { 
    he.printStackTrace();
  }    
  finally {
    if ( sess != null ) {
      try {
        sess.close();
      }
      catch ( Exception e ) {
      }
    }  
  }    
%>
