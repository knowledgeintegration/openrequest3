<div id="ORPanel"><div id="Weblet">
<%
  String location_id = (String) request.getAttribute("com.k_int.openrequest.location_id");
  String base_dir = request.getContextPath();
  if ( location_id == null ) {
    %>An unexpected system error has occoured... No location data in request<%
  }
  else
  {
%>
      <h1>Location Actions</h2>
      <ul>
        <li><a href="<%=base_dir%>/secure/location/<%=location_id%>/newRequest/">New Request</a></li>
        <li><a href="<%=base_dir%>/secure/location/<%=location_id%>/newSymbol">New Symbol</a></li>
        <li><a href="<%=base_dir%>/secure/location/<%=location_id%>/requests">Request Search</a></li>
        <li><a href="<%=base_dir%>/secure/location/<%=location_id%>/inbox">Location Inbox</a></li>
        <li><a href="<%=base_dir%>/secure/location/<%=location_id%>/perms">Permissions</a></li>
      </ul>
<%
  }
%>
</div></div>
