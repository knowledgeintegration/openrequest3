<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %> <%
  String location_id = (String) request.getAttribute("com.k_int.openrequest.location_id");

  String base_dir = request.getContextPath();
  String username = request.getRemoteUser();

  if ( location_id == null ) {
    %>An unexpected system error has occoured... No location data in request<%
  }
  else {
    com.k_int.openrequest.webservices.generated.BriefTransactionInfoTypeDTO[] sr =
      (com.k_int.openrequest.webservices.generated.BriefTransactionInfoTypeDTO[]) request.getAttribute("openrequest.searchResult");

    java.util.Set available_actions = (java.util.Set) request.getAttribute("openrequest.possibleActions");

    org.springframework.web.context.WebApplicationContext ctx =
      org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext(application);
%>

<div id="LeftColumn">
</div>

<div id="RightColumn">
  <tiles:insert definition="location.actions.weblet"/>
</div>

<div id="MainContent">
  <div id="ORPanel">
    <div id="Weblet">
      <h1>Requests</h1>

<font size="-1">
<form action="actionSelectedRequests" method="post">
<table>
  <tr>
    <th>TG#</th>
    <th>T#</th>
    <th>Requester Reference</th>
    <th>TGQ</th>
    <th>Role</th>
    <th>Author/Title/Publication</th>
    <th>Last Message</th>
    <th>Request Status</th>
    <th>Select</th>
  </tr>
<%
  if ( sr != null ) {
    for ( int i=0; i<sr.length; i++ ) {
      com.k_int.openrequest.webservices.generated.BriefTransactionInfoTypeDTO ti = sr[i];

      String row_id = null;
      String trans_id = null;
      String trans_state = null;
      if ( ti.getCurrentTransactionId() != null ) {
        trans_state =  ti.getState();
        // If there is a live transaction, use it
        row_id = "TI:"+ti.getCurrentTransactionId();
        trans_id = ti.getCurrentTransactionId();
      }
      else {
        trans_state = "Idle";
        // Otherwise the action is on the transaction group
        row_id = "TG:"+ti.getInternalId();
        trans_id = "*None*";
      }

     
      String ta = "";
      if ( ti.getTitle() != null ) 
        ta = ta + ti.getTitle();
      if ( ti.getAuthor() != null )
        ta = ta + " / "+ ti.getAuthor();
      if ( ti.getPubdate() != null ) 
        ta = ta + ", "+ti.getPubdate();

      String last_msg_date = "";
      if ( ti.getLastMessage() != null ) {
        int month = ti.getLastMessage().get(java.util.Calendar.MONTH) + 1;

        last_msg_date = ti.getLastMessage().get(java.util.Calendar.DAY_OF_MONTH) + "/" +
                        month+"/"+
                        ti.getLastMessage().get(java.util.Calendar.YEAR);

      }

      String req_ref = "*None*";
      if ( ti.getRequesterReference() != null )
        req_ref = ti.getRequesterReference();

%>
      <tr>
        <td><%=ti.getInternalId()%></td>
        <td><%=trans_id%></td>
        <td><%=req_ref%></td>
        <td><%=ti.getTgq()%></td>
        <td><%=ti.getRole()%></td>
        <td><%=ta%></td>
        <td><%=last_msg_date%></td>
        <td><%=trans_state%></td>
        <td><input type="Checkbox" name="or.ReqSel.<%=row_id%>" value="true"/></td>
      </tr>
<%
    }
%>
    <tr><td colspan="8" align="Centre">
      <select name="ORAction">
<%
    if (  available_actions != null ) {
      for ( java.util.Iterator ai=available_actions.iterator(); ai.hasNext(); ) {
        String code = (String) ai.next();
        String msg = ctx.getMessage(code,null,code,request.getLocale());
        %> <option value="<%=code%>"><%=msg%></option> <%
      }
    }
%>
      </select><input type="Submit" value="Action ->"/>
    </td></tr>

    <tr><td colspan="8" align="Centre">Completed processing of <%=sr.length%> requests</td></tr>
<%
    }
    else {
%>
    <tr><td colspan="8" align="Centre">Search found no requests</td></tr>
<%
    }
  }
%>
</table>
</form>
</font>
    </div>
  </div>
</div>
