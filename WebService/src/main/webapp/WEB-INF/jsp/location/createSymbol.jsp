<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %><%
  String location_id = (String) request.getAttribute("com.k_int.openrequest.location_id");

  String base_dir = request.getContextPath();
  String username = request.getRemoteUser();

  if ( location_id == null ) {
    %>An unexpected system error has occoured... No location data in request<%
  }
  else
  {
%>
<div id="LeftColumn">
 <div id="ORPanel">
    <div id="Weblet">
      Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah 
      Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah 
      Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah Blah 
    </div>
  </div>
</div>

<div id="RightColumn">
  <tiles:insert definition="location.actions.weblet"/>
</div>

<div id="MainContent">
  <div id="ORPanel">
    <div id="Weblet">
      <h1>Create Symbol For This Location</h1>
<form action="<%=base_dir%>/secure/location/<%=location_id%>/processNewSymbol">
  <table>
    <tr><td colspan="2">Symbol Details</td></tr>
    <tr>
      <td align="right" valign="top">Authority :</td>
      <td>
        <select name="authority">
          <option value="KI">Knowledge Integration Ltd</option>
        </select>
        &nbsp; Symbol: <input type="text" name="symbol"/>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top">Display Name :</td>
      <td>
        <input type="text" name="symbol_name"/>
      </td>
    </tr>
    <tr><td colspan="2">Service For Symbol</td></tr>
    <tr>
      <td align="right" valign="top">Telecom System Address:</td>
      <td>
        <select name="symboltype">
          <option value="TCP">TCP(Direct Connection)</option>
          <option value="SMTP">SMTP (Electronic Mail)</option>
        </select>
        Address:
        <input type="text" name="service"/><br/>
        Service Address Strings <b>ill.host.name:port</b> for TCP or <b>user@mailserver.host.name</b> for SMTP.
      </td>
    </tr>
    <tr>
      <td align="right" valign="top">App specific #1 :</td>
      <td>
        <input type="text" name="app_specific_1"/>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top">Auth Method :</td>
      <td>
      <select name="auth_method">
        <option value="0">None</option>
        <option value="1">Username/Password</option>
      </select>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top">Username (optional) :</td>
      <td>
        <input type="text" name="username"/>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top">Password (optional) :</td>
      <td>
        <input type="text" name="password"/>
      </td>
    </tr>
  </table>
</form>
</div>
</div>
</div>
<%
  }
%>
