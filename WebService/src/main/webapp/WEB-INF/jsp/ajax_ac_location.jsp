<ul>
<%
  org.springframework.web.context.WebApplicationContext ctx =
    org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext(application);

  org.hibernate.Session sess = null;

  String query = request.getParameter("location_name");

  try {
    org.hibernate.SessionFactory factory = (org.hibernate.SessionFactory) ctx.getBean("OpenRequestSessionFactory");
    sess = factory.openSession();
    sess.clear();

     org.hibernate.Query q = sess.createQuery("Select l from com.k_int.openrequest.db.Location.Location l where l.name like ?");

    q.setString(0,"%"+query+"%");
    java.util.Iterator i = q.iterate();

    for ( ; i.hasNext(); ) {
      com.k_int.openrequest.db.Location.Location loc = (com.k_int.openrequest.db.Location.Location) i.next();
      %><li><%=loc.getName()%><span class="informal"> Location 1 symbol</span></li><%
    }
  }
  catch ( org.hibernate.HibernateException he ) {
    he.printStackTrace();
  }
  finally {
    if ( sess != null ) {
      try {
        sess.close();
      }
      catch ( Exception e ) {
      }
    }
  }
    // Produce entries like...
    // <li>Location 1<span class="informal"> Location 1 symbol</span></li>
    // <li>Location 2<span class="informal"> 1066 West Redlands Parkway</span></li>
%>
</ul>
