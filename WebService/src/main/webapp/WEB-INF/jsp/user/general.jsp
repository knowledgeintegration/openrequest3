<%
  String base_dir = request.getContextPath();
%>

<div id="LeftColumn">
  <div id="ORPanel">
    <div id="Weblet">
      <h1>User Menu</h1>
    </div>
  </div>
</div>

<div id="RightColumn">
</div>

<div id="MainContent">
  <div id="ORPanel">

    <div id="Weblet">
      <h1>Your Locations</h1>
      <ul>
<%
  java.util.List l = (java.util.List) request.getAttribute("com.k_int.openrequest.webapp.UserLocations");
  if ( ( l != null ) && ( l.size() > 0 ) ) {
    for ( java.util.Iterator i = l.iterator(); i.hasNext(); ) {
      com.k_int.openrequest.db.Location.Location loc = (com.k_int.openrequest.db.Location.Location) i.next();
      %><li><a href="<%=base_dir%>/secure/location/<%=loc.getId()%>/general"><%=loc.getName()%></a></li><%
    }
  }
  else {
    %><li>You are not currently assigned a management role for any location.</li><%
  }
%>
      </ul>
    </div>

    <div id="Weblet">
      <h1>Add a location</h1>
      <form action="<%=base_dir%>/secure/addLocation">
        Enter a Location Name or Symbol : 
          <input autocomplete="off" 
                 id="location_name" 
                 name="location_name" 
                 size="30" 
                 type="text" 
                 value="" /><br/>
          <div class="auto_complete" id="location_name_auto_complete">
          </div>
          <script type="text/javascript">new Ajax.Autocompleter('location_name', 'location_name_auto_complete', '<%=base_dir%>/ajax/ac/location', {})</script>
        <input type="submit"/>
      </form>
    </div>
    &nbsp;

    <div id="Weblet">
      <h1>User Home Page</h1>
      Your Requests
    </div>

  </div>
</div>


