<%
  String location_name = (String) request.getAttribute("loc_name");
  String base_dir = request.getContextPath();
%>
<div id="LeftColumn">
  <div id="ORPanel">
    <div id="Weblet">
      <h1>User Menu</h1>
    </div>
  </div>
</div>

<div id="RightColumn">
</div>

<div id="MainContent">
  <div id="ORPanel">
    <div id="Weblet">
      <h1>Create new Location</h1>
      The location name you have provided does not currently exist in this system. The following form will allow you to
         specify the details for this new location. You will be the primary administrator for this location, and will be asked
         to approve any subsequent requests to be added to this location.
      <br/>
      <form action="<%=base_dir%>/secure/processNewLocation">
        <table>
          <tr><td align="right" valign="top">Location name</td>
              <td><%=location_name%><input type="hidden" name="location_name" value="<%=location_name%>"/></td><td></td></tr>
          <tr><td align="right" valign="top">Location symbol</td>
              <td><input type="text" name="location_symbol"/></td>
              <td valign="top">Enter a uniqie symbol for the location in the format "Authority:Symbol". Examples include OR:SHEF1, KI:TEST1. The symbol must not exist in the database already</td></tr>
          <tr><td align="right" valign="top">Address Type</td>
              <td> <select name="address_type">
                     <option value="M">MIME</option>
                     <option value="T">TCP</option>
                   </select></td>
              <td valign="top">Enter the address type for this system - MIME and TCP are currently supported</td></tr>
          <tr><td align="right" valign="top">Address</td>
              <td><input type="text" name="address"/></td>
              <td valign="top">The actual system address in the format of an email address for MIME systems, or fully.qualified.host:port for tcp</td></tr>
          <tr><td colspan="3"><input type="submit"/></td></tr>
        </table>
      </form>
    </div>
  </div>
</div>
