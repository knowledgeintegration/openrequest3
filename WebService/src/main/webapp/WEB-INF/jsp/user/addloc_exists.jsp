<%
  String location_name = (String) request.getAttribute("loc_name");
%>
<div id="LeftColumn">
  <div id="ORPanel">
    <div id="Weblet">
      <h1>User Menu</h1>
    </div>
  </div>
</div>

<div id="RightColumn">
</div>

<div id="MainContent">
  <div id="ORPanel">

    <div id="Weblet">
      <h1>Add location to your account</h1>
      <p>The location you have selected "<%=location_name%>" has already been created in the system, and is managed by another
         OpenRequest user. If you wish to be added to this location please click <a href="">Here</a> to 
         submit a request to the location administrator for approval. If you feel you should be the
         primary administrator for this system, please submit a support request.
      </p>
  </div>
</div>
