<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%
  String base_dir = request.getContextPath();
%>


<div id="LeftColumn">
</div>

<div id="RightColumn">
  <tiles:insert definition="location.actions.weblet"/>
</div>

<div id="MainContent">
  <div id="ORPanel">
    <div id="Weblet">
      <h1>New User Registration</h1>

<div class="tab-form">
  <h3>Registration Data</h3>
    <table style="border: 1px solid #000; margin:10px; background: #dfdfff;"><tr><td>
    <center>
    <strong><small>Your information:</small></strong>
    <html:form method="post" action="/processRegistration">
      <table>
        <tr>
          <td valign="top">&nbsp;</td>
        </tr>
        <tr> 
          <td class="tab-form" valign="top">
            <label style="font-weight:bold;" for="Username">Requested Username* (Use this to log in)</label> <br/>
            <html:text styleClass="login-inputbox" style="background: #fff;" property="username" styleId="Username" />
            <html:errors property="username"/>
          </td>
        </tr>
        <tr> 
          <td class="tab-form" valign="top">
            <label style="font-weight:bold;" for="name">Your Name (For Display Purposes)</label> <br/>
            <html:text styleClass="login-inputbox" style="background: #fff;" property="name" styleId="Username" />
            <html:errors property="name"/>
          </td>
        </tr>
        <tr> 
          <td class="tab-form" valign="top">
            <label style="font-weight:bold;" for="Email">Email address*</label><br/>
            <html:text styleClass="login-inputbox"  style="background: #fff;" property="email" styleId="Email" />
            <html:errors property="email"/>
          </td>
        </tr>
        <tr> 
          <td class="tab-form" valign="top">
            <label style="font-weight:bold;" for="Password">Password*</label><br/>
            <html:password styleClass="login-inputbox"  style="background: #fff;" property="password" styleId="Password" />
            <html:errors property="password"/>
          </td>
        </tr>
        <tr> 
          <td class="tab-form" valign="top">
            <label style="font-weight:bold;" for="ConfirmPassword">Confirm Password*</label><br/>
            <html:password styleClass="login-inputbox" style="background: #fff;" property="confirmPassword" styleId="ConfirmPassword" />
            <html:errors property="confirmPassword"/>
          </td>
        </tr>
        <tr> 
          <td class="tab-form" valign="top">
            <label style="font-weight:bold;" for="Location Description">Location Description.</label>In this test system, you can automatically create a new location and symbol that can be used to send, receive and manage ILL transactions. Enter the description of your new location, for example "My LMS Vendor Company Test Location" After creating the new location, you will be able to add ILL symbols for that location in the UI.<br/>
            <html:text styleClass="login-inputbox" style="background: #fff;" property="locationDescription" styleId="locationDescription" />
            <html:errors property="locationDescription"/>
          </td>
        </tr>
        <tr>
          <td class="tab-form"><input class="submit-login" type="submit" name="submit" value="Register" /></td>
        </tr>
      </table>
      <ul>
        <html:messages id="message" property="<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>">
           <li><bean:write name="message"/></li>
        </html:messages>
      </ul>
    </html:form>
  </center>
  </td>
  </tr>
</table>
</div>
<div class="clearing">&#160;</div>
    </div>
  </div>
</div>

