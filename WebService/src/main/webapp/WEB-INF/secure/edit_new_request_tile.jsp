<%@ page language="java" %>
<%@ taglib  uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<html:html>
<head>
<title>
    <bean:message key="logon.title"/>
</title>
</head>

<body bgcolor="white">

<html:errors/>

<html:form action="/processNewRequest" focus="title">
<table border="0" width="100%">
    <tr>
        <th class="right">
            <bean:message key="prompt.request.title"/>
        </th>
        <td class="left">
            <html:text  property="requestData.ILLRequest.itemId.title" size="16"/>
        </td>
    </tr>
    <tr>
        <td class="right">
            <html:submit>
                <bean:message key="button.submit"/>
            </html:submit>
        </td>
        <td class="right">
            <html:reset>
                <bean:message key="button.reset"/>
            </html:reset>
        </td>
    </tr>
</table>

</html:form>
</body>
</html:html>
