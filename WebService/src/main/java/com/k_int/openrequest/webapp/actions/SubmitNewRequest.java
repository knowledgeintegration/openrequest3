package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;
import com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper;
import com.k_int.openrequest.webservices.generated.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Implementation of <strong>Action</strong> that starts a new request workflow.
 */

public final class SubmitNewRequest extends Action {

  private static Log log = LogFactory.getLog(SubmitNewRequest.class);

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) {

    HttpSession session = request.getSession();

    WebApplicationContext ctx =
      WebApplicationContextUtils.getRequiredWebApplicationContext(
        this.getServlet().getServletContext());

    String tab = mapping.getParameter();

    String context_id = "NewRequestContext";
    String request_id = "NewRequest";

    Map create_request_context = com.k_int.codbif2.web.util.Codbif2Helpers.lookupOrCreateContext(session, context_id);

    com.k_int.openrequest.webapp.dto.CreateRequestParametersDTO req = 
      (com.k_int.openrequest.webapp.dto.CreateRequestParametersDTO)  create_request_context.get(request_id);

    long result = 0;

    if ( req == null ) {
      return mapping.findForward("error");
    }
    else {
      log.debug("Sending new request........");

      // Figure out the requesting location ID
      long location_id = 0;

      // Populate the rota as an array rather than a list
      int ctr = 0;
      com.k_int.openrequest.webservices.generated.RotaElementDTO[] rota = 
         new com.k_int.openrequest.webservices.generated.RotaElementDTO[req.getRota().size()];

      for ( java.util.Iterator i = req.getRota().iterator(); i.hasNext(); ) {
        rota[ctr++] = (com.k_int.openrequest.webservices.generated.RotaElementDTO) i.next();
      }

      // Use the stateless helper to actually submit the request to the engine.
      try {
        log.debug("Calling helper create request method...");
        result = com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper.createRequest(
                           ctx, req.getRequesterSymbol(), req.getReqData(),rota);
        log.debug("Done...");
      }
      catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
        log.warn("Error sending request",rme);
        return mapping.findForward("error");
      }
      catch ( com.k_int.openrequest.integration.iface.DuplicateUserReferenceException dure ) {
        log.warn("Error sending request",dure);
        return mapping.findForward("error");
      }
    }

    return mapping.findForward("success");
  }
}

