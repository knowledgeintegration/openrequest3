package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;
import com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper;
import com.k_int.openrequest.webservices.generated.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Implementation of <strong>Action</strong> that starts a new request workflow.
 */

public final class UserHomeRedirectAction extends Action {

  private static Log log = LogFactory.getLog(UserHomeRedirectAction.class);

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) {

    log.debug("Checking");

    HttpSession session = request.getSession();

    WebApplicationContext ctx =
      WebApplicationContextUtils.getRequiredWebApplicationContext(
        this.getServlet().getServletContext());

    java.security.Principal p = request.getUserPrincipal();

    if ( p != null ) {
      String user_id = p.getName();
      log.debug("Redirecting to user home for "+user_id);

      ActionForward new_action_forward = new ActionForward();
      new_action_forward.setPath("/secure/user/"+user_id+"/general");
      new_action_forward.setRedirect(true);

      return new_action_forward;
    }
    else {
      log.warn("Attempt to access home page, no valid user");
    }

    return mapping.findForward("error");
  }
}

