package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;

import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;


/**
 * Implementation of <strong>Action</strong> that starts a search.
 */

public final class UserAddLocationAction extends Action {

  private static Log log = LogFactory.getLog(UserAddLocationAction.class);

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response)
  {
    HttpSession http_session = request.getSession(false);
    String location_name = request.getParameter("location_name");
    String result = "error";
    Session sess = null;

    WebApplicationContext ctx =
      WebApplicationContextUtils.getRequiredWebApplicationContext(
        this.getServlet().getServletContext());

    // com.k_int.svc.identity.service.IdentityService identity_service = (com.k_int.svc.identity.service.IdentityService) ctx.getBean("IdentityService");
   try {
      SessionFactory factory = (org.hibernate.SessionFactory) ctx.getBean("OpenRequestSessionFactory");
      sess = factory.openSession();
      sess.clear();

      if ( ( location_name != null ) && ( location_name.length() > 0 ) ) {

        // Determine if the location already exists.. if it does, then the user is requesting
        // that they become attached to that location and someone will have to grant that role to
        // them. Otherwise, they are creating a new location, and will be the primary administrator.
       
        Query q = sess.createQuery("Select l from com.k_int.openrequest.db.Location.Location l where l.name like ?");
        q.setString(0,location_name);
      
        List locations = q.list();

        switch ( locations.size() ) {
          case 0:
            log.debug("Creating new location for user request");
            result = "newloc";
            request.setAttribute("loc_name",location_name);
            break;
          case 1:
            log.debug("Adding user to existing location");
            com.k_int.openrequest.db.Location.Location l = (com.k_int.openrequest.db.Location.Location) locations.get(0);
            request.setAttribute("loc_name",location_name);
            request.setAttribute("loc_id",l.getId());
            result = "exists";
            break;
          default:
            // Error - Location name must be unique
            break;
        }
      }
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("Problem obtaining data for user home page",he);
    }
    finally {
      if ( sess != null ) {
        try {
          sess.close();
        }
        catch ( Exception e ) {
        }
      }
    }

    return (mapping.findForward(result));
  }
}

