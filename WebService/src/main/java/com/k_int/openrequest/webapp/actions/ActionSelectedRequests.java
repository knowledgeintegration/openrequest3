package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.k_int.openrequest.webapp.dto.*;

/**
 * Implementation of <strong>Action</strong> that starts a search.
 */

public final class ActionSelectedRequests extends Action {

  private static Log log = LogFactory.getLog(ActionSelectedRequests.class);

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) {

    HttpSession session = request.getSession();

    String location_id = mapping.getParameter();
    log.debug("Get location info for "+location_id);
    List request_ids = new ArrayList();
    String action = request.getParameter("ORAction");
    String result = action;

    WebApplicationContext ctx =
      WebApplicationContextUtils.getRequiredWebApplicationContext(
        this.getServlet().getServletContext());

    String context_id = "ActionRequestContext";
    String request_id = "ActionRequest";
    Map action_request_context = com.k_int.codbif2.web.util.Codbif2Helpers.lookupOrCreateContext(session, context_id);

    log.debug("Creating new request action object");
    request.setAttribute("codbif2.context.id", context_id);
    request.setAttribute("codbif2.pojo.id", request_id);

    try {
      for (Enumeration e = request.getParameterNames(); e.hasMoreElements(); ) {
        String param_name = (String)e.nextElement();
        log.debug("Checking "+param_name);
        if(param_name.startsWith("or.ReqSel.")) {
          String[] components = param_name.split("\\.");
          if ( components.length == 3 ) {
            log.debug("Adding "+components[2]);
            request_ids.add(components[2]);
          }
          else {
            log.debug("Unable to parse, length of split was "+components.length);
          }
        }
      }
  
      request.setAttribute("com.k_int.openrequest.location_id", location_id);
      ActionRequestParametersDTO action_request = new ActionRequestParametersDTO();
      action_request.setSourceLocation( Long.parseLong(location_id));
      action_request_context.put(request_id, action_request);

      log.debug("Action:"+action);
      log.debug("RequestIDs:"+request_ids);
  
      if ( action.equalsIgnoreCase("TG_START") ) {
        // Special Case.. TG start.
        for ( java.util.Iterator tgi = request_ids.iterator(); tgi.hasNext(); ) {
          String tg = (String) tgi.next();
          String[] components = tg.split(":");
          if ( ( components != null ) && ( components.length==2 ) ) {
            String the_tg = components[1];
            Long tg_long = new Long(Long.parseLong(components[1]));
            try {
              StatelessRequestManagerHelper.startMessagingSync(ctx,tg_long);
            }
            catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
              rme.printStackTrace();
            }
          }
          else {
            // Request for TG_START on an already running transaction group, mark as error and go back to results page.
            log.debug("TG_START on a TG already started "+tg);
          }
        }
        result = "success";
      }
      else {
        long[] trans_ids = new long[request_ids.size()];
        for ( java.util.Iterator tgi = request_ids.iterator(); tgi.hasNext(); ) {
          String tg = (String) tgi.next();
          String[] components = tg.split(":");
          int counter = 0;
          if ( ( components != null ) && ( components.length==2 ) ) {
            // Extract transaction ID, if it's a TG then it's in error
            trans_ids[counter++] = Long.parseLong(components[1]);
          }
        }
        action_request.setRequestIds(trans_ids);
        com.k_int.openrequest.webservices.generated.ActionMessageDTO action_message = new com.k_int.openrequest.webservices.generated.ActionMessageDTO();
        action_request.setMessageDTO(action_message);

        if ( action.equalsIgnoreCase("MSGreq") ) {
          log.debug("Message request");
           action_message.setMessage(new com.k_int.openrequest.webservices.generated.MessageActionDTO());
        }
        else if ( action.equalsIgnoreCase("CANreq") ) {
          log.debug("Cancel request");
          action_message.setCancel(new com.k_int.openrequest.webservices.generated.CancelActionDTO());
        }
        else if ( action.equalsIgnoreCase("SHIreq") ) {
          log.debug("Shipped request");
          action_message.setShipped(new com.k_int.openrequest.webservices.generated.ShippedActionDTO());
        }
        else if ( action.equalsIgnoreCase("RCVreq") ) {
          log.debug("Received request");
        }
        else if ( action.equalsIgnoreCase("STQreq") ) {
          log.debug("Status query request");
        }
        else if ( action.equalsIgnoreCase("STRreq") ) {
          log.debug("Status report request");
        }
        else if ( action.equalsIgnoreCase("LSTreq") ) {
          log.debug("Lost request");
        }
      }
    }
    finally {
    }

    return (mapping.findForward(result));
  }
}

