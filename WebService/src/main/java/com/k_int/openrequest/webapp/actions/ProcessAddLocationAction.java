package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;

import org.hibernate.*;
import org.hibernate.type.*; 

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.k_int.openrequest.webapp.form_beans.RegistrationFormBean;
                                    
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Implementation of <strong>Action</strong> that starts a search.
 */

public final class ProcessAddLocationAction extends Action {

  private static Log log = LogFactory.getLog(ProcessAddLocationAction.class);
  private static String GET_USERID_OQL = "select user.id from com.k_int.svc.identity.datamodel.AuthenticationDetailsHDO auth left outer join auth.user as user where auth.username=?";

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) {

    log.debug("ProcessAddLocationAction");

    HttpSession http_session = request.getSession(false);
    String location_name = request.getParameter("location_name");
    String location_symbol = request.getParameter("location_symbol");
    String address_type = request.getParameter("address_type");
    String address = request.getParameter("address");
    String username = null;

    java.security.Principal p = request.getUserPrincipal();
    if ( p != null )
      username = p.getName();

    WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(this.getServlet().getServletContext());

    org.hibernate.Session sess = null;

    try {
      SessionFactory factory = (org.hibernate.SessionFactory) ctx.getBean("OpenRequestSessionFactory");
      sess = factory.openSession();
      sess.clear();

      log.debug("Attempting to look up user "+username);

      Query q = sess.createQuery(GET_USERID_OQL);
      q.setString(0,username);
      Long user_id = (Long) q.uniqueResult();

      com.k_int.svc.identity.service.IdentityService identity_service = (com.k_int.svc.identity.service.IdentityService) ctx.getBean("IdentityService");
      
      log.debug("User "+user_id+"  wants to create a location: "+location_name);

      Long loc_code = com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper.createLocation(
               ctx, new com.k_int.openrequest.webservices.generated.LocationInfoDTO(location_name,location_symbol,address_type,address));

      log.debug("Created location : "+loc_code+" for name "+location_name);

      if ( ( loc_code != null ) && ( loc_code.longValue() >= 0 ) && ( user_id != null ) ) {

        log.debug("Redirecting to new location home page");

        identity_service.grant(user_id, "com.k_int.openrequest.location.admin",
          new com.k_int.svc.identity.service.TargetObjectIdentifier("com.k_int.openrequest.db.Location.Location",loc_code));

        ActionForward new_action_forward = new ActionForward();
        new_action_forward.setPath("/secure/location/"+loc_code+"/general");
        new_action_forward.setRedirect(true);
        return new_action_forward;
      }
      else {
        log.debug("No location available.. error");
      }

    }
    catch ( Throwable t ) {
      log.warn("Problem registering user: ",t);
    }
    finally {
      if ( sess != null ) {
        try {
          sess.close();
        }
        catch ( Exception e ) {
        }
      }
    }

    return (mapping.findForward("error"));
  }
}

