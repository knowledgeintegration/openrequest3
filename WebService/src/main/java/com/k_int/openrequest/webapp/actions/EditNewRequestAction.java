package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;
import com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper;
import com.k_int.openrequest.webservices.generated.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import org.hibernate.type.*;

/**
 * Implementation of <strong>Action</strong> that starts a new request workflow.
 */

public final class EditNewRequestAction extends Action {

  private static Log log = LogFactory.getLog(EditNewRequestAction.class);

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) {

    HttpSession session = request.getSession();

    WebApplicationContext ctx =
      WebApplicationContextUtils.getRequiredWebApplicationContext(
        this.getServlet().getServletContext());

    if ( (  mapping.getParameter() == null ) || (  mapping.getParameter().length()==0 ) )
      return mapping.findForward("errro");

    String[] loctab = mapping.getParameter().split(":");
      
    String loc=loctab[0];

    String tab="general";
    if ( loctab.length > 1 ) {
      tab=loctab[1];
    }

    request.setAttribute("com.k_int.openrequest.location_id", loctab[0]);

    String context_id = "NewRequestContext";
    String request_id = "NewRequest";

    Map create_request_context = com.k_int.codbif2.web.util.Codbif2Helpers.lookupOrCreateContext(session, context_id);

    com.k_int.openrequest.webapp.dto.CreateRequestParametersDTO req = 
      (com.k_int.openrequest.webapp.dto.CreateRequestParametersDTO)  create_request_context.get(request_id);

    if ( req == null ) {
      log.debug("Creating new request object");
      req = new com.k_int.openrequest.webapp.dto.CreateRequestParametersDTO();

      org.hibernate.Session sess = null;
      // set requesterSymbol in new request object.
      try {
        SessionFactory factory = (org.hibernate.SessionFactory) ctx.getBean("OpenRequestSessionFactory");
        sess = factory.openSession();
        sess.clear();
        org.hibernate.Query q = sess.createQuery("select l from com.k_int.openrequest.db.Location.Location l where l.id = ?");
        q.setString(0,loc);
        com.k_int.openrequest.db.Location.Location loc_record = (com.k_int.openrequest.db.Location.Location) q.uniqueResult();
        if ( loc_record != null )
          req.setRequesterSymbol(loc_record.getDefaultSymbol());
      }
      catch ( Exception e ) {
        e.printStackTrace();
      }
      finally {
        if ( sess != null ) {
          try {
            sess.close();
          }
          catch ( Exception e ) {
          }
        }
      }
      // req.getRota().add(new com.k_int.openrequest.webservices.generated.RotaElementDTO());
      create_request_context.put(request_id, req);
    }
    else {
      log.debug("Using existing request object");
    }

    request.setAttribute("codbif2.context.id", context_id);
    request.setAttribute("codbif2.pojo.id", request_id);

    ActionForward actionForward = null;

    if ( ( tab != null ) && ( tab.length() > 0 ) ) {
      actionForward = mapping.findForward(tab);
    }
    else {
      actionForward = mapping.findForward("success");
    }

    // ActionForward new_action_forward = new ActionForward(actionForward);
    // new_action_forward.setPath(new_path);
    return actionForward;
  }
}

