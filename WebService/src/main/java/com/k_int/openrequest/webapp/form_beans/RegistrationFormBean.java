package com.k_int.openrequest.webapp.form_beans;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import java.util.*;

public class RegistrationFormBean extends ActionForm {

  private String username;
  private String password;
  private String confirm_password;
  private String email;
  private String name;
  private String location_description;

  public void setUsername(String username) {this.username=username; }
  public void setPassword(String password) {this.password=password; }
  public void setConfirmPassword(String confirm_password) {this.confirm_password=confirm_password; }
  public void setEmail(String email) {this.email=email; }
  public void setName(String name) {this.name=name; }
  public void setLocationDescription(String location_description) {this.location_description=location_description; }

  public String getUsername() {return this.username; }
  public String getPassword() {return this.password; }
  public String getConfirmPassword() {return this.confirm_password; }
  public String getEmail() {return this.email; }
  public String getName() {return this.name; }
  public String getLocationDescription() {return this.location_description; }

}
