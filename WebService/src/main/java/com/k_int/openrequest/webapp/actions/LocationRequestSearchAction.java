package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;
import com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper;
import com.k_int.openrequest.webservices.generated.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Implementation of <strong>Action</strong> that starts a new request workflow.
 */

public final class LocationRequestSearchAction extends Action {

  private static Log log = LogFactory.getLog(LocationRequestSearchAction.class);

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) {

    log.debug("Request Search");

    HttpSession session = request.getSession();

    String location_id = mapping.getParameter();

    WebApplicationContext ctx =
      WebApplicationContextUtils.getRequiredWebApplicationContext(
        this.getServlet().getServletContext());

    try {
      com.k_int.openrequest.webservices.generated.SearchRequestDTO qry = new com.k_int.openrequest.webservices.generated.SearchRequestDTO();
      // qry.setRole("REQ"); // Or RESP
      log.debug("Setting location to "+location_id);
      qry.setLocationId(location_id);
      com.k_int.openrequest.webservices.generated.BriefTransactionInfoTypeDTO[] sr = StatelessRequestManagerHelper.transactionSearch(ctx, qry);


      if ( sr != null ) {
        Set all_states = new HashSet();
        Set possible_actions = new HashSet();

        for ( int i=0; i<sr.length; i++ ) {
          String state_model = null;
          String state_code = null;
          if ( sr[i].getState() != null ) {
            state_model = sr[i].getStateModel();
            state_code = sr[i].getState();
          }
          else {
            state_model = "REQ/TG";
            state_code = "IDLE";
          }

          String code = state_model+":"+state_code;

          if ( all_states.contains(code) ) {
            log.debug("Already handled: "+code);
          }
          else {
            log.debug("Process: "+code);
            all_states.add(code);
            // Now determine what the available actions are for the given code.
            String[] actions = StatelessRequestManagerHelper.listValidActions(ctx, state_model, state_code);
            if ( actions != null ) {
              for ( int i2=0; i2<actions.length; i2++ ) {
                log.debug("Possible action: "+actions[i2]);
                possible_actions.add(actions[i2]);
              }
            }
          }
        }

        log.debug("All states : "+all_states);
        log.debug("Possible actions : "+possible_actions);
  
        request.setAttribute("com.k_int.openrequest.location_id", location_id);
        request.setAttribute("openrequest.searchResult",sr);
        request.setAttribute("openrequest.allStatesInSearchResult",all_states);
        request.setAttribute("openrequest.possibleActions",possible_actions);
      }
      else {
        log.warn("Search RMI op returned NULL");
      }
    }
    catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
      rme.printStackTrace();
      return mapping.findForward("error");
    }
    finally {
    }

    return mapping.findForward("success");
  }
}

