package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import javax.naming.*;
import org.apache.struts.action.*;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;
import org.hibernate.*;
import org.hibernate.type.*;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.k_int.openrequest.webapp.form_beans.RegistrationFormBean;
                                                                                                                                          
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Implementation of <strong>Action</strong> that starts a search.
 */

public final class ProcessRegistration extends Action {

  private static Log log = LogFactory.getLog(ProcessRegistration.class);

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) {

    RegistrationFormBean reg_form = (RegistrationFormBean)form;

    ActionMessages messages = new ActionMessages();
    ActionErrors errors = new ActionErrors();

    // msg = new ActionMessage("data.continue");
    // messages.add("message2", msg);

    log.debug("Processing registration....");

    String result = "";

    if ( ( reg_form.getUsername() == null ) || ( reg_form.getUsername().length() == 0 ) ) {
      // messages.add("username", new ActionMessage("reg.missing.username"));
      log.debug("Missing username");
      errors.add("username", new ActionError("reg.missing.username"));
      result = "validation_failure";
    }

    if ( ( reg_form.getPassword() == null ) || ( reg_form.getPassword().length() == 0 ) ) {
      log.debug("Missing password");
      errors.add("password", new ActionError("reg.missing.password"));
      // messages.add("password", new ActionMessage("reg.missing.password"));
      result = "validation_failure";
    }

    if ( ( reg_form.getConfirmPassword() == null ) || ( reg_form.getConfirmPassword().length() == 0 ) ) {
      // messages.add("confirmPassword", new ActionMessage("reg.missing.confirmation_password"));
      log.debug("Missing confirm pass");
      errors.add("confirmPassword", new ActionError("reg.missing.confirmation_password"));
      result = "validation_failure";
    }

    if ( ( reg_form.getConfirmPassword() != null ) && 
         ( reg_form.getPassword() != null ) &&
         ( ! reg_form.getPassword().equals(reg_form.getConfirmPassword() ) ) ) {
      log.debug("Password mismatch");
      // messages.add("password", new ActionMessage("reg.password.mismatch"));
      errors.add("password", new ActionError("reg.password.mismatch"));
      result = "validation_failure";
    }

    if ( result.equals("validation_failure") ) {
      // Dont do any more
      System.err.println("Validation failure... go back to web page");
    }
    else {
      try
      {
        // 1. Check that the supplied username is not already in use
        WebApplicationContext ctx =
          WebApplicationContextUtils.getRequiredWebApplicationContext(
                  this.getServlet().getServletContext());
                                                                                                                                            
        com.k_int.svc.identity.service.IdentityService identity_service = (com.k_int.svc.identity.service.IdentityService) ctx.getBean("IdentityService");

        String username = reg_form.getUsername();
        com.k_int.svc.identity.service.SystemUserDTO user = identity_service.findUsersByUsername(username);
  
        if ( user != null ) {
          // Not unique
          errors.add("username", new ActionError("reg.duplicate.username"));
          // messages.add("username", new ActionMessage("reg.duplicate.username"));
          result = "validation_failure";
        }
        else {
          // The user identity
          log.debug("Calling identity service create user...");
          user = new com.k_int.svc.identity.service.SystemUserDTO(username,
                                                                  username,
                                                                  reg_form.getName(),
                                                                  reg_form.getEmail(),
                                                                  null,
                                                                  new java.rmi.dgc.VMID().toString());


          Long user_id = identity_service.registerUser(user, reg_form.getPassword());
          identity_service.addRole(username, "com.k_int.openrequest.user", false);

          // identity_service.grant(username,"com.k_int.apps.colws.user", Boolean.FALSE);

          request.setAttribute("reg_usr",user);

          if ( ( reg_form.getLocationDescription() != null ) && ( reg_form.getLocationDescription().length() > 0 ) ) {
            // User supplied a location description...
            log.debug("User wants to create a location: "+reg_form.getLocationDescription());
            Long loc_code = com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper.createLocation(
                           ctx, new com.k_int.openrequest.webservices.generated.LocationInfoDTO(reg_form.getLocationDescription(),null,null,null));

            log.debug("Created location : "+loc_code+" for name "+reg_form.getLocationDescription());

            log.debug("Testing for grant: "+user_id);
            // Now grant the location permission 
            if ( ( loc_code != null ) && ( loc_code.longValue() >= 0 ) && ( user_id != null ) ) {
              log.debug("User Id is present, granting new user admin permission for the new location");

              identity_service.grant(user_id,
                                     "com.k_int.openrequest.location.admin",
                                     new com.k_int.svc.identity.service.TargetObjectIdentifier("com.k_int.openrequest.db.Location.Location",loc_code));
            }
            else { 
              log.warn("Problem creating grant.. user not present or location creation failed.");
            }
          }

          result = "success";
        }
      }
      catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
        log.warn("Problem creating location: ",rme);
      }
      catch ( com.k_int.svc.identity.service.IdentityServiceException ise ) {
        log.warn("Problem registering user: ",ise);
      }
      catch ( Throwable t ) {
        log.warn("Problem registering user: ",t);
      }
      finally {
        log.debug("Create User Action Try Catch Block Completed");
      }

    }

    System.err.println("Result of process reg is "+result);
    saveErrors(request, errors);
    saveMessages(request, messages);
    return (mapping.findForward(result));
  }

}
