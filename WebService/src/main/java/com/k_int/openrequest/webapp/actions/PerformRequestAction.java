package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Implementation of <strong>Action</strong> that starts a search.
 */

public final class PerformRequestAction extends Action {

  private static Log log = LogFactory.getLog(PerformRequestAction.class);

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) {

    HttpSession session = request.getSession();

    log.debug("PerformRequestAction");

    WebApplicationContext ctx =
      WebApplicationContextUtils.getRequiredWebApplicationContext(
        this.getServlet().getServletContext());

    String context_id = "ActionRequestContext";
    String request_id = "ActionRequest";
    String result = "success";

    try {
      Map action_request_context = com.k_int.codbif2.web.util.Codbif2Helpers.lookupOrCreateContext(session, context_id);

      com.k_int.openrequest.webapp.dto.ActionRequestParametersDTO req =
        (com.k_int.openrequest.webapp.dto.ActionRequestParametersDTO)  action_request_context.get(request_id);

      if ( req != null ) {
        log.debug("Actioning transactions, got DTO");
        log.debug("Target locations: "+req.getRequestIds());
        log.debug("Source location: "+req.getSourceLocation());
        if ( req.getMessageDTO() != null ) {
          log.debug("Calling backend service");

          // Process action.
          String res = com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper.actionRequests(ctx,
                                         req.getSourceLocation(),
                                         req.getRequestIds(), 
                                         req.getMessageDTO());
        }
      }
      else {
        log.debug("Actioning transactions, cant get DTO");
        result = "error";
      }
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
    }

    return (mapping.findForward(result));
  }
}

