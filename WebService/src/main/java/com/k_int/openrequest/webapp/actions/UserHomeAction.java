package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;
import com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper;
import com.k_int.openrequest.webservices.generated.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

/**
 * Implementation of <strong>Action</strong> that starts a new request workflow.
 */

public final class UserHomeAction extends Action {

  private static Log log = LogFactory.getLog(UserHomeAction.class);

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) {

    HttpSession session = request.getSession();

    String user_id = mapping.getParameter();

    log.debug("Find locations for user "+user_id);

    WebApplicationContext ctx =
      WebApplicationContextUtils.getRequiredWebApplicationContext(
        this.getServlet().getServletContext());

    org.hibernate.Session sess = null;

    try {
      SessionFactory factory = (org.hibernate.SessionFactory) ctx.getBean("OpenRequestSessionFactory");
      sess = factory.openSession();
      sess.clear();

      // Just for reference, the following insert would grant the administrator permission to access all locations
      // insert into PERMISSION(NAME) values ('com.k_int.openrequest.location.admin');
      // insert into IDENT_GRANT(PARTY_ID,PERM_ID,TARGET_OBJECT_CLASS) values (p,p,'com.k_int.openrequest.db.Location.Location');
      // insert into IDENT_GRANT_KEY values ( 1,'%',0 );

      // Step 1 - Identify locations this user has permission to manage
      Query q = sess.createQuery("Select l from com.k_int.openrequest.db.Location.Location l, com.k_int.svc.identity.datamodel.GrantHDO g, com.k_int.svc.identity.datamodel.AuthenticationDetailsHDO a where a.username=? and g.party.id=a.user.id and g.permission.name like ? and g.targetObjectClass like ? and l.id like g.keys[0]");
      q.setString(0,user_id); // UserId that will join Auth Details through Party to get PartyID
      q.setString(1,"com.k_int.openrequest.location.admin"); // Permission Name We Are Looking For
      q.setString(2,"com.k_int.openrequest.db.Location.Location"); // Target Class We are looking for 

      // Essentially this query will find all grants for the given user on the specified object type, it then joins back to the
      // Location object and finds all locations on which the specified grant has been made. If a wildcard grant has been made,
      // it's likely to be all locations, which could be a lot.
      List locations = q.list();
      log.debug("Result of query: "+locations);
      request.setAttribute("com.k_int.openrequest.webapp.UserLocations",locations);
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("Problem obtaining data for user home page",he);
    }
    finally {
      if ( sess != null ) {
        try {
          sess.close();
        }
        catch ( Exception e ) {
        }
      }
    }

    return mapping.findForward("success");
  }
}

