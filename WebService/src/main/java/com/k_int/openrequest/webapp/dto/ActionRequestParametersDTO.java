package com.k_int.openrequest.webapp.dto;

import java.util.List;
import java.util.ArrayList;

public class ActionRequestParametersDTO implements java.io.Serializable {

  public long[] request_ids = null;
  public com.k_int.openrequest.webservices.generated.ActionMessageDTO message_dto;
  public long source_location;

  public com.k_int.openrequest.webservices.generated.ActionMessageDTO getMessageDTO() {
    return message_dto;
  }

  public void setMessageDTO(com.k_int.openrequest.webservices.generated.ActionMessageDTO message_dto) {
    this.message_dto = message_dto;
  }

  public long[] getRequestIds() {
    return request_ids;
  }

  public void setRequestIds(long[] request_ids) {
    this.request_ids=request_ids;
  }

  public void setSourceLocation(long source_location) {
    this.source_location = source_location;
  }

  public long getSourceLocation() {
    return source_location;
  }
}
