package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;
import com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper;
import com.k_int.openrequest.webservices.generated.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Implementation of <strong>Action</strong> that starts a new request workflow.
 */

public final class AddRotaEntry extends Action {

  private static Log log = LogFactory.getLog(AddRotaEntry.class);

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) {

    HttpSession session = request.getSession();

    WebApplicationContext ctx =
      WebApplicationContextUtils.getRequiredWebApplicationContext(
        this.getServlet().getServletContext());

    String tab = mapping.getParameter();

    String context_id = "NewRequestContext";
    String request_id = "NewRequest";

    Map create_request_context = com.k_int.codbif2.web.util.Codbif2Helpers.lookupOrCreateContext(session, context_id);

    com.k_int.openrequest.webapp.dto.CreateRequestParametersDTO req = 
      (com.k_int.openrequest.webapp.dto.CreateRequestParametersDTO)  create_request_context.get(request_id);

    System.err.println("Processing rota entry....");

    if ( req == null ) {
      return mapping.findForward("error");
    }
    else {
      // Locate the rota and add the new entry
      String new_symbol = request.getParameter("new_rota_symbol");
      if ( ( new_symbol != null ) && ( new_symbol.length() > 0 ) ) {
        req.getRota().add ( new com.k_int.openrequest.webservices.generated.RotaElementDTO(null,null,new_symbol) );
      }
    }
    return mapping.findForward("success");
  }
}

