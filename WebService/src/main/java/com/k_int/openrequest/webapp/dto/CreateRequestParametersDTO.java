package com.k_int.openrequest.webapp.dto;

import java.util.List;
import java.util.ArrayList;

public class CreateRequestParametersDTO implements java.io.Serializable {

  public String requester_symbol;
  public com.k_int.openrequest.webservices.generated.RequesterViewDTO req_data = null;
  public List rota = new ArrayList();

  public com.k_int.openrequest.webservices.generated.RequesterViewDTO getReqData() {
    return req_data;
  }

  public void setReqData(com.k_int.openrequest.webservices.generated.RequesterViewDTO req_data) {
    this.req_data = req_data;
  }

  public List getRota() {
    return rota;
  }

  public void setRota(List rota) {
    this.rota=rota;
  }

  public String getRequesterSymbol() {
    return requester_symbol;
  }

  public void setRequesterSymbol(String requester_symbol) {
    this.requester_symbol = requester_symbol;
  }
}
