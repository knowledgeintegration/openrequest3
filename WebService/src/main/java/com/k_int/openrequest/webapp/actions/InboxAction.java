package com.k_int.openrequest.webapp.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.tiles.ComponentContext;
import org.apache.struts.tiles.actions.TilesAction;
import com.k_int.openrequest.webservices.provider.StatelessRequestManagerHelper;
import com.k_int.openrequest.webservices.generated.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Implementation of <strong>Action</strong> that starts a search.
 */

public final class InboxAction extends Action {

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) {

    WebApplicationContext ctx =
      WebApplicationContextUtils.getRequiredWebApplicationContext(
        this.getServlet().getServletContext());

    String location_id_str = mapping.getParameter();
    System.err.println("Get Inbox info for "+location_id_str);
    Long location_id = Long.parseLong(location_id_str);
    
    BriefMessageInfoDTO[] result = StatelessRequestManagerHelper.getUnreadMessages(ctx, location_id.longValue());

    request.setAttribute("unreadMessages",result);

    return (mapping.findForward("success"));
  }
}

