package com.k_int.openrequest.webservices.provider;

import org.apache.axis.MessageContext;
import org.apache.axis.transport.http.HTTPConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.k_int.openrequest.integration.iface.*;

public class StatelessRequestManagerHelper {

  public static Log log = LogFactory.getLog(StatelessRequestManagerHelper.class);

  public static com.k_int.openrequest.webservices.generated.BriefMessageInfoDTO[] getUnreadMessages(ApplicationContext ctx,
                                                                                             long location_id) {

    com.k_int.openrequest.webservices.generated.BriefMessageInfoDTO[] result = null;

    // Getting hold of spring application context
    try {
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");
      BriefMessageInfoDTO[] api_res = srm.getUnreadMessages(new Long(location_id));
      if ( api_res != null ) {
        result = new com.k_int.openrequest.webservices.generated.BriefMessageInfoDTO[api_res.length];
        for ( int i=0; i<api_res.length; i++ ) {
          java.util.GregorianCalendar gc = new java.util.GregorianCalendar();

          if ( api_res[i].getServiceDateTime() != null )
            gc.setTime(api_res[i].getServiceDateTime());

          result[i] = new com.k_int.openrequest.webservices.generated.BriefMessageInfoDTO(
                                          api_res[i].getAdditional(),
                                          api_res[i].getEventCode(),
                                          api_res[i].getFromState(),
                                          api_res[i].getMessageId(),
                                          api_res[i].isRead().booleanValue(),
                                          gc,
                                          api_res[i].getToState(),
                                          api_res[i].getType() );
        }
      }
      else {
        result = new com.k_int.openrequest.webservices.generated.BriefMessageInfoDTO[0];
      }
    }
    finally {
    }

    return result;
  }

  public static boolean setMessageReadStatus(ApplicationContext ctx,
                                      long location_id, 
                                      long message_id) {
    boolean result = false;
    log.debug("setMessageReadStatus loc:"+location_id+" msg:"+message_id);
    try {
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");
      Boolean svc_res = srm.setMessageReadStatus(location_id,message_id);
      if ( svc_res != null ) {
        result = svc_res.booleanValue();
      }
    }
    finally {
    }
    return result;
  }

  public static com.k_int.openrequest.webservices.generated.BriefTransactionInfoTypeDTO[] transactionSearch(
                                                                ApplicationContext ctx, 
                                                                com.k_int.openrequest.webservices.generated.SearchRequestDTO query) 
                                 throws com.k_int.openrequest.integration.iface.RequestManagerException {
    com.k_int.openrequest.webservices.generated.BriefTransactionInfoTypeDTO[] result = null;
    try {
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");

      // Convert the local query DTO to the backend RMI interface DTO
      RequestManagerQueryDTO be_qry = new RequestManagerQueryDTO();
      be_qry.setLocationId(query.getLocationId());
      be_qry.setRole(query.getRole());
      be_qry.setReadFlag(query.getReadFlag());
      be_qry.setPhase(query.getPhase());
      be_qry.setState(query.getState());
      be_qry.setRequestId(query.getRequestId());
      be_qry.setAuthor(query.getAuthor());
      be_qry.setTitle(query.getTitle());
      be_qry.setIdentifier(query.getIdentifier());
      be_qry.setResponder(query.getResponder());
      be_qry.setPatronId(query.getPatronId());

      log.debug("Calliing SRM Search function with query "+be_qry);
      ResultSetDTO sr = srm.search(be_qry);
   
      // Convert the returned ResultSetDTO into an array of BriefTransactionInfoTypeDTO objects and we're set
      if ( sr != null ) {
        if ( sr.results != null ) {
          result = new com.k_int.openrequest.webservices.generated.BriefTransactionInfoTypeDTO[sr.results.length];
          for ( int i=0; i<sr.results.length; i++ ) {
            java.util.Calendar c = null;
            if ( sr.results[i].getLastMsgDate() != null ) {
              c = java.util.Calendar.getInstance();
              c.setTime(sr.results[i].getLastMsgDate());
            }
            result[i] = new com.k_int.openrequest.webservices.generated.BriefTransactionInfoTypeDTO();
            result[i].setInternalId(sr.results[i].getInternalId().toString());
            result[i].setTgq(sr.results[i].getTGQ());
            result[i].setRequesterReference(sr.results[i].getRequesterRef());
            if ( sr.results[i].getRole() != null )
              result[i].setRole(sr.results[i].getRole().toString());
            result[i].setTitle(sr.results[i].getTitle());
            result[i].setAuthor(sr.results[i].getAuthor());
            result[i].setPubdate(sr.results[i].getPublication());
            result[i].setCurrentPartnerId(sr.results[i].getCurrentPartner());

            result[i].setLastMessage(c);

            result[i].setState(sr.results[i].getState());
            result[i].setStateModel(sr.results[i].getStateModel());
            
            if ( sr.results[i].isClosed() ) {
              result[i].setActive("false");
            }
            else {
              result[i].setActive("true");
            }

            if ( sr.results[i].getPhase() != null )
              result[i].setPhase(sr.results[i].getPhase().toString());

            result[i].setUnreadMessageCount(""+(sr.results[i].getUnreadMessageCount()));
            
            if ( sr.results[i].getProtocol() != null )
              result[i].setProtocol(sr.results[i].getProtocol().toString());

            result[i].setInitiatingBranch(sr.results[i].getInitiatingBranch());

            if ( sr.results[i].getCurrentTransId() != null ) 
              result[i].setCurrentTransactionId(sr.results[i].getCurrentTransId().toString());
          }
        }
        else {
          log.error("Search result array was null");
        }
      }
      else {
        log.error("Search result object null");
      }
    }
    finally {
      log.debug("Completed static search helper");
    }
    return result;
  }

  public static Long createLocation(ApplicationContext ctx, com.k_int.openrequest.webservices.generated.LocationInfoDTO location_info)
             throws RequestManagerException {
    log.debug("createLocation...");
    long result = -1;
    try {
      log.debug("getting handle to srm");
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");
      com.k_int.openrequest.integration.iface.ORLocationInfoDTO new_loc = 
        new com.k_int.openrequest.integration.iface.ORLocationInfoDTO(location_info.getLocationName(),
                                                                      location_info.getSymbol(),
                                                                      location_info.getAddressType(),
                                                                      location_info.getAddress());
      new_loc.setLocationName(location_info.getLocationName());
      Long res = srm.createLocation(new_loc);
      if ( res != null )
        result = res.longValue();
    }
    finally {
    }
    return result;
  }

  /* Helpers */

  public static long createRequest(ApplicationContext ctx,
                            String requester_symbol,
                            com.k_int.openrequest.webservices.generated.RequesterViewDTO request_data, 
                            com.k_int.openrequest.webservices.generated.RotaElementDTO[] rota)
             throws RequestManagerException,
                    DuplicateUserReferenceException {
    long result = -1;
    log.debug("createRequest...");

    try {
      log.debug("getting handle to srm");
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");
      com.k_int.openrequest.integration.iface.RequesterViewDTO nw_req = null;
      com.k_int.openrequest.integration.iface.RotaElementDTO[] nw_rota = null;

      log.debug("Converting request data into RMI dto");

      if ( request_data != null ) {
        nw_req = reqFromWSDTO(request_data);
      }

      log.debug("Converting rota into RMI dto");
      if ( rota != null ) {
        nw_rota = rotaFromWSDTO(rota);
      }

      // Import any defaults for the institution/location/symbol
      // Specifically needed:
      // Requester optional messages

      log.debug("Making remote call");
      result = srm.createTransGroup(nw_req, nw_rota, null, requester_symbol);
    }
    finally {
    }
    return result;
  }

  public static long createRequest(ApplicationContext ctx,
                                   String requester_symbol,
                                   com.k_int.openrequest.webservices.generated.RequesterViewDTO request_data, 
                                   com.k_int.openrequest.webservices.generated.RotaElementDTO[] rota, 
                                   java.lang.String mandatory_tgq)  
             throws RequestManagerException,
                    DuplicateUserReferenceException {
    long result = -1;
    try {
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");
      com.k_int.openrequest.integration.iface.RequesterViewDTO nw_req = null;
      com.k_int.openrequest.integration.iface.RotaElementDTO[] nw_rota = null;

      if ( request_data != null ) {
        nw_req = reqFromWSDTO(request_data);
      }

      if ( rota != null ) {
        nw_rota = rotaFromWSDTO(rota);
      }

      result = srm.createTransGroup(nw_req, nw_rota, mandatory_tgq, requester_symbol);
    }
    finally {
    }
    return result;
  }

  public static long startMessagingSync(ApplicationContext ctx, long transaction_group_id) throws RequestManagerException {
    log.debug("startMessagingSync for "+transaction_group_id);
    try {
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");
      srm.startMessagingSync(transaction_group_id);
    }
    finally {
    }
    return 0;
  }

  public static java.lang.String getVersion(ApplicationContext ctx) {
    String result=null;
    try {
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");
      result = srm.getVersion();
    }
    catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
      log.error("Problem with getVersion()",rme);
    }
    finally {
    }
    return result;
  }

  public static void startMessaging(ApplicationContext ctx,
                             long transaction_group_id) throws RequestManagerException{
    try {
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");
      srm.startMessaging(transaction_group_id);
    }
    finally {
    }
  }


  public static String[] listValidActions(ApplicationContext ctx, 
                                          String state_model, 
                                          String state_code) 
                        throws com.k_int.openrequest.integration.iface.RequestManagerException {
    String[] result = null;
    try {
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");
      result = srm.listValidActions(state_model,state_code,Boolean.TRUE);
    }
    finally {
    }
    return result;
  }

  private static com.k_int.openrequest.integration.iface.RequesterViewDTO reqFromWSDTO(
                      com.k_int.openrequest.webservices.generated.RequesterViewDTO req) {

    com.k_int.openrequest.integration.iface.RequesterViewDTO result = null;

    java.util.ArrayList service_types = new java.util.ArrayList();
    service_types.add(com.k_int.openrequest.integration.iface.ServiceType.LOAN);

    result = new com.k_int.openrequest.integration.iface.RequesterViewDTO(
      extractTransactionType(req.getILLRequest().getTransactionType()),
      extractPostalAddressDTO(req.getILLRequest().getDeliveryAddress()),  // Delivery Address
      extractDeliveryService(), // Delivery Service
      extractPostalAddressDTO(req.getILLRequest().getBillingAddress()), // Billing address
      service_types,
      extractRequesterOptionalMessages(),
      extractSearchType(req.getILLRequest().getSearchType()),
      extractSupplyMediumInfoType(),
      (PlaceOnHoldType) null, // extractPlaceOnHoldType(req),
      extractItemId(req.getILLRequest().getItemId()),
      extractCostInfoType(req.getILLRequest().getCostInfoType()),  
      false, // Forward Flag
      (String) null, // Requester Note
      (String) null, // Requester Ref Authority
      (String) null, // Requester Ref
      (String) null, // Client ID
      (String) null, // Copyright Compliance
      (RequesterSpecificMessageDTO) null // RequesterSpecificMessageDTO
      );

    return result;
  }

  public static CostInfoTypeDTO extractCostInfoType(com.k_int.openrequest.webservices.generated.CostInfoTypeDTO cost_info_type) {
    com.k_int.openrequest.integration.iface.CostInfoTypeDTO result = new com.k_int.openrequest.integration.iface.CostInfoTypeDTO();
    result.account_number = cost_info_type.getAccountNumber();
    result.currency_code = cost_info_type.getCostCurrencyCode();
    result.value = cost_info_type.getCostMonetaryValue();
    result.reciprocal_agreement = cost_info_type.getReciprocalAgreement() != null ? cost_info_type.getReciprocalAgreement().booleanValue() : false;
    result.will_pay_fee = cost_info_type.isWillPayFee();
    result.payment_provided = cost_info_type.getPaymentProvided() != null ? cost_info_type.getPaymentProvided().booleanValue() : false;
    return result;
  }

  public static com.k_int.openrequest.integration.iface.RotaElementDTO[] rotaFromWSDTO(com.k_int.openrequest.webservices.generated.RotaElementDTO[] rota) {

    com.k_int.openrequest.integration.iface.RotaElementDTO[] result = new com.k_int.openrequest.integration.iface.RotaElementDTO[rota.length];

    for ( int i=0; i<rota.length; i++ ) {
      com.k_int.openrequest.integration.iface.ServiceProtocol protocol = com.k_int.openrequest.integration.iface.ServiceProtocol.ISO;

      if ( rota[i].getProtocol() != null ) {
        if ( rota[i].getProtocol().equalsIgnoreCase("Email") ) {
          protocol = com.k_int.openrequest.integration.iface.ServiceProtocol.EMAIL;
        }
      }
      result[i] = new com.k_int.openrequest.integration.iface.RotaElementDTO(
                    rota[i].getSymbol(),
                    protocol,
                    rota[i].getMandatoryTransactionQualifier());
    }

    return result;
  }

  private static com.k_int.openrequest.integration.iface.TransactionType extractTransactionType(String in_trans_type) {
    com.k_int.openrequest.integration.iface.TransactionType result = null;

    if ( in_trans_type != null ) {
      if ( in_trans_type.equalsIgnoreCase("simple") || in_trans_type.equalsIgnoreCase("1") ) {  //  1
        result = com.k_int.openrequest.integration.iface.TransactionType.SIMPLE;
      }
      else if ( in_trans_type.equalsIgnoreCase("chained") ) {  // 2
        result = com.k_int.openrequest.integration.iface.TransactionType.CHAINED;
      }
      else if ( in_trans_type.equalsIgnoreCase("partitioned") ) {  //  3
        result = com.k_int.openrequest.integration.iface.TransactionType.PARTITIONED;
      }
    }
    return result;
  }

  private static com.k_int.openrequest.integration.iface.PostalAddressDTO 
    extractPostalAddressDTO(com.k_int.openrequest.webservices.generated.PostalAddressDTO addr) {

    com.k_int.openrequest.integration.iface.PostalAddressDTO result = null;

    if ( addr != null ) {
      result = new com.k_int.openrequest.integration.iface.PostalAddressDTO(
          addr.getName(),
          addr.getExtendedPostal(),
          addr.getStreetAndNumber(),
          addr.getPostOfficeBox(),
          addr.getCity(),
          addr.getRegion(),
          addr.getCountry(),
          addr.getPostcode());
    }

    return result;
  }

  private static com.k_int.openrequest.integration.iface.DeliveryServiceDTO extractDeliveryService() {
    return null;
  }

  private static com.k_int.openrequest.integration.iface.RequesterOptionalMessagesDTO extractRequesterOptionalMessages() {
    // boolean can_send_received,
    // boolean can_send_returned,
    // MessageOption requester_shipped,
    // MessageOption requester_checked_in
    return new com.k_int.openrequest.integration.iface.RequesterOptionalMessagesDTO(true,
                                                                                    true,
                                                                                    com.k_int.openrequest.integration.iface.MessageOption.DESIRES,
                                                                                    com.k_int.openrequest.integration.iface.MessageOption.DESIRES);
  }

  private static com.k_int.openrequest.integration.iface.SearchTypeDTO 
                     extractSearchType(com.k_int.openrequest.webservices.generated.SearchTypeDTO search_type) {

    com.k_int.openrequest.integration.iface.SearchTypeDTO result = null;

    if ( search_type != null ) {

      LevelOfService los = null;
      ExpiryFlag ef = null;
      java.util.Date need_before = null;
      java.util.Date expiry_date = null;
    
      if ( search_type.getLevelOfService() != null )
        los = LevelOfService.fromIso(search_type.getLevelOfService().charAt(0));

      if ( search_type.getNeedBeforeDate() != null )
        need_before = search_type.getNeedBeforeDate().getTime();

      if ( search_type.getExpiryFlag() != null )
        ef = ExpiryFlag.fromIso(Integer.parseInt(search_type.getExpiryFlag()));

      if ( search_type.getExpiryDate() != null ) 
        expiry_date = search_type.getExpiryDate().getTime();

      result = new com.k_int.openrequest.integration.iface.SearchTypeDTO(los,need_before,ef,expiry_date);

    }

    return result;
  }

  private static com.k_int.openrequest.integration.iface.SupplyMediumType[] extractSupplyMediumInfoType() {
    return null;
  }

  private static com.k_int.openrequest.integration.iface.PlaceOnHoldType extractPlaceOnHoldType() {
    return null;
  }

  private static com.k_int.openrequest.integration.iface.ItemIdDTO extractItemId(com.k_int.openrequest.webservices.generated.ItemIdDTO in_item_id) {
    com.k_int.openrequest.integration.iface.ItemIdDTO result =
      new com.k_int.openrequest.integration.iface.ItemIdDTO(
        null, // com.k_int.openrequest.integration.iface.ItemTypeExtended.intFromString(in_item_id.getItemType),
        null, // MediumType held_medium_type,
        in_item_id.getTitle(),
        in_item_id.getSubTitle(),
        in_item_id.getAuthor(),
        in_item_id.getSponsoringBody(),
        in_item_id.getPublisher(),
        in_item_id.getPlaceOfPublication(),
        in_item_id.getPublicationDate(),
        in_item_id.getIsbn(),
        in_item_id.getIssn(),
        in_item_id.getTitleOfArticle(),
        in_item_id.getAuthorOfArticle(),
        in_item_id.getVolumeIssue(),
        in_item_id.getPublicationDateOfComp(),
        in_item_id.getPagination(),
        null, // com.k_int.openrequest.db.IPIGSystemNumber system_no,
        in_item_id.getCallNumber(),
        null, // com.k_int.openrequest.db.IPIGSystemNumber nat_bib_no,
        in_item_id.getSeriesTitleNumber(),
        in_item_id.getVerificationReferenceSource(),
        in_item_id.getEdition()
      );
    return result;
  }

  public static java.lang.String actionRequests(ApplicationContext ctx,
                                                long sourceLocationId,
                                                long[] transactionId,
                                                com.k_int.openrequest.webservices.generated.ActionMessageDTO message) throws java.rmi.RemoteException, 
                                                                                                                             com.k_int.openrequest.integration.iface.RequestManagerException {
    
    log.debug("actionRequests");
    String result = null;
    try {
      log.debug("get remote stateless manager");
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");
      log.debug("Convert DTO and action");
      result = srm.actionMessage(sourceLocationId,transactionId,msgFromWSDTO(message));
    }
    finally {
      log.debug("WS actionRequest call completed");
    }
    return result;
  }

  private static ProtocolMessageDTO msgFromWSDTO(com.k_int.openrequest.webservices.generated.ActionMessageDTO message) throws com.k_int.openrequest.integration.iface.RequestManagerException {
    com.k_int.openrequest.integration.iface.ProtocolMessageDTO msg = null;
    if ( message != null ) {
      log.debug("msgFromWSDTO");
      if ( message.getShipped() != null ) {
        com.k_int.openrequest.webservices.generated.ShippedActionDTO ws_shipped = message.getShipped();
        log.debug("converting shipped DTO - due:"+ws_shipped.getDateDue());
        msg = new com.k_int.openrequest.integration.iface.ShippedMessageDTO(
          com.k_int.openrequest.integration.iface.ServiceType.fromIso((int)(ws_shipped.getShippedServiceType())), // ServiceType shipped_service_type,
          ws_shipped.getDateShipped() != null ? ws_shipped.getDateShipped().getTime() : null, // Date date_shipped,
          ws_shipped.getDateDue() != null ? ws_shipped.getDateDue().getTime() : null, // Date date_due,
          ws_shipped.getRenewable(), // boolean renewable,
          ws_shipped.getTotalCostCurrency(),
          ws_shipped.getTotalCostAmount(), // String total_cost_amount,
          null, // ShippedCondition shipped_conditions,
          null, // DeliveryService delivery_service,
          null, // Currency insured_for_currency,
          null, // String ins_for_amount,
          null, // Currency ins_on_ret_currency,
          null, // String ins_on_ret_amount,
          null, // SupplyMediumType[] mediums,
          null, // int[] units,
          null, // ResponderOptionalMessagesDTO responder_optional_messages,
          ws_shipped.getReturnToAddress() != null ? ws_shipped.getReturnToAddress().getName() : null, // String name,
          ws_shipped.getReturnToAddress() != null ? ws_shipped.getReturnToAddress().getExtendedPostal() : null, // String extendedPostal,
          ws_shipped.getReturnToAddress() != null ? ws_shipped.getReturnToAddress().getStreetAndNumber() : null, // String streetAndNumber,
          null, // PoBox
          ws_shipped.getReturnToAddress() != null ? ws_shipped.getReturnToAddress().getCity() : null, // String city,
          ws_shipped.getReturnToAddress() != null ? ws_shipped.getReturnToAddress().getRegion() : null, // String region,
          ws_shipped.getReturnToAddress() != null ? ws_shipped.getReturnToAddress().getCountry() : null, // String county,
          ws_shipped.getReturnToAddress() != null ? ws_shipped.getReturnToAddress().getPostcode() : null, // String postcode,
          ws_shipped.getNote() // String responder_note
        );
      }
      else if ( message.getAnswer() != null ) {
        if ( message.getAnswer().getAnswerConditional() != null ) {
          log.debug("converting answer conditional DTO");
          com.k_int.openrequest.webservices.generated.AnswerConditionalDTO ws_answer_conditional = message.getAnswer().getAnswerConditional();
          msg = new com.k_int.openrequest.integration.iface.ConditionalAnswerMessageDTO (
                             ws_answer_conditional.getConditions() != null ? Conditions.fromIso(ws_answer_conditional.getConditions().intValue()): null,
                             ws_answer_conditional.getDateForReply() != null ? ws_answer_conditional.getDateForReply().getTime() : null,
                             ws_answer_conditional.getLocations(),
                             ws_answer_conditional.getResponderNote());
        }
        else if ( message.getAnswer().getAnswerHoldPlaced() != null ) {
          log.debug("converting answer conditional DTO");
          com.k_int.openrequest.webservices.generated.AnswerHoldPlacedDTO ws_answer_hold_placed = message.getAnswer().getAnswerHoldPlaced();
          msg = new com.k_int.openrequest.integration.iface.HoldPlacedAnswerDTO (
             ws_answer_hold_placed.getEstimatedDateAvailable() != null ? ws_answer_hold_placed.getEstimatedDateAvailable().getTime() : null,
             ws_answer_hold_placed.getHoldPlacedMediumType() != null ? MediumType.fromIso(ws_answer_hold_placed.getHoldPlacedMediumType().intValue()) : null,
             ws_answer_hold_placed.getResponderNote());
        }
        else if ( message.getAnswer().getAnswerRetry() != null ) {
          log.debug("converting answer conditional DTO");
          com.k_int.openrequest.webservices.generated.AnswerRetryDTO ws_answer_retry = message.getAnswer().getAnswerRetry();
          msg = new com.k_int.openrequest.integration.iface.RetryAnswerDTO (
             ws_answer_retry.getReasonNotAvailable() != null ? ws_answer_retry.getReasonNotAvailable().intValue() : null,
             ws_answer_retry.getRetryDate() != null ? ws_answer_retry.getRetryDate().getTime(): null,
             ws_answer_retry.getResponderNote() );
        }
        else if ( message.getAnswer().getAnswerUnfilled() != null ) {
          log.debug("converting answer conditional DTO");
          com.k_int.openrequest.webservices.generated.AnswerUnfilledDTO ws_answer_unfilled = message.getAnswer().getAnswerUnfilled();
          msg = new com.k_int.openrequest.integration.iface.UnfilledAnswerDTO (
            ws_answer_unfilled.getReason() != null ? ReasonUnfilled.fromIso(ws_answer_unfilled.getReason().intValue()) : null,
            ws_answer_unfilled.getResponderNote());
        }
        else if ( message.getAnswer().getAnswerWillSupply() != null ) {
          log.debug("converting answer conditional DTO");
          com.k_int.openrequest.webservices.generated.AnswerWillSupplyDTO ws_answer_will_supply = message.getAnswer().getAnswerWillSupply();
          msg = new com.k_int.openrequest.integration.iface.WillSupplyAnswerDTO (
            ws_answer_will_supply.getReason() != null ? ReasonWillSupply.fromIso(ws_answer_will_supply.getReason().intValue()) : null,
            ws_answer_will_supply.getSupplyDate() != null ? ws_answer_will_supply.getSupplyDate().getTime() : null,
            ws_answer_will_supply.getReturnToAddress() != null ? ws_answer_will_supply.getReturnToAddress().getName() : null,
            ws_answer_will_supply.getReturnToAddress() != null ? ws_answer_will_supply.getReturnToAddress().getExtendedPostal() : null,
            ws_answer_will_supply.getReturnToAddress() != null ? ws_answer_will_supply.getReturnToAddress().getStreetAndNumber() : null,
            ws_answer_will_supply.getReturnToAddress() != null ? ws_answer_will_supply.getReturnToAddress().getCity() : null,
            ws_answer_will_supply.getReturnToAddress() != null ? ws_answer_will_supply.getReturnToAddress().getRegion() : null,
            ws_answer_will_supply.getReturnToAddress() != null ? ws_answer_will_supply.getReturnToAddress().getCountry() : null,
            ws_answer_will_supply.getReturnToAddress() != null ? ws_answer_will_supply.getReturnToAddress().getPostcode() : null,
            ws_answer_will_supply.getResponderNote());
        }
      }
      else if ( message.getConditionalReply() != null ) {
        log.debug("converting conditional reply DTO");
        com.k_int.openrequest.webservices.generated.ConditionalReplyDTO ws_condrep = message.getConditionalReply();
        msg = new com.k_int.openrequest.integration.iface.ConditionalReplyMessageDTO(
            ws_condrep.getAnswer(),
            ws_condrep.getResponderNote());
      }
      else if ( message.getCancel() != null ) {
        log.debug("converting cancel DTO");
        com.k_int.openrequest.webservices.generated.CancelActionDTO ws_cancel = message.getCancel();
        msg = new com.k_int.openrequest.integration.iface.CancelMessageDTO(ws_cancel.getNote());
      }
      else if ( message.getCancelReply() != null ) {
        log.debug("converting cancel reply DTO");
        com.k_int.openrequest.webservices.generated.CancelReplyActionDTO ws_cancel_reply = message.getCancelReply();
        msg = new com.k_int.openrequest.integration.iface.CancelReplyMessageDTO(
            ws_cancel_reply.getAnswer(),
            ws_cancel_reply.getNote());
      }
      else if ( message.getReceived() != null ) {
        log.debug("converting received DTO");
        com.k_int.openrequest.webservices.generated.ReceivedActionDTO ws_received = message.getReceived();
        msg = new com.k_int.openrequest.integration.iface.ReceivedMessageDTO(
            ws_received.getDateReceived() != null ? ws_received.getDateReceived().getTime() : null,
            ServiceType.fromIso((int)(ws_received.getShippedServiceType())),
            ws_received.getNote());
      }
      else if ( message.getRecall() != null ) {
        log.debug("converting recall DTO");
        com.k_int.openrequest.webservices.generated.RecallActionDTO ws_recall = message.getRecall();
        msg = new com.k_int.openrequest.integration.iface.RecallMessageDTO(
            ws_recall.getNote());
      }
      else if ( message.getReturned() != null ) {
        log.debug("converting returned DTO");
        com.k_int.openrequest.webservices.generated.ReturnedActionDTO ws_returned = message.getReturned();
        msg = new com.k_int.openrequest.integration.iface.ReturnedMessageDTO(
            ws_returned.getDateReturned() != null ? ws_returned.getDateReturned().getTime() : null,
            ws_returned.getReturnedVia(),
            ws_returned.getInsuredForCurrency(),
            ws_returned.getInsuredAmount(),
            ws_returned.getRequesterNote());
      }
      else if ( message.getCheckedIn() != null ) {
        log.debug("converting checked-in DTO");
        com.k_int.openrequest.webservices.generated.CheckedInActionDTO ws_checked_in = message.getCheckedIn();
        msg = new com.k_int.openrequest.integration.iface.CheckedInMessageDTO(
          ws_checked_in.getDateCheckedIn() != null ? ws_checked_in.getDateCheckedIn().getTime() : null,
          ws_checked_in.getNote());
      }
      else if ( message.getOverdue() != null ) {
        com.k_int.openrequest.webservices.generated.OverdueActionDTO ws_overdue = message.getOverdue();
        log.debug("converting overdue DTO - date_due="+ws_overdue.getDateDue());
        msg = new com.k_int.openrequest.integration.iface.OverdueMessageDTO(
          ws_overdue.getDateDue() != null ? ws_overdue.getDateDue().getTime() : null,
          ws_overdue.getRenewable() != null ? ws_overdue.getRenewable().booleanValue() : null,
          ws_overdue.getNote());
      }
      else if ( message.getRenew() != null ) {
        log.debug("converting renew DTO");
        com.k_int.openrequest.webservices.generated.RenewActionDTO ws_renew = message.getRenew();
        msg = new com.k_int.openrequest.integration.iface.RenewMessageDTO(
          ws_renew.getDesiredDateDue() != null ? ws_renew.getDesiredDateDue().getTime(): null,
          ws_renew.getNote());
      }
      else if ( message.getRenewAnswer() != null ) {
        log.debug("converting renew answer DTO");
        com.k_int.openrequest.webservices.generated.RenewAnswerActionDTO ws_renew_answer = message.getRenewAnswer();
        msg = new com.k_int.openrequest.integration.iface.RenewAnswerMessageDTO(
          ws_renew_answer.getAnswer() != null ? ws_renew_answer.getAnswer().booleanValue() : false,
          ws_renew_answer.getDueDate() != null ? ws_renew_answer.getDueDate().getTime() : null,
          ws_renew_answer.getRenewable() != null ? ws_renew_answer.getRenewable().booleanValue() : false,
          ws_renew_answer.getNote());
      }
      else if ( message.getLost() != null ) {
        log.debug("converting lost DTO");
        com.k_int.openrequest.webservices.generated.LostActionDTO ws_lost = message.getLost();
        msg = new com.k_int.openrequest.integration.iface.LostMessageDTO(
          ws_lost.getNote()
        );
      }
      else if ( message.getStatusQuery() != null ) {
        log.debug("converting status query DTO");
      }
      else if ( message.getExpired() != null ) {
        log.debug("converting expired DTO");
        com.k_int.openrequest.webservices.generated.ExpiredActionDTO ws_renew = message.getExpired();
        msg = new com.k_int.openrequest.integration.iface.ExpiredDTO(
          ws_renew.getNote()
        );
      }
      else if ( message.getMessage() != null ) {
        log.debug("converting message DTO");
        com.k_int.openrequest.webservices.generated.MessageActionDTO ws_message = message.getMessage();
        msg = new com.k_int.openrequest.integration.iface.MessageDTO(ws_message.getMessage());
      }
      else {
        log.error("Error - No handler to convert from WS DTO to internal DTO");
        throw new com.k_int.openrequest.integration.iface.RequestManagerException("Unable to convert WS DTO to Internal DTO");
      }
    }
    else {
      log.error("No message");
      throw new com.k_int.openrequest.integration.iface.RequestManagerException("No message to convert");
    }

    return msg;
  }

}
