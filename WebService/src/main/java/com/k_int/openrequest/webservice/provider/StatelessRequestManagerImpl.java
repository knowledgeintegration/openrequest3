package com.k_int.openrequest.webservices.provider;

import org.apache.axis.MessageContext;
import org.apache.axis.transport.http.HTTPConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.context.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.k_int.openrequest.integration.iface.*;

public class StatelessRequestManagerImpl implements com.k_int.openrequest.webservices.generated.StatelessRequestManagerPort {

  public static Log log = LogFactory.getLog(StatelessRequestManagerImpl.class);

  public StatelessRequestManagerImpl() {
    log.debug("*** NEW StatelessRequestManagerImpl ***");
  }

  public com.k_int.openrequest.webservices.generated.BriefMessageInfoDTO[] getUnreadMessages(long location_id) throws java.rmi.RemoteException {

    com.k_int.openrequest.webservices.generated.BriefMessageInfoDTO[] result = null;

    // Getting hold of spring application context
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      result = StatelessRequestManagerHelper.getUnreadMessages(ctx,location_id);
    }
    finally {
    }

    return result;
  }

  public boolean setMessageReadStatus(long location_id, 
                                      long message_id) throws java.rmi.RemoteException {
    boolean result = false;
    log.debug("setMessageReadStatus "+location_id+","+message_id);
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      result = StatelessRequestManagerHelper.setMessageReadStatus(ctx,location_id,message_id);
    }
    finally {
    }
    return result;
  }

  public long createRequest(String requester_symbol,
                            com.k_int.openrequest.webservices.generated.RequesterViewDTO request_data, 
                            com.k_int.openrequest.webservices.generated.RotaElementDTO[] rota,
                            String mandatory_tgq) throws java.rmi.RemoteException {
    log.debug("createRequest from "+requester_symbol);
    long result = -1;
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      log.debug("Calling StatelessRequestManagerHelper.createRequest");
      result = StatelessRequestManagerHelper.createRequest(ctx,requester_symbol,request_data,rota,mandatory_tgq);
    }
    catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
      log.warn("Problem ",rme);
      throw new java.rmi.RemoteException("Problem creating request",rme);
    }
    catch ( DuplicateUserReferenceException dure ) {
      log.warn("Problem ",dure);
      throw new java.rmi.RemoteException("Problem creating request",dure);
    }
    finally {
    }
    return result;
  }

  public long createTransGroup(String requester_symbol,
                               com.k_int.openrequest.webservices.generated.RequesterViewDTO request_data, 
                               com.k_int.openrequest.webservices.generated.RotaElementDTO[] rota, 
                               java.lang.String mandatory_tgq) throws java.rmi.RemoteException {
    long result = -3;
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      result = StatelessRequestManagerHelper.createRequest(ctx,requester_symbol,request_data,rota,mandatory_tgq);
    }
    catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
      throw new java.rmi.RemoteException("Problem creating request",rme);
    }
    catch ( DuplicateUserReferenceException dure ) {
      throw new java.rmi.RemoteException("Problem creating request",dure);
    }
    finally {
    }
    return result;
  }

  public long startMessagingSync(long transaction_group_id) throws java.rmi.RemoteException {
    long result = -3;
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      result = StatelessRequestManagerHelper.startMessagingSync(ctx,transaction_group_id);
    }
    catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
      throw new java.rmi.RemoteException("Problem creating request",rme);
    }
    finally {
    }
    return result;
  }

  public java.lang.String getVersion() throws java.rmi.RemoteException {
    String result = null;
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      result = StatelessRequestManagerHelper.getVersion(ctx);
    }
    finally {
    }
    return result;
  }

  public void startMessaging(long transaction_group_id) throws java.rmi.RemoteException {
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      StatelessRequestManagerHelper.startMessaging(ctx,transaction_group_id);
    }
    catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
      throw new java.rmi.RemoteException("Problem creating request",rme);
    }
    finally {
    }
  }

  public com.k_int.openrequest.webservices.generated.BriefTransactionInfoTypeDTO[] transactionSearch(
                  com.k_int.openrequest.webservices.generated.SearchRequestDTO query) throws java.rmi.RemoteException {
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      return StatelessRequestManagerHelper.transactionSearch(ctx,query);
    }
    catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
      throw new java.rmi.RemoteException("Backend Problem",rme);
    }
    finally {
    }
  }

  public long createLocation(com.k_int.openrequest.webservices.generated.LocationInfoDTO location_info) throws java.rmi.RemoteException {

    log.debug("createLocation called "+location_info);

    long result = 0;
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      result = StatelessRequestManagerHelper.createLocation(ctx,location_info);
    }
    catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
      log.warn("Problem: "+rme);
      throw new java.rmi.RemoteException("Backend Problem",rme);
    }
    finally {
    }

    log.debug("createLocation returning "+result);
    return result;
  }

  public java.lang.String actionRequests(long sourceLocationId, 
                                         long[] transactionId, 
                                         com.k_int.openrequest.webservices.generated.ActionMessageDTO message) throws java.rmi.RemoteException {
    log.debug("actionRequests");

    String result = "OK";
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message axis_message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      result = StatelessRequestManagerHelper.actionRequests(ctx,sourceLocationId,transactionId,message);
    }
    catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
      log.warn("Problem: "+rme);
      throw new java.rmi.RemoteException("Backend Problem",rme);
    }
    finally {
    }

    return result;

  }

  public String[] getEngineStatus(long max_entries) {
    log.debug("getEngineStatus");

    String[] result = null;
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message axis_message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      if ( ctx.containsBean("EngineStatusService") ) {
        com.k_int.openrequest.util.EngineStatusService status_service = (com.k_int.openrequest.util.EngineStatusService) ctx.getBean("EngineStatusService");
        result = (String[]) ( status_service.getStatus(max_entries).toArray(new String[0]));
      }
    }
    finally {
    }

    return result;
  }

  public String getProperty(java.lang.String prop_name) {
    log.debug("getProperty("+prop_name+")");

    String result = null;
    try {
      MessageContext context = MessageContext.getCurrentContext();
      org.apache.axis.Message axis_message = context.getCurrentMessage();
      ServletContext sc = ((HttpServlet)context.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
      WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);
      if ( ctx.containsBean("EngineStatusService") ) {
        com.k_int.openrequest.util.EngineStatusService status_service = (com.k_int.openrequest.util.EngineStatusService) ctx.getBean("EngineStatusService");
        result = status_service.getProperty(prop_name);
      }
    }
    finally {
    }

    return result;
  }
}
