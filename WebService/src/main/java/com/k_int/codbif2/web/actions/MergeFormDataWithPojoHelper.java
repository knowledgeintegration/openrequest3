package com.k_int.codbif2.web.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.*;
import org.apache.struts.util.MessageResources;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.logging.*;
import org.apache.commons.logging.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import java.io.StringWriter;

/**
 * Merge the result of a HTML form submission with a POJO from the session.
 * This action is responsible for taking a HTML form and looking for prefedined
 * attribute names in the request parameters. Attribute names of the form
 * codbif2.pojo.<beanutils_access_path>.[_sel|_mms]
 * The submitted web form must contain a codbif2.pojo.id attribute that this action can
 * use to obtain a handle to the object and a codbif2.context.id that can be used to identify
 * an operation context in which that object can be found. a context equates to a
 * single use case execution, for example "Create Request" which should have a definite
 * start and end, and which the user causes to "Close", cleaning up any resources.
 *
 */
public final class MergeFormDataWithPojoHelper {

  /** The <code>Log</code> instance for this application.  */
  private static Log log = LogFactory.getLog(MergeFormDataWithPojoHelper.class);

  /**
   * Merge any pojo objects, return the name of any successful control matching the name "codbif2.action"
   */
  public static String setSimpleAndComboAttrs(HttpServletRequest request,
                                              Object pojo) throws java.lang.IllegalAccessException,
                                                                 java.lang.reflect.InvocationTargetException {
    log.debug("**Process edit form....");
    Map entity_params = new HashMap();
    String result = null;

    // For each parameter in the request.
    for (Enumeration e = request.getParameterNames(); e.hasMoreElements(); ) {
      String param_name = (String)e.nextElement();
      log.debug("consider: "+param_name+"="+request.getParameter(param_name));
      String [] values = request.getParameterValues(param_name);
      String param_value = values[0];

      if(param_name.startsWith("codbif2.pojo.")) {
  
        if ( ( param_value != null ) && (param_value.equals("")) ) {
          System.out.println("Param " + param_name + " null value");
          param_value = null;
        }
  
        try {
          if(param_name.endsWith("_sel")) {
            // It's a combo selection, deal with it. Drop combos must now come with a hidden form field that identifies
            // the field of the reference object containing a primary key. (Single key values only atm)
            String property_name = param_name.substring(13, param_name.length()-4);
            Object o = dereference(param_value);
            log.debug("Processing a selection... Trying resolve "+property_name+" form="+param_value+" resolved="+o);
            entity_params.put(property_name, o);
            // Expect sel control values to be of the forms:
            //    hib:SessionId:Class:PK
            //    string:value
            //    others as needed.

            // Object value = HelperFramework.resolveFormSelBoxToObject(param_value);
            // org.apache.commons.beanutils.PropertyUtils.setProperty(pojo, 
            //                                                        property_name, 
            //                                                        value);
          }
          else if(param_name.endsWith("_mms")) {
            // Many to Many Select / Checkbox List
            // String property_name = param_name.substring(13, property_name.length()-4);
            // Set s = (Set) org.apache.commons.beanutils.PropertyUtils.getProperty(pojo, attr_name);
            // s.clear();
            // for ( int i=0; i<values.length; i++ ) {
            //   s.add(HibernateObjectId.objectFromKey(values[i], hibernate_session));
            // }
          }
          else{
            String property_name = param_name.substring(13, param_name.length());
            entity_params.put(property_name, param_value);
          }
        }        
        //catch(java.lang.IllegalAccessException iae) {
        //  iae.printStackTrace();
        //}
        //catch(java.lang.reflect.InvocationTargetException ite) {
        //  ite.printStackTrace();
        //}         
        //catch(java.lang.NoSuchMethodException nsme) {
        //  nsme.printStackTrace();
        //}
        //catch(java.lang.ClassNotFoundException cnfe) {
        //  cnfe.printStackTrace();
        //}
        //catch(java.lang.InstantiationException ie) {
        //  ie.printStackTrace();
        //}
        finally {
        }
      }
      else if(param_name.startsWith("codbif2.action")) {
        result=param_name;
      }
    }
    // System.err.println("Attempting to process :"+entity_params);
    org.apache.commons.beanutils.BeanUtils.populate(pojo, entity_params);
    log.debug("All done");
    return result;
  }

  public static Object dereference(String ref) {
    Object result = null;

    if ( ref != null ) {
      String ref_components[] = new String[2];
      int colon_pos = ref.indexOf(':');
      ref_components[0] = ref.substring(0,colon_pos);
      ref_components[1] = ref.substring(colon_pos+1,ref.length());

       log.debug("dereference trying "+ref_components[0]+" "+ref_components[1]);

      if ( ref_components[0].equals("pre") ) {
        if ( ref_components[1].equals("null") ) {
        }
      }
      else if ( ref_components[0].equals("string") ) {
        result = ref_components[1];
      }
      else {
        result = ref;
      }
    }

    return result;
  }
}
