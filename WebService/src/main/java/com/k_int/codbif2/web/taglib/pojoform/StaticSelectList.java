package com.k_int.codbif2.web.taglib.pojoform;
import java.io.*;
import java.util.*;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import org.apache.commons.beanutils.*;

/**
 * @author ben.kitchener
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class StaticSelectList extends BodyTagSupport implements DataContainer {

  private String property = null;
  private String id = null;
  private Map options = null;
  
  /**
   * @return Returns the id.
   */
  public String getId() {
    return id;
  }

  /**
   * @param id The id to set.
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return Returns the property.
   */
  public String getProperty() {
    return property;
  }
  /**
   * @param property The property to set.
   */
  public void setProperty(String property) {
    this.property = property;
  }

  public int doEndTag() throws JspException {
    PojoContext pojo_context = (PojoContext)findAncestorWithClass(this,PojoContext.class);
    try {
      if ( pojo_context != null ) {
        DynaBean db = pojo_context.getDynaBean();

        if ( db != null ) {
          Object result = null;
          try {
            // result = PropertyUtils.getProperty(the_object, property);
            result = db.get(property);
          }
          catch ( java.lang.IllegalArgumentException iae ) {
            iae.printStackTrace();
            result = null;
          }

          String current_value = null;

          if(result!=null){
            current_value = result.toString();
          }
          else {
            current_value = "";
          }
          
          Set possible_values = options.keySet();

          if(possible_values!=null && possible_values.size()>0) {
            String selected = "";
            pageContext.getOut().write("<select name=\"codbif2.pojo.");
            pageContext.getOut().write(pojo_context.getBeanUtilPath());
            pageContext.getOut().write(property);
            pageContext.getOut().write("_sel\" size=\"1\"");
            if (id != null)
              pageContext.getOut().write("id=\"" + id + "\"");
            
            pageContext.getOut().write(">");
            Iterator i = possible_values.iterator();
            int count = 0;

            if ( current_value==null) {
              pageContext.getOut().write("<option value=\"pre:NULL\"></option>");
            }
            else {
              pageContext.getOut().write("<option value=\"pre:NULL\" selected=\"yes\"></option>");
            }

            while(i.hasNext()){
              String key = (String)i.next();
              String value = (String) options.get(key);
              pageContext.getOut().write("<option value=\"string:");
              pageContext.getOut().write(key);
              pageContext.getOut().write("\"");
                
              if((current_value != null ) && ( key.equalsIgnoreCase(current_value))){
                pageContext.getOut().write(" selected=\"true\"");
              }
              pageContext.getOut().write(">");
              pageContext.getOut().write(value);
              pageContext.getOut().write("</option>");
              count++;
            }
            pageContext.getOut().write("</select>");
          }
        }
        else {
          pageContext.getOut().write("Unable to get dynabean tag");
        }
      }
      else {
        pageContext.getOut().write("Unable to get parent tag");
      }
    } 
    catch(IOException e) {
      e.printStackTrace();
      throw new JspTagException("An IOException occurred.");
    }
    catch (Exception oe) {
      oe.printStackTrace();
      throw new JspException("Another exception occured.");
    }

    return EVAL_PAGE;
  }

  public int doStartTag() throws JspException {
    options = new LinkedHashMap();
    return EVAL_BODY_INCLUDE;
  }

  public void registerOption(String key, String value) {
    options.put(key,value);
  }
}
