package com.k_int.codbif2.web.taglib.pojoform;

import java.io.*;
import java.util.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import org.apache.commons.beanutils.*;

public class OQLDataSource extends TagSupport{

  private String oql = null;
  private String connection_factory = null;

  public String getOQL() {
    return oql;
  }

  public void setOQL(String oql) {
    this.oql = oql;
  }

  public String getConnectionFactory() {
    return connection_factory;
  }

  public void setConnectionFactory(String connection_factory) {
    this.connection_factory = connection_factory;
  }


  public int doStartTag() throws JspException {
    return EVAL_BODY_INCLUDE;
  }

  public int doEndTag() throws JspException {
    try {
      DataContainer parent = (DataContainer)findAncestorWithClass(this,DataContainer.class);
      if ( parent != null ) {
        System.err.println("OQL : "+oql+" from "+connection_factory);
      //   parent.registerOption(key,value);
      }
      else {
        System.err.println("Error OQL : "+oql+" from "+connection_factory);
        pageContext.getOut().write("Unable to get parent Data Container");
      }
    } 
    catch(IOException e) {
      e.printStackTrace();
      throw new JspTagException("An IOException occurred.");
    }
    catch (Exception oe) {
      oe.printStackTrace();
      throw new JspException("Another exception occured.");
    }

    return EVAL_PAGE;
  }

}
