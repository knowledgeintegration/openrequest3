package com.k_int.codbif2.web.actions;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.*;
import org.apache.struts.util.MessageResources;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.logging.*;
import org.apache.commons.logging.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import java.io.StringWriter;

/**
 * Merge the result of a HTML form submission with a POJO from the session.
 * This action is responsible for taking a HTML form and looking for prefedined
 * attribute names in the request parameters. Attribute names of the form
 * codbif2.pojo.<beanutils_access_path>.[_sel|_mms]
 * The submitted web form must contain a codbif2.pojo.id attribute that this action can
 * use to obtain a handle to the object and a codbif2.context.id that can be used to identify
 * an operation context in which that object can be found. a context equates to a
 * single use case execution, for example "Create Request" which should have a definite
 * start and end, and which the user causes to "Close", cleaning up any resources.
 *
 */
public final class MergeFormDataWithPojoAction extends Action {

  /** The <code>Log</code> instance for this application.  */
  private static Log log = LogFactory.getLog(MergeFormDataWithPojoAction.class);


  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response) throws Exception {

    ActionErrors errors = new ActionErrors();
    HttpSession session = request.getSession();

    String contxt_identifier = request.getParameter("codbif2.context.id");
    String pojo_identifier = request.getParameter("codbif2.pojo.id");

    Map local_context = com.k_int.codbif2.web.util.Codbif2Helpers.getContext(contxt_identifier, session);

    if ( local_context == null ) {
      return mapping.findForward("failure");
    }

    Object pojo = local_context.get(pojo_identifier);

    if ( pojo == null ) {
      return mapping.findForward("failure");
    }

    String codbif_action = MergeFormDataWithPojoHelper.setSimpleAndComboAttrs(request, pojo);

    System.err.println("result of codbif form handling: "+codbif_action);

    ActionForward actionForward = null;
    if ( codbif_action != null ) {
      String[] components = codbif_action.split(":");
      if ( ( components.length == 3 ) && ( components[1].equals("forward") ) ) {
        actionForward = mapping.findForward(components[2]);
        log.debug("Forwarding to "+components[2]);
      }
    }

    if ( actionForward == null ) {
      actionForward = mapping.findForward("success");
    }

    // ActionForward new_action_forward = new ActionForward(actionForward);
    // new_action_forward.setPath(new_path);

    return actionForward;
  }
}
