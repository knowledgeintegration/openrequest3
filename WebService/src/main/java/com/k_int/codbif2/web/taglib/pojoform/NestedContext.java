package com.k_int.codbif2.web.taglib.pojoform;

import java.io.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.commons.beanutils.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.apache.commons.beanutils.*;
import java.util.*;


public class NestedContext extends PojoContext {

  private String property = null;
  private String auto_create = null;
  private PojoContext pojo_parent = null;

  public String getProperty() {
    return property;
  }

  public void setProperty(String property) {
    this.property = property;
  }

  public String getAutoCreate() {
    return auto_create;
  }

  public void setAutoCreate(String auto_create) {
    this.auto_create = auto_create;
  }

  public PojoContext getPojoParent() {
    return pojo_parent;
  }

  public int doStartTag() throws JspException {

    System.err.println("NestedContext::doStartTag()");

    pojo_parent = (PojoContext)findAncestorWithClass(this,PojoContext.class);

    System.err.println("Parent context = "+pojo_parent);

    try {
      if ( pojo_parent != null ) {
        DynaBean parent_db = pojo_parent.getDynaBean();

        if ( parent_db != null ) {
          Object result = null;
          try {
            result = parent_db.get(property);
          }
          catch ( java.lang.IllegalArgumentException iae ) {
            iae.printStackTrace();
            System.err.println("Problem getting property "+property+" on instance of "+pojo_parent.getObject().getClass());
            result = null;
          }

          if ( result == null ) {
            // The object was null.. test for auto_create or the list of options we will present to the user
            // for creating or linking in an existing object at this location.
            System.err.println("result of lookup was null, auto-create= "+auto_create);
            if ( ( auto_create != null ) && ( auto_create.length() > 0 ) ) {
              Class auto_create_class = Class.forName(auto_create);
              System.err.println("AutoCreate context instance of "+auto_create);
              result = auto_create_class.newInstance();
              parent_db.set(property, result);
              System.err.println("Result of get property after calling set : "+parent_db.get(property));
            }
          }
          else {
            System.err.println("Located object for nested context");
          }
 
          if ( result != null ) {
            System.err.println("Creating nested context dynabean");
            the_dynabean = new ConvertingWrapDynaBean(result);
            the_object = result;
          }
          else {
            // There was no bean
            return SKIP_BODY;
          }
        }
        else {
          pageContext.getOut().write("Unable to get dynabean for object being edited");
        }
      }
      else {
        pageContext.getOut().write("Unable to get parent tag");
      }
    }
    catch(IOException e) {
      throw new JspTagException("An IOException occurred attempting to process property "+property+" from an instance "+pojo_parent.getObject()+".");
    }
    catch (Exception oe) {
      throw new JspTagException("An IOException occurred attempting to process property "+property+" from an instance "+pojo_parent.getObject()+".");
    }

    System.err.println("Return eval body include for nested pojo context");
    return EVAL_BODY_INCLUDE;
  }

  public int doEndTag() throws JspException {
    return EVAL_PAGE;
  }

  public String getBeanUtilPath() {
    if ( pojo_parent != null )
      return pojo_parent.getBeanUtilPath()+property+".";
    return property+".";
  }
}
