package com.k_int.codbif2.web.taglib.pojoform;

import java.io.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import org.apache.commons.beanutils.*;
import java.util.Iterator;

public class List extends PojoContext {

  private String classname = null;
  private String property = null;
  private String id = null;
  private Iterator iterator = null;
  private int index=0;
  private PojoContext pojo_parent = null;
  
  public void setProperty(String s) {
    property = s;
  }
  
  public String getProperty() {
    return property;
  }

  public void setClassname(String s) {
    classname = s;
  }

  public String getClassname() {
    return classname;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int doStartTag() throws JspException {
    java.util.List result = null;
    pojo_parent = (PojoContext)findAncestorWithClass(this,PojoContext.class);
    try {
      if ( pojo_parent != null ) {
        DynaBean db = pojo_parent.getDynaBean();

        if ( db != null ) {
          try {
            result = (java.util.List) db.get(property);
            if ( result != null ) {
              iterator = result.iterator();
            }
          }
          catch ( java.lang.IllegalArgumentException iae ) {
            result = null;
          }
        }
        else {
          pageContext.getOut().write("Unable to get dynabean for object being edited");
        }
      }
      else {
        pageContext.getOut().write("Unable to get parent tag");
      }
    } 
    catch(IOException e) {
      throw new JspTagException("An IOException occurred attempting to process property "+property+" from an instance "+pojo_parent.getObject()+".");
    }
    catch (Exception oe) {
      throw new JspTagException("An IOException occurred attempting to process property "+property+" from an instance "+pojo_parent.getObject()+".");
    }

    if ( iterator.hasNext() ) {
      Object new_child = iterator.next();
      the_dynabean = new ConvertingWrapDynaBean(new_child);
      the_object = new_child;
      return EVAL_BODY_TAG;
    }
    else {
      return SKIP_BODY;
    }
  }

  public int doAfterBody() throws JspTagException {
    BodyContent body = getBodyContent();
    try {
      body.writeOut(getPreviousOut());
    } catch (IOException e) {
      throw new JspTagException("IterationTag: " +
      e.getMessage());
    }

    // clear up so the next time the body content is empty
    body.clearBody();
    if (iterator.hasNext()) {
      Object new_child = iterator.next();
      the_dynabean = new ConvertingWrapDynaBean(new_child);
      the_object = new_child;
      index++;
      return EVAL_BODY_TAG;
    } else {
      return SKIP_BODY;
    }
  }

  public int doEndTag() throws JspException {
    return EVAL_PAGE;
  }

  public String getBeanUtilPath() {
    if ( pojo_parent != null )
      return pojo_parent.getBeanUtilPath()+property+"["+index+"].";
    return property+"["+index+"].";
  }

}
