package com.k_int.codbif2.web.taglib.pojoform;

import java.io.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;

public class Submit extends TagSupport {

  private String label = "Submit";
  private String name = null;

  public int doStartTag() throws JspException {

    try {
      pageContext.getOut().write("<input type=\"submit\" name=\""+name+"\" value=\""+label+"\">");
    } 
    catch(IOException e) {
      throw new JspTagException("An IOException occurred.");
    }

    return SKIP_BODY;
  }

  public int doEndTag() throws JspException {
    return EVAL_PAGE;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
