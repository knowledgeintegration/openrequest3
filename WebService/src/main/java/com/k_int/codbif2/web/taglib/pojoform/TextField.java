package com.k_int.codbif2.web.taglib.pojoform;

import java.io.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import org.apache.commons.beanutils.*;

public class TextField extends TagSupport {

  private String classname = null;
  private String property = null;
  private String size = null;
  private String id = null;
  private boolean editable = false;
  
  public void setProperty(String s) {
    property = s;
  }
  
  public String getProperty() {
    return property;
  }

  public void setSize(String s) {
    size = s;
  }
  
  public String getSize() {
    return size;
  }

  public void setClassname(String s) {
    classname = s;
  }

  public String getClassname() {
    return classname;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public boolean getEditable() {
    return editable;
  }
                                                                                                                                        
  public void setEditable(boolean editable) {
    this.editable = editable;
  }

      
  public int doStartTag() throws JspException {
    PojoContext pojo_context = (PojoContext)findAncestorWithClass(this,PojoContext.class);
    try {
      if ( pojo_context != null ) {
        DynaBean db = pojo_context.getDynaBean();

        if ( db != null ) {
          Object result = null;
          try {
            // result = PropertyUtils.getProperty(the_object, property);
            result = db.get(property);
          }
          catch ( java.lang.IllegalArgumentException iae ) {
            result = null;
          }

          System.out.println("Property " + property);
          String value = "";
          
          if (result != null) {
            value = result.toString();
            
            // if its a list must get rid of []
            if (result instanceof java.util.List) {
              // remove leading and trailing []
              if (value.startsWith("["))
                value = value.substring(1);
              if (value.endsWith("]"))
                value = value.substring(0, value.lastIndexOf("]"));
            }
          }

          if ( editable ) {
            pageContext.getOut().write("<input type=\"text\" name=\"codbif2.pojo.");
            pageContext.getOut().write(pojo_context.getBeanUtilPath());
            pageContext.getOut().write(property);
            pageContext.getOut().write("\" value=\"");

            pageContext.getOut().write(value);
            pageContext.getOut().write("\"");
            if (size!= null)
              pageContext.getOut().write("size=\"" + size + "\"");
          
            if (id != null)
              pageContext.getOut().write("id=\"" + id + "\"");
            
            if(classname!=null) {
              pageContext.getOut().write(" class=\"" + classname + "\" ");
            }
            
            pageContext.getOut().write("/>");
          }
          else {
            pageContext.getOut().write(value);
          }
        }
        else {
          pageContext.getOut().write("Unable to get dynabean for object being edited");
        }
      }
      else {
        pageContext.getOut().write("Unable to get parent tag");
      }
    } 
    catch(IOException e) {
      throw new JspTagException("An IOException occurred attempting to process property "+property+" from an instance "+pojo_context.getObject()+".");
    }
    catch (Exception oe) {
      throw new JspTagException("An IOException occurred attempting to process property "+property+" from an instance "+pojo_context.getObject()+".");
    }

    return SKIP_BODY;
  }

  public int doEndTag() throws JspException {
    return EVAL_PAGE;
  }
}
