package com.k_int.codbif2.web.taglib.pojoform;

import java.io.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.commons.beanutils.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.apache.commons.beanutils.*;
import java.util.*;

public abstract class PojoContext extends BodyTagSupport {

  protected DynaBean the_dynabean = null;
  protected Object the_object = null;

  public DynaBean getDynaBean() {
    return the_dynabean;
  }

  public Object getObject() {
    return the_object;
  }
  
  public abstract String getBeanUtilPath();
}
