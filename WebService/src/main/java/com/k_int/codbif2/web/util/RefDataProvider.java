package com.k_int.codbif2.web.util;

import java.util.List;

public interface RefDataProvider {
  public List getRefDataFor(String prop_set_name, String filter, int limit, Object provider_context);
  public Object deReference(String prop_set_name, String oid, Object provider_context);
}
