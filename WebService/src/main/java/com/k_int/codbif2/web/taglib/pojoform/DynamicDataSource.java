package com.k_int.codbif2.web.taglib.pojoform;

import java.io.*;
import java.util.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import org.apache.commons.beanutils.*;

public class DynamicDataSource extends TagSupport{

  private String type = null;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  public int doStartTag() throws JspException {
    return EVAL_BODY_INCLUDE;
  }

  public int doEndTag() throws JspException {
    try {
      DataContainer parent = (StaticSelectList)findAncestorWithClass(this,DataContainer.class);
      if ( parent != null ) {
      //   parent.registerOption(key,value);
      }
      else {
        pageContext.getOut().write("Unable to get parent Data Container");
      }
    } 
    catch(IOException e) {
      e.printStackTrace();
      throw new JspTagException("An IOException occurred.");
    }
    catch (Exception oe) {
      oe.printStackTrace();
      throw new JspException("Another exception occured.");
    }

    return EVAL_PAGE;
  }

}
