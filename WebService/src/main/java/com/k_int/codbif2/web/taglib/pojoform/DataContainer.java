package com.k_int.codbif2.web.taglib.pojoform;
import java.io.*;
import java.util.*;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import org.apache.commons.beanutils.*;

/**
 * A data container holds a list of values for a form, the data source can be static or dynamic but the container
 * has the job of storing the data presented by the source and using it in the UI. Examples of a container might
 * be a select control or a list control
 */
public interface DataContainer {
  public void registerOption(String key, String value);
}
