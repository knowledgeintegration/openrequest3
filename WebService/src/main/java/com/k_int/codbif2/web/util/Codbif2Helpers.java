package com.k_int.codbif2.web.util;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.HashMap;

public class Codbif2Helpers {
 
  public static Map getGlobalContext(HttpSession session) {
    Map result = (Map) session.getAttribute("com.k_int.codbif2.global_context");

    if ( result == null ) {
      result = new HashMap();
      session.setAttribute("com.k_int.codbif2.global_context",result);
    }

    return result;
  }

  public static Map getContext(String context_id,
                               HttpSession session) {
    Map global_context = getGlobalContext(session);
    return (Map) global_context.get(context_id);
  }

  public static Map createContext(String context_id,
                                      HttpSession session) {
    Map global_context = getGlobalContext(session);
    Map result = new HashMap();
    global_context.put(context_id, result);
    return result;
  }

  public static void removeContext(String context_id,
                                   HttpSession session) {
    Map global_context = getGlobalContext(session);
    global_context.remove(context_id);
  }

  public static Map lookupOrCreateContext(HttpSession session,String context_id) {
    Map result = getContext(context_id,session);
    if ( result == null ) {
      result = createContext(context_id, session);
    }
    
    return result;
  }

}
