package com.k_int.codbif2.web.util;

import java.util.List;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.*;

/**
 * Read a series of string name/value pairs from a property set and present the set as reference data
 */
public class PropertySetRefDataProvider implements RefDataProvider, ApplicationContextAware {

  public static Log log = LogFactory.getLog(PropertySetRefDataProvider.class);
  private ApplicationContext ctx = null;

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }

  public PropertySetRefDataProvider() {
  }

  public List getRefDataFor(String prop_set_name, 
                            String filter, 
                            int limit, 
                            Object provider_context) {
    return new ArrayList();
  }

  public Object deReference(String prop_set_name, 
                            String oid, 
                            Object provider_context) {
    return null;
  }
}
