package com.k_int.codbif2.web.taglib.pojoform;

import java.io.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.commons.beanutils.*;
import org.hibernate.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.apache.commons.beanutils.*;
import java.util.*;

public class Form extends PojoContext {

  private String dynabean_name = null;
  private String action = null;
  private String id = null;
  private String onSubmit = null;
  private String base_dir;

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getOnSubmit() {
    return onSubmit;
  }

  public void setOnSubmit(String s) {
    onSubmit = s;
  }
  
  public int doStartTag() throws JspException {

    WebApplicationContext ctx =
      WebApplicationContextUtils.getRequiredWebApplicationContext(
        pageContext.getServletContext());

    String context_id = (String) pageContext.getRequest().getAttribute("codbif2.context.id");
    String pojo_id = (String) pageContext.getRequest().getAttribute("codbif2.pojo.id");

    Map the_context = com.k_int.codbif2.web.util.Codbif2Helpers.getContext(context_id, pageContext.getSession());
    the_object = the_context.get(pojo_id);

    the_dynabean = new ConvertingWrapDynaBean(the_object);

    try {
      pageContext.getOut().write("<!--This is a PojoForm-->");
      pageContext.getOut().write("<form method=\"post\"");
      writeAttrIfPresent("action",action);
      writeAttrIfPresent("onSubmit",onSubmit);
      pageContext.getOut().write(">");
    } 
    catch(Exception e) {
      throw new JspTagException("An IOException occurred.");
    }

    return EVAL_BODY_INCLUDE;
  }

  public int doEndTag() throws JspException {

    try {
      pageContext.getOut().write("</form>");
    } 
    catch(Exception e) {
      throw new JspTagException("An IOException occurred.");
    }
    return EVAL_PAGE;
  }

  private void writeHiddenIfPresent(String hidden_field_name, Object value) throws java.io.IOException {
    if ( ( value != null ) &&
         ( value.toString().length() > 0 ) ) {
      pageContext.getOut().write("<input type=\"hidden\" name=\""+hidden_field_name+"\" value=\""+value+"\">");
    }
  }

  public void writeAttrIfPresent(String name, String value) throws java.io.IOException {
    if ( ( value != null ) &&
         ( value.toString().length() > 0 ) ) {
      pageContext.getOut().write(" "+name+"=\""+value+"\" ");
    }
  }

  protected String getBaseDir() {
      return base_dir;     
  }

  public String getBeanUtilPath() {
    return "";
  }
}
