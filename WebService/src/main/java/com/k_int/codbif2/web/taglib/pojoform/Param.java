package com.k_int.codbif2.web.taglib.pojoform;

import java.io.*;
import java.util.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import org.apache.commons.beanutils.*;

public class Param extends TagSupport{

  public String name = null;
  public String value = null;

  public Param() {
    super();
    System.out.println("New Param out");
    System.err.println("New Param err");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public int doStartTag() throws JspException {
    return SKIP_BODY;
  }

  public int doEndTag() throws JspException {
    System.out.println("doEndTag for param "+name+" to "+value);
    try {
      Object parent = findAncestorWithClass(this,TagSupport.class);

      if ( parent != null ) {
        // Set property name of parent to value using property utils
        System.out.println("Set param "+name+" to "+value+" parent is "+parent.getClass().getName());
      }
      else {
        pageContext.getOut().write("Unable to get parent to set parameter");
      }
    } 
    catch(IOException e) {
      e.printStackTrace();
      throw new JspTagException("An IOException occurred.");
    }
    catch (Exception oe) {
      oe.printStackTrace();
      throw new JspException("Another exception occured.");
    }

    return EVAL_PAGE;
  }

}
