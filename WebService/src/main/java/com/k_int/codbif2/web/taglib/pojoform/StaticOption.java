package com.k_int.codbif2.web.taglib.pojoform;

import java.io.*;
import java.util.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import org.apache.commons.beanutils.*;

public class StaticOption extends TagSupport{

  public String key = null;
  public String value = null;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public int doStartTag() throws JspException {
    return SKIP_BODY;
  }

  public int doEndTag() throws JspException {
    try {
      DataContainer parent = (StaticSelectList)findAncestorWithClass(this,DataContainer.class);
      if ( parent != null ) {
        parent.registerOption(key,value);
      }
      else {
        pageContext.getOut().write("Unable to get parent tag");
      }
    } 
    catch(IOException e) {
      e.printStackTrace();
      throw new JspTagException("An IOException occurred.");
    }
    catch (Exception oe) {
      oe.printStackTrace();
      throw new JspException("Another exception occured.");
    }

    return EVAL_PAGE;
  }

}
