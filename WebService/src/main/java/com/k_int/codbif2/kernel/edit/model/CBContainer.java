package com.k_int.codbif2.kernel.edit.model;

import java.util.ArrayList;
import java.util.List;

public abstract class CBContainer extends CBComponent {

  private List<CBContainerComponentInfo> components = new java.util.ArrayList();
  private String pojo_context;
  private CBLayout cb_layout_manager;

  public CBContainer() {
  }

  public CBContainer(String pojo_context) {
    this.pojo_context = pojo_context;
  }

  public CBContainer(CBLayout cb_layout_manager) {
    this.cb_layout_manager = cb_layout_manager;
  }

  public CBContainer(String pojo_context, CBLayout cb_layout_manager) {
    this.pojo_context = pojo_context;
    this.cb_layout_manager = cb_layout_manager;
  }


  public List<CBContainerComponentInfo> getComponents() {
    return components;
  }

  public void setComponents(List<CBContainerComponentInfo> components) {
    this.components = components;
  }

  public String getPojoContext() {
    return pojo_context;
  }

  public void setPojoContext(String pojo_context) {
    this.pojo_context = pojo_context;
  }

  public CBContainer add(CBComponent component) {
    components.add(new CBContainerComponentInfo(component,null));
    return this;
  }

  public CBContainer add(CBComponent component, Object layout_constraints) {
    components.add( new CBContainerComponentInfo(component, layout_constraints));
    return this;
  }
}
