package com.k_int.codbif2.kernel.edit.model;

import java.util.ArrayList;
import java.util.List;

public class CBContainerComponentInfo {
  public CBComponent component;
  public Object constraints;

  public CBContainerComponentInfo(CBComponent component, Object constraints) {
    this.component = component;
    this.constraints = constraints;
  }

}
