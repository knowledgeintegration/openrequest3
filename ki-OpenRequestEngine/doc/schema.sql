drop table state_model
drop table Brokered_Request_Details
drop table status_code
drop table rota
drop table Generic_Artifact_Request
drop table ISOILL_Request_Details
drop table state_transition
drop table iso10161_request_message_details
drop table iso10161_message
drop sequence hibernate_sequence
create table state_model (id INT8 not null, name VARCHAR(255), primary key (id))
create table Brokered_Request_Details (brokered_request_id INT8 not null, message VARCHAR(255), primary key (brokered_request_id))
create table status_code (description VARCHAR(255), id INT8 not null, model INT8, primary key (id))
create table rota (message VARCHAR(255), parent_id INT4 not null, responder_dn VARCHAR(255) not null, primary key (parent_id, responder_dn))
create table Generic_Artifact_Request (message VARCHAR(255), request_id INT8 not null, request_type VARCHAR(255) not null, primary key (request_id))
create table ISOILL_Request_Details (iso_request_id INT8 not null, message VARCHAR(255), primary key (iso_request_id))
create table state_transition (description VARCHAR(255), from_state INT8, id INT8 not null, to_state INT8, primary key (id))
create table iso10161_request_message_details (id INT8 not null, transaction_type INT8, primary key (id))
create table iso10161_message (initial_requester_id_symbol VARCHAR(255), initial_requester_id_symbol_type INT8, initial_requester_name VARCHAR(255), initial_requester_name_type INT8, message_id INT8 not null, message_type VARCHAR(255) not null, requester_id_name VARCHAR(255), requester_id_name_type INT8, requester_id_symbol VARCHAR(255), requester_id_symbol_type INT8, responder_id_name VARCHAR(255), responder_id_name_type INT8, responder_id_symbol VARCHAR(255), responder_id_symbol_type INT8, stq VARCHAR(255), str_service_date_time VARCHAR(255), tgq VARCHAR(255), tq VARCHAR(255), primary key (message_id))
alter table state_transition add constraint state_transitionFK1 foreign key (to_state) references status_code
alter table state_transition add constraint state_transitionFK0 foreign key (from_state) references status_code
alter table status_code add constraint status_codeFK0 foreign key (model) references state_model
create sequence hibernate_sequence

