/**
 * Title:       
 * @version:    $Id: Setup.java,v 1.2 2005/05/14 22:39:06 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.setup;

import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import java.util.*;
import cirrus.hibernate.*;

/* TODO: Check that protocol adapters dont use the services list hanging off loc.. but instead uses service attached to symbol */

public class Setup {
  public static String CLASS_KEY = "Class";

  public static void registerEventHandler(LocationType lt,
                                            String precondition,
                                            String message_type,
                                            String message_params,
                                            String handler_type,
                                            String handler_params,
                                            Session sess) throws java.sql.SQLException,
                                                          cirrus.hibernate.HibernateException
  {
    Map m = lt.getLocationEventHandlers();
    m.put(precondition, new EventHandler(message_type,
                                         message_params,
                                         handler_type,
                                         handler_params));
  }

  public static Object createLocationSymbol(String symbol,
                                     NamingAuthority authority,
                                     Location location,
                                     Service service_for_this_symbol,
                                     Session sess) throws java.sql.SQLException,
                                                          cirrus.hibernate.HibernateException
  {
    LocationSymbol sym = new LocationSymbol(symbol, authority, location, service_for_this_symbol);
    return sess.save(sym);
  }

  public static StateModel createStateModel(String description, 
                                            String code,
                                            Session sess) throws java.sql.SQLException,
                                                          cirrus.hibernate.HibernateException
  {
    StateModel sm = new StateModel();
    sm.setName(description);
    sm.setCode(code);
    sess.save(sm);
    return sm;
  }

  public static StatusCode createStatusCode(String description,
                                            String code,
                                            Session sess) throws java.sql.SQLException,
                                                          cirrus.hibernate.HibernateException
  {
    StatusCode sc = new StatusCode();
    sc.setDescription(description);
    sc.setCode(code);
    sess.save(sc);
    return sc;
  }

  public static StateTransition createST(StateModel model,
                                         String transition_code,
                                         StatusCode from,
                                         StatusCode to,
                                         String predicate,
                                         String description,
                                         Session sess) throws java.sql.SQLException,
                                                          cirrus.hibernate.HibernateException
  {
    StateTransition st = new StateTransition();
    st.setModel(model);
    st.setTransitionCode(transition_code);
    st.setDescription(description);
    st.setPredicate(predicate);
    st.setFrom_state(from);
    st.setTo_state(to);
    sess.save(st);
    return st;
  }


  public static void main(String[] args)
  {
    try
    {
      System.err.println("Configure");
      Hibernate.configure();
      Context jndi_context = new InitialContext();
      SessionFactory sf = (SessionFactory) jndi_context.lookup("/jndi/OpenRequestSessionFactory");
      Session sess = sf.openSession();

      System.err.println("\n\nSetting up database\n\n");

      // Set up the ISO ILL State models:
      StateModel sm_req_proc = createStateModel("Requester processing phase", "REQ/PRO", sess);
      StateModel sm_res_proc = createStateModel("Responder processing phase", "RES/PRO",sess);
      StateModel sm_req_track = createStateModel("Requester tracking phase", "REQ/TRACK", sess);
      StateModel sm_res_track = createStateModel("Responder tracking phase", "RES/TRACK", sess);
      StateModel sm_int = createStateModel("State Model for Intermediary", "INT",sess);
      StateModel sm_tg = createStateModel("Transaction Group State Model", "TG",sess);
     
      StatusCode sc_idle = createStatusCode("Idle","IDLE",sess);
      StatusCode sc_pending = createStatusCode("Pending","PENDING",sess);
      StatusCode sc_not_supplied = createStatusCode("Not Supplied", "NOTSUPPLIED",sess);
      StatusCode sc_conditional = createStatusCode("Conditional","CONDITIIONAL",sess);
      StatusCode sc_cancel_pending = createStatusCode("Cancel Pending","CANCELPENDING",sess);
      StatusCode sc_cancelled = createStatusCode("Cancelled","CANCELLED",sess);
      StatusCode sc_shipped = createStatusCode("Shipped","SHIPPED",sess);
      StatusCode sc_received = createStatusCode("Received","RECEIVED",sess);
      StatusCode sc_renew_pending = createStatusCode("Renew Pending","RENEWPENDING",sess);
      StatusCode sc_renew_overdue = createStatusCode("Renew Overdue","RENEWOVERDUE",sess);
      StatusCode sc_overdue = createStatusCode("Overdue","OVERDUE",sess);
      StatusCode sc_not_rcvd_overdue = createStatusCode("Not Received/Overdue","NOTREC/OVERDUE",sess);
      StatusCode sc_recall = createStatusCode("Recall","RECALL",sess);
      StatusCode sc_returned = createStatusCode("Returned","RETURNED",sess);
      StatusCode sc_lost = createStatusCode("Lost","LOST",sess);
      StatusCode sc_in_process = createStatusCode("In Process","IN-PROCESS",sess);
      StatusCode sc_forward = createStatusCode("Forward","FORWARD",sess);
      StatusCode sc_checked_in = createStatusCode("Checked In","CHECKEDIN",sess);

      StatusCode sc_end_of_rota = createStatusCode("End of rota","ENDROTA",sess);
      StatusCode sc_satisfied = createStatusCode("Satisfied","SATISFIED",sess);
      StatusCode sc_tracking = createStatusCode("Tracking","TRACKING",sess);
      StatusCode sc_closed = createStatusCode("Closed","CLOSED",sess);


      // State model for Requester processing phase

      createST(sm_req_proc, "ILLreq", sc_idle, sc_pending, "", "", sess);
      createST(sm_req_proc, "ILLreq", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "C-REPreq+", sc_conditional, sc_pending, "", "", sess);
      createST(sm_req_proc, "C-REPreq+", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "C-REPreq-", sc_conditional, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "C-REPreq-", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "CANreq", sc_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "CANreq", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "RCVreq", sc_pending, sc_received, "", "set RETURN var", sess);
      createST(sm_req_proc, "RCVreq", sc_cancel_pending, sc_received, "", "set RETURN var", sess);
      createST(sm_req_proc, "RCVreq", sc_shipped, sc_received, "", "set RETURN var", sess);
      createST(sm_req_proc, "LSTreq", sc_pending, sc_lost, "", "", sess);
      createST(sm_req_proc, "LSTreq", sc_cancel_pending, sc_lost, "", "", sess);
      createST(sm_req_proc, "LSTreq", sc_shipped, sc_lost, "", "", sess);
      createST(sm_req_proc, "MSGreq", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "MSGreq", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "MSGreq", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_req_proc, "MSGreq", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "MSGreq", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "MSGreq", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "STQreq", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "STQreq", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "STQreq", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_req_proc, "STQreq", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "STQreq", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "STQreq", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "STRreq", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "STRreq", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "STRreq", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_req_proc, "STRreq", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "STRreq", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "STRreq", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "FWD", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "FWD", sc_cancel_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "ANS-CO", sc_pending, sc_conditional, "", "", sess); // p7, ANSind-CO
      createST(sm_req_proc, "ANS-CO", sc_not_supplied, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-CO", sc_conditional, sc_conditional, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-CO", sc_cancel_pending, sc_cancel_pending, "", "", sess); // p7, ANSind-CO
      createST(sm_req_proc, "ANS-CO", sc_cancelled, sc_cancelled, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-RY", sc_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-RY", sc_not_supplied, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-RY", sc_cancel_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-UN", sc_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-UN", sc_not_supplied, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-UN", sc_cancel_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-LP", sc_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-LP", sc_not_supplied, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-LP", sc_cancel_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-WS", sc_pending, sc_pending, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-WS", sc_not_supplied, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-WS", sc_conditional, sc_conditional, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-WS", sc_cancel_pending, sc_cancel_pending, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-WS", sc_shipped, sc_shipped, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-HP", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "ANS-HP", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "ANS-HP", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_req_proc, "ANS-HP", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "ANS-HP", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "ANS-ES", sc_pending, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "ANS-ES", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "ANS-ES", sc_cancel_pending, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "CAR+", sc_cancel_pending, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "CAR+", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "CAR-", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "CAR-", sc_cancel_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "CAR-", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "SHI", sc_pending, sc_shipped, "", "", sess);
      createST(sm_req_proc, "SHI", sc_cancel_pending, sc_shipped, "", "", sess);
      createST(sm_req_proc, "SHI", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "MSG", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "MSG", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "MSG", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_req_proc, "MSG", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "MSG", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "MSG", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "STQ", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "STQ", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "STQ", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_req_proc, "STQ", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "STQ", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "STQ", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "STR", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "STR", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "STR", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_req_proc, "STR", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "STR", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "STR", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "EXP", sc_pending, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "EXP", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "EXP", sc_conditional, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "EXP", sc_cancel_pending, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "LST", sc_pending, sc_lost, "", "", sess);
      createST(sm_req_proc, "LST", sc_cancel_pending, sc_lost, "", "", sess);
      createST(sm_req_proc, "LST", sc_shipped, sc_lost, "", "", sess);

      // State model for Requester processing phase
      createST(sm_req_track, "RCVreq", sc_pending, sc_shipped, "", "", sess);
      createST(sm_req_track, "RCVreq", sc_cancel_pending, sc_shipped, "", "", sess);
      createST(sm_req_track, "RCVreq", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "RCVreq", sc_shipped, sc_shipped, "", "", sess);

      createST(sm_req_track, "RET", sc_received, sc_returned, "p5", "", sess);
      createST(sm_req_track, "RET", sc_renew_pending, sc_returned, "", "", sess);
      createST(sm_req_track, "RET", sc_renew_overdue, sc_returned, "", "", sess);

      createST(sm_req_track, "RENreq", sc_received, sc_renew_pending, "p5", "", sess);
      createST(sm_req_track, "RENreq", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "RENreq", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "RENreq", sc_overdue, sc_renew_overdue, "", "", sess);

      createST(sm_req_track, "LSTreq", sc_pending, sc_lost, "", "", sess);
      createST(sm_req_track, "LSTreq", sc_cancel_pending, sc_lost, "", "", sess);
      createST(sm_req_track, "LSTreq", sc_received, sc_lost, "p5", "", sess);
      createST(sm_req_track, "LSTreq", sc_renew_pending, sc_lost, "", "", sess);
      createST(sm_req_track, "LSTreq", sc_renew_overdue, sc_lost, "", "", sess);
      createST(sm_req_track, "LSTreq", sc_shipped, sc_lost, "", "", sess);
      createST(sm_req_track, "LSTreq", sc_not_rcvd_overdue, sc_lost, "", "", sess);
      createST(sm_req_track, "LSTreq", sc_overdue, sc_lost, "", "", sess);
      createST(sm_req_track, "LSTreq", sc_returned, sc_lost, "", "", sess);
      createST(sm_req_track, "LSTreq", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "LSTreq", sc_recall, sc_lost, "", "", sess);

      createST(sm_req_track, "DAMreq", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "DAMreq", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "DAMreq", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "DAMreq", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "DAMreq", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "DAMreq", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "MSGreq", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "MSGreq", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "MSGreq", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "MSGreq", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "MSGreq", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "MSGreq", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "MSGreq", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "MSGreq", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "MSGreq", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "MSGreq", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "MSGreq", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "STQreq", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "STQreq", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "STQreq", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "STQreq", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "STQreq", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "STQreq", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "STQreq", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "STQreq", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "STQreq", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "STQreq", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "STQreq", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "STRreq", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "STRreq", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "STRreq", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "STRreq", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "STRreq", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "STRreq", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "STRreq", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "STRreq", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "STRreq", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "STRreq", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "STRreq", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "ANS-WS", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "ANS-HP", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "CAR-", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "CAR-", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "CAR-", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "CAR-", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "CAR-", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "CAR-", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "CAR-", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "CAR-", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "CAR-", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "CAR-", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "CAR-", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "SHI", sc_pending, sc_shipped, "", "", sess);
      createST(sm_req_track, "SHI", sc_cancel_pending, sc_shipped, "", "", sess);
      createST(sm_req_track, "SHI", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "SHI", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "SHI", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "SHI", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "SHI", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "SHI", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "SHI", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "SHI", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "SHI", sc_recall, sc_recall, "", "", sess);
    
      createST(sm_req_track, "RCL", sc_pending, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_cancel_pending, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_received, sc_recall, "p5", "", sess);
      createST(sm_req_track, "RCL", sc_renew_pending, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_renew_overdue, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_shipped, sc_recall, "p5", "", sess);
      createST(sm_req_track, "RCL", sc_not_rcvd_overdue, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_overdue, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "RCL", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "RCL", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "DUE", sc_pending, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "DUE", sc_cancel_pending, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "DUE", sc_received, sc_overdue, "p5", "", sess);
      createST(sm_req_track, "DUE", sc_renew_pending, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "DUE", sc_renew_overdue, sc_overdue, "p7 and p8", "", sess);
      createST(sm_req_track, "DUE", sc_shipped, sc_not_rcvd_overdue, "p5", "", sess);
      createST(sm_req_track, "DUE", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "DUE", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "DUE", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "DUE", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "LST", sc_pending, sc_lost, "", "", sess);
      createST(sm_req_track, "LST", sc_cancel_pending, sc_lost, "", "", sess);

      createST(sm_req_track, "MSG", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "MSG", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "MSG", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "MSG", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "MSG", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "MSG", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "MSG", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "MSG", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "MSG", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "MSG", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "MSG", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "STQ", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "STQ", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "STQ", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "STQ", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "STQ", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "STQ", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "STQ", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "STQ", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "STQ", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "STQ", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "STQ", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "STR", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "STR", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "STR", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "STR", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "STR", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "STR", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "STR", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "STR", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "STR", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "STR", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "STR", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "REA+", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "REA+", sc_renew_pending, sc_received, "", "", sess);
      createST(sm_req_track, "REA+", sc_renew_overdue, sc_received, "", "", sess);
      createST(sm_req_track, "REA+", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "REA+", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "REA+", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "REA-", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "REA-", sc_renew_pending, sc_received, "", "", sess);
      createST(sm_req_track, "REA-", sc_renew_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "REA-", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "REA-", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "REA-", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "REA-", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "CHK", sc_pending, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_cancel_pending, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_received, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_renew_pending, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_renew_overdue, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_shipped, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_not_rcvd_overdue, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_overdue, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_recall, sc_returned, "", "", sess);

      createST(sm_req_track, "RCVreq", sc_not_rcvd_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "RCVreq", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "RETreq", sc_overdue, sc_returned, "", "", sess);
      createST(sm_req_track, "RETreq", sc_returned, sc_returned, "", "", sess);

      createST(sm_req_track, "DAM", sc_returned, sc_returned, "", "", sess);

      // Responder processing phase
      createST(sm_res_proc, "ILL", sc_idle, sc_in_process, "", "", sess);
      createST(sm_res_proc, "ILL", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "ILL", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "ILL", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_res_proc, "ILL", sc_forward, sc_forward, "", "", sess);

      createST(sm_res_proc, "FWDreq", sc_in_process, sc_forward, "", "", sess);
      createST(sm_res_proc, "FWDreq", sc_forward, sc_forward, "", "", sess);

      createST(sm_res_proc, "ANSreq-CO", sc_in_process, sc_conditional, "", "", sess);
      createST(sm_res_proc, "ANSreq-CO", sc_conditional, sc_conditional, "", "", sess);

      createST(sm_res_proc, "ANSreq-RY", sc_in_process, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "ANSreq-RY", sc_not_supplied, sc_not_supplied, "", "", sess);

      createST(sm_res_proc, "ANSreq-UN", sc_in_process, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "ANSreq-UN", sc_not_supplied, sc_not_supplied, "", "", sess);

      createST(sm_res_proc, "ANSreq-PL", sc_in_process, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "ANSreq-PL", sc_not_supplied, sc_not_supplied, "", "", sess);

      createST(sm_res_proc, "ANSreq-WS", sc_in_process, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "ANSreq-WS", sc_not_supplied, sc_not_supplied, "", "", sess);

      createST(sm_res_proc, "ANSreq-HP", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "ANSreq-HP", sc_not_supplied, sc_in_process, "", "", sess);

      createST(sm_res_proc, "ANSreq-ES", sc_in_process, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "ANSreq-ES", sc_not_supplied, sc_not_supplied, "", "", sess);

      createST(sm_res_proc, "CARreq+", sc_cancel_pending, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "CARreq+", sc_cancelled, sc_cancelled, "", "", sess);

      createST(sm_res_proc, "CARreq-", sc_cancel_pending, sc_in_process, "", "", sess);
      createST(sm_res_proc, "CARreq-", sc_in_process, sc_in_process, "", "", sess);

      createST(sm_res_proc, "SHIreq", sc_in_process, sc_shipped, "", "", sess);

      createST(sm_res_proc, "MSGreq", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "MSGreq", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "MSGreq", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_res_proc, "MSGreq", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "MSGreq", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "MSGreq", sc_forward, sc_forward, "", "", sess);

      createST(sm_res_proc, "STQreq", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "STQreq", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "STQreq", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_res_proc, "STQreq", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "STQreq", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "STQreq", sc_forward, sc_forward, "", "", sess);

      createST(sm_res_proc, "STRreq", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "STRreq", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "STRreq", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_res_proc, "STRreq", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "STRreq", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "STRreq", sc_forward, sc_forward, "", "", sess);

      createST(sm_res_proc, "C-REP+", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "C-REP+", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "C-REP+", sc_conditional, sc_in_process, "", "reset expiry timer", sess);

      createST(sm_res_proc, "C-REP-", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "C-REP-", sc_conditional, sc_not_supplied, "", "", sess);

      createST(sm_res_proc, "CAN", sc_in_process, sc_cancel_pending, "p7", "", sess);
      createST(sm_res_proc, "CAN", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "CAN", sc_conditional, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "CAN", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "CAN", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "CAN", sc_forward, sc_cancelled, "", "", sess);

      createST(sm_res_proc, "MSG", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "MSG", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "MSG", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_res_proc, "MSG", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "MSG", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "MSG", sc_forward, sc_forward, "", "", sess);

      createST(sm_res_proc, "STQ", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "STQ", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "STQ", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_res_proc, "STQ", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "STQ", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "STQ", sc_forward, sc_forward, "", "", sess);

      createST(sm_res_proc, "STR", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "STR", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "STR", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_res_proc, "STR", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "STR", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "STR", sc_forward, sc_forward, "", "", sess);

      // Responder tracking phase
      createST(sm_res_track, "SHIreq", sc_shipped, sc_shipped, "", "", sess);

      createST(sm_res_track, "CHKreq", sc_shipped, sc_checked_in, "", "", sess);
      createST(sm_res_track, "CHKreq", sc_renew_pending, sc_checked_in, "", "", sess);
      createST(sm_res_track, "CHKreq", sc_renew_overdue, sc_checked_in, "", "", sess);
      createST(sm_res_track, "CHKreq", sc_overdue, sc_checked_in, "", "", sess);
      createST(sm_res_track, "CHKreq", sc_recall, sc_checked_in, "", "", sess);
      createST(sm_res_track, "CHKreq", sc_checked_in, sc_checked_in, "", "", sess);

      createST(sm_res_track, "RCLreq", sc_shipped, sc_recall, "p5", "", sess);
      createST(sm_res_track, "RCLreq", sc_renew_pending, sc_recall, "", "", sess);
      createST(sm_res_track, "RCLreq", sc_renew_overdue, sc_recall, "", "", sess);
      createST(sm_res_track, "RCLreq", sc_overdue, sc_recall, "", "", sess);
      createST(sm_res_track, "RCLreq", sc_recall, sc_recall, "", "", sess);

      createST(sm_res_track, "DUEreq", sc_shipped, sc_overdue, "p5", "", sess);
      createST(sm_res_track, "DUEreq", sc_renew_pending, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "DUEreq", sc_overdue, sc_overdue, "p8", "", sess);
      createST(sm_res_track, "DUEreq", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "DUEreq", sc_overdue, sc_overdue, "", "", sess);

      createST(sm_res_track, "LSTreq", sc_shipped, sc_lost, "", "", sess);
      createST(sm_res_track, "LSTreq", sc_renew_pending, sc_lost, "", "", sess);
      createST(sm_res_track, "LSTreq", sc_renew_overdue, sc_lost, "", "", sess);
      createST(sm_res_track, "LSTreq", sc_overdue, sc_lost, "", "", sess);
      createST(sm_res_track, "LSTreq", sc_recall, sc_lost, "", "", sess);
      createST(sm_res_track, "LSTreq", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "DAMreq", sc_checked_in, sc_checked_in, "", "", sess);

      createST(sm_res_track, "MSGreq", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "MSGreq", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "MSGreq", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "MSGreq", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "MSGreq", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "MSGreq", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "MSGreq", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "STQreq", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "STQreq", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "STQreq", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "STQreq", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "STQreq", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "STQreq", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "STQreq", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "STRreq", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "STRreq", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "STRreq", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "STRreq", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "STRreq", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "STRreq", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "STRreq", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "REQreq+", sc_renew_pending, sc_shipped, "", "", sess);
      createST(sm_res_track, "REQreq+", sc_renew_overdue, sc_shipped, "", "", sess);
      createST(sm_res_track, "REQreq+", sc_shipped, sc_shipped, "", "", sess);

      createST(sm_res_track, "REQreq-", sc_renew_pending, sc_shipped, "", "", sess);
      createST(sm_res_track, "REQreq-", sc_renew_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "REQreq-", sc_shipped, sc_shipped, "", "", sess);

      createST(sm_res_track, "ILL", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "ILL", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "ILL", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "ILL", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "ILL", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "CAN", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "CAN", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "CAN", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "CAN", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "CAN", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "CAN", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "CAN", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "RCV", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "RCV", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "RCV", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "RCV", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "RCV", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "RCV", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "RCV", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "RET", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "RET", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "RET", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "RET", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "RET", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "RET", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "RET", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "REN", sc_shipped, sc_shipped, "p7", "", sess);
      createST(sm_res_track, "REN", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "REN", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "REN", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "REN", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "REN", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "REN", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "LST", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "LST", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "LST", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "LST", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "LST", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "LST", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "LST", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "DAM", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "DAM", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "DAM", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "DAM", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "DAM", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "DAM", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "DAM", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "MSG", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "MSG", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "MSG", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "MSG", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "MSG", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "MSG", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "MSG", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "STQ", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "STQ", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "STQ", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "STQ", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "STQ", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "STQ", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "STQ", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "STR", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "STR", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "STR", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "STR", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "STR", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "STR", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "STR", sc_lost, sc_lost, "", "", sess);


      //
      // Transaction group state model
      //
      createST(sm_tg, "TG_START",    sc_idle,             sc_in_process, "", "", sess);

      createST(sm_tg, "TG_NEXT",     sc_in_process,       sc_in_process, "", "", sess);
      createST(sm_tg, "TG_EOR",      sc_in_process,       sc_end_of_rota, "", "", sess);
      createST(sm_tg, "TG_SHIP_NR",  sc_in_process,       sc_satisfied, "", "", sess); // Shipped non-ret
      createST(sm_tg, "TG_SHIP_RET", sc_in_process,       sc_tracking, "", "", sess); // Shipped returnable
      createST(sm_tg, "TG_CAN_REQ",  sc_in_process,       sc_cancel_pending, "", "", sess);

      createST(sm_tg, "TG_RESTART",  sc_end_of_rota,      sc_in_process, "", "", sess);
      createST(sm_tg, "TG_CLOSE",    sc_end_of_rota,      sc_closed, "", "", sess);

      createST(sm_tg, "TG_SHIP_NR",  sc_cancel_pending,   sc_tracking, "", "", sess);
      createST(sm_tg, "TG_SHIP_RET", sc_cancel_pending,   sc_satisfied, "", "", sess);
      createST(sm_tg, "TG_CAN_OK",   sc_cancel_pending,   sc_cancelled, "", "", sess);

      createST(sm_tg, "TG_CLOSE",    sc_satisfied,        sc_closed, "", "", sess);
      createST(sm_tg, "TG_CLOSE",    sc_tracking,         sc_closed, "", "", sess);
      createST(sm_tg, "TG_CLOSE",    sc_cancelled,        sc_closed, "", "", sess);

      // ILL FWD ANS-CO ANS-RY ANS-UN ANS-LP ANS-WS ANS-HP ANS-ES C-REP+ C-REP- CAN CAR+
      // CAR-  SHI RCV RCL DUE RET REN REA+ REA- CHK LST DAM MSG STQ STR EXP

      LocationType system_location_type = new LocationType("SYSTEM");  // The System
      LocationType normal_location_type = new LocationType("NORM");    // Standard location
      LocationType intermediary_location_type = new LocationType("INT");     // Intermediary

      // Register some handlers

      // Request that an ILL be sent
      registerEventHandler(system_location_type,
                           "ILLreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ILLreq",
                           sess);

      registerEventHandler(system_location_type,
                           "FWDreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-CO",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-RY",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-UN",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-LP",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-WS",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-HP",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-ES",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "C-REPreq+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "C-REPreq-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "CANreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "CARreq+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "CARreq-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "SHIreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_SHIreq",
                           sess);

      registerEventHandler(system_location_type,
                           "RCVreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "RCLreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "DUEreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "RETreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "RENreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "REAreq+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "REAreq-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "CHKreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "LSTreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "DAMreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "MSGreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "STQreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      registerEventHandler(system_location_type,
                           "STRreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDUreq",
                           sess);

      // Handle an incoming ILL message
      registerEventHandler(system_location_type,
                           "ILL",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ILL",
                           sess);

      registerEventHandler(system_location_type,
                           "FWD",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-CO",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-RY",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-UN",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-LP",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-WS",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-HP",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-ES",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "C-REP+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "C-REP-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "CAN",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "CAR+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "CAR-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "SHI",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "RCV",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "RCL",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "DUE",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "RET",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "REN",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "REA+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "REA-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "CHK",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "LST",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "DAM",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "MSG",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "STQ",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "STR",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "EXP",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_PDU",
                           sess);

      registerEventHandler(system_location_type,
                           "EXPIRYtimeout",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_EXPIRYtimeout",
                           sess);

      // Got this far re-working state tables.

      registerEventHandler(system_location_type,
                           "TG_NEXT",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.workflow.HandleNextRotaResponderRequest",
                           sess);

      registerEventHandler(system_location_type,
                           "internal:IncomingPDU",
                           CLASS_KEY, "com.k_int.openrequest.isoill.ILLMessageEnvelope",
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleIncomingProtocolMessage",
                           sess);


      /*
       * Call the correct handler for a specific event
       */
      registerEventHandler(system_location_type,
                           "internal:IncomingValidPDU",
                           CLASS_KEY,
                           "com.k_int.openrequest.helpers.InternalIso10161Message",
                           CLASS_KEY,
                           "com.k_int.openrequest.actions.iso10161.IncomingPDUDispatcher",
                           sess);


      sess.save(system_location_type);
      sess.save(normal_location_type);
      sess.save(intermediary_location_type);

      // Create some location information
      // from maintenance agency
      NamingAuthority a_g_authority = new NamingAuthority("a_g", "Auto-Graphics, Inc");
      sess.save(a_g_authority);

      NamingAuthority ait_authority = new NamingAuthority("ait", "Aurora Information Technology");
      sess.save(ait_authority);

      NamingAuthority als_authority = new NamingAuthority("als", "Ameritech Library Services");
      sess.save(als_authority);

      NamingAuthority bl_authority = new NamingAuthority("bl", "British Library");
      sess.save(bl_authority);

      NamingAuthority btp_authority = new NamingAuthority("btp", "Big 12 Plus Libraries Consortium");
      sess.save(btp_authority);

      NamingAuthority calis_authority = new NamingAuthority("calis", "China Academic Library and Information");
      sess.save(calis_authority);

      NamingAuthority carl_authority = new NamingAuthority("carl", "CARL Corporation");
      sess.save(carl_authority);

      NamingAuthority cisti_authority = new NamingAuthority("cisti", 
                         "Canada Institute for Scientific and Technical Information (CISTI)");
      sess.save(cisti_authority);

      NamingAuthority dra_authority = new NamingAuthority("dra", "Data Research Associates, Inc");
      sess.save(dra_authority);

      NamingAuthority elias_authority = new NamingAuthority("elias", "ELiAS nv");
      sess.save(elias_authority);

      NamingAuthority exl_authority = new NamingAuthority("exl", "Ex Libris (USA)");
      sess.save(exl_authority);

      NamingAuthority ilib_authority = new NamingAuthority("ilib", "CMC Co. Ltd");
      sess.save(ilib_authority);

      NamingAuthority irc_authority = new NamingAuthority("irc", 
                                                          "Information Resource Center of Da Nang University (Viet Nam)");
      sess.save(irc_authority);

      NamingAuthority lvep_authority = new NamingAuthority("lveb", "Lac Viet Corp.");
      sess.save(lvep_authority);

      NamingAuthority mnlink_authority = new NamingAuthority("mnlink", "Minnesota Library Information Network (MnLINK)");
      sess.save(mnlink_authority);

      NamingAuthority mnscu_authority = new NamingAuthority("MnSCU/PALS", 
                   "Minnesota State Colleges and Universities Project for Automated Library Systems Authority:");
      sess.save(mnscu_authority);

      NamingAuthority nacsis_authority = new NamingAuthority("nacsis", 
                                                             "National Center for Science Information Systems (Japan");
      sess.save(nacsis_authority);

      NamingAuthority nla_authority = new NamingAuthority("nla", "National Library of Australia");
      sess.save(nla_authority);

      NamingAuthority nlc_authority = new NamingAuthority("nlc-bnc", "National Library of Canada");
      sess.save(nlc_authority);

      NamingAuthority nlm_authority = new NamingAuthority("nlm", "National Library of Medicine");
      sess.save(nlm_authority);

      NamingAuthority nlnz_authority = new NamingAuthority("nlnz", "National Library of New Zealand");
      sess.save(nlnz_authority);

      NamingAuthority oclc_authority = new NamingAuthority("oclc", "OCLC");
      sess.save(oclc_authority);

      NamingAuthority psi_authority = new NamingAuthority("psi", "Pigasus Software, Inc.");
      sess.save(psi_authority);

      NamingAuthority rct_authority = new NamingAuthority("rct", "The RCT Digital Library");
      sess.save(rct_authority);

      NamingAuthority relais_authority = new NamingAuthority("relais", "Relais International Inc.");
      sess.save(relais_authority);

      NamingAuthority rlg_authority = new NamingAuthority("rlg", "Research Libraries Group");
      sess.save(rlg_authority);

      NamingAuthority sirsi_authority = new NamingAuthority("sirsi", "SIRSI Corporation");
      sess.save(sirsi_authority);

      NamingAuthority tlc_authority = new NamingAuthority("tlc", "The Library Corporation");
      sess.save(tlc_authority);

      NamingAuthority usnuc_authority = new NamingAuthority("usnuc", "Library of Congress");
      sess.save(usnuc_authority);

      NamingAuthority vlts_authority = new NamingAuthority("vlts", "VLTS Inc");
      sess.save(vlts_authority);

      // made up stuff
      NamingAuthority dn_authority = new NamingAuthority("dn", "X.500/LDAP DN");
      sess.save(dn_authority);

      NamingAuthority k_int_authority = new NamingAuthority("KI", "Knowledge Integration Ltd");
      sess.save(k_int_authority);

      NamingAuthority talis_authority = new NamingAuthority("talis", "Talis Information Ltd");
      sess.save(talis_authority);

      Service or_connection_ill_service = new Service("Duplex ILL Connection at OpenRequest",
                                                      "ILL/DUPLEX",
		   				      "DUPLEXTCP",
                                                      0);
      or_connection_ill_service.setTelecomServiceIdentifier("TCP");
      or_connection_ill_service.setTelecomServiceAddress("ill.openrequest.info:1499");
      sess.save(or_connection_ill_service);

      Service connection_ill_service = new Service("Duplex ILL Connection at K-int",
                                                   "ILL/DUPLEX",
						   "DUPLEXTCP",
                                                   0);
      connection_ill_service.setTelecomServiceIdentifier("TCP");
      connection_ill_service.setTelecomServiceAddress("ill.k-int.com:1499");
      sess.save(connection_ill_service);

      Service simplex_ill_service = new Service("Simplex ILL Connection at K-int",
                                                   "ILL/SIMPLEX",
						   "DUPLEXTCP",
                                                   0);
      simplex_ill_service.setTelecomServiceIdentifier("TCP");
      simplex_ill_service.setTelecomServiceAddress("ill.k-int.com:1499");
      // simplex_ill_service.getProperties().put("HOST","localhost");
      // simplex_ill_service.getProperties().put("PORT","1611");
      // Service type= FAX,FTP,SMTP,TCP,URL,VOICE
      // simplex_ill_service.getProperties().put("TelecoServiceType","TCP");
      // simplex_ill_service.getProperties().put("TelecoServiceAddress","localhost:1611");
      sess.save(simplex_ill_service);

      Service mime_ill_service = new Service("Mime Based ILL at K-int",
                                             "ILL/MIME",
					     "MIME",
                                             0);
      mime_ill_service.setTelecomServiceIdentifier("SMTP");
      mime_ill_service.setTelecomServiceAddress("ill@k-int.com");
      //mime_ill_service.getProperties().put("ADDRESS","ill@k-int.com");
      // Service type= FAX,FTP,SMTP,TCP,URL,VOICE
      //mime_ill_service.getProperties().put("TelecoServiceType","SMTP");
      //mime_ill_service.getProperties().put("TelecoServiceAddress","ill@k-int.com");
      sess.save(mime_ill_service);

      Service tlc_connection = new Service("Simplex Test server at TLC",
                                           "ILL/SIMPLEX",
                                           "DUPLEXTCP",
                                           0);
      tlc_connection.setTelecomServiceIdentifier("TCP");
      tlc_connection.setTelecomServiceAddress("Library.Request.test.tlcdelivers.com:1611");

      // tlc_connection.getProperties().put("HOST","Library.Request.test.tlcdelivers.com");
      // tlc_connection.getProperties().put("PORT","1611");
      // tlc_connection.getProperties().put("TelecoServiceType","TCP");
      // tlc_connection.getProperties().put("TelecoServiceAddress","Library.Request.test.tlcdelivers.com:1611");
      sess.save(tlc_connection);

      Service tlc_mime = new Service("Mime Test server at TLC", "ILL/MIME", "MIME",0);
      // tlc_mime.setTelecomServiceIdentifier("SMTP");
      // tlc_mime.setTelecomServiceAddress("Library.Request.test@tlcdelivers.com");
      // tlc_mime.getProperties().put("ADDRESS","Library.Request.test@tlcdelivers.com");
      // tlc_mime.getProperties().put("TelecoServiceType","SMTP");
      // tlc_mime.getProperties().put("TelecoServiceAddress","Library.Request.test@tlcdelivers.com");
      sess.save(tlc_mime);

      Service loopback = new Service("Loopback", "LOOPBACK", "LOOPBACK",0);
      sess.save(loopback);

      Location k_int = new Location("K-Int, Sheffield Sicence and Tech. Parks", normal_location_type, "k-int:TCPTEST" );
      // k_int.getServices().add(connection_ill_service);
      // k_int.getServices().add(mime_ill_service);
      k_int.getProperties().put("RequiresAPDUDeliveryInfo","f");
      // k_int.getProperties().put("TelecoServiceType","TCP");
      // k_int.getProperties().put("TelecoServiceAddress","demo.k-int.com:1611");
      sess.save(k_int);

      Location test1 = new Location("TEST1", normal_location_type, "KI:TEST1" );
      // test1.getServices().add(loopback);
      // test1.getServices().add(connection_ill_service);
      // test1.getServices().add(mime_ill_service);
      test1.getProperties().put("RequiresAPDUDeliveryInfo","f");
      // test1.getProperties().put("TelecoServiceType","TCP");
      // test1.getProperties().put("TelecoServiceAddress","demo.k-int.com:1611");
      sess.save(test1);

      Location test2 = new Location("TEST2", normal_location_type, "KI:TEST2" );
      // test2.getServices().add(loopback);
      // test2.getServices().add(connection_ill_service);
      // test2.getServices().add(mime_ill_service);
      test2.getProperties().put("RequiresAPDUDeliveryInfo","f");
      // test2.getProperties().put("TelecoServiceType","TCP");
      // test2.getProperties().put("TelecoServiceAddress","demo.k-int.com:1611");
      sess.save(test2);

      Location tlc_simplex_test = new Location("TLC Test Simplex server", normal_location_type, "tlc:TCPTESTING" );
      tlc_simplex_test.getServices().add(tlc_connection);
      tlc_simplex_test.getProperties().put("RequiresAPDUDeliveryInfo","t");
      sess.save(tlc_simplex_test);
      
      Location tlc_mime_test = new Location("TLC Test Mime server", normal_location_type, "tlc:ILLTESTING" );
      tlc_mime_test.getServices().add(tlc_mime);
      tlc_mime_test.getProperties().put("RequiresAPDUDeliveryInfo","t");
      sess.save(tlc_mime_test);

      // TLC Test servers
      createLocationSymbol("TCPTESTING",tlc_authority,tlc_simplex_test,tlc_connection,sess);
      createLocationSymbol("ILLTESTING",tlc_authority,tlc_mime_test,tlc_mime,sess);

      // k-int Test servers
      createLocationSymbol("TEST1",k_int_authority,test1,connection_ill_service, sess);
      createLocationSymbol("TEST2",k_int_authority,test2,or_connection_ill_service, sess);

      sess.flush();
      sess.connection().commit();
      sess.close();
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }
    finally
    {
      System.err.println("Done");
    }
  }
}
