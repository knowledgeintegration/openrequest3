package com.k_int.openrequest.integration.iface;

public enum TransactionType {

  SIMPLE, CHAINED, PARTITIONED;

  public String toString() {
    switch ( this ) {
      case SIMPLE: return "Simple";
      case CHAINED: return "Chained";
      case PARTITIONED: return "Partitioned";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case SIMPLE: return 1;
      case CHAINED: return 2;
      case PARTITIONED: return 3;
      default: return 0;
    }
  }

  public static TransactionType fromIso(int iso){
    switch ( iso ) {
      case 1: return SIMPLE;
      case 2: return CHAINED;
      case 3: return PARTITIONED;
      default: return null;
    }
  }
}
