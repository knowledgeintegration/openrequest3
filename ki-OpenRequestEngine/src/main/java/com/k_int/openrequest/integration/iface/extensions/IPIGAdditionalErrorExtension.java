package com.k_int.openrequest.integration.iface.extensions;

import com.k_int.openrequest.integration.iface.*;

/**
 * Title:       IPIGAdditionalErrorExtension
 * @version:    $Id: IPIGAdditionalErrorExtension.java,v 1.1 2005/03/02 17:07:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class IPIGAdditionalErrorExtension extends ProtocolMessageExtensionDTO {

  public IPIGAdditionalErrorExtension() {
  }

}
