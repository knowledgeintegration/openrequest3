package com.k_int.openrequest.integration.provider.openrequest;

import java.math.BigInteger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.*;
import java.io.StringWriter;
import org.hibernate.*;
import org.hibernate.type.*;
import com.k_int.openrequest.api.MessageDeliveryObserver;
import com.k_int.openrequest.actions.workflow.NextResponderRequest;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.db.folders.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import com.k_int.QueryDescriptor.*;
import com.k_int.openrequest.integration.iface.*;
import com.k_int.openrequest.integration.provider.base.*;
import com.k_int.oql.iface.*;
import com.k_int.oql.util.*;
import org.springframework.context.ApplicationContext;



/*
 * $Log: RequestManagerImpl.java,v $
 * Revision 1.46  2005/07/07 17:57:20  ibbo
 * UPdated to add timeout to start sync messaging, and preparatory work for new
 * APDU Delivery Info methods
 *
 * Revision 1.45  2005/07/06 08:34:01  ibbo
 * Added option to not store incoming ADI information
 *
 * Revision 1.44  2005/07/04 14:38:14  ibbo
 * Updated
 *
 * Revision 1.43  2005/07/02 16:41:35  ibbo
 * Updated - fixed typo in constants, improved partner info tracking in HandleILL_ILL
 *
 * Revision 1.42  2005/07/02 07:24:43  ibbo
 * Cleanup:
 * Not setting date time of original service in outgoing request messages
 * Added Source to Required item details and use that instead of verfi.ref.src
 * when processing OCLC Request Extensions.
 * Don't set client id unless one of it's component fields are set.
 *
 * Revision 1.41  2005/07/01 08:59:11  ibbo
 * Fixed issues with duplicate definitions for OCLC_ILL_REQUEST_EXTENSION, and
 * it's sibsequent creation and storage
 *
 * Revision 1.40  2005/06/30 10:52:30  ibbo
 * Updated
 *
 * Revision 1.39  2005/06/30 10:42:12  ibbo
 * Improved timeout handling
 *
 * Revision 1.38  2005/06/25 08:30:58  ibbo
 * UPdated
 *
 * Revision 1.37  2005/06/10 18:04:19  ibbo
 * Improvents to Satus Or Error Report, including Intermediary Control Extension
 *
 * Revision 1.36  2005/06/10 17:09:37  ibbo
 * Updated
 *
 * Revision 1.35  2005/06/10 16:26:06  ibbo
 * Updated SOER handling
 *
 * Revision 1.33  2005/06/10 15:36:48  ibbo
 * Updated
 *
 * Revision 1.32  2005/06/10 14:34:11  ibbo
 * Added SOER classes
 *
 * Revision 1.31  2005/06/10 13:09:42  ibbo
 * Modified import
 *
 * Revision 1.30  2005/06/09 13:43:25  ibbo
 * Configuration error in ILL_TRANSACTION_GROUP. Current Partnter Telecom System Address
 * had a length of 20. Oops.
 *
 * Revision 1.29  2005/05/14 09:46:25  ibbo
 * UPdated.. error trapping for OCLC
 *
 * Revision 1.28  2005/05/12 15:15:41  ibbo
 * Updated
 *
 * Revision 1.27  2005/05/03 10:38:42  ibbo
 * Updated
 *
 * Revision 1.26  2005/05/03 10:33:11  ibbo
 * Updated
 *
 * Revision 1.25  2005/04/28 08:53:17  ibbo
 * Updated
 *
 * Revision 1.24  2005/04/25 20:34:31  ibbo
 * Updated
 *
 * Revision 1.23  2005/04/21 10:08:38  ibbo
 * Fixed currency code bug
 *
 * Revision 1.22  2005/04/10 15:28:56  ibbo
 * Added TG Next sync method to interface
 *
 * Revision 1.21  2005/04/10 13:25:43  ibbo
 * Updated
 *
 * Revision 1.20  2005/04/09 16:45:14  ibbo
 * Started to add framework for message delivery notification
 *
 * Revision 1.19  2005/04/07 22:44:27  ibbo
 * Updated
 *
 * Revision 1.18  2005/04/07 22:24:14  ibbo
 * Fixed integration_test unit test
 *
 * Revision 1.17  2005/04/05 10:55:36  ibbo
 * Added fix for cost-info-type
 *
 * Revision 1.16  2005/03/31 18:58:31  ibbo
 * Updated
 *
 * Revision 1.15  2005/03/31 18:29:29  ibbo
 * Updated
 *
 * Revision 1.14  2005/03/29 23:03:37  ibbo
 * Updates to address OCLC Issues logged under mantis 57 - Lots of issues to do with
 * sending requests.
 *
 * Revision 1.13  2005/03/29 21:55:55  ibbo
 * updated
 *
 * Revision 1.12  2005/03/22 16:49:57  ibbo
 * Updated
 *
 * Revision 1.11  2005/03/15 22:29:41  ibbo
 * Updated
 *
 * Revision 1.10  2005/03/15 20:27:25  ibbo
 * Updated logging in some classes
 *
 * Revision 1.9  2005/03/15 15:45:27  ibbo
 * updated
 *
 * Revision 1.8  2005/03/11 09:37:59  ibbo
 * Updated
 *
 * Revision 1.7  2005/03/09 15:09:14  ibbo
 * Updated
 *
 * Revision 1.6  2005/03/06 14:46:40  ibbo
 * Updated to allow a null delivery address in transaction group, and to correctly
 * set make delivery service an abstract class, so users of the API can only instansiate
 * Electronic or Physical delivery info.
 *
 * Revision 1.5  2005/02/25 23:20:26  ibbo
 * Bugfixes for the annoying enum types in the intergration API.. some of the enums
 * are based on optional members, and those were causing problems in the serializable
 * enum code
 *
 * Revision 1.4  2005/02/24 19:16:49  ibbo
 * updated
 *
 * Revision 1.3  2005/02/22 16:17:14  ibbo
 * Added retrieve transaction method to request manager api
 *
 * Revision 1.2  2005/02/21 10:02:58  ibbo
 * Updated
 *
 * Revision 1.1.1.1  2005/02/14 11:46:29  ibbo
 * Import
 *
 * Revision 1.7  2004/08/24 16:46:29  ibbo
 * Requests now take a list of service types
 *
 * Revision 1.6  2004/08/24 08:46:38  ibbo
 * Added missing getters / setters in some DTO objects. Added new request use case to
 * Console app.
 *
 * Revision 1.5  2004/08/01 12:38:24  ibbo
 * Updated
 *
 * Revision 1.4  2004/07/30 08:40:35  ibbo
 * Updated
 *
 * Revision 1.3  2004/07/25 17:35:56  ibbo
 * Updated
 *
 * Revision 1.2  2004/07/23 15:07:27  ibbo
 * Updated
 *
 */

/**
 * Title:       RequestManagerImpl
 * @version:    $Id: RequestManagerImpl.java,v 1.46 2005/07/07 17:57:20 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:   
 *
 * TODO: Close session should release any un-released search results
 *
 */
public class RequestManagerImpl extends AbstractRequestManager implements RequestManager {

  private static Log log = LogFactory.getLog(RequestManagerImpl.class);
  private static final String VERSION = "$Revision: 1.46 $";
  private MessageDispatcher md = null;
  private String service_provider_symbol = null;
  private static int instance_counter = 0;
  private String host = "localhost";
  private Long internal_loc_id = null;
  private SessionFactory sf = null;
  private int qry_ctr = 0;
  private Hashtable open_queries = new Hashtable();
  private String rmi_server_url;
  private ApplicationContext app_context = null;

  private static final String Q1 = "select tg.id, tg.requesterReference, tg.role, tg.requiredItemDetails.title, tg.requiredItemDetails.author, tg.requiredItemDetails.publicationDate, tg.lastTransaction.responderSymbol, tg.lastMessageDate, tg.lastTransaction.requestStatusCode.code, tg.active, tg.transactionGroupStatusCode.code, tg.unreadMessageCount, tg.lastTransaction.protocol, tg.initiatingBranchCode, tg.requiredItemDetails.service_types.elements, tg.lastTransaction.id from fh in class com.k_int.openrequest.db.folders.FolderHeader, tg in fh.FolderItems.elements where fh.ownerId=? and fh.code=?";

  public RequestManagerImpl(String service_provider_symbol, ApplicationContext app_context) throws RequestManagerException {
    super();
    this.app_context = app_context;

    log.info("New request manager ("+VERSION+") session for location : "+service_provider_symbol);

    log.debug("new RequestManagerImpl, instance count="+(++instance_counter));
    this.service_provider_symbol = service_provider_symbol;

    Session session = null;
    try
    {
      this.sf = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      this.md = (MessageDispatcher) app_context.getBean("DISPATCHER");
      session = sf.openSession();
      LocationSymbol loc = DBHelper.lookupLocation(session,service_provider_symbol);
      if ( loc != null ) {
        internal_loc_id = loc.getCanonical_location().getId();
      }
      session.flush();
      session.connection().commit();
    }
    catch ( Exception e )
    {
      log.fatal("Problem",e);
      throw new RequestManagerException("Unable to resolve location symbol "+service_provider_symbol,e);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }

    log.debug("internal loc id = "+internal_loc_id);

    if ( internal_loc_id == null )
    {
      throw new RequestManagerException("Unable to identify your location:"+service_provider_symbol);
    }

  }

  public String getVersion() throws RequestManagerException
  {
    return VERSION;
  }


  public TransactionGroupIdDTO createTransGroup(RequesterViewDTO request, 
                                                RotaElementDTO[] rota) 
                           throws RequestManagerException, DuplicateUserReferenceException {
    Long result = StatelessRequestManagerImpl.createTransGroupHelper(sf,request,rota,null,service_provider_symbol);
    return new TransactionGroupIdDTO(result);
  }

  
  public TransactionGroupIdDTO createTransGroup(RequesterViewDTO request, 
                                                RotaElementDTO[] rota,
                                                String mandatory_tgq) 
                           throws RequestManagerException, DuplicateUserReferenceException {
    Long result = StatelessRequestManagerImpl.createTransGroupHelper(sf,request,rota,mandatory_tgq,service_provider_symbol);
    return new TransactionGroupIdDTO(result);
  }

  public void startMessaging(TransactionGroupIdDTO id) throws RequestManagerException {
    log.debug("startMessaging "+id);
    try {
      // Send a message to the system asking for the transaction group to move to the next
      // request in the rota
      md.dispatch("System","TG_NEXT",id.getId());
    }
    catch ( Exception e ) {
      throw new RequestManagerException("problem dispatching TG_NEXT in rota",e);
    }
  }

  public Long startMessagingSync(TransactionGroupIdDTO id) throws RequestManagerException {

    log.debug("startMessagingSync "+id);
    try {
      // Send a message to the system asking for the transaction group to move to the next
      // request in the rota
      MessageDeliveryObserver mdo = new MessageDeliveryObserver() {

        public Long new_trans_id = null;
        public int error = 0;

        public void notify(Long tg_id,
                           Long trans_id,
                           String message,
                           Object additional_data) {
          log.debug("got notification that tg "+tg_id+" has a new request "+trans_id);
          new_trans_id = trans_id;
          synchronized(this) {
            this.notifyAll();
          }
        }

        public void notifyError(int error_code) {
          error = error_code;
          synchronized(this) {
            this.notifyAll();
          }
        }


        public Long getTransId() { return new_trans_id; }
        public int getError() { return error; }
      };

      NextResponderRequest nrr = new NextResponderRequest(id.getId(), mdo);
      md.dispatch("System","TG_NEXT",nrr);

      // Wait for up to 60 seconds
      long timeout_time = System.currentTimeMillis()+60000;

      while ( ( mdo.getTransId() == null ) && 
              ( mdo.getError() == 0 ) &&
              ( System.currentTimeMillis() < timeout_time ) ) {
        log.debug("Waiting on message delivery observer for new transaction id");
        synchronized(mdo) {
          mdo.wait(timeout_time-System.currentTimeMillis());
        }
      }

      if ( mdo.getTransId() == null ) {
        throw new RequestManagerException("problem dispatching TG_NEXT in rota - TIMEOUT");
      }

      return mdo.getTransId();
    }
    catch ( Exception e ) {
      throw new RequestManagerException("problem dispatching TG_NEXT in rota",e);
    }
  }

  public TransGroupInfoDTO retrieveTransGroup(TransactionGroupIdDTO id) throws RequestManagerException {

    log.debug("retrieveTransGroup "+id);

    TransGroupInfoDTO retval = null;
    Session session = null;
    try {
      session = sf.openSession();
      ILLTransactionGroup tg = DBHelper.lookupGAR(session, id.getId());

      Long current_trans_id = null;

      if ( tg == null )
        throw new RequestManagerException("Unable to locate transaction id "+id);

      RequestView view = null;
      Role role = null;
      RotaElementDTO rota[] = retrieveRota(tg);

      // Some providers only uses a single service type, not a preference list as in iso ill
      List service_type = new ArrayList();
      List service_types = tg.getRequiredItemDetails().getService_types();
      if ( service_types.size() > 0 ) {
        for ( Iterator i = service_types.iterator(); i.hasNext(); ) {
          service_type.add(ServiceType.fromIso(((Integer)(i.next())).intValue()));
        }
      }

      SupplyMediumType[] required_mediums = extractRequiredMediums(tg.getRequiredItemDetails().getSupplyMediumInfoType());

      LevelOfService level_of_service = null;
      PlaceOnHoldType place_on_hold_type = null;
      ExpiryFlag expiry_flag = null;

      // ToDo.. this really needs to go in TG
      boolean forward_flag = false;
      if ( tg.getLastTransaction() != null ) {
        forward_flag = tg.getLastTransaction().getForwardFlag();
      }

      if ( tg.getRequiredItemDetails().getLevelOfService() != null )
        level_of_service = LevelOfService.fromIso(tg.getRequiredItemDetails().getLevelOfService().charAt(0));

      if ( tg.getRequiredItemDetails().getExpiryFlag() > 0 )
        expiry_flag = ExpiryFlag.fromIso((int)(tg.getRequiredItemDetails().getExpiryFlag()));

      if ( tg.getRequiredItemDetails().getPlaceOnHold() > 0 )
        place_on_hold_type = PlaceOnHoldType.fromIso((int)(tg.getRequiredItemDetails().getPlaceOnHold()));

      if ( tg.getRole().equals("REQ") ) {
        role=Role.REQUESTER;
        view = new RequesterViewDTO(TransactionType.SIMPLE, // TransactionType transaction_type,
                                   createPostalAddress(tg.getCurrentDeliveryAddress()),
                                   extractDeliveryService(tg.getRequiredItemDetails()),
                                   createPostalAddress(tg.getCurrentBillingAddress()),
                                   service_type, // ServiceType service_type,
                                   new RequesterOptionalMessagesDTO(), // RequesterOptionalMessagesDTO requester_optional_messages,
                                   new SearchTypeDTO(level_of_service,
                                                    tg.getRequiredItemDetails().getNeedBeforeDate(), 
                                                    expiry_flag,
                                                    tg.getRequiredItemDetails().getExpiryDate()),
                                   required_mediums,
                                   place_on_hold_type,
                                   extractItemId(tg.getRequiredItemDetails()),
                                   extractCostInfo(tg.getRequiredItemDetails()),
                                   forward_flag,
                                   tg.getRequesterNote(),
                                   tg.getRequesterReferenceAuthority(),
                                   tg.getRequesterReference(),
                                   tg.getRequiredItemDetails().getClientIdentifier(),
                                   tg.getRequiredItemDetails().getCopyrightCompliance(),
                                   new RequesterSpecificMessageDTO(tg.getInitiatingBranchCode(),
                                                           tg.getPatronName(),
                                                           tg.getPatronSymbol(),
                                                           tg.getPatronStatus(),
                                                           "Borrower notes",
                                                           "Private note",
                                                           "reply to email"));
      }
      else {
        role=Role.RESPONDER;

        view = new ResponderViewDTO(TransactionType.SIMPLE,
                                   createPostalAddress(tg.getCurrentDeliveryAddress()),
                                   extractDeliveryService(tg.getRequiredItemDetails()),
                                   createPostalAddress(tg.getCurrentBillingAddress()),
                                   service_type, // ServiceType service_type,
                                   null, // RequesterOptionalMessagesDTO requester_optional_messages,
                                   new SearchTypeDTO(level_of_service,
                                                    tg.getRequiredItemDetails().getNeedBeforeDate(), 
                                                    expiry_flag,
                                                    tg.getRequiredItemDetails().getExpiryDate()),
                                   required_mediums,
                                   place_on_hold_type,
                                   extractItemId(tg.getRequiredItemDetails()),
                                   extractCostInfo(tg.getRequiredItemDetails()),
                                   forward_flag,
                                   tg.getRequesterNote(),
                                   tg.getRequiredItemDetails().getCopyrightCompliance(),
                                   tg.getRequesterReferenceAuthority(),
                                   tg.getRequesterReference(),
                                   tg.getRequiredItemDetails().getClientIdentifier());
      }

      // view.setILLRequest(createBeILLRequest(com.k_int.openrequest.db.ISO10161RequestMessageDetails dets));
      // Get the initial request message for info
      StatusDTO tgdto_status = null;

      if ( tg.getLastTransaction() != null ) {
        view.setILLRequest(createBeILLRequest((com.k_int.openrequest.db.ISO10161RequestMessageDetails)(tg.getLastTransaction().getInitialRequestMessage())));
        tgdto_status=new StatusDTO(
                       State.fromIso(
                         com.k_int.openrequest.integration.iface.Constants.convertState(
                           tg.getLastTransaction().getRequestStatusCode().getCode())));
        current_trans_id = tg.getLastTransaction().getId();
      }
      else {
        log.warn("Transaction group has no last transaction set");
      }

      // TODO: Convert second param fromIso(), null=transaction

      retval = new TransGroupInfoDTO(id, 
                                     tgdto_status,
                                     view, 
                                     extractAllGroupTransactions(tg),
                                     rota, 
                                     role,
                                     current_trans_id);

      session.flush();
      session.connection().commit();
    }
    catch ( Exception e ) {
      log.error("Problem creating transaction group info DTO",e);
    }
    finally {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
      log.debug("Leaving retrieveTransGroup()");
    }

    return retval;
  }

  public TransactionInfoDTO retrieveTransaction(TransactionIdDTO id) throws RequestManagerException {
    Session session = null;
    TransactionInfoDTO result = null;
    try {
      session = sf.openSession();
      result = extractTransaction(DBHelper.lookupTransaction(session,id.getId()));
      session.flush();
      session.connection().commit();
    }
    catch ( Exception e ) {
      log.warn("Problem creating transaction DTO",e);
    }
    finally {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }

    return result;
  }

  public TransactionInfoDTO[] extractAllGroupTransactions(ILLTransactionGroup tg)
  {
    TransactionInfoDTO[] result = new TransactionInfoDTO[1];
    result[0] = extractTransaction(tg.getLastTransaction());
    return result;
  }

  private RotaElementDTO[] retrieveRota(ILLTransactionGroup tg)
  {
    RotaElementDTO[] result = new RotaElementDTO[tg.getRota().size()];
    int count=0;
    for(Iterator i = tg.getRota().iterator(); i.hasNext(); )
    {
      RotaEntry re = (RotaEntry) i.next();
      // TODO: Problem
      result[count++] = new RotaElementDTO("BROKEN",ServiceProtocol.ISO);
    }

    return result;
  }

  public ProtocolMessageDTO[] retrieveMessageHistory(TransactionIdDTO id) throws RequestManagerException {
    ProtocolMessageDTO[] retval = null;
    Session session = null;
    try
    {
      session = sf.openSession();
      Vector v = new Vector();

      ILLTransaction t = DBHelper.lookupTransaction(session,id.getId());
      Long trans_id = t.getId();
      Set messages = t.getMessages();
      for ( Iterator message_iterator = messages.iterator(); message_iterator.hasNext(); ) {
        MessageHeader mh = (MessageHeader) message_iterator.next();
        log.debug("Got message header");
        v.add(new ProtocolMessageDTO(1,
                              mh.getEventCode(),
                              new MessageIdDTO(trans_id,mh.getId()),
                              mh.getTimestamp(),
                              mh.getRead()));
      }
      retval = (ProtocolMessageDTO[]) v.toArray();

      session.flush();
      session.connection().commit();
    }
    catch ( Exception e )
    {
      log.warn("Problem fetching transaction group from database",e);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }

    return retval;
  }

  public ProtocolMessageDTO[] retrieveMessageHistory(TransactionGroupIdDTO id) throws RequestManagerException
  {
    ProtocolMessageDTO[] retval = null;
    Session session = null;
    try
    {
      session = sf.openSession();
      Vector v = new Vector();

      ILLTransactionGroup tg = DBHelper.lookupGAR(session, id.getId());
      Set transactions = tg.getTransactions();
      for ( Iterator i = transactions.iterator(); i.hasNext(); )
      {
        ILLTransaction t = (ILLTransaction)i.next();
        Long trans_id = t.getId();
        Set messages = t.getMessages();
        for ( Iterator message_iterator = messages.iterator(); message_iterator.hasNext(); )
        {
          MessageHeader mh = (MessageHeader) message_iterator.next();
          log.debug("Got message header");
          v.add(new ProtocolMessageDTO(1, 
                              mh.getEventCode(),
                              new MessageIdDTO(trans_id,mh.getId()), 
                              mh.getTimestamp(), 
                              mh.getRead()));
        }
      }
      retval = (ProtocolMessageDTO[]) v.toArray();
      session.flush();
      session.connection().commit();
    }
    catch ( Exception e )
    {
      log.warn("Problem fetching transaction group from database",e);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }

    return retval;
  }

  public void updateRequest(TransactionGroupIdDTO id, RequesterViewDTO request) throws RequestManagerException
  {
    Session session = null;
    try
    {
      session = sf.openSession();
      ILLTransactionGroup tg = DBHelper.lookupGAR(session, id.getId());

      // Copy the generic details back into the database
      updateILLRequest(tg, request.getILLRequest());

      if ( request.getRequesterSpecific() != null )
      {
        tg.setPatronName(request.getRequesterSpecific().getBorrowerName());
        tg.setPatronStatus(request.getRequesterSpecific().getBorrowerType());
        tg.setPatronSymbol(request.getRequesterSpecific().getBorrowerId());
        tg.setInitiatingBranchCode(request.getRequesterSpecific().getInitiatingBranch());
        // TODO: private_note, borrower_notes and reply_to_email
      }

      session.flush();
      session.connection().commit();
    }
    catch ( Exception e )
    {
      log.warn("Problem fetching transaction group from database",e);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
  }

  public void updateRequest(TransactionGroupIdDTO id, 
                            ItemIdDTO item_data, 
                            ResponderSpecificInformationDTO request) throws RequestManagerException
  {
    Session session = null;
    try
    {
      session = sf.openSession();
      ILLTransactionGroup tg = DBHelper.lookupGAR(session, id.getId());

      // Copy the generic details back into the database
      // updateILLRequest(tg, request.getILLRequest());
      updateItemData(tg.getRequiredItemDetails(), item_data);

      if ( request != null )
      {
        tg.getRequiredItemDetails().setAccessionNumber(request.getAccessionNumber());
        tg.getRequiredItemDetails().setShelfmark(request.getShelfmark());
        tg.getRequiredItemDetails().setLocalNotes(request.getLocalNotes());
        // TODO: request.getPrivateNote();
      }

      session.flush();
      session.connection().commit();
    }
    catch ( Exception e )
    {
      log.warn("Problem fetching transaction group from database",e);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
  }

  private void updateILLRequest(ILLTransactionGroup tg, ILLRequestMessageDTO req)
  {
    // req.getDeliveryAddress()
    tg.getCurrentDeliveryAddress().setPostal_name(req.getDeliveryAddress().getName());
    tg.getCurrentDeliveryAddress().setPostal_extended_address(req.getDeliveryAddress().getExtendedPostal());
    tg.getCurrentDeliveryAddress().setPostal_street_and_number(req.getDeliveryAddress().getStreetAndNumber());
    tg.getCurrentDeliveryAddress().setPostal_po_box(req.getDeliveryAddress().getPostOfficeBox());
    tg.getCurrentDeliveryAddress().setPostal_city(req.getDeliveryAddress().getCity());
    tg.getCurrentDeliveryAddress().setPostal_region(req.getDeliveryAddress().getRegion());
    tg.getCurrentDeliveryAddress().setPostal_country(req.getDeliveryAddress().getCountry());
    tg.getCurrentDeliveryAddress().setPostal_postcode(req.getDeliveryAddress().getPostcode());
    // req.getDeliveryService()
    // req.getBillingAddress()
    // req.getServiceType()
    // req.getReqOptMessages()
    // req.getSearchType()
    // req.getRequiredMediums()
    // req.getPlaceOnHoldType()
    // req.getItemId()
      // if ( this.service_types != null )
      //   new_details.setService_types((List) ((ArrayList)this.service_types).clone());
      // tg.getRequiredItemDetails().setLevelOfService(this.level_of_service);
      // tg.getRequiredItemDetails().setNeedBeforeDate(this.need_before_date);
      // tg.getRequiredItemDetails().setExpiryFlag(this.expiry_flag);
      // tg.getRequiredItemDetails().setExpiryDate(this.expiry_date);
      // if ( this.supply_medium_info_type != null )
      //   tg.getRequiredItemDetails().setSupplyMediumInfoType((List) ((ArrayList)this.supply_medium_info_type).clone());
      // tg.getRequiredItemDetails().setPlaceOnHold(this.place_on_hold);
      // tg.getRequiredItemDetails().setClientName(this.client_name);
      // tg.getRequiredItemDetails().setClientStatus(this.client_status);
      // tg.getRequiredItemDetails().setClientIdentifier(this.client_identifier);
      // // if ( this.delivery_address != null )
      // //   new_details.setDeliveryAddress((ISO10161Address) this.delivery_address.clone());
      // tg.getRequiredItemDetails().setDeliveryServiceType(this.delivery_service_type);
      // tg.getRequiredItemDetails().setTransportationMode(this.transportation_mode);
      // // if ( this.billing_address != null )
      // //   tg.getRequiredItemDetails().setBillingAddress((ISO10161Address) this.billing_address.clone());
      // tg.getRequiredItemDetails().setItemType(this.item_type);
      // tg.getRequiredItemDetails().setHeldMediumType(this.held_medium_type);
      // tg.getRequiredItemDetails().setCallNumber(this.call_number);
      // tg.getRequiredItemDetails().setAuthor(this.author);
      // tg.getRequiredItemDetails().setAdditionalNoLetters(req.getItemId().getAdditionalNoLetters());
      // tg.getRequiredItemDetails().setAccountNumber(this.account_number);
      // tg.getRequiredItemDetails().setMaxCostCurrencyCode(this.max_cost_currency_code);
      // tg.getRequiredItemDetails().setMaxCostString(this.max_cost_string);
      // tg.getRequiredItemDetails().setReciprocalAgreement(this.reciprocal_agreement);
      // tg.getRequiredItemDetails().setWillPayFee(this.will_pay_fee);
      // tg.getRequiredItemDetails().setPaymentProvided(this.payment_provided);
      // tg.getRequiredItemDetails().setCopyrightCompliance(this.copyright_compliance);
      // if ( this.sysno != null )
        // tg.getRequiredItemDetails().setSystemNo((IPIGSystemNumber)this.sysno.clone());
      // if ( this.natbib != null )
        // tg.getRequiredItemDetails().setNatBibNo((IPIGSystemNumber)this.natbib.clone());
      // tg.getRequiredItemDetails().setAccessionNumber(this.accession_number);
      // tg.getRequiredItemDetails().setShelfmark(this.shelfmark);
      // tg.getRequiredItemDetails().setLocalNotes(this.local_notes);

    // req.getCostInfoType()
    // req.getRequesterNote()
    // req.getCopyrightCompliance()
    // req.String getRequesterRef()
    updateItemData(tg.getRequiredItemDetails(), req.getItemId());
  }

  private void updateItemData(RequiredItemDetails dets, ItemIdDTO item)
  {
    dets.setTitle(item.getTitle());
    dets.setSubtitle(item.getSubTitle());
    dets.setSponsoringBody(item.getSponsoringBody());
    dets.setPlaceOfPublication(item.getPlaceOfPublication());
    dets.setPublisher(item.getPublisher());
    dets.setSeriesTitleNumber(item.getSeriesTitleNumber());
    dets.setVolume(item.getVolumeIssue());
    dets.setIssue(item.getVolumeIssue());
    dets.setEdition(item.getEdition());
    dets.setPublicationDate(item.getPublicationDate());
    dets.setPublicationDateOfComponent(item.getPublicationDateOfComp());
    dets.setAuthorOfArticle(item.getAuthorOfArticle());
    dets.setTitleOfArticle(item.getTitleOfArticle());
    dets.setPagination(item.getPagination());
    dets.setISBN(item.getIsbn());
    dets.setISSN(item.getIssn());
    dets.setVerificationReferenceSource(item.getVerificationReferenceSource());
  }

  /**
   *
   */
  public void replaceUnusedRota(TransactionGroupIdDTO id, RotaElementDTO[] new_rota) throws RequestManagerException
  {
    Session session = null;
    try
    {
      session = sf.openSession();
      ILLTransactionGroup tg = DBHelper.lookupGAR(session, id.getId());
      deleteUnusedRota(session, tg);
      appendRota(session, tg, new_rota);
      session.save(tg);
      session.flush();
      session.connection().commit();
    }
    catch ( Exception e )
    {
      log.warn("Problem fetching transaction group from database",e);
      throw new RuntimeException("Problem replacing rota");
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
  }

  private void deleteUnusedRota(Session session, ILLTransactionGroup tg) throws java.sql.SQLException,
                                                                                org.hibernate.HibernateException
  {
    long current_pos = tg.getCurrentRotaPosition();
    List rota = tg.getRota();

    while ( tg.getRota().size() > (current_pos+1) )
    {
      RotaEntry re = (RotaEntry) tg.getRota().remove((int)(current_pos+1));
      session.delete(re);
    }
  }

  private void appendRota(Session session, ILLTransactionGroup tg, RotaElementDTO[] new_rota)
  {
    for ( int i=0; i<new_rota.length; i++ )
    {
      tg.getRota().add(new RotaEntry(false,
                                     0,
                                     new_rota[i].symbol,
                                     new_rota[i].getMandatoryTransactionQualifier(),
                                     0,      // Name type
                                     (String)null,   // name
                                     (String)null,   // Account number
                                     (String)null,   // Service Identifier
                                     (String)null)); // Service Address
      }
  }

  /**
   *
   */
  public ProtocolMessageDTO retrieveMessage(MessageIdDTO message_id) throws RequestManagerException
  {
    // Fetch back the message as identified in the id part of MessageIdDTO
    ProtocolMessageDTO retval = null;
    Session session = null;
    try
    {
      session = sf.openSession();

      MessageHeader mh = DBHelper.lookupMessage(session,message_id.getMessageId());

      if ( mh instanceof com.k_int.openrequest.db.ISO10161RequestMessageDetails )
      {
        retval = createBeILLRequest((com.k_int.openrequest.db.ISO10161RequestMessageDetails)mh);
        // retval = new ILLRequestMessageDTO();
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161ForwardMessageDetails )
      {
        throw new RuntimeException("Forward not currently supported via Integration Engine ");
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161ShippedMessageDetails )
      {
        retval = createShippedMessageDTO((com.k_int.openrequest.db.ISO10161ShippedMessageDetails)mh);
        // retval = new ShippedMessageDTO();
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161AnswerMessageDetails )
      {
        // Switch different message type based on specific message
        com.k_int.openrequest.db.ISO10161AnswerMessageDetails dets = (com.k_int.openrequest.db.ISO10161AnswerMessageDetails) mh;
        // ISO10161AnswerMessageDetails dets = ans_hdr.getMessageDetails();

        switch ( dets.getResultsExplanationType() )
        {
          case 1: // Conditional
            // retval = new ConditionalAnswerMessageDTO();
            retval = createBeConditionalAnswer(dets);
            break;
          case 2: // Retry
            // retval = new RetryAnswerDTO();
            retval = createBeRetryAnswer(dets);
            break;
          case 3: // Unfilled
            retval = createBeUnfilledAnswer(dets);
            // retval = new UnfilledAnswerDTO();
            break;
          case 4: // Locations
            break;
          case 5: // Will Supply
            // retval = new WillSupplyAnswerDTO();
            retval = createBeWillSupplyAnswer(dets);
            break;
          case 6: // Hold Placed
            // retval = new HoldPlacedAnswerDTO();
            retval = createBeHoldPlacedAnswer(dets);
            break;
          case 7: // Estimate
            break;
        }
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161ConditionalMessageDetails )
      {
        retval = createBeConditionalReply((com.k_int.openrequest.db.ISO10161ConditionalMessageDetails) mh);
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161CancelMessageDetails )
      {
        retval = createCancel((com.k_int.openrequest.db.ISO10161CancelMessageDetails)mh);
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161CancelReplyMessageDetails )
      {
        retval = createCancelReply((com.k_int.openrequest.db.ISO10161CancelReplyMessageDetails)mh);
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161ReceivedMessageDetails )
      {
        retval = createReceived((com.k_int.openrequest.db.ISO10161ReceivedMessageDetails) mh);
        // retval = new ReceivedMessageDTO();
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161RecallMessageDetails )
      {
        retval = createRecall((com.k_int.openrequest.db.ISO10161RecallMessageDetails) mh);
        // retval = new RecallMessageDTO();
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161ReturnedMessageDetails )
      {
        retval = createReturned((com.k_int.openrequest.db.ISO10161ReturnedMessageDetails) mh);
        // retval = new ReturnedMessageDTO();
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161CheckedInMessageDetails )
      {
        retval = createCheckedIn((com.k_int.openrequest.db.ISO10161CheckedInMessageDetails)mh);
        //retval = new CheckedInMessageDTO();
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161OverdueMessageDetails )
      {
        retval = createOverdue((com.k_int.openrequest.db.ISO10161OverdueMessageDetails)mh);
        // retval = new OverdueMessageDTO();
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161RenewMessageDetails )
      {
        retval = createRenew((com.k_int.openrequest.db.ISO10161RenewMessageDetails)mh);
        // retval = new RenewMessageDTO();
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161RenewAnswerMessageDetails )
      {
        retval = createRenewAnswer((com.k_int.openrequest.db.ISO10161RenewAnswerMessageDetails) mh);
        // retval = new RenewAnswerMessageDTO();
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161LostMessageDetails )
      {
        retval = createLost((com.k_int.openrequest.db.ISO10161LostMessageDetails) mh);
        // retval = new LostMessageDTO();
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161DamagedMessageDetails )
      {
        retval = createDamaged((com.k_int.openrequest.db.ISO10161DamagedMessageDetails)mh);
        // retval = new DamagedDTO("Note");
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161MessageMessageDetails )
      {
        retval = createMessage((com.k_int.openrequest.db.ISO10161MessageMessageDetails)mh);
        // retval = new MessageDTO();
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161StatusQueryMessageDetails )
      {
        throw new RuntimeException("No BE Interface class for Status Query");
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161StatusOrErrorReportMessageDetails )
      {
        throw new RuntimeException("No BE Interface class for Status Query");
      }
      else if ( mh instanceof com.k_int.openrequest.db.ISO10161ExpiredMessageDetails )
      {
        retval = createExpired((com.k_int.openrequest.db.ISO10161ExpiredMessageDetails)mh);
        // retval = new ExpiredDTO();
      }
      else
      {
        throw new RuntimeException("Unsupported Message Type");
      }
      session.flush();
      session.connection().commit();
    }
    catch ( Exception e )
    {
      log.warn("Problem fetching message from database",e);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }

    return retval;
  }

  /**
   *
   */
  public void sendMessage(TransactionIdDTO transaction_id, ProtocolMessageDTO message) throws RequestManagerException {
    if ( transaction_id == null )
      throw new RequestManagerException("Null transaction id");

    Session session = null;
    try
    {
      log.debug("Looking up transaction "+transaction_id+", message type is "+message.getIsoType() );
      session = sf.openSession();
      ILLTransaction trans = DBHelper.lookupTransaction(session,transaction_id.getId());

      if ( trans == null )
        throw new RequestManagerException("Unable to locate Transaction "+transaction_id.getId()+" at this location");

      if ( trans.getLocation() == null )
        throw new RequestManagerException("Internal error : No location  for this transaction "+transaction_id);

      LocationSymbol sender = DBHelper.lookupLocation(session,trans.getLocation().getDefaultSymbol());

      if ( sender == null )
        throw new RequestManagerException("Unable to resolve sender : "+trans.getLocation().getDefaultSymbol());

      LocationSymbol recipient = DBHelper.lookupLocation(session,trans.getPVCurrentPartnerIdSymbol());

      if ( recipient == null )
        throw new RequestManagerException("Unable to resolve recipient : "+trans.getPVCurrentPartnerIdSymbol());

      switch ( message.getIsoType() )
      {
        case 1 :
          log.warn("sendMessage:ILL_REQUEST - Should never do this");
          throw new RequestManagerException("You cannot send a request via the sendMessage method. use createTransGroup");
        case 2 :
          log.warn("sendMessage:FORWARD_NOTIFICATION - Not implemented via Integration Engine (yet)");
          break;
        case 3 :
          log.debug("SHIPPED");
          shippedAction(session, transaction_id.getId(), (ShippedMessageDTO) message, trans, sender, recipient);
          break;
        case 4 :
          log.debug("ILL_ANSWER");
          if ( message instanceof ConditionalAnswerMessageDTO )
            conditionalAnswerAction(session, transaction_id.getId(),(ConditionalAnswerMessageDTO)message, trans, sender, recipient);
          else if ( message instanceof HoldPlacedAnswerDTO )
            holdPlacedAnswerAction(session, transaction_id.getId(),(HoldPlacedAnswerDTO)message, trans, sender, recipient);
          else if ( message instanceof RetryAnswerDTO )
            retryAnswerAction(session, transaction_id.getId(),(RetryAnswerDTO)message, trans, sender, recipient);
          else if ( message instanceof UnfilledAnswerDTO )
            unfilledAnswerAction(session, transaction_id.getId(),(UnfilledAnswerDTO)message, trans, sender, recipient);
          else if ( message instanceof WillSupplyAnswerDTO )
            willSupplyAnswerAction(session, transaction_id.getId(),(WillSupplyAnswerDTO)message, trans, sender, recipient);
          break;
        case 5 :
          log.debug("CONDITIONAL_REPLY");
          conditionalReplyAction(session, transaction_id.getId(),(ConditionalReplyMessageDTO)message, trans, sender, recipient);
          break;
        case 6 :
          log.debug("CANCEL");
          cancelAction(session, transaction_id.getId(),(CancelMessageDTO)message, trans, sender, recipient);
          break;
        case 7 :
          log.debug("CANCEL_REPLY");
          cancelReplyAction(session, transaction_id.getId(),(CancelReplyMessageDTO)message, trans, sender, recipient);
          break;
        case 8 :
          log.debug("RECEIVED");
          receivedAction(session, transaction_id.getId(), (ReceivedMessageDTO)message, trans, sender, recipient);
          break;
        case 9 :
          log.debug("RECALL");
          recallAction(session, transaction_id.getId(), (RecallMessageDTO)message, trans, sender, recipient);
          break;
        case 10 :
          log.debug("RETURNED");
          returnedAction(session, transaction_id.getId(), (ReturnedMessageDTO)message, trans, sender, recipient);
          break;
        case 11 :
          log.debug("CHECKED_IN");
          checkedInAction(session, transaction_id.getId(), (CheckedInMessageDTO)message, trans, sender, recipient);
          break;
        case 12 :
          log.debug("OVERDUE");
          overdueAction(session, transaction_id.getId(), (OverdueMessageDTO)message, trans, sender, recipient);
          break;
        case 13 :
          log.debug("RENEW");
          renewAction(session, transaction_id.getId(), (RenewMessageDTO)message, trans, sender, recipient);
          break;
        case 14 :
          log.debug("RENEW_ANSWER");
          renewAnswerAction(session, transaction_id.getId(), (RenewAnswerMessageDTO)message, trans, sender, recipient);
          break;
        case 15 :
          log.debug("LOST");
          lostAction(session, transaction_id.getId(), (LostMessageDTO)message, trans, sender, recipient);
          break;
        case 16 :
          log.debug("DAMAGED");
          damagedAction(session, transaction_id.getId(), (DamagedDTO)message, trans, sender, recipient);
          break;
        case 17 :
          log.debug("MESSAGE");
          messageAction(session, transaction_id.getId(), (MessageDTO)message, trans, sender, recipient);
          break;
        case 18 :
          log.debug("STATUS_QUERY");
          break;
        case 19 :
          log.debug("STATUS_ERROR_REPORT");
          statusOrErrorReportAction(session, transaction_id.getId(), (StatusOrErrorReportDTO)message, trans, sender, recipient);
          break;
        case 20 :
          log.debug("EXPIRED");
          break;
        default:
          log.debug("DEFAULT!");
          break;
      }

      session.flush();
      session.connection().commit();

    } 
    catch ( java.sql.SQLException sqle )
    {
      log.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      log.warn("Problem",he);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
  }

  public void repeatLastService(TransactionIdDTO transaction_id) throws RequestManagerException {

    log.debug("Repeat last service");

    if ( transaction_id == null )
      throw new RequestManagerException("Null transaction id");

    Session session = null;
    try {
      log.debug("Looking up transaction "+transaction_id);
      session = sf.openSession();

      // 1. Lookup last transaction
      ILLTransaction trans = DBHelper.lookupTransaction(session,transaction_id.getId());

      String sender = null;
      String recipient = trans.getPVCurrentPartnerIdSymbol();

      // 2. Lookup last repeatable message
      MessageHeader last_repeatable_message = trans.getLastRepeatableMessage();

      if ( last_repeatable_message != null ) {
        ILL_APDU_type repeat = null;
        String action_string = null;

        // 3. Create message
        log.debug("Attempting to repeat last message: instanceOf "+last_repeatable_message.getClass().getName());

        if ( last_repeatable_message instanceof com.k_int.openrequest.db.ISO10161CancelMessageDetails ) {
          repeat = com.k_int.openrequest.helpers.ISOCancelMessageFactory.repeat(session, 
                                                                                trans, 
                                                                                (com.k_int.openrequest.db.ISO10161CancelMessageDetails) last_repeatable_message);
          action_string="CANreq";
        }

        // 4. Send message
        if ( ( repeat != null ) && ( action_string != null ) ) {
          ILLMessageEnvelope e = new ILLMessageEnvelope(repeat, sender, recipient);
          sendMessage(e,action_string);
        }
      }
      session.flush();
      session.connection().commit();
    }
    catch ( java.sql.SQLException sqle ) {
      log.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("Problem",he);
    }
    finally {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
  }

  /**
   *
   */
  public void acknowledgeMessage(MessageIdDTO message_id) throws RequestManagerException
  {
    Session session = null;
    try
    {
      log.debug("Looking up message "+message_id);
      MessageHeader mh = DBHelper.lookupMessage(session,message_id.getMessageId());
      mh.setRead(true);

      ILLTransaction trans = mh.getParentRequest();
      if ( trans.getUnreadMessageCount() > 0 )
        trans.decrementUnreadMessageCounter();

      ILLTransactionGroup tg = trans.getTransactionGroup();
      if ( tg.getUnreadMessageCount() > 0 )
        tg.decrementUnreadMessageCounter();

      session.flush();
      session.connection().commit();
    } 
    catch ( java.sql.SQLException sqle )
    {
      log.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      log.warn("Problem",he);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
  }

  /**
   *
   */
  public void acknowledgeAllMessages(TransactionGroupIdDTO transaction_group_id) throws RequestManagerException
  {
    Session session = null;
    try
    {
      session = sf.openSession();
      Vector v = new Vector();

      ILLTransactionGroup tg = DBHelper.lookupGAR(session, transaction_group_id.getId());
      Set transactions = tg.getTransactions();
      for ( Iterator i = transactions.iterator(); i.hasNext(); )
      {
        ILLTransaction t = (ILLTransaction)i.next();
        Long trans_id = t.getId();
        Set messages = t.getMessages();
        for ( Iterator message_iterator = messages.iterator(); message_iterator.hasNext(); )
        {
          MessageHeader mh = (MessageHeader) message_iterator.next();
          mh.setRead(true);
        }
        t.setUnreadMessageCount(0);
      }
      tg.setUnreadMessageCount(0);

      session.flush();
      session.connection().commit();
    }
    catch ( Exception e )
    {
      log.warn("Problem fetching transaction group from database",e);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
  }

  /**
   *
   */
  public void closeTransGroup(TransactionGroupIdDTO transaction_group_id, int reason) throws RequestManagerException
  {
    Session session = null;
    try
    {
      session = sf.openSession();
      ILLTransactionGroup tg = DBHelper.lookupGAR(session, transaction_group_id.getId());

      StateTransition st = lookupEvent(session, 
                                       tg.getActiveStateModel().getId(), 
                                       tg.getTransactionGroupStatusCode().getId(), 
                                       "TG_CLOSE");

      if ( st != null )
      {
        tg.setTransactionGroupStatusCode(st.getTo_state());
        tg.setCloseReason(reason);
      }
      else
      {
        throw new RuntimeException("CLOSE Is not a valid action for this Transaction Group");
      }

      // Check that close is a valid operation given the current state of the TG

      session.flush();
      session.connection().commit();
    }
    catch ( Exception e )
    {
      log.warn("Problem fetching transaction group from database",e);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
  }

  /**
   *
   */
  public void closeTransGroup(TransactionGroupIdDTO transaction_group_id) throws RequestManagerException
  {
    Session session = null;
    try
    {
      session = sf.openSession();
      ILLTransactionGroup tg = DBHelper.lookupGAR(session, transaction_group_id.getId());

      if ( tg == null )
        throw new RuntimeException("Unable to locate transaction group for supplied ID");

      StateTransition st = lookupEvent(session, 
                                       tg.getActiveStateModel().getId(), 
                                       tg.getTransactionGroupStatusCode().getId(), 
                                       "TG_CLOSE");

      if ( st != null )
      {
        tg.setTransactionGroupStatusCode(st.getTo_state());
      }
      else
      {
        throw new RuntimeException("CLOSE Is not a valid action for this Transaction Group");
      }

      // Check that close is a valid operation given the current state of the TG

      session.flush();
      session.connection().commit();
    }
    catch ( Exception e )
    {
      log.warn("Problem fetching transaction group from database",e);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
  }

  /**
   *
   */
  public void changeCloseReason(TransactionGroupIdDTO transaction_group_id, int reason) throws RequestManagerException
  {
    Session session = null;
    try
    {
      session = sf.openSession();
      ILLTransactionGroup tg = DBHelper.lookupGAR(session, transaction_group_id.getId());

      if ( tg != null )
        tg.setCloseReason(reason);
      else
        throw new RuntimeException("Unable to locate transaction group for supplied ID");

      // Check that close is a valid operation given the current state of the TG

      session.flush();
      session.connection().commit();
    }
    catch ( Exception e )
    {
      log.warn("Problem fetching transaction group from database",e);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
  }

  /**
   *
   */
  public RequestCountDTO[] retrieveRequestCounts() throws RequestManagerException {
    return StatelessRequestManagerImpl.retrieveRequestCounts(sf, internal_loc_id);
  }

  private static void putIfNotNull(Map m, String name, Object value) {
    if ( value != null ) {
      m.put(name,value);
    }
  }

  /**
   *
   */
  public SearchResultDTO searchQuery(RequestManagerQueryDTO query) throws RequestManagerException {

    // Use the OQL Builder to construct a query....
    int qry_handle = qry_ctr++;
    SearchResultDTO result = new SearchResultDTO(qry_handle);

    Session session = null;
    try {
      session = sf.openSession();

      log.debug("Search...."+query.toString());

      Hashtable values = new Hashtable();
      putIfNotNull(values,"Role",query.getRole());
      putIfNotNull(values,"LocationId",internal_loc_id);
      putIfNotNull(values,"ReqStatusCode",  query.getState());
      putIfNotNull(values,"ReqPhaseCode", query.getPhase());
      putIfNotNull(values,"RequesterReference",query.getRequestId());
      putIfNotNull(values,"Author",query.getAuthor());
      putIfNotNull(values,"Title",query.getTitle());
      // values.put("ISBN",query.getIdentifier());
      // values.put("ISSN",query.getIdentifier());
      putIfNotNull(values,"ResponderRef",query.getResponder());
      putIfNotNull(values,"Borrower",query.getPatronId());

      log.debug("Query Values : "+values);

      Vector bind_vars = new Vector();
      Vector bind_types = new Vector();
      OQLSelectStatement s = com.k_int.oql.util.OQLSelectFactory.createStatement(StatelessRequestManagerImpl.qd, bind_vars, bind_types, values);

      // This is taken care of by Default Element Set
      OQLCollectionScope root_scope = s.lookupCollectionScope("tg");
      OQLCollectionScope lt_scope = s.lookupCollectionScope("last_trans");
      // OQLCollectionScope lts_scope = s.lookupCollectionScope("last_trans_status");
      // s.addSelectExpression(new OQLCountExpression(new ScopedAccessPathExpression(root_scope, "Id")));

      log.debug("New statement: "+s);
      log.debug("bind vars: "+bind_vars);

      Iterator iter = session.createQuery(s.toString())
                       .setParameters(bind_vars.toArray(), (Type[])bind_types.toArray(new Type[0]))
                       .iterate();

      if ( iter.hasNext() ) {
        Long count = (Long) iter.next();
        long hit_count = count.longValue();
        log.debug("Result count: "+hit_count);
        iter = null;

        result.setRowCount(hit_count);
        result.setStatus(SearchStatus.OK);

        if ( hit_count > 0 ) {
          bind_vars.clear();
          bind_types.clear();
          OQLSelectStatement result_rows = 
                com.k_int.oql.util.OQLSelectFactory.createStatement(StatelessRequestManagerImpl.qd, bind_vars, bind_types, values, "QueryResult");

          log.debug("New query is : "+result_rows);
          List qry_result = session.createQuery(result_rows.toString())
                              .setParameters(bind_vars.toArray(), (Type[])bind_types.toArray(new Type[0]))
                              .list();

          log.debug("Store internal search with handle "+qry_handle);
          InternalSearchInfo info = new InternalSearchInfo(session, result, qry_result, qry_handle );
          open_queries.put(new Integer(qry_handle), info);
        }
        else
        {
          InternalSearchInfo info = new InternalSearchInfo(session, result, null, qry_handle );
          open_queries.put(new Integer(qry_handle), info);
        }
      }
    }
    // catch ( java.sql.SQLException sqle )
    // {
    //   log.warn("\n****Problem with search:",sqle);
    //   throw new RuntimeException(sqle.toString());
    // }
    catch ( org.hibernate.HibernateException he )
    {
      log.warn("\n****Problem with search:",he);
      throw new RuntimeException(he.toString());
    }
    finally
    {
    }

    return result;
  }

  /**
   *
   */
  public SearchResultDTO searchCategory(String category) throws RequestManagerException
  {
    int qry_handle = qry_ctr++;
    SearchResultDTO result = new SearchResultDTO(qry_handle);

    Session session = null;
    try
    {
      session = sf.openSession();

      // String folder_code = RequestCategory.fromIso(category).toFolderCode();
      // String folder_code = RequestCategory.fromIso(category).toFolderCode();
      log.debug("Searching for loc "+internal_loc_id+" for folder code "+category);

      // Convert the category into a String
      List qry_result = session.createQuery(Q1)
                           .setParameter(0,internal_loc_id,Hibernate.LONG)
                           .setParameter(1,category,Hibernate.STRING)
                           .list();
      InternalSearchInfo info = new InternalSearchInfo(session, result, qry_result, qry_handle );
      result.setRowCount(qry_result.size());

      log.debug("Result count: "+qry_result.size());

      result.setStatus(SearchStatus.OK);

      log.debug("Store internal search with handle "+qry_handle);
      open_queries.put(new Integer(qry_handle), info);
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("\n****Problem with search:",he);
    }
    finally
    {
      // try { session.close(); } catch ( Exception e ) {}
      // session = null;
    }

    return result;
  }

  /**
   *
   */
  public ResultSetDTO retrieveResultSet(SearchResultDTO result, long count, int start) throws RequestManagerException {
    // Retrieve the query info
    SearchResultEntryDTO[] recs = new SearchResultEntryDTO[(int)count];
    Integer key = new Integer(result.getHandle());

    InternalSearchInfo info = (InternalSearchInfo) open_queries.get(new Integer(result.getHandle()));

    for ( int i=0; i<count; i++ ) {
      recs[i] = info.getResultFor(start+i);
    }

    return new ResultSetDTO(ResultStatus.OK,count,recs);
  }

  /**
   *
   */
  public void releaseSearchResult(SearchResultDTO result) throws RequestManagerException {
    log.debug("releaseSearchResult");
    InternalSearchInfo info = (InternalSearchInfo) open_queries.remove(new Integer(result.getHandle()));
    if ( info != null ) {
      try {
        info.getSession().flush();
        info.getSession().connection().commit();
        info.getSession().close(); 
      } 
      catch ( Exception e ) { 
      }
    }
    else {
      throw new RuntimeException("Attempting to release an unknown SearchResult. (Perhaps you already released it?)");
    }
    info = null;
  }

  public void release() throws RequestManagerException {
    log.debug("Release session");
    log.debug("open_queries.size() (Should=0) "+open_queries.size());
    open_queries = null;
  }

  protected void finalize()
  {
    open_queries = null;
    log.debug("finalize RequestManagerImpl, instance count="+(--instance_counter));
  }

  // Forward-Notification - not implemented yet

  // Shipped,
  private boolean shippedAction(Session session,
                                Long transaction_id, 
                                ShippedMessageDTO shipped_message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) {
    try {
      int shipped_service_type_int = shipped_message.getShippedServiceType() != null ?
                      shipped_message.getShippedServiceType().toIso() : 1;
      Long shipped_conditions = ( ( shipped_message.getShippedConditions() != null ) && 
                                  ( shipped_message.getShippedConditions().toIso() > 0 ) ) ?
                       new Long(shipped_message.getShippedConditions().toIso()) : null;

      ILL_APDU_type pdu = ISOShippedMessageFactory.create(session,
                                                          sender,
                                                          recipient,
                                                          trans,
                                                          shipped_service_type_int,
                                                          shipped_message.getDateShipped(),
                                                          shipped_message.getDateDue(),
                                                          new Boolean(shipped_message.isRenewable()),
                                                          new Long(0), // chargeable_units, // Chargable units
                                                          shipped_message.getTotalCostCurrency(),
                                                          shipped_message.getTotalCostAmount(),
                                                          shipped_conditions,
                                                          shipped_message.getName(),
                                                          shipped_message.getExtendedPostal(),
                                                          shipped_message.getStreetAndNumber(),
                                                          shipped_message.getPOBox(),
                                                          shipped_message.getCity(),
                                                          shipped_message.getRegion(),
                                                          shipped_message.getCountry(),
                                                          shipped_message.getPostcode(),
                                                          shipped_message.getResponderNote(),
                                                          null,
                                                          app_context);  // Extensions

      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"SHIreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // ILL-Answer (Variants)
  private boolean conditionalAnswerAction(Session session,
                                          Long transaction_id, 
                                          ConditionalAnswerMessageDTO conditional_answer,
                                          ILLTransaction trans,
                                          LocationSymbol sender,
                                          LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOAnswerMessageFactory.createConditionalAnswer(session,
                                                                          sender,
                                                                          recipient,
                                                                          trans,
                                                                          conditional_answer.getConditionsIso(),
                                                                          conditional_answer.getDateForReply(),
                                                                          conditional_answer.getResponderNote(), app_context);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"ANSreq-CO");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

      return true;
  }

  private boolean holdPlacedAnswerAction(Session session,
                                         Long transaction_id, HoldPlacedAnswerDTO hold_placed_answer,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOAnswerMessageFactory.createHoldPlacedAnswer(session,
                                                                          sender,
                                                                          recipient,
                                                     trans,
                                                     hold_placed_answer.getEstimatedDateAvailable(),
                                                     hold_placed_answer.getISOHoldPlacedMediumType(),
                                                     null, 
                                                     hold_placed_answer.getResponderNote(),
                                                     app_context); // Vector locations
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"ANSreq-HP");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  private boolean retryAnswerAction(Session session,
                                    Long transaction_id, RetryAnswerDTO retry_answer,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOAnswerMessageFactory.createRetryAnswer(session,
                                                                          sender,
                                                                          recipient,
                                                trans,
                                                retry_answer.getISOReasonNotAvailable(),
                                                retry_answer.getRetryDate(),   // Date retry_date
                                                null, 
                                                retry_answer.getResponderNote(),   // Date retry_date
                                                app_context) ; // Vector locations
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"ANSreq-RY");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  private boolean unfilledAnswerAction(Session session,
                                       Long transaction_id, UnfilledAnswerDTO unfilled_answer,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOAnswerMessageFactory.createUnfilledAnswer(session,
                                                                       sender,
                                                                       recipient,
                                                                       trans,
                                                                       unfilled_answer.ISOReason(),    // int reason_unfilled,
                                                                       null,
                                                                       unfilled_answer.getResponderNote(),
                                                                       app_context);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"ANSreq-UN");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  private boolean willSupplyAnswerAction(Session session,
                                          Long transaction_id, WillSupplyAnswerDTO will_supply_answer,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOAnswerMessageFactory.createWillSupplyAnswer(session,
                                                                          sender,
                                                                          recipient,
                                                trans,
                                                will_supply_answer.getISOReasonWillSupply(),
                                                will_supply_answer.getSupplyDate(),
                                                null,  //Postal_Address_type return_to_address,
                                                null,  // Vector locations,
                                                null,
                                                will_supply_answer.getResponderNote(),
                                                app_context); // Electronic_Delivery_Service_type electronic_delivery_service);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"ANSreq-WS");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }
    return true;
  }


  // Conditional-Reply,
  private boolean conditionalReplyAction(Session session,
                                          Long transaction_id, 
                                         ConditionalReplyMessageDTO conditional_reply,
                                         ILLTransaction trans,
                                         LocationSymbol sender,
                                         LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOConditionalReplyMessageFactory.create(session,
                                     trans,
                                     new Boolean(conditional_reply.isAnswer()), // Boolean answer,
                                     conditional_reply.getRequesterNote(),      // String note,
                                     null);        // Extensions factories.
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());

      if ( conditional_reply.isAnswer() )
        sendMessage(e,"C-REPreq+");
      else
        sendMessage(e,"C-REPreq-");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }
    return true;
  }


  // Cancel,
  private boolean cancelAction(Session session,
                               Long transaction_id, 
                               CancelMessageDTO cancel,
                               ILLTransaction trans,
                               LocationSymbol sender,
                               LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOCancelMessageFactory.create(session,
                                     trans,
                                     cancel.getRequesterNote(),  // String requester note
                                     null); // ISOExtensionFactory[] extens);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"CANreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }
    return true;
  }


  // Cancel-Reply,
  private boolean cancelReplyAction(Session session,
                                          Long transaction_id, 
                                    CancelReplyMessageDTO cancel_reply,
                                    ILLTransaction trans,
                                    LocationSymbol sender,
                                    LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOCancelReplyMessageFactory.create(session,
                                     trans,
                                     new Boolean(cancel_reply.isAnswer()), // Boolean answer,
                                     cancel_reply.getResponderNote(),      // String note,
                                     null);        // ISOExtensionFactory[] extens);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      if ( cancel_reply.isAnswer() )
        sendMessage(e,"CARreq+");
      else
        sendMessage(e,"CARreq-");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }
    return true;
  }


  // Received
  //
  private boolean receivedAction(Session session,
                                 Long transaction_id, 
                                 ReceivedMessageDTO received_message,
                                 ILLTransaction trans,
                                 LocationSymbol sender,
                                 LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOReceivedMessageFactory.create(session,
                                                           trans,
                                                           received_message.getDateReceived(),
                                                           BigInteger.valueOf(received_message.getServiceType().toIso()),
                                                           received_message.getRequesterNote(),
                                                           null);

      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());

      sendMessage(e,"RCVreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // Recall,
  private boolean recallAction(Session session,
                                          Long transaction_id, 
                               RecallMessageDTO recall,
                               ILLTransaction trans,
                               LocationSymbol sender,
                               LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISORecallMessageFactory.create(session,
                                                         trans,
                                                         recall.getResponderNote(),  // String responder note
                                                         null); // extensions factories

      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());

      sendMessage(e,"RCLreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // Returned,
  private boolean returnedAction(Session session,
                                          Long transaction_id, 
                                 ReturnedMessageDTO returned,
                                 ILLTransaction trans,
                                 LocationSymbol sender,
                                 LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOReturnedMessageFactory.create(session,
                                     trans,
                                     null,  // java.util.Vector supplemental_item_description,
                                     returned.getDateReturned(),  // Date date_returned,
                                     returned.getReturnedVia(),  // String returned_via,
                                     null,  // Amount_type insured_for,
                                     returned.getRequesterNote(),  // String requester_note,
                                     null); // ISOExtensionFactory[] exten;
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"RETreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // Checked-In,
  private boolean checkedInAction(Session session,
                                          Long transaction_id, 
                                  CheckedInMessageDTO recall,
                                  ILLTransaction trans,
                                  LocationSymbol sender,
                                  LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOCheckedInMessageFactory.create(session,
                                     trans,
                                     recall.getDateCheckedIn(),  // Date date_checked_in,
                                     recall.getResponderNote(),  // String responder_note,
                                     null); // ISOExtensionFactory[] extens)
;
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"CHKreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // Overdue,
  private boolean overdueAction(Session session,
                                          Long transaction_id, 
                                OverdueMessageDTO overdue,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOOverdueMessageFactory.create(session,
                                     trans,
                                     overdue.getDateDue(),  // Date date_due,
                                     overdue.isRenewable(),  // boolean renewable,
                                     overdue.getResponderNote(),  // String responder_note,
                                     null); // ISOExtensionFactory[] extens);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"DUEreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // Renew,
  private boolean renewAction(Session session,
                                          Long transaction_id, 
                              RenewMessageDTO renew,
                              ILLTransaction trans,
                              LocationSymbol sender,
                              LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISORenewMessageFactory.create(session,
                                     trans,
                                     renew.getDesiredDueDate(),  // Date desired_due_date,
                                     renew.getRequesterNote(),  // String requester_note,
                                     null); // ISOExtensionFactory[] extens)
;
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"RENreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // Renew-Answer,
  private boolean renewAnswerAction(Session session,
                                          Long transaction_id, 
                                    RenewAnswerMessageDTO renew_answer,
                                    ILLTransaction trans,
                                    LocationSymbol sender,
                                    LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISORenewAnswerMessageFactory.create(session,
                                                              trans,
                                                              renew_answer.isAnswer(), // boolean answer,
                                                              renew_answer.getDateDue(),  // Date date_due,
                                                              renew_answer.isRenewable(), // boolean renewable,
                                                              renew_answer.getResponderNote(),  // String responder_note,
                                                              null); // ISOExtensionFactory[] extens);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      if ( true )
        sendMessage(e,"REAreq+");
      else
        sendMessage(e,"REAreq-");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // Lost,
  private boolean lostAction(Session session,
                                          Long transaction_id, 
                             LostMessageDTO lost,
                             ILLTransaction trans,
                             LocationSymbol sender,
                             LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISOLostMessageFactory.create(session,
                                     trans,
                                     lost.getNote(),  // String note,
                                     null); // ISOExtensionFactory[] extens);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"LSTreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // Damaged,
  private boolean damagedAction(Session session,
                                          Long transaction_id, 
                                DamagedDTO damaged,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient)
  {
    try
    {
      ILL_APDU_type pdu = ISODamagedMessageFactory.create(session,
                                     trans,
                                     damaged.getNote(),   // String note,
                                     null);  // ISOExtensionFactory[] extens);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"DAMreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // Message,
  private boolean messageAction(Session session,
                                Long transaction_id, 
                                MessageDTO message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) throws RequestManagerException
  {
    try
    {
      ILL_APDU_type pdu = ISOMessageMessageFactory.create(session,
                                                          trans,
                                                          message.getNote(),   // String note,
                                                          null);  // ISOExtensionFactory[] extens;
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"MSGreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // Status-Query,
  // Status-Or-Error-Report,
  private boolean statusOrErrorReportAction(Session session, 
                                            Long transaction_id,
                                            StatusOrErrorReportDTO dto_message,
                                            ILLTransaction trans,
                                            LocationSymbol sender,
                                            LocationSymbol recipient) throws RequestManagerException {
    try
    {
      ILL_APDU_type pdu = ISOStatusOrErrorReportMessageFactory.create(session,
                                                                      trans,
                                                                      dto_message);

      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"STRreq");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

    return true;
  }

  // Expired


  // End of PDU packages


  public void sendMessage(ILLMessageEnvelope e, String event) throws RequestManagerException {

    try {
      // MessageDispatcher md = (MessageDispatcher) java.rmi.Naming.lookup(rmi_server_url+"/OpenRequest/WorkflowController");
      md.dispatch("System",event,e);
    }
    catch ( Exception exception ) {
      throw new RequestManagerException("Problems sending Message",exception);
    }
  }

  // ToDo:
  private PostalAddressDTO createPostalAddress(ISO10161Address address)
  {
    return new PostalAddressDTO("name",
                               "extended_postal",
                               "street_and_number",
                               "post_office_box",
                               "city",
                               "region",
                               "country",
                               "postcode");
  }

  private ItemIdDTO extractItemId(RequiredItemDetails details)
  {
    ItemIdDTO retval = new ItemIdDTO(ItemTypeExtended.fromIso((int)(details.getItemType())),
                    MediumType.fromIso((int)(details.getHeldMediumType())),
                    details.getTitle(),
                    details.getSubtitle(),
                    details.getAuthor(),
                    details.getSponsoringBody(),
                    details.getPublisher(),
                    details.getPlaceOfPublication(),
                    details.getPublicationDate(),
                    details.getISBN(),
                    details.getISSN(),
                    details.getTitleOfArticle(),
                    details.getAuthorOfArticle(),
                    details.getVolume()+" "+details.getIssue(),
                    details.getPublicationDateOfComponent(),
                    details.getPagination(),
                    details.getSystemNo(),
                    details.getCallNumber(),
                    details.getNatBibNo(),
                    details.getSeriesTitleNumber(),
                    details.getVerificationReferenceSource(),
                    details.getEdition());
    return retval;
  }

  private ILLRequestMessageDTO createBeILLRequest(com.k_int.openrequest.db.ISO10161RequestMessageDetails dets)
  {
    // com.k_int.openrequest.db.ISO10161RequestMessageDetails dets = mh.getIsoRequestMessageDetails();

    LevelOfService level_of_service = null;
    ExpiryFlag expiry_flag = null;
    PlaceOnHoldType place_on_hold_type = null;

    if ( dets.getRequiredItemDetails().getLevelOfService() != null )
      level_of_service = LevelOfService.fromIso(dets.getRequiredItemDetails().getLevelOfService().charAt(0));
    if ( dets.getRequiredItemDetails().getExpiryFlag() > 0 )
      expiry_flag = ExpiryFlag.fromIso((int)(dets.getRequiredItemDetails().getExpiryFlag()));
    if ( dets.getRequiredItemDetails().getPlaceOnHold() > 0 )
      place_on_hold_type = PlaceOnHoldType.fromIso((int)(dets.getRequiredItemDetails().getPlaceOnHold()));

    // Info about service types:
    //
    List service_types_list = new ArrayList();
    if (  dets.getRequiredItemDetails().getService_types() != null ) {
      for ( java.util.Iterator i = dets.getRequiredItemDetails().getService_types().iterator(); i.hasNext(); ) {
        service_types_list.add(i.next());
      }
    }

    return new ILLRequestMessageDTO( TransactionType.fromIso((int)(dets.getTransactionType())),
                             createPostalAddress(dets.getDeliveryAddress()),
                             extractDeliveryService(dets.getRequiredItemDetails()),
                             createPostalAddress(dets.getBillingAddress()),
                             service_types_list,
                             new RequesterOptionalMessagesDTO(), // TODO: Get real data from DB
                             new SearchTypeDTO(level_of_service,
                                               dets.getRequiredItemDetails().getNeedBeforeDate(), 
                                               expiry_flag,
                                               dets.getRequiredItemDetails().getExpiryDate()),
                             extractRequiredMediums(dets.getRequiredItemDetails().getSupplyMediumInfoType()),
                             place_on_hold_type,
                             extractItemId(dets.getRequiredItemDetails()),
                             extractCostInfo(dets.getRequiredItemDetails()),
                             dets.getForwardFlag(),
                             dets.getRequesterNote(),
                             dets.getRequiredItemDetails().getCopyrightCompliance(),
                             dets.getParentRequest().getTransactionGroup().getRequesterReferenceAuthority(),
                             dets.getParentRequest().getTransactionGroup().getRequesterReference(),
                             dets.getRequiredItemDetails().getClientIdentifier()
                             );
  }

  private ShippedMessageDTO createShippedMessageDTO(com.k_int.openrequest.db.ISO10161ShippedMessageDetails dets)
  {
    // ISO10161ShippedMessageDetails dets = mh.getMessageDetails();
    // ToDo: Loads of fixes in here
    return new ShippedMessageDTO(ServiceType.fromIso((int)(dets.getShippedServiceType())),
                         dets.getDateShipped(),
                         dets.getDateDue(),
                         dets.getRenewable().booleanValue(), // boolean renewable,
                         dets.getCurrencyCode(), // Currency total_cost_currency,
                         dets.getMonetaryValue(), // String total_cost_amount,
                         ShippedCondition.fromIso((int)(dets.getShippedConditions())), //ShippedCondition shipped_conditions,
                         DeliveryService.fromIso(dets.getTransportationMode()), // TODO: Talis funny.. phy deliv only
                         dets.getInsuredForCurrencyCode(),
                         dets.getInsuredForMonetaryValue(),
                         dets.getReturnInsuredForCurrencyCode(),
                         dets.getReturnInsuredForMonetaryValue(),
                         new SupplyMediumType[0], // SupplyMediumType[] mediums,
                         new int[0], // int[] units,
                         new ResponderOptionalMessagesDTO(), // ResponderOptionalMessagesDTO responder_optional_messages,
                         dets.getPostalName(), // String name,
                         dets.getPostalExtendedAddress(), // String extendedPostal,
                         dets.getPostalStreetAndNumber(), // String streetAndNumber,
                         dets.getPostalPOBox(), // String POBox
                         dets.getPostalCity(), // String city,
                         dets.getPostalRegion(), // String region,
                         dets.getPostalCountry(), // String county,
                         dets.getPostalPostcode(), // String postcode,
                         dets.getNote());
  }

  private ConditionalAnswerMessageDTO createBeConditionalAnswer(com.k_int.openrequest.db.ISO10161AnswerMessageDetails dets)
  {
    // ISO10161AnswerMessageDetails dets = mh.getMessageDetails();
    // TODO.. Parse and store date as date rather than anything else
    return new ConditionalAnswerMessageDTO( Conditions.fromIso(dets.getConditionalConditions()),
                                    (Date) dets.getConditionalDateForReply(),
                                    dets.getLocations(),
                                    dets.getNote());
  }

  private RetryAnswerDTO createBeRetryAnswer(com.k_int.openrequest.db.ISO10161AnswerMessageDetails dets)
  {
    // ISO10161AnswerMessageDetails dets = mh.getMessageDetails();

    return new RetryAnswerDTO(27, (Date) null, dets.getNote());
  }

  private UnfilledAnswerDTO createBeUnfilledAnswer(com.k_int.openrequest.db.ISO10161AnswerMessageDetails dets)
  {
    // ISO10161AnswerMessageDetails dets = mh.getMessageDetails();
    return new UnfilledAnswerDTO(ReasonUnfilled.OTHER, dets.getNote()); // TODO: Fix
  }

  private WillSupplyAnswerDTO createBeWillSupplyAnswer(com.k_int.openrequest.db.ISO10161AnswerMessageDetails dets)
  {
    // ISO10161AnswerMessageDetails dets = mh.getMessageDetails();
    return new WillSupplyAnswerDTO(ReasonWillSupply.OTHER,
                                  (null), // Date supply_date,
                                  "name", // String name,
                                  "extended_postal", // String extended_postal,
                                  "street_and_number", // String street_and_number,
                                  "city", // String city,
                                  "region", // String region,
                                  "country", // String country,
                                  "postcode", // String postcode,
                                  dets.getNote());
  }

  private HoldPlacedAnswerDTO createBeHoldPlacedAnswer(com.k_int.openrequest.db.ISO10161AnswerMessageDetails dets)
  {
    // ISO10161AnswerMessageDetails dets = mh.getMessageDetails();
    return new HoldPlacedAnswerDTO(null,
                                  MediumType.NONE,
                                  dets.getNote());
  }

  private ConditionalReplyMessageDTO createBeConditionalReply(com.k_int.openrequest.db.ISO10161ConditionalMessageDetails dets)
  {
    // ISO10161ConditionalMessageDetails dets = mh.getMessageDetails();
    return new ConditionalReplyMessageDTO(dets.getAnswer().booleanValue(), dets.getNote());
  }

  private CancelMessageDTO createCancel(com.k_int.openrequest.db.ISO10161CancelMessageDetails dets)
  {
    // ISO10161CancelMessageDetails dets = mh.getMessageDetails();
    return new CancelMessageDTO(dets.getNote());
  }

  private CancelReplyMessageDTO createCancelReply(com.k_int.openrequest.db.ISO10161CancelReplyMessageDetails dets)
  {
    // ISO10161CancelReplyMessageDetails dets = mh.getMessageDetails();
    return new CancelReplyMessageDTO(dets.getAnswer(), dets.getNote());
  }
  
  private ReceivedMessageDTO createReceived(com.k_int.openrequest.db.ISO10161ReceivedMessageDetails dets)
  {
    // ISO10161ReceivedMessageDetails dets = mh.getMessageDetails();
    return new ReceivedMessageDTO(dets.getDateReceived(),
                          ServiceType.fromIso(dets.getShippedServiceType().intValue()),
                          dets.getNote());
  }

  private RecallMessageDTO createRecall(com.k_int.openrequest.db.ISO10161RecallMessageDetails dets)
  {
    // ISO10161RecallMessageDetails dets = mh.getMessageDetails();
    return new RecallMessageDTO(dets.getNote());
  }

  private ReturnedMessageDTO createReturned(com.k_int.openrequest.db.ISO10161ReturnedMessageDetails dets)
  {
    // ISO10161ReturnedMessageDetails dets = mh.getMessageDetails();
    return new ReturnedMessageDTO(dets.getDateReturned(),
                          dets.getReturnedVia(),
                          dets.getInsuredForCurrencyCode(),
                          dets.getInsuredForMonetaryValue(),
                          dets.getNote());
  }

  private CheckedInMessageDTO createCheckedIn(com.k_int.openrequest.db.ISO10161CheckedInMessageDetails dets)
  {
    // ISO10161CheckedInMessageDetails dets = mh.getMessageDetails();
    return new CheckedInMessageDTO(dets.getDateCheckedIn(),
                           dets.getNote());
  }

  private OverdueMessageDTO createOverdue(com.k_int.openrequest.db.ISO10161OverdueMessageDetails dets)
  {
    // ISO10161OverdueMessageDetails dets = mh.getMessageDetails();

    return new OverdueMessageDTO(dets.getDateDue(),
                         false, // TOTO: boolean renewable,
                         dets.getNote());
  }

  private RenewMessageDTO createRenew(com.k_int.openrequest.db.ISO10161RenewMessageDetails dets)
  {
    // ISO10161RenewMessageDetails dets = mh.getMessageDetails();
    return new RenewMessageDTO(dets.getDesiredDueDate(), dets.getNote());
  }

  private RenewAnswerMessageDTO createRenewAnswer(com.k_int.openrequest.db.ISO10161RenewAnswerMessageDetails  dets)
  {
    // ISO10161RenewAnswerMessageDetails dets = mh.getMessageDetails();
    return new RenewAnswerMessageDTO(dets.getAnswer().booleanValue(),
                             dets.getDateDue(), 
                             dets.getRenewable().booleanValue(),
                             dets.getNote());
  }

  private LostMessageDTO createLost(com.k_int.openrequest.db.ISO10161LostMessageDetails dets)
  {
    // ISO10161LostMessageDetails dets = mh.getMessageDetails();
    return new LostMessageDTO(dets.getNote());
  }

  private DamagedDTO createDamaged(com.k_int.openrequest.db.ISO10161DamagedMessageDetails dets)
  {
    // ISO10161DamagedMessageDetails dets = mh.getMessageDetails();
    return new DamagedDTO(dets.getNote());
  }

  private MessageDTO createMessage(com.k_int.openrequest.db.ISO10161MessageMessageDetails dets)
  {
    // ISO10161MessageMessageDetails dets = mh.getMessageDetails();
    return new MessageDTO(dets.getNote());
  }

  private ExpiredDTO createExpired(com.k_int.openrequest.db.ISO10161ExpiredMessageDetails dets)
  {
    // ISO10161ExpiredMessageDetails dets = mh.getMessageDetails();
    return new ExpiredDTO();
  }

  private SupplyMediumType[] extractRequiredMediums(List supply_type_list) {

    SupplyMediumType[] retval = null;

    if ( supply_type_list != null ) {
      int count=0;
      retval = new SupplyMediumType[supply_type_list.size()];
      for ( Iterator i = supply_type_list.iterator(); i.hasNext(); ) {
        MediumInfo mi = (MediumInfo)i.next();
        retval[count++] = SupplyMediumType.fromIso((int)(mi.getMediumType()));
      }
    }

    return retval;
  }

  // TODO: Implement
  private CostInfoTypeDTO extractCostInfo(RequiredItemDetails dets)
  {
    return new CostInfoTypeDTO(dets.getAccountNumber(),
                               dets.getMaxCostCurrencyCode(),
                               dets.getMaxCostString(),
                               dets.getReciprocalAgreement());
  }

  private StateTransition lookupEvent(Session session,
                                      Long state_model,
                                      Long from_state,
                                      String event_code) {
    StateTransition retval = null;

    try {
      retval = DBHelper.findTransition(session,
                                       state_model,
                                       from_state,
                                       event_code);
    }
    catch ( Exception e ) {
      log.warn("Problem looking up transition from state "+from_state+" using event "+event_code+" using model "+state_model,e);
    }

    return retval;
  }

  private TransactionInfoDTO extractTransaction(ILLTransaction trans) {
    TransactionInfoDTO retval = null;

    if ( trans != null ) {
      retval =  new TransactionInfoDTO( 
                  new TransactionIdDTO(trans.getId()),
                  com.k_int.openrequest.integration.iface.Constants.convertState(trans.getRequestStatusCode().getCode()),
                  trans.getRequesterSymbol(),
                  trans.getResponderSymbol(),
                  ( trans.getUnreadMessageCount() > 0 ? true : false ) );
    }
    else {
      log.warn("Cannot extract transaction info DTO from null transaction");
    }

    return retval;
  }

  private DeliveryServiceDTO extractDeliveryService(RequiredItemDetails dets) {
    DeliveryServiceDTO retval = null;

    int val = (int) ( dets.getDeliveryServiceType() ) ;

    if ( BeDeliveryType.ELECTRONIC.toIso() == val ) {
      retval = new ElectronicDeliveryDTO(
                 BeTelecomServiceId.fromIso(dets.getTelecomServiceIdentifier()),  // FAX Or EMAIL
                 dets.getTelecomServiceAddress());
    } else if ( BeDeliveryType.PHYSICAL.toIso() == val ) {
      retval = new PhysicalDeliveryDTO(
                 BeTransportationMode.fromIso(dets.getTransportationMode())); // HAYS OR POSTAL
    }

    return retval;
  }
}
