package com.k_int.openrequest.integration.iface;

public enum BeTelecomServiceId {

  FAX, EMAIL;

  public String toString()  {
    switch ( this ) {
      case FAX: return "Fax";
      case EMAIL: return "Email";
      default: return "Unknown";
    }
  }

  public String toIso()     { 
    switch ( this ) {
      case FAX: return "Fax";
      case EMAIL: return "Email";
      default: return "Unknown";
    }
  }

  public static BeTelecomServiceId fromIso(String iso) {
    if ( iso != null ) {
      if ( iso.equals("Fax") ) return FAX;
      if ( iso.equals("Email") ) return EMAIL;
    }
    return null;
  }
}
