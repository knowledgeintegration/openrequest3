package com.k_int.openrequest.integration.provider.openrequest;

import java.math.BigInteger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.*;
import java.io.StringWriter;
import org.hibernate.*;
import org.hibernate.type.*;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import com.k_int.QueryDescriptor.*;
import com.k_int.openrequest.integration.iface.*;
import com.k_int.openrequest.integration.provider.base.*;
import com.k_int.oql.iface.*;
import com.k_int.oql.util.*;


/*
 * $Log: InternalSearchInfo.java,v $
 * Revision 1.3  2005/05/14 10:16:56  ibbo
 * Sync unit test
 *
 * Revision 1.2  2005/05/12 15:15:41  ibbo
 * Updated
 *
 * Revision 1.1.1.1  2005/02/14 11:46:29  ibbo
 * Import
 *
 * Revision 1.3  2004/07/30 08:40:35  ibbo
 * Updated
 *
 * Revision 1.2  2004/07/23 15:07:27  ibbo
 * Updated
 *
 * Revision 1.1.1.1  2004/07/23 07:13:17  ibbo
 * Initial
 *
 * Revision 1.17  2003/06/24 09:34:04  imi
 * Updated
 *
 * Revision 1.16  2003/06/20 10:33:43  imi
 * Many fixes to searching code...
 *
 * Revision 1.15  2003/06/19 11:53:18  imi
 * Added getter for Current Transaction and Current Group to SearchResultEntryDTO, with
 * corresponding mods to backend
 *
 * Revision 1.14  2003/05/30 16:37:31  imi
 * Updated
 *
 * Revision 1.13  2003/05/30 13:58:06  imi
 * Minor fixes, service type not coming thru
 *
 * Revision 1.12  2003/05/28 11:04:48  imi
 * updated
 *
 * Revision 1.11  2003/05/22 12:48:58  imi
 * Fix
 *
 * Revision 1.10  2003/05/22 12:09:46  imi
 * Changed way search sessions are allocated and released.
 *
 * Revision 1.9  2003/05/20 14:36:34  imi
 * up
 *
 * Revision 1.8  2003/05/07 12:19:14  imi
 * Updated
 *
 * Revision 1.7  2003/05/06 16:56:21  imi
 * Bugfix when not correctly releasing session
 *
 * Revision 1.6  2003/05/06 14:47:50  imi
 * Additional work to fill out SearchResultEntryDTO structure
 *
 * Revision 1.5  2003/05/06 13:18:40  imi
 * Merge
 *
 * Revision 1.4  2003/05/06 13:04:50  imi
 * Updated
 *
 * Revision 1.3  2003/05/02 15:00:41  imi
 * Working
 *
 * Revision 1.2  2003/05/02 13:31:02  imi
 * Updated
 *
 * Revision 1.1  2003/05/02 11:55:17  imi
 * Step forward on returning search results
 *
 */

/**
 * Title:       InternalSearchInfo
 * @version:    $Id: InternalSearchInfo.java,v 1.3 2005/05/14 10:16:56 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class InternalSearchInfo
{
  private static Log log = LogFactory.getLog(InternalSearchInfo.class);
  private static int instance_counter = 0;

  public SearchResultDTO info = null;
  // public ScrollableResults results = null;
  public int handle = -1;
  // public int current_pos = -1;
  public List results;
  public Session session;

  public InternalSearchInfo(Session session,
                            SearchResultDTO info, 
                            List results, 
                            int handle)
  {
    log.debug("New InternalSearchInfo "+(++instance_counter));

    this.info = info;
    this.results = results;
    this.handle = handle;
    this.session = session;
  }

  public SearchResultEntryDTO getResultFor(int rownum) {
    SearchResultEntryDTO result = null;

    if ( results == null )
      throw new RuntimeException("No search results available");

    if ( rownum >= results.size() )
      throw new RuntimeException("Requesting record past end of results");

    Object[] row = null;
    try
    {
      row = (Object[]) results.get(rownum);

      if ( row == null )
        throw new RuntimeException("row must not be null");

      Long internal_id = (Long)row[0];
      long time = row[8] != null ? ((Date)row[8]).getTime() : 0;
      long unread_message_count = ( row[12] != null ? ((Long)row[12]).longValue() : -1 ) ;
      // int service_type = ((Integer)row[15]).intValue();
      Long current_trans_id  =(Long)row[15];

      result = new SearchResultEntryDTO( internal_id,
                             (String)row[1],
                             (String)row[2],
                             (String)row[3],
                             (String)row[4],
                             (String)row[5],
                             (String)row[6],
                             (String)row[7],
                             new java.util.Date(time),
                             (String)row[9],
                             (String)row[16],
                             (Boolean) row[10],
                             (String) row[11],
                             unread_message_count,
                             (String)row[13],
                             (String)row[14],
                             current_trans_id);

      log.debug("Returning request status code tg: "+row[11]+", trans:"+row[9]);
    }
    catch ( Exception e ) {

      log.warn("Problem",e);

      if ( row != null ) {
        for ( int i=0; i<row.length; i++ ) {
          if ( row[i] != null ) {
            log.debug("row["+i+"] = "+row[i]+" instanceof "+row[i].getClass().getName());
          }
          else {
            log.debug("row["+i+"] = "+row[i]);
          }
        }
      }
      else {
        log.debug("row==null");
      }

      throw new RuntimeException("problem processing search result",e);
    }

    return result;
  }

  public Session getSession()
  {
    return session;
  }

  protected void finalize()
  {
    log.debug("finalize: instance count="+(--instance_counter));
  }
}
