package com.k_int.openrequest.integration.iface;

import java.util.Hashtable;

/**
 * Title:       RequestView
 * @version:    $Id: RequestView.java,v 1.3 2005/06/09 13:43:25 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class RequestView implements java.io.Serializable
{
  public ILLRequestMessageDTO ill_request = null;
  public Hashtable additional_attrs = new Hashtable();             // Not yet added, backend to do
  
  public RequestView() {
  }

  public RequestView(ILLRequestMessageDTO ill_request, String request_no) {
    this.ill_request = ill_request;
  }

  public ILLRequestMessageDTO getILLRequest() {
    return ill_request;
  }

  public void setILLRequest(ILLRequestMessageDTO ill_request) {
    this.ill_request = ill_request;
  }

  public String toString() {
    if ( ill_request != null )
      return ""+ill_request.toString()+", "+additional_attrs.toString();
    else
      return "";
  }
}
