package com.k_int.openrequest.integration.iface;

/**
 * Title:       ProviderErrorReportDTO
 * @version:    $Id: StateTransitionProhibitedDTO.java,v 1.2 2005/06/10 16:26:06 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class StateTransitionProhibitedDTO extends ProviderErrorReportDTO implements java.io.Serializable {

  public long apdu_type;
  public long current_state;

  public StateTransitionProhibitedDTO(long apdu_type, long current_state) {
    this.apdu_type = apdu_type;
    this.current_state = current_state;
  }

  public long getAPDUType() {
    return apdu_type;
  }

  public long getCurrentState() {
    return current_state;
  }
}
