package com.k_int.openrequest.integration.iface;

import java.util.Hashtable;

/**
 * Title:       RequestManagerQueryDTO
 * @version:    $Id: RequestManagerQueryDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class RequestManagerQueryDTO implements java.io.Serializable {

    private java.lang.String location_id;
    private java.lang.String role;
    private java.lang.String readFlag;
    private java.lang.String phase;
    private java.lang.String state;
    private java.lang.String requestId;
    private java.lang.String author;
    private java.lang.String title;
    private java.lang.String identifier;
    private java.lang.String responder;
    private java.lang.String patronId;

    public RequestManagerQueryDTO() {
    }

    public RequestManagerQueryDTO(
           java.lang.String location_id,
           java.lang.String role,
           java.lang.String readFlag,
           java.lang.String phase,
           java.lang.String state,
           java.lang.String requestId,
           java.lang.String author,
           java.lang.String title,
           java.lang.String identifier,
           java.lang.String responder,
           java.lang.String patronId) {
           this.location_id = location_id;
           this.role = role;
           this.readFlag = readFlag;
           this.phase = phase;
           this.state = state;
           this.requestId = requestId;
           this.author = author;
           this.title = title;
           this.identifier = identifier;
           this.responder = responder;
           this.patronId = patronId;
    }

    public java.lang.String getLocationId() {
        return location_id;
    }

    public void setLocationId(String location_id) {
      this.location_id = location_id;
    }

    public java.lang.String getRole() {
        return role;
    }

    public void setRole(java.lang.String role) {
        this.role = role;
    }

    public java.lang.String getReadFlag() {
        return readFlag;
    }

    public void setReadFlag(java.lang.String readFlag) {
        this.readFlag = readFlag;
    }

    public java.lang.String getPhase() {
        return phase;
    }

    public void setPhase(java.lang.String phase) {
        this.phase = phase;
    }

    public java.lang.String getState() {
        return state;
    }

    public void setState(java.lang.String state) {
        this.state = state;
    }

    public java.lang.String getRequestId() {
        return requestId;
    }

    public void setRequestId(java.lang.String requestId) {
        this.requestId = requestId;
    }

    public java.lang.String getAuthor() {
        return author;
    }

    public void setAuthor(java.lang.String author) {
        this.author = author;
    }

    public java.lang.String getTitle() {
        return title;
    }

    public void setTitle(java.lang.String title) {
        this.title = title;
    }

    public java.lang.String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(java.lang.String identifier) {
        this.identifier = identifier;
    }

    public java.lang.String getResponder() {
        return responder;
    }

    public void setResponder(java.lang.String responder) {
        this.responder = responder;
    }

    public java.lang.String getPatronId() {
        return patronId;
    }

    public void setPatronId(java.lang.String patronId) {
        this.patronId = patronId;
    }
}
