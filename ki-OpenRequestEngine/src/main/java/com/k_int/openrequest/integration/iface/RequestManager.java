package com.k_int.openrequest.integration.iface;

/**
 * Title:       RequestManager
 * @version:    $Id: RequestManager.java,v 1.8 2005/04/25 20:34:31 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson (ian.ibbotson@k-int.com)
 * Company:     Knowledge Integration Ltd.
 * Description: Generic interface used by LMS Vendors to communicate with an instance of the OpenRequest Engine
 *
 * Outstanding Issues
 *              1) Use of  naming convention
 */
public interface RequestManager {
  /**
   * Report implementation version
   */
  public String getVersion() throws RequestManagerException;

  /** Create a new transaction group with the supplied details and rota.
   * @param request Generic details about the item required.
   * @param rota List of locations to which this request may be sent.
   * @return A transaction group id for the new transaction.A
   */
  public TransactionGroupIdDTO createTransGroup(RequesterViewDTO request, 
                                                RotaElementDTO[] rota) throws RequestManagerException, DuplicateUserReferenceException;

  /** Create a new transaction group with the supplied details and rota.
   * @param request Generic details about the item required.
   * @param rota List of locations to which this request may be sent.
   * @param mandatory_tgq The TGQ that must be used (Must be unique)
   * @return A transaction group id for the new transaction.A
   */
  public TransactionGroupIdDTO createTransGroup(RequesterViewDTO request, 
                                                RotaElementDTO[] rota,
                                                String mandatory_tgq) throws RequestManagerException, DuplicateUserReferenceException;

  /** Start the ILL Messaging process for the identified group
   * @param id ID of group to start
   */
  public void startMessaging(TransactionGroupIdDTO id) throws RequestManagerException;

  /** Start the ILL Messaging process for the identified group and return the next ILL Transaction ID
   * @param id ID of group to start
   * @return id of new ILL transaction
   */
  public Long startMessagingSync(TransactionGroupIdDTO id) throws RequestManagerException;


  /** Retrieve information about the idenfied TransGroupId
   * @param id Id of the transaction we want to retrieve
   * @return a transaction group value object containing information about that transaction
   */
  public TransGroupInfoDTO retrieveTransGroup(TransactionGroupIdDTO id) throws RequestManagerException;

  /** Retrieve information about the idenfied TransId
   * @param id Id of the transaction we want to retrieve
   * @return a transaction value object containing information about that transaction
   */
  public TransactionInfoDTO retrieveTransaction(TransactionIdDTO id) throws RequestManagerException;

  /** Retrieve brief information about the message history of a transaction
   * @param id Id of the transaction we want to retrieve messages for
   * @return Array of brief message value objects
   */
  public ProtocolMessageDTO[] retrieveMessageHistory(TransactionIdDTO id) throws RequestManagerException;

  /** Retrieve brief information about the message history of a transaction group
   * @param id Id of the transaction we want to retrieve messages for
   * @return Array of brief message value objects
   */
  public ProtocolMessageDTO[] retrieveMessageHistory(TransactionGroupIdDTO id) throws RequestManagerException;

  /**
   *
   */
  public void updateRequest(TransactionGroupIdDTO id, RequesterViewDTO request) throws RequestManagerException;

  /**
   *
   */
  public void updateRequest(TransactionGroupIdDTO id, ItemIdDTO item_data, ResponderSpecificInformationDTO request) throws RequestManagerException;

  /**
   *
   */
  public void replaceUnusedRota(TransactionGroupIdDTO id, RotaElementDTO[] new_rota) throws RequestManagerException;

  /**
   *
   */
  public ProtocolMessageDTO retrieveMessage(MessageIdDTO id) throws RequestManagerException;

  /**
   *
   */
  public void sendMessage(TransactionIdDTO transaction_id, ProtocolMessageDTO message) throws RequestManagerException;

  /**
   *  Repeat the last service message
   */
  public void repeatLastService(TransactionIdDTO transaction_id) throws RequestManagerException;

  /**
   *
   */
  public void acknowledgeMessage(MessageIdDTO message_id) throws RequestManagerException;
 
  /**
   *
   */
  public void acknowledgeAllMessages(TransactionGroupIdDTO transaction_id) throws RequestManagerException;
  
  /**
   *
   */
  public void closeTransGroup(TransactionGroupIdDTO transaction_id, int reason) throws RequestManagerException;
  
  /**
   *
   */
  public void closeTransGroup(TransactionGroupIdDTO transaction_id) throws RequestManagerException;
  
  /**
   *
   */
  public void changeCloseReason(TransactionGroupIdDTO transaction_id, int reason) throws RequestManagerException;
  
  /**
   *
   */
  public RequestCountDTO[] retrieveRequestCounts() throws RequestManagerException;
  
  /**
   * TODO: rename search to searchQuery
   * TODO: a State/Phase that represents the non-created transaction period(after initial request creation)
   */
  public SearchResultDTO searchQuery(RequestManagerQueryDTO query) throws RequestManagerException;
  
  /**
   *
   */
  public SearchResultDTO searchCategory(String category) throws RequestManagerException;
  
  /**
   *
   */
  public ResultSetDTO retrieveResultSet(SearchResultDTO result, long count, int start) throws RequestManagerException;
  
  /**
   *
   */
  public void releaseSearchResult(SearchResultDTO result) throws RequestManagerException;

  public void release() throws RequestManagerException;

  /**
   * We need to expose toString so that remote clients can ask the remote object for it's name
   */
  public String toString();
}
