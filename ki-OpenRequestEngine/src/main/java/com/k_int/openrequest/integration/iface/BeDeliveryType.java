package com.k_int.openrequest.integration.iface;

public enum BeDeliveryType {

  PHYSICAL,
  ELECTRONIC;

  public String toString() {
    switch ( this ) {
      case PHYSICAL:   return "Physical";
      case ELECTRONIC: return "Electronic";
      default:             return "Unknown";
    }
  }

  public int  toIso() {
    switch ( this ) {
      case PHYSICAL:   return 1;
      case ELECTRONIC: return 2;
      default:             return 0;
    }
  }

  public static BeDeliveryType fromIso(int i) {
    switch ( i ) {
      case 1: return PHYSICAL;
      case 2: return ELECTRONIC;
      default: return null;
    }
  }
}
