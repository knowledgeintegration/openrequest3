package com.k_int.openrequest.integration.iface;

/**
 * Title:       BeItemIdVO
 * @version:    $Id: ItemIdDTO.java,v 1.4 2005/06/29 11:21:13 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ItemIdDTO implements java.io.Serializable
{
  public int item_type;
  public int held_medium_type;
  public String title;
  public String subtitle;
  public String author;
  public String sponsoring_body;
  public String publisher;
  public String place_of_publication;
  public String publication_date;
  public String isbn;
  public String issn;
  public String title_of_article;
  public String author_of_article;
  public String volume_issue;
  public String publication_date_of_component;
  public String pagination;
  public com.k_int.openrequest.db.IPIGSystemNumber system_no;
  public String call_number;
  public com.k_int.openrequest.db.IPIGSystemNumber nat_bib_no;
  public String series_title_number;
  public String verification_reference_source;
  public String edition;

  public ItemIdDTO() {
  }

  public ItemIdDTO(ItemTypeExtended item_type,
                   MediumType held_medium_type,
                   String title,
                   String subtitle,
                   String author,
                   String sponsoring_body,
                   String publisher,
                   String place_of_publication,
                   String publication_date,
                   String isbn,
                   String issn,
                   String title_of_article,
                   String author_of_article,
                   String volume_issue,
                   String publication_date_of_component,
                   String pagination,
                   com.k_int.openrequest.db.IPIGSystemNumber system_no,
                   String call_number,
                   com.k_int.openrequest.db.IPIGSystemNumber nat_bib_no,
                   String series_title_number,
                   String verification_reference_source,
                   String edition)
  {
    this.item_type = item_type != null ? item_type.toIso() : 0;
    this.held_medium_type = held_medium_type != null ? held_medium_type.toIso() : 0;
    this.title = title;
    this.subtitle = subtitle;
    this.author = author;
    this.sponsoring_body = sponsoring_body;
    this.publisher = publisher;
    this.place_of_publication = place_of_publication;
    this.publication_date = publication_date;
    this.isbn = isbn;
    this.issn = issn;
    this.title_of_article = title_of_article;
    this.author_of_article = author_of_article;
    this.volume_issue = volume_issue;
    this.publication_date_of_component = publication_date_of_component;
    this.pagination = pagination;
    this.system_no = system_no;
    this.call_number = call_number;
    this.nat_bib_no = nat_bib_no;
    this.series_title_number = series_title_number;
    this.verification_reference_source = verification_reference_source;
    this.edition = edition;
  }

  public int getItemType() {
    return item_type;
  }

  public int getHeldMediumType() {
    return held_medium_type;
  }

  public String getCallNumber() {
    return call_number;
  }

  public void setCallNumber(String call_number) {
    this.call_number = call_number;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubTitle() {
    return subtitle;
  }

  public void setSubTitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public String getSponsoringBody() {
    return sponsoring_body;
  }

  public void setSponsoringBody(String sponsoring_body) {
    this.sponsoring_body = sponsoring_body;
  }

  public String getPlaceOfPublication() {
    return place_of_publication;
  }

  public void setPlaceOfPublication(String place_of_publication) {
    this.place_of_publication = place_of_publication;
  }

  public String getPublisher() {
    return publisher;
  }

  public void setPublisher(String publisher) {
    this.publisher = publisher;
  }

  public String getSeriesTitleNumber() {
    return series_title_number;
  }

  public void setSeriesTitleNumber(String series_title_number) {
    this.series_title_number = series_title_number;
  }

  public String getVolumeIssue() {
    return volume_issue;
  }

  public void setVolumeIssue(String volume_issue) {
    this.volume_issue = volume_issue;
  }

  public String getEdition() {
    return edition;
  }

  public void setEdition(String edition) {
    this.edition = edition;
  }

  public String getPublicationDate() {
    return publication_date;
  }

  public void setPublicationDate(String publication_date) {
    this.publication_date = publication_date;
  }

  public String getPublicationDateOfComp() {
    return publication_date_of_component;
  }

  public void setPublicationDateOfComp(String publication_date_of_component) {
    this.publication_date_of_component = publication_date_of_component;
  }

  public String getAuthorOfArticle() {
    return author_of_article;
  }

  public void setAuthorOfArticle(String author_of_article) {
    this.author_of_article = author_of_article;
  }

  public String getTitleOfArticle() {
    return title_of_article;
  }

  public void setTitleOfArticle(String title_of_article) {
    this.title_of_article = title_of_article;
  }

  public String getPagination() {
    return pagination;
  }

  public void setPagination(String pagination) {
    this.pagination = pagination;
  }

  public com.k_int.openrequest.db.IPIGSystemNumber getNationalBibliographyNo() {
    return nat_bib_no;
  }

  public void setNationalBibliographyNo(com.k_int.openrequest.db.IPIGSystemNumber nat_bib_no) {
    this.nat_bib_no = nat_bib_no;
  }

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public String getIssn() {
    return issn;
  }

  public void setIssn(String issn) {
    this.issn = issn;
  }

  public com.k_int.openrequest.db.IPIGSystemNumber getSystemNo() {
    return system_no;
  }

  public void setSystemNo(com.k_int.openrequest.db.IPIGSystemNumber system_no) {
    this.system_no = system_no;
  }

  public String getVerificationReferenceSource() {
    return verification_reference_source;
  }

  public void setVerificationReferenceSource(String verification_reference_source) {
    this.verification_reference_source = verification_reference_source;
  }
}
