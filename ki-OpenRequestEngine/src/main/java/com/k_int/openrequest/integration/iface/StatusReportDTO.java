package com.k_int.openrequest.integration.iface;

/**
 * Title:       StatusReportDTO
 * @version:    $Id: StatusReportDTO.java,v 1.2 2005/03/08 10:20:49 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class StatusReportDTO implements java.io.Serializable {

  /** MANDATORY */
  HistoryReportDTO user_status_report = null;

  /** MANDATORY */
  State provider_status_report = null;

  public StatusReportDTO() {
  }

  public StatusReportDTO(HistoryReportDTO user_status_report,
                         State provider_status_report) {
    this.user_status_report = user_status_report;
    this.provider_status_report = provider_status_report;
  }

  public State getProviderStatusReport() {
    return provider_status_report;
  }

  public void setProviderStatusReport(State provider_status_report) {
    this.provider_status_report = provider_status_report;
  }

  public HistoryReportDTO getUserStatusReport() {
    return user_status_report;
  }

  public void setUserStatusReport(HistoryReportDTO user_status_report) {
    this.user_status_report = user_status_report;
  }
}
