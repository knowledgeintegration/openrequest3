package com.k_int.openrequest.integration.iface;

public enum PlaceOnHoldType {

  YES, NO, PREF;

  public String toString() {
    switch ( this ) {
      case YES: return "Yes";
      case NO: return "No";
      case PREF: return "Responder Preference";
      default: return "None";
    }
  }

  public int toIso() {
    switch ( this ) {
      case YES: return 1;
      case NO: return 2;
      case PREF: return 3;
      default: return 0;
    }
  }


  public static PlaceOnHoldType fromIso(int iso){
    switch ( iso ) {
      case 1: return YES;
      case 2: return NO;
      case 3: return PREF;
      default: return null;
    }
  }

}
