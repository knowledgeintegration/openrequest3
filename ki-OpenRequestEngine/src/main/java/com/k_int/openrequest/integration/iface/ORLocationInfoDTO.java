package com.k_int.openrequest.integration.iface;


/**
 * Title:       ORLocationInfoDTO
 * Copyright:   Copyright (C) 2003- Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ORLocationInfoDTO implements java.io.Serializable {

  private String location_name = null;
  private String symbol = null;
  private String address_type = null;
  private String address = null;

  public ORLocationInfoDTO() {
  }

  public ORLocationInfoDTO(String location_name,
                           String symbol,
                           String address_type,
                           String address) {
    this.location_name = location_name;
    this.symbol = symbol;
    this.address_type = address_type;
    this.address = address;
  }

  public String getLocationName() {
    return location_name;
  }

  public void setLocationName(String location_name) {
    this.location_name = location_name;
  }

  public java.lang.String getSymbol() {
    return symbol;
  }

  public void setSymbol(java.lang.String symbol) {
    this.symbol = symbol;
  }

  public java.lang.String getAddressType() {
    return address_type;
  }

  public void setAddressType(java.lang.String addressType) {
    this.address_type = address_type;
  }

  public java.lang.String getAddress() {
    return address;
  }

  public void setAddress(java.lang.String address) {
    this.address = address;
  }
}
