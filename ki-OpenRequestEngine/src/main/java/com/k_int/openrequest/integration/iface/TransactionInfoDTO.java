package com.k_int.openrequest.integration.iface;

/**
 * Title:       Transaction
 * @version:    $Id: TransactionInfoDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class TransactionInfoDTO implements java.io.Serializable
{
  private TransactionIdDTO trans_id;
  private int state_code;
  private String responder_id;
  private String requester_id;
  private boolean is_read;

  public TransactionInfoDTO()
  {
  }

  public TransactionInfoDTO(TransactionIdDTO trans_id,
                       int state_code,
                       String requester_id,
                       String responder_id,
                       boolean is_read)
  {
    this.trans_id = trans_id;
    this.state_code = state_code;
    this.requester_id = requester_id;
    this.responder_id = responder_id;
    this.is_read = is_read;

  }

  public TransactionIdDTO getId()
  {
    return trans_id;
  }

  public State getState()
  {
    return State.fromIso(state_code);
  }

  public String getResponderId()
  {
    return responder_id;
  }

  public String getRequesterId()
  {
    return requester_id;
  }

  public boolean isRead()
  {
    return is_read;
  }
}
