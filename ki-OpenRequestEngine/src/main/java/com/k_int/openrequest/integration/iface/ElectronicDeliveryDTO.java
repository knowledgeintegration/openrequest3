package com.k_int.openrequest.integration.iface;

/**
 * Title:       ElectronicDeliveryDTO
 * @version:    $Id: ElectronicDeliveryDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ElectronicDeliveryDTO extends DeliveryServiceDTO implements java.io.Serializable
{
  private String telecom_service_id;
  private String telecom_service_address;

  public ElectronicDeliveryDTO(BeTelecomServiceId telecom_service_id, 
                              String telecom_service_address)
  {
    super(BeDeliveryType.ELECTRONIC);
    this.telecom_service_id = telecom_service_id.toIso();
    this.telecom_service_address = telecom_service_address;
  }

  public BeTelecomServiceId getTelecomServiceId()
  {
    return BeTelecomServiceId.fromIso(telecom_service_id);
  }

  public void setTelecomServiceId(BeTelecomServiceId telecom_service_id)
  {
    this.telecom_service_id = telecom_service_id.toIso();
  }

  public String getTelecomServiceAddress()
  {
    return telecom_service_address;
  }

  public void setTelecomServiceAddress(String telecom_service_address)
  {
    this.telecom_service_address = telecom_service_address;
  }
}
