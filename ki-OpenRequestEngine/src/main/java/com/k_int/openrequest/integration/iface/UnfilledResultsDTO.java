package com.k_int.openrequest.integration.iface;

import java.util.Date;
import java.util.List;

/**
 * Title:       UnfilledResultsDTO
 * @version:    $Id: UnfilledResultsDTO.java,v 1.2 2005/06/10 18:42:50 ibbo Exp $
 * Copyright:   Copyright (C) 2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class UnfilledResultsDTO extends ResultsExplanationDTO implements java.io.Serializable {

  public long reason_unfilled;
  public List locations = null;

  public UnfilledResultsDTO() {
  }

  public UnfilledResultsDTO(long reason_unfilled, List locations) {
    this.reason_unfilled = reason_unfilled;
    this.locations = locations;
  }

  public long getReasonUnfilled() {
    return reason_unfilled;
  }

  public void setReasonUnfulled(long reason_unfilled) {
    this.reason_unfilled = reason_unfilled;
  }

  public List getLocations() {
    return locations;
  }

  public void seLocations(List locations) {
    this.locations = locations;
  }


}
