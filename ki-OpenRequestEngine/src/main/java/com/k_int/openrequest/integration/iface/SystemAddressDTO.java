package com.k_int.openrequest.integration.iface;

/**
 * Title:       SystemAddressDTO
 * @version:    $Id: SystemAddressDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class SystemAddressDTO implements java.io.Serializable
{
  private String telecom_service_id;
  private String telecom_service_address;

  public SystemAddressDTO(String telecom_service_id, String telecom_service_address)
  {
    this.telecom_service_id = telecom_service_id;
    this.telecom_service_address = telecom_service_address;
  }

  public String getTelecomServiceId()
  {
    return telecom_service_id;
  }

  public String getTelecomServiceAddress()
  {
    return telecom_service_address;
  }
}
