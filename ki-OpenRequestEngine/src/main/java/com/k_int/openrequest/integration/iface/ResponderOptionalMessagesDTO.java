package com.k_int.openrequest.integration.iface;
import java.util.Hashtable;

/**
 * Title:       ResponderOptionalMessagesDTO
 * @version:    $Id: ResponderOptionalMessagesDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ResponderOptionalMessagesDTO implements java.io.Serializable
{
  private boolean can_send_shipped;
  private boolean can_send_checked_in;
  private int responder_received;
  private int responder_returned;

  public ResponderOptionalMessagesDTO()
  {
  }

  public ResponderOptionalMessagesDTO(boolean can_send_shipped,
                      boolean can_send_checked_in,
                      MessageOption responder_received,
                      MessageOption responder_returned)
  {
    this.can_send_shipped = can_send_shipped;
    this.can_send_checked_in = can_send_checked_in;
    this.responder_received = responder_received.toIso();
    this.responder_returned = responder_returned.toIso();
  }

  public boolean canSendShipped()
  {
    return can_send_shipped;
  }

  public boolean canSendCheckedIn()
  {
    return can_send_checked_in;
  }

  public MessageOption getResponderReceived()
  {
    return MessageOption.fromIso(responder_received);
  }

  public MessageOption getResponderReturned()
  {
    return MessageOption.fromIso(responder_returned);
  }
}
