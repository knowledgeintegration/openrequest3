package com.k_int.openrequest.integration.iface;

public enum ItemTypeExtended {

  UNKNOWN, MONO, SERIAL, OTHERS, NON_FICTION, FICTION, PLAYSETS, MUSIC, ALTERNATIVE;

  public String toString() {
    switch (this) {
      case UNKNOWN: return "Unknown";
      case MONO: return "Mono";
      case SERIAL: return "Serial";
      case OTHERS: return "Others";
      case NON_FICTION: return "Monograph: Non Fiction";
      case FICTION: return "Monograph: Fiction";
      case PLAYSETS: return "Monograph: Playsets";
      case MUSIC: return "Monograph: Music Sets/Orchestral Parts, etc";
      case ALTERNATIVE: return "Monograph: Alternative Format Materials";
      default: return "None";
    } 
  }

  public int toIso() {
    switch (this) {
      case UNKNOWN: return 0;
      case MONO: return 1;
      case SERIAL: return 2;
      case OTHERS: return 3;
      case NON_FICTION: return 4;
      case FICTION: return 5;
      case PLAYSETS: return 6;
      case MUSIC: return 7;
      case ALTERNATIVE: return 8;
      default: return 0;
    }
  }

  public static ItemTypeExtended fromIso(int iso) {
    switch ( iso ) {
      case 0: return UNKNOWN;
      case 1: return MONO;
      case 2: return SERIAL;
      case 3: return OTHERS;
      case 4: return NON_FICTION;
      case 5: return FICTION;
      case 6: return PLAYSETS;
      case 7: return MUSIC;
      default: return UNKNOWN;
    }
  }
}
