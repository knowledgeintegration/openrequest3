package com.k_int.openrequest.integration.iface;

import java.util.Date;
import java.util.List;

/**
 * Title:       ConditionalResultsDTO
 * @version:    $Id: ConditionalResultsDTO.java,v 1.3 2005/06/11 12:30:44 ibbo Exp $
 * Copyright:   Copyright (C) 2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ConditionalResultsDTO extends ResultsExplanationDTO implements java.io.Serializable {

  public int conditions;
  public Date date_for_reply;
  public List locations;
  public DeliveryServiceDTO proposed_delivery_service;

  public ConditionalResultsDTO() {
  }

  public ConditionalResultsDTO(int conditions,
                               Date date_for_reply,
                               List locations,
                               DeliveryServiceDTO proposed_delivery_service) {
    this.conditions = conditions;
    this.date_for_reply = date_for_reply;
    this.locations = locations;
    this.proposed_delivery_service = proposed_delivery_service;
  }

  public int getConditions() {
    return conditions;
  }

  public void setConditions(int conditions) {
    this.conditions = conditions;
  }

  public Date getDateForReply() {
    return date_for_reply;
  }

  public void setDateForReply(Date date_for_reply) {
    this.date_for_reply = date_for_reply;
  }

  public List getLocations() {
    return locations;
  }

  public void setLocations(List locations) {
    this.locations = locations;
  }

  public DeliveryServiceDTO getProposedDeliveryService() {
    return proposed_delivery_service;
  }

  public void setProposedDeliveryService(DeliveryServiceDTO proposed_delivery_service) {
    this.proposed_delivery_service = proposed_delivery_service;
  }

}
