package com.k_int.openrequest.integration.iface;

public enum ShippedCondition {

  NONE, LIBRARY_USE_ONLY, NO_REPRODUCTION, CLIENT_SIGNATURE_REQUIRED, SUPERVISION_REQUIRED, OTHER;

  public String toString() {
    switch ( this ) {
      case NONE: return "None";
      case LIBRARY_USE_ONLY: return "Library-Use Only";
      case NO_REPRODUCTION: return "No Reproduction";
      case CLIENT_SIGNATURE_REQUIRED: return "Client Signature Required";
      case SUPERVISION_REQUIRED: return "Special Collections Supervision Required";
      case OTHER: return "Other";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case NONE: return 0;
      case LIBRARY_USE_ONLY: return 22;
      case NO_REPRODUCTION: return 23;
      case CLIENT_SIGNATURE_REQUIRED: return 24;
      case SUPERVISION_REQUIRED: return 25;
      case OTHER: return 27;
      default: return 0;
    }
  }


  public static ShippedCondition fromIso(int iso){
    switch ( iso ) {
      case 0: return NONE;
      case 22: return LIBRARY_USE_ONLY;
      case 23: return NO_REPRODUCTION;
      case 24: return CLIENT_SIGNATURE_REQUIRED;
      case 25: return SUPERVISION_REQUIRED;
      case 27: return OTHER;
      default: return null;
    }
  }
}
