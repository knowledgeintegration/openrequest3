package com.k_int.openrequest.integration.iface;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public enum BeTransportationMode {

  HAYS, POSTAL;

  public String toString()  {
    switch ( this ) {
      case HAYS: return "HAYS";
      case POSTAL: return "Postal";
      default: return "Unknown";
    }
  }

  public String toIso() {
    switch ( this ) {
      case HAYS: return "Hays";
      case POSTAL: return "Postal";
      default: return "Unknown";
    }
  }


  public static BeTransportationMode fromIso(String iso) {
    if ( iso != null ) {
      if ( iso.equals("Hays") ) return HAYS;
      if ( iso.equals("Postal") ) return POSTAL;
    }
    return null;
  }
}
