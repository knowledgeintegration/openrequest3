package com.k_int.openrequest.integration.iface;

/**
 * Title:       ErrorReportDTO
 * @version:    $Id: ErrorReportDTO.java,v 1.2 2005/06/10 14:34:11 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ErrorReportDTO implements java.io.Serializable {

  public String correlation_information = null;
  public Long report_source = null;
  public UserErrorReportDTO user_error_report = null;  
  public ProviderErrorReportDTO provider_error_report = null;  

  public ErrorReportDTO() {
  }

  public ErrorReportDTO( String correlation_information,
                        Long report_source,
                        UserErrorReportDTO user_error_report,
                        ProviderErrorReportDTO provider_error_report) {
    this.correlation_information = correlation_information;
    this.report_source = report_source;
    this.user_error_report = user_error_report;
    this.provider_error_report = provider_error_report;
  }

}
