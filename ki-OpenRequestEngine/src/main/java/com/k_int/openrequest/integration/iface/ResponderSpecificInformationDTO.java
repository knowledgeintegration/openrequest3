package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       ResponderSpecificInformationDTO
 * @version:    $Id: ResponderSpecificInformationDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ResponderSpecificInformationDTO implements java.io.Serializable
{
  private String accession_number;
  private String shelfmark;
  private String local_notes;
  private String private_note;

  public ResponderSpecificInformationDTO(String accession_number,
                             String shelfmark,
                             String local_notes,
                             String private_note)
  {
    this.accession_number = accession_number;
    this.shelfmark = shelfmark;
    this.local_notes = local_notes;
    this.private_note = private_note;
  }

  public String getAccessionNumber()
  {
    return accession_number;
  }

  public String getShelfmark()
  {
    return shelfmark;
  }

  public String getLocalNotes()
  {
    return local_notes;
  }

  public String getPrivateNote()
  {
    return private_note;
  }

}
