package com.k_int.openrequest.integration.iface;

import java.util.Hashtable;
import java.util.List;

/**
 * Title:       BeRequest
 * @version:    $Id: ILLRequestMessageDTO.java,v 1.8 2005/06/17 18:52:15 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ILLRequestMessageDTO extends ProtocolMessageDTO implements java.io.Serializable {

  public int transaction_type;
  public PostalAddressDTO delivery_address;
  public DeliveryServiceDTO delivery_service;
  public PostalAddressDTO billing_address;
  public List requested_service_type;
  public RequesterOptionalMessagesDTO req_opt_messages;
  public SearchTypeDTO search_type;
  public int[] required_mediums;
  public int place_on_hold;
  public ItemIdDTO item_id;
  public CostInfoTypeDTO cost_info;
  public boolean forward_flag;
  public String requester_note;
  public String copyright_compliance;
  public String requester_ref_authority;
  public String requester_ref;
  public String responder_ref_authority;
  public String responder_ref;
  public String client_id;
  
  public ILLRequestMessageDTO() {
    this.message_type = MessageType.ILL_REQUEST.toIso();
  }

  public ILLRequestMessageDTO(TransactionType transaction_type,
                     PostalAddressDTO delivery_address,
                     DeliveryServiceDTO delivery_service,
                     PostalAddressDTO billing_address,
                     List requested_service_type, // ServiceType service_type,
                     RequesterOptionalMessagesDTO req_opt_messages,
                     SearchTypeDTO search_type,
                     SupplyMediumType[] required_mediums,
                     PlaceOnHoldType place_on_hold,
                     ItemIdDTO item_id,
                     CostInfoTypeDTO cost_info,
                     boolean forward_flag,
                     String requester_note,
                     String copyright_compliance,
                     String requester_ref_authority,
                     String requester_ref,
                     String client_id) {

    this.message_type = MessageType.ILL_REQUEST.toIso();

    if ( transaction_type != null )
      this.transaction_type = transaction_type.toIso();

    this.delivery_address = delivery_address;
    this.delivery_service = delivery_service;
    this.billing_address = billing_address;
    this.requested_service_type = requested_service_type;
    this.req_opt_messages = req_opt_messages;
    this.search_type = search_type;
    if ( required_mediums != null ) {
      this.required_mediums = new int[required_mediums.length];
      for ( int i = 0; i < required_mediums.length; i++ ) {
        if ( required_mediums[i] != null ) {
          this.required_mediums[i] = required_mediums[i].toIso();
        }
        else {
          throw new RuntimeException("There is a null required mediums entry");
        }
      }
    }
    if ( place_on_hold != null )
      this.place_on_hold = place_on_hold.toIso();
    this.item_id = item_id;
    this.cost_info = cost_info;
    this.forward_flag = forward_flag;
    this.requester_note = requester_note;
    this.requester_ref_authority = requester_ref_authority;
    this.requester_ref = requester_ref;
    this.client_id = client_id;
  }

  public TransactionType getTransactionType() {
    return TransactionType.fromIso(transaction_type);
  }

  public void setTransactionType(TransactionType transaction_type) {
    this.transaction_type = transaction_type.toIso();
  }

  public PostalAddressDTO getDeliveryAddress() {
    return delivery_address;
  }

  public void setDeliveryAddress(PostalAddressDTO delivery_address) {
    this.delivery_address=delivery_address;
  }

  public DeliveryServiceDTO getDeliveryService() {
    return delivery_service;
  }

  public void setBeDeliveryService(DeliveryServiceDTO delivery_service) {
    this.delivery_service = delivery_service;
  }

  public PostalAddressDTO getBillingAddress() {
    return delivery_address;
  }

  public void setBillingAddress(PostalAddressDTO billing_address) {
    this.billing_address=billing_address;
  }

  public List getRequestedServiceType() {
    return requested_service_type;
  }

  public void setRequestedServiceType(List requested_service_type) {
    this.requested_service_type = requested_service_type;
  }

  public RequesterOptionalMessagesDTO getReqOptMessages() {
    return req_opt_messages;
  }

  public void setReqOptMessages(RequesterOptionalMessagesDTO req_opt_messages) {
    this.req_opt_messages = req_opt_messages;
  }

  public SearchTypeDTO getSearchType() {
    return search_type;
  }

  public void setSearchType(SearchTypeDTO search_type) {
    this.search_type = search_type;
  }

  public SupplyMediumType[] getRequiredMediums() {
    SupplyMediumType[] result = new SupplyMediumType[required_mediums.length];

    for ( int i = 0; i < required_mediums.length; i++ )
      result[i] = SupplyMediumType.fromIso(required_mediums[i]);

    return result;
  }

  public void setRequiredMediums(SupplyMediumType[] required_mediums) {
    this.required_mediums = new int[required_mediums.length];
    for ( int i = 0; i < required_mediums.length; i++ )
      this.required_mediums[i] = required_mediums[i].toIso();
  }

  public PlaceOnHoldType getPlaceOnHoldType() {
    return PlaceOnHoldType.fromIso(place_on_hold);
  }

  public void setPlaceOnHoldType(PlaceOnHoldType place_on_hold_type) {
    this.place_on_hold=place_on_hold_type.toIso();
  }

  public ItemIdDTO getItemId() {
    return item_id;
  }

  public void setItemId(ItemIdDTO item_id) {
    this.item_id = item_id;
  }

  public CostInfoTypeDTO getCostInfoType() {
    return cost_info;
  }

  public void setCostInfoType(CostInfoTypeDTO cost_info) {
    this.cost_info = cost_info;
  }

  public String getRequesterNote() {
    return requester_note;
  }

  public void setRequesterNote(String requester_note) {
    this.requester_note = requester_note;
  }

  public String getCopyrightCompliance() {
    return copyright_compliance;
  }

  public void setCopyrightCompliance(String copyright_compliance) {
    this.copyright_compliance = copyright_compliance;
  }

  public String getRequesterRef() {
    return requester_ref;
  }

  public void setRequesterRef(String requester_ref) {
    this.requester_ref = requester_ref;
  }

  public String getResponderRef() {
    return responder_ref;
  }

  public void setResponderRef(String responder_ref) {
    this.responder_ref = responder_ref;
  }

  public String getClientId() {
    return client_id;
  }

  public void setClientId(String client_id) {
    this.client_id = client_id;
  }

  public String getRequesterRefAuthority() {
    return requester_ref_authority;
  }

  public void setRequesterRefAuthority(String requester_ref_authority) {
    this.requester_ref_authority = requester_ref_authority;
  }

  public String getResponderRefAuthority() {
    return responder_ref_authority;
  }

  public void setResponderRefAuthority(String responder_ref_authority) {
    this.responder_ref_authority = responder_ref_authority;
  }

  public String toString() {
    return "note "+requester_note+"\n copy "+copyright_compliance+" auth "+ requester_ref_authority+" ref "+requester_ref+" client "+client_id+".";
  }
}
