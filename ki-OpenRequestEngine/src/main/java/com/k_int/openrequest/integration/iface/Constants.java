package com.k_int.openrequest.integration.iface;

import java.util.Hashtable;
import java.util.Date;
import java.io.StringWriter;

/**
 * Title:       Constants
 * @version:    $Id: Constants.java,v 1.2 2005/07/02 16:41:35 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: This class maps from the integration api enum int values into OpenRequest internal
 *              string enum values (The ones used in the database).
 */
public class Constants
{
  public static final Hashtable service_protocol_conversion_table = new Hashtable();
  public static final Hashtable state_conversion_table = new Hashtable();
  public static final Hashtable reverse_state_conversion_table = new Hashtable();
  public static final Hashtable phase_conversion_table = new Hashtable();
  public static final Hashtable reverse_phase_conversion_table = new Hashtable();
  public static final Hashtable role_conversion_table = new Hashtable();
  public static final Hashtable message_type_conversion_table = new Hashtable();

  static
  {
    service_protocol_conversion_table.put("ISO10161v1",new Integer(1));
    service_protocol_conversion_table.put("ISO10161v2",new Integer(1));
    service_protocol_conversion_table.put("ISO10161v3",new Integer(1));
    service_protocol_conversion_table.put("SimpleEmail",new Integer(2));

    registerPair(state_conversion_table,reverse_state_conversion_table,"IDLE", new Integer(0) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"PENDING", new Integer(2) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"NOTSUPPLIED", new Integer(1) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"CONDITIONAL", new Integer(5) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"CANCELPENDING", new Integer(6) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"CANCELLED", new Integer(7) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"SHIPPED", new Integer(8) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"RECEIVED", new Integer(9) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"RENEWPENDING", new Integer(10) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"RENEWOVERDUE", new Integer(12) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"OVERDUE", new Integer(13) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"NOTREC/OVERDUE", new Integer(11) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"RECALL", new Integer(16) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"RETURNED", new Integer(14) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"LOST", new Integer(17) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"IN-PROCESS", new Integer(3) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"FORWARD", new Integer(4) );
    registerPair(state_conversion_table,reverse_state_conversion_table,"CHECKEDIN", new Integer(15) );

    message_type_conversion_table.put("ISOREQUEST", new Integer( 1 ) );
    message_type_conversion_table.put("ISOFWD", new Integer( 2 ) );
    message_type_conversion_table.put("ISOSHIPPED", new Integer( 3 ) );
    message_type_conversion_table.put("ISOANSWER", new Integer( 4 ) );
    message_type_conversion_table.put("ISOCOND", new Integer( 5 ) );
    message_type_conversion_table.put("ISOCANCEL", new Integer( 6 ) );
    message_type_conversion_table.put("ISOCANREP", new Integer( 7 ) );
    message_type_conversion_table.put("ISOREC", new Integer( 8 ) );
    message_type_conversion_table.put("ISORECALL", new Integer( 9 ) );
    message_type_conversion_table.put("ISORET", new Integer( 10 ) );
    message_type_conversion_table.put("ISOCHECKIN", new Integer( 11 ) );
    message_type_conversion_table.put("ISOOVERDUE", new Integer( 12 ) );
    message_type_conversion_table.put("ISORENEW", new Integer( 13 ) );
    message_type_conversion_table.put("ISORENEWANS", new Integer( 14 ) );
    message_type_conversion_table.put("ISOLOST", new Integer( 15 ) );
    message_type_conversion_table.put("ISODAMAGED", new Integer( 16 ) );
    message_type_conversion_table.put("ISOMESSAGE", new Integer( 17 ) );
    message_type_conversion_table.put("ISOSTATQRY", new Integer( 18 ) );
    message_type_conversion_table.put("ISOSTATERRPT", new Integer( 19 ) );
    message_type_conversion_table.put("ISOEXPIRED", new Integer( 20 ) );
    // EMAIL/OTHER Message Type values
    message_type_conversion_table.put("CANCEL_EMAIL_ISO", new Integer( 21 ) );
    message_type_conversion_table.put("SHIPPED_EMAIL_ISO", new Integer( 22 ) );
    message_type_conversion_table.put("RECEIVED_EMAIL_ISO", new Integer( 23 ) );
    message_type_conversion_table.put("RENEW_ANSWER_EMAIL_ISO", new Integer( 24 ) );
    message_type_conversion_table.put("LOST_EMAIL_ISO", new Integer( 25 ) );
    message_type_conversion_table.put("RETURNED_EMAIL_ISO", new Integer( 26 ) );
    message_type_conversion_table.put("ILL_ANSWER_EMAIL_ISO", new Integer( 27 ) );

    registerPair(phase_conversion_table,reverse_phase_conversion_table,"IN-PROCESS",new Integer(1));
    registerPair(phase_conversion_table,reverse_phase_conversion_table,"TRACKING",new Integer(2));
    registerPair(phase_conversion_table,reverse_phase_conversion_table,"SATISFIED",new Integer(3));
    registerPair(phase_conversion_table,reverse_phase_conversion_table,"CLOSED",new Integer(4));
    registerPair(phase_conversion_table,reverse_phase_conversion_table,"ENDROTA",new Integer(5));
    registerPair(phase_conversion_table,reverse_phase_conversion_table,"IDLE",new Integer(7));

    role_conversion_table.put("REQ",new Integer(1));
    role_conversion_table.put("RESP",new Integer(2));
    role_conversion_table.put("INT",new Integer(3));
  }

  public static int convertProtocol(String protocol) { return lookup(service_protocol_conversion_table,protocol); }
  public static int convertState(String state) { return lookup(state_conversion_table,state); }
  public static int convertPhase(String phase) { return lookup(phase_conversion_table,phase); }
  public static int convertRole(String role) { return lookup(role_conversion_table,role); }
  public static int convertMessageType(String mtype) { return lookup(message_type_conversion_table,mtype); }

  public static String convertState(int state) { return lookup(reverse_state_conversion_table,state); }
  public static String convertPhase(int phase) { return lookup(reverse_phase_conversion_table,phase); }

  private static int lookup(Hashtable t, String value) {
    if ( ( value != null ) && ( t != null ) ) {
      Integer result = (Integer) t.get(value);
      if ( result != null )
        return result.intValue();
      else
        return -1;
    }
    return -1;
  }

  private static String lookup(Hashtable t, int value)
  {
    return (String) t.get(new Integer(value));
  }


  private static void registerPair(Hashtable forward_mapping,  // From String to Integer
                                   Hashtable reverse_mapping,  // From Integer to String
                                   String str_code,
                                   Integer int_code)
  {
    forward_mapping.put(str_code, int_code);
    reverse_mapping.put(int_code, str_code);
  }
}
