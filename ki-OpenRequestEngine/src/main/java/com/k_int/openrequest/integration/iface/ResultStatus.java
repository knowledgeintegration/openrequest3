package com.k_int.openrequest.integration.iface;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public enum ResultStatus {

  OK;

  public String toString() {
    switch ( this ) {
      case OK: return "OK";
      default: return "Unknown";
    }
  }
}
