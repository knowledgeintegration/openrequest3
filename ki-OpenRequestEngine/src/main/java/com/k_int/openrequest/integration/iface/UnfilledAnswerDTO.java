package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       UnfilledAnswerDTO
 * @version:    $Id: UnfilledAnswerDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class UnfilledAnswerDTO extends ILLAnswerMessageDTO implements java.io.Serializable
{
  // private ReasonUnfilled reason;
  private int reason;
  private String responder_note;

  public UnfilledAnswerDTO()
  {
    super();
  }

  public UnfilledAnswerDTO(ReasonUnfilled reason, 
                          String responder_note)
  {
    super();
    this.reason = reason.toIso();
    this.responder_note = responder_note;
  }

  public ReasonUnfilled getReasonUnfilled()
  {
    return ReasonUnfilled.fromIso(reason);
  }

  public String getResponderNote()
  {
    return responder_note;
  }

  public int ISOReason()
  {
    return reason;
  }
}
