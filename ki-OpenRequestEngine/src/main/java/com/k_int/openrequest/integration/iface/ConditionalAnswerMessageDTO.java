package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       ConditionalAnswerMessageDTO
 * @version:    $Id: ConditionalAnswerMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ConditionalAnswerMessageDTO extends ILLAnswerMessageDTO implements java.io.Serializable
{
  private int conditions;
  private Date date_for_reply;
  private String locations;
  private String responder_note;

  public ConditionalAnswerMessageDTO()
  {
    super();
  }

  public ConditionalAnswerMessageDTO(Conditions conditions,
                             Date date_for_reply,
                             String locations,
                             String responder_note)
  {
    super();
    this.message_type = MessageType.ILL_ANSWER.toIso();
    this.conditions = conditions.toIso();
    this.date_for_reply = date_for_reply;
    this.responder_note = responder_note;
  }

  public Conditions getConditions()
  {
    return Conditions.fromIso(conditions);
  }

  public int getConditionsIso()
  {
    return conditions;
  }

  public Date getDateForReply()
  {
    return date_for_reply;
  }

  public String getLocations() {
    return locations;
  }

  public String getResponderNote()
  {
    return responder_note;
  }

  public String toString() {
    return "ConditionalAnswerMessageDTO {conditions:"+conditions+",date_for_reply:"+date_for_reply+",locations:"+locations+",responder_note:"+responder_note+"}";
  }
}
