package com.k_int.openrequest.integration.iface;

import java.util.Hashtable;

/**
 * Title:       RoleCountDTO
 * @version:    $Id: RoleCountDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration  Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class RoleCountDTO implements java.io.Serializable
{
  private long count;

  public RoleCountDTO(long count)
  {
    this.count = count;
  }
}
