package com.k_int.openrequest.integration.iface.extensions;

import com.k_int.openrequest.db.SystemId;
import com.k_int.openrequest.integration.iface.*;

import java.util.Date;
import java.util.List;

/**
 * Title:       IntermediaryControlDTO
 * @version:    $Id: IntermediaryControlDTO.java,v 1.1 2005/06/10 18:04:19 ibbo Exp $
 * Copyright:   Copyright (C) 2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class IntermediaryControlDTO extends ProtocolMessageExtensionDTO {

  public IntermediaryActionsDTO intermediary_actions = null;
  public IntermediaryResponderControlDTO last_responder = null;
  public SystemId current_responder = null;

  public IntermediaryControlDTO() {
  }

  public IntermediaryControlDTO(IntermediaryActionsDTO intermediary_actions,
                                IntermediaryResponderControlDTO last_responder,
                                SystemId current_responder) {
    this.intermediary_actions = intermediary_actions;
    this.last_responder = last_responder;
    this.current_responder = current_responder;
  }

  public IntermediaryActionsDTO getIntermediaryActions() {
    return intermediary_actions;
  }

  public void setIntermediaryActions(IntermediaryActionsDTO intermediary_actions) {
    this.intermediary_actions = intermediary_actions;
  }

  public IntermediaryResponderControlDTO getLastResponder() {
    return last_responder;
  }

  public void setLastResponder(IntermediaryResponderControlDTO last_responder) {
    this.last_responder = last_responder;
  }

  public SystemId getCurrentResponder() {
    return current_responder;
  }

  public void setCurrentResponder(SystemId current_responder) {
    this.current_responder = current_responder;
  }

}
