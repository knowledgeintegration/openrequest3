package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       ShippedMessageDTO
 * @version:    $Id: ShippedMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ShippedMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  private int shipped_service_type = -1;
  private Date date_shipped = null;
  private Date date_due = null;
  private boolean renewable = false;
  private String total_cost_currency = null;
  private String total_cost_amount = null;
  private int shipped_conditions = -1;
  private String note = null;

  private String name = null;
  private String extendedPostal = null;
  private String streetAndNumber = null;
  private String pobox = null;
  private String city = null;
  private String region = null;
  private String country = null;
  private String postcode = null;
  private String responder_note = null;

  public ShippedMessageDTO()
  {
    this.message_type = MessageType.SHIPPED.toIso();
  }

  public ShippedMessageDTO(ServiceType shipped_service_type,
                   Date date_shipped,
                   Date date_due,
                   boolean renewable,
                   String total_cost_currency,
                   String total_cost_amount,
                   ShippedCondition shipped_conditions,
                   DeliveryService delivery_service,
                   String insured_for_currency,
                   String ins_for_amount,
                   String ins_on_ret_currency,
                   String ins_on_ret_amount,
                   SupplyMediumType[] mediums,
                   int[] units,
                   ResponderOptionalMessagesDTO responder_optional_messages,
		   String name,
		   String extendedPostal,
		   String streetAndNumber,
		   String pobox,
		   String city,
		   String region,
            	   String country,
		   String postcode,
                   String responder_note)
  {
    this.message_type = MessageType.SHIPPED.toIso();
    this.shipped_service_type = shipped_service_type.toIso();
    this.date_shipped = date_shipped;
    this.date_due = date_due;
    this.renewable = renewable;
    this.total_cost_currency = total_cost_currency;
    this.total_cost_amount = total_cost_amount;
    if ( shipped_conditions != null )
      this.shipped_conditions = shipped_conditions.toIso();
    this.note=responder_note;
    this.name = name;
    this.extendedPostal = extendedPostal;
    this.streetAndNumber = streetAndNumber;
    this.pobox = pobox;
    this.city = city;
    this.region = region;
    this.country = country;
    this.postcode = postcode;
  }

  public ServiceType getShippedServiceType()
  {
    return ServiceType.fromIso(shipped_service_type);
  }

  public Date getDateShipped()
  {
    return date_shipped;
  }

  public Date getDateDue()
  {
    return date_due;
  }

  public boolean isRenewable()
  {
    return renewable;
  }

  public String getTotalCostAmount()
  {
    return total_cost_amount;
  }
  
  public SupplyMediumType[] getMedium()
  {
    return null;
  }

  public int[] getUnits() 
  {
    return null;
  }

  public String getTotalCostCurrency()
  {
    return total_cost_currency;
  }

  public String getCostMonetaryValue()
  {
    return null;
  }

  public ShippedCondition getShippedConditions()
  {
    return ShippedCondition.fromIso(shipped_conditions);
  }

  public DeliveryService getShippedVia()
  {
    return null;
  }

  public String getInsForCurrency()
  {
    return null;
  }

  public String getInsForAmount()
  {
    return null;
  }

  public String getRetInsCurrency()
  {
    return null;
  }

  public String getRetInsAmount()
  {
    return null;
  }

  public PostalAddressDTO getReturnAddress()
  {
    return null;
  }

  public ResponderOptionalMessagesDTO getResOptMsgs()
  {
    return null;
  }

  public String getResponderNote() {
    return note;
  }

  public String getName() {
    return name;
  }

  public String getExtendedPostal() {
    return extendedPostal;
  }

  public String getStreetAndNumber() {
    return streetAndNumber;
  }

  public String getPOBox() {
    return pobox;
  }

  public String getCity() {
    return city;
  }

  public String getRegion() {
    return region;
  }

  public String getCountry() {
    return country;
  }

  public String getPostcode() {
    return postcode;
  }

  public String toString() {
    return "Shipped {shipped_service_type:"+shipped_service_type+","+
           "date_shipped:"+ date_shipped+","+
           "date_due:"+ date_due+","+
           "renewable:"+ renewable+","+
           "total_cost_currency:"+ total_cost_currency+","+
           "total_cost_amount:"+ total_cost_amount+","+
           "shipped_conditions:"+ shipped_conditions+","+
           "note:"+ note+
           "name:"+ name+
           "extendedPostal:"+ extendedPostal+
           "streetAndNumber:"+ streetAndNumber+
           "pobox:"+ pobox+
           "city:"+ city+
           "region:"+ region+
           "country:"+ country+
           "postcode:"+ postcode+
           "}";
  }
}
