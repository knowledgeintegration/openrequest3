package com.k_int.openrequest.integration.iface;
import java.util.Date;

public class BriefMessageInfoDTO implements java.io.Serializable {


  protected String message_type = null;
  protected String event_code;
  protected String message_id;
  protected String from_state;
  protected String to_state;
  protected java.util.Date timestamp;
  protected Boolean is_read;
  protected String additional;

  public BriefMessageInfoDTO() {
  }

  public BriefMessageInfoDTO(String message_type,
                             String event_code,
                             java.util.Date timestamp,
                             Boolean is_read,
                             String message_id,
                             String from_state,
                             String to_state,
                             String additional) {
    this.message_type = message_type;
    this.event_code = event_code;
    this.timestamp = timestamp;
    this.is_read = is_read;
    this.message_id = message_id;
    this.from_state = from_state;
    this.to_state = to_state;
    this.additional = additional;
  }

  public String getType() {
    return message_type;
  }

  public String getEventCode() {
    return event_code;
  }

  public java.util.Date getServiceDateTime() {
    return timestamp;
  }

  public Boolean isRead() {
    return is_read;
  }

  public String getMessageId() {
    return message_id;
  }

  public String getFromState() {
    return from_state;
  }

  public String getToState() {
    return to_state;
  }

  public String getAdditional() {
    return additional;
  }
}
