package com.k_int.openrequest.integration.iface;

public enum TelecomServiceId {

  FAX, EMAIL, TELEPHONE;

  public String toString()  {
    switch ( this ) {
      case FAX: return "Fax";
      case EMAIL: return "Email";
      case TELEPHONE: return "Telephone";
      default: return "Unknown";
    }
  }

  public String toIso()     {
    switch ( this ) {
      case FAX: return "FAX";
      case EMAIL: return "SMTP";
      case TELEPHONE: return "VOICE";
      default: return "Unknown";
    }
  }
}
