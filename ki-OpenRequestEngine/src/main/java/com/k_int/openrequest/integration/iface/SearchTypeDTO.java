package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       RotaElementDTO
 * @version:    $Id: SearchTypeDTO.java,v 1.3 2005/03/29 23:03:37 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class SearchTypeDTO implements java.io.Serializable {

  public char level_of_service=' ';
  public Date need_before_date;
  public int expiry = 3;
  public Date expiry_date;

  public SearchTypeDTO() {
  }

  public SearchTypeDTO(LevelOfService level_of_service,
                      Date need_before_date,
                      ExpiryFlag expiry,
                      Date expiry_date) {
    if ( level_of_service != null )
      this.level_of_service = level_of_service.toIso();
    this.need_before_date = need_before_date;
    if ( expiry != null )
      this.expiry = expiry.toIso();
    this.expiry_date = expiry_date;
  }

  public LevelOfService getLevelOfService() {
    return LevelOfService.fromIso(level_of_service);
  }

  public void setLevelOfService(LevelOfService level_of_service) {
    this.level_of_service = level_of_service.toIso();
  }

  public void setLevelOfService(char level_of_service) {
    this.level_of_service = level_of_service;
  }

  public Date getNeedBeforeDate() {
    return need_before_date;
  }

  public void setNeedBeforeDate(Date need_before_date) {
    this.need_before_date = need_before_date;
  }

  public ExpiryFlag getExpiryFlag() {
    return ExpiryFlag.fromIso(expiry);
  }

  public void setExpiryFlag(ExpiryFlag expiry) {
    this.expiry = expiry.toIso();
  }

  public void setExpiryFlag(int expiry) {
    this.expiry = expiry;
  }

  public Date getExpiryDate() {
    return expiry_date;
  }

  public void setExpiryDate(Date expiry_date) {
    this.expiry_date = expiry_date;
  }
}
