package com.k_int.openrequest.integration.iface;

import java.util.Hashtable;

/**
 * Title:       RequestCountDTO
 * @version:    $Id: RequestCountDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class RequestCountDTO implements java.io.Serializable
{
  private long count;
  // private RequestCategory category = null;
  private String category = null;

  public RequestCountDTO(String category, long count)
  {
    this.category = category;
    this.count = count;
  }

  public RequestCountDTO(RequestCategory category, long count)
  {
    this.category = category.toIso();
    this.count = count;
  }

  public int getCount()
  {
    return (int)count;
  }

  public RequestCategory getCategory()
  {
    return RequestCategory.fromIso(category);
  }

  public String toString()
  {
    return category+":"+count;
  }
}
