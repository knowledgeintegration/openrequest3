package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       StatusDTO
 * @version:    $Id: StatusDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class StatusDTO implements java.io.Serializable {

  private int state;

  public StatusDTO(State state) {
    this.state = state.toIso();
  }

  public State getState() {
    return State.fromIso(state);
  }

  public boolean isClosed() {
    return false;
  }

  public CloseReason getCloseReason() {
    return null;
  }

  public Date getClosureDate() {
    return null;
  }

  public Date getCreateDate() {
    return null;
  }

  public Date getShippedDate() {
    return null;
  }

  public Date getDueDate() {
    return null;
  }

  public Date getReturnDate()
  {
    return null;
  }

  public boolean canRenew()
  {
    return false;
  }

  public Date getLastMessageDate()
  {
    return null;
  }

  public Date getReceiveDate()
  {
    return null;
  }

  public Date getLastRenewalDate()
  {
    return null;
  }

  public int getItemCount()
  {
    return 0;
  }

  public Phase getPhase()
  {
    return  null;
  }
}
