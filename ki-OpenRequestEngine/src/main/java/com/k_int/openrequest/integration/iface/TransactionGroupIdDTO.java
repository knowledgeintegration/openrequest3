package com.k_int.openrequest.integration.iface;

/**
 * Title:       TransGroupId
 * @version:    $Id: TransactionGroupIdDTO.java,v 1.2 2005/02/25 23:20:26 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class TransactionGroupIdDTO implements java.io.Serializable
{
  private Long id = null;

  public TransactionGroupIdDTO(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public String toString() {
    return "TransactionGroupID:"+id;
  }
}
