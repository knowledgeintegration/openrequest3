package com.k_int.openrequest.integration.iface;

/**
 * Title:       CancelReplyMessageDTO
 * @version:    $Id: CancelReplyMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class CancelReplyMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  private boolean answer;
  private String responder_note;

  public CancelReplyMessageDTO()
  {
    this.message_type = MessageType.CANCEL_REPLY.toIso();
  }

  public CancelReplyMessageDTO(boolean answer, String responder_note)
  {
    this.message_type = MessageType.CANCEL_REPLY.toIso();
    this.answer = answer;
    this.responder_note = responder_note;
  }

  public boolean isAnswer()
  {
    return answer;
  }

  public String getResponderNote()
  {
    return responder_note;
  }
}
