package com.k_int.openrequest.integration.iface;

/**
 * Title:       TransId
 * @version:    $Id: TransactionIdDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class TransactionIdDTO implements java.io.Serializable
{
  private Long id = null;

  public TransactionIdDTO(Long id) {
    this.id = id;
  }

  public TransactionIdDTO(long id) {
    this.id = new Long(id);
  }

  public Long getId() {
    return id;
  }

  public String toString() {
    if ( id != null )
      return id.toString();

    return null;
  }
 
}
