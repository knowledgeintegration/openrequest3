package com.k_int.openrequest.integration.iface;

public enum ExpiryFlag {

  NEED_BEFORE_DATE, OTHER_DATE, NO_EXPIRY;

  public String toString() { 
    switch ( this ) {
      case NEED_BEFORE_DATE: return "Need Before Date";
      case OTHER_DATE: return "Other Date";
      case NO_EXPIRY: return "No Expiry";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case NEED_BEFORE_DATE: return 1;
      case OTHER_DATE: return 2;
      case NO_EXPIRY: return 3;
      default: return 3;
    }
  }

  public static ExpiryFlag fromIso(int iso) {
    switch ( iso ) {
      case 1: return NEED_BEFORE_DATE;
      case 2: return OTHER_DATE;
      case 3: return NO_EXPIRY;
      default: return null;
    }
  }

}
