package com.k_int.openrequest.integration.iface;

public interface StatelessRequestManager {
  /**
   * Report implementation version
   */
  public String getVersion() throws RequestManagerException;

  /**
   * An internal API call that returns all unread messages for a given location.
   * It is the callers responsibility to ensure that the caller is allowed to view
   * the unread messages at a given location.
   */
  public BriefMessageInfoDTO[] getUnreadMessages(Long location_id);

  /** 
   * Set the message read status of the given message
   */
  public Boolean setMessageReadStatus(Long location_id, Long message_id);

  /** Create a new transaction group with the supplied details and rota.
   * @param location_id ID of the requesting location
   * @param request_data Generic details about the item required.
   * @param rota List of locations to which this request may be sent.
   * @return The ID of the created TransactionGroup
   */
  public Long createRequest(RequesterViewDTO request_data,
                            RotaElementDTO[] rota,
                            String requester_symbol) throws RequestManagerException, DuplicateUserReferenceException;

  /** Create a new transaction group with the supplied details and rota.
   * @param location_id ID of the requesting location
   * @param request_data Generic details about the item required.
   * @param rota List of locations to which this request may be sent.
   * @param mandatory_tgq The TGQ that must be used (Must be unique)
   * @return the ID of the created TransactionGroup
   */
  public Long createTransGroup(RequesterViewDTO request_data,
                               RotaElementDTO[] rota,
                               String mandatory_tgq,
                               String requester_symbol) throws RequestManagerException, DuplicateUserReferenceException;

  /** (Re)Start the ILL Messaging process for the identified group.
   *  This method starts the messaging, but does not wait for the system to send the first request. and is
   *  essentially "Fire-And-Forget".
   * @param transaction_group_id ID of group to start
   */
  public void startMessaging(Long transaction_group_id) throws RequestManagerException;

  /** (Re)Start the ILL Messaging process for the identified group and return the next ILL Transaction ID.
   *  This method waits for the ILL to be sent before returning, and ensures that a transaction ID
   *  can be handed back to the caller.
   * @param transaction_group_id ID of group to start
   * @return id of new ILL transaction
   */
  public Long startMessagingSync(Long transaction_group_id) throws RequestManagerException;

  /**
   * Obtain a list of request counts for different important application states.
   * @param loc_id the location we are retrieving counts for.
   */
  public RequestCountDTO[] retrieveRequestCounts(Long loc_id) throws RequestManagerException;

  /**
   * Search the database given the input query and return a result set object. No Pagination!.
   * @param query The input query
   * @return brief information about the matching transactions (via transaction group)
   */
  public ResultSetDTO search(RequestManagerQueryDTO query)  throws RequestManagerException;

  /**
   * List all the valid operations that can be applied given the input state model and state code.
   * @param input_model The input state model
   * @param input_model state input state
   * @param user_selectable_only only list user selectable actions - yes or no
   * @return A string array of the valid action codes that can be applied from the input state
   */
  public String[] listValidActions(String input_model, String input_state, Boolean user_selectable_only) throws RequestManagerException;


  /**
   * Lookup the location with the suppled ID
   * @param id ID to look up
   * @return A DTO of the the location info
   */
  public ORLocationInfoDTO lookupLocation(Long id)  throws RequestManagerException;

  /**
   * Create the specified location
   * @param loc_info Location Info
   * @return ID of newly created location.
   */
  public Long createLocation(ORLocationInfoDTO loc_info)  throws RequestManagerException;

  /**
   * Lookup the symbol with the suppled authority:symbol
   * @param symbol symbol to look up.
   * @return A DTO of the the symbol info
   */
  public ORSymbolInfoDTO lookupSymbol(String symbol) throws RequestManagerException;

  /**
   * Create the specified symbol
   * @param symbol symbol to create
   * @return ID of newly created location.
   */
  public Long createSymbol(ORSymbolInfoDTO symbol) throws RequestManagerException;

  /**
   * Action the specified transactions with the supplied message
   */
  public String actionMessage(long loc_id, long[] transaction_ids, ProtocolMessageDTO action) throws RequestManagerException;
}
