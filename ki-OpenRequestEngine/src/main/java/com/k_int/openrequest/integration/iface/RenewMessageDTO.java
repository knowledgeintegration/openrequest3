package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       RenewMessageDTO
 * @version:    $Id: RenewMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class RenewMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  public Date desired_due_date;
  public String requester_note;

  public RenewMessageDTO()
  {
    this.message_type = MessageType.RENEW.toIso();
  }

  public RenewMessageDTO(Date desired_due_date,
                 String requester_note)
  {
    this.message_type = MessageType.RENEW.toIso();
    this.desired_due_date = desired_due_date;
    this.requester_note = requester_note;
  }

  public Date getDesiredDueDate()
  {
    return desired_due_date;
  }

  public String getRequesterNote()
  {
    return requester_note;
  }
}
