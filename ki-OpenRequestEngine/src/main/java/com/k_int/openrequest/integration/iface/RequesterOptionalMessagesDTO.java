package com.k_int.openrequest.integration.iface;

import java.util.Hashtable;

/**
 * Title:       RequesterOptionalMessagesDTO
 * @version:    $Id: RequesterOptionalMessagesDTO.java,v 1.2 2005/03/29 23:03:37 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class RequesterOptionalMessagesDTO implements java.io.Serializable {
  private boolean can_send_received;
  private boolean can_send_returned;
  private int requester_shipped;
  private int requester_checked_in;

  public RequesterOptionalMessagesDTO() {
  }

  public RequesterOptionalMessagesDTO(boolean can_send_received,
                      boolean can_send_returned,
                      MessageOption requester_shipped,
                      MessageOption requester_checked_in) {
    this.can_send_received = can_send_received;
    this.can_send_returned = can_send_returned;
    this.requester_shipped = requester_shipped.toIso();
    this.requester_checked_in = requester_checked_in.toIso();
  }

  public boolean canSendReceived() {
    return can_send_received;
  }

  public boolean canSendReturned() {
    return can_send_returned;
  }

  public MessageOption getRequesterShipped() {
    return MessageOption.fromIso(requester_shipped);
  }

  public MessageOption getRequesterCheckedIn() {
    return MessageOption.fromIso(requester_checked_in);
  }
}
