package com.k_int.openrequest.integration.iface;

public enum Answer {

  YES,
  NO;

  public String toString() {
    switch(this) {
      case YES:              return "Yes";
      case NO:               return "No";
      default:               return "Unknown";
    }
  }

  public int toIso() {
    switch(this) {
      case YES:              return 1;
      case NO:               return 2;
      default:               return 0;
    }
  }
}
