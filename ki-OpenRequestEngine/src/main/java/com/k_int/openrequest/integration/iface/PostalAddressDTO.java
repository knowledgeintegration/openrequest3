package com.k_int.openrequest.integration.iface;

/**
 * Title:       PostalAddressDTO
 * @version:    $Id: PostalAddressDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class PostalAddressDTO implements java.io.Serializable
{
  public String name;
  public String extended_postal;
  public String street_and_number;
  public String post_office_box;
  public String city;
  public String region;
  public String country;
  public String postcode;

  public PostalAddressDTO(String name,
                         String extended_postal,
                         String street_and_number,
                         String post_office_box,
                         String city,
                         String region,
                         String country,
                         String postcode)
  {
    this.name = name;
    this.extended_postal = extended_postal;
    this.street_and_number = street_and_number;
    this.post_office_box = post_office_box;
    this.city = city;
    this.region = region;
    this.country = country;
    this.postcode = postcode;
  }

  public PostalAddressDTO() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getExtendedPostal() {
    return extended_postal;
  }

  public void setExtendedPostal(String extended_postal) {
    this.extended_postal = extended_postal;
  }

  public String getStreetAndNumber() {
    return street_and_number;
  }

  public void setStreetAndNumber(String street_and_number) {
    this.street_and_number = street_and_number;
  }

  public String getPostOfficeBox() {
    return post_office_box;
  }

  public void setPostOfficeBox(String post_office_box) {
    this.post_office_box = post_office_box;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }
}
