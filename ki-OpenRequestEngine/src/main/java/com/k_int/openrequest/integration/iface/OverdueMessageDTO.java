package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       OverdueMessageDTO
 * @version:    $Id: OverdueMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class OverdueMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  private Date date_due;
  private String responder_note;
  private boolean renewable;

  public OverdueMessageDTO() {
    this.message_type = MessageType.OVERDUE.toIso();
  }

  public OverdueMessageDTO(Date due_date, boolean renewable, String responder_note) {
    this.message_type = MessageType.OVERDUE.toIso();
    this.date_due = due_date;
    this.responder_note = responder_note;
    this.renewable = renewable;
  }

  public Date getDateDue() {
    return date_due;
  }

  public String getResponderNote() {
    return responder_note;
  }

  public boolean isRenewable() {
    return renewable;
  }

  public String toString() {
    return "Overdue {date_due:"+date_due+",responder_note:"+responder_note+",renewable:"+renewable+"}";
  }
}
