package com.k_int.openrequest.integration.iface;

public enum TransportationMode {

  HAYS, POSTAL, FAX, RESPONDER;

  public String toString() {
    switch ( this ) {
      case HAYS: return "Hays";
      case POSTAL: return "Postal";
      case FAX: return "Fax";
      case RESPONDER: return "Responder Choice";
      default: return null;
    }
  }

  public String toIso() {
    switch ( this ) {
      case HAYS: return "Hays";
      case POSTAL: return "Postal";
      case FAX: return "Fax";
      case RESPONDER: return "Responder";
      default: return null;
    }
  }
}
