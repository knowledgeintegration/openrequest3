package com.k_int.openrequest.integration.iface;

import java.util.Date;
import java.util.List;

/**
 * Title:       TransactionResultsDTO
 * @version:    $Id: TransactionResultsDTO.java,v 1.1 2005/06/10 18:04:19 ibbo Exp $
 * Copyright:   Copyright (C) 2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public abstract class TransactionResultsDTO {
}
