package com.k_int.openrequest.integration.iface;

/**
 * Title:       MessageIdDTO
 * @version:    $Id: MessageIdDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration  Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class MessageIdDTO implements java.io.Serializable
{
  private Long trans_id = null;
  private Long message_id = null;

  public MessageIdDTO()
  {
  }

  public MessageIdDTO(Long trans_id, Long message_id)
  {
    this.trans_id = trans_id;
    this.message_id = message_id;
  }

  public Long getTransId()
  {
    return trans_id;
  }

  public Long getMessageId()
  {
    return message_id;
  }
}
