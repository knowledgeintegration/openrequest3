package com.k_int.openrequest.integration.iface;

/**
 * Title:       StatusOrErrorReportDTO
 * @version:    $Id: StatusOrErrorReportDTO.java,v 1.3 2005/06/17 18:52:15 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class StatusOrErrorReportDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  /** Optional member - leave as null for no value */
  private String note;

  /** Optional member - leave as null for no value */
  Long reason_no_report = null;

  /** Optional member */
  public StatusReportDTO status_report = null;

  /** Optional member */
  public ErrorReportDTO error_report = null;


  public StatusOrErrorReportDTO() {
    this.message_type = MessageType.STATUS_OR_ERR_REPORT.toIso();
  }

  public StatusOrErrorReportDTO(String note) {
    this(note,null,null,null);
    this.message_type = MessageType.STATUS_OR_ERR_REPORT.toIso();
  }

  public StatusOrErrorReportDTO(String note,
                                Long reason_no_report,
                                StatusReportDTO status_report,
                                ErrorReportDTO error_report) {
    this.note = note;
    this.reason_no_report = reason_no_report;
    this.status_report = status_report;
    this.error_report = error_report;
    this.message_type = MessageType.STATUS_OR_ERR_REPORT.toIso();
  }

  public Long getReasonNoReport() {
    return reason_no_report;
  }

  public void setReasonNoReport(Long reason_no_report) {
    this.reason_no_report = reason_no_report;
  }

  public StatusReportDTO getStatusReport() {
    return status_report;
  }

  public void setStatusReport(StatusReportDTO status_report) {
    this.status_report = status_report;
  }

  public ErrorReportDTO getErrorReport() {
    return error_report;
  }

  public void setErrorReport(ErrorReportDTO error_report) {
    this.error_report = error_report;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String not) {
    this.note = note;
  }


}
