package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       RequesterSpecificMessageDTO
 * @version:    $Id: RequesterSpecificMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class RequesterSpecificMessageDTO implements java.io.Serializable
{
  private String initiating_branch;
  private String borrower_name;
  private String borrower_id;
  private String borrower_type;
  private String borrower_notes;
  private String private_note;
  private String reply_to_email_address;

  public RequesterSpecificMessageDTO()
  {
  }

  public RequesterSpecificMessageDTO(String initiating_branch,
                             String borrower_name,
                             String borrower_id,
                             String borrower_type,
                             String borrower_notes,
                             String private_note,
                             String reply_to_email_address)
  {
    this.initiating_branch = initiating_branch;
    this.borrower_name = borrower_name;
    this.borrower_id = borrower_id;
    this.borrower_type = borrower_type;
    this.borrower_notes = borrower_notes;
    this.private_note = private_note;
    this.reply_to_email_address = reply_to_email_address;
  }

  public String getInitiatingBranch()
  {
    return initiating_branch;
  }

  public String getBorrowerName()
  {
    return borrower_name;
  }

  public String getBorrowerId()
  {
    return borrower_id;
  }

  public String getBorrowerType()
  {
    return borrower_type;
  }

  public String getBorrowerNotes()
  {
    return borrower_notes;
  }

  public String getPrivateNote()
  {
    return private_note;
  }

  public String getReplyToEmailAddress()
  {
    return reply_to_email_address;
  }
}
