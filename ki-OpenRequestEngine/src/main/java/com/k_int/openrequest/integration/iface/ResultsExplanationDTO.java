package com.k_int.openrequest.integration.iface;

/**
 * Title:       ResultsExplanationDTO
 * @version:    $Id: ResultsExplanationDTO.java,v 1.1 2005/06/10 18:04:19 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public abstract class ResultsExplanationDTO implements java.io.Serializable {
}
