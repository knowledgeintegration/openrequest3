package com.k_int.openrequest.integration.iface;

import java.util.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public enum MsgReadStatus {

  ANY, UNREAD, READ;

  public String toString() {
    switch ( this ) {
      case ANY: return "NO RESTRICTION";
      case UNREAD: return "Unread Messages";
      case READ: return "No Unread Messages";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case ANY: return 1;
      case UNREAD: return 2;
      case READ: return 3;
      default: return 0;
    }
  }

  public static MsgReadStatus fromIso(int iso) {
    switch (iso) {
      case 1: return ANY;
      case 2: return UNREAD;
      case 3: return READ;
      default: return null;
    }
  }

}
