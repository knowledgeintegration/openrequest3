package com.k_int.openrequest.integration.iface;

/**
 * Title: DeliveryService
 * Description:Typesafe enum pattern for Delivery-Service.physical-delivery; see BS ISO 10161-1 page 38.
 * Copyright:    Copyright (c) 2003,2004
 * @version 1.0
 */

public enum DeliveryService {

  HAYS, POSTAL, FAX, EMAIL, NONE;

  public String toString() {
    switch( this ) {
      case HAYS: return "Hays";
      case POSTAL: return "Postal";
      case FAX: return "Fax";
      case EMAIL: return "EMail";
      case NONE: return "None";
      default: return null;
    }
  }

  public String toIso() {
    switch( this ) {
      case HAYS: return "Hays";
      case POSTAL: return "Postal";
      case FAX: return "Fax";
      case EMAIL: return "Email";
      case NONE: return "None";
      default: return null;
    }
  }

  public static DeliveryService fromIso(String iso) {
    if ( iso != null ) {
      if ( iso.equalsIgnoreCase("Hays") ) return HAYS;
      if ( iso.equalsIgnoreCase("Postal") ) return POSTAL;
      if ( iso.equalsIgnoreCase("Fax") ) return FAX;
      if ( iso.equalsIgnoreCase("Email") ) return EMAIL;
      if ( iso.equalsIgnoreCase("None") ) return NONE;
    }
    System.err.println("Unable to resolve delivery service "+iso);
    return null;
  }
}
