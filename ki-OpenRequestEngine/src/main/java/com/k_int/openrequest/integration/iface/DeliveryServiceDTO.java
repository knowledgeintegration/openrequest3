package com.k_int.openrequest.integration.iface;

/**
 * Title:       DeliveryServiceDTO
 * @version:    $Id: DeliveryServiceDTO.java,v 1.3 2005/06/11 12:30:44 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public abstract class DeliveryServiceDTO implements java.io.Serializable
{
  private int delivery_type;

  public DeliveryServiceDTO() {
  }

  public DeliveryServiceDTO(int devlivery_type)
  {
    this.delivery_type = devlivery_type;
  }

  public DeliveryServiceDTO(BeDeliveryType devlivery_type)
  {
    this.delivery_type = devlivery_type.toIso();
  }

  public BeDeliveryType getDeliveryType()
  {
    return BeDeliveryType.fromIso(delivery_type);
  }

  public void setDeliveryType(BeDeliveryType delivery_type)
  {
    this.delivery_type = delivery_type.toIso();
  }

  public void setDeliveryType(int delivery_type)
  {
    this.delivery_type = delivery_type;
  }
}
