package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       ReturnedMessageDTO
 * @version:    $Id: ReturnedMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ReturnedMessageDTO extends ProtocolMessageDTO implements java.io.Serializable {

  private Date date_returned;
  private String returned_via;
  private String insured_for_currency;
  private String insured_for_amount;
  private String requester_note;

  public ReturnedMessageDTO() {
    this.message_type = MessageType.RETURNED.toIso();
  }

  public ReturnedMessageDTO(Date date_returned,
                    String returned_via,
                    String insured_for_currency,
                    String insured_for_amount,
                    String requester_note)
  {
    this.message_type = MessageType.RETURNED.toIso();
    this.date_returned = date_returned;
    this.returned_via = returned_via;
    this.insured_for_currency = insured_for_currency;
    this.insured_for_amount = insured_for_amount;
    this.requester_note = requester_note;
  }

  public Date getDateReturned()
  {
    return date_returned;
  }

  public String getReturnedVia()
  {
    return returned_via;
  }

  public String getRequesterNote()
  {
    return requester_note;
  }

  public String getInsuredForCurrencyCode()
  {
    return insured_for_currency;
  }

  public String getInsuredForMonetaryValue()
  {
    return insured_for_amount;
  }

  public String toString() {
    return "Returned {date_returned:"+date_returned+", returned_via:"+returned_via+", insured_for_currency:"+insured_for_currency+",insured_for_amount:"+insured_for_amount+",requester_note:"+requester_note+"}";
  }
}
