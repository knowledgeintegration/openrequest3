//**********
// Copyright(C) 2003 by OCLC Online Computer Library Center, Inc.
// OCLC proprietary information: the enclosed materials contain 
// proprietary information of OCLC Online Computer Library 
// Center, Inc. and shall not be disclosed in whole or in any 
// part to any third party or used by any person for any purpose, 
// without written consent of OCLC, Inc. Duplication of 
// any portion of these materials shall include this legend.
//**********

package com.k_int.openrequest.integration.dataload.oclc;

import org.apache.log4j.Logger;

/**
 * The mapping table to connect the OpenRequest and WorldCat Resource Sharing data.
 * @hibernate.class  table="ISO_WRS_MAPPING" dynmic-update="true" dynamic-insert="true"
 *
 */
public class ISOWRSMappingHDO {

  /**
   * Holds a custom log4j Logger object instance
   */
  private static final Logger logger = Logger.getLogger(ISOWRSMappingHDO.class);


  /**
   * Private database key
   */
  private Long id;

  /**
   * The ILL number
   */
  private int illNumber;

  /**
    * The status of the requested item (pending, shipped, etc.)
    */
  private int status;

  /**
   * The OCLC symbol of the User
   */
  private String symbol;

  /**
   * A special status indicating a problem with the record. Must be a value
   * defined in ProblemStatus.
   * @see org.oclc.sage.client.common.ProblemStatus
   */
  private int problemStatus;

  /**
   * the ISO 10161 role of the user
   */
  private int role;

  /**
   * the ISO 10161 transaction Id
   */
  private String transId;

  /**
   * the ISO 10161 requester Id
   */
  private String requester;


  //// Getters and setters

  /**
   * @hibernate.id  generator-class="assigned" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  /**
   * Get the illNumber
   * @hibernate.property
   * @hibernate.column name="OCLC_ILL_NUMBER" index="OCLC_ILL_NO_IDX"
   */
  public int getIllNumber() {
     return illNumber;
  }

  /**
   * Set the illNumber
   * @param illNumber
   */
  public void setIllNumber(int illNumber) {
     this.illNumber = illNumber;
  }

  /**
   * Get the status
   * @hibernate.property column="OCLC_ILL_STATUS"
   */
  public int getStatus() {
     return status;
  }

  /**
   * Set the status
   * @param status
   */
  public void setStatus(int status) {
     this.status = status;
  }

  /**
   * Set the status
   * @hibernate.property column="OCLC_PROBLEM_STATUS"
   */
  public int getProblemStatus() {
    return problemStatus;
  }

  public void setProblemStatus(int value) {
     this.problemStatus = value;
  }

  /**
   * get Role
   * @hibernate.property column="OCLC_ROLE"
   */
   public int getRole() {
       return role;
   }

   public void setRole(int role) {
       this.role = role;
   }

  /**
   * get Trans ID
   * @hibernate.property
   * @hibernate.column name="OCLC_TRANSACTION_ID" index="OCLC_TRANS_ID_IDX" length="50"
   */
   public String getTransId() {
       return transId;
   }

   public void setTransId(String transId) {
       this.transId = transId;
   }

  /**
   * get Trans ID
   * @hibernate.property column="OCLC_REQUESTER" length="50"
   * N.B. This is really current partner, not requester (IE, it depends on role)
   */
   public String getRequester() {
       return requester;
   }

   public void setRequester(String requester) {
       this.requester = requester;
   }

  /**
   * get Trans ID
   * @hibernate.property column="OCLC_SYMBOL" length="25"
   */
   public String getSymbol() {
       return symbol;
   }

   public void setSymbol(String symbol) {
       this.symbol = symbol;
   }
}

