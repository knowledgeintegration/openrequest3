package com.k_int.openrequest.integration.iface;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public enum EventCode {

  ILLreq, FWDreq, ANSreq_CO, ANSreq_RY, ANSreq_UN, ANSreq_LP, ANSreq_WS, ANSreq_HP, ANSreq_ES, C_REPreq_plus, C_REPreq_minus, CANreq, CARreq_plus, CARreq_minus, SHIreq, RCVreq, RCLreq, DUEreq, RETreq, RENreq, REAreq_plus, REAreq_minus, CHKreq, LSTreq, DAMreq, MSGreq, STQreq, STRreq, ILL, FWD, ANS_CO, ANS_RY, ANS_UN, ANS_LP, ANS_WS, ANS_HP, ANS_ES, C_REP_plus, C_REP_minus, CAN, CAR_plus, CAR_minus, SHI, RCV, RCL, DUE, RET, REN, REA_plus, REA_minus, CHK, LST, DAM, MSG, STQ, STR, EXP;
}
