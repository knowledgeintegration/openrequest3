package com.k_int.openrequest.integration.iface;

public enum ServiceType {

  LOAN, COPY, LOCATIONS, ESTIMATE, RESPONDER_SPECIFIC;

  public String toString() { 
    switch ( this ) {
      case LOAN: return "Loan";
      case COPY: return "Copy";
      case LOCATIONS: return "Locations";
      case ESTIMATE: return "Estimate";
      case RESPONDER_SPECIFIC: return "Responder Specific";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case LOAN: return 1;
      case COPY: return 2;
      case LOCATIONS: return 3;
      case ESTIMATE: return 4;
      case RESPONDER_SPECIFIC: return 5;
      default: return 0;
    }
  }

  public static ServiceType fromIso(int iso){
    switch (iso) {
      case 1: return LOAN;
      case 2: return COPY;
      case 3: return LOCATIONS;
      case 4: return ESTIMATE;
      case 5: return RESPONDER_SPECIFIC;
      default: return null;
    }
  }
}
