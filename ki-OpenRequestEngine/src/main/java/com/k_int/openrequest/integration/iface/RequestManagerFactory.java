package com.k_int.openrequest.integration.iface;

/*
 * $Log: RequestManagerFactory.java,v $
 * Revision 1.2  2005/03/08 10:20:49  ibbo
 * Updated - Added extra error checking around Extensions
 *
 * Revision 1.1.1.1  2005/02/14 11:46:29  ibbo
 * Import
 *
 * Revision 1.3  2004/08/01 12:38:24  ibbo
 * Updated
 *
 * Revision 1.2  2004/07/26 13:28:08  ibbo
 * UPdate
 *
 * Revision 1.1.1.1  2004/07/23 07:13:16  ibbo
 * Initial
 *
 * Revision 1.4  2003/05/01 08:53:20  imi
 * Many changes for revised RMI structure
 *
 * Revision 1.3  2003/04/17 11:39:54  imi
 * Blah
 *
 * Revision 1.2  2003/04/04 14:21:47  imi
 * Renamed BE to Be Be ;)
 *
 * Revision 1.1.1.1  2003/04/04 10:18:21  imi
 * Initial import
 *
 * Revision 1.1  2003/03/27 09:43:08  imi
 * Renamed some classes to add BE prefix
 *
 */

/**
 * Title:       RequestManagerFactory
 * @version:    $Id: RequestManagerFactory.java,v 1.2 2005/03/08 10:20:49 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Return instances of the manager object for an identified location code.
 *              Single instances should be maintained so repeated calls return the same
 *              live manager.
 */
public interface RequestManagerFactory {
  public RequestManager getRequestManager(String location_symbol) throws RequestManagerException;

  /**
   * We need to expose toString so that remote clients can ask the remote object for it's name
   */
  public String toString();
}
