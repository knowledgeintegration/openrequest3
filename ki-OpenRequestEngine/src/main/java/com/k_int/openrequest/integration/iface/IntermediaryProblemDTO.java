package com.k_int.openrequest.integration.iface;

import java.math.BigInteger;

/**
 * Title:       IntermediaryProblemDTO
 * @version:    $Id: IntermediaryProblemDTO.java,v 1.2 2005/06/10 16:00:56 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class IntermediaryProblemDTO extends UserErrorReportDTO implements java.io.Serializable {

  public static IntermediaryProblemDTO CANNOT_SEND_ONWARD = new IntermediaryProblemDTO(1);

  private long code = 0;

  public IntermediaryProblemDTO(long code) {
    this.code = code;
  }

  public BigInteger getProblemCode() {
    return BigInteger.valueOf(code);
  }
}
