package com.k_int.openrequest.integration.iface;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public enum RequestCategory {

  REQ_UNREAD_MSGS,
  CONDITIONAL_QUERY,
  EXPIRED_ALERT,
  NEED_BY_EXPIRED,
  RETRY_DATE_EXPIRED,
  END_OF_ROTA,
  EMAIL_OTHER,
  DUE_DATE_PENDING,
  OVERDUE_ALERT,
  RECALL_ALERT,
  NEW_REQUESTS,
  RES_UNREAD_MSGS,
  NEED_BY_PENDING,
  HOLD_DATE_EXPIRED,
  EXPIRY_PENDING,
  CANCEL_REQUEST,
  RENEWAL_REQUEST,
  DUE_DATE_EXPIRED,
  OVERDUE_LIST;

  public String toString() { 
    switch ( this ) {
      case REQ_UNREAD_MSGS: return "REQ_UNREAD_MSGS";
      case CONDITIONAL_QUERY: return "CONDITIONAL_QUERY";
      case EXPIRED_ALERT: return "EXPIRED_ALERT";
      case NEED_BY_EXPIRED: return "NEED_BY_EXPIRED";
      case RETRY_DATE_EXPIRED: return "RETRY_DATE_EXPIRED";
      case END_OF_ROTA: return "END_OF_ROTA";
      case EMAIL_OTHER: return "EMAIL_OTHER";
      case DUE_DATE_PENDING: return "DUE_DATE_PENDING";
      case OVERDUE_ALERT: return "OVERDUE_ALERT";
      case RECALL_ALERT: return "RECALL_ALERT";
      case NEW_REQUESTS: return "NEW_REQUESTS";
      case RES_UNREAD_MSGS: return "RES_UNREAD_MSGS";
      case NEED_BY_PENDING: return "NEED_BY_PENDING";
      case HOLD_DATE_EXPIRED: return "HOLD_DATE_EXPIRED";
      case EXPIRY_PENDING: return "EXPIRY_PENDING";
      case CANCEL_REQUEST: return "CANCEL_REQUEST";
      case RENEWAL_REQUEST: return "RENEWAL_REQUEST";
      case DUE_DATE_EXPIRED: return "DUE_DATE_EXPIRED";
      case OVERDUE_LIST: return "OVERDUE_LIST";
      default: return null;
    }
  }

  public String toIso() {
    switch (this) {
      case REQ_UNREAD_MSGS: return "REQ_UNREAD";
      case CONDITIONAL_QUERY: return "REQ_COND_REQ";
      case EXPIRED_ALERT: return "REQ_EXPIRED";
      case NEED_BY_EXPIRED: return "REQ_NEED_BY_EXPIRED";
      case RETRY_DATE_EXPIRED: return "REQ_RETRY_EXPIRED";
      case END_OF_ROTA: return "REQ_EOR";
      case EMAIL_OTHER: return "REQ_OTHER";
      case DUE_DATE_PENDING: return "REQ_DUE_PENDING";
      case OVERDUE_ALERT: return "REQ_OVERDUE";
      case RECALL_ALERT: return "REQ_RECALL";
      case NEW_REQUESTS: return "RESP_NEW_REQUESTS";
      case RES_UNREAD_MSGS: return "RESP_UNREAD";
      case NEED_BY_PENDING: return "RESP_NEED_BY";
      case HOLD_DATE_EXPIRED: return "RESP_HOLD_EXP";
      case EXPIRY_PENDING: return "RESP_EXPIRY_PENDING";
      case CANCEL_REQUEST: return "RESP_CANCEL";
      case RENEWAL_REQUEST: return "RESP_RENEWAL";
      case DUE_DATE_EXPIRED: return "RESP_DUE_EXPIRED";
      case OVERDUE_LIST: return "RESP_OVERDUE";
      default: return null;
    }
  }

  // public Role getRole()    { return myRole; }

  public static RequestCategory fromIso(String iso) {
    if ( iso != null ) {
      if ( iso.equals("REQ_UNREAD") ) return REQ_UNREAD_MSGS;
      if ( iso.equals("REQ_COND_REQ") ) return CONDITIONAL_QUERY;
      if ( iso.equals("REQ_EXPIRED") ) return EXPIRED_ALERT;
      if ( iso.equals("REQ_NEED_BY_EXPIRED") ) return NEED_BY_EXPIRED;
      if ( iso.equals("REQ_RETRY_EXPIRED") ) return RETRY_DATE_EXPIRED;
      if ( iso.equals("REQ_EOR") ) return END_OF_ROTA;
      if ( iso.equals("REQ_OTHER") ) return EMAIL_OTHER;
      if ( iso.equals("REQ_DUE_PENDING") ) return DUE_DATE_PENDING;
      if ( iso.equals("REQ_OVERDUE") ) return OVERDUE_ALERT;
      if ( iso.equals("REQ_RECALL") ) return RECALL_ALERT;
      if ( iso.equals("RESP_NEW_REQUESTS") ) return NEW_REQUESTS;
      if ( iso.equals("RESP_UNREAD") ) return RES_UNREAD_MSGS;
      if ( iso.equals("RESP_NEED_BY") ) return NEED_BY_PENDING;
      if ( iso.equals("RESP_HOLD_EXP") ) return HOLD_DATE_EXPIRED;
      if ( iso.equals("RESP_EXPIRY_PENDING") ) return EXPIRY_PENDING;
      if ( iso.equals("RESP_CANCEL") ) return CANCEL_REQUEST;
      if ( iso.equals("RESP_RENEWAL") ) return RENEWAL_REQUEST;
      if ( iso.equals("RESP_DUE_EXPIRED") ) return DUE_DATE_EXPIRED;
      if ( iso.equals("RESP_OVERDUE") ) return OVERDUE_LIST;
    }
    return null;
  }

   // Requester Category Enumeration
  //public static final RequestCategory REQ_UNREAD_MSGS = new RequestCategory("Unread Messages", "REQ_UNREAD", Role.REQUESTER);
  //public static final RequestCategory CONDITIONAL_QUERY = new RequestCategory("Conditional Query", "REQ_COND_REQ", Role.REQUESTER);
  //public static final RequestCategory EXPIRED_ALERT = new RequestCategory("EXPIRY Alert", "REQ_EXPIRED", Role.REQUESTER );
  //public static final RequestCategory NEED_BY_EXPIRED = new RequestCategory("Needed By Expired", "REQ_NEED_BY_EXPIRED", Role.REQUESTER);
  //public static final RequestCategory RETRY_DATE_EXPIRED = new RequestCategory("Retry Date Expired", "REQ_RETRY_EXPIRED", Role.REQUESTER);
  //public static final RequestCategory END_OF_ROTA = new RequestCategory("End of Rota", "REQ_EOR", Role.REQUESTER );
  //public static final RequestCategory EMAIL_OTHER = new RequestCategory("Email/Other Requests", "REQ_OTHER", Role.REQUESTER );
  //public static final RequestCategory DUE_DATE_PENDING = new RequestCategory("Due Date Pending", "REQ_DUE_PENDING", Role.REQUESTER );
  //public static final RequestCategory OVERDUE_ALERT = new RequestCategory("OVERDUE Alert", "REQ_OVERDUE" , Role.REQUESTER);
  //public static final RequestCategory RECALL_ALERT = new RequestCategory("RECALL Alert", "REQ_RECALL", Role.REQUESTER );

  // Responder Category Enumeration
  //public static final RequestCategory NEW_REQUESTS= new RequestCategory("New Requests", "RESP_NEW_REQUESTS", Role.RESPONDER);
  //public static final RequestCategory RES_UNREAD_MSGS = new RequestCategory("Unread Messages", "RESP_UNREAD", Role.RESPONDER);
  //public static final RequestCategory NEED_BY_PENDING = new RequestCategory("Needed By Pending", "RESP_NEED_BY", Role.RESPONDER);
  //public static final RequestCategory HOLD_DATE_EXPIRED = new RequestCategory("Hold Date Expired", "RESP_HOLD_EXP", Role.RESPONDER);
  //public static final RequestCategory EXPIRY_PENDING = new RequestCategory("EXPIRY Pending", "RESP_EXPIRY_PENDING", Role.RESPONDER);
  //public static final RequestCategory CANCEL_REQUEST = new RequestCategory("Cancel Request", "RESP_CANCEL", Role.RESPONDER);
  //public static final RequestCategory RENEWAL_REQUEST = new RequestCategory("Renewal Request", "RESP_RENEWAL", Role.RESPONDER);
  //public static final RequestCategory DUE_DATE_EXPIRED = new RequestCategory("Due Date Expired", "RESP_DUE_EXPIRED", Role.RESPONDER);
  //public static final RequestCategory OVERDUE_LIST = new RequestCategory("OVERDUE List", "RESP_OVERDUE", Role.RESPONDER);

  //static void addToMappings(String iso, RequestCategory cat){
  //      mappings.put(iso, cat);
  //}
}
