package com.k_int.openrequest.integration.iface;

public enum MessageType {

  ILL_REQUEST, FORWARD_NOTIFICATION, SHIPPED, ILL_ANSWER, CONDITIONAL_REPLY, CANCEL, CANCEL_REPLY, RECEIVED, RECALL, RETURNED, CHECKED_IN, OVERDUE, RENEW, RENEW_ANSWER, LOST, DAMAGED, MESSAGE, STATUS_QUERY, STATUS_OR_ERR_REPORT, EXPIRED;

  public String toString() { 
    switch ( this ) {  
      case ILL_REQUEST: return "ILL Request";
      case FORWARD_NOTIFICATION: return "Forward Notification";
      case SHIPPED: return "Shipped";
      case ILL_ANSWER: return "ILL Answer";
      case CONDITIONAL_REPLY: return "Conditional Reply";
      case CANCEL: return "Cancel";
      case CANCEL_REPLY: return "Cancel Reply";
      case RECEIVED: return "Received";
      case RECALL: return "Recall";
      case RETURNED: return "Returned";
      case CHECKED_IN: return "Checked In";
      case OVERDUE: return "Overdue";
      case RENEW: return "Renew";
      case RENEW_ANSWER: return "Renew Answer";
      case LOST: return "Lost";
      case DAMAGED: return "Damaged";
      case MESSAGE: return "Message";
      case STATUS_QUERY: return "Status Query";
      case STATUS_OR_ERR_REPORT: return "Status Or Error Report";
      case EXPIRED: return "Expired";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case ILL_REQUEST: return 1;
      case FORWARD_NOTIFICATION: return 2;
      case SHIPPED: return 3;
      case ILL_ANSWER: return 4; 
      case CONDITIONAL_REPLY: return 5;
      case CANCEL: return 6;
      case CANCEL_REPLY: return 7;
      case RECEIVED: return 8;
      case RECALL: return 9;
      case RETURNED: return 10;
      case CHECKED_IN: return 11;
      case OVERDUE: return 12;
      case RENEW: return 13;
      case RENEW_ANSWER: return 14;
      case LOST: return 15;
      case DAMAGED: return 16;
      case MESSAGE: return 17;
      case STATUS_QUERY: return 18;
      case STATUS_OR_ERR_REPORT: return 19;
      case EXPIRED: return 20;
      default: return 0;
    }
  }

  public static MessageType fromIso(int iso){
    switch ( iso ) {
      case 1: return ILL_REQUEST;
      case 2: return FORWARD_NOTIFICATION;
      case 3: return SHIPPED;
      case 4: return ILL_ANSWER;
      case 5: return CONDITIONAL_REPLY;
      case 6: return CANCEL;
      case 7: return CANCEL_REPLY;
      case 8: return RECEIVED;
      case 9: return RECALL;
      case 10: return RETURNED;
      case 11: return CHECKED_IN;
      case 12: return OVERDUE;
      case 13: return RENEW;
      case 14: return RENEW_ANSWER;
      case 15: return LOST;
      case 16: return DAMAGED;
      case 17: return MESSAGE;
      case 18: return STATUS_QUERY;
      case 19: return STATUS_OR_ERR_REPORT;
      case 20: return EXPIRED;
      default: return null;
    }
  }
}
