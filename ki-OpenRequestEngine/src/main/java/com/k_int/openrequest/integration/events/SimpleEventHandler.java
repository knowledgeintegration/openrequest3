package com.k_int.openrequest.integration.events;

import com.k_int.openrequest.services.*;
import com.k_int.openrequest.api.*;
import org.springframework.context.ApplicationContext;

// For different protocol message DTO's
import com.k_int.openrequest.integration.iface.*;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.commons.logging.*;
import java.util.*;


public class SimpleEventHandler extends AbstractMessageRecipient {

  /**
   * Used to send messages back to the system workflow controller
   */
  private MessageDispatcher dispatcher = null;
  private static Log log = LogFactory.getLog(SimpleEventHandler.class);
  private Map handlers = new HashMap();


  public SimpleEventHandler() {
    super();
    log.debug("SimpleEventHandler::SimpleEventHandler()");
    handlers.put("ILLind",null);
    handlers.put("C-REPind+",null);
    handlers.put("C-REPind-",null);
    handlers.put("CANind",null);
    handlers.put("RCVind",null);
    handlers.put("LSTind",null);
    handlers.put("MSGind",null);
    handlers.put("STQind",null);
    handlers.put("STRind",null);
    handlers.put("RENind",null);
    handlers.put("DAMind",null);
    handlers.put("RETind",null);
    handlers.put("FWDind",null);
    handlers.put("ANSind-CO",null);
    handlers.put("ANSind-RY",null);
    handlers.put("ANSind-UN",null);
    handlers.put("ANSind-PL",null);
    handlers.put("ANSind-WS",null);
    handlers.put("ANSind-HP",null);
    handlers.put("ANSind-ES",null);
    handlers.put("CARind+",null);
    handlers.put("CARind-",null);
    handlers.put("SHIind",null);
    handlers.put("CHKind",null);
    handlers.put("RCLind",null);
    handlers.put("DUEind",null);
    handlers.put("REAind+",null);
    handlers.put("REAind-",null);
    handlers.put("EXPind",null);
  }

  /**
   * Called by the framework whenever a message is sent on the "SYSTEM" Channel. Events
   * are basically the ISO System event names (For ISO Requests, N.B. that sometimes, this
   * handler may recieve notifications of non iso events, and should be prepared to
   * igore events it's not interested in. The class should not assume that message
   * will always be an instance of an ISO Message Header object if the event type is
   * not an ISO indication!). NB also, message processing is parallel, so the system
   * handler which stores the message, although called first, may not have completed by
   * the time this handler is invoked.
   * @param event A string defining the event that is taking place, EG ILLind
   * @param message An object containing the message data.
   * @return an optional result object - Not used for this handler and should be null!
   */
  public Object handle(String event, Object message) throws MessageHandlerException {
    log.debug("handle "+event);
    return null;
  }

  /**
   * init() - Called by the spring dependency inversion framework at object creation time.
   * Sets up any needed class varibales.
   * @return void
   */
  public void init() {
    log.info("Initialise Simple Message Handler");

    // Get hold of the system message dispatcher
    dispatcher = (MessageDispatcher) ctx.getBean("DISPATCHER");

    // Register this handler as being interested in anything in the System Channel
    dispatcher.registerObserver("ASE", this);
  }

  public void stop() {
    log.info("Stop SystemMessageRecipient");
  }
}
