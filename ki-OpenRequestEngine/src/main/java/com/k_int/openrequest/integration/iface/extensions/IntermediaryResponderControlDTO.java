package com.k_int.openrequest.integration.iface.extensions;

import com.k_int.openrequest.integration.iface.*;
import com.k_int.openrequest.db.SystemId;

import java.util.Date;
import java.util.List;

/**
 * Title:       IntermediaryResponderControlDTO
 * @version:    $Id: IntermediaryResponderControlDTO.java,v 1.2 2005/06/10 18:42:50 ibbo Exp $
 * Copyright:   Copyright (C) 2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class IntermediaryResponderControlDTO {

  public long transaction_results;
  public ResultsExplanationDTO results_explanation;
  public SystemId responder_id;

  public IntermediaryResponderControlDTO() {
  }

  public IntermediaryResponderControlDTO(long transaction_results,
                                         ResultsExplanationDTO results_explanation,
                                         SystemId responder_id) {
    this.transaction_results = transaction_results;
    this.results_explanation = results_explanation;
    this.responder_id = responder_id;
  }

  public long getTransactionResults() {
    return transaction_results;
  }

  public void setTransactionResults(long transaction_results) {
    this.transaction_results = transaction_results;
  }

  public ResultsExplanationDTO getResultsExplanation() {
    return results_explanation;
  }

  public void setResultsExplanation(ResultsExplanationDTO results_explanation) {
    this.results_explanation = results_explanation;
  }

  public SystemId getResponderId() {
    return responder_id;
  }

  public void setResponderId(SystemId responder_id) {
    this.responder_id = responder_id;
  }
}
