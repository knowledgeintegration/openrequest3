package com.k_int.openrequest.integration.iface;

import java.util.Hashtable;

/**
 * Title:       ResultSetDTO
 * @version:    $Id: ResultSetDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ResultSetDTO implements java.io.Serializable {

  public String status = null;
  public long count;
  public SearchResultEntryDTO[] results;

  public ResultSetDTO() {
  }

  public ResultSetDTO(ResultStatus status, long count, SearchResultEntryDTO[] results) {
    this.status = status.toString();
    this.count = count;
    this.results = results;
  }

  public long getCount() {
    return count;
  }

  public void setCount(long count) {
    this.count = count;
  }

  public ResultStatus getStatus() {
    return ResultStatus.OK;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public SearchResultEntryDTO[] getResults() {
    return results;
  }

  public void setResults(SearchResultEntryDTO[] results) {
    this.results=results;
  }
  
}
