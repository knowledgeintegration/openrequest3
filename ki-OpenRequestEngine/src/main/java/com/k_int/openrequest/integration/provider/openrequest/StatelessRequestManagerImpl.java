package com.k_int.openrequest.integration.provider.openrequest;

import java.math.BigInteger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.*;
import java.io.StringWriter;
import org.hibernate.*;
import org.hibernate.type.*;
import com.k_int.openrequest.api.MessageDeliveryObserver;
import com.k_int.openrequest.actions.workflow.NextResponderRequest;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.db.folders.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import com.k_int.QueryDescriptor.*;
import com.k_int.openrequest.integration.iface.*;
import com.k_int.openrequest.integration.provider.base.*;
import com.k_int.oql.iface.*;
import com.k_int.oql.util.*;
import org.springframework.context.*;

public class StatelessRequestManagerImpl implements StatelessRequestManager, ApplicationContextAware {

  private static Log log = LogFactory.getLog(StatelessRequestManagerImpl.class);

  protected static QueryDescriptor qd = null;

  protected static final String[] REQUEST_SEARCH_FIELDS = { "Unused",
                                                          "RequesterReference",
                                                          "Author",
                                                          "Title",
                                                          "ISBN",
                                                          "ISSN",
                                                          "ResponderRef",
                                                          "Borrower" };

  // Populate the template query descriptior....
  static {
      qd = new QueryDescriptor();
      QueryScope transaction_group_scope = qd.createRootScope("com.k_int.openrequest.db.ILLTransactionGroup","tg");
      QueryScope last_transaction = qd.createJoinScope("tg","lastTransaction","last_trans","left outer");
      QueryScope item_details = qd.createJoinScope("tg","requiredItemDetails","item_details","left outer");

      // Outer join between last trans and it's status (Since we might not have a last trans, we can't auto assume
      // we will be able to get a status for it)
      QueryScope last_trans_status = qd.createJoinScope("last_trans","requestStatusCode","last_trans_status","left outer");
      QueryScope last_trans_model = qd.createJoinScope("last_trans","activeStateModel","last_trans_model","left outer");

      ElementSet es = new ElementSet("Default");
      es.addCountElement("Count","tg","id");
      qd.registerElementSet(es);

      ElementSet qr_element_set = new ElementSet("QueryResult");
      qd.registerElementSet(qr_element_set);
      qr_element_set.addScopedElement("ID","tg","id");
      qr_element_set.addScopedElement("TGQ","tg","TGQ");
      qr_element_set.addScopedElement("RequesterReference","tg","requesterReference");
      qr_element_set.addScopedElement("Role","tg","role");
      qr_element_set.addScopedElement("Title","item_details","title");
      qr_element_set.addScopedElement("Author","item_details","author");
      qr_element_set.addScopedElement("PublicationDate","item_details","publicationDate");
      qr_element_set.addScopedElement("ResponderSymbol","last_trans","responderSymbol");
      qr_element_set.addScopedElement("LastMessageDate","tg","lastMessageDate");
      qr_element_set.addScopedElement("Code","last_trans_status","code");
      // qr_element_set.addScopedElement("RequestStatusCode.Code","last_trans","RequestStatusCode.Code");
      qr_element_set.addScopedElement("Active","tg","active");
      qr_element_set.addScopedElement("TransactionGroupStatusCode.Code","tg","transactionGroupStatusCode.code");
      qr_element_set.addScopedElement("UnreadMessageCount","tg","unreadMessageCount");
      qr_element_set.addScopedElement("Protocol","last_trans","protocol");
      qr_element_set.addScopedElement("InitiatingBranchCode","tg","initiatingBranchCode");
      // qr_element_set.addScopedElement("service_types.elements", "item_details", "service_types.elements");
      qr_element_set.addScopedElement("id","last_trans","id");
      qr_element_set.addScopedElement("Model","last_trans_model","code");

      qd.setDefaultElementSetName("Default");

      BooleanNode bn = new BooleanNode("RootAnd",BooleanNode.BOOL_OP_AND);

      //public ExpressionNode(String name, String scope, String access_path, int type, String id, Object default_value, Object editing_info)

      bn.getComponents().add( new ExpressionNode("TransactionGroup.ID", "tg", "id", ExpressionNode.VARCHAR_EXPRESSION, "Id", null, null));
      bn.getComponents().add( new ExpressionNode("Location", "tg", "location.id", ExpressionNode.LONG_EXPRESSION, "LocationId", null, null));
      bn.getComponents().add( new ExpressionNode("Role", "tg", "role", ExpressionNode.VARCHAR_EXPRESSION, "Role", null, null));
      bn.getComponents().add( new ExpressionNode("RequesterReference", "tg", "requesterReference", ExpressionNode.VARCHAR_EXPRESSION, "RequesterReference", null, null));
      bn.getComponents().add( new ExpressionNode("ISSN", "item_details", "ISSN", ExpressionNode.VARCHAR_EXPRESSION, "ISSN", null, null));
      bn.getComponents().add( new ExpressionNode("ISBN", "item_details", "ISBN", ExpressionNode.VARCHAR_EXPRESSION, "ISBN", null, null));
      bn.getComponents().add( new ExpressionNode("Title", "item_details", "title", ExpressionNode.VARCHAR_EXPRESSION, "Title", null, null));
      bn.getComponents().add( new ExpressionNode("Author", "item_details", "author", ExpressionNode.VARCHAR_EXPRESSION, "Author", null, null));
      bn.getComponents().add( new ExpressionNode("RequestStatusCode.Code", "last_trans", "requestStatusCode.Code", ExpressionNode.VARCHAR_EXPRESSION, "ReqStatusCode", null, null));
      bn.getComponents().add( new ExpressionNode("TransactionGroupStatusCode.Code", "tg",
                                                 "transactionGroupStatusCode.code", 
                                                 ExpressionNode.VARCHAR_EXPRESSION, 
                                                 "ReqPhaseCode", null, null));
      qd.setQuery(bn);
  }


  private ApplicationContext app_context = null;

  public void setApplicationContext(ApplicationContext app_context) {
    this.app_context = app_context;
  }

  public void init() {
    log.debug("StatelessRequestManagerImpl::init");
  }

  public String getVersion() throws RequestManagerException {
    String name="OpenRequest";
    if ( app_context.containsBean("SystemName") )
      name = app_context.getBean("SystemName").toString();

    return name+":"+com.k_int.openrequest.services.Controller.or_props.get("app.version");
  }

  /**
   * An internal API call that returns all unread messages for a given location.
   * It is the callers responsibility to ensure that the caller is allowed to view
   * the unread messages at a given location.
   */
  public BriefMessageInfoDTO[] getUnreadMessages(Long location_id) {
    Session session = null;
    BriefMessageInfoDTO[] result = null;

    try {
      SessionFactory sf = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      session = sf.openSession();

      List l = session.createQuery("Select msg from com.k_int.openrequest.db.MessageHeader msg where msg.parentRequest.location.id = ? and msg.read = 0")
                     .setParameter(0,location_id,Hibernate.LONG)
                     .list();
      // Search for messages;
      result=new BriefMessageInfoDTO[l.size()];

      log.debug("getUnreadMessages for location "+location_id+" returned list of size "+l.size());

      int ctr = 0;
      for ( Iterator i = l.iterator(); i.hasNext(); ) {
        com.k_int.openrequest.db.MessageHeader msg = (com.k_int.openrequest.db.MessageHeader) i.next();

        if ( msg != null ) {
          String from_state = null;
          String to_state = null;

          if ( msg.getFromState() != null )
            from_state = msg.getFromState().getCode();

          if ( msg.getToState() != null )
            to_state = msg.getToState().getCode();

          result[ctr++] = new BriefMessageInfoDTO(msg.getMessageType(),
                                                  msg.getEventCode(),
                                                  (java.util.Date)msg.getTimestamp(),
                                                  msg.getRead(),
                                                  msg.getId().toString(),
                                                  from_state,
                                                  to_state,
                                                  msg.getNote());
        }
        else {
          log.warn("Message returned by getUnreadMessages was NULL");
        }
      }
     
      session.flush();
      session.connection().commit();
    }
    catch ( Exception e ) {
      log.fatal("Unhandled Excepton",e);
    }
    finally {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }

    return result;
  }

  /** 
   * Set the message read status of the given message
   */
  public Boolean setMessageReadStatus(Long location_id, Long message_id) {
    log.debug("setMessageReadStatus "+location_id+","+message_id);
    Session session = null;
    try
    {
      SessionFactory sf = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      session = sf.openSession();

      MessageHeader mh = DBHelper.lookupMessage(session,message_id);
      mh.setRead(true);

      ILLTransaction trans = mh.getParentRequest();
      if ( trans.getUnreadMessageCount() > 0 )
        trans.decrementUnreadMessageCounter();

      ILLTransactionGroup tg = trans.getTransactionGroup();
      if ( tg.getUnreadMessageCount() > 0 )
        tg.decrementUnreadMessageCounter();
 
      session.merge(mh);
      session.merge(mh.getParentRequest());
      session.flush();
      session.connection().commit();
    }
    catch ( java.sql.SQLException sqle )
    {
      log.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      log.warn("Problem",he);
    }
    finally
    {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
    return Boolean.TRUE;
  }

  /** Create a new transaction group with the supplied details and rota.
   * @param location_id ID of the requesting location
   * @param request_data Generic details about the item required.
   * @param rota List of locations to which this request may be sent.
   * @return The ID of the created TransactionGroup
   */
  public Long createRequest(RequesterViewDTO request_data,
                            RotaElementDTO[] rota,
                            String requester_symbol) throws RequestManagerException, DuplicateUserReferenceException {
    return createTransGroup(request_data,rota,null,requester_symbol);
  }

  /** Create a new transaction group with the supplied details and rota.
   * @param location_id ID of the requesting location
   * @param request_data Generic details about the item required.
   * @param rota List of locations to which this request may be sent.
   * @param mandatory_tgq The TGQ that must be used (Must be unique)
   * @return the ID of the created TransactionGroup
   */
  public Long createTransGroup(RequesterViewDTO request_data,
                               RotaElementDTO[] rota,
                               String mandatory_tgq,
                               String requester_symbol) throws RequestManagerException, DuplicateUserReferenceException {
    Long result = null;
    log.debug("createTransGroup");
    try {
      SessionFactory sf = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      result = createTransGroupHelper(sf,request_data,rota,mandatory_tgq,requester_symbol);
    }
    catch ( Exception e ) {
      log.fatal("Unhandled Excepton",e);
    }
    finally {
      log.debug("createTransGroup completed, returning "+result);
    }

    return result;
  }

  /** (Re)Start the ILL Messaging process for the identified group.
   *  This method starts the messaging, but does not wait for the system to send the first request. and is
   *  essentially "Fire-And-Forget".
   * @param transaction_group_id ID of group to start
   */
  public void startMessaging(Long transaction_group_id) throws RequestManagerException {
    log.debug("startMessaging "+transaction_group_id);
    try {
      MessageDispatcher md = (MessageDispatcher) app_context.getBean("DISPATCHER");
      md.dispatch("System","TG_NEXT",transaction_group_id);
    }
    catch ( Exception e ) {
      throw new RequestManagerException("problem dispatching TG_NEXT in rota",e);
    }
  }

  /** (Re)Start the ILL Messaging process for the identified group and return the next ILL Transaction ID.
   *  This method waits for the ILL to be sent before returning, and ensures that a transaction ID
   *  can be handed back to the caller.
   * @param transaction_group_id ID of group to start
   * @return id of new ILL transaction
   */
  public Long startMessagingSync(Long transaction_group_id) throws RequestManagerException {
    log.debug("startMessagingSync "+transaction_group_id);
    Long result = null;
    try {
      MessageDispatcher md = (MessageDispatcher) app_context.getBean("DISPATCHER");

      // Send a message to the system asking for the transaction group to move to the next
      // request in the rota
      MessageDeliveryObserver mdo = new MessageDeliveryObserver() {

        public Long new_trans_id = null;
        public int error = 0;

        public void notify(Long tg_id,
                           Long trans_id,
                           String message,
                           Object additional_data) {
          log.debug("got notification that tg "+tg_id+" has a new request "+trans_id);
          new_trans_id = trans_id;
          synchronized(this) {
            this.notifyAll();
          }
        }

        public void notifyError(int error_code) {
          error = error_code;
          synchronized(this) {
            this.notifyAll();
          }
        }


        public Long getTransId() { return new_trans_id; }
        public int getError() { return error; }
      };

      NextResponderRequest nrr = new NextResponderRequest(transaction_group_id, mdo);
      md.dispatch("System","TG_NEXT",nrr);

      // Wait for up to 60 seconds
      long timeout_time = System.currentTimeMillis()+60000;

      while ( ( mdo.getTransId() == null ) &&
              ( mdo.getError() == 0 ) &&
              ( System.currentTimeMillis() < timeout_time ) ) {
        log.debug("Waiting on message delivery observer for new transaction id");
        synchronized(mdo) {
          mdo.wait(timeout_time-System.currentTimeMillis());
        }
      }

      if ( mdo.getTransId() == null ) {
        throw new RequestManagerException("problem dispatching TG_NEXT in rota - TIMEOUT");
      }

      result = mdo.getTransId();
    }
    catch ( Exception e ) {
      throw new RequestManagerException("problem dispatching TG_NEXT in rota",e);
    }

    return result;
  }

  public RequestCountDTO[] retrieveRequestCounts(Long loc_id) throws RequestManagerException {
    RequestCountDTO[] result = null;
    try {
      SessionFactory sf = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      result = retrieveRequestCounts(sf,loc_id);
    }
    catch ( Exception e ) {
      log.fatal("Unhandled Excepton",e);
    }
    finally {
    }
    
    return result;
  }


  public ResultSetDTO search(RequestManagerQueryDTO query)  throws RequestManagerException {
    SessionFactory sf = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");

    if ( query == null ) 
      throw new RequestManagerException("No Query Supplied");

    return searchQuery(sf, query);
  }

  public String[] listValidActions(String input_model, String input_state, Boolean user_selectable_only) throws RequestManagerException {
    String[] result = null;
    Session session = null;
    SessionFactory sf = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
    try {
      session = sf.openSession();
      List l = null;
      if ( ( user_selectable_only != null ) && ( user_selectable_only.equals(Boolean.TRUE) ) ) {
        Query q = session.createQuery("select st from com.k_int.openrequest.db.StateTransition st where st.model.code=? and st.from_state.code=? and st.userSelectable=?");
        q.setParameter(0,input_model,Hibernate.STRING);
        q.setParameter(1,input_state,Hibernate.STRING);
        q.setParameter(2,Boolean.TRUE,Hibernate.BOOLEAN);
        l = q.list();
      }
      else {
        Query q = session.createQuery("select st from com.k_int.openrequest.db.StateTransition st where st.model.code=? and st.from_state.code=?");
        q.setParameter(0,input_model,Hibernate.STRING);
        q.setParameter(1,input_state,Hibernate.STRING);
        l = q.list();
      }

      result = new String[l.size()];
      int ctr=0;

      for ( Iterator i = l.iterator(); i.hasNext(); ) {
        com.k_int.openrequest.db.StateTransition st = (com.k_int.openrequest.db.StateTransition) i.next();
        result[ctr++] = st.getTransitionCode();
      }
    }
    catch ( Exception e ) {
      log.warn("Problem listValidActions",e);
    }
    finally
    {
      log.debug("Done");
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }

    return result;
  }


  /**
   * Lookup the location with the suppled ID
   * @param id ID to look up
   * @return A DTO of the the location info
   */
  public ORLocationInfoDTO lookupLocation(Long id)  throws RequestManagerException {
    return null;
  }

  /**
   * Create the specified location
   * @param loc_info Location Info
   * @return ID of newly created location.
   */
  public Long createLocation(ORLocationInfoDTO loc_info)  throws RequestManagerException {
    Long result = null;
    Session session = null;
    SessionFactory sf = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
    try {
      session = sf.openSession();
      // Lookup or create location name
      Location new_loc = Location.lookupOrCreateByName(session, loc_info.getLocationName(), loc_info.getSymbol());
    
      // Lookup or create ILL system
      com.k_int.openrequest.db.Location.Service serv = com.k_int.openrequest.db.Location.Service.lookupOrCreate(session,loc_info.getAddressType(), loc_info.getAddress());
      if ( ( loc_info.getAddressType() != null ) && ( loc_info.getAddress() != null ) ) {
        serv.setTelecomServiceAddress(loc_info.getAddress());
        if ( loc_info.getAddressType().equals("M") ) {
          serv.setTelecomServiceIdentifier("SMTP");
        }
        else {
          serv.setTelecomServiceIdentifier("TCP");
        }
      }

      // Lookup or create Symbol
      String alias = loc_info.getSymbol();

      int pos = alias.indexOf(":");
      String alias_auth_part = alias.substring(0,pos);
      String alias_name_part = alias.substring(pos+1);
      NamingAuthority auth = DBHelper.lookupOrCreateAuthority(session,alias_auth_part,null);
      Long symbol_id = createLocationSymbol(alias, alias_name_part,auth,new_loc,serv,session);

      result = new_loc.getId();

      session.flush();
      session.connection().commit();
    }
    catch ( Exception e ) {
      log.warn("Problem createLocation",e);
    }
    finally
    {
      log.debug("createLocation Done, result is: "+result);
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }
    return result;
  }

  /**
   * Lookup the symbol with the suppled authority:symbol
   * @param symbol symbol to look up.
   * @return A DTO of the the symbol info
   */
  public ORSymbolInfoDTO lookupSymbol(String symbol) throws RequestManagerException {
    return null;
  }

  /**
   * Create the specified symbol
   * @param symbol symbol to create
   * @return ID of newly created location.
   */
  public Long createSymbol(ORSymbolInfoDTO symbol) throws RequestManagerException {
    return null;
  }

  /* Helpers */

  protected static Long createTransGroupHelper(SessionFactory sf,
                                               RequesterViewDTO request, 
                                               RotaElementDTO[] rota,
                                               String mandatory_tgq,
                                               String requester_symbol) 
                           throws RequestManagerException, DuplicateUserReferenceException {
    Long retval = null;
    log.debug("We will be creating a request for location "+requester_symbol);

    Session session = null;

    try {
      session = sf.openSession();

      LocationSymbol requester_loc = DBHelper.lookupLocation(session,requester_symbol);
      String tgq = null;

      if ( ( mandatory_tgq != null ) && ( mandatory_tgq.length() > 0 ) ) {
        tgq = mandatory_tgq;
        ILLTransactionGroup dup_test = DBHelper.findExistingTG(requester_loc.getCanonical_location(),
                                                               requester_symbol,
                                                               tgq,
                                                               session);
        if ( dup_test != null ) {
          throw new RequestManagerException("Duplicate TGQ Used for mandatory tgq");
        }
      }
      else {
        tgq = "OR:"+com.k_int.openrequest.helpers.Constants.getNewUniqueId();
      }

      log.debug("New TGQ will be : "+tgq);

      ILLTransactionGroup tg = new ILLTransactionGroup(requester_loc.getCanonical_location(),
                                                       requester_symbol,
                                                       tgq,
                                                       "REQ");

      log.debug("Create base details");

      tg.setRequiredItemDetails( new RequiredItemDetails() );
      tg.setCurrentRotaPosition(-1);
      tg.setRequesterSymbol(requester_symbol);
      tg.setActiveStateModel(DBHelper.lookupStateModel(session,"REQ/TG"));
      tg.setTransactionGroupStatusCode(DBHelper.lookupStatusCode(session,"IDLE"));

      if ( request.getRequesterSpecific() != null )
      {
        tg.setPatronName(request.getRequesterSpecific().getBorrowerName());
        tg.setPatronStatus(request.getRequesterSpecific().getBorrowerType());
        tg.setPatronSymbol(request.getRequesterSpecific().getBorrowerId());
        tg.setInitiatingBranchCode(request.getRequesterSpecific().getInitiatingBranch());
      }

      // ToDo: Had to hard code this for now.... workaround on way
      tg.setRequesterReferenceAuthority(request.ill_request.requester_ref_authority);
      tg.setRequesterReference(request.ill_request.requester_ref);

      // TransactionType
      log.debug("Delivery Address");
      // Delivery Address... request.ill_request.delivery_address.
      if ( request.ill_request.delivery_address != null ) {
        tg.setCurrentDeliveryAddress( new ISO10161Address(0, 
                                                          request.ill_request.delivery_address.name,
                                                          request.ill_request.delivery_address.extended_postal,
                                                          request.ill_request.delivery_address.street_and_number,
                                                          request.ill_request.delivery_address.post_office_box,
                                                          request.ill_request.delivery_address.city,
                                                          request.ill_request.delivery_address.region,
                                                          request.ill_request.delivery_address.country,
                                                          request.ill_request.delivery_address.postcode,
                                                          null,
                                                          null) );
      }

      log.debug("Delivery service");
      // Delivery Service request.ill_request.delivery_service
      // ... Is an instance of DeliveryServiceDTO which is a superclass of ElectronicDeliveryDTO and
      // PhysicalDeliveryDTO. field request.ill_request.delivery_service.delivery_type contains discriminator
      if ( request.ill_request.delivery_service != null )
      {
        if ( request.ill_request.delivery_service instanceof PhysicalDeliveryDTO ) {
          // TODO:
        }
        else if (  request.ill_request.delivery_service instanceof ElectronicDeliveryDTO ) {
          // TODO:
        }
      }

      // Billing Address request.billing_address
      if ( request.ill_request.billing_address != null ) {
        tg.setCurrentBillingAddress( new ISO10161Address(0, 
                                                         request.ill_request.billing_address.name,
                                                         request.ill_request.billing_address.extended_postal,
                                                         request.ill_request.billing_address.street_and_number,
                                                         request.ill_request.billing_address.post_office_box,
                                                         request.ill_request.billing_address.city,
                                                         request.ill_request.billing_address.region,
                                                         request.ill_request.billing_address.country,
                                                         request.ill_request.billing_address.postcode,
                                                         null,
                                                         null) );
      }

      // ServiceType: request.ill_request.service_type (.LOAN or .COPY...)
      // tg.getRequiredItemDetails().getService_types().add(new Integer(request.ill_request.service_type));
      if ( request.ill_request.requested_service_type != null ) {
        for ( Iterator i = request.ill_request.requested_service_type.iterator(); i.hasNext(); ) {
          tg.getRequiredItemDetails().getService_types().add(new Integer( ((ServiceType)(i.next())).toIso()));
          // tg.getRequiredItemDetails().getService_types().add( ServiceType.fromIso(((Integer)(i.next())).intValue() ) );
        }
      }

      // Requester Optional Messages: request.ill_request.req_opt_messages
      if ( request.ill_request.req_opt_messages != null ) {
        tg.setCanSendReceived(new Boolean(request.ill_request.req_opt_messages.canSendReceived()));
        tg.setCanSendReturned(new Boolean(request.ill_request.req_opt_messages.canSendReturned()));
        tg.setRequesterShipped(new Long(request.ill_request.req_opt_messages.getRequesterShipped().toIso()));
        tg.setRequesterCheckedIn(new Long(request.ill_request.req_opt_messages.getRequesterCheckedIn().toIso()));
      }

      // Search Type: request.ill_request.search_type
      if ( request.ill_request.search_type != null ) {
        if ( request.ill_request.search_type.need_before_date != null ) {
          tg.setNeedByDate(request.ill_request.search_type.need_before_date);
        }
      }

      // Process SearchTypeDTO request.getSearchType
      // LevelOfService
      if ( request.ill_request.getSearchType() != null ) {
        log.debug("Request has search type data");
        // TODO: Other attributes from SearchType
        if ( request.ill_request.getSearchType().getLevelOfService() != null )
          tg.getRequiredItemDetails().setLevelOfService(""+request.ill_request.getSearchType().getLevelOfService().toIso());
        if ( request.ill_request.getSearchType().getExpiryFlag() != null )
          tg.getRequiredItemDetails().setExpiryFlag((long)(request.ill_request.getSearchType().getExpiryFlag().toIso()));
        tg.getRequiredItemDetails().setNeedBeforeDate(request.ill_request.getSearchType().getNeedBeforeDate());
        log.debug("Setting expiry date: "+request.ill_request.getSearchType().getExpiryDate());
        tg.getRequiredItemDetails().setExpiryDate(request.ill_request.getSearchType().getExpiryDate());
      }
      else
      {
        log.warn("No search type processed for new request... Watch out for expiry defaults");
      }

      // SupplyMediumType[] request.ill_request.required_mediums
      if ( request.ill_request.required_mediums != null ) {
        for ( int rm_counter=0; rm_counter < request.ill_request.required_mediums.length; rm_counter++ ) {
          tg.getRequiredItemDetails().getSupplyMediumInfoType().add(new MediumInfo((long)(request.ill_request.required_mediums[rm_counter]),"none"));
             // N.B. Second param (string) is characteristics, but we just bung the name in for now
        }  
      }

      if ( request.ill_request.getPlaceOnHoldType() != null ) {
        tg.getRequiredItemDetails().setPlaceOnHold(request.ill_request.getPlaceOnHoldType().toIso());
      }
      else {
        tg.getRequiredItemDetails().setPlaceOnHold(3);
      }

      tg.getRequiredItemDetails().setItemType(request.ill_request.item_id.item_type);
      tg.getRequiredItemDetails().setHeldMediumType(request.ill_request.item_id.held_medium_type);

      // ItemId
      tg.getRequiredItemDetails().setAuthor(request.ill_request.item_id.author);
      tg.getRequiredItemDetails().setTitle(request.ill_request.item_id.title);
      tg.getRequiredItemDetails().setSubtitle(request.ill_request.item_id.subtitle);
      tg.getRequiredItemDetails().setSponsoringBody(request.ill_request.item_id.sponsoring_body);
      tg.getRequiredItemDetails().setPlaceOfPublication(request.ill_request.item_id.place_of_publication);
      tg.getRequiredItemDetails().setPublisher(request.ill_request.item_id.publisher);
      tg.getRequiredItemDetails().setPublicationDate(request.ill_request.item_id.publication_date);
      //tg.getRequiredItemDetails().setLanguage(request.ill_request.item_id.language);
      tg.getRequiredItemDetails().setSeriesTitleNumber(request.ill_request.item_id.series_title_number);
      tg.getRequiredItemDetails().setVolume(request.ill_request.item_id.volume_issue);
      tg.getRequiredItemDetails().setIssue(request.ill_request.item_id.volume_issue);
      tg.getRequiredItemDetails().setEdition(request.ill_request.item_id.edition);
      tg.getRequiredItemDetails().setAuthorOfArticle(request.ill_request.item_id.author_of_article);
      tg.getRequiredItemDetails().setTitleOfArticle(request.ill_request.item_id.title_of_article);
      tg.getRequiredItemDetails().setPagination(request.ill_request.item_id.pagination);
      //tg.getRequiredItemDetails().setPublicationDateOfComponent(request.ill_request.item_id.publication_date_of_component);
      tg.getRequiredItemDetails().setCallNumber(request.ill_request.item_id.call_number);
      tg.getRequiredItemDetails().setISBN(request.ill_request.item_id.isbn);
      tg.getRequiredItemDetails().setISSN(request.ill_request.item_id.issn);
      tg.getRequiredItemDetails().setVerificationReferenceSource(request.ill_request.item_id.verification_reference_source);
      tg.getRequiredItemDetails().setCopyrightCompliance(request.ill_request.copyright_compliance);
      tg.getRequiredItemDetails().setSystemNo(request.ill_request.item_id.system_no);
      tg.getRequiredItemDetails().setNatBibNo(request.ill_request.item_id.nat_bib_no);

      // tg.getRequiredItemDetails().setSystemNo(new IPIGSystemNumber(
      // CostInfoTypeDTO cost_info
      if ( request.ill_request.cost_info != null ) {
        tg.getRequiredItemDetails().setAccountNumber(request.ill_request.cost_info.account_number);
        tg.getRequiredItemDetails().setMaxCostCurrencyCode(request.ill_request.cost_info.currency_code);
        tg.getRequiredItemDetails().setMaxCostString(request.ill_request.cost_info.value);
        tg.getRequiredItemDetails().setReciprocalAgreement(request.ill_request.cost_info.reciprocal_agreement);
        tg.getRequiredItemDetails().setWillPayFee(request.ill_request.cost_info.will_pay_fee);
        tg.getRequiredItemDetails().setPaymentProvided(request.ill_request.cost_info.payment_provided);
      }

      // forward_flag

      // requester_note
      tg.setRequesterNote( request.ill_request.requester_note );

      // End of BeILLRequestFields
      // Process ROTA
      log.debug("Processing rota");
      for ( int i=0; i<rota.length; i++ ) {
        log.debug("rota entry "+rota[i].symbol);
        tg.getRota().add(new RotaEntry(false,
                                       0,
                                       rota[i].symbol,
                                       rota[i].getMandatoryTransactionQualifier(),
                                       0,      // Name type
                                       (String)null,   // name
                                       (String)null,   // Account number
                                       (String)null,   // Service Identifier
                                       (String)null)); // Service Address

      }

      // Process any extension elements
      log.debug("Process extensions");
      if ( (request.ill_request.getExtensionDTOs() != null) && ( request.ill_request.getExtensionDTOs().size() > 0 ) ) {
        for ( Iterator i = request.ill_request.getExtensionDTOs().iterator(); i.hasNext(); ) {
          ProtocolMessageExtensionDTO extension = (ProtocolMessageExtensionDTO)i.next();
  
          if ( extension instanceof OCLCRequestExtension ) {
            OCLCRequestExtension oclc_req_extn = (OCLCRequestExtension)extension;
            tg.setClientDepartment(oclc_req_extn.client_department);
            tg.setPaymentMethod(oclc_req_extn.payment_method);
            tg.setAffiliations(oclc_req_extn.affiliations);
            tg.getRequiredItemDetails().setUniformTitle(oclc_req_extn.uniform_title);
            tg.getRequiredItemDetails().setDissertation(oclc_req_extn.dissertation);
            tg.getRequiredItemDetails().setIssue(oclc_req_extn.issue_number);
            tg.getRequiredItemDetails().setVolume(oclc_req_extn.volume);
            tg.getRequiredItemDetails().setSource(oclc_req_extn.source);
          }
          else {
            log.warn("** Unhandled extension of type "+extension.getClass().getName()+" for new Transaction Group");
          }
        }
      }


      session.save(tg);
      session.flush();
      session.connection().commit();
      retval = tg.getId();
      log.debug("Created new transaction group, ID is "+retval);
    }
    catch ( Exception e ) {
      log.error("Problem creating new transaction group, or dispatching TG_NEXT in rota",e);
    }
    finally
    {
      log.debug("Done");
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }

    return retval;
  }

  public static RequestCountDTO[] retrieveRequestCounts(SessionFactory sf, Long loc_id) throws RequestManagerException {

    log.debug("retrieveRequestCounts");
    RequestCountDTO[] result = null;
    int i=0;

    Session session = null;
    try {
      session = sf.openSession();

      log.debug("Retrieve Request Counts for Location id:"+loc_id);

      // this way is ok-ish but its hard to transform the folder code back into the category enum
      List folders = session.createQuery("select f from com.k_int.openrequest.db.folders.FolderHeader f where f.ownerId=?")
                         .setParameter(0,loc_id,Hibernate.LONG)
                         .list();

      log.debug("Found "+folders.size()+" folders for location "+loc_id);

      result = new RequestCountDTO[folders.size()];
      for ( Iterator it = folders.iterator(); it.hasNext(); )
      {
        FolderHeader fh = (FolderHeader)it.next();
        result[i++] = new RequestCountDTO(fh.getCode(), fh.getRequestCount());
      }
      session.flush();
      session.connection().commit();
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("Problem",he);
      throw new RequestManagerException(he.toString());
    }
    catch ( java.sql.SQLException sqle ) {
      log.warn("Problem",sqle);
      throw new RequestManagerException(sqle.toString());
    }
    finally {
      log.debug("Close session");
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }

    return result;
  }

  private static void putIfNotNull(Map m, String name, Object value) {
    if ( value != null ) {
      m.put(name,value);
    }
  }

  /**
   *
   */
  public static ResultSetDTO searchQuery(SessionFactory sf, RequestManagerQueryDTO query) throws RequestManagerException {

    // Use the OQL Builder to construct a query....
    ResultSetDTO result = new ResultSetDTO();

    if ( query != null ) {
      Session session = null;
      try {
        session = sf.openSession();
  
        log.debug("Search...."+query.toString());
  
        Hashtable values = new Hashtable();
  
        putIfNotNull(values,"Role",query.getRole());
        putIfNotNull(values,"Role",query.getRole());
        putIfNotNull(values,"LocationId",query.getLocationId());
        putIfNotNull(values,"ReqStatusCode",  query.getState());
        putIfNotNull(values,"ReqPhaseCode", query.getPhase());
        putIfNotNull(values,"RequesterReference",query.getRequestId());
        putIfNotNull(values,"Author",query.getAuthor());
        putIfNotNull(values,"Title",query.getTitle());
        // values.put("ISBN",query.getIdentifier());
        // values.put("ISSN",query.getIdentifier());
        putIfNotNull(values,"ResponderRef",query.getResponder());
        putIfNotNull(values,"Borrower",query.getPatronId());
  
        log.debug("Query Values : "+values);
  
        Vector bind_vars = new Vector();
        Vector bind_types = new Vector();
        OQLSelectStatement s = com.k_int.oql.util.OQLSelectFactory.createStatement(qd, bind_vars, bind_types, values);
  
        // This is taken care of by Default Element Set
        OQLCollectionScope root_scope = s.lookupCollectionScope("tg");
        OQLCollectionScope lt_scope = s.lookupCollectionScope("last_trans");
  
        log.debug("New statement: "+s);
        log.debug("bind vars: "+bind_vars);
  
        Iterator iter = session.createQuery(s.toString())
                         .setParameters(bind_vars.toArray(), (Type[])bind_types.toArray(new Type[0]))
                         .iterate();
  
        if ( iter.hasNext() ) {
          Long count = (Long) iter.next();
          long hit_count = count.longValue();
          log.debug("Result count: "+hit_count);
          iter = null;
  
          result.setCount(hit_count);
          result.setStatus("OK");
  
          if ( hit_count > 0 ) {
            bind_vars.clear();
            bind_types.clear();
            OQLSelectStatement result_rows = com.k_int.oql.util.OQLSelectFactory.createStatement(qd, bind_vars, bind_types, values, "QueryResult");
  
            log.debug("New query is : "+result_rows);
            List qry_result = session.createQuery(result_rows.toString())
                                .setParameters(bind_vars.toArray(), (Type[])bind_types.toArray(new Type[0]))
                                .list();

            int ctr=0;
            result.setResults(new SearchResultEntryDTO[qry_result.size()]);

            Object[] row = null;
            for ( Iterator i=qry_result.iterator(); i.hasNext(); ) {
              log.debug("Adding another row..");
              row = (Object[]) i.next();
  
              Long internal_id = (Long)row[0];
              long time = row[8] != null ? ((Date)row[8]).getTime() : 0;
              long unread_message_count = ( row[12] != null ? ((Long)row[12]).longValue() : -1 ) ;
              // int service_type = ((Integer)row[15]).intValue();
              Long current_trans_id  =(Long)row[15];

              result.getResults()[ctr++] = new SearchResultEntryDTO( internal_id,
                             (String)row[1],
                             (String)row[2],
                             (String)row[3],
                             (String)row[4],
                             (String)row[5],
                             (String)row[6],
                             (String)row[7],
                             new java.util.Date(time),
                             (String)row[9],
                             (String)row[16],  // State Model
                             (Boolean)row[10],
                             (String)row[11],
                             unread_message_count,
                             (String)row[13],
                             (String)row[14],
                             current_trans_id);
            }
          }
          else {
          }
        }
      }
      catch ( org.hibernate.HibernateException he ) {
        log.warn("\n****Problem with search:",he);
        throw new RuntimeException(he.toString());
      }
      finally {
      }
    }
    else {
      log.warn("No Query Provided");
    }

    return result;
  }

  public Long createLocationSymbol(String full_alias,
                                     String symbol,
                                     NamingAuthority authority,
                                     Location location,
                                     Service service_for_this_symbol,
                                     Session sess) throws java.sql.SQLException,
                                                          org.hibernate.HibernateException
  {
    Long result = null;

    LocationSymbol test = com.k_int.openrequest.helpers.DBHelper.lookupLocation(sess, full_alias);

    if ( test == null ) {
      LocationSymbol sym = new LocationSymbol(symbol, authority, location, service_for_this_symbol);
      result = (Long) sess.save(sym);
    }
    else {
      System.err.println("Symbol not created - Duplicate - Updating service for this symbol");
      test.setServiceForThisSymbol(service_for_this_symbol);
      result = test.getId();
    }
    return result;
  }

  // Cancel
  private boolean cancelAction(Session session,
                               Long transaction_id, 
                               CancelMessageDTO cancel,
                               ILLTransaction trans,
                               LocationSymbol sender,
                               LocationSymbol recipient) {
    log.debug("processing cancelAction "+cancel);
    try {
      ILL_APDU_type pdu = ISOCancelMessageFactory.create(session,
                                     trans,
                                     cancel.getRequesterNote(),  // String requester note
                                     null); // ISOExtensionFactory[] extens);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"CANreq");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      log.debug("completed processing cancelAction");
    }
    return true;
  }

  // Shipped,
  private boolean shippedAction(Session session,
                                Long transaction_id, 
                                ShippedMessageDTO shipped_message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) {
    log.debug("processing shippedAction - "+shipped_message);
    try {
      int shipped_service_type_int = shipped_message.getShippedServiceType() != null ?
                      shipped_message.getShippedServiceType().toIso() : 1;
      Long shipped_conditions = ( ( shipped_message.getShippedConditions() != null ) && 
                                  ( shipped_message.getShippedConditions().toIso() > 0 ) ) ?
                       new Long(shipped_message.getShippedConditions().toIso()) : null;

      ILL_APDU_type pdu = ISOShippedMessageFactory.create(session,
                                                          sender,
                                                          recipient,
                                                          trans,
                                                          shipped_service_type_int,
                                                          shipped_message.getDateShipped(),
                                                          shipped_message.getDateDue(),
                                                          new Boolean(shipped_message.isRenewable()),
                                                          new Long(0), // chargeable_units, // Chargable units
                                                          shipped_message.getTotalCostCurrency(),
                                                          shipped_message.getTotalCostAmount(),
                                                          shipped_conditions,
                                                          shipped_message.getName(),
                                                          shipped_message.getExtendedPostal(),
                                                          shipped_message.getStreetAndNumber(),
                                                          shipped_message.getPOBox(),
                                                          shipped_message.getCity(),
                                                          shipped_message.getRegion(),
                                                          shipped_message.getCountry(),
                                                          shipped_message.getPostcode(),
                                                          shipped_message.getResponderNote(),
                                                          null,
                                                          app_context);  // Extensions

      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"SHIreq");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      log.debug("done processing shippedAction");
    }
    return true;
  }

  public boolean conditionalAnswerAction(Session session,
                                Long transaction_id,
                                ConditionalAnswerMessageDTO conditional_answer_message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) {
    log.debug("conditionalAnswerAction - "+conditional_answer_message);

    try {
      ILL_APDU_type pdu = ISOAnswerMessageFactory.createConditionalAnswer(session,
                                                          sender,
                                                          recipient,
                                                          trans,
                                                          conditional_answer_message.getConditionsIso(),
                                                          conditional_answer_message.getDateForReply(),
                                                          conditional_answer_message.getResponderNote(),
                                                          app_context);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"ANSreq-CO");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      log.debug("done processing conditionalAnswerAction");
    }
    return true;

  }

  public boolean holdPlacedAnswerAction(Session session,
                                Long transaction_id,
                                HoldPlacedAnswerDTO hold_placed_message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) {
    log.debug("holdPlacedAnswerAction - "+hold_placed_message);
    try {
      ILL_APDU_type pdu = ISOAnswerMessageFactory.createHoldPlacedAnswer(session,
                                                          sender,
                                                          recipient,
                                                          trans,
                                                          hold_placed_message.getEstimatedDateAvailable(),
                                                          hold_placed_message.getISOHoldPlacedMediumType(),
                                                          null, // Locations
                                                          hold_placed_message.getResponderNote(),
                                                          app_context);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"ANSreq-HP");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      log.debug("done processing holdPlacedAnswerAction");
    }
    return true;
  }

  public boolean retryAnswerAction(Session session,
                                Long transaction_id,
                                RetryAnswerDTO retry_message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) {
    log.debug("retryAnswerAction - "+retry_message);
    try {
      ILL_APDU_type pdu = ISOAnswerMessageFactory.createRetryAnswer(session,
                                                          sender,
                                                          recipient,
                                                          trans,
                                                          retry_message.getISOReasonNotAvailable(),
                                                          retry_message.getRetryDate(),
                                                          null,  // Locations.
                                                          retry_message.getResponderNote(),
                                                          app_context);

      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"ANSreq-RY");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      log.debug("done processing shippedAction");
    }
    return true;
  }

  public boolean unfilledAnswerAction(Session session,
                                Long transaction_id,
                                UnfilledAnswerDTO unfilled_message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) {
    log.debug("unfilledAnswerAction - "+unfilled_message);
    try {
      ILL_APDU_type pdu = ISOAnswerMessageFactory.createUnfilledAnswer(session,
                                                          sender,
                                                          recipient,
                                                          trans,
                                                          unfilled_message.ISOReason(),
                                                          null,  // Locations.
                                                          unfilled_message.getResponderNote(),
                                                          app_context);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"ANSreq-UN");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      log.debug("done processing unfilledAnswerAction");
    }
    return true;
  }

  public boolean willSupplyAnswerAction(Session session,
                                Long transaction_id,
                                WillSupplyAnswerDTO will_supply_message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) {
    log.debug("willSupplyAnswerAction - "+will_supply_message);
    try {
      ILL_APDU_type pdu = ISOAnswerMessageFactory.createWillSupplyAnswer(session,
                                                          sender,
                                                          recipient,
                                                          trans,
                                                          will_supply_message.getISOReasonWillSupply(),
                                                          will_supply_message.getSupplyDate(),
                                                          new Postal_Address_type(),
                                                          null,  // Locations.
                                                          null,  // Electronic Delivery service.
                                                          will_supply_message.getResponderNote(),
                                                          app_context);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"ANSreq-WS");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      log.debug("done processing willSupplyAnswerAction");
    }
    return true;
  }

  private boolean conditionalReplyAction(Session session,
                                Long transaction_id,
                                ConditionalReplyMessageDTO message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) throws RequestManagerException {
    log.debug("conditionalReplyAction - "+message);
    try {
      ILL_APDU_type pdu = ISOConditionalReplyMessageFactory.create(session,
                                                          trans,
                                                          message.isAnswer(),
                                                          message.getRequesterNote(),
                                                          null); // Extensions
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      if ( message.isAnswer()) 
        sendMessage(e,"C-REPreq+");
      else
        sendMessage(e,"C-REPreq-");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      log.debug("done processing conditionalReplyAction");
    }
    return true;
  }

  private boolean receivedAction(Session session,
                                 Long transaction_id,
                                 ReceivedMessageDTO message,
                                 ILLTransaction trans,
                                 LocationSymbol sender,
                                 LocationSymbol recipient) throws RequestManagerException {
    log.debug("receivedAction - "+message);
    try {
      ILL_APDU_type pdu = ISOReceivedMessageFactory.create(session,
                                                          trans,
                                                          message.getDateReceived(),
                                                          BigInteger.valueOf(message.getISOServiceType()),
                                                          message.getRequesterNote(),
                                                          null); // Extensions
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"RCVreq");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      log.debug("done processing receivedAction");
    }
    return true;
  }

  private boolean messageAction(Session session,
                                Long transaction_id,
                                MessageDTO message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) throws RequestManagerException {
    log.debug("messageAction - "+message);
    try {
      ILL_APDU_type pdu = ISOMessageMessageFactory.create(session,
                                                          trans,
                                                          message.getNote(),
                                                          null);
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"MSGreq");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    return true;
  }

  private boolean cancelReplyAction(Session session,
                                Long transaction_id,
                                CancelReplyMessageDTO message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) throws RequestManagerException {
    log.debug("cancelReplyAction - "+message);
    try {
      ILL_APDU_type pdu = ISOCancelReplyMessageFactory.create(session,
                                                          trans,
                                                          message.isAnswer(),
                                                          message.getResponderNote(),
                                                          null);  // Extensions
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      if ( message.isAnswer()) 
        sendMessage(e,"CARreq+");
      else
        sendMessage(e,"CARreq-");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    return true;
  }

  private boolean recallAction(Session session,
                               Long transaction_id,
                               RecallMessageDTO message,
                               ILLTransaction trans,
                               LocationSymbol sender,
                               LocationSymbol recipient) throws RequestManagerException {
    log.debug("recallAction - "+message);
    try {
      ILL_APDU_type pdu = ISORecallMessageFactory.create(session,
                                                         trans,
                                                         message.getResponderNote(),
                                                         null);  // Extensions
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"RCLreq");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    return true;
  }

  private boolean returnedAction(Session session,
                                 Long transaction_id,
                                 ReturnedMessageDTO message,
                                 ILLTransaction trans,
                                 LocationSymbol sender,
                                 LocationSymbol recipient) throws RequestManagerException {
    log.debug("returnedAction - "+message);
    try {
      log.debug("returnedAction, returnedVia="+message.getReturnedVia());
      String returned_via = message.getReturnedVia() != null ? message.getReturnedVia().toString() : null;
      Amount_type insured_for = null;
      if ( ( message.getInsuredForCurrencyCode() != null ) && ( message.getInsuredForMonetaryValue() != null ) ) {
        insured_for = new Amount_type(message.getInsuredForCurrencyCode().toString(), message.getInsuredForMonetaryValue().toString());
      }

      ILL_APDU_type pdu = ISOReturnedMessageFactory.create(session,
                                                           trans,
                                                           null, // Supplemental item description
                                                           message.getDateReturned(),
                                                           returned_via,
                                                           insured_for,
                                                           message.getRequesterNote(),
                                                           null);  // Extensions
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"RETreq");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    return true;
  }

  private boolean checkedInAction(Session session,
                                  Long transaction_id,
                                  CheckedInMessageDTO message,
                                  ILLTransaction trans,
                                  LocationSymbol sender,
                                  LocationSymbol recipient) throws RequestManagerException {
    log.debug("checkedInAction - "+message);
    try {
      ILL_APDU_type pdu = ISOCheckedInMessageFactory.create(session,
                                                            trans,
                                                            message.getDateCheckedIn(),
                                                            message.getResponderNote(),
                                                            null);  // Extensions
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"CHKreq");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    return true;
  }

  private boolean overdueAction(Session session,
                                Long transaction_id,
                                OverdueMessageDTO message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) throws RequestManagerException {
    log.debug("overdueAction - "+message);
    try {
      log.debug("overdueAction for trans "+transaction_id+" (due date="+message.getDateDue()+")");
      ILL_APDU_type pdu = ISOOverdueMessageFactory.create(session,
                                                          trans,
                                                          message.getDateDue(),
                                                          message.isRenewable(),
                                                          message.getResponderNote(),
                                                          null);  // Extensions
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"DUEreq");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    return true;
  }

  private boolean renewAction(Session session,
                              Long transaction_id,
                              RenewMessageDTO message,
                              ILLTransaction trans,
                              LocationSymbol sender,
                              LocationSymbol recipient) throws RequestManagerException {
    log.debug("renewAction - "+message);
    try {
      ILL_APDU_type pdu = ISORenewMessageFactory.create(session,
                                                        trans,
                                                        message.getDesiredDueDate(),
                                                        message.getRequesterNote(),
                                                        null);  // Extensions
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"RENreq");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    return true;
  }

  private boolean renewAnswerAction(Session session,
                                    Long transaction_id,
                                    RenewAnswerMessageDTO message,
                                    ILLTransaction trans,
                                    LocationSymbol sender,
                                    LocationSymbol recipient) throws RequestManagerException {
    log.debug("renewAnswerAction - "+message);
    try {
      ILL_APDU_type pdu = ISORenewAnswerMessageFactory.create(session,
                                                              trans,
                                                              message.isAnswer(),
                                                              message.getDateDue(),
                                                              message.isRenewable(),
                                                              message.getResponderNote(),
                                                              null);  // Extensions
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      if ( message.isAnswer()) 
        sendMessage(e,"REAreq+");
      else
        sendMessage(e,"REAreq-");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    return true;
  }

  private boolean lostAction(Session session,
                             Long transaction_id,
                             LostMessageDTO message,
                             ILLTransaction trans,
                             LocationSymbol sender,
                             LocationSymbol recipient) throws RequestManagerException {
    log.debug("lostAction - "+message);
    try {
      ILL_APDU_type pdu = ISOLostMessageFactory.create(session,
                                                       trans,
                                                       message.getNote(),
                                                       null);  // Extensions
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"LSTreq");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    return true;
  }

  private boolean damagedAction(Session session,
                                Long transaction_id,
                                DamagedDTO message,
                                ILLTransaction trans,
                                LocationSymbol sender,
                                LocationSymbol recipient) throws RequestManagerException {
    log.debug("damagedAction - "+message);
    try {
      ILL_APDU_type pdu = ISODamagedMessageFactory.create(session,
                                                          trans,
                                                          message.getNote(),
                                                          null);  // Extensions
      ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
      sendMessage(e,"DAMreq");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    return true;
  }




  public void sendMessage(ILLMessageEnvelope e, String event) throws RequestManagerException {

    try {
      log.debug("sendMessage calling dispatch(\"System\","+event+",[msg]");
      MessageDispatcher md = (MessageDispatcher) app_context.getBean("DISPATCHER");
      md.dispatch("System",event,e);
    }
    catch ( Exception exception ) {
      throw new RequestManagerException("Problems sending Message",exception);
    }
  }

  public String actionMessage(long loc_id, long[] transaction_ids, ProtocolMessageDTO message) throws RequestManagerException {

    log.debug("actionMessage "+loc_id+", "+transaction_ids+", "+message);

    String result="OK";

    if ( message == null ) {
      throw new RequestManagerException("Null message passed to actionMessage");
    }

    Session session = null;
    try {
      SessionFactory sf = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      session = sf.openSession();
      for ( int i=0; i<transaction_ids.length; i++ ) {
        log.debug("Looking up transaction "+transaction_ids[i]+", message type is "+message.getIsoType() );

        ILLTransaction trans = DBHelper.lookupTransaction(session,transaction_ids[i]);

        if ( trans == null )
          throw new RequestManagerException("Unable to locate Transaction "+transaction_ids[i]+" at this location");

        if ( trans.getLocation() == null )
          throw new RequestManagerException("Internal error : No location  for this transaction "+transaction_ids[i]);

        LocationSymbol sender = DBHelper.lookupLocation(session,trans.getLocation().getDefaultSymbol());

        if ( sender == null )
          throw new RequestManagerException("Unable to resolve sender : "+trans.getLocation().getDefaultSymbol());

        LocationSymbol recipient = DBHelper.lookupLocation(session,trans.getPVCurrentPartnerIdSymbol());
  
        if ( recipient == null )
          throw new RequestManagerException("Unable to resolve recipient : "+trans.getPVCurrentPartnerIdSymbol());

        switch ( message.getIsoType() )
        {
          case 1 :
            log.warn("sendMessage:ILL_REQUEST - Should never do this");
            throw new RequestManagerException("You cannot send a request via the sendMessage method. use createTransGroup");
          case 2 :
            log.warn("sendMessage:FORWARD_NOTIFICATION - Not implemented via Integration Engine (yet)");
            break;
          case 3 :
            log.debug("SHIPPED");
            shippedAction(session, transaction_ids[i], (ShippedMessageDTO) message, trans, sender, recipient);
            break;
          case 4 :
            log.debug("ILL_ANSWER");
            if ( message instanceof ConditionalAnswerMessageDTO )
               conditionalAnswerAction(session, transaction_ids[i],(ConditionalAnswerMessageDTO)message, trans, sender, recipient);
            else if ( message instanceof HoldPlacedAnswerDTO )
              holdPlacedAnswerAction(session, transaction_ids[i],(HoldPlacedAnswerDTO)message, trans, sender, recipient);
            else if ( message instanceof RetryAnswerDTO )
              retryAnswerAction(session,transaction_ids[i],(RetryAnswerDTO)message, trans, sender, recipient);
            else if ( message instanceof UnfilledAnswerDTO )
              unfilledAnswerAction(session,transaction_ids[i],(UnfilledAnswerDTO)message, trans, sender, recipient);
            else if ( message instanceof WillSupplyAnswerDTO )
              willSupplyAnswerAction(session,transaction_ids[i],(WillSupplyAnswerDTO)message, trans, sender, recipient);
            break;
          case 5 :
            log.debug("CONDITIONAL_REPLY");
            conditionalReplyAction(session,transaction_ids[i],(ConditionalReplyMessageDTO)message, trans, sender, recipient);
            break;
          case 6 :
            log.debug("CANCEL");
            cancelAction(session,transaction_ids[i],(CancelMessageDTO)message, trans, sender, recipient);
            break;
          case 7 :
            log.debug("CANCEL_REPLY");
            cancelReplyAction(session,transaction_ids[i],(CancelReplyMessageDTO)message, trans, sender, recipient);
            break;
          case 8 :
            log.debug("RECEIVED");
            receivedAction(session,transaction_ids[i], (ReceivedMessageDTO)message, trans, sender, recipient);
            break;
          case 9 :
            log.debug("RECALL");
            recallAction(session,transaction_ids[i], (RecallMessageDTO)message, trans, sender, recipient);
            break;
          case 10 :
            log.debug("RETURNED");
            returnedAction(session,transaction_ids[i], (ReturnedMessageDTO)message, trans, sender, recipient);
            break;
          case 11 :
            log.debug("CHECKED_IN");
            checkedInAction(session,transaction_ids[i], (CheckedInMessageDTO)message, trans, sender, recipient);
            break;
          case 12 :
            log.debug("OVERDUE");
            overdueAction(session,transaction_ids[i], (OverdueMessageDTO)message, trans, sender, recipient);
            break;
          case 13 :
            log.debug("RENEW");
            renewAction(session,transaction_ids[i], (RenewMessageDTO)message, trans, sender, recipient);
            break;
          case 14 :
            log.debug("RENEW_ANSWER");
            renewAnswerAction(session,transaction_ids[i], (RenewAnswerMessageDTO)message, trans, sender, recipient);
            break;
          case 15 :
            log.debug("LOST");
            lostAction(session,transaction_ids[i], (LostMessageDTO)message, trans, sender, recipient);
            break;
          case 16 :
            log.debug("DAMAGED");
            damagedAction(session,transaction_ids[i], (DamagedDTO)message, trans, sender, recipient);
            break;
          case 17 :
            log.debug("MESSAGE");
            messageAction(session,transaction_ids[i], (MessageDTO)message, trans, sender, recipient);
            break;
          case 18 :
            log.debug("STATUS_QUERY");
            break;
          case 19 :
            log.debug("STATUS_ERROR_REPORT");
            // statusOrErrorReportAction(session,transaction_ids[i], (StatusOrErrorReportDTO)message, trans, sender, recipient);
            break;
          case 20 :
            log.debug("EXPIRED");
            // This is requested by a timer inside open request, users would not normally request expiry.
            break;
          default:
            log.debug("DEFAULT!");
            break;
        }
        session.flush();
        session.connection().commit();
      }
    } 
    catch ( java.sql.SQLException sqle ) {
      log.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("Problem",he);
    }
    finally {
      try { session.close(); } catch ( Exception e ) {}
      session = null;
    }

    return result;
  }

  public java.util.List<String> getEngineStatus(long max_reports) {
    java.util.List<String> result = null;
    if ( app_context.containsBean("EngineStatusService") ) {
      com.k_int.openrequest.util.EngineStatusService status_service = (com.k_int.openrequest.util.EngineStatusService) app_context.getBean("EngineStatusService");
      result = status_service.getStatus(max_reports);
    }
    return result;
  }


}
