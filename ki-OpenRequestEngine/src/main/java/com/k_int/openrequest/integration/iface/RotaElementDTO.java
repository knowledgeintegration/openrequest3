package com.k_int.openrequest.integration.iface;

/**
 * Title:       RotaElementDTO
 * @version:    $Id: RotaElementDTO.java,v 1.2 2005/02/24 19:16:49 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class RotaElementDTO implements java.io.Serializable
{
  public String symbol;
  public int protocol;
  public String mandatory_transaction_qualifier;

  public RotaElementDTO(String symbol, 
                        ServiceProtocol protocol) {
    this(symbol,protocol,null);
  }

  public RotaElementDTO(String symbol, 
                        int protocol) {
    this(symbol,protocol,null);
  }

  public RotaElementDTO(String symbol, ServiceProtocol protocol, String mandatory_transaction_qualifier) {
    this.symbol = symbol;
    this.protocol = protocol.toIso();
    this.mandatory_transaction_qualifier = mandatory_transaction_qualifier;
  }

  public RotaElementDTO(String symbol, int protocol, String mandatory_transaction_qualifier) {
    this.symbol = symbol;
    this.protocol = protocol;
    this.mandatory_transaction_qualifier = mandatory_transaction_qualifier;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public ServiceProtocol getProtocol() {
    return ServiceProtocol.fromIso(protocol);
  }

  public void setProtocol(ServiceProtocol protocol) {
    this.protocol=protocol.toIso();
  }

  public String getResponderId() {
    return symbol;
  }

  public String getMandatoryTransactionQualifier() {
    return mandatory_transaction_qualifier;
  }

  public void setMandatoryTransactionQualifier(String mandatory_transaction_qualifier) {
    this.mandatory_transaction_qualifier = mandatory_transaction_qualifier;
  }
}
