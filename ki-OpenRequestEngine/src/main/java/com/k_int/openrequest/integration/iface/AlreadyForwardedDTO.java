package com.k_int.openrequest.integration.iface;

/**
 * Title:       AlreadyForwardedDTO
 * @version:    $Id: AlreadyForwardedDTO.java,v 1.2 2005/06/10 15:36:48 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class AlreadyForwardedDTO extends UserErrorReportDTO implements java.io.Serializable {

  private com.k_int.openrequest.db.SystemId system_id;

  public AlreadyForwardedDTO() {
  }

  public AlreadyForwardedDTO(com.k_int.openrequest.db.SystemId system_id) {
    this.system_id = system_id;
  }

  public com.k_int.openrequest.db.SystemId getSystemId() {
    return system_id;
  }

  public void setSystemId(com.k_int.openrequest.db.SystemId system_id) {
    this.system_id = system_id;
  }
}
