package com.k_int.openrequest.integration.iface;

/**
 * Title:       ConditionalReplyMessageDTO
 * @version:    $Id: ConditionalReplyMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ConditionalReplyMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  public boolean answer;
  public String requester_note;

  public ConditionalReplyMessageDTO()
  {
    this.message_type = MessageType.CONDITIONAL_REPLY.toIso();
  }

  public ConditionalReplyMessageDTO(boolean answer,
                            String responder_note)
  {
    this.message_type = MessageType.CONDITIONAL_REPLY.toIso();
    this.answer = answer;
    this.requester_note = requester_note;
  }

  public boolean isAnswer()
  {
    return answer;
  }

  public String getRequesterNote()
  {
    return requester_note;
  }
}
