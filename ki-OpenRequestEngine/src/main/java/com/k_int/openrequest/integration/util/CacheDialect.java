package com.k_int.openrequest.integration.util;

import org.hibernate.dialect.Dialect;
import java.sql.Types;
                                                                                                                                            
public class CacheDialect extends Dialect {
                                                                                                                                            
        public CacheDialect() {
                super();
                registerColumnType( Types.BIT, "BIT" );
                registerColumnType( Types.BIGINT, "INT" );
                registerColumnType( Types.SMALLINT, "SMALLINT" );
                registerColumnType( Types.TINYINT, "TINYINT" );
                registerColumnType( Types.INTEGER, "INTEGER" );
                // It looks like there might be an issue between hibernate and the Cache
                // Char datatype, specficially, with null instances in the database. 
                // Cache seems to return a string where string.length() == 0 instead
                // of the value null. Not sure if a different mapping here would fix that
                registerColumnType( Types.CHAR, "CHAR(1)" );
                registerColumnType( Types.VARCHAR, "VARCHAR($l)" );
                registerColumnType( Types.FLOAT, "FLOAT" );
                registerColumnType( Types.DOUBLE, "DOUBLE" );
                registerColumnType( Types.DATE, "DATE" );
                registerColumnType( Types.TIME, "TIME" );
                registerColumnType( Types.TIMESTAMP, "TIMESTAMP" );
                registerColumnType( Types.VARBINARY, "VARBINARY($l)" );
                registerColumnType( Types.NUMERIC, "NUMERIC(19, $l)" );
                registerColumnType( Types.BLOB, "LONGVARBINARY" );
                registerColumnType( Types.CLOB, "LONGVARCHAR" );
        }

        public String getAddColumnString() {
                return "add column";
        }
                                                                                                                                            
}
