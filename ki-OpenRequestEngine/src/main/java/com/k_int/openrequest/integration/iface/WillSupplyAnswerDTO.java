package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       WillSupplyAnswerDTO
 * @version:    $Id: WillSupplyAnswerDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class WillSupplyAnswerDTO extends ILLAnswerMessageDTO implements java.io.Serializable
{
  // private ReasonWillSupply reason;
  private int reason;
  private Date supply_date;
  private PostalAddressDTO return_to_address;
  private String responder_note;

  public WillSupplyAnswerDTO()
  {
    super();
  }

  public WillSupplyAnswerDTO(ReasonWillSupply reason,
                            Date supply_date,
                            String name,
                            String extended_postal,
                            String street_and_number,
                            String city,
                            String region,
                            String country,
                            String postcode,
                            String responder_note)
  {
    super();
    this.reason = reason.toIso();
    this.supply_date = supply_date;
    this.responder_note = responder_note;
  }

  public ReasonWillSupply getReasonWillSupply()
  {
    return ReasonWillSupply.fromIso(reason);
  }

  public int getISOReasonWillSupply()
  {
    return reason;
  }

  public Date getSupplyDate()
  {
    return supply_date;
  }

  public PostalAddressDTO getReturnToAddress()
  {
    return return_to_address;
  }

  public String getResponderNote()
  {
    return responder_note;
  }
}
