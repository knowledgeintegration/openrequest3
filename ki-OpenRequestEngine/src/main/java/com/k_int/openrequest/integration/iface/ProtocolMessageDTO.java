package com.k_int.openrequest.integration.iface;

import java.util.Date;
import java.util.List;

/**
 * Title:       ProtocolMessageDTO
 * @version:    $Id: ProtocolMessageDTO.java,v 1.3 2005/03/31 20:16:32 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ProtocolMessageDTO implements java.io.Serializable
{
  protected int message_type = -1;
  protected String event_code;
  protected MessageIdDTO message_id;
  protected Date timestamp;
  protected boolean is_read;
  protected List extension_dtos = new java.util.ArrayList();

  public ProtocolMessageDTO() {
    this.message_type = MessageType.MESSAGE.toIso();
  }

  public ProtocolMessageDTO(int message_type,
                   String event_code,
                   MessageIdDTO message_id,
                   Date timestamp,
                   boolean is_read) {
    this.message_type = MessageType.MESSAGE.toIso();
    this.message_type = message_type;
    this.event_code = event_code;
    this.message_id = message_id;
    this.timestamp = timestamp;
    this.is_read = is_read;
  }

  public int getIsoType() {
    return message_type;
  }

  public MessageType getType() {
    return MessageType.fromIso(message_type);
  }

  public EventCode getEventCode() {
    return null;
  }

  public MessageIdDTO getId() {
    return message_id;
  }

  public Date getServiceDateTime() {
    return timestamp;
  }

  public boolean isRead() {
    return is_read;
  }

  public List getExtensionDTOs() {
    return extension_dtos;
  }

  public void setExtensionDTOs(List extension_dtos) {
    this.extension_dtos = extension_dtos;
  }
}
