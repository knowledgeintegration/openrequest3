package com.k_int.openrequest.integration.iface;

/**
 * Title:       UserErrorReportDTO
 * @version:    $Id: UserErrorReportDTO.java,v 1.1 2005/06/10 14:34:11 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public abstract class UserErrorReportDTO implements java.io.Serializable {
}
