package com.k_int.openrequest.integration.iface;

public enum ReasonUnfilled {

  IN_USE_ON_LOAN, IN_PROCESS, LOST, NON_CIRCULATING, NOT_OWNED, ON_ORDER, NOT_YET_AVAILABLE, AT_BINDERY, LACKING, NOT_ON_SHELF, ON_RESERVE, POOR_CONDITION, COST_EXCEEDS_LIMIT, CHARGES, PREPAYMENT_REQUIRED, LACKS_COPYRIGHT_COMPLIANCE, NOT_FOUND_AS_CITED, LOCATIONS_NOT_FOUND, ON_HOLD, POLICY_PROBLEM, MESSAGING_NOT_SUPPORTED, EXPIRY_NOT_SUPPORTED, DELIVERY_NOT_SUPPORTED, DELIVERY_TIME_NOT_SUPPORTED, OTHER, RESPONDER_SPECIFIC;

  public String toString() { 
    switch ( this ) {
      case IN_USE_ON_LOAN: return "In use on loan";
      case IN_PROCESS: return "In process";
      case LOST: return "Lost";
      case NON_CIRCULATING: return "Non circulating";
      case NOT_OWNED: return "Not Owned";
      case ON_ORDER: return "On Order";
      case NOT_YET_AVAILABLE: return "Not yet available";
      case AT_BINDERY: return "At bindery";
      case LACKING: return "Lacking";
      case NOT_ON_SHELF: return "Not on shelf";
      case ON_RESERVE: return "On Reserve";
      case POOR_CONDITION: return "Poor Condition";
      case COST_EXCEEDS_LIMIT: return "Cost exceeds limit";
      case CHARGES: return "Charges";
      case PREPAYMENT_REQUIRED: return "Prepayment Required";
      case LACKS_COPYRIGHT_COMPLIANCE: return "Lacks copyright compliance";
      case NOT_FOUND_AS_CITED: return "Not found as cited";
      case LOCATIONS_NOT_FOUND: return "Locations not found";
      case ON_HOLD: return "On Hold";
      case POLICY_PROBLEM: return "Policy Problem";
      case MESSAGING_NOT_SUPPORTED: return "Messaging not supported";
      case EXPIRY_NOT_SUPPORTED: return "Expiry not supported";
      case DELIVERY_NOT_SUPPORTED: return "Delivery not supported";
      case DELIVERY_TIME_NOT_SUPPORTED: return "Delivery time not supported";
      case OTHER: return "Other";
      case RESPONDER_SPECIFIC: return "Responder Specific";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case IN_USE_ON_LOAN: return 1;
      case IN_PROCESS: return 2;
      case LOST: return 3;
      case NON_CIRCULATING: return 4;
      case NOT_OWNED: return 5;
      case ON_ORDER: return 6;
      case NOT_YET_AVAILABLE: return 7;
      case AT_BINDERY: return 8;
      case LACKING: return 9;
      case NOT_ON_SHELF: return 10;
      case ON_RESERVE: return 11;
      case POOR_CONDITION: return 12;
      case COST_EXCEEDS_LIMIT: return 13;
      case CHARGES: return 14;
      case PREPAYMENT_REQUIRED: return 15;
      case LACKS_COPYRIGHT_COMPLIANCE: return 16;
      case NOT_FOUND_AS_CITED: return 17;
      case LOCATIONS_NOT_FOUND: return 18;
      case ON_HOLD: return 19;
      case POLICY_PROBLEM: return 20;
      case MESSAGING_NOT_SUPPORTED: return 21;
      case EXPIRY_NOT_SUPPORTED: return 22;
      case DELIVERY_NOT_SUPPORTED: return 23;
      case DELIVERY_TIME_NOT_SUPPORTED: return 24;
      case OTHER: return 27;
      case RESPONDER_SPECIFIC: return 28;
      default: return 0;
    }
  }

  public static ReasonUnfilled fromIso(int iso){
    switch (iso) {
      case 1: return IN_USE_ON_LOAN;
      case 2: return IN_PROCESS;
      case 3: return LOST;
      case 4: return NON_CIRCULATING;
      case 5: return NOT_OWNED;
      case 6: return ON_ORDER;
      case 7: return NOT_YET_AVAILABLE;
      case 8: return AT_BINDERY;
      case 9: return LACKING;
      case 10: return NOT_ON_SHELF;
      case 11: return ON_RESERVE;
      case 12: return POOR_CONDITION;
      case 13: return COST_EXCEEDS_LIMIT;
      case 14: return CHARGES;
      case 15: return PREPAYMENT_REQUIRED;
      case 16: return LACKS_COPYRIGHT_COMPLIANCE;
      case 17: return NOT_FOUND_AS_CITED;
      case 18: return LOCATIONS_NOT_FOUND;
      case 19: return ON_HOLD;
      case 20: return POLICY_PROBLEM;
      case 21: return MESSAGING_NOT_SUPPORTED;
      case 22: return EXPIRY_NOT_SUPPORTED;
      case 23: return DELIVERY_NOT_SUPPORTED;
      case 24: return DELIVERY_TIME_NOT_SUPPORTED;
      case 27: return OTHER;
      case 28: return RESPONDER_SPECIFIC;
      default: return null;
    }
  }
}
