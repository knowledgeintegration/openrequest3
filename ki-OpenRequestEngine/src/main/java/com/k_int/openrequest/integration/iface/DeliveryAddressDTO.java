package com.k_int.openrequest.integration.iface;

/**
 * Title:       DeliveryAddressDTO
 * @version:    $Id: DeliveryAddressDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class DeliveryAddressDTO implements java.io.Serializable
{
  public PostalAddressDTO postal_address;
  public SystemAddressDTO system_address;

  public DeliveryAddressDTO(PostalAddressDTO postal_address,
                           SystemAddressDTO system_address)
  {
    this.postal_address = postal_address;
    this.system_address = system_address;
  }

  public PostalAddressDTO getPostalAddress()
  {
    return postal_address;
  }

  public SystemAddressDTO getSystemAddress()
  {
    return system_address;
  }
}
