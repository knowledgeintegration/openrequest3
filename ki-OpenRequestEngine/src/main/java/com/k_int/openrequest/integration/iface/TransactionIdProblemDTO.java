package com.k_int.openrequest.integration.iface;

import java.math.BigInteger;

/**
 * Title:       ProviderErrorReportDTO
 * @version:    $Id: TransactionIdProblemDTO.java,v 1.2 2005/06/10 16:26:06 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class TransactionIdProblemDTO extends ProviderErrorReportDTO implements java.io.Serializable {

  public static final TransactionIdProblemDTO DUPLICATE_TRANSACTION_ID = new TransactionIdProblemDTO(1);
  public static final TransactionIdProblemDTO INVALID_TRANSACTION_ID = new TransactionIdProblemDTO(2);
  public static final TransactionIdProblemDTO UNKNOWN_TRANSACTION_ID = new TransactionIdProblemDTO(3);

  private long code = 0;

  public TransactionIdProblemDTO(long code) {
    this.code = code;
  }

  public BigInteger getProblemCode() {
    return BigInteger.valueOf(code);
  }
}
