package com.k_int.openrequest.integration.iface.extensions;

import java.util.Date;
import java.util.List;

/**
 * Title:       IntermediaryActionsDTO
 * @version:    $Id: IntermediaryActionsDTO.java,v 1.1 2005/06/10 18:04:19 ibbo Exp $
 * Copyright:   Copyright (C) 2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class IntermediaryActionsDTO {

  public static final IntermediaryActionsDTO STATUS_INFORMATION = new IntermediaryActionsDTO(1);
  public static final IntermediaryActionsDTO CONTROL_REQUEST = new IntermediaryActionsDTO(2);
  public static final IntermediaryActionsDTO CONTROL_RELEASE = new IntermediaryActionsDTO(3);
  public static final IntermediaryActionsDTO CONTROL_DENIED = new IntermediaryActionsDTO(4);

  private final long code;

  public IntermediaryActionsDTO (long code) {
    this.code = code;
  }

  public long getCode() {
    return code;
  }
}
