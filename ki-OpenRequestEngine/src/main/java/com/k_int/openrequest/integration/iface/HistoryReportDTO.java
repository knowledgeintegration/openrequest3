package com.k_int.openrequest.integration.iface;

/**
 * Title:       HistoryReportDTO
 * @version:    $Id: HistoryReportDTO.java,v 1.1 2005/03/08 10:21:54 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class HistoryReportDTO implements java.io.Serializable {

  /** Optional member */
  // public java.lang.String date_requested = null;
  /** Optional member */
  // public ILL_String_type author = null;
  /** Optional member */
  // public ILL_String_type title = null;
  /** Optional member */
  // public ILL_String_type author_of_article = null;
  /** Optional member */
  // public ILL_String_type title_of_article = null;
  /** Mandatory member */
  // public java.lang.String date_of_last_transition = null;
  /** Mandatory member */
  // public BigInteger most_recent_service = null;
  /** Mandatory member */
  // public java.lang.String date_of_most_recent_service = null;
  /** Mandatory member */
  // public System_Id_type initiator_of_most_recent_service = null;
  /** Optional member */
  // public java.math.BigInteger shipped_service_type = null;
  /** Optional member */
  // public java.math.BigInteger transaction_results = null;
  /** Optional member */
  // public ILL_String_type most_recent_service_note = null;

  public HistoryReportDTO() {
  }



}

