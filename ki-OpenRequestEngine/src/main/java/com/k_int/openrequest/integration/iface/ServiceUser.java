package com.k_int.openrequest.integration.iface;

public enum ServiceUser {

  REQUESTER, RESPONDER;

  public String toString() {
    switch ( this ) {
      case REQUESTER: return "Requester";
      case RESPONDER: return "Responder";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case REQUESTER: return 1;
      case RESPONDER: return 2;
      default: return 0;
    }
  }
}
