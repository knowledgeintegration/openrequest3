package com.k_int.openrequest.integration.iface;

/**
 * Title:       ILLAnswerMessageDTO
 * @version:    $Id: ILLAnswerMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ILLAnswerMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  private int results = -1;

  public ILLAnswerMessageDTO()
  {
    this.message_type = MessageType.ILL_ANSWER.toIso();
  }

  public TransactionResults getTransactionResults()
  {
    return TransactionResults.fromIso(results);
  }
}
