package com.k_int.openrequest.integration.provider.base;

import com.k_int.openrequest.integration.iface.*;
import org.springframework.context.*;

/*
 * $Log: AbstractRequestManagerFactory.java,v $
 * Revision 1.2  2005/02/20 18:37:31  ibbo
 * Updated
 *
 * Revision 1.1.1.1  2005/02/14 11:46:29  ibbo
 * Import
 *
 * Revision 1.2  2004/08/01 12:38:24  ibbo
 * Updated
 *
 * Revision 1.1.1.1  2004/07/23 07:13:17  ibbo
 * Initial
 *
 * Revision 1.1  2003/05/01 08:53:20  imi
 * Many changes for revised RMI structure
 *
 * Revision 1.3  2003/04/16 16:30:51  imi
 * added ability to specify host
 *
 * Revision 1.2  2003/04/04 14:21:49  imi
 * Renamed BE to Be Be ;)
 *
 * Revision 1.1.1.1  2003/04/04 10:18:21  imi
 * Initial import
 *
 * Revision 1.2  2003/03/27 13:46:23  imi
 * Refresh, added a number of Value Objects
 *
 * Revision 1.1  2003/03/27 09:43:09  imi
 * Renamed some classes to add BE prefix
 *
 */

/**
 * Title:       AbstractRequestManagerFactory
 * @version:    $Id: AbstractRequestManagerFactory.java,v 1.2 2005/02/20 18:37:31 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public abstract class AbstractRequestManagerFactory implements RequestManagerFactory, ApplicationContextAware
{
  protected ApplicationContext ctx = null;

  public AbstractRequestManagerFactory() throws RequestManagerException {
  }

  public abstract RequestManager getRequestManager(String location_symbol) throws RequestManagerException;

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }
}
