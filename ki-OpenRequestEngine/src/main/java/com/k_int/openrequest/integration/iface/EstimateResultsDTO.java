package com.k_int.openrequest.integration.iface;

import java.util.Date;
import java.util.List;

/**
 * Title:       EstimateResultsDTO
 * @version:    $Id: EstimateResultsDTO.java,v 1.2 2005/06/10 18:42:50 ibbo Exp $
 * Copyright:   Copyright (C) 2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class EstimateResultsDTO extends ResultsExplanationDTO implements java.io.Serializable {

  public EstimateResultsDTO() {
  }

}
