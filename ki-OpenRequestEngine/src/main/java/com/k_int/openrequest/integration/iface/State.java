package com.k_int.openrequest.integration.iface;

/**
 * Title:
 * Description: Typesafe enum pattern for 'Current-State'; see BS ISO 10161-1 page 37.
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public enum State {

  IDLE,
  NOT_SUPPLIED,
  PENDING,
  IN_PROCESS,
  FORWARD,
  CONDITIONAL,
  CANCEL_PENDING,
  CANCELLED,
  SHIPPED,
  RECEIVED,
  RENEW_PENDING,
  NOT_RCVD_OVERDUE,
  RENEW_OVERDUE,
  OVERDUE,
  RETURNED,
  CHECKED_IN,
  RECALL,
  LOST,
  UKNOWN,
  NO_RESTRICTION;

  public String toString()  {
    switch ( this ) {
      case IDLE: return "Not Started Yet";
      case NOT_SUPPLIED: return "Not Supplied";
      case PENDING: return "Pending";
      case IN_PROCESS: return "In Process";
      case FORWARD: return "Forward";
      case CONDITIONAL: return "Conditional";
      case CANCEL_PENDING: return "Cancel Pending";
      case CANCELLED: return "Cancelled";
      case SHIPPED: return "Shipped";
      case RECEIVED: return "Received";
      case RENEW_PENDING: return "Renew Pending";
      case NOT_RCVD_OVERDUE: return "Not Recd Overdue";
      case RENEW_OVERDUE: return "Renew Overdue";
      case OVERDUE: return "Overdue";
      case RETURNED: return "Returned";
      case CHECKED_IN: return "Checked In";
      case RECALL: return "Recall";
      case LOST: return "Lost";
      case UKNOWN: return "Unknown";
      case NO_RESTRICTION: return "No Restriction";
      default: return "Unknown";
    }
  }

  public int toIso() {
   switch ( this ) {
      case IDLE: return 0;
      case NOT_SUPPLIED: return 1;
      case PENDING: return 2;
      case IN_PROCESS: return 3;
      case FORWARD: return 4;
      case CONDITIONAL: return 5;
      case CANCEL_PENDING: return 6;
      case CANCELLED: return 7;
      case SHIPPED: return 8;
      case RECEIVED: return 9;
      case RENEW_PENDING: return 10;
      case NOT_RCVD_OVERDUE: return 11;
      case RENEW_OVERDUE: return 12;
      case OVERDUE: return 13;
      case RETURNED: return 14;
      case CHECKED_IN: return 15;
      case RECALL: return 16;
      case LOST: return 17;
      case UKNOWN: return 18;
      case NO_RESTRICTION: return 99;
      default: return 99;
    }
  }

  public static State fromIso(int iso){
    switch (iso) {
      case 0: return IDLE;
      case 1: return NOT_SUPPLIED;
      case 2: return PENDING;
      case 3: return IN_PROCESS;
      case 4: return FORWARD;
      case 5: return CONDITIONAL;
      case 6: return CANCEL_PENDING;
      case 7: return CANCELLED;
      case 8: return SHIPPED;
      case 9: return RECEIVED;
      case 10: return RENEW_PENDING;
      case 11: return NOT_RCVD_OVERDUE;
      case 12: return RENEW_OVERDUE;
      case 13: return OVERDUE;
      case 14: return RETURNED;
      case 15: return CHECKED_IN;
      case 16: return RECALL;
      case 17: return LOST;
      case 18: return UKNOWN;
      case 99: return NO_RESTRICTION;
      default: return null;
    }
  }
}
