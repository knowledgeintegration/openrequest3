package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       RenewAnswerMessageDTO
 * @version:    $Id: RenewAnswerMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class RenewAnswerMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  private boolean answer;
  private Date date_due;
  private boolean renewable;
  private String responder_note;

  public RenewAnswerMessageDTO()
  {
    this.message_type = MessageType.RENEW_ANSWER.toIso();
  }

  public RenewAnswerMessageDTO(boolean answer,
                       Date date_due,
                       boolean renewable,
                       String responder_note)
  {
    this.message_type = MessageType.RENEW_ANSWER.toIso();
    this.answer = answer;
    this.date_due = date_due;
    this.renewable = renewable;
    this.responder_note = responder_note;
  }

  public boolean isAnswer()
  {
    return answer;
  }

  public Date getDateDue()
  {
    return date_due;
  }

  public boolean isRenewable()
  {
    return renewable;
  }

  public String getResponderNote()
  {
    return responder_note;
  }

  public String toString() {
    return "RenewAnswerMessageDTO {answer:"+answer+",date_due:"+date_due+",renewable:"+renewable+",responder_note:"+responder_note+"}";
  }
}
