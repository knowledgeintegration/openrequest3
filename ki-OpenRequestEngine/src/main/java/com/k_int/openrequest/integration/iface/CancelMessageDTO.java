package com.k_int.openrequest.integration.iface;

/**
 * Title:       CancelMessageDTO
 * @version:    $Id: CancelMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class CancelMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  private String requester_note = null;

  public CancelMessageDTO()
  {
    this.message_type = MessageType.CANCEL.toIso();
  }

  public CancelMessageDTO(String requester_note)
  {
    this.message_type = MessageType.CANCEL.toIso();
    this.requester_note = requester_note;
  }

  public String getRequesterNote()
  {
    return requester_note;
  }
}
