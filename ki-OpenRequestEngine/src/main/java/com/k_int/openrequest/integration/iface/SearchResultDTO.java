package com.k_int.openrequest.integration.iface;

import java.util.Hashtable;

/**
 * Title:       SearchResultDTO
 * @version:    $Id: SearchResultDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class SearchResultDTO implements java.io.Serializable
{
  public String status = null;
  public long row_count = 0;
  public int handle = 0;

  public SearchResultDTO(int handle) {
    this.handle = handle;
  }

  public SearchStatus getStatus() {
    return SearchStatus.OK;
  }

  public void setStatus(SearchStatus status) {
    this.status = status.toString();
  }

  public long getRowCount() {
    return row_count;
  }

  public void setRowCount(long row_count) {
    this.row_count = row_count;
  }

  public int getHandle() {
    return handle;
  }

  public void setHandle(int handle) {
    this.handle = handle;
  }

  public long getCount() {
    return row_count;
  }
}
