package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       CheckedInMessageDTO
 * @version:    $Id: CheckedInMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class CheckedInMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  private String responder_note;
  private Date date_checked_in;

  public CheckedInMessageDTO()
  {
    this.message_type = MessageType.CHECKED_IN.toIso();
  }

  public CheckedInMessageDTO(Date date_checked_in,
                     String responder_note)
  {
    this.message_type = MessageType.CHECKED_IN.toIso();
    this.responder_note = responder_note;
    this.date_checked_in = date_checked_in;
  }

  public String getResponderNote()
  {
    return responder_note;
  }

  public Date getDateCheckedIn()
  {
    return date_checked_in;
  }
  
}
