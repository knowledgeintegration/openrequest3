package com.k_int.openrequest.integration.iface;

/**
 * Title:       DamagedDTO
 * @version:    $Id: DamagedDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class DamagedDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  private String note;

  public DamagedDTO(String note)
  {
    this.message_type = MessageType.DAMAGED.toIso();
    this.note = note;
  }

  public String getNote()
  {
    return note;
  }
}
