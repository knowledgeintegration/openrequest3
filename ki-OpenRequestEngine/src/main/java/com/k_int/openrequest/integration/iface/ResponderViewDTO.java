package com.k_int.openrequest.integration.iface;

import java.util.*;

/**
 * Title:       ResponderViewDTO
 * @version:    $Id: ResponderViewDTO.java,v 1.4 2005/03/29 23:03:37 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ResponderViewDTO extends RequestView implements java.io.Serializable
{
  public ResponderViewDTO()
  {
  }

  public ResponderViewDTO(TransactionType transaction_type,
                          PostalAddressDTO delivery_address,
                          DeliveryServiceDTO delivery_service,
                          PostalAddressDTO billing_address,
                          List service_type,
                          RequesterOptionalMessagesDTO requester_optional_messages,
                          SearchTypeDTO search_type,
                          SupplyMediumType[] required_mediums,
                          PlaceOnHoldType place_on_hold,
                          ItemIdDTO item_id,
                          CostInfoTypeDTO cost_info,
                          boolean forward_flag,
                          String requester_note,
                          String copyright_compliance,
                          String requester_reference_auth,
                          String requester_reference,
                          String client_id)
  {
    super( new ILLRequestMessageDTO( transaction_type,
                             delivery_address,
                             delivery_service,
                             billing_address,
                             service_type,
                             requester_optional_messages,
                             search_type,
                             required_mediums,
                             place_on_hold,
                             item_id,
                             cost_info,
                             forward_flag,
                             requester_note,
                             copyright_compliance,
                             requester_reference_auth,
                             requester_reference,
                             client_id),
           requester_reference );
  }

  public ResponderSpecificInformationDTO getResponderSpecific()
  {
    return null;
  }
}
