package com.k_int.openrequest.integration.iface;

import java.util.Hashtable;
import java.util.Date;
import java.io.StringWriter;

/**
 * Title:       ResultSetDTO
 * @version:    $Id: SearchResultEntryDTO.java,v 1.2 2005/05/12 15:15:41 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class SearchResultEntryDTO implements java.io.Serializable
{
  private Long internal_id;
  private String tgq;
  private String requester_reference;
  private String role;
  private String title;
  private String author;
  private String pubdate;
  private String current_partner_id;
  private Date last_message;
  private String state;
  private String state_model;
  private Boolean active;
  private String phase;
  private long unread_message_count;
  private String protocol;
  private String initiating_branch;
  private Long current_trans_id;

  public SearchResultEntryDTO(Long internal_id, 
                  String tgq,
                  String requester_reference,
                  String role,
                  String title,
                  String author,
                  String pubdate,
                  String current_partner_id,
                  Date last_message,
                  String state,
                  String state_model,
                  Boolean active,
                  String phase,
                  long unread_message_count,
                  String protocol,
                  String initiating_branch,
                  Long current_trans_id)
  {
    this.internal_id = internal_id;
    this.tgq = tgq;
    this.requester_reference = requester_reference;
    this.role = role;
    this.title = title;
    this.author = author;
    this.pubdate = pubdate;
    this.current_partner_id = current_partner_id;
    this.last_message = last_message;
    this.state = state;
    this.state_model = state_model;
    this.active = active;
    this.phase = phase;
    this.unread_message_count = unread_message_count;
    this.protocol = protocol;
    this.initiating_branch = initiating_branch;
    this.current_trans_id = current_trans_id;
  }

  public String toString()
  {  
    StringWriter sw = new StringWriter();
    sw.write( "internal_id="+ internal_id );
    sw.write( ",tgq="+ tgq );
    sw.write( ",requester_reference="+ requester_reference );
    sw.write( ",role="+ role );
    sw.write( ",title="+ title );
    sw.write( ",author="+ author );
    sw.write( ",pubdate="+ pubdate );
    sw.write( ",current_partner_id="+ current_partner_id );
    sw.write( ",last_message="+ last_message );
    sw.write( ",state="+ state );
    sw.write( ",state_model="+ state_model );
    sw.write( ",active="+ active );
    sw.write( ",phase="+ phase );
    sw.write( ",unread_message_count="+unread_message_count);
    sw.write( ",protocol="+protocol);
    sw.write( ",initiating_branch="+initiating_branch);

    return sw.toString();
  }

  public TransactionGroupIdDTO getTransGroupId() {
    return new TransactionGroupIdDTO(internal_id);
  }

  public Long getInternalId() {
    return internal_id;
  }

  public String getRequesterRef() {
    return requester_reference;
  }

  public String getTGQ() {
    return tgq;
  }

  public String getTitle() {
    return title;
  }

  public String getAuthor() {
    return author;
  }

  public String getPublication() {
    return pubdate;
  }

  public String getCurrentPartner() {
    return current_partner_id;
  }

  public Date getLastMsgDate() {
    return last_message;
  }

  public String getState() {
    return state;
  }

  public String getStateModel() {
    return state_model;
  }

  public boolean isClosed() {
    if ( active != null ) {
      return !active.booleanValue() ;
    }

    return false;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public Phase getPhase() {
    return Phase.fromIso(Constants.convertPhase(phase));
  }

  public int getUnreadMessageCount() {
    return (int)unread_message_count;
  }

  public ServiceProtocol getProtocol() {  
    return ServiceProtocol.fromIso(Constants.convertProtocol(protocol));
  }

  public Role getRole() {
    return Role.fromIso(Constants.convertRole(role));
  }

  public TransactionGroupIdDTO getCurrentTransGroupId() {
    return new TransactionGroupIdDTO(internal_id);
  }

  public String getInitiatingBranch() {
    return initiating_branch != null ? initiating_branch : "";
  }

  public TransactionIdDTO getCurrentTransId() {
    return new TransactionIdDTO(current_trans_id);
  }
}
