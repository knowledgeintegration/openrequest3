package com.k_int.openrequest.integration.iface;

/**
 * Title:       LostMessageDTO
 * @version:    $Id: LostMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class LostMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  private String note;

  public LostMessageDTO()
  {
    this.message_type = MessageType.LOST.toIso();
  }

  public LostMessageDTO(String note)
  {
    this.message_type = MessageType.LOST.toIso();
    this.note = note;
  }

  public String getNote()
  {
    return note;
  }
}
