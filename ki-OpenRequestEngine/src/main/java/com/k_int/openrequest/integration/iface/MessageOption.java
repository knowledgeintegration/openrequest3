package com.k_int.openrequest.integration.iface;

public enum MessageOption {

  REQUIRES, DESIRES, NEITHER;

  public String toString() {
    switch ( this ) {
      case REQUIRES: return "Requires";
      case DESIRES: return "Desires";
      case NEITHER: return "Neither";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case REQUIRES: return 1;
      case DESIRES: return 2;
      case NEITHER: return 3;
      default: return 0;
    }
  }

  public static MessageOption fromIso(int iso){
    switch ( iso ) {
      case 1: return REQUIRES;
      case 2: return DESIRES;
      case 3: return NEITHER;
      default: return null;
    }
  }
}
