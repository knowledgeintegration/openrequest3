package com.k_int.openrequest.integration.iface;

/**
 * Title:       CostInfoTypeDTO
 * @version:    $Id: CostInfoTypeDTO.java,v 1.3 2005/04/21 10:08:38 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class CostInfoTypeDTO implements java.io.Serializable
{
  public String account_number;
  public String currency_code;
  public String value;
  public boolean reciprocal_agreement = false;
  public boolean will_pay_fee = false;
  public boolean payment_provided = false;

  public CostInfoTypeDTO() {
  }

  public CostInfoTypeDTO(String account_number,
                        String currency,
                        String value,
                        boolean will_pay_fee) {
    this(account_number,currency,value,will_pay_fee,false,false);
  }

  public CostInfoTypeDTO(String account_number,
                        String currency,
                        String value,
                        boolean will_pay_fee,
                        boolean reciprocal_agreement,
                        boolean payment_provided) {
    this.account_number = account_number;
    this.currency_code = currency;
    this.value = value;
    this.will_pay_fee = will_pay_fee;
    this.reciprocal_agreement = reciprocal_agreement;
    this.payment_provided = payment_provided;
  }

  public String getAccountNumber() {
    return account_number;
  }

  public String getCostCurrencyCode() {
    return currency_code;
  }

  public String getCostMonetaryValue() {
    return value;
  }

  public boolean isWillPayFee() {
    return will_pay_fee;
  }
}
