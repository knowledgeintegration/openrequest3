package com.k_int.openrequest.integration.iface;

public enum ReasonNotAvailable {

  IN_USE_ON_LOAN, IN_PROCESS, ON_ORDER, NOT_YET_AVAILABLE, AT_BINDERY, COST_EXCEEDS_LIMIT, CHARGES, PREPAYMENT_REQUIRED, LACKS_COPYRIGHT_COMPLIANCE, NOT_FOUND_AS_CITED, ON_HOLD, OTHER, RESPONDER_SPECIFIC, NONE;

  public String toString() { 
    switch ( this ) {
      case IN_USE_ON_LOAN: return "In Use On Loan";
      case IN_PROCESS: return "In Process";
      case ON_ORDER: return "On Order";
      case NOT_YET_AVAILABLE: return "Volume Issue Not Yet Available";
      case AT_BINDERY: return "At Bindery";
      case COST_EXCEEDS_LIMIT: return "Cost Exceeds Limit";
      case CHARGES: return "Charges";
      case PREPAYMENT_REQUIRED: return "Prepayment Required";
      case LACKS_COPYRIGHT_COMPLIANCE: return "Lacks Copyright Compliance";
      case NOT_FOUND_AS_CITED: return "Not Found As Cited";
      case ON_HOLD: return "On Hold";
      case OTHER: return "Other";
      case RESPONDER_SPECIFIC: return "Responder Specific";
      case NONE: return "None";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case IN_USE_ON_LOAN: return 1;
      case IN_PROCESS: return 2;
      case ON_ORDER: return 6;
      case NOT_YET_AVAILABLE: return 7;
      case AT_BINDERY: return 8;
      case COST_EXCEEDS_LIMIT: return 13;
      case CHARGES: return 14;
      case PREPAYMENT_REQUIRED: return 15;
      case LACKS_COPYRIGHT_COMPLIANCE: return 16; 
      case NOT_FOUND_AS_CITED: return 17;
      case ON_HOLD: return 19;
      case OTHER: return 27;
      case RESPONDER_SPECIFIC: return 28;
      case NONE: return 0;
      default: return 0;
    }
  }

  public static ReasonNotAvailable fromIso(int iso){
    switch ( iso ) {
      case 0: return NONE;
      case 1: return IN_USE_ON_LOAN;
      case 2: return IN_PROCESS;
      case 6: return ON_ORDER;
      case 7: return NOT_YET_AVAILABLE;
      case 8: return AT_BINDERY;
      case 13: return COST_EXCEEDS_LIMIT;
      case 14: return CHARGES;
      case 15: return PREPAYMENT_REQUIRED;
      case 16: return LACKS_COPYRIGHT_COMPLIANCE;
      case 17: return NOT_FOUND_AS_CITED;
      case 19: return ON_HOLD;
      case 27: return OTHER;
      case 28: return RESPONDER_SPECIFIC;
      default: return null;
    }
  }

}
