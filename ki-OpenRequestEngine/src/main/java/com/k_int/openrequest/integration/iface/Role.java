package com.k_int.openrequest.integration.iface;

import java.util.*;

public enum Role {

  REQUESTER, RESPONDER;

  public String toString() {
    switch(this) {
      case REQUESTER: return "REQ";
      case RESPONDER: return "RESP";
      default: return null;
    }
  }

  public int toIso() {
    switch(this) {
      case REQUESTER: return 1;
      case RESPONDER: return 2;
      default: return 0;
    }
  }

  public static Role fromIso(int iso){
    switch ( iso ) {
      case 1: return REQUESTER;
      case 2: return RESPONDER;
      default: return null;
    }
  }
}
