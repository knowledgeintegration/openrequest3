package com.k_int.openrequest.integration.iface;

/**
 * Title:       BriefMessageDTO
 * @version:    $Id: BriefMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class BriefMessageDTO implements java.io.Serializable
{
  public BriefMessageDTO()
  {
  }
}
