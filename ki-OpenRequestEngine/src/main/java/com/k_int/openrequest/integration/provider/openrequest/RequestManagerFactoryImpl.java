package com.k_int.openrequest.integration.provider.openrequest;

import com.k_int.openrequest.services.*;
import com.k_int.openrequest.integration.iface.*;
import com.k_int.openrequest.integration.provider.base.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.Map;
import java.rmi.*;

/*
 * $Log: RequestManagerFactoryImpl.java,v $
 * Revision 1.4  2005/04/21 10:08:38  ibbo
 * Fixed currency code bug
 *
 * Revision 1.3  2005/02/21 10:02:58  ibbo
 * Updated
 *
 * Revision 1.2  2005/02/20 18:30:42  ibbo
 * Updated
 *
 * Revision 1.1.1.1  2005/02/14 11:46:29  ibbo
 * Import
 *
 * Revision 1.4  2004/08/01 12:38:24  ibbo
 * Updated
 *
 * Revision 1.3  2004/07/25 17:35:56  ibbo
 * Updated
 *
 * Revision 1.2  2004/07/23 15:07:27  ibbo
 * Updated
 *
 * Revision 1.1.1.1  2004/07/23 07:13:17  ibbo
 * Initial
 *
 * Revision 1.6  2003/06/02 10:39:37  imi
 * Updated
 *
 * Revision 1.5  2003/05/01 10:15:46  imi
 * Updated
 *
 * Revision 1.4  2003/05/01 08:53:20  imi
 * Many changes for revised RMI structure
 *
 * Revision 1.3  2003/04/16 16:30:51  imi
 * added ability to specify host
 *
 * Revision 1.2  2003/04/04 14:21:49  imi
 * Renamed BE to Be Be ;)
 *
 * Revision 1.1.1.1  2003/04/04 10:18:21  imi
 * Initial import
 *
 * Revision 1.2  2003/03/27 13:46:23  imi
 * Refresh, added a number of Value Objects
 *
 * Revision 1.1  2003/03/27 09:43:09  imi
 * Renamed some classes to add BE prefix
 *
 */

/**
 * Title:       RequestManagerFactoryImpl
 * @version:    $Id: RequestManagerFactoryImpl.java,v 1.4 2005/04/21 10:08:38 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */

public class RequestManagerFactoryImpl extends AbstractRequestManagerFactory implements RequestManagerFactory {

  private Log log = LogFactory.getLog(this.getClass());

  public RequestManagerFactoryImpl() throws RequestManagerException {
    super();
  }

  public RequestManager getRequestManager(String location_symbol) throws RequestManagerException {
    log.debug("RequestManagerFactoryImpl::getRequestManager("+location_symbol+")");
    RequestManager result = new RequestManagerImpl(location_symbol, ctx);
    return result;
  }

  public void init() {
    log.debug("RequestManagerFactoryImpl::init");
  }
}
