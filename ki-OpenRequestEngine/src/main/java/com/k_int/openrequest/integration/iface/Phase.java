package com.k_int.openrequest.integration.iface;

public enum Phase {

  NO_RESTRICTION, PROCESSING, TRACKING, SATISFIED, CLOSED, EOR, OPEN;

  public String toString()  {
    switch ( this ) {   
      case NO_RESTRICTION: return "NO RESTRICTION";
      case PROCESSING: return "OPEN (Processing Only)";
      case TRACKING: return "OPEN (Tracking Only)";
      case SATISFIED: return "OPEN (Satisfied Only)";
      case CLOSED: return "CLOSED";
      case EOR: return "END OF ROTA";
      case OPEN: return "OPEN";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case NO_RESTRICTION: return 0;
      case PROCESSING: return 1;
      case TRACKING: return 2;
      case SATISFIED: return 3;
      case CLOSED: return 4;
      case EOR: return 5;
      case OPEN: return 6;
      default: return 0;
    }
  }

  public static Phase fromIso(int iso){
    switch ( iso ) {
      case 0: return NO_RESTRICTION;
      case 1: return PROCESSING;
      case 2: return TRACKING;
      case 3: return SATISFIED;
      case 4: return CLOSED;
      case 5: return EOR;
      case 6: return OPEN;
      default: return null;
    }
  }
}
