package com.k_int.openrequest.integration.iface;

/*
 * $Log: RequestManagerException.java,v $
 * Revision 1.1.1.1  2005/02/14 11:46:29  ibbo
 * Import
 *
 * Revision 1.1  2004/08/01 12:38:24  ibbo
 * Updated
 *
 */

/**
 * Title:       RequestManagerException
 * @version:    $Id: RequestManagerException.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:   
 */
public class RequestManagerException extends Exception
{
  public RequestManagerException(String reason) {
    super(reason);
  }

  public RequestManagerException(String reason, Throwable cause) {
    super(reason, cause);
  }
}

