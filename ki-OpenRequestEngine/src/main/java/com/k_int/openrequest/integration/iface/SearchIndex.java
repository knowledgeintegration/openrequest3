package com.k_int.openrequest.integration.iface;

public enum SearchIndex {

  REQUEST_NO, AUTHOR_KEY, TITLE_KEY, ISBN, ISSN, RESPONDER, BOR_ID;

  public String toString() { 
    switch ( this ) {
      case REQUEST_NO: return "Internal Request Number";
      case AUTHOR_KEY: return "Author Keyword(s)";
      case TITLE_KEY: return "Title Keyword(s)";
      case ISBN: return "ISBN";
      case ISSN: return "ISSN";
      case RESPONDER: return "Responder Reference";
      case BOR_ID: return "Borrower ID";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case REQUEST_NO: return 1;
      case AUTHOR_KEY: return 2;
      case TITLE_KEY: return 3;
      case ISBN: return 4;
      case ISSN: return 5;
      case RESPONDER: return 6;
      case BOR_ID: return 7;
      default: return 0;
    }
  }

  public static SearchIndex fromIso(int iso) {
    switch ( iso ) {
      case 1: return REQUEST_NO;
      case 2: return AUTHOR_KEY;
      case 3: return TITLE_KEY;
      case 4: return ISBN;
      case 5: return ISSN;
      case 6: return RESPONDER;
      case 7: return BOR_ID;
      default: return null;
    }
  }
}
