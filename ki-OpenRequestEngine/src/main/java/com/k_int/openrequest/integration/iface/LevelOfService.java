package com.k_int.openrequest.integration.iface;

public enum LevelOfService {

  LOCAL, EXTENDED, FULL;

  public String toString() {
    switch ( this ) {
      case LOCAL: return "Local";
      case EXTENDED: return "Extended";
      case FULL: return "Full";
      default: return "Unknown";
    }
  }

  public char toIso() {
    switch ( this ) {
      case LOCAL: return '(';
      case EXTENDED: return '{';
      case FULL: return '[';
      default: return ' ';
    }
  }

  public static LevelOfService fromIso(char iso) {
    switch ( iso ) {
      case '(': return LOCAL;
      case '{': return EXTENDED;
      case '[': return FULL;
      default: return null;
    }
  }
}
