//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.integration.dataload.oclc;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.commons.logging.*;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import org.jzkit.a2j.codec.runtime.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.ILL_APDU_Delivery_Info.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import java.net.*;
import java.io.*;
import java.util.*;
import java.math.BigInteger;
import com.k_int.openrequest.isoill.gen.ISO_DATA.*;
import org.hibernate.*;
import org.hibernate.type.*;
import com.k_int.openrequest.helpers.ILLStringHelper;

/**
 * Title:       
 * @version:    $Id: OCLCImport.java,v 1.10 2005/06/30 10:42:12 ibbo Exp $
 * Copyright:   Copyright (C) 2005 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */
public class OCLCImport {

  private static Log log = LogFactory.getLog(OCLCImport.class);

  private static int oclc_roles[] = { 23000001, 23000002, 23000001 };
  private static String roles[] = { "REQ", "RESP", "INT-REQ", "INT-RESP" };
  private static String state_codes[] = { null,
                                          "NOTSUPPLIED",
                                          "PENDING",
                                          "IN-PROCESS",
                                          "FORWARD",
                                          "CONDITIONAL",
                                          "CANCELPENDING",
                                          "CANCELLED",
                                          "SHIPPED",
                                          "RECEIVED",
                                          "RENEWPENDING",
                                          "NOTREC/OVERDUE",
                                          "RENEWOVERDUE",
                                          "OVERDUE",
                                          "RETURNED",
                                          "CHECKEDIN",
                                          "RECALL",
                                          "LOST",
                                          "UNKNOWN" };

  public static int counter = 0;
  public static int bad = 0;
  public static int requester_count = 0;
  public static int responder_count = 0;
  public static int quirk_counter = 0;

  public static void main (String args[])
  {
    Session sess = null;

    if ( args.length < 2 ) {
      System.out.println("usage: OCLCImport infile badfile");
      System.exit(1);
    }

    // Set up the application context
    OutputStream badfile = null;
    try {
      ClassPathXmlApplicationContext app_context = new ClassPathXmlApplicationContext( new String[] { "BaseApplicationContext.xml" } );

      SessionFactory factory = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      sess = factory.openSession();

      InputStream impdata = new  FileInputStream(args[0]);
      badfile = new  FileOutputStream(args[1]);

      boolean running = true;
      String charset_encoding="UTF-8";
      OIDRegister reg = (OIDRegister) app_context.getBean("OIDRegister");
      SAGE_DATA_codec codec = SAGE_DATA_codec.getCodec();
      // BERInputStream bds = new BERInputStream(impdata,charset_encoding,reg);
      ChunkingBERInputStream chunker = new ChunkingBERInputStream(impdata);

      while(running) {
        try {
          log.debug("Waiting for iso_data on input stream");
          byte[] complete_apdu = chunker.getNextCompleteAPDU();

          log.debug("Next pdu : "+complete_apdu.length);
          BERInputStream bds = new BERInputStream(new ByteArrayInputStream(complete_apdu),charset_encoding,reg);
          SAGE_DATA_type iso_data = null;
          iso_data = (SAGE_DATA_type)codec.serialize(bds, iso_data, false, "PDU");
          counter++;

          FileOutputStream fos = new FileOutputStream("/tmp/ber/imp_"+counter+".ber");
          fos.write(complete_apdu);
          fos.flush();
          fos.close();

          log.info("Record#"+counter);

          if ( process1(sess, iso_data, reg) ) {
            log.debug("Commit the new ILL transaction, group and request information to the database");
            sess.flush();
            sess.connection().commit();
            sess.clear();
          }
          else {
            log.warn("Bad (Rollback DB, add record to badfile): "+(bad++));
            sess.connection().rollback();
            sess.clear();
            badfile.write(complete_apdu);
            badfile.flush();
          }
          log.debug("-*-*-*-");
        }
        catch ( java.io.IOException ioe )
        {
          log.warn("Processing java.io.IOException, shut down import", ioe);
          running = false;
        }
        catch ( Exception e )
        {
          log.warn("Processing exception : ",e);
          running=false;
        }
        report();
      }
    }
    catch ( org.hibernate.HibernateException he ) {
      he.printStackTrace();
    }
    catch ( java.io.FileNotFoundException fnfe ) {
      fnfe.printStackTrace();
    }
    finally {
      if ( sess != null ) {
        try { sess.close(); } catch (Exception e) { }
        sess=null;
      }

      if ( badfile != null ) {
        try { badfile.close(); } catch (Exception e) {}
      }
    }
    report();
  }


  public static boolean process1(Session session, SAGE_DATA_type sage_data, OIDRegister reg) {
    log.info("New request");
    if ( sage_data != null ) {
      log.info("sage_data.ill_number                           "+sage_data.illNumber);
      log.debug("sage_data.status                               "+sage_data.status);
      log.debug("sage_data.problemStatus                        "+sage_data.problemStatus);
      log.debug("Decoding iso_data chunk.... (length="+sage_data.iso_data.length+")");

      try {

        // Quick Fix
        //ApduNormalizer n = new ApduNormalizer(sage_data.illNumber.longValue());
        //byte[] new_data = n.normalizeApdu(sage_data.iso_data);
        byte[] new_data = sage_data.iso_data;

        if ( new_data == null ) {
          log.warn("NormalizeAPDU returned null");
          return false;
        }

        ByteArrayInputStream bais = new ByteArrayInputStream(new_data);

        // ByteArrayInputStream bais = new ByteArrayInputStream(sage_data.iso_data);
        ISO_DATA_codec codec = ISO_DATA_codec.getCodec();
        BERInputStream bds = new BERInputStream(bais,"UTF-8",reg);
        ISO_DATA_type iso_data = null;
        iso_data = (ISO_DATA_type)codec.serialize(bds, iso_data, false, "ISO_DATA");
        if ( iso_data != null ) {
          dumpFlags(iso_data.flags);

          log.debug("requester:"+iso_data.requester);
          log.debug("responder:"+iso_data.responder);

          if ( iso_data.requester != null ) {
            log.debug("responder "+(responder_count++));
            process2(session, "RESP",
                     iso_data.requester,
                     sage_data.illNumber.intValue(),
                     sage_data.status.intValue(),
                     sage_data.problemStatus.intValue());
          }
          else {
            log.debug("iso_data.requester was null");
          }

          if ( iso_data.responder != null ) {
            log.debug("requester "+(requester_count++));
            process2(session, 
                     "REQ",
                     iso_data.responder,
                     sage_data.illNumber.intValue(),
                     sage_data.status.intValue(),
                     sage_data.problemStatus.intValue());
          }
          else {
            log.debug("iso_data.responder was null");
          }
        }
        else {
          log.warn(" **** ISO_DATA is null **** ");
          return false;
        }

      }
      catch ( java.io.IOException ioe ) {
        log.warn("*** FAILED DECODING RECORD "+counter+" *** (ioe)",ioe);
        return false;
      }
    }
    else {
      log.warn("*** FAILED DECODING RECORD "+counter+" *** (sage data is null)");
      return false;
    }

    return true;
  }

  public static boolean process2(Session session, 
                                 String role, 
                                 Pm_Data_type data,
                                 int sage_ill_number,
                                 int sage_status,
                                 int sage_problem_status) {

    if ( data != null ) {
      log.debug("Importing for role: "+role);

      try {

        String location_symbol = null;
        String partner_symbol = null;
        String location_name = null;
        String partner_name = null;

        if ( data.iso_id != null ) {
          if ( data.iso_id.person_or_institution_symbol != null ) {
            location_symbol = ILLStringHelper.extract((ILL_String_type)(data.iso_id.person_or_institution_symbol.o));
            if ( data.iso_id.name_of_person_or_institution != null ) 
              location_name = ILLStringHelper.extract((ILL_String_type)(data.iso_id.name_of_person_or_institution.o));
            log.debug("sage_data.data.iso_id.symbol   "+location_symbol);
            log.debug("sage_data.data.iso_id.name   "+location_name);
          }
        }
  
        if ( data.current_partner != null ) {
          if ( data.current_partner.person_or_institution_symbol != null ) {
            partner_symbol = ILLStringHelper.extract((ILL_String_type)(data.current_partner.person_or_institution_symbol.o));
            if ( data.current_partner.name_of_person_or_institution != null ) 
              partner_name = ILLStringHelper.extract((ILL_String_type)(data.current_partner.name_of_person_or_institution.o));
            log.debug("sage_data.data.iso_id.symbol   "+partner_symbol);
            log.debug("sage_data.data.iso_id.name   "+partner_name);
          }
        }

        String resolved_requester_symbol = null;
        String resolved_requester_name = null;
        String resolved_responder_symbol = null;
        String resolved_responder_name = null;
        ArrayList requester_delivery_info = null;
        ArrayList responder_delivery_info = null;

        // Ok... Role
        if ( role.equals("REQ") ) {
          resolved_requester_symbol = partner_symbol;
          resolved_requester_name = partner_name;
          resolved_responder_symbol = location_symbol;
          resolved_responder_name = location_name;
          requester_delivery_info = data.delivery_info.sender_info;
          responder_delivery_info = data.delivery_info.recipient_info;
        }
        else {
          resolved_requester_symbol = location_symbol;
          resolved_requester_name = location_name;
          resolved_responder_symbol = partner_symbol;
          resolved_responder_name = partner_name;
          requester_delivery_info = data.delivery_info.recipient_info;
          responder_delivery_info = data.delivery_info.sender_info;
        }
  
        // First thing we need to do is to get a location for this request
        LocationSymbol requester_location = DBHelper.lookupLocation(session, resolved_requester_symbol);
        LocationSymbol responder_location = DBHelper.lookupLocation(session, resolved_responder_symbol);


        if ( data.delivery_info != null ) {
          if ( requester_location == null ) {
            log.debug("Unable to locate identified location "+resolved_requester_symbol+", importing APDU Delivery Info");
            requester_location = importLocationData(session, resolved_requester_symbol, resolved_requester_name, requester_delivery_info);
          }

          if ( responder_location == null ) {
            log.debug("Unable to locate identified partner location "+resolved_responder_symbol+", importing APDU Delivery Info");
            responder_location = importLocationData(session, resolved_responder_symbol, resolved_responder_name, responder_delivery_info);
          }
        }
        else {
          log.warn("NO DELIVERY INFO");
        }

        String initial_requester = null;
        if ( data.transactionId.initial_requester_id.person_or_institution_symbol != null ) {
          initial_requester = ILLStringHelper.extract((ILL_String_type)(data.transactionId.initial_requester_id.person_or_institution_symbol.o));
          log.debug("sage_data.data.tid.initial_req   "+initial_requester);
        }
  
        String tgq = ILLStringHelper.extract(data.transactionId.transaction_group_qualifier);
        String tq = ILLStringHelper.extract(data.transactionId.transaction_qualifier);
        String stq = ILLStringHelper.extract(data.transactionId.sub_transaction_qualifier);
        int current_state = 0;

        log.info("data.tid.tgq           "+tgq);
        log.info("data.tid.tq            "+tq);
        log.info("data.tid.stq           "+stq);
        log.info("requester_symbol       "+resolved_requester_symbol);
        log.info("requester_name         "+resolved_requester_name);
        log.info("responder_symbol       "+resolved_responder_symbol);
        log.info("responder_name         "+resolved_responder_name);

  
        int sage_role = -1;
        if ( data.variables != null ) {
          log.debug("data.variables length ="+data.variables.length);
          dumpFlags(data.variables);

          // Process flags
          byte flags[] = data.variables;
  
          int role_int = extractShort(flags[0], flags[1]);;
          log.debug("data.variables.Role : "+role_int);
          sage_role = role_int;
          int version = extractShort(flags[2], flags[3]);
          int return_var = extractShort(flags[4], flags[5]);
          int fwd = extractShort(flags[6], flags[7]);
          int part = extractShort(flags[8], flags[9]);
          int chain = extractShort(flags[10], flags[11]);
          int shipped_service_type = extractShort(flags[12], flags[13]);
          int transaction_results = extractShort(flags[14], flags[15]);
          int transaction_type = extractShort(flags[16], flags[17]);
          current_state = extractShort(flags[18], flags[19]);
  
          log.debug("data.variables.Current State:"+current_state+" = "+state_codes[current_state]);

          int expiry_disable = extractShort(flags[20], flags[21]);
          int sequence_flag = extractShort(flags[11], flags[22]);
          int repeat_flag = extractShort(flags[23], flags[24]);
          int most_recent_service = extractShort(flags[25], flags[26]);
          int conditions = extractShort(flags[27], flags[28]);
          int reasonNotAvailable = extractShort(flags[29], flags[30]);
          int reasonUnfilled = extractShort(flags[31], flags[32]);
          int reasonWillSupply = extractShort(flags[33], flags[34]);
          byte[] time_array = new byte[15];
          System.arraycopy(flags, 35, time_array, 0, 14);
          String sequence_ts = new String(time_array);
          System.arraycopy(flags, 49, time_array, 0, 14);
          String repeat_ts = new String(time_array);
          char expiry_timer = (char) flags[63];
          char date_for_reply = (char) flags[64];
          System.arraycopy(flags, 65, time_array, 0, 14);
          String last_transition = new String(time_array);
          log.debug("last_trans : "+last_transition);
          System.arraycopy(flags, 79, time_array, 0, 14);
          String most_recent_service_ts = new String(time_array);
          int control_request = flags[93];
          int control_response = flags[94];
        }
        else {
          log.debug("data.variables = null");
        }

        ILLTransaction trans = DBHelper.lookupTransaction(session,
                                                          requester_location.getCanonical_location().getId(),
                                                          initial_requester,
                                                          tgq,
                                                          tq,
                                                          stq);

        log.debug("Already located a transaction with those details");

        if ( trans == null ) {

          RequiredItemDetails item_details = new RequiredItemDetails();

          log.debug("Creating new trans, req="+initial_requester+", tgq="+tgq+", tq="+tq+", role="+role);

          trans = new ILLTransaction(initial_requester,
                                     resolved_requester_symbol,
                                     tgq,
                                     tq,
                                     stq,
                                     role);

          trans.setResponderSymbol(resolved_responder_symbol);

          ILLTransactionGroup trans_group = DBHelper.createOrFindExistingTG(requester_location.getCanonical_location(),
                                                                            initial_requester,
                                                                            location_symbol,
                                                                            tgq,
                                                                            session,
                                                                            role);

          trans_group.setRequiredItemDetails(item_details);
          // Printed
          trans_group.getRequiredItemDetails().getSupplyMediumInfoType().add(new MediumInfo(1,null));
          // Photocopy
          trans_group.getRequiredItemDetails().getSupplyMediumInfoType().add(new MediumInfo(2,null));

          // printed (1), photocopy (2), microform (3), film-or-video-recording (4), audio-recording (5), machine-readable (6), other (7)
          trans_group.getRequiredItemDetails().getService_types().add(new Integer(1));
          trans_group.getRequiredItemDetails().getService_types().add(new Integer(2));
          // loan    (1), copy-non-returnable     (2), locations       (3), estimate        (4), responder-specific      (5)


          trans_group.setLastTransaction(trans);
          trans.setTransactionGroup(trans_group);
      
          if ( role.equals("REQ") ) {
            trans.setActiveStateModel(DBHelper.lookupStateModel(session,"REQ/PRO"));
          }
          else {
            trans.setActiveStateModel(DBHelper.lookupStateModel(session,"RES/PRO"));
          }
          trans.setRequestStatusCode(DBHelper.lookupStatusCode(session,state_codes[current_state]));


          log.debug("Save transaction");
          Long new_trans_id = (Long) session.save(trans);
          System.err.println("New transaction id: "+new_trans_id);

          if ( role.equals("REQ") ) {
            trans_group.setTransactionGroupStatusCode(DBHelper.lookupStatusCode(session,"IN-PROCESS"));
            trans_group.setActiveStateModel(DBHelper.lookupStateModel(session,"REQ/TG"));
          }
          else if (role.equals("RESP") ) {
            trans_group.setTransactionGroupStatusCode(DBHelper.lookupStatusCode(session,"IN-PROCESS"));
            trans_group.setActiveStateModel(DBHelper.lookupStateModel(session,"RES/TG"));
          }

          String[] oclc_symbol_components = location_symbol.split(":");
          if ( ! oclc_symbol_components[0].equalsIgnoreCase("OCLC") ) {
            log.warn("WARNING: Symbol namespace was not OCLC");
          }

          String[] partner_oclc_symbol_components = partner_symbol.split(":");
          if ( ! partner_oclc_symbol_components[0].equalsIgnoreCase("OCLC") ) {
            log.warn("WARNING: Symbol namespace for PARTNER was not OCLC");
          }
          session.update(trans_group);
          ISOWRSMappingHDO mapping_data = new ISOWRSMappingHDO();
          mapping_data.setId(new_trans_id);
          mapping_data.setTransId(partner_oclc_symbol_components[1]);
          mapping_data.setRole(oclc_roles[sage_role]);
          mapping_data.setIllNumber(sage_ill_number);
          mapping_data.setStatus(sage_status);
          mapping_data.setProblemStatus(sage_problem_status);
          mapping_data.setRequester(resolved_requester_symbol);
          mapping_data.setSymbol(oclc_symbol_components[1]);
          session.save(mapping_data);

        }
      }
      catch( java.sql.SQLException se) {
        se.printStackTrace();
      }
      catch ( org.hibernate.HibernateException he ) {
        he.printStackTrace();
      }
      catch ( com.k_int.openrequest.helpers.HelperException he ) {
        he.printStackTrace();
      }
      finally {
      }
    }
    else {
      log.debug("data = null");
    }
  
    return true;
  }
  
  private static void report() {
    System.err.println("Report : imported="+counter+" bad="+bad+" requester="+requester_count+" responder="+responder_count+" quirk="+quirk_counter);
  }

  private static LocationSymbol importLocationData(Session session, 
                                                   String auth_symbol, 
                                                   String auth_name, 
                                                   ArrayList info) throws java.sql.SQLException, 
                                                                          org.hibernate.HibernateException{
    log.debug("Lookup location symbol record for "+auth_symbol);

    if ( ( info != null ) && ( info.size() > 0 ) )
      APDUDeliveryInfoStorageHelper.checkPartner(session,
                                                 (APDU_Delivery_Parameters_type)info.get(0),
                                                 auth_symbol,
                                                 (auth_name != null ? auth_name : auth_symbol));
    else
      addSimpleLocationData(session, auth_symbol, (auth_name != null ? auth_name : auth_symbol));

    log.debug("Import completed.. repeat lookup: "+auth_symbol);

    LocationSymbol result = DBHelper.lookupLocation(session, auth_symbol);

    return result;
  }


  private static LocationSymbol addSimpleLocationData(Session session,
                                                      String auth_symbol,
                                                      String auth_name) throws java.sql.SQLException, org.hibernate.HibernateException {
    // Create a new location and import all aliases.
    log.debug("addSimpleLocationData - No location found, Creating a new location");

    Location l = new Location();
    l.setName(auth_name != null ? auth_name : auth_symbol);
    l.setDefaultSymbol(auth_symbol);
    session.save(l);

    // Add in an alias for the sending location itself (Some implementations don't list the
    // sending symbol in the aliases list
    return APDUDeliveryInfoStorageHelper.addAlias(session, l, auth_symbol, auth_name, null);
  }


  private static void dumpFlags(byte[] flags) {
    log.debug("Length of flags : "+flags.length);
    String flag_data = "";
    for ( int i=0; i<flags.length; i++) {
      flag_data = flag_data + ((int)(flags[i])) + " ";
    }
    log.debug("flags="+flag_data);
  }

  public static int extractShort(byte one, byte two) {
    BigInteger i = new BigInteger( new byte[] { one, two } );
    return i.intValue();
  }
}
