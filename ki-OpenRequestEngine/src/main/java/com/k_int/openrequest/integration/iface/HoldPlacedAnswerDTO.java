package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       HoldPlacedAnswerDTO
 * @version:    $Id: HoldPlacedAnswerDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowlege Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class HoldPlacedAnswerDTO extends ILLAnswerMessageDTO implements java.io.Serializable
{
  private Date estimated_date_available;
  private int hold_placed_medium_type;
  private String responder_note;

  public HoldPlacedAnswerDTO()
  {
    super();
  }

  public HoldPlacedAnswerDTO(Date estimated_date_available,
                            MediumType hold_placed_medium_type,
                            String responder_note)
  {
    super();
    this.estimated_date_available = estimated_date_available;
    if ( hold_placed_medium_type != null )
      this.hold_placed_medium_type = hold_placed_medium_type.toIso();
    this.responder_note = responder_note;
  }

  public Date getEstimatedDateAvailable()
  {
    return estimated_date_available;
  }

  public MediumType getHoldPlacedMediumType()
  {
    return MediumType.fromIso(hold_placed_medium_type);
  }

  public int getISOHoldPlacedMediumType()
  {
    return hold_placed_medium_type;
  }

  public String getResponderNote()
  {
    return responder_note;
  }
}
