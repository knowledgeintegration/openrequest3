package com.k_int.openrequest.integration.iface;

/**
 * Title:       ElectronicDeliveryDTO
 * @version:    $Id: ElectronicDeliveryServiceDTO.java,v 1.1 2005/06/11 12:35:48 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ElectronicDeliveryServiceDTO extends DeliveryServiceDTO implements java.io.Serializable
{
  public ElectronicDeliveryServiceDTO() {
  }

}
