package com.k_int.openrequest.integration.iface;

import java.util.Date;
import java.util.List;

/**
 * Title:       ExtensionDTO
 * @version:    $Id: OCLCRequestExtension.java,v 1.2 2005/07/01 08:59:11 ibbo Exp $
 * Copyright:   Copyright (C) 2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class OCLCRequestExtension extends ProtocolMessageExtensionDTO {

  public String client_department;
  public String payment_method;
  public String uniform_title;
  public String dissertation;
  public String issue_number;
  public String volume;
  public String affiliations;
  public String source;

  public OCLCRequestExtension() {
  }

  public OCLCRequestExtension(String client_department,
                              String payment_method,
                              String uniform_title,
                              String dissertation,
                              String issue_number,
                              String volume,
                              String affiliations,
                              String source) {
    this.client_department=client_department;
    this.payment_method=payment_method;
    this.uniform_title=uniform_title;
    this.dissertation=dissertation;
    this.issue_number=issue_number;
    this.volume=volume;
    this.affiliations=affiliations;
    this.source=source;
  }

  public String getClientDepartment() {
    return client_department;
  }

  public void setClientDepartment(String client_department) {
    this.client_department=client_department;
  }

  public String getPaymentMethod() {
    return payment_method;
  }

  public void setPaymentMethod(String payment_method) {
    this.payment_method=payment_method;
  }

  public String getUniformTitle() {
    return uniform_title;
  }

  public void setUniformTitle(String uniform_title) {
    this.uniform_title=uniform_title;
  }

  public String getDissertation() {
    return dissertation;
  }

  public void setDissertation(String dissertation) {
    this.dissertation=dissertation;
  }

  public String getIssueNumber() {
    return issue_number;
  }

  public void setIssueNumber(String issue_number) {
    this.issue_number=issue_number;
  }

  public String getVolume() {
    return volume;
  }

  public void setVolume(String volume) {
    this.volume=volume;
  }

  public String getAffiliations() {
    return affiliations;
  }

  public void setAffiliations(String affiliations) {
    this.affiliations=affiliations;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source=source;
  }
}
