package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       ReceivedMessageDTO
 * @version:    $Id: ReceivedMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ReceivedMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  private Date date_received;
  private int shipped_service_type;
  private String requester_note;

  public ReceivedMessageDTO()
  {
    this.message_type = MessageType.RECEIVED.toIso();
  }

  public ReceivedMessageDTO(Date date_received,
                    ServiceType shipped_service_type,
                    String requester_note)
  {
    this.message_type = MessageType.RECEIVED.toIso();
    this.date_received = date_received;
    this.shipped_service_type = shipped_service_type.toIso();
    this.requester_note = requester_note;
  }

  public Date getDateReceived()
  {
    return date_received;
  }

  public ServiceType getServiceType()
  {
    return ServiceType.fromIso(shipped_service_type);
  }

  public int getISOServiceType() {
    return shipped_service_type;
  }

  public String getRequesterNote()
  {
    return requester_note;
  }
}
