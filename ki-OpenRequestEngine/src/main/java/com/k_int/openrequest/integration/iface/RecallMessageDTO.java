package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       RecallMessageDTO
 * @version:    $Id: RecallMessageDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class RecallMessageDTO extends ProtocolMessageDTO implements java.io.Serializable
{
  private String responder_note;

  public RecallMessageDTO()
  {
    this.message_type = MessageType.RECALL.toIso();
  }

  public RecallMessageDTO(String responder_note)
  {
    this.message_type = MessageType.RECALL.toIso();
    this.responder_note = responder_note;
  }

  public String getResponderNote()
  {
    return responder_note;
  }
}
