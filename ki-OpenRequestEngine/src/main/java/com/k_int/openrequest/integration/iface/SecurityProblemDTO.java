package com.k_int.openrequest.integration.iface;

/**
 * Title:       SecurityProblemDTO
 * @version:    $Id: SecurityProblemDTO.java,v 1.2 2005/06/10 16:00:56 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class SecurityProblemDTO extends UserErrorReportDTO implements java.io.Serializable {

  private String report;

  public SecurityProblemDTO() {
  }

  public SecurityProblemDTO(String report) {
    this.report = report;
  }

  public String getReport() {
    return report;
  }
}
