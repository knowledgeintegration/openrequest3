package com.k_int.openrequest.integration.provider.base;

import com.k_int.openrequest.integration.iface.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.Hashtable;
import java.rmi.*;
import java.rmi.server.*;


/*
 * $Log: AbstractRequestManager.java,v $
 * Revision 1.7  2005/04/25 20:34:31  ibbo
 * Updated
 *
 * Revision 1.6  2005/04/10 15:28:56  ibbo
 * Added TG Next sync method to interface
 *
 * Revision 1.5  2005/04/09 16:45:14  ibbo
 * Started to add framework for message delivery notification
 *
 * Revision 1.4  2005/02/24 19:16:49  ibbo
 * updated
 *
 * Revision 1.3  2005/02/22 16:17:14  ibbo
 * Added retrieve transaction method to request manager api
 *
 * Revision 1.2  2005/02/21 10:02:58  ibbo
 * Updated
 *
 * Revision 1.1.1.1  2005/02/14 11:46:29  ibbo
 * Import
 *
 * Revision 1.2  2004/08/01 12:38:24  ibbo
 * Updated
 *
 */

/**
 * Title:       AbstractRequestManager
 * @version:    $Id: AbstractRequestManager.java,v 1.7 2005/04/25 20:34:31 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public abstract class AbstractRequestManager {

  public AbstractRequestManager() throws RequestManagerException {
  }

  public abstract String getVersion() throws RequestManagerException;
  public abstract TransactionGroupIdDTO createTransGroup(RequesterViewDTO request, RotaElementDTO[] rota) throws RequestManagerException, DuplicateUserReferenceException; 
  public abstract TransactionGroupIdDTO createTransGroup(RequesterViewDTO request, 
                                                         RotaElementDTO[] rota,
                                                         String mandatory_tgq) throws RequestManagerException, DuplicateUserReferenceException; 
  public abstract void startMessaging(TransactionGroupIdDTO id) throws RequestManagerException;
  public abstract Long startMessagingSync(TransactionGroupIdDTO id) throws RequestManagerException;
  public abstract TransGroupInfoDTO retrieveTransGroup(TransactionGroupIdDTO id) throws RequestManagerException;
  public abstract TransactionInfoDTO retrieveTransaction(TransactionIdDTO id) throws RequestManagerException;
  public abstract ProtocolMessageDTO[] retrieveMessageHistory(TransactionGroupIdDTO id) throws RequestManagerException;
  public abstract ProtocolMessageDTO[] retrieveMessageHistory(TransactionIdDTO id) throws RequestManagerException;
  public abstract void updateRequest(TransactionGroupIdDTO id, RequesterViewDTO request) throws RequestManagerException;
  public abstract void updateRequest(TransactionGroupIdDTO id, ItemIdDTO item_data, ResponderSpecificInformationDTO request) throws RequestManagerException;
  public abstract void replaceUnusedRota(TransactionGroupIdDTO id, RotaElementDTO[] new_rota) throws RequestManagerException;
  public abstract ProtocolMessageDTO retrieveMessage(MessageIdDTO message_id) throws RequestManagerException;
  public abstract void sendMessage(TransactionIdDTO transaction_id, ProtocolMessageDTO message) throws RequestManagerException;
  public abstract void repeatLastService(TransactionIdDTO transaction_id) throws RequestManagerException;

  public abstract void acknowledgeMessage(MessageIdDTO message_id) throws RequestManagerException;
  public abstract void acknowledgeAllMessages(TransactionGroupIdDTO transaction_id) throws RequestManagerException;
  public abstract void closeTransGroup(TransactionGroupIdDTO transaction_id, int reason) throws RequestManagerException;
  public abstract void closeTransGroup(TransactionGroupIdDTO transaction_id) throws RequestManagerException;
  public abstract void changeCloseReason(TransactionGroupIdDTO transaction_id, int reason) throws RequestManagerException;
  public abstract RequestCountDTO[] retrieveRequestCounts() throws RequestManagerException;
  public abstract SearchResultDTO searchQuery(RequestManagerQueryDTO query) throws RequestManagerException;
  public abstract SearchResultDTO searchCategory(String category) throws RequestManagerException;
  public abstract ResultSetDTO retrieveResultSet(SearchResultDTO result, long count, int start) throws RequestManagerException;
  public abstract void releaseSearchResult(SearchResultDTO result) throws RequestManagerException;
  public abstract void release() throws RequestManagerException;
}
