package com.k_int.openrequest.integration.iface;

public enum SupplyMediumType {

  PRINTED, PHOTOCOPY, MICROFORM, FILM_VIDEO, AUDIO, MACHINE, OTHER, NONE;

  public String toString() {
    switch ( this ) {
      case PRINTED: return "Printed";
      case PHOTOCOPY: return "Photocopy";
      case MICROFORM: return "Microform";
      case FILM_VIDEO: return "Film Video";
      case AUDIO: return "Audio";
      case MACHINE: return "Machine";
      case OTHER: return "Other";
      case NONE: return "None";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case PRINTED: return 1;
      case PHOTOCOPY: return 2;
      case MICROFORM: return 3;
      case FILM_VIDEO: return 4;
      case AUDIO: return 5;
      case MACHINE: return 6;
      case OTHER: return 7;
      case NONE: return 0;
      default: return 0;
    }
  }

  public static SupplyMediumType fromIso(int iso){
    switch (iso) {
      case 1: return PRINTED;
      case 2: return PHOTOCOPY;
      case 3: return MICROFORM;
      case 4: return FILM_VIDEO;
      case 5: return AUDIO;
      case 6: return MACHINE;
      case 7: return OTHER;
      case 0: return NONE;
      default: return null;
    }
  }
}
