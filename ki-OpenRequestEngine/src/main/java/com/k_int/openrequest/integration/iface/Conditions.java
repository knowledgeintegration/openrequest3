package com.k_int.openrequest.integration.iface;

public enum Conditions {

  COST_EXCEEDS_LIMIT, CHARGES, PREPAYMENT_REQUIRED, LACKS_COPYRIGHT_COMPLIANCE, LIBRARY_USE_ONLY, NO_REPRODUCTION, CLIENT_SIGNATURE_REQUIRED, SUPERVISION_REQUIRED, OTHER, RESPONDER_SPECIFIC, PROPOSED_DELIVERY_SERVICE;

  public String toString() { 
    switch (this) {
      case COST_EXCEEDS_LIMIT: return "Cost Exceeds Limit";
      case CHARGES: return "Charges";
      case PREPAYMENT_REQUIRED: return "Prepayment Required";
      case LACKS_COPYRIGHT_COMPLIANCE: return "Lacks Copyright Compliance";
      case LIBRARY_USE_ONLY: return "Library-Use Only";
      case NO_REPRODUCTION: return "No Reproduction";
      case CLIENT_SIGNATURE_REQUIRED: return "Client Signature Required";
      case SUPERVISION_REQUIRED: return "Special Collections Supervision Required";
      case OTHER: return "Other";
      case RESPONDER_SPECIFIC: return "Responder Specific";
      case PROPOSED_DELIVERY_SERVICE: return "Proposed Delivery Service";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch (this) {
      case COST_EXCEEDS_LIMIT: return 13;
      case CHARGES: return 14;
      case PREPAYMENT_REQUIRED: return 15;
      case LACKS_COPYRIGHT_COMPLIANCE: return 16;
      case LIBRARY_USE_ONLY: return 22;
      case NO_REPRODUCTION: return 23;
      case CLIENT_SIGNATURE_REQUIRED: return 24;
      case SUPERVISION_REQUIRED: return 25;
      case OTHER: return 27;
      case RESPONDER_SPECIFIC: return 28;
      case PROPOSED_DELIVERY_SERVICE: return 30;
      default: return 27;
    }
  }

  public static Conditions fromIso(int iso) {
    switch ( iso ) {
      case 13: return COST_EXCEEDS_LIMIT;
      case 14: return CHARGES;
      case 15: return PREPAYMENT_REQUIRED;
      case 16: return LACKS_COPYRIGHT_COMPLIANCE;
      case 22: return LIBRARY_USE_ONLY;
      case 23: return NO_REPRODUCTION;
      case 24: return CLIENT_SIGNATURE_REQUIRED;
      case 25: return SUPERVISION_REQUIRED;
      case 27: return OTHER;
      case 28: return RESPONDER_SPECIFIC;
      case 30: return PROPOSED_DELIVERY_SERVICE;
      default: return null;
    }
  }
}
