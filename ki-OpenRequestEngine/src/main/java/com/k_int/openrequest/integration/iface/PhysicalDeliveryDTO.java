package com.k_int.openrequest.integration.iface;

/**
 * Title:       PhysicalDeliveryDTO
 * @version:    $Id: PhysicalDeliveryDTO.java,v 1.2 2005/03/29 23:03:37 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class PhysicalDeliveryDTO extends DeliveryServiceDTO implements java.io.Serializable
{
  private String transportation_mode = null;

  public PhysicalDeliveryDTO(BeTransportationMode transportation_mode) {
    super(BeDeliveryType.PHYSICAL);
    this.transportation_mode = transportation_mode.toIso();
  }

  public PhysicalDeliveryDTO(String transportation_mode) {
    super(BeDeliveryType.PHYSICAL);
    this.transportation_mode = transportation_mode;
  }

  public String getTransportationMode() {
    return transportation_mode;
  }

  public void setTransportationMode(BeTransportationMode transportation_mode) {
    this.transportation_mode = transportation_mode.toIso();
  }

  public void setTransportationMode(String transportation_mode) {
    this.transportation_mode = transportation_mode;
  }
}
