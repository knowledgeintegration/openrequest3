package com.k_int.openrequest.integration.iface;

public enum ReasonWillSupply {


  IN_USE_ON_LOAN,
  IN_PROCESS,
  ON_ORDER,
  AT_BINDERY,
  ON_HOLD,
  BEING_PROCESSED_FOR_SUPPLY,
  OTHER,
  RESPONDER_SPECIFIC,
  ELECTRONIC_DELIVERY;

  public String toString() {
    switch ( this ) {
      case IN_USE_ON_LOAN: return "In use on loan";
      case IN_PROCESS: return "In Process";
      case ON_ORDER: return "On Order";
      case AT_BINDERY: return "At Bindery";
      case ON_HOLD: return "On Hold";
      case BEING_PROCESSED_FOR_SUPPLY: return "Being Processed for Supply";
      case OTHER: return "Other";
      case RESPONDER_SPECIFIC: return "Responder Specific";
      case ELECTRONIC_DELIVERY: return "Electronic Delivery";
      default: return "None";
    }
  }

  public int toIso() {
    switch ( this ) {
      case IN_USE_ON_LOAN: return 1;
      case IN_PROCESS: return 2;
      case ON_ORDER: return 6;
      case AT_BINDERY: return 8;
      case ON_HOLD: return 19;
      case BEING_PROCESSED_FOR_SUPPLY: return 26;
      case OTHER: return 27;
      case RESPONDER_SPECIFIC: return 28;
      case ELECTRONIC_DELIVERY: return 30;
      default: return 0;
    }
  }

  public static ReasonWillSupply fromIso(int iso){
    switch ( iso ) {
      case 1: return IN_USE_ON_LOAN;
      case 2: return IN_PROCESS;
      case 6: return ON_ORDER;
      case 8: return AT_BINDERY;
      case 19: return ON_HOLD;
      case 26: return BEING_PROCESSED_FOR_SUPPLY;
      case 27: return OTHER;
      case 28: return RESPONDER_SPECIFIC;
      case 30: return ELECTRONIC_DELIVERY;
      default: return null;
    }
  }
}
