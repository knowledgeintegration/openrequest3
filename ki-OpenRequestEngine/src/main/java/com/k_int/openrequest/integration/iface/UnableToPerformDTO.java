package com.k_int.openrequest.integration.iface;

import java.math.BigInteger;

/**
 * Title:       UnableToPerformDTO
 * @version:    $Id: UnableToPerformDTO.java,v 1.2 2005/06/10 16:00:56 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class UnableToPerformDTO extends UserErrorReportDTO implements java.io.Serializable {
  public static UnableToPerformDTO NOT_AVAILABLE = new UnableToPerformDTO(1);
  public static UnableToPerformDTO RESOURCE_LIMITATION = new UnableToPerformDTO(2);
  public static UnableToPerformDTO OTHER = new UnableToPerformDTO(3);

  private long code = 0;

  public UnableToPerformDTO(long code) {
    this.code = code;
  }

  public BigInteger getProblemCode() {
    return BigInteger.valueOf(code);
  }

}
