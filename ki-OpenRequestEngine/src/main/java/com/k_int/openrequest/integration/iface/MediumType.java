package com.k_int.openrequest.integration.iface;

public enum MediumType {

  NONE, PRINTED, PHOTOCOPY, MICROFORM, FILM_VIDEO, AUDIO, MACHINE, OTHER;

  public String toString() {
    switch ( this ) {
      case NONE: return "None";
      case PRINTED: return "Printed";
      case PHOTOCOPY: return "Photocopy";
      case MICROFORM: return "Microform";
      case FILM_VIDEO: return "Film or Video recording";
      case AUDIO: return "Audio Recording";
      case MACHINE: return "Machine Readable";
      case OTHER: return "Other";
      default: return "None";
    }
  }

  public int toIso() {
    switch ( this ) {
      case NONE: return 0;
      case PRINTED: return 1;
      case PHOTOCOPY: return 2;
      case MICROFORM: return 3;
      case FILM_VIDEO: return 4;
      case AUDIO: return 5;
      case MACHINE: return 6;
      case OTHER: return 7;
      default: return 0;
    }
  }

  public static MediumType fromIso(int iso){
    switch ( iso ) {
      case 0: return NONE;
      case 1: return PRINTED;
      case 2: return PHOTOCOPY;
      case 3: return MICROFORM;
      case 4: return FILM_VIDEO;
      case 5: return AUDIO;
      case 6: return MACHINE;
      case 7: return OTHER;
      default: return null;
    }
  }
}
