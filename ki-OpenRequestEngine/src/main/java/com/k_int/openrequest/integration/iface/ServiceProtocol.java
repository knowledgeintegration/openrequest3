package com.k_int.openrequest.integration.iface;

public enum ServiceProtocol {

  ISO, EMAIL, OTHER;

  public String toString() {
    switch ( this ) {
      case ISO: return "ISO";
      case EMAIL: return "EMAIL";
      case OTHER: return "OTHER";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case ISO: return 1;
      case EMAIL: return 2;
      case OTHER: return 3;
      default: return 0;
    }
  }

  public static ServiceProtocol fromIso(int iso) {
    switch ( iso ) {
      case 1: return ISO;
      case 2: return EMAIL;
      case 3: return OTHER;
      default: return null;
    }
  }
}
