package com.k_int.openrequest.integration.iface;

/**
 * Title:       TransGroupInfoDTO
 * @version:    $Id: TransGroupInfoDTO.java,v 1.2 2005/04/09 16:45:14 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class TransGroupInfoDTO implements java.io.Serializable {
  private TransactionGroupIdDTO id;
  // private int status;
  private StatusDTO status;
  private RequestView request_view;
  private TransactionInfoDTO[] trans;
  private RotaElementDTO[] rota;
  // private Role role;
  private int role;
  private Long current_transaction_id;

  public TransGroupInfoDTO() {
  }

  public TransGroupInfoDTO(TransactionGroupIdDTO id,
                      StatusDTO status,
                      RequestView request_view,
                      TransactionInfoDTO[] trans,
                      RotaElementDTO[] rota,
                      Role role,
                      Long current_transaction_id) {
    this.id = id;
    this.status = status;
    this.request_view = request_view;
    this.trans = trans;
    this.rota = rota;
    this.role = role.toIso();
    this.current_transaction_id = current_transaction_id;
  }

  public TransGroupInfoDTO(TransactionGroupIdDTO id,
                      StatusDTO status,
                      RequestView request_view,
                      TransactionInfoDTO[] trans,
                      RotaElementDTO[] rota,
                      int role,
                      Long current_transaction_id) {
    this.id = id;
    this.status = status;
    this.request_view = request_view;
    this.trans = trans;
    this.rota = rota;
    this.role = role;
    this.current_transaction_id = current_transaction_id;
  }


  public TransactionGroupIdDTO getId() {
    return id;
  }

  public StatusDTO getStatus() {
    return status;
  }

  public RequestView getRequestView() {
    return request_view;
  }

  public TransactionInfoDTO[] getTrans() {
    return trans;
  }

  public RotaElementDTO[] getRota() {
    return rota;
  }

  public Role getRole() {
    return Role.fromIso(role);
  }

  public Long getCurrentTransactionId() {
    return current_transaction_id;
  }

}
