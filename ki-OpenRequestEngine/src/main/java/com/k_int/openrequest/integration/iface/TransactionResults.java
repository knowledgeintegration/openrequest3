package com.k_int.openrequest.integration.iface;

public enum TransactionResults {

  CONDITIONAL, RETRY, UNFILLED, LOCATIONS_PROVIDED, WILL_SUPPLY, HOLD_PLACED;

  public String toString() {
    switch ( this ) {
      case CONDITIONAL: return "Conditional";
      case RETRY: return "Retry";
      case UNFILLED: return "Unfilled";
      case LOCATIONS_PROVIDED: return "Locations Provided";
      case WILL_SUPPLY: return "Will Supply";
      case HOLD_PLACED: return "Hold Placed";
      default: return "Unknown";
    }
  }

  public int toIso() {
    switch ( this ) {
      case CONDITIONAL: return 1;
      case RETRY: return 2;
      case UNFILLED: return 3;
      case LOCATIONS_PROVIDED: return 4;
      case WILL_SUPPLY: return 5;
      case HOLD_PLACED: return 6;
      default: return 0;
    }
  }

  public static TransactionResults fromIso(int iso){
    switch ( iso ) {
      case 1: return CONDITIONAL;
      case 2: return RETRY;
      case 3: return UNFILLED;
      case 4: return LOCATIONS_PROVIDED;
      case 5: return WILL_SUPPLY;
      case 6: return HOLD_PLACED;
      default: return null;
    }
  }

}
