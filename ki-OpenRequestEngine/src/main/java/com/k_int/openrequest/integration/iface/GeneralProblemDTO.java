package com.k_int.openrequest.integration.iface;

import java.math.BigInteger;

/**
 * Title:       ProviderErrorReportDTO
 * @version:    $Id: GeneralProblemDTO.java,v 1.2 2005/06/10 16:26:06 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004,2005 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class GeneralProblemDTO extends ProviderErrorReportDTO implements java.io.Serializable {

  public static GeneralProblemDTO UNRECOGNISED_APDU = new GeneralProblemDTO(1);
  public static GeneralProblemDTO MISTYPED_APDU = new GeneralProblemDTO(2);
  public static GeneralProblemDTO BADLY_STRUCTURED_APDU = new GeneralProblemDTO(3);
  public static GeneralProblemDTO PROTOCOL_VERSION_NOT_SUPPORTED = new GeneralProblemDTO(4);
  public static GeneralProblemDTO OTHER = new GeneralProblemDTO(5);

  private long code = 0;

  public GeneralProblemDTO(long code) {
    this.code = code;
  }

  public BigInteger getProblemCode() {
    return BigInteger.valueOf(code);
  }

}
