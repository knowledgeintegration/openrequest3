package com.k_int.openrequest.integration.iface;

import java.util.Date;

/**
 * Title:       RetryAnswerDTO
 * @version:    $Id: RetryAnswerDTO.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class RetryAnswerDTO extends ILLAnswerMessageDTO implements java.io.Serializable
{
  // public ReasonNotAvailable reason_not_available = null;
  public int reason_not_available = -1;
  public Date retry_date;
  public String responder_note;

  public RetryAnswerDTO()
  {
    super();
  }

  public RetryAnswerDTO(int reason_not_available, Date retry_date, String responder_note)
  {
    super();
    this.reason_not_available = reason_not_available;
    this.retry_date = retry_date;
    this.responder_note = responder_note;
  }

  public int getReasonNotAvailable()
  {
    return reason_not_available;
  }

  public int getISOReasonNotAvailable()
  {
    return reason_not_available;
  }

  public Date getRetryDate()
  {
    return retry_date;
  }

  public String getResponderNote()
  {
    return responder_note;
  }

  public String toString() {
    return "RetryAnswerDTO {reason_not_available:"+reason_not_available+",retry_date:"+retry_date+",responder_note:"+responder_note+"}";
  }
}
