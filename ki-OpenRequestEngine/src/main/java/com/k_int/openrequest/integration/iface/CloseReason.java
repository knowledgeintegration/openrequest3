package com.k_int.openrequest.integration.iface;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public enum CloseReason {

  SATISFIED, LOST_SATISFIED, LOST_UNSATISFIED, NEED_BY_EXPIRED, BORROWER_CANCELLED, LIBRARY_PURCHASED, NO_LOCATIONS;

  public String toString() {
    switch ( this ) {
      case SATISFIED: return "Satisfied";
      case LOST_SATISFIED: return "Lost Satisfied";
      case LOST_UNSATISFIED: return "Lost Unsatisfied";
      case NEED_BY_EXPIRED: return "Need By Expired";
      case BORROWER_CANCELLED: return "Borrower Cancelled";
      case LIBRARY_PURCHASED: return "Library Purchased";
      case NO_LOCATIONS: return "No Locations";
      default: return null;
    }
  }

  public static CloseReason fromIso(int iso) {
    switch ( iso ) {
      case 1: return SATISFIED;
      case 2: return LOST_SATISFIED;
      case 3: return LOST_UNSATISFIED;
      case 4: return NEED_BY_EXPIRED;
      case 5: return BORROWER_CANCELLED;
      case 6: return LIBRARY_PURCHASED;
      case 7: return NO_LOCATIONS;
      default: return null;
    }
  }

  public int toIso() {
    switch ( this ) {
      case SATISFIED: return 1;
      case LOST_SATISFIED: return 2;
      case LOST_UNSATISFIED: return 3;
      case NEED_BY_EXPIRED: return 4;
      case BORROWER_CANCELLED: return 5;
      case LIBRARY_PURCHASED: return 6;
      case NO_LOCATIONS: return 7;
      default: return 0;
    }
  }
}
