/**
 * Title:       
 * @version:    $Id: NextResponderRequest.java,v 1.1 2005/04/10 12:42:46 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.workflow;

import com.k_int.openrequest.api.MessageDeliveryObserver;

public class NextResponderRequest {

  public Long tg_id;
  public MessageDeliveryObserver mdo;

  public NextResponderRequest(Long tg_id, MessageDeliveryObserver mdo) {
    this.tg_id = tg_id;
    this.mdo = mdo;
  }

}
