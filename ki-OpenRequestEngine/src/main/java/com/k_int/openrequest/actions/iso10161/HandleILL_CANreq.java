/**
 * Title:       
 * @version:    $Id: HandleILL_CANreq.java,v 1.2 2005/06/12 18:58:13 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.iso10161;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.isoill.InternalIso10161Message;
import com.k_int.openrequest.helpers.*;
import java.util.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

public class HandleILL_CANreq extends HandleGenericRequest
{
  private Log log = LogFactory.getLog(this.getClass());


  public void performAction(String event_code, 
                            Session session, 
                            ILLTransaction transaction)
  {
  }

  public void afterSendMessage(String event_code, Session session, InternalIso10161Message message, ILLTransaction t) {
  }

}
