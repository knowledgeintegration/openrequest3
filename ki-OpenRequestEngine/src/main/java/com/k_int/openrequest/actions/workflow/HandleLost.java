/**
 * Title:       
 * @version:    $Id: HandleLost.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.workflow;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.util.*;
import org.hibernate.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;

public class HandleLost extends AbstractMessageHandler
{
  private Log log = LogFactory.getLog(this.getClass());

  public Object handle(String event, 
                       Object message, 
                       MessageHandlerContext ctx,
                       ApplicationContext app_ctx) throws MessageHandlerException
  {
    log.debug("HandleLost");
    Session session = ctx.getSession();
    MessageDispatcher dispatcher = ctx.getDispatcher();
    
    Long transaction_group_id = (Long)message;

    try
    {
      ILLTransactionGroup tg = DBHelper.lookupGAR(session, transaction_group_id) ;
      tg.setTransactionGroupStatusCode(DBHelper.lookupStatusCode(session,"TRACKING"));
    }
    catch ( java.sql.SQLException sqle )
    {
      log.warn("problem",sqle);
    }
    catch ( HibernateException he )
    {
      log.warn("problem",he);
    }
    return Boolean.TRUE;
  }
}
