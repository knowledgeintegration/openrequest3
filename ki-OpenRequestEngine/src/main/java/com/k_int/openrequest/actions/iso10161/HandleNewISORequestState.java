/**
 * Title:       
 * @version:    $Id: HandleNewISORequestState.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.iso10161;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import java.util.*;
import org.hibernate.*;
import com.k_int.openrequest.util.WorkflowLogger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HandleNewISORequestState
{
  // private static LoggingContext cat = LogContextFactory.getContext("HandleILL_SHIreq");
  private static Log log = LogFactory.getLog(HandleNewISORequestState.class);

  public static void handleNewRequestState(ILLTransaction transaction,
                                    MessageHandlerContext ctx) throws com.k_int.openrequest.api.MessageHandlerException
  {
    try
    {
      MessageDispatcher dispatcher = ctx.getDispatcher();
      StatusCode sc = transaction.getRequestStatusCode();
      if ( transaction.getRole().equals("REQ") )
      {
        if ( ( sc.getCode().equals("NOTSUPPLIED") ) || ( sc.getCode().equals("CANCELLED") ) )
        {
          WorkflowLogger.log(""+transaction.getLocation().getId(),
                             sc.getCode(), 
                             "Sening TG_NEXT event to TG:"+transaction.getTransactionGroup().getId());

          log.debug("** Post system message event TG_NEXT with the internal TG_ID for this group **");

          dispatcher.dispatch("System", 
                              "TG_NEXT", 
                              transaction.getTransactionGroup().getId());
        }
        else if ( sc.getCode().equals("SHIPPED") )
        {
          if ( transaction.getShippedServiceType() == 1 )
          {
            WorkflowLogger.log(""+transaction.getLocation().getId(),
                               sc.getCode(), 
                               "Sending TG_SHIP_RET event to TG:"+transaction.getTransactionGroup().getId());

            dispatcher.dispatch("System", 
                                "TG_SHIP_RET",  // Or TG_SHIPPED_NR / TG_SHIPPED_RET
                                transaction.getTransactionGroup().getId());
          }
          else
          {
            WorkflowLogger.log(""+transaction.getLocation().getId(),
                               sc.getCode(), 
                               "Sending TG_SHIP_NR event to TG:"+transaction.getTransactionGroup().getId());

            dispatcher.dispatch("System", 
                                "TG_SHIP_NR",  // Or TG_SHIPPED_NR / TG_SHIPPED_RET
                                transaction.getTransactionGroup().getId());
          }
        }
        else if ( sc.getCode().equals("RECEIVED") )
        {
          if ( transaction.getShippedServiceType() == 1 )
          {
            WorkflowLogger.log(""+transaction.getLocation().getId(),
                               sc.getCode(), 
                               "Sending TG_SHIP_RET event to TG:"+transaction.getTransactionGroup().getId());

            dispatcher.dispatch("System", 
                                "TG_SHIP_RET",  // Or TG_SHIPPED_NR / TG_SHIPPED_RET
                                transaction.getTransactionGroup().getId());
          }
          else
          {
            WorkflowLogger.log(""+transaction.getLocation().getId(),
                               sc.getCode(), 
                               "Sending TG_SHIP_NR event to TG:"+transaction.getTransactionGroup().getId());

            dispatcher.dispatch("System", 
                                "TG_SHIP_NR",  // Or TG_SHIPPED_NR / TG_SHIPPED_RET
                                transaction.getTransactionGroup().getId());
          }
        }
        else if ( sc.getCode().equals("RECEIVED") )
        {
          WorkflowLogger.log(""+transaction.getLocation().getId(),
                             sc.getCode(), 
                             "Sending TG_SHIP_NR event to TG:"+transaction.getTransactionGroup().getId());

          dispatcher.dispatch("System", 
                              "TG_SHIP_NR",  // Or TG_SHIPPED_NR / TG_SHIPPED_RET
                              transaction.getTransactionGroup().getId());
        }
        else if ( sc.getCode().equals("LOST") )
        {

          WorkflowLogger.log(""+transaction.getLocation().getId(),
                             sc.getCode(), 
                             "Sending TG_LOST event to TG:"+transaction.getTransactionGroup().getId());

          dispatcher.dispatch("System", 
                              "TG_LOST",  // Or TG_SHIPPED_NR / TG_SHIPPED_RET
                              transaction.getTransactionGroup().getId());
        }
        else
          WorkflowLogger.log(""+transaction.getLocation().getId(),
                             sc.getCode(),
                             "No special action");
      }
      else if ( transaction.getRole().equals("RESP") )  // Any special actions for RESPONDER half?
      {
        if ( sc.getCode().equals("SHIPPED") )
        {
          if ( transaction.getShippedServiceType() == 1 )
          {
            WorkflowLogger.log("RESP:"+transaction.getLocation().getId(),
                               sc.getCode(), 
                               "Sending TG_SHIP_RET event to TG:"+transaction.getTransactionGroup().getId());

            dispatcher.dispatch("System", 
                                "TG_SHIP_RET",  // Or TG_SHIPPED_NR / TG_SHIPPED_RET
                                transaction.getTransactionGroup().getId());
          }
          else
          {
            WorkflowLogger.log("RESP:"+transaction.getLocation().getId(),
                               sc.getCode(), 
                               "Sending TG_SHIP_NR event to TG:"+transaction.getTransactionGroup().getId());

            dispatcher.dispatch("System", 
                                "TG_SHIP_NR",  // Or TG_SHIPPED_NR / TG_SHIPPED_RET
                                transaction.getTransactionGroup().getId());
          }
        }
        else if ( sc.getCode().equals("RECEIVED") )
        {
          if ( transaction.getShippedServiceType() == 1 )
          {
            WorkflowLogger.log("RESP:"+transaction.getLocation().getId(),
                               sc.getCode(), 
                               "Sending TG_SHIP_RET event to TG:"+transaction.getTransactionGroup().getId());

            dispatcher.dispatch("System", 
                                "TG_SHIP_RET",  // Or TG_SHIPPED_NR / TG_SHIPPED_RET
                                transaction.getTransactionGroup().getId());
          }
          else
          {
            WorkflowLogger.log("RESP:"+transaction.getLocation().getId(),
                               sc.getCode(), 
                               "Sending TG_SHIP_NR event to TG:"+transaction.getTransactionGroup().getId());

            dispatcher.dispatch("System", 
                                "TG_SHIP_NR",  // Or TG_SHIPPED_NR / TG_SHIPPED_RET
                                transaction.getTransactionGroup().getId());
          }
        }
        else
        {
          WorkflowLogger.log("RESP:"+transaction.getLocation().getId(), sc.getCode(), "No special action");
        }
      }
    }
    catch ( Exception re )
    {
      throw new com.k_int.openrequest.api.MessageHandlerException(re.toString());
    }
  }
}
