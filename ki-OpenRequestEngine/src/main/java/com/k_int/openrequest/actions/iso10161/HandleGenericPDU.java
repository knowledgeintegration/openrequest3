/**
 * Title:       
 * @version:    $Id: HandleGenericPDU.java,v 1.17 2005/07/04 16:24:06 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.iso10161;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.util.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.k_int.openrequest.util.WorkflowLogger;
import org.springframework.context.ApplicationContext;
import java.math.BigInteger;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import org.hibernate.*;

public abstract class HandleGenericPDU extends AbstractMessageHandler
{
  private Log log = LogFactory.getLog(this.getClass());
  private static String SYSTEM_LOCATION = "System";

  private static String[][] msg_names = { { "ILL", "ILLind" },
                                          { "FWD", "FWDind" },
                                          { "ANS-CO", "ANSind-CO" },
                                          { "ANS-RY", "ANSind-RY" },
                                          { "ANS-UN", "ANSind-UN" },
                                          { "ANS-LP", "ANSind-LP" },
                                          { "ANS-WS", "ANSind-WS" },
                                          { "ANS-HP", "ANSind-HP" },
                                          { "ANS-ES", "ANSind-ES" },
                                          { "C-REP+", "C-REPind+" },
                                          { "C-REP-", "C-REPind-" },
                                          { "CAN", "CANind" },
                                          { "CAR+", "CARind+" },
                                          { "CAR-", "CARind-" },
                                          { "SHI", "SHIind" },
                                          { "RCV", "RCVind" },
                                          { "RCL", "RCLind" },
                                          { "DUE", "DUEind" },
                                          { "RET", "RETind" },
                                          { "REN", "RENind" },
                                          { "REA+", "REAind+" },
                                          { "REA-", "REAind-" },
                                          { "CHK", "CHKind" },
                                          { "LST", "LSTind" },
                                          { "DAM", "DAMind" },
                                          { "MSG", "MSGind" },
                                          { "STQ", "STQind" },
                                          { "STR", "STRind" },
                                          { "EXP", "EXPind" } };

  private static Map status_code_enum_values = new HashMap();
  private static Map pdu_type_enum_values = new HashMap();

  static {
    status_code_enum_values.put("NOTSUPPLIED", BigInteger.valueOf(1));
    status_code_enum_values.put("PENDING", BigInteger.valueOf(2));
    status_code_enum_values.put("IN-PROCESS", BigInteger.valueOf(3));
    status_code_enum_values.put("FORWARD", BigInteger.valueOf(4));
    status_code_enum_values.put("CONDITIONAL", BigInteger.valueOf(5));
    status_code_enum_values.put("CANCELPENDING", BigInteger.valueOf(6));
    status_code_enum_values.put("CANCELLED", BigInteger.valueOf(7));
    status_code_enum_values.put("SHIPPED", BigInteger.valueOf(8));
    status_code_enum_values.put("RECEIVED", BigInteger.valueOf(9));
    status_code_enum_values.put("RENEWPENDING", BigInteger.valueOf(10));
    status_code_enum_values.put("NOTREC/OVERDUE", BigInteger.valueOf(11));
    status_code_enum_values.put("RENEWOVERDUE", BigInteger.valueOf(12));
    status_code_enum_values.put("OVERDUE", BigInteger.valueOf(13));
    status_code_enum_values.put("RETURNED", BigInteger.valueOf(14));
    status_code_enum_values.put("CHECKEDIN", BigInteger.valueOf(15));
    status_code_enum_values.put("RECALL", BigInteger.valueOf(16));
    status_code_enum_values.put("LOST", BigInteger.valueOf(17));
    status_code_enum_values.put("UNKNOWN", BigInteger.valueOf(18));

    pdu_type_enum_values.put(new Integer(0), BigInteger.valueOf(1));
    pdu_type_enum_values.put(new Integer(1), BigInteger.valueOf(2));
    pdu_type_enum_values.put(new Integer(2), BigInteger.valueOf(3));
    pdu_type_enum_values.put(new Integer(3), BigInteger.valueOf(4));
    pdu_type_enum_values.put(new Integer(4), BigInteger.valueOf(5));
    pdu_type_enum_values.put(new Integer(5), BigInteger.valueOf(6));
    pdu_type_enum_values.put(new Integer(6), BigInteger.valueOf(7));
    pdu_type_enum_values.put(new Integer(7), BigInteger.valueOf(8));
    pdu_type_enum_values.put(new Integer(8), BigInteger.valueOf(9));
    pdu_type_enum_values.put(new Integer(9), BigInteger.valueOf(10));
    pdu_type_enum_values.put(new Integer(10), BigInteger.valueOf(11));
    pdu_type_enum_values.put(new Integer(11), BigInteger.valueOf(12));
    pdu_type_enum_values.put(new Integer(12), BigInteger.valueOf(13));
    pdu_type_enum_values.put(new Integer(13), BigInteger.valueOf(14));
    pdu_type_enum_values.put(new Integer(14), BigInteger.valueOf(15));
    pdu_type_enum_values.put(new Integer(15), BigInteger.valueOf(16));
    pdu_type_enum_values.put(new Integer(16), BigInteger.valueOf(17));
    pdu_type_enum_values.put(new Integer(17), BigInteger.valueOf(18));
    pdu_type_enum_values.put(new Integer(18), BigInteger.valueOf(19));
    pdu_type_enum_values.put(new Integer(19), BigInteger.valueOf(20));
  }

  public abstract void preStateChange(String old_state,
                                      String event_code,
                                      String new_state,
                                      Session session,
                                      ILLTransaction transaction);

  public abstract void postStateChange(String old_state,
                                       String event_code,
                                       String new_state,
                                       Session session,
                                       ILLTransaction transaction);

  public boolean isValidTransition(ILLTransaction transaction,
                                   Session session,
                                   String event_code) {

    log.debug("Validating state transition - "+transaction.getActiveStateModel().getId()+","+transaction.getRequestStatusCode().getId()+","+event_code);

    try {
      StateTransition st = DBHelper.findTransition(session,
                                                   transaction.getActiveStateModel().getId(),
                                                   transaction.getRequestStatusCode().getId(),
                                                   event_code);

      if ( st != null )
        return true;
      else
       log.warn("Unable to locate a valid state transition. Assuming message is out of sequence.");
    }
    catch ( Exception e ) {
      log.warn("Problem locating transition for "+transaction.toString()+" given event "+event_code);
    }

    return false;
  }

  public void performTransition(ILLTransaction transaction,
                                Session session,
                                String event_code) {
    log.debug("STATE: performTransition(tno:"+transaction.getId()+" event:"+event_code);

    try {
      StateTransition st = DBHelper.findTransition(session,
                                                   transaction.getActiveStateModel().getId(),
                                                   transaction.getRequestStatusCode().getId(),
                                                   event_code);

      log.debug("STATE: Update transaction "+transaction.getId()+" status to "+st.getTo_state().getDescription());

      preStateChange(st.getFrom_state().getCode(), event_code, st.getTo_state().getCode(), session, transaction);

      transaction.setRequestStatusCode(st.getTo_state());

      WorkflowLogger.log(""+transaction.getLocation().getId(),
                    "transaction:"+transaction.getId()+" current state:"+st.getFrom_state().getCode()+" event:"+event_code,
                    "Set transaction status to "+st.getTo_state().getCode());


      postStateChange(st.getFrom_state().getCode(), event_code, st.getTo_state().getCode(), session, transaction);
    }
    catch ( Exception e ) {
      log.warn("Problem updating state",e);
    }
  }

  public void sendLocationIndication(MessageDispatcher dispatcher,
                                     ILLMessageEnvelope apdu_envelope,
                                     ILLTransaction ill_transacton_from_db,
                                     MessageHeader message_as_stored_in_db,
                                     Session session,
                                     String event,
                                     Location this_location,
                                     Long message_id,
                                     Map protocol_stack_context) {
    log.info("sendLocationIndication... "+event);
    try {
      MessageIndicationDetails dets = new MessageIndicationDetails(
                                  ill_transacton_from_db.getTransactionGroup().getId(),
                                  ill_transacton_from_db.getId(),
                                  message_id,
                                  apdu_envelope,
                                  message_as_stored_in_db,
                                  protocol_stack_context);  // Map context

      dispatcher.dispatch("ServiceProvider:"+this_location.getId(), event, dets);

      // Announce the message to the ASE channel
      dispatcher.dispatch("ASE", event, dets);
   
    }
    catch ( Exception e ) {
      log.warn("Problem sending indication",e);
    }
  }


  /**
   * Handle an incoming ISO 10161 indication event.
   *
   *  Preconditions: message parameter can be cast into the class expected by this handler
   *                 (ILL_APDU_type).
   *  Postconditions: If a valid transaction can be located for the request and the request
   *                  indicates a valid transition in that transaction then the request will
   *                  be saved in the database, the state of the transaction will be updated.
   *                  Otherwise a message will be generated and sent to the administrator.
   *
   *  @author Ian Ibbotson
   *  @param message An instance of ILL_APDU_type
   *  @param ctx Context for this handler
   *  @return Boolean object true
   *  @throws MessageHandlerException if an exceptional condition arose
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public Object handle(String event, 
                       Object message, 
                       MessageHandlerContext ctx,
                       ApplicationContext app_ctx) throws MessageHandlerException {
    try {
      Date now = new Date(System.currentTimeMillis());
      Session session = null;
      MessageDispatcher dispatcher = null;
      log.debug("handle(...)");
      session=ctx.getSession();
      dispatcher=ctx.getDispatcher();
      OIDRegister reg = (OIDRegister)app_ctx.getBean("OIDRegister"); // OIDRegister.getRegister();

      InternalIso10161Message msg = (InternalIso10161Message)message;
      ILL_APDU_type pdu = msg.getPDU();
      Object base = pdu.o;

      if ( event == null )
        throw new MessageHandlerException("Unable to determine event for incoming message");
  
      ILLTransaction t = null;
      // LocationSymbol this_location = null;
      Location this_location = null;

      java.lang.reflect.Field transaction_id_field = base.getClass().getDeclaredField("transaction_id");
      Transaction_Id_type transaction_id = (Transaction_Id_type) transaction_id_field.get(base);
      System_Id_type initial_requester_id = transaction_id.initial_requester_id;

      String initial_requester_symbol = null;

      if ( initial_requester_id != null )
        initial_requester_symbol = ILLStringHelper.extract(
          (ILL_String_type)initial_requester_id.person_or_institution_symbol.o);

      String tgq = ILLStringHelper.extract( (ILL_String_type)transaction_id.transaction_group_qualifier);
      String tq = ILLStringHelper.extract( (ILL_String_type)transaction_id.transaction_qualifier);
      String stq = ILLStringHelper.extract( (ILL_String_type)transaction_id.sub_transaction_qualifier);

      // If we explicitly know, for whatever reason (Internal delivery, APDU-Deliv-info etc, who this message
      // is intended for, then just do a lookup within that location, otherwise, hope that the sender and
      // responder are not both in this system
      if ( msg.getToSymbol() == null ) {
        log.info("Unable to explicitly identify recipient for message from APDU-Delivery-Info("+event+"). Looking up from transaction-id instead");

        // So see if the TGQ/TQ identifies a unique transaction. If so, the message must be
        // bound for the recipient (Dodgy logic really, but it works)
        t = DBHelper.lookupTransaction(session, initial_requester_symbol, tgq, tq, stq);

        if ( t != null ) {
          this_location = t.getLocation();
        }

        msg.setToSymbol(this_location.getDefaultSymbol());
      }
      else {
        log.info("Incoming ILL message ("+event+") for location "+ msg.getToSymbol()+" from "+msg.getFromSymbol());

        this_location = DBHelper.lookupLocation(session, msg.getToSymbol()).getCanonical_location();
        log.debug("Calling ISO10161StorageHelper.store to persist message sending loc = "+this_location.getId());

        t = ISO10161StorageHelper.lookupTransaction(session, this_location, pdu.o);
      }

      if ( t == null ) {
        ILL_APDU_type soer = ISOStatusOrErrorReportMessageFactory.createSOERForTransactionIdProblem(pdu, BigInteger.valueOf(3),tgq+":"+tq+":"+stq);
        ILLMessageEnvelope envelope = new ILLMessageEnvelope(soer, msg.getFromSymbol(), msg.getToSymbol(), null);
        dispatcher.dispatch("System","STRreq",envelope);
        return Boolean.TRUE;
        // throw new MessageHandlerException("Unable to determine a transaction for the request");
      }

      // here we need to update current partner APDU delivery info if it was sent to us
      // in the message.
      if ( ( msg.getPartnerTelecomServiceIdentifier() != null ) && ( msg.getPartnerTelecomServiceAddress() != null ) ) {
        t.setPartnerTelecomServiceIdentifier(msg.getPartnerTelecomServiceIdentifier());
        t.setPartnerTelecomServiceAddress(msg.getPartnerTelecomServiceAddress());
      }

      log.debug("Validating message and state transition");
      if ( isValidTransition(t,session,event) ) {
        log.debug("TODO: Calling ISO10161StorageHelper.store to persist message");
        t.incrementUnreadMessageCount();
        t.setLastMessageDate(now);
        t.getTransactionGroup().incrementUnreadMessageCount();
        t.getTransactionGroup().setLastMessageDate(now);

        MessageHeader msg_header = ISO10161StorageHelper.store(pdu, 
                                                               session, 
                                                               t,
                                                               new com.k_int.openrequest.isoill.ILLSystemAddress(msg.getFromSymbol(),null,null),
                                                               new com.k_int.openrequest.isoill.ILLSystemAddress(msg.getToSymbol(),null,null),
                                                               reg);

        msg_header.setRead(false);
        msg_header.setFromState(t.getRequestStatusCode());
        msg_header.setEventCode(event);
        performTransition(t,session,event);
        msg_header.setToState(t.getRequestStatusCode());
        session.flush();
        session.connection().commit();

        String ind_event = null;
        boolean found = false;
        for ( int i=0; ((i<msg_names.length)&&(!found)); i++ ) {
          if ( msg_names[i][0].equals(event) ) {
            ind_event=msg_names[i][1];
            found=true;
          }
        }

        ILLMessageEnvelope em = new ILLMessageEnvelope(pdu,msg.getFromSymbol(),msg.getToSymbol());

        log.debug("Send location indication");
        sendLocationIndication(dispatcher,
                               em,
                               t,
                               msg_header,
                               session,
                               ind_event,
                               this_location,
                               msg_header.getId(),
                               msg.getILLAssociationContext());

        // Perform any state specific actions (E.G. NextRota for NOTSUPPLIED or CANCELLED)
        HandleNewISORequestState.handleNewRequestState(t,ctx);
      }
      else {
        log.error("**** Invalid state transition for request "+t.toString()+" and event "+event+" Sending STATUS_OR_ERROR_REPORT ****");
        // Send SOER
        ILL_APDU_type soer = ISOStatusOrErrorReportMessageFactory.create(session,
                                                                         t,
                                                                         null, // Reason no report
                                                                         null, // Status_Report_type status_report,
                                                                         new Error_Report_type(
                                                                           ILLStringHelper.generalString("CorrelationInfo"),
                                                                           BigInteger.valueOf(2), // 1=user report, 2=provider report
                                                                           null, // user report
                                                                           new Provider_Error_Report_type(
                                                                             Provider_Error_Report_type.state_transition_prohibited_CID,
                                                                             new State_Transition_Prohibited_type(
                                                                               (BigInteger) pdu_type_enum_values.get(new Integer(pdu.which)), // APDU type
                                                                               (BigInteger) status_code_enum_values.get(t.getRequestStatusCode().getCode())
                                                                             )
                                                                           )
                                                                         ),
                                                                         null,
                                                                         null);

        // ILLMessageEnvelope envelope = new ILLMessageEnvelope(soer, msg.getFromSymbol(), msg.getToSymbol(), null);
        ILLMessageEnvelope envelope = new ILLMessageEnvelope(soer, msg.getToSymbol(), msg.getFromSymbol());
        dispatcher.dispatch("System","STRreq",envelope);
      }
    }
    catch ( java.sql.SQLException sqle ) {
      log.warn("Problem processing generic indication", sqle );
      throw new MessageHandlerException(sqle.toString());
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("Problem processing generic indication", he );
      throw new MessageHandlerException(he.toString());
    }
    catch ( com.k_int.openrequest.helpers.HelperException he ) {
      log.warn("Problem processing generic indication", he );
      throw new MessageHandlerException(he.toString());
    }
    catch ( java.lang.NoSuchFieldException nsfe ) {
      nsfe.printStackTrace();
    }
    catch ( java.lang.IllegalAccessException iae ) {
      iae.printStackTrace();
    }

    log.debug("Leaving handle");
    return Boolean.TRUE;
  }
}
