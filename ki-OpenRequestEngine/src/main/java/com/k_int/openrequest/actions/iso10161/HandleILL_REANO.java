/**
 * Title:       
 * @version:    $Id: HandleILL_REANO.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.iso10161;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.util.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

public class HandleILL_REANO extends HandleGenericPDU {

  public void preStateChange(String old_state,
                             String event_code,
                             String new_state,
                             org.hibernate.Session session,
                             ILLTransaction transaction) {
  }

  public void postStateChange(String old_state,
                              String event_code,
                              String new_state,
                              org.hibernate.Session session,
                              ILLTransaction transaction) {
  }
}
