/**
 * Title:       
 * @version:    $Id: HandleNextRotaResponderRequest.java,v 1.5 2005/07/07 17:57:19 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.workflow;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.util.*;
import org.hibernate.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.k_int.openrequest.util.WorkflowLogger;
import org.springframework.context.ApplicationContext;

public class HandleNextRotaResponderRequest extends AbstractMessageHandler
{
  private Log log = LogFactory.getLog(HandleNextRotaResponderRequest.class);

  public Object handle(String event, 
                       Object message, 
                       MessageHandlerContext ctx,
                       ApplicationContext app_ctx) throws MessageHandlerException {
    log.debug("HandleNextRotaResponderRequest");
    Session session = ctx.getSession();
    MessageDispatcher dispatcher = ctx.getDispatcher();
    
    Long transaction_group_id = null;
    MessageDeliveryObserver mdo = null;

    if ( message instanceof Long ) {
      transaction_group_id = (Long)message;
    }
    else if ( message instanceof NextResponderRequest ) {
      transaction_group_id = ((NextResponderRequest)message).tg_id;
      mdo = ((NextResponderRequest)message).mdo;
    }
    else
      throw new MessageHandlerException("Unhandled type ("+message.getClass().getName()+") for HandleNextRotaResponderRequest");

    try {
      // Step 1 : Retrieve the transaction group
      ILLTransactionGroup tg = DBHelper.lookupGAR(session, transaction_group_id) ;

      // Step 2 : Check we are in an OK state to move to the next rota position
      // No special validation here at the moment......

      // Step 3 : Construct and ILL response message using the appropriate helper
      ILLMessageEnvelope msg = ISORequestMessageFactory.nextRequestInRota(session, tg, mdo, app_ctx);

      // Step 4 : Use the dispatcher to send the message.
      if ( msg != null ) {
        WorkflowLogger.log("TG:"+transaction_group_id,
                         event,
                         "HandleNextRotaResponderRequest sending request: "+msg);

        log.debug("Send new request message to next responder in rota");
        dispatcher.dispatch("System","ILLreq",msg);
        log.debug("Request message sent");
      }
      else {
        WorkflowLogger.log("TG:"+transaction_group_id,
                         event,
                         "No message envelope to send.. ERROR");

        // Maybe we should set the rota status to "PAUSED"
        log.fatal("No message envelope to send.. Perhaps end of rota?");
      }

    }
    catch ( java.sql.SQLException sqle ) {
      log.warn("problem",sqle);
      mdo.notifyError(1);
      throw new MessageHandlerException("unable to create new transaction for group",sqle);
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("problem",he);
      mdo.notifyError(1);
      throw new MessageHandlerException("unable to create new transaction for group",he);
    }
    catch ( Exception re ) {
      log.warn("problem",re);
      mdo.notifyError(1);
      throw new MessageHandlerException("unable to create new transaction for group",re);
    }
    return Boolean.TRUE;
  }
}
