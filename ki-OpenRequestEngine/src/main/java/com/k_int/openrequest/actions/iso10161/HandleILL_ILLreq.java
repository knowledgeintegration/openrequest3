/**
 * Title:       
 * @version:    $Id: HandleILL_ILLreq.java,v 1.11 2005/07/02 22:16:52 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.iso10161;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.isoill.InternalIso10161Message;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.util.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.springframework.context.ApplicationContext;
import org.apache.commons.logging.*;
import org.hibernate.Session;

public class HandleILL_ILLreq extends AbstractMessageHandler
{
  private static Log log = LogFactory.getLog(HandleILL_ILLreq.class);
  private ApplicationContext app_ctx = null;

  public Object handle(String event, 
                       Object message, 
                       MessageHandlerContext ctx,
                       ApplicationContext app_ctx) throws MessageHandlerException
  { 
    this.app_ctx = app_ctx;
    ILLMessageEnvelope em = (ILLMessageEnvelope)message;
    log.debug(em);
    storeAndSend(em.getPDU(), em.getSendingLocationSymbol(), em.getDestinationLocationSymbol(), ctx, em.getMessageDeliveryObserver());

    return Boolean.TRUE;
  }

  public void storeAndSend(ILL_APDU_type pdu,
                           String source,
                           String destination,
                           MessageHandlerContext ctx,
                           MessageDeliveryObserver mdo)  throws MessageHandlerException
  {
    log.debug("handle..... storeAndSend");
    Session session = ctx.getSession();

    ILL_Request_type request = ( ILL_Request_type ) pdu.o;
    System_Id_type responder_id = request.responder_id;
    System_Id_type requester_id = request.requester_id;
    System_Id_type initial_requester_id = request.transaction_id.initial_requester_id;

    String responder_symbol = null;
    String requester_symbol = null;
    String initial_requester_symbol = null;

    responder_symbol = destination;
    requester_symbol = source;

    ILLTransactionGroup the_tg = null;

    if ( initial_requester_id != null ) {
      initial_requester_symbol = ILLStringHelper.extract( 
        (ILL_String_type)initial_requester_id.person_or_institution_symbol.o);
    }
    else {
      log.warn("Unable to extract initial requester symbol");
      initial_requester_symbol = requester_symbol;
    }

    try {
      log.debug("Looking up requester "+requester_symbol);
      LocationSymbol requester_symbol_info = DBHelper.lookupLocation(session,"internal",requester_symbol);
      Location requester_loc = requester_symbol_info.getCanonical_location();

      // Create a header for the request.
      log.debug("Creating a new ILL Request to track this request object");
      com.k_int.openrequest.db.ILLTransaction new_request_info = new com.k_int.openrequest.db.ILLTransaction();

      new_request_info.setRole("REQ");

      new_request_info.setLocation(requester_loc);

      if ( request.transaction_type != null )
        new_request_info.setTransactionType(request.transaction_type.longValue());

      String tgq = ILLStringHelper.extract(
                    (ILL_String_type)request.transaction_id.transaction_group_qualifier);

      new_request_info.setProtocol("ISO10161v"+request.protocol_version_num);

      if ( ( tgq != null ) && ( ! tgq.equals("") ) )
      {
        new_request_info.setTGQ(tgq);
        the_tg = DBHelper.createOrFindExistingTG(requester_loc, 
                                                 requester_symbol,
                                                 initial_requester_symbol,
                                                 tgq,
                                                 session,
                                                 "REQ");

        new_request_info.setTransactionGroup(the_tg);
      }

      new_request_info.setTQ(ILLStringHelper.extract(
                    (ILL_String_type)request.transaction_id.transaction_qualifier));

      new_request_info.setSTQ(ILLStringHelper.extract(
                    (ILL_String_type)request.transaction_id.sub_transaction_qualifier));

      new_request_info.setRequesterSymbol(requester_symbol);
      new_request_info.setResponderSymbol(responder_symbol);
      new_request_info.setInitialRequesterSymbol(initial_requester_symbol);

      // Extract original service data and time from header... Only done for repeat messages.
      if ( request.service_date_time != null )
      {
        if ( request.service_date_time.date_time_of_original_service != null )
        {
          new_request_info.setOriginalServiceDate(
                    request.service_date_time.date_time_of_original_service.date);
          new_request_info.setOriginalServiceTime(
                    request.service_date_time.date_time_of_original_service.time);
        }
      }

      Long new_trans_id = (Long) session.save(new_request_info);


      // Update the latest transaction link in the group to point to this new request.
      the_tg.setLastTransaction(new_request_info);
      
      // Convert the request message into database tables
      log.debug("Storing request message, responder symbol is "+responder_symbol);
      ISO10161RequestMessageDetails rm = ISO10161StorageHelper.storeRequest((ILL_Request_type) pdu.o,
                                                                            session,
                                                                            new_request_info);
      rm.setRead(true);
 
      // Set up any possible protocol variables for the new request header
      new_request_info.setPVReturn(false);
      new_request_info.setPVFwd(rm.getPermToForward());
      new_request_info.setPVChain(rm.getPermToChain());
      new_request_info.setPVPart(rm.getPermToPartition());
      new_request_info.setPVSequenceTimeStamp(new Date(System.currentTimeMillis()));
      new_request_info.setPVCurrentPartnerIdSymbol(responder_symbol);

      new_request_info.setActiveStateModel(DBHelper.lookupStateModel(session,"REQ/PRO"));
      new_request_info.setRequestStatusCode(DBHelper.lookupStatusCode(session,"PENDING"));
      rm.setFromState(DBHelper.lookupStatusCode(session,"IDLE"));
      rm.setEventCode("ILLreq");
      rm.setToState(DBHelper.lookupStatusCode(session,"PENDING"));

      log.debug("FromState="+rm.getFromState());
      log.debug("EventCode="+rm.getEventCode());
      log.debug("ToState="+rm.getToState());

      Date now = new Date(System.currentTimeMillis());
      new_request_info.setLastMessageDate(now);
      new_request_info.getTransactionGroup().setLastMessageDate(now);

      // Reference initial request message in request header.
      new_request_info.setInitialRequestMessage(rm);
      new_request_info.setLastRepeatableMessage(rm);

      log.debug("Sending message to responder");
      LocationSymbol responder_symbol_info = DBHelper.lookupLocation(session,"internal",responder_symbol);

      Location loc = responder_symbol_info.getCanonical_location();
      System.err.println("Sending message to "+loc);
      Service service_to_use = (Service) responder_symbol_info.getServiceForThisSymbol();
      String adapter_bean_name = service_to_use.getHandlerServiceName();
      log.debug("The service we will use is "+service_to_use.getHandlerServiceName());
      log.debug("Service props are "+service_to_use.getProperties());

      new_request_info.setPartnerTelecomServiceIdentifier(service_to_use.getTelecomServiceIdentifier());
      new_request_info.setPartnerTelecomServiceAddress(service_to_use.getTelecomServiceAddress());

      // It's important to make sure that we write and commit the outgoing message before
      // actually sending that message! This is because in the "Hub" model. the only way to
      // tell that a message was bound for the requester/responder might be to look who has
      // already sent a message. Hopefully, we will normally be able to find a parent request
      // just by looking for transactions where initail_requester. tgq, tq and stq are all ==
       
      // session.update(rm);
      // session.update(the_tg);
      // session.update(new_request_info);

      session.flush();
      session.connection().commit();

      // Really send the message!
      if ( responder_symbol_info == null )
        throw new MessageHandlerException("Unable to look up responder symbol");

      try {
        ILLProtocolAdapter adapter = (ILLProtocolAdapter) app_ctx.getBean(adapter_bean_name);

        if ( adapter == null ) {
          throw new MessageHandlerException("Unable to locate ill protocol adapter with bean name "+adapter_bean_name+" in app ctx.");
        }

        InternalIso10161Message msg = new InternalIso10161Message(pdu,
                                                              service_to_use.getTelecomServiceIdentifier(),
                                                              service_to_use.getTelecomServiceAddress(),
                                                              service_to_use.getProperties(),
                                                              System.currentTimeMillis(),
                                                              source,
                                                              destination);

        adapter.sendProtocolMessage(msg);

        if ( mdo != null ) {
          log.debug("Notify any waiting threads that the new ILL Transaction ID is "+the_tg.getId());
          mdo.notify(the_tg.getId(),new_trans_id,"ILLreq",null);
        }
      }
      catch ( Exception e ) {
        // log.warn("Problem: ",e);
        throw new MessageHandlerException(e.toString(), e);
      }
    }
    catch ( java.sql.SQLException sqle ) {
      // log.warn("SQL Exception",sqle);
      throw new MessageHandlerException(sqle.toString(), sqle);
    }
    catch ( org.hibernate.HibernateException he ) {
      // log.warn("Hibernate Exception",he);
      throw new MessageHandlerException(he.toString(), he);
    }
    catch ( Exception e ) {
      // log.warn("Other Exception",e);
      throw new MessageHandlerException(e.toString(), e);
    }
  }
}
