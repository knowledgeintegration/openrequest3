/**
 * Title:       
 * @version:    $Id: HandleIncomingProtocolMessage.java,v 1.13 2005/07/04 14:38:14 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.iso10161;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import java.util.*;
import org.hibernate.*;
import com.k_int.openrequest.isoill.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.ILL_APDU_Delivery_Info.*;
import org.jzkit.z3950.gen.v3.AccessControlFormat_prompt_1.*;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import java.math.BigInteger;
import org.springframework.context.ApplicationContext;
import org.apache.commons.logging.*;

public class HandleIncomingProtocolMessage extends AbstractMessageHandler
{
  private static Log cat = LogFactory.getLog(HandleIncomingProtocolMessage.class);
  private static String SYSTEM_LOCATION = "System";


  /**
   * Handle an incoming ISO 10161 message.
   *
   * This handler is responsible for a first pass analysis of a protocol message
   * received from outside the system. It must look at the TGQ, TQ and STQ and 
   * attempt to discover which transaction this request actually relates to.
   * it is then responsible for invoking the incoming pdu distribution handler which
   * will make sure the right event gets called.
   *
   * If the PDU carries APDU Delivery Info, that information can be used to discern sender and recipient
   *
   *  Preconditions: 
   *  Postconditions: 
   *
   *  @author Ian Ibbotson
   *  @param message An instance of InternalIso10161Message
   *  @param ctx Context for this handler
   *  @return Boolean object true
   *  @throws MessageHandlerException if an exceptional condition arose
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public Object handle(String event, 
                       Object message, 
                       MessageHandlerContext ctx,
                       ApplicationContext app_ctx) throws MessageHandlerException
  {
    Session session = ctx.getSession();
    MessageDispatcher dispatcher = null;
    cat.debug("HandleIncomingProtocolMessage::handle("+event+",msg,ctx)");
    dispatcher=ctx.getDispatcher();
    OIDRegister reg = (OIDRegister)app_ctx.getBean("OIDRegister"); // OIDRegister.getRegister();

    InternalIso10161Message iso_message = (InternalIso10161Message)message;

    try
    {
      // dispatcher.handle("internal:IncomingValidPDU",iso_message);
      cat.debug("Try and extract recipient data...");

      extractRecipient(iso_message, session, reg, iso_message.getILLAssociationContext(), app_ctx);
      dispatcher.dispatch(SYSTEM_LOCATION, "internal:IncomingValidPDU", iso_message);
    }
    catch ( Exception re )
    {
      throw new MessageHandlerException(re.toString(), re);
    }

    return Boolean.TRUE;
  }

  private void extractRecipient(InternalIso10161Message iso_message,
                                Session session,
                                OIDRegister reg,
                                Map ill_assoc_context,
                                ApplicationContext app_ctx) throws com.k_int.openrequest.api.MessageHandlerException {

    cat.debug("Attempting to extract sender and recipient information from message which="+iso_message.getPDU().which);

    // Step 1: See if the PDU contains an APDUDeliveryInfo external
    ArrayList extensions = null;
    switch ( iso_message.getPDU().which )
    {
      case ILL_APDU_type.ill_request_var_CID:
        extensions = ((ILL_Request_type)iso_message.getPDU().o).iLL_request_extensions;
        break;
      case ILL_APDU_type.forward_notification_var_CID:
        extensions = ((Forward_Notification_type)iso_message.getPDU().o).forward_notification_extensions;
        break;
      case ILL_APDU_type.shipped_var_CID:
        extensions = ((Shipped_type)iso_message.getPDU().o).shipped_extensions;
        break;
      case ILL_APDU_type.ill_answer_var_CID:
        extensions = ((ILL_Answer_type)iso_message.getPDU().o).ill_answer_extensions;
        break;
      case ILL_APDU_type.conditional_reply_var_CID:
        extensions = ((Conditional_Reply_type)iso_message.getPDU().o).conditional_reply_extensions;
        break;
      case ILL_APDU_type.cancel_var_CID:
        extensions = ((Cancel_type)iso_message.getPDU().o).cancel_extensions;
        break;
      case ILL_APDU_type.cancel_reply_var_CID:
        extensions = ((Cancel_Reply_type)iso_message.getPDU().o).cancel_reply_extensions;
        break;
      case ILL_APDU_type.received_var_CID:
        extensions = ((Received_type)iso_message.getPDU().o).received_extensions;
        break;
      case ILL_APDU_type.recall_var_CID:
        extensions = ((Recall_type)iso_message.getPDU().o).recall_extensions;
        break;
      case ILL_APDU_type.returned_var_CID:
        extensions = ((Returned_type)iso_message.getPDU().o).returned_extensions;
        break;
      case ILL_APDU_type.checked_in_var_CID:
        extensions = ((Checked_In_type)iso_message.getPDU().o).checked_in_extensions;
        break;
      case ILL_APDU_type.overdue_var_CID:
        extensions = ((Overdue_type)iso_message.getPDU().o).overdue_extensions;
        break;
      case ILL_APDU_type.renew_var_CID:
        extensions = ((Renew_type)iso_message.getPDU().o).renew_extensions;
        break;
      case ILL_APDU_type.renew_answer_var_CID:
        extensions = ((Renew_Answer_type)iso_message.getPDU().o).renew_answer_extensions;
        break;
      case ILL_APDU_type.lost_var_CID:
        extensions = ((Lost_type)iso_message.getPDU().o).lost_extensions;
        break;
      case ILL_APDU_type.damaged_var_CID:
        extensions = ((Damaged_type)iso_message.getPDU().o).damaged_extensions;
        break;
      case ILL_APDU_type.message_var_CID:
        extensions = ((Message_type)iso_message.getPDU().o).message_extensions;
        break;
      case ILL_APDU_type.status_query_var_CID:
        extensions = ((Status_Query_type)iso_message.getPDU().o).status_query_extensions;
        break;
      case ILL_APDU_type.status_or_error_report_var_CID:
        extensions = ((Status_Or_Error_Report_type)iso_message.getPDU().o).status_or_error_report_extensions;
        break;
      case ILL_APDU_type.expired_var_CID:
        extensions = ((Expired_type)iso_message.getPDU().o).expired_extensions;
        break;
    }

    cat.debug("Checking extensions.....");
    if ( extensions != null ) {
      cat.debug("Looking for APDUDeliveryInfo extension");
      for ( Iterator i = extensions.iterator(); i.hasNext(); ) {
        cat.debug("Checking extension......");
        Extension_type et = (Extension_type)i.next();

        // we have et.identifier, et.critical, et.item (which is the any containing an External
        if ( ( et.identifier != null ) && ( et.identifier.equals(BigInteger.valueOf(1))) ) {

          if ( et.item != null ) { 

            cat.debug("Got EXTERNAL_type, class is "+et.item.getClass().getName());

            EXTERNAL_type ext = (EXTERNAL_type) et.item;
            int[] oid = ext.direct_reference;
            if ( oid != null ) {
              OIDRegisterEntry ent = reg.lookupByOID(oid);
              if ( ent != null ) {
                if ( ent.getName().equals("ILL_APDU_Delivery_Info") ) { 

                  if ( ( ext.encoding != null ) && ( ext.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {
                    cat.debug("Processing APDU Delivery Info");

                    if ( ext.encoding.o instanceof APDU_Delivery_Info_type ) {
                      APDU_Delivery_Info_type delivery_info = (APDU_Delivery_Info_type) ext.encoding.o;

                      ILLSystemAddress sender_addr = extractSystemAddress(delivery_info.sender_info, app_ctx);
                      ILLSystemAddress recipient_addr = extractSystemAddress(delivery_info.recipient_info, app_ctx);

                      iso_message.setFromSymbol(sender_addr.getSymbol());
                      iso_message.setToSymbol(recipient_addr.getSymbol());
                      iso_message.setPartnerTelecomServiceIdentifier(sender_addr.getTelecomServiceIdentifier());
                      iso_message.setPartnerTelecomServiceAddress(sender_addr.getTelecomServiceAddress());
                    }
                    else {
                      throw new MessageHandlerException("delivery_info member of the message was not of correct class. This probably indicates a problem with the configuration of external codecs in the a2j.properties file, or the ISOILL External Codec is wrong. Instance of "+ext.encoding.o.getClass().getName());
                    }
                    // Vector transponder_info = delivery_info.transponder_info;
                  }
                }
                else if ( ent.getName().equals("AccessControlFormat-prompt-1") ) { 
                  cat.debug("PDU Contains access control format prompt-1");
                  if ( ( ext.encoding != null ) &&
                       ( ext.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {
                    cat.debug("Processing Access Control Info");
                    PromptObject_type access_control = (PromptObject_type) ext.encoding.o;
                    
                    List info = (List)access_control.o;
                    for ( Iterator access_control_iterator = info.iterator(); access_control_iterator.hasNext(); ) {
                      ResponseItem161_type response = (ResponseItem161_type)access_control_iterator.next();
                      if ( response.promptId.which == PromptId_type.enummeratedprompt_CID ) {
                        enummeratedPrompt_inline163_type ep = (enummeratedPrompt_inline163_type) response.promptId.o;
                        if ( ep != null ) {
                          if ( response.promptResponse != null ) {
                            cat.info("response type "+ep.type+" = "+response.promptResponse.o);
                            ill_assoc_context.put("AUTH"+ep.type,response.promptResponse.o);
                          }
                          else {
                            cat.warn("Access Control prompt response was null");
                          }
                        }
                        else {
                          cat.warn("Access Control enumerated prompt was null");
                        }
                      }
                      else {
                        cat.info("response type "+response.promptId.o+" = "+response.promptResponse.o);
                        ill_assoc_context.put("AUTH"+response.promptId.o,response.promptResponse.o);
                      }
                    }
                  }
                }
                else {
                  cat.debug("APDU Contains recognised,  but unprocessed (by HandleIncomingProtocolMessage), external : "+ent.getName());
                  cat.debug("A subsequent handler in the chain of responsibility will deal with this external");
                }
              }
            }
            cat.debug("Got external:" +ext.direct_reference);
          }
        }
      }
      cat.debug("Done...");
    }
    else {
      cat.warn("No extensions present in APDU");
    }
  }


  public ILLSystemAddress extractSystemAddress(ArrayList possible_addresses, ApplicationContext app_ctx) {

    String symbol = null;
    String telecom_service_identifier = null;
    String telecom_service_address = null;

    boolean supports_tcp = app_ctx.containsBeanDefinition("DUPLEXTCP");
    boolean supports_mime = app_ctx.containsBeanDefinition("MIME");

    List valid_alternatives = new ArrayList();

    // Iterate through each possible encoding (Currently, eDIFACT, bER, bER-IN-MIME) this vector is in
    // preference order.
    for ( Iterator i = possible_addresses.iterator(); i.hasNext(); ) {
      symbol = null;
      cat.debug("Processing a possible encoding...");
      APDU_Delivery_Parameters_type params = (APDU_Delivery_Parameters_type) i.next();
      // Each possible encoding then has a list of potential system-id'sA
      // OpenRequest is happy with just about enything except edifact atm.
      //
      ArrayList encodings_allowed_at_this_address = params.encoding;
      if ( ( encodings_allowed_at_this_address.contains( BigInteger.valueOf(2) ) && supports_mime ) ||
           ( encodings_allowed_at_this_address.contains( BigInteger.valueOf(3) ) && supports_tcp ) )  { // 1 = eDIFACT! 

        cat.debug("Processing an address...");

        // Cool.. this location accepts bER or bER-IN-MIME :)
        System_Address_type transport = params.transport;
        telecom_service_identifier = ILLStringHelper.extract(transport.telecom_service_identifier);
        telecom_service_address = ILLStringHelper.extract(transport.telecom_service_address);
        
        ArrayList aliases = params.aliases;

        if ( aliases != null ) {
          for ( Iterator alias_iterator = aliases.iterator(); alias_iterator.hasNext(); ) {
            cat.debug("Processing alias...");
            System_Id_type system_id = (System_Id_type) alias_iterator.next();
            Person_Or_Institution_Symbol_type person_or_institution_symbol = system_id.person_or_institution_symbol;
            Name_Of_Person_Or_Institution_type name_of_person_or_institution = system_id.name_of_person_or_institution;
  
            if ( ( person_or_institution_symbol != null ) && 
                 ( person_or_institution_symbol.which == Person_Or_Institution_Symbol_type.institution_symbol_CID ) ) {
              if ( symbol == null )
                symbol = ILLStringHelper.extract((ILL_String_type)(person_or_institution_symbol.o));
  
              cat.debug("Done processing alias: "+ILLStringHelper.extract((ILL_String_type)(person_or_institution_symbol.o)));
            }
          }
        }
        else {
          cat.debug("No alaises");
        }

        cat.info("Processing a possible APDU Delivery option "+symbol+" "+telecom_service_identifier+" "+telecom_service_address);
        valid_alternatives.add(new ILLSystemAddress(symbol, telecom_service_identifier, telecom_service_address));
      }
      else {
        cat.debug("APDU Delivery Info was rejected because it's not supported by this configuration");
      }
    }

    cat.debug("extractSymbol returns "+symbol);

    if ( valid_alternatives.size() > 0 )
      return (ILLSystemAddress) valid_alternatives.get(0);
   
    cat.warn("No valid alternatives for APDU Delivery info");

    return null;
  }
}




