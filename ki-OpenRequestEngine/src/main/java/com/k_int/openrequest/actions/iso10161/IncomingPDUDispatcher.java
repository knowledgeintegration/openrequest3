/**
 * Title:       
 * @version:    $Id: IncomingPDUDispatcher.java,v 1.3 2005/06/11 16:27:04 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.iso10161;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import java.util.*;
import org.hibernate.*;
import com.k_int.openrequest.isoill.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.springframework.context.ApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class IncomingPDUDispatcher extends AbstractMessageHandler
{
  public static Log log = LogFactory.getLog(IncomingPDUDispatcher.class);


  /**
   * Dispatch an incoming ISO 10161 message to an appropriate event handler.
   *
   * make sure the right event gets called.
   *
   *  Preconditions: 
   *  Postconditions: 
   *
   *  @author Ian Ibbotson
   *  @param message An instance of SendProtocolMessageRequest
   *  @param ctx Context for this handler
   *  @return Boolean object true
   *  @throws MessageHandlerException if an exceptional condition arose
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public Object handle(String event, 
                       Object message, 
                       MessageHandlerContext ctx,
                       ApplicationContext app_ctx) throws MessageHandlerException
  {
    log.debug("Passing incoming ISO 10161 message on to message specific handler");

    Session session = null;
    MessageDispatcher dispatcher = null;
    session=ctx.getSession();
    dispatcher=ctx.getDispatcher();

    InternalIso10161Message req = (InternalIso10161Message)message;
    ILL_APDU_type pdu = req.getPDU();
    String destination = req.getToSymbol();
    String source = req.getFromSymbol();
    String new_event = null;


    switch ( pdu.which )
    {
      case ILL_APDU_type.ill_request_var_CID :
      {
        new_event= "ILL";
        break;
      }
      case ILL_APDU_type.forward_notification_var_CID :
        new_event= "FWD";
        break;
      case ILL_APDU_type.shipped_var_CID :
        new_event= "SHI";
        break;
      case ILL_APDU_type.ill_answer_var_CID :
        ILL_Answer_type ans = (ILL_Answer_type) pdu.o;

        switch ( ans.transaction_results.intValue() )
        {
          case 1:
            new_event= "ANS-CO";
            break;
          case 2:
            new_event= "ANS-RY";
            break;
          case 3:
            new_event= "ANS-UN";
            break;
          case 4:
            new_event= "ANS-LP";
            break;
          case 5:
            new_event= "ANS-WS";
            break;
          case 6:
            new_event= "ANS-HP";
            break;
          case 7:
            new_event= "ANS-ES";
            break;
          default:
            new_event= "ERROR";
            break;
        }
        break;
      case ILL_APDU_type.conditional_reply_var_CID :
        Conditional_Reply_type crep = (Conditional_Reply_type)pdu.o;
        if ( crep.answer.booleanValue() )
          new_event= "C-REP+";
        else
          new_event= "C-REP-";
        break;
      case ILL_APDU_type.cancel_var_CID :
        new_event= "CAN";
        break;
      case ILL_APDU_type.cancel_reply_var_CID :
        Cancel_Reply_type canrep = (Cancel_Reply_type)pdu.o;
        if ( canrep.answer.booleanValue() )
          new_event= "CAR+";
        else
          new_event= "CAR-";
        break;
      case ILL_APDU_type.received_var_CID :
        new_event= "RCV";
        break;
      case ILL_APDU_type.recall_var_CID :
        new_event= "RCL";
        break;
      case ILL_APDU_type.returned_var_CID :
        new_event= "RET";
        break;
      case ILL_APDU_type.checked_in_var_CID :
        new_event= "CHK";
        break;
      case ILL_APDU_type.overdue_var_CID :
        new_event= "DUE";
        break;
      case ILL_APDU_type.renew_var_CID :
        new_event= "REN";
        break;
      case ILL_APDU_type.renew_answer_var_CID :
        Renew_Answer_type ra = (Renew_Answer_type) pdu.o;
        if ( ra.answer.booleanValue() )
          new_event= "REA+";
        else
          new_event= "REA-";
        break;
      case ILL_APDU_type.lost_var_CID :
        new_event= "LST";
        break;
      case ILL_APDU_type.damaged_var_CID :
        new_event= "DAM";
        break;
      case ILL_APDU_type.message_var_CID :
        new_event= "MSG";
        break;
      case ILL_APDU_type.status_query_var_CID :
        new_event= "STQ";
        break;
      case ILL_APDU_type.status_or_error_report_var_CID :
        new_event= "STR";
        break;
      case ILL_APDU_type.expired_var_CID :
        new_event= "EXP";
        break;
      default:
        new_event="Error";
        break;
    }
  
    log.info("Incoming ISO ILL ADU ("+new_event+") for "+destination+" from "+source);

    try
    {
      // dispatcher.handle(new_event, req);
      // ToDo: One day I guess event code needs to be qualified by protocol or something
      dispatcher.dispatch("System", new_event, req);
    }
    catch ( Exception re )
    {
      throw new MessageHandlerException(re.toString());
    }

    return Boolean.TRUE;
  }
}
