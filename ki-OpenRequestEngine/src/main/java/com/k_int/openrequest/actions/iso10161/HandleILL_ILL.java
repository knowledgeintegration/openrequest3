/**
 * Title:       
 * @version:    $Id: HandleILL_ILL.java,v 1.24 2005/07/06 08:34:01 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.iso10161;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.util.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.Internal_Reference_Number.*;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import com.k_int.openrequest.isoill.gen.ILL_APDU_Delivery_Info.*;
import java.math.BigInteger;
import org.springframework.context.ApplicationContext;
import org.apache.commons.logging.*;
import org.hibernate.Session;

public class HandleILL_ILL extends AbstractMessageHandler
{
  private static String SYSTEM_LOCATION = "System";
  private static Log log = LogFactory.getLog(HandleILL_ILL.class);

  /**
   *  Send an indication of this incoming message to any interested party.
   *  @param dispatcher The Message Dispatcher
   *  @param em The ILL Message Envelope.. contains the decoded APDU
   *  @param t - The internal transaction object.. lots of info about the transaction
   *  @param rm - The message that has been stored in the database (MessageHeader should really be AbstractILLMessage)
   *  @param session - the active database (Hibernate) session
   *  @param event - The ILL Event code for the indication
   *  @param this_location - The location the indication is bound for
   *  @param message_id - The ID assigned to the message.
   *  @return void
   */
  public void sendLocationIndication(MessageDispatcher dispatcher,
                                     ILLMessageEnvelope apdu_envelope,
                                     ILLTransaction ill_transacton_from_db,
                                     MessageHeader message_as_stored_in_db,
                                     Session session,
                                     String event,
                                     Location this_location,
                                     Long message_id,
                                     Map protocol_stack_context)
  {
    log.debug("sendLocationIndication... "+event+" env="+apdu_envelope);
 
    try
    {
      MessageIndicationDetails dets = new MessageIndicationDetails(
                                  ill_transacton_from_db.getTransactionGroup().getId(),
                                  ill_transacton_from_db.getId(),
                                  message_id,
                                  apdu_envelope,
                                  message_as_stored_in_db,
                                  protocol_stack_context);  // Map context

      // dispatcher.handle(this_location.getType().getCode(),event, em);
      // Dispatch a message to the recipientinationLocationSymbol(): service provider notifying them of the message.
      dispatcher.dispatch("ServiceProvider:"+this_location.getId(), event, dets);

      log.debug("Dispatch event to ASE: "+event);

      // Announce the message to the ASE channel
      dispatcher.dispatch("ASE", event, dets);
    }
    catch ( Exception e )
    {
      log.warn("Problem sending indication",e);
    }
  }

  /** Handle an incoming ISO 10161 Request message, trapping the ILLREQUEST.indication event.
   *
   */
  public Object handle(String event, 
                       Object message, 
                       MessageHandlerContext ctx,
                       ApplicationContext app_ctx)
  {
    log.debug("Handle ILL");
    Session session=ctx.getSession();
    MessageDispatcher dispatcher=ctx.getDispatcher();
    Date now = new Date(System.currentTimeMillis());
    OIDRegister reg = (OIDRegister) app_ctx.getBean("OIDRegister");

    InternalIso10161Message msg = (InternalIso10161Message)message;
    log.debug("Incoming ILL Request for location "+msg.getToSymbol()+" from "+msg.getFromSymbol());

    ILL_APDU_type pdu = msg.getPDU();
    ILL_Request_type request = ( ILL_Request_type ) pdu.o;
    System_Id_type responder_id = request.responder_id;
    System_Id_type requester_id = request.requester_id;
    System_Id_type initial_requester_id = request.transaction_id.initial_requester_id;

    String responder_symbol = null;
    String requester_symbol = null;
    String initial_requester_symbol = null;

    // Figure out who the request was actually intended for.
    if ( responder_id.person_or_institution_symbol != null )
      responder_symbol = ILLStringHelper.extract((ILL_String_type) responder_id.person_or_institution_symbol.o);

    if ( requester_id.person_or_institution_symbol != null )
      requester_symbol = ILLStringHelper.extract((ILL_String_type) requester_id.person_or_institution_symbol.o);

    if ( initial_requester_id != null )
      initial_requester_symbol = ILLStringHelper.extract(
        (ILL_String_type)initial_requester_id.person_or_institution_symbol.o);
    else
      initial_requester_symbol = requester_symbol;

    try {
      /* TODO: If requester symbol is unknown, see if the request extension contains an 
       ILL APDU Delivery Info extension and automatically register the symbol (and namespace) */
      checkSender(requester_symbol, 
                  null,  // Requester name
                  (APDU_Delivery_Info_type)com.k_int.openrequest.helpers.ISO10161v2ExternalHelper.extractNamedExternal(pdu,"ILL_APDU_Delivery_Info",reg),
                  session,
                  app_ctx);

      boolean intermediary = false;
      log.debug("main: Looking up responder : "+responder_symbol);
      LocationSymbol responder_symbol_info = DBHelper.lookupLocation(session,"internal",responder_symbol);

      if ( responder_symbol_info == null ) {
        throw new OpenRequestException("Unable to locate responder : "+responder_symbol);
      }

      log.debug("main: Looking up requester : "+requester_symbol);
      LocationSymbol requester_symbol_info = DBHelper.lookupLocation(session,"internal",requester_symbol);

      if ( requester_symbol_info == null ) {
        throw new OpenRequestException("Unable to locate requester : "+requester_symbol);
      }

      Location loc = responder_symbol_info.getCanonical_location();
      Location req_loc = requester_symbol_info.getCanonical_location();

      if ( loc.getType() != null ) {
        String responder_type = loc.getType().getCode();
        if ( ( responder_type != null ) && ( responder_type.equals("INT") ) )
          intermediary = true;
      }
      else {
        log.warn("loc.getType returned null!");
      }

      String tgq = ILLStringHelper.extract( (ILL_String_type)request.transaction_id.transaction_group_qualifier);
      String tq = ILLStringHelper.extract( (ILL_String_type)request.transaction_id.transaction_qualifier);

      // Before we continue : Check for duplicate transaction id at the responder location.
      log.debug("Testing for duplicate transaction id....");
      try {
        ILLTransaction chk = DBHelper.lookupTransaction(session,
                                                        loc.getId(),
                                                        initial_requester_symbol,
                                                        tgq,
                                                        tq);
        if ( chk != null ) {
          log.warn("DUPLICATE TRANSACTION DETECTED. Cannot proceed. Respond with SOER from "+msg.getToSymbol()+"to "+msg.getFromSymbol());
          // ToDo : Direct send SOER
          try {
            // 1 = DuplicateTransactionId
            ILL_APDU_type soer_pdu = ISOStatusOrErrorReportMessageFactory.createSOERForDuplicateTransactionId(pdu, BigInteger.valueOf(1),"tgq:"+tgq+", tq: "+tq);

            // pdu, from, to
            ILLMessageEnvelope e = new ILLMessageEnvelope(soer_pdu, msg.getToSymbol(), msg.getFromSymbol());
            // dispatcher.dispatch("System", "STRreq", e);
            DirectAPDUSender.send(e, session, reg, app_ctx);
          }
          catch ( Exception e ) {
            e.printStackTrace();
          }

          throw new OpenRequestException("Duplicate Transaction ID Detected");
        }
      }
      catch ( HelperException he ) {
        log.warn("Problem checking for duplicate transaction",he);
      }

      // Create a header for the request.
      log.debug("Creating a new ILL Request to track this request object");
      com.k_int.openrequest.db.ILLTransaction new_request_info = new com.k_int.openrequest.db.ILLTransaction();

      new_request_info.setLocation(loc);

      if ( intermediary )
        new_request_info.setRole("INT-RESP");
      else
        new_request_info.setRole("RESP");


      if ( request.transaction_type != null )
        new_request_info.setTransactionType(request.transaction_type.longValue());

      ILL_Request_type base_info = (ILL_Request_type) pdu.o;

      new_request_info.setProtocol("ISO10161v"+base_info.protocol_version_num);

      ILLTransactionGroup the_tg = null;
      if ( ( tgq != null ) && ( ! tgq.equals("") ) ) {
        new_request_info.setTGQ(tgq);
        the_tg = DBHelper.createOrFindExistingTG(loc,
                                                 requester_symbol,
                                                 initial_requester_symbol,
                                                 tgq,
                                                 session,
                                                 ( intermediary ? "INT-RESP" : "RESP" ));

        new_request_info.setTransactionGroup(the_tg);

        // Increment the unread message count on both the transaction and the transaction group.
        the_tg.incrementUnreadMessageCount();
        the_tg.setLastMessageDate(now);
        new_request_info.incrementUnreadMessageCount();
        new_request_info.setLastMessageDate(now);
      }
      else {
        log.warn("TGQ is null or empty");
      }

      new_request_info.setRequesterSymbol(requester_symbol);
      new_request_info.setResponderSymbol(responder_symbol);
      new_request_info.setInitialRequesterSymbol(initial_requester_symbol);

      new_request_info.setActiveStateModel(DBHelper.lookupStateModel(session,"RES/PRO"));
      new_request_info.setRequestStatusCode(DBHelper.lookupStatusCode(session,"IN-PROCESS"));

      processExtensions(the_tg, new_request_info, session, request.iLL_request_extensions,reg);

      // We must save first to make sure that the TG has an ID!
      log.debug("Intermediate transaction save to get ID");
      Long new_req_id = (Long) session.save(new_request_info);
      log.debug("New Transaction ID is "+new_req_id);
      the_tg.setLastTransaction(new_request_info);
      the_tg.setTransactionGroupStatusCode(DBHelper.lookupStatusCode(session,"IN-PROCESS"));
      the_tg.setActiveStateModel(DBHelper.lookupStateModel(session,"RES/TG"));

      // Convert the request message into database tables
      log.debug("Storing request message, responder symbol is "+responder_symbol);
      ISO10161RequestMessageDetails rm = ISO10161StorageHelper.storeRequest(base_info,
                                                                            session,
                                                                            new_request_info);



      rm.setRead(false);
      rm.setFromState(DBHelper.lookupStatusCode(session,"IDLE"));
      rm.setEventCode(event);
      rm.setToState(DBHelper.lookupStatusCode(session,"IN-PROCESS"));

      // details.getRequiredItemDetails();
      // the_tg.setRequiredItemDetails(rm.getIsoRequestMessageDetails().getRequiredItemDetails());
      the_tg.setRequiredItemDetails(rm.getRequiredItemDetails());

      // Store any extensions. Do this after setRequiredItemDetails, as some extensions contain additional
      // info about the required item
      ISO10161StorageHelper.storeExtensions(session, new_request_info, rm,  base_info.iLL_request_extensions, reg);

      // Set up any possible protocol variables for the new request header
      new_request_info.setPVReturn(false);
      new_request_info.setPVFwd(rm.getPermToForward());
      new_request_info.setPVChain(rm.getPermToChain());
      new_request_info.setPVPart(rm.getPermToPartition());
      new_request_info.setPVSequenceTimeStamp(now);
      new_request_info.setPVCurrentPartnerIdSymbol(requester_symbol);

      new_request_info.setTGQ(ILLStringHelper.extract((ILL_String_type)request.transaction_id.transaction_group_qualifier));
      new_request_info.setTQ(ILLStringHelper.extract((ILL_String_type)request.transaction_id.transaction_qualifier));
      new_request_info.setSTQ(ILLStringHelper.extract((ILL_String_type)request.transaction_id.sub_transaction_qualifier));

      // If there was partner information in the APDU, store it for use when we next reply, otherwise,
      // use what we know about the symbol.
      if ( ( msg.getPartnerTelecomServiceIdentifier() != null ) && ( msg.getPartnerTelecomServiceAddress() != null ) ) {
        new_request_info.setPartnerTelecomServiceIdentifier(msg.getPartnerTelecomServiceIdentifier());
        new_request_info.setPartnerTelecomServiceAddress(msg.getPartnerTelecomServiceAddress());
      }
      else {
        Service req_svc = (Service) requester_symbol_info.getServiceForThisSymbol();
        new_request_info.setPartnerTelecomServiceIdentifier(req_svc.getTelecomServiceIdentifier());
        new_request_info.setPartnerTelecomServiceAddress(req_svc.getTelecomServiceAddress());
      }

      // Reference initial request message in request header.
      new_request_info.setInitialRequestMessage(rm);

      if ( intermediary ) {
        log.warn("TODO: Intermediary not implemented in this handler");
      }

      log.debug("Commit the new ILL transaction, group and request information to the database");

      // session.update(new_request_info);
      session.flush();
      session.connection().commit();

      // Notify anyone registered that there is a new ILL.
      log.info("Notify any observing locations about the new request ("+new_request_info.getId()+") TG="+the_tg.getId());
      
      ILLMessageEnvelope em = new ILLMessageEnvelope(pdu,requester_symbol,responder_symbol);
      // ILLMessageEnvelope em = new ILLMessageEnvelope(pdu,msg.getFromSymbol(),msg.getToSymbol());

      sendLocationIndication(dispatcher,
                             em,
                             new_request_info,
                             rm,
                             session,
                             "ILLind",
                             loc, 
                             rm.getId(),
                             msg.getILLAssociationContext());
    }
    catch ( OpenRequestException ore ) {
      log.warn("Problem:",ore);
    }
    catch ( java.sql.SQLException sqle ) {
      log.warn("Problem: ",sqle);
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("Problem: ",he);
    }

    return null;
  }

  private void processExtensions(ILLTransactionGroup the_tg, 
                                 ILLTransaction new_request_info, 
                                 Session session, 
                                 ArrayList extensions,
                                 OIDRegister reg) {

    if ( extensions != null ) {

      for ( Iterator i = extensions.iterator(); i.hasNext(); ) {

        Extension_type extension = (Extension_type)i.next();

        if ( ( extension.item != null ) && ( extension.item instanceof EXTERNAL_type ) ) {
          EXTERNAL_type external = (EXTERNAL_type)extension.item;
          int[] oid = external.direct_reference;
          if ( oid != null ) {
            OIDRegisterEntry ent = reg.lookupByOID(oid);
            if ( ent != null ) { 
              if ( ent.getName().equals("ILL_Internal_Reference_Number") ) {
                log.debug("Request has an internal reference number extension");
                if ( ( external.encoding != null ) && ( external.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {
                  Internal_Reference_type irt = (Internal_Reference_type) external.encoding.o;
                  if ( irt.requester_reference != null ) {
                   the_tg.setRequesterReferenceAuthority(ILLStringHelper.extract(irt.requester_reference.reference_authority));
                   the_tg.setRequesterReference(ILLStringHelper.extract(irt.requester_reference.internal_reference));
                  }
                  if ( irt.responder_reference != null ) {
                   the_tg.setResponderReferenceAuthority(ILLStringHelper.extract(irt.responder_reference.reference_authority));
                   the_tg.setResponderReference(ILLStringHelper.extract(irt.responder_reference.internal_reference));
                  }
                }
                else {
                  log.warn("Other problem.....");
                }
  
              }
              else {
                log.warn("Un-processed : "+ent.getName());
              }
            }
            else {
              log.warn("Unknown OID for extension : "+oid);
            }
          }
          else {
              log.warn("No direct reference for extension");
          }
        }
      }
    }
  }

  private void checkSender(String sender_symbol,
                           String requester_name,
                           APDU_Delivery_Info_type delivery_info,
                           Session session,
                           ApplicationContext app_ctx ) throws java.sql.SQLException,
                                                    org.hibernate.HibernateException {
    log.debug("Check "+sender_symbol+" is a known requester symbol");

    LocationSymbol sender_symbol_info = DBHelper.lookupLocation(session,"internal",sender_symbol);

    if ( sender_symbol_info == null ) {
      // Ouch.. unknown requester.
      if ( delivery_info != null ) {
        com.k_int.openrequest.services.Controller controller = (com.k_int.openrequest.services.Controller) app_ctx.getBean("CONTROLLER");

        if ( controller.getImportADISymbols() ) {
          log.debug("Attempting to import delivery information for "+sender_symbol);
          APDUDeliveryInfoStorageHelper.checkPartner(session,
                                                     (APDU_Delivery_Parameters_type)(delivery_info.sender_info.get(0)),
                                                     sender_symbol,
                                                     requester_name);
        }
      }
      else {
        log.warn("Unknown requester and no APDU Delivery Info. Will not be able to import location data");
      }
    }
  }
}
