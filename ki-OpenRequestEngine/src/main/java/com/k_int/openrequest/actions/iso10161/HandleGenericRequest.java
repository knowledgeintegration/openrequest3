/**
 * Title:       
 * @version:    $Id: HandleGenericRequest.java,v 1.12 2005/07/07 17:57:19 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.iso10161;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.util.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.ILL_APDU_Delivery_Info.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.k_int.openrequest.util.WorkflowLogger;
import org.springframework.context.ApplicationContext;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import org.hibernate.*;

public abstract class HandleGenericRequest extends AbstractMessageHandler {

  private Log log = LogFactory.getLog(this.getClass());

  public abstract void performAction(String event_code, Session session, ILLTransaction transaction);
  public abstract void afterSendMessage(String event_code, Session session, InternalIso10161Message message, ILLTransaction t);

  public boolean isValidTransition(ILLTransaction transaction,
                                   Session session,
                                   String event_code)
  {
    try
    {
      StateTransition st = DBHelper.findTransition(session,
                                                   transaction.getActiveStateModel().getId(),
                                                   transaction.getRequestStatusCode().getId(),
                                                   event_code);

      if ( st != null ) 
        return true;
    }
    catch ( Exception e )
    {
    }

      return false;
  }

  public boolean performTransition(ILLTransaction transaction,
                                Session session,
                                String event_code)
  {
    boolean state_changed = false;
    log.debug("performTransition(tno:"+transaction.getId()+" event:"+event_code);

    try
    {
      StateTransition st = DBHelper.findTransition(session,
                                                   transaction.getActiveStateModel().getId(),
                                                   transaction.getRequestStatusCode().getId(),
                                                   event_code);

      log.debug("STATE(req):Update transaction "+transaction.getId()+" status to "+st.getTo_state().getDescription());

      WorkflowLogger.log(""+transaction.getLocation().getId(),
                    "transaction:"+transaction.getId()+" current state:"+st.getFrom_state().getCode()+" event:"+event_code,
                    "Set transaction status to "+st.getTo_state().getCode());

      transaction.setRequestStatusCode(st.getTo_state());

      if ( st.getTo_state().getCode() != st.getFrom_state().getCode() ) {
        state_changed = true;
      }

      /*
      SystemHelper.announceStateTransition(session,
                                           "Transaction.STATE",
                                           st.getFrom_state().getCode(),
                                           st.getTo_state().getCode(),
                                           event_code,      //
                                           transaction.getTransactionGroup());
      */
      log.debug("STATE(req):Request status code now: "+transaction.getRequestStatusCode().getCode());
    }
    catch ( Exception e )
    {
      log.warn("Problem updating state",e);
    }

    return state_changed;
  }

  public Object handle(String event, 
                       Object message, 
                       MessageHandlerContext ctx,
                       ApplicationContext app_ctx) throws MessageHandlerException
  {
    log.debug("storeMessage");
    Session session = ctx.getSession();
    MessageDispatcher dispatcher = ctx.getDispatcher();
    OIDRegister reg = (OIDRegister) app_ctx.getBean("OIDRegister");

    try
    {
      ILLMessageEnvelope msg = (ILLMessageEnvelope)message;
      ILL_APDU_type pdu = msg.getPDU();
      String from_symbol =  msg.getSendingLocationSymbol();
      String to_symbol =  msg.getDestinationLocationSymbol();

      log.debug("Handle Generic Message from "+from_symbol+" to "+to_symbol);

      APDUDeliveryInfoHelper.addMissingDeliveryInfo(msg,session,reg,app_ctx);

      LocationSymbol this_location = DBHelper.lookupLocation(session,"internal",from_symbol);

      log.debug("Calling ISO10161StorageHelper.store to persist message sending loc = "+
                          this_location.getCanonical_location().getId());

      ILLTransaction t = ISO10161StorageHelper.lookupTransaction(session, 
                                                                 this_location.getCanonical_location(), 
                                                                 pdu.o);

      if ( t == null )
      {
        throw new MessageHandlerException("Unable to locate transaction for PDU");
      }

      if ( isValidTransition(t,session,event) )
      {
        // We need to store the outgoing message
        MessageHeader msg_header = ISO10161StorageHelper.store(pdu, 
                                                               session, 
                                                               t,
                                                               new com.k_int.openrequest.isoill.ILLSystemAddress(from_symbol,null,null),
                                                               new com.k_int.openrequest.isoill.ILLSystemAddress(to_symbol,null,null),
                                                               reg);

        msg_header.setRead(true);

        // Make sure we update the various last message dates
        Date now = new Date(System.currentTimeMillis());
        t.setLastMessageDate(now);
        t.getTransactionGroup().setLastMessageDate(now);
   


        msg_header.setFromState(t.getRequestStatusCode());
        boolean state_changed = performTransition(t,session,event);
        // If this message is repeatable....
        if ( state_changed ) {
          t.setLastRepeatableMessage(msg_header);
        }

        msg_header.setToState(t.getRequestStatusCode());
        msg_header.setEventCode(event);
        log.debug("Calling subclass "+this.getClass().getName()+" specific performAction");
        performAction(event,session,t);

        session.flush(); 
        session.connection().commit();

        // And then send it.
        log.debug("Looking up location symbol : "+to_symbol);

        LocationSymbol partner_info = DBHelper.lookupLocation(session,"internal",to_symbol);
        Service service_to_use = partner_info.getServiceForThisSymbol();

        // Service service_to_use = LocateServiceHelper.locate(null,partner_info);

        // ToDo: We need to be a little more careful here,
        if ( service_to_use == null )
          throw new MessageHandlerException("Unable to locate messaging service for Symbol "+to_symbol);

        String queue_name = service_to_use.getHandlerServiceName();
        ILLProtocolAdapter adapter = (ILLProtocolAdapter) app_ctx.getBean(service_to_use.getHandlerServiceName());

        String target_service_identifier = t.getPartnerTelecomServiceIdentifier();
        String target_service_address = t.getPartnerTelecomServiceAddress();

        if ( t.getPartnerTelecomServiceIdentifier() == null ) {
          target_service_identifier =  service_to_use.getTelecomServiceIdentifier();
          target_service_address =  service_to_use.getTelecomServiceAddress();
          t.setPartnerTelecomServiceIdentifier(target_service_identifier);
          t.setPartnerTelecomServiceAddress(target_service_address);
          session.save(t);
        }

        log.info("Sending message to "+target_service_address+" - "+target_service_identifier);

        // We now use the current partner addressing information as found in the APDU Delivery info and
        // stored against the transacton. This allows us to change the recipient for a message by changing the
        // delivery info. We used to use service_to_use.getTelecomServiceIdentifier() and service_to_use.getTelecomServiceAddress()
        InternalIso10161Message reqmsg = new InternalIso10161Message(pdu,
                                                                target_service_identifier,
                                                                target_service_address,
                                                                service_to_use.getProperties(),
                                                                System.currentTimeMillis(),
                                                                from_symbol,
                                                                to_symbol);

        adapter.sendProtocolMessage(reqmsg);
        log.debug("At end of HandleGenericRequest, status code of "+t.getId()+" is "+t.getRequestStatusCode().getCode());

        afterSendMessage(event, session, reqmsg, t);

        if ( msg.getMessageDeliveryObserver() != null ) {
          msg.getMessageDeliveryObserver().notify(null,null,null,null);
        }
        else {
          log.debug("no message delivery observer");
        }
      }
      else
      {
        throw new InvalidStateTransitionException(event+" in state model "+t.getActiveStateModel().getName()+" trying to move from state "+t.getRequestStatusCode().getCode());
      }
    }
    catch ( java.sql.SQLException e ) {
      log.warn("Problem sending message",e);
      throw new MessageHandlerException(e.toString());
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("Problem sending message",he);
      throw new MessageHandlerException(he.toString());
    }
    catch ( com.k_int.openrequest.helpers.HelperException he ) {
      log.warn("Problem sending message",he);
      throw new MessageHandlerException(he.toString());
    }
    catch ( InvalidStateTransitionException iste ) {
      log.warn("Problem sending message",iste);
      throw new MessageHandlerException(iste.toString());
    }

    return Boolean.TRUE;
  }
}
