/**
 * Title:       
 * @version:    $Id: HandleILL_SHI.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.actions.iso10161;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.util.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

public class HandleILL_SHI extends HandleGenericPDU
{
  private Log log = LogFactory.getLog(this.getClass());

  // Use this to veto the change
  public void preStateChange(String old_state,
                             String event_code,
                             String new_state,
                             org.hibernate.Session session,
                             ILLTransaction transaction)
  {
  }

  // Use this to perform an action after the message processed
  public void postStateChange(String old_state,
                              String event_code,
                              String new_state,
                              org.hibernate.Session session,
                              ILLTransaction transaction)
  {
    // Send an internal system message "TG_SHIP_NR" or "TG_SHIP_RET" to the transaction group (Id)
    // Check that ILLTransaction has a member for type (isReturnable)
    // md.dispatch("System","TG_SHIP_NR",new Long(tg.getId()));
    log.debug("Checking for state model transfer");
    try {
      // We must switch the active state model over to the tracking model.
      // Current state will remain SHIPPED!
      if ( ( transaction.getRole().equals("REQ") ) && 
           ( transaction.getShippedServiceType() == 1 ) ) // 1=loan, 2=copy-non-ret
      {
        log.debug("Flick requester state model over to tracking model, since shipped svc type=loan");
        StateModel sm = DBHelper.lookupStateModel(session,"REQ/TRACK");
        transaction.setActiveStateModel(sm);
      }
      else
      {
        log.debug("Shipped svc type=copy non ret so keep non-ret state model");
      }
    }
    catch ( java.sql.SQLException sqle )
    {
      log.warn("Problem performing SHI action",sqle);
    }
    catch ( org.hibernate.HibernateException he)
    {
      log.warn("Problem performing SHI action",he);
    }
  }
}
