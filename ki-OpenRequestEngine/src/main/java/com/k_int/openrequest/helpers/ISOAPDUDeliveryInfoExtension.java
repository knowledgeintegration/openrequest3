package com.k_int.openrequest.helpers;

import java.util.Properties;
import java.util.ArrayList;
import java.util.Date;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.ILL_APDU_Delivery_Info.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import java.math.BigInteger;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.hibernate.*;

/**
 * Title:       ISOShippedMessageFactory
 * @version:    $Id: ISOAPDUDeliveryInfoExtension.java,v 1.2 2005/04/21 10:45:25 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: A helper class that tries to fill out APDU Delivery Info for a particular set of locations.
 */

public class ISOAPDUDeliveryInfoExtension implements ISOExtensionFactory {

  private Session session;
  private Object[] params;

  public ISOAPDUDeliveryInfoExtension(Session session, Object[] params) {
    this.session = session;
    this.params = params;
  }

  public Extension_type create() {
    LocationSymbol sender_location = (LocationSymbol) params[0];
    LocationSymbol recipient_location = (LocationSymbol) params[1];
    LocationSymbol transponder_location = (LocationSymbol) params[2];

    ArrayList sender_info = popLocationInfo(sender_location);
    ArrayList recipient_info = popLocationInfo(recipient_location);
    ArrayList transponder_info = popLocationInfo(transponder_location);

    EXTERNAL_type ext = 
      new EXTERNAL_type (
        new int[] {1,0,10161,13,3}, // reg.oidByName("ILL_APDU_Delivery_Info"), // int[] direct reference
        null, // BigInteger indirect reference
        null, // String data value descriptor
        new encoding_inline0_type(
          encoding_inline0_type.single_asn1_type_CID,
          new APDU_Delivery_Info_type( sender_info, recipient_info, transponder_info)
        )
      );

    return new Extension_type(BigInteger.valueOf(1), Boolean.FALSE, ext);
  }

  // Provide one entry per each transport/encoding supported by the location.
  private static ArrayList popLocationInfo(LocationSymbol loc)
  {
    ArrayList retval = null;
    if ( loc != null )
    {
      retval = new ArrayList();

      // We actually want to add to this vector information about the canonical location
      // rather than the location symbol specified. To begin with tho, we will just provide
      // details of the LocationSymbol
      APDU_Delivery_Parameters_type params = new APDU_Delivery_Parameters_type();

      // Encoding is 1..3 elements of an enumerated integer (1=edifact,2=ber/mime,3=ber) of the
      // prefered encoding at the defined location. List is ordered!
      params.encoding = new ArrayList();
      params.encoding.add(new Integer(3)); // TODO: Quick Test!
  
      Service s = loc.getServiceForThisSymbol();
      String telecom_service_identifier = s.getTelecomServiceIdentifier();
      String telecom_service_address = s.getTelecomServiceAddress();

      params.transport = new System_Address_type(ILLStringHelper.generalString(telecom_service_identifier),
                                                 ILLStringHelper.generalString(telecom_service_address));

      params.aliases = new ArrayList();


    }
    return retval;
  }
}
