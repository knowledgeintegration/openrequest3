/**
 * Title:       DBHelper
 * @version:    $Id: LocateServiceHelper.java,v 1.2 2005/05/13 17:24:40 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import org.hibernate.type.*;
import java.util.*;
import java.math.BigInteger;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

/**
 *  Find the highest priority service that two symbols/locations have in common.
 */
public class LocateServiceHelper
{
  public static Service locate(LocationSymbol local_symbol,
                               LocationSymbol remote_symbol) throws ServiceLocationException
  {
    Service service_to_use = null;

    try
    {
      // TODO: This is a stub that always just returns the first service of the remote symbol
      Location loc = remote_symbol.getCanonical_location();
      System.err.println("Sending message to "+loc);
      service_to_use = (Service) remote_symbol.getServiceForThisSymbol();
    }
    catch ( Exception e )
    {
      throw new ServiceLocationException(e.toString());
    }

    return service_to_use;
  }
}
