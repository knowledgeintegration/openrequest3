/**
 * Title:       ISORequestFactory
 * @version:    $Id: ILLStringHelper.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class ILLStringHelper
{
  public static String extract(ILL_String_type s)
  {
    if ( s != null )
    {
      return s.o.toString();
    }

    return null;
  }

  public static ILL_String_type generalString(String s)
  {
    if ( s != null )
       return new ILL_String_type(ILL_String_type.generalstring_var_CID,s);

    return null;
  }
}
