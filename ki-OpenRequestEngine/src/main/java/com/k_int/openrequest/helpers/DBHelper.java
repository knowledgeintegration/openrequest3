package com.k_int.openrequest.helpers;

import org.hibernate.*;
import org.hibernate.type.*;
import java.util.*;
import java.math.BigInteger;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import org.apache.commons.logging.*;

/**
 * Title:       DBHelper
 * @version:    $Id: DBHelper.java,v 1.18 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: A number of static utility functions that extract proxy objects from the database
 *              given different criteria.
 */
public class DBHelper {

  private static Log log = LogFactory.getLog(DBHelper.class);

  /** 
   *  Attempt to look up a canonical location given a list of symbols 
   * @param session A valid Hibernate session connected to the ILL transaction store database
   * @param auth_symbol An array of location symbols in the format NamingAuthority:Symbol
   * @return A Proxy object that reflects the location symbol information from the database.
   * @exception java.sql.SQLException If malformed SQL was generated (Probably indicating a connection to
   * the wrong database schema.
   * @exception org.hibernate.HibernateException If there was a problem with the persistence layer.
   */
  public static LocationSymbol lookupLocation(Session session,
                                              String[] auth_symbols) throws java.sql.SQLException,
                                                                            org.hibernate.HibernateException {
    LocationSymbol result = null;
    for ( int i=0; ((i<auth_symbols.length)&&(result==null)); i++) {
      result = lookupLocation(session,auth_symbols[i]);
    }
    return result;
  }

  /**
   * Add any unknown symbols to the given location.
   * @param session A valid Hibernate session connected to the ILL transaction store database
   * @param auth_symbol An array of location symbols in the format NamingAuthority:Symbol
   * @return A Proxy object that reflects the location symbol information from the database.
   * @exception java.sql.SQLException If malformed SQL was generated (Probably indicating a connection to
   * the wrong database schema.
   * @exception org.hibernate.HibernateException If there was a problem with the persistence layer.
   */
  public static LocationSymbol lookupLocationViaSymbolList(Session session,
                                                           String[] auth_symbols) throws java.sql.SQLException,
                                                                            org.hibernate.HibernateException {
    List unknown_symbols = new ArrayList(); 
    LocationSymbol result = null;
    for ( int i=0; ((i<auth_symbols.length)&&(result==null)); i++) {
      LocationSymbol current_symbol = lookupLocation(session,auth_symbols[i]);
      if ( current_symbol != null ) {
        if ( result != null ) {
          // Symbol recognised but we already have determined the location
        }
        else {
          result = current_symbol;
        }
      }
      else {
        unknown_symbols.add(auth_symbols[i]);
      }
    }

    // Add any missing symbols
    if ( result != null ) {
      for ( Iterator it = unknown_symbols.iterator(); it.hasNext(); ) {
        String symbol = (String)(it.next());
        log.debug("Unknown symbol "+symbol+" at location "+ result.toString());
        // result.getAliases().add( new LocationSymbol(name, authority, location, service_for_this_symbol) );
      }
    }

    return result;
  }


  /**
   * Look up location symbol information for the supplied string given a specific database session.
   * Requires: 
   * <ul>
   *   <li>A valid Session connection to the ILL Transaction Store Database
   *   <li>A location symbol in the format "NamingAuthority:LocationSymbol" for example NATLIB:NHATS
   * </ul>
   * Provides:
   * <ul>
   *   <li>An Instance of the java class that acts as a proxy for LocationSymbol objects taken from the
   *   database. From the LocationSymbol you can navigate to the canonical location details and other 
   *   information.
   * </ul>
   *
   * @param session A valid Hibernate session connected to the ILL transaction store database
   * @param auth_symbol A location symbol in the format NamingAuthority:Symbol
   * @return A Proxy object that reflects the location symbol information from the database.
   * @exception java.sql.SQLException If malformed SQL was generated (Probably indicating a connection to
   * the wrong database schema.
   * @exception org.hibernate.HibernateException If there was a problem with the persistence layer.
   *           
   */
  public static LocationSymbol lookupLocation(Session session,
                                              String auth_symbol) throws java.sql.SQLException,
                                                                         org.hibernate.HibernateException
  {
    return lookupLocation(session, null, auth_symbol);
  }

  public static LocationSymbol lookupLocation(Session session,
                                              String default_naming_authority,
                                              String auth_symbol) throws java.sql.SQLException,
                                                                         org.hibernate.HibernateException {
    LocationSymbol retval = null;
    String authority = null;
    String symbol = null;

    log.debug("DBHelper::lookupLocation("+auth_symbol+")");

    int pos = 0;
    if ( ( pos = auth_symbol.indexOf(":") ) >= 0 ) {
      authority = auth_symbol.substring(0,pos);
      symbol = auth_symbol.substring(pos+1);
    }
    else {
      authority = default_naming_authority;
      symbol = auth_symbol;
    }

    log.debug("Symbol: "+symbol+", authority: "+authority);

    Query q = session.createQuery("select r from com.k_int.openrequest.db.Location.LocationSymbol r where r.authority.upperIdentifier = ?  and r.upperSymbol = ?");
    q.setParameter(0,authority.toUpperCase(),Hibernate.STRING);
    q.setParameter(1,symbol.toUpperCase(),Hibernate.STRING);
    retval = (LocationSymbol) q.uniqueResult();

    log.debug("Returning "+retval);
    return retval;
  }

  public static ILLTransaction lookupTransaction(Session session,
                                                 Long id) throws java.sql.SQLException,
                                                                 org.hibernate.HibernateException {
    ILLTransaction result = null;

    log.debug("lookupTransaction("+id+")");
    Query q = session.createQuery("select r from com.k_int.openrequest.db.ILLTransaction r where r.id = ?");
    q.setParameter(0,id, Hibernate.LONG);
    result = (ILLTransaction) q.uniqueResult();
    return result;
  }

  /**
   *  Try and lookup a transaction.. Assume that only the requester or responder is present in this system.
   */ 
  public static ILLTransaction lookupTransaction(Session session,
                                                 String initial_requester,
                                                 String tgq,
                                                 String tq,
                                                 String stq) throws HelperException {

    ILLTransaction retval = null;

    try {
      log.info("lookupTransaction("+initial_requester+","+tgq+","+tq+","+stq+")");
      
      List l = null;

      if ( stq != null ) {
        Query q = session.createQuery("select t from com.k_int.openrequest.db.ILLTransaction t where t.initialRequesterSymbol = ? and t.TGQ = ? and t.TQ = ? and t.STQ = ?");
        q.setParameter(0,initial_requester,Hibernate.STRING);
        q.setParameter(1,tgq,Hibernate.STRING);
        q.setParameter(2,tq,Hibernate.STRING);
        q.setParameter(3,stq,Hibernate.STRING);
        l = q.list();
      }
      else {
        Query q = session.createQuery("select t from com.k_int.openrequest.db.ILLTransaction t where t.initialRequesterSymbol = ? and t.TGQ = ? and t.TQ = ?");
        q.setParameter(0,initial_requester,Hibernate.STRING);
        q.setParameter(1,tgq,Hibernate.STRING);
        q.setParameter(2,tq,Hibernate.STRING);
        l = q.list();
      }

      if ( l.size() == 1 ) {
        retval = (ILLTransaction)l.get(0);
      }
      else {
        log.warn("Unable to identify unique transaction with supplied details..(found "+l.size()+" rows)");
      }
    }
    catch ( org.hibernate.HibernateException he ) {
      throw new HelperException(he.toString(), he);
    }

    return retval;
  }

  public static ILLTransaction lookupTransaction(Session session,
                                                 Long location_id,
                                                 String initial_requester,
                                                 String tgq,
                                                 String tq,
                                                 String stq) throws HelperException
  {
    ILLTransaction retval = null;

    try {
      log.info("lookupTransaction("+location_id+","+initial_requester+","+tgq+","+tq+","+stq+")");

      Query q = session.createQuery("select  t from com.k_int.openrequest.db.ILLTransaction t where t.location.id = ? and t.initialRequesterSymbol = ? and t.TGQ = ? and t.TQ = ? and t.STQ = ?");
      q.setParameter(0,location_id,Hibernate.LONG);
      q.setParameter(1,initial_requester,Hibernate.STRING);
      q.setParameter(2,tgq,Hibernate.STRING);
      q.setParameter(3,tq,Hibernate.STRING);
      q.setParameter(4,stq,Hibernate.STRING);

      retval = (ILLTransaction) q.uniqueResult();
    }
    catch ( org.hibernate.HibernateException he ) {
      throw new HelperException(he.toString());
    }

    return retval;
  }

  public static ILLTransaction lookupTransaction(Session session,
                                                 Long location_id,
                                                 String initial_requester,
                                                 String tgq,
                                                 String tq) throws HelperException
  {
    ILLTransaction retval = null;
    try {
      log.info("lookupTransaction("+location_id+","+initial_requester+","+tgq+","+tq+")");

      Query q = session.createQuery("select t from com.k_int.openrequest.db.ILLTransaction t where t.location.id = ? and t.initialRequesterSymbol = ? and t.TGQ = ? and t.TQ = ?");
      q.setParameter(0,location_id,Hibernate.LONG);
      q.setParameter(1,initial_requester,Hibernate.STRING);
      q.setParameter(2,tgq,Hibernate.STRING);
      q.setParameter(3,tq,Hibernate.STRING);

      retval = (ILLTransaction) q.uniqueResult();
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("HE",he);
      throw new HelperException(he.toString());
    }

    return retval;
  }

  // TODO: This should throw a not-found exception.. As should everything in here really!
  public static ILLTransactionGroup lookupGAR(Session session,
                                              Long id) throws java.sql.SQLException,
                                                              org.hibernate.HibernateException {
    Query q = session.createQuery("select t from com.k_int.openrequest.db.ILLTransactionGroup t where t.id = ?");
    q.setParameter(0,id,Hibernate.LONG);
    ILLTransactionGroup req = (ILLTransactionGroup) q.uniqueResult();
    return req;
  }

  public static ILLTransactionGroup findExistingTG(Location location,
                                                   String original_requester,
                                                   String tgq,
                                                   Session session) throws java.sql.SQLException,
                                                              org.hibernate.HibernateException {
    log.debug("findExistingTG("+location.getId()+","+original_requester+","+tgq+")");
    Query q = session.createQuery("select tg from com.k_int.openrequest.db.ILLTransactionGroup tg where tg.location.id = ? and tg.initialRequesterSymbol = ? and tg.TGQ = ?");
    q.setParameter(0,location.getId(),Hibernate.LONG);
    q.setParameter(1,original_requester,Hibernate.STRING);
    q.setParameter(2,tgq,Hibernate.STRING);
    ILLTransactionGroup retval = (ILLTransactionGroup) q.uniqueResult();
    return retval;
  }

  public static ILLTransactionGroup createOrFindExistingTG(Location location,
                                                           String requester,
                                                           String original_requester,
                                                           String tgq,
                                                           Session session,
                                                           String role) throws java.sql.SQLException,
                                                              org.hibernate.HibernateException {
    log.debug("createOrFindExistingTG("+location.getId()+","+original_requester+","+tgq+")");
    ILLTransactionGroup retval = null;

    retval=findExistingTG(location, original_requester, tgq, session);

    if ( retval == null ) {
      log.debug("Create new transaction group.....");
      retval = new ILLTransactionGroup(location, original_requester, tgq, role);
      retval.setRequesterSymbol(requester);
      session.save(retval);
    }

    return retval;
  }

  public static List getLiveTransactions(Session session, 
                                         Long location_id) throws java.sql.SQLException,
                                                                  org.hibernate.HibernateException {
    Query q = session.createQuery("select r from com.k_int.openrequest.db.ILLTransaction r where r.location.id = ?");
    q.setParameter(0,location_id,Hibernate.LONG);
    List result = q.list();
    return result;
  }
  
  public static StateTransition findTransition(Session session,
                                               Long model_id,
                                               Long current_state_id,
                                               String transition) throws java.sql.SQLException,
                                                                  org.hibernate.HibernateException {
    StateTransition retval = null;
    Query q = session.createQuery("select st from com.k_int.openrequest.db.StateTransition st where st.model.id = ? and st.from_state.id = ? and st.transitionCode = ?");
    q.setParameter(0,model_id,Hibernate.LONG);
    q.setParameter(1,current_state_id,Hibernate.LONG);
    q.setParameter(2,transition,Hibernate.STRING);
    retval = (StateTransition) q.uniqueResult();
    return retval;
  }

  public static StateModel lookupStateModel(Session session,
                                            String code) throws java.sql.SQLException,
                                                         org.hibernate.HibernateException {
    StateModel retval = null;
    Query q = session.createQuery("select sm from com.k_int.openrequest.db.StateModel sm where sm.code = ?");
    q.setParameter(0,code,Hibernate.STRING);
    retval = (StateModel) q.uniqueResult();
    return retval;
  }

  public static StatusCode lookupStatusCode(Session session,
                                            String code) throws java.sql.SQLException,
                                                         org.hibernate.HibernateException {
    StatusCode retval = null;
    Query q = session.createQuery("select sc from com.k_int.openrequest.db.StatusCode sc where sc.code = ?");
    q.setParameter(0,code,Hibernate.STRING);
    retval = (StatusCode) q.uniqueResult();
    return retval;
  }

  public static LocationType lookupLocationType(Session session,
                                                String code) throws java.sql.SQLException,
                                                         org.hibernate.HibernateException {
    LocationType retval = null;
    Query q = session.createQuery("select lt from com.k_int.openrequest.db.Location.LocationType lt where lt.code = ?");
    q.setParameter(0,code,Hibernate.STRING);
    retval = (LocationType) q.uniqueResult();
    return retval;
  }

  public static NamingAuthority lookupOrCreateAuthority(Session session,
                                                        String code,
                                                        String name) throws java.sql.SQLException,
                                                                            org.hibernate.HibernateException{
    NamingAuthority result = lookupNamingAuthority(session, code);
    if ( result == null ) {
      result = new NamingAuthority(code,name);
      session.save(result);
    }
    return result;
  }

  public static NamingAuthority lookupNamingAuthority(Session session,
                                                      String code)throws java.sql.SQLException,
                                                         org.hibernate.HibernateException {
    NamingAuthority retval = null;
    Query q = session.createQuery("select na from com.k_int.openrequest.db.Location.NamingAuthority na where na.upperIdentifier = ?");
    q.setParameter(0,code,Hibernate.STRING);
    retval = (NamingAuthority) q.uniqueResult();
    return retval;
  }

  public static Service lookupServiceByName(Session session,
                                            String desc)throws java.sql.SQLException,
                                                         org.hibernate.HibernateException {
    Service retval = null;
    Query q = session.createQuery("select svc from com.k_int.openrequest.db.Location.Service svc where svc.description = ?");
    q.setParameter(0,desc,Hibernate.STRING);
    retval = (Service) q.uniqueResult();
    return retval;
  }

  public static MessageHeader lookupMessage(Session session,
                                            Long message_id) throws java.sql.SQLException,
                                                                    org.hibernate.HibernateException {
    MessageHeader retval = null;
    Query q = session.createQuery("select hdr from com.k_int.openrequest.db.MessageHeader hdr where hdr.id = ?");
    q.setParameter(0,message_id,Hibernate.LONG);
    retval = (MessageHeader) q.uniqueResult();
    return retval;
  }

  public static Service lookupOrCreateService(Session session,
                                              String service_identifier,
                                              String service_address,
                                              String name,
                                              String type,
                                              String adapter,
                                              long priority,
                                              int encoding) throws java.sql.SQLException, org.hibernate.HibernateException {
    return lookupOrCreateService(session, service_identifier,service_address,name,type,adapter,priority,encoding,0,null,null);
  }

  public static Service lookupOrCreateService(Session session,
                                              String service_identifier,
                                              String service_address,
                                              String name,
                                              String type,
                                              String adapter,
                                              long priority,
                                              int encoding,
                                              int auth_method,
                                              String user,
                                              String pass) throws java.sql.SQLException,
                                                                   org.hibernate.HibernateException {
    Service result = null;

    Query q = session.createQuery("select svc from com.k_int.openrequest.db.Location.Service svc where svc.telecomServiceIdentifier = ? and svc.telecomServiceAddress = ?");
    q.setParameter(0,service_identifier,Hibernate.STRING);
    q.setParameter(1,service_address,Hibernate.STRING);
    result = (Service) q.uniqueResult();

    if ( result == null ) {
      result = new com.k_int.openrequest.db.Location.Service(name,type,adapter,priority,encoding);
      result.setTelecomServiceIdentifier(service_identifier);
      result.setTelecomServiceAddress(service_address);
      result.setAuthMethod(auth_method);
      result.setUsername(user);
      result.setPassword(pass);
      session.save(result);
    }

    return result;
  }

  /**
   * Try to locate a namespace within a specific authority for the input symbol.
   * Exmaple:  If we have location 1000 with aliases oclc:FRED and other:FREDA and we get a request to other:FREDA,
   * this method can be used as resolveAlias('other:FREDA', 'oclc') to try and discover an alternate location
   * code for the input location. This is useful where organisations always maintain their own set of location
   * codes and need to map from the symbol used by the request into a local symbol.
   * @param alias_to_resolve : The symbol to lookup, eg "other:FREDA'
   * @param target_authority : The target authority, eg "OCLC"
   * @param session : The Hibernate Session
   * @return The identified LocationSymbol or null if one cannot be found
   */
  public static LocationSymbol resolveAlias(String alias_to_resolve, 
                                            String target_authority, 
                                            Session session) throws java.sql.SQLException,
                                                                    org.hibernate.HibernateException {

    LocationSymbol result = null;

    // Step 1 : Locate the location symbol for the alias to resolve.
    LocationSymbol l1 = lookupLocation(session,null,alias_to_resolve);
    NamingAuthority target_auth = lookupNamingAuthority(session, target_authority);

    if ( ( l1 == null ) && ( target_auth == null ) ) {
      log.debug("resolveAlias("+alias_to_resolve+","+target_authority+") Unable to lookup alias to resolve or target namespace");
    }
    else {
      Object[] values = { l1.getCanonical_location().getId(), target_auth.getId() };
      Type[] types = { TypeFactory.basic("java.lang.Long"), TypeFactory.basic("java.lang.Long") };

      Query q = session.createQuery("select l from com.k_int.openrequest.db.Location.LocationSymbol l where l.canonical_location.id=? and l.authority.id=?");
      q.setParameter(0,l1.getCanonical_location().getId(),Hibernate.LONG);
      q.setParameter(1,target_auth.getId(),Hibernate.LONG);
      result = (LocationSymbol) q.uniqueResult();
    }

    return result;
  }

  public static List getQueuedMessages(Session sess, String queue_name) throws java.sql.SQLException, org.hibernate.HibernateException {
    return sess.createQuery("select l from com.k_int.openrequest.db.MessageQueueItem l where l.queueName=? order by l.dateAdded desc")
                   .setParameter(0,queue_name,Hibernate.STRING)
                   .list();
  }
}
