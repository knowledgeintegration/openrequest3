/**
 * Title:       ISOExtensionsHelper
 * @version:    $Id: ISOExtensionsHelper.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Recall PDU
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.*;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *  Populate the requested extensions returning a vector of extensions
 *  @author Ian Ibbotson
 *  @version $Id: ISOExtensionsHelper.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 */
public class ISOExtensionsHelper
{
  public static Log cat = LogFactory.getLog(ISOExtensionsHelper.class);

  /**
   *
   */
  public static ArrayList create(ISOExtensionFactory[] extens) throws java.sql.SQLException,
                                                             org.hibernate.HibernateException
  {
    ArrayList retval = null;

    if ( ( extens != null ) && ( extens.length > 0 ) )
    {
      retval = new ArrayList();

      for ( int i=0; i<extens.length; i++ )
      {
        // Process exten i and add to vector
        cat.debug("Adding extension.....");
        retval.add(extens[i].create());
      }
    }

    return retval;
  }
}
