/**
 * Title:       ISOReceivedMessageFactory
 * @version:    $Id: ISOReceivedMessageFactory.java,v 1.2 2005/07/08 08:29:17 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.Properties;
import java.util.ArrayList;
import java.util.Date;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 *  Create an ISO ILL Received Message.
 *  This class will construct an iso ill received message
 *  @author Ian Ibbotson
 *  @version $Id: ISOReceivedMessageFactory.java,v 1.2 2005/07/08 08:29:17 ibbo Exp $
 */
public class ISOReceivedMessageFactory {
  /**
   *  create a Received message.
   *
   *  Requires: An ILL Transaction in a state for which RCVreq is a valid action
   *
   *  Provides: A completed ILL Recieved message with data derived as follows:
   *            N.B where a member could be set from the transaction store, speak with k-int
   *            about defaulting that member in, it's better than starting to track transaction data
   *            in databases outside the transaction store.
   *
   *  element name/type                        M/O	Source
   *  retval : ILL_APDU_type (Choice of pdu)	M       Filled in by factory
   *    .which : int				M       Filled in by factory
   *    .o : Recieved_type			M       Filled in by factory
   *      .protocol_version_num
   *      .transaction_id			M	Constructed field, populated by this method
   *        .initial_requester_id		M	
   *          .person_or_institution_symbol	O	Set by factory from database
   *          .name_of_person_or_institution    O	Not set
   *        .tgq				M	Set by factory from database
   *        .tq					M	Set by factory from database
   *        .stq				O	Set by factory from database
   *      .service_date_time			M	Constructed by factory
   *        .date_time_of_this_service          M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *        .date_time_of_original_service      M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *      .requester_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *      .responder_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *
   *                   -- End of common elements. Recieved specific follow --
   *
   *      .supplier_id				O	Not set
   *        .person_or_institution_symbol       O
   *        .person_or_institution_name         O
   *      .supplemental_item_description	O	Not set
   *      .date_received			M	Set to system date
   *      .shipped_service_type			M	Set from database to value from shipped msg
   *      .requester_note			O	Not Set
   *	  .received_extensions			O	Depends on Extension Factory
   *      
   *
   *
   *  Ends description of how shipped PDU is populated by this factory method
   *
   *  @author Ian Ibbotson
   *  @param database_session The database session we will use to access the database
   *  @param request_id request id (internal number) of the transaction we are dealing with
   *  @param shipped_service_type 
   *  @param date_shipped On what date was the item shipped
   *  @param date_due When is the item due
   *  @param renewable Is the load renewable
   *  @param chargeable_units What are the chargable units
   *  @param cost What is the cost
   *  @param conditions What are the loan conditions
   *  @param extens And array of extension factories that will set up a shipped extension
   *                for each factory in the array.
   * 
   *  @return An ILL_APDU containing a Received message
   */
  public static ILL_APDU_type create(Session database_session, 
                                     ILLTransaction transaction,
                                     Date date_received,
                                     BigInteger received_service_type,
                                     String requester_note,
                                     ISOExtensionFactory[] extens) throws java.sql.SQLException,
                                                             org.hibernate.HibernateException
  {
    Received_type received = new Received_type();
    ILL_APDU_type retval = new ILL_APDU_type( ILL_APDU_type.received_var_CID, received);

    // Fill out protocol version num,  transaction id
    ISOHeaderDetailsHelper.fillOutHeader(database_session, transaction, received);

    // received.date_received = df.format(date_received).toString();
    received.date_received = Constants.formatDate(date_received);

    if ( received_service_type != null )
      received.shipped_service_type = received_service_type;
    else
      received.shipped_service_type = BigInteger.valueOf(transaction.getShippedServiceType());

    if ( requester_note != null )
      received.requester_note = ILLStringHelper.generalString(requester_note);

    if ( (extens != null) && ( extens.length > 0 ) )
    {
      ArrayList extensions = new ArrayList();
      for ( int i=0; i<extens.length; i++ )
      {
        // Process exten i and add to vector
      }

      received.received_extensions = extensions;
    }

    return retval;
  }
}
