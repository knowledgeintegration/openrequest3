/**
 * Title:       CancelReplyMessageFactory
 * @version:    $Id: ISOCancelReplyMessageFactory.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Cancel Reply PDU 
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.Properties;
import java.util.ArrayList;
import java.util.Date;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 *  Create an ISO ILL Cancel Reply
 *  This class will construct an iso cancel reply
 *  @author Ian Ibbotson
 *  @version $Id: ISOCancelReplyMessageFactory.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 */
public class ISOCancelReplyMessageFactory {

  /**
   *  create a Cancel Reply Messahe
   *
   *  Requires: An ILL Transaction in a state for which CREPreq+ or CREPreq- is a valid action
   *
   *  Provides: A completed ILL Cancel Reply message with data derived as follows:
   *            N.B where a member could be set from the transaction store, speak with k-int
   *            about defaulting that member in, it's better than starting to track transaction data
   *            in databases outside the transaction store.
   *
   *  element name/type                        M/O	Source
   *  retval : ILL_APDU_type (Choice of pdu)	M       Filled in by factory
   *    .which : int				M       Filled in by factory
   *    .o : Shipped_type			M       Filled in by factory
   *      .protocol_version_num
   *      .transaction_id			M	Constructed field, populated by this method
   *        .initial_requester_id		M	
   *          .person_or_institution_symbol	O	Set by factory from database
   *          .name_of_person_or_institution    O	Not set
   *        .tgq				M	Set by factory from database
   *        .tq					M	Set by factory from database
   *        .stq				O	Set by factory from database
   *      .service_date_time			M	Constructed by factory
   *        .date_time_of_this_service          M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *        .date_time_of_original_service      M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *      .requester_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *      .responder_id				M	Set from new_responder param of create method
   *        .person_or_institution_symbol       O       Set from new_responder param of create method
   *        .person_or_institution_name         O       Set from new_responder param of create method
   *
   *                   -- End of common elements. Shipped specific follow --
   *      .answer				M	Set from answer parameter
   *      .responder_note			O  	Set from note parameter
   *      .cancel_reply_extensions		O	Created by factory from extens param
   *  Ends description of how Conditional Reply PDU is populated by this factory method
   *
   *  @author Ian Ibbotson
   *  @param answer true or false
   *  @param note
   *  @param extens An array of extension factories that will set up a shipped extension
   *                for each factory in the array.
   * 
   *  @return An ILL_APDU containing a Cancel Reply message
   */
  public static ILL_APDU_type create(Session database_session, 
                                     ILLTransaction transaction,
                                     Boolean answer,
                                     String note,
                                     ISOExtensionFactory[] extens) throws java.sql.SQLException,
                                                             org.hibernate.HibernateException
  {
    Cancel_Reply_type crep = new Cancel_Reply_type();
    ILL_APDU_type retval = new ILL_APDU_type( ILL_APDU_type.cancel_reply_var_CID, crep);

    // Fill out protocol version num,  transaction id
    ISOHeaderDetailsHelper.fillOutHeader(database_session, transaction, crep);

    crep.answer = answer;
    crep.responder_note = ILLStringHelper.generalString(note);
    
    if ( ( extens != null ) && ( extens.length > 0 ) )
    {
      ArrayList extensions = new ArrayList();
      for ( int i=0; i<extens.length; i++ )
      {
        // Process exten i and add to vector
      }

      crep.cancel_reply_extensions = extensions;
    }


    return retval;
  }
}
