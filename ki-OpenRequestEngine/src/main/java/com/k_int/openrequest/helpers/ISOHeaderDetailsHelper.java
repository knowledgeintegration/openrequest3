/**
 * Title:       ISORequestFactory
 * @version:    $Id: ISOHeaderDetailsHelper.java,v 1.9 2005/07/08 08:29:17 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.*;

import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.ILL_APDU_Delivery_Info.*;
import com.k_int.openrequest.isoill.gen.Internal_Reference_Number.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import org.jzkit.z3950.gen.v3.AccessControlFormat_prompt_1.*;

import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.context.ApplicationContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class ISOHeaderDetailsHelper {
  
  public static Log cat = LogFactory.getLog(ISOHeaderDetailsHelper.class);

  public static void fillOutHeader(Session hibernate_session, 
 				   ILLTransaction req,
				   Object pdu) throws java.sql.SQLException, org.hibernate.HibernateException {
    fillOutHeader(hibernate_session,req,pdu,false,null,null);
  }

  /**
   * Fills out the common fields for an ISO request (For all message except the inital request)
   */
  public static void fillOutHeader(Session hibernate_session, 
                                   ILLTransaction req,
                                   Object pdu,
                                   boolean repeat,
                                   String original_service_date_str,
                                   String original_service_time_str) throws java.sql.SQLException,
                                                                      org.hibernate.HibernateException
  {
    // ILLTransaction req = (ILLTransaction) hibernate_session.find("from r in class com.k_int.openrequest.db.ILLTransaction where r.id = ?", new Long(request_id), Hibernate.LONG).get(0);

    // public BigInteger protocol_version_num = null;
    // public Transaction_Id_type transaction_id = null;
    // public Service_Date_Time_type service_date_time = null;
    // public System_Id_type requester_id = null;
    // public System_Id_type responder_id = null;

    //   java.lang.reflect.Field extensions_field = pdu.o.getClass().getDeclaredField(field_name);
    //   extensions = (ArrayList) extensions_field.get(pdu.o);
    //   if ( extensions == null ) {
    //     extensions = new ArrayList();
    //     extensions_field.set(pdu.o, extensions);
    //   }
    try {
      java.lang.reflect.Field protocol_version_num_field = pdu.getClass().getDeclaredField("protocol_version_num");
      java.lang.reflect.Field transaction_id_field = pdu.getClass().getDeclaredField("transaction_id");
      java.lang.reflect.Field service_date_time_field = pdu.getClass().getDeclaredField("service_date_time");
      java.lang.reflect.Field requester_id_field = pdu.getClass().getDeclaredField("requester_id");
      java.lang.reflect.Field responder_id_field = pdu.getClass().getDeclaredField("responder_id");


      String requester_symbol = req.getRequesterSymbol();
      String responder_symbol = req.getResponderSymbol();
      // String responder_symbol = req.getPVCurrentPartnerIdSymbol();
      String initial_requester_symbol = req.getInitialRequesterSymbol();

      java.util.Date d = new java.util.Date( System.currentTimeMillis() );

      String date_str = Constants.formatDate(d);
      String time_str = Constants.formatTime(d);

      System_Id_type initial_requester_info = null;
      if ( initial_requester_symbol != null )
      {
        cat.debug("Set initial requester symbol to "+initial_requester_symbol);
        initial_requester_info =
          new System_Id_type(
            new Person_Or_Institution_Symbol_type(
              Person_Or_Institution_Symbol_type.institution_symbol_CID,
               ILLStringHelper.generalString(initial_requester_symbol)),
          null);
      }

      protocol_version_num_field.set(pdu,BigInteger.valueOf(2));

      cat.debug("Adding transaction id");

      transaction_id_field.set(pdu, new Transaction_Id_type( initial_requester_info,
                                                             ILLStringHelper.generalString(req.getTGQ()),
                                                             ILLStringHelper.generalString(req.getTQ()),
                                                             ILLStringHelper.generalString(req.getSTQ())));

      cat.debug("Adding Service Date Time");


      // Don't set orig date and time by default, only on a repeat pdu.
      date_time_of_original_service_inline36_type orig_dt = null;
      if ( repeat )
        orig_dt = new date_time_of_original_service_inline36_type(original_service_date_str, original_service_time_str);

      service_date_time_field.set(pdu, new Service_Date_Time_type( new date_time_of_this_service_inline35_type(date_str,time_str),
                                                                   orig_dt ) );

      cat.debug("Adding Requester Id");

      requester_id_field.set(pdu, new System_Id_type( new Person_Or_Institution_Symbol_type(                         //   o     Person or inst sym
                                                       Person_Or_Institution_Symbol_type.institution_symbol_CID,
                                                       ILLStringHelper.generalString(requester_symbol)),
                                                     null));

      cat.debug("Adding Responder Id");

      responder_id_field.set(pdu, new System_Id_type(
                  new Person_Or_Institution_Symbol_type(                         //   o     Person or inst sym
                    Person_Or_Institution_Symbol_type.institution_symbol_CID,
                    ILLStringHelper.generalString(responder_symbol)),
                  null));
    }
    catch (java.lang.NoSuchFieldException nsfe) {
      nsfe.printStackTrace();
    }
    catch (java.lang.IllegalAccessException iae) {
      iae.printStackTrace();
    }
  }

  public static void addAPDUDeliveryInfo(Session hibernate_session, 
                                         LocationSymbol sender_location,
                                         LocationSymbol transponder_location,
                                         LocationSymbol recipient_location,
                                         ArrayList extensions,
                                         ApplicationContext app_context) throws java.sql.SQLException,
                                                                          org.hibernate.HibernateException {

    if ( app_context.containsBean("APDUDeliveryInfoProvider") ) {
      APDUDeliveryInfoPolicyProvider apdu_deliv_info_provider = (APDUDeliveryInfoPolicyProvider) app_context.getBean("APDUDeliveryInfoProvider");
      apdu_deliv_info_provider.process(hibernate_session,
                                       sender_location,
                                       transponder_location,
                                       recipient_location,
                                       extensions,
                                       app_context);
    }
    else
      defaultAPDUDeliveryInfo(hibernate_session,sender_location,transponder_location,recipient_location,extensions,app_context);
  }

  public static void defaultAPDUDeliveryInfo(Session hibernate_session, 
                                             LocationSymbol sender_location,
                                             LocationSymbol transponder_location,
                                             LocationSymbol recipient_location,
                                             ArrayList extensions,
                                             ApplicationContext app_context) throws java.sql.SQLException,
                                                                          org.hibernate.HibernateException
  {
    cat.debug("Adding APDU Delivery Info sender="+sender_location+", recipient="+recipient_location);

    ArrayList sender_info = popLocationInfo(sender_location,0);
    ArrayList recipient_info = popLocationInfo(recipient_location,1);
    ArrayList transponder_info = popLocationInfo(transponder_location,2);

    Extension_type extension = new Extension_type();

    extension.identifier = BigInteger.valueOf(1);
    extension.critical = Boolean.FALSE;
    extension.item = 
      new EXTERNAL_type (
        new int[] {1,0,10161,13,3}, // reg.oidByName("ILL_APDU_Delivery_Info"), // int[] direct reference
        null, // BigInteger indirect reference
        null, // String data value descriptor
        new encoding_inline0_type(
          encoding_inline0_type.single_asn1_type_CID,
          new APDU_Delivery_Info_type( sender_info, recipient_info, transponder_info)
        )
      );

      extensions.add(extension);
  }

  public static void addAuthenticationInfo(Session hibernate_session, 
                                           LocationSymbol sender_location,
                                           LocationSymbol transponder_location,
                                           LocationSymbol recipient_location,
                                           ArrayList extensions) throws java.sql.SQLException,
                                                                        org.hibernate.HibernateException {


    // If the target service needs us to add APDU delivery info
    if ( recipient_location.getServiceForThisSymbol().getAuthMethod() == 1 ) {  // Don't ever do this yet
      cat.debug("Adding Authentication Info sender="+sender_location+", recipient="+recipient_location);

      Extension_type extension = new Extension_type();

      List response_info_list = new ArrayList();
      response_info_list.add(new ResponseItem161_type(
                               new PromptId_type(PromptId_type.enummeratedprompt_CID,null),
                               new promptResponse_inline162_type(promptResponse_inline162_type.string_CID,recipient_location.getServiceForThisSymbol().getUsername())));
      response_info_list.add(new ResponseItem161_type(
                               new PromptId_type(PromptId_type.enummeratedprompt_CID,null),
                               new promptResponse_inline162_type(promptResponse_inline162_type.string_CID,recipient_location.getServiceForThisSymbol().getPassword())));

      extension.identifier = BigInteger.valueOf(1);
      extension.critical = Boolean.FALSE;
      extension.item = 
        new EXTERNAL_type (
          new int[] {1,2,840,10003,8,1},
          null, // BigInteger indirect reference
          null, // String data value descriptor
          new encoding_inline0_type(
            encoding_inline0_type.single_asn1_type_CID,
            new PromptObject_type(PromptObject_type.response_CID,response_info_list)
          )
        );
        extensions.add(extension);
    }
  }

  // Provide one entry per each transport/encoding supported by the location.
  /**
   * 
   * @param loc
   * @param type 0=Sender, 1=Recipient, 2=Transponder
   */
  private static ArrayList popLocationInfo(LocationSymbol loc, int type)  {
    return addSymbolOnly(loc);
  }

  private static ArrayList addSymbolOnly(LocationSymbol loc) {
    ArrayList retval = null;

    if ( loc != null ) {
      retval = new ArrayList();

      Service s = loc.getServiceForThisSymbol();
      cat.debug("Processing Alias...."+loc.getSymbol()+"getEncoding="+s.getEncoding());

      if ( s.getEncoding() > 0 ) {
        APDU_Delivery_Parameters_type params = new APDU_Delivery_Parameters_type();
        params.encoding = new ArrayList();
        params.encoding.add(BigInteger.valueOf(s.getEncoding()));
        params.transport = new System_Address_type(
                                 ILLStringHelper.generalString(s.getTelecomServiceIdentifier()),
                                 ILLStringHelper.generalString(s.getTelecomServiceAddress()));
        params.aliases = new ArrayList();

        params.aliases.add(new System_Id_type(
          new Person_Or_Institution_Symbol_type(
            Person_Or_Institution_Symbol_type.institution_symbol_CID,
            ILLStringHelper.generalString(loc.toString())),
          null));
        retval.add(params);
      }
    }

    return retval;
  }

  private static ArrayList addAllAliasesFor(LocationSymbol loc) {

    cat.debug("Adding APDUDeliveryInfo - location info for "+loc);

    ArrayList retval = null;

    if ( loc != null )
    {
      retval = new ArrayList();

      if ( loc.getCanonical_location().getAliases().size() == 0 ) {
        cat.warn("The list of aliases for location "+loc+" is empty. Check the LOCATION and LOCATION_SYMBOL tables");
        return retval;
      }

      // Encoding is 1..3 elements of an enumerated integer (1=edifact,2=ber/mime,3=ber) of the
      // prefered encoding at the defined location. List is ordered!
      // We should probably add a flag that indicates if the origial symbol should be included in the
      // alias list.
      cat.debug("Attempting to iterate aliases....");
      for ( Iterator i = loc.getCanonical_location().getAliases().iterator(); i.hasNext(); )
      {
        LocationSymbol ls = (LocationSymbol)i.next();

        Service s = loc.getServiceForThisSymbol();
        cat.debug("Processing Alias...."+ls.getSymbol()+"getEncoding="+s.getEncoding());
      
        if ( s.getEncoding() > 0 )  // Don't bother with loopback adapters
        {
          APDU_Delivery_Parameters_type params = new APDU_Delivery_Parameters_type();
          params.encoding = new ArrayList();
          params.encoding.add(BigInteger.valueOf(s.getEncoding()));
          params.transport = new System_Address_type(
                                 ILLStringHelper.generalString(s.getTelecomServiceIdentifier()),
                                 ILLStringHelper.generalString(s.getTelecomServiceAddress()));
          params.aliases = new ArrayList();

          params.aliases.add(new System_Id_type(
            new Person_Or_Institution_Symbol_type(
              Person_Or_Institution_Symbol_type.institution_symbol_CID,
              ILLStringHelper.generalString(ls.toString())),
            null));
          retval.add(params);
              // ILLStringHelper.generalString(ls.getSymbol())),
        }
      }
    }
    return retval;
  }

  public static void addInternalReferenceNumber(String requester_reference_authority,
                                                String requester_reference,
                                                String responder_reference_authority,
                                                String responder_reference,
                                                ArrayList extensions)
  {  
    Internal_Reference_type irt = new Internal_Reference_type();

    if ( requester_reference != null )
    {
      irt.requester_reference = new Internal_Reference_Type_type();
      irt.requester_reference.reference_authority = ILLStringHelper.generalString(requester_reference_authority);
      irt.requester_reference.internal_reference = ILLStringHelper.generalString(requester_reference);
    }
    if ( responder_reference != null )
    {
      irt.responder_reference = new Internal_Reference_Type_type();
      irt.responder_reference.reference_authority = ILLStringHelper.generalString(responder_reference_authority);
      irt.responder_reference.internal_reference = ILLStringHelper.generalString(responder_reference);
    }
  
    Extension_type et = new Extension_type(BigInteger.valueOf(1), // BigInteger identifier, 
                                           Boolean.FALSE,         // Boolean critical, 
                                           new EXTERNAL_type (
                                             new int[] {1,0,10161,13,8}, // reg.oidByName("ILL_Internal_Reference_Number"), // int[] direct reference
                                             null, // BigInteger indirect reference
                                             null, // String data value descriptor
                                             new encoding_inline0_type( encoding_inline0_type.single_asn1_type_CID, irt)));

    extensions.add( et );
  }

  public static void copyHeader(Object copy_from, Object copy_to) {
    
    try {
      java.lang.reflect.Field from_protocol_version_num_field = copy_from.getClass().getDeclaredField("protocol_version_num");
      java.lang.reflect.Field from_transaction_id_field = copy_from.getClass().getDeclaredField("transaction_id");
      java.lang.reflect.Field from_service_date_time_field = copy_from.getClass().getDeclaredField("service_date_time");
      java.lang.reflect.Field from_requester_id_field = copy_from.getClass().getDeclaredField("requester_id");
      java.lang.reflect.Field from_responder_id_field = copy_from.getClass().getDeclaredField("responder_id");

      java.lang.reflect.Field to_protocol_version_num_field = copy_to.getClass().getDeclaredField("protocol_version_num");
      java.lang.reflect.Field to_transaction_id_field = copy_to.getClass().getDeclaredField("transaction_id");
      java.lang.reflect.Field to_service_date_time_field = copy_to.getClass().getDeclaredField("service_date_time");
      java.lang.reflect.Field to_requester_id_field = copy_to.getClass().getDeclaredField("requester_id");
      java.lang.reflect.Field to_responder_id_field = copy_to.getClass().getDeclaredField("responder_id");

      to_protocol_version_num_field.set(copy_to, from_protocol_version_num_field.get(copy_from));
      to_transaction_id_field.set(copy_to, from_transaction_id_field.get(copy_from));
      to_service_date_time_field.set(copy_to, from_service_date_time_field.get(copy_from));
      to_requester_id_field.set(copy_to, from_requester_id_field.get(copy_from));
      to_responder_id_field.set(copy_to, from_responder_id_field.get(copy_from));
    }
    catch (java.lang.NoSuchFieldException nsfe) {
      nsfe.printStackTrace();
    }
    catch (java.lang.IllegalAccessException iae) {
      iae.printStackTrace();
    }
  }

  public static void addIPIGILLRequestExtenson(String client_department,
                                               String payment_method,
                                               String affiliations,
                                               String uniform_title,
                                               String dissertation,
                                               String issue,
                                               String volume,
                                               String source,
                                               ArrayList extensions) {
    Extension_type et = new Extension_type(BigInteger.valueOf(1), // BigInteger identifier,
                                           Boolean.FALSE,         // Boolean critical,
                                           new EXTERNAL_type (
                                             new int[] {1,0,10161,13,2},
                                             null, // BigInteger indirect reference
                                             null, // String data value descriptor
                                             new encoding_inline0_type( 
                                               encoding_inline0_type.single_asn1_type_CID, 
                                               new com.k_int.openrequest.isoill.gen.OCLCILLRequestExtension.OCLCILLRequestExtension_type (
                                                 ILLStringHelper.generalString(client_department),
                                                 ILLStringHelper.generalString(payment_method),
                                                 ILLStringHelper.generalString(uniform_title),
                                                 ILLStringHelper.generalString(dissertation),
                                                 ILLStringHelper.generalString(issue),
                                                 ILLStringHelper.generalString(volume),
                                                 ILLStringHelper.generalString(affiliations),
                                                 ILLStringHelper.generalString(source)))));

    extensions.add( et );

  }

}
