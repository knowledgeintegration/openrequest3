/**
 * Title:       ISOShippedMessageFactory
 * @version:    $Id: ISOShippedMessageFactory.java,v 1.4 2005/07/08 08:29:17 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import java.util.Properties;
import java.util.*;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import java.math.BigInteger;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.context.ApplicationContext;
import org.hibernate.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *  Create an ISO ILL Shipped Message.
 *  This class will construct an iso ill shipped message in a variety of different
 *  situations according to the supplied parameters.
 *  @author Ian Ibbotson
 *  @version $Id: ISOShippedMessageFactory.java,v 1.4 2005/07/08 08:29:17 ibbo Exp $
 */
public class ISOShippedMessageFactory {

 private static Log log = LogFactory.getLog(ISOShippedMessageFactory.class.getName());

  /**
   *  create a Shipped message.
   *
   *  Requires: An ILL Transaction in a state for which SHPreq is a valid action
   *
   *  Provides: A completed ILL Shipped message with data derived as follows:
   *            N.B where a member could be set from the transaction store, speak with k-int
   *            about defaulting that member in, it's better than starting to track transaction data
   *            in databases outside the transaction store.
   *
   *  element name/type                        M/O	Source
   *  retval : ILL_APDU_type (Choice of pdu)	M       Filled in by factory
   *    .which : int				M       Filled in by factory
   *    .o : Shipped_type			M       Filled in by factory
   *      .protocol_version_num
   *      .transaction_id			M	Constructed field, populated by this method
   *        .initial_requester_id		M	
   *          .person_or_institution_symbol	O	Set by factory from database
   *          .name_of_person_or_institution    O	Not set
   *        .tgq				M	Set by factory from database
   *        .tq					M	Set by factory from database
   *        .stq				O	Set by factory from database
   *      .service_date_time			M	Constructed by factory
   *        .date_time_of_this_service          M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *        .date_time_of_original_service      M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *      .requester_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *      .responder_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *
   *                   -- End of common elements. Shipped specific follow --
   *
   *      .responder_address			O       
   *        .telecom_service_identifier         O
   *        .telecom_service_address            O
   *      .intermediary_id			O       
   *        .person_or_institution_symbol       O       
   *        .person_or_institution_name         O       
   *      .supplier_id				O       Not currently defaulted
   *        .person_or_institution_symbol       O       
   *        .person_or_institution_name         O       
   *      .client_id				O       
   *        .client_name                        O
   *        .client_status                      O
   *        .client_identifier                  O
   *      .transaction_type			M       Defaulted to BigInteger.valueOf(1)
   *                                                    1=Simple, 2=Chained, 3=Partitioned
   *      .supplemental_item_description	O       
   *      .shipped_service_type			M       Set according to long value passed in
   *                                                    1=Loan, 2=Copy non returnable
   *      .responder_optional_messages		O       Not currently defaulted
   *        .can_send_SHIPPED                   M       *should* be defaulted from defaults mechanism
   *        .can_send_CHECKED_IN                M       *should* be defaulted from defaults mechanism
   *        .responder_RECEIVED                 M       *should* be defaulted from defaults mechanism
   *        .responder_RETURNED                 M       *should* be defaulted from defaults mechanism
   *      .supply_details			M       New structured type created by factory
   *        .date_shipped			O 	Set according to date_shipped factory param
   *        .date_due				O	Only set if date_due and renewable not null
   *          .date_due_field			M	set from date_due param
   *          .renewable			M	set from renewable param
   *        .chargeable_units			O	not currently defaulted
   *        .cost				O	not currently defaulted
   *        .shipped_conditions			O	not currently defaulted
   *        .shipped_via			O	not currently defaulted
   *        { choice of eletronic or physical delivery }
   *          ...TODO...
   *        .insured_for			O	not currently defaulted
   *        .return_insurance_require		O	not currently defaulted
   *        .no_of_units_per_medium		O	not currently defaulted
   *      .return_to_address			O       Not currently defaulted
   *        .name_of_person_or_institution      O
   *        .extended_postal_delivery_address
   *        .street_and_number
   *        .post_office_box
   *        .city
   *        .region
   *        .country
   *        .postal_code
   *      .responder_note			O       Not currently defaulted
   *      .shipped_extensions			O       Not currently defaulted
   *        { Vector of extensions... }
   *
   *  Ends description of how shipped PDU is populated by this factory method
   *
   *  @author Ian Ibbotson
   *  @param database_session The database session we will use to access the database
   *  @param request_id request id (internal number) of the transaction we are dealing with
   *  @param shipped_service_type 
   *  @param date_shipped On what date was the item shipped
   *  @param date_due When is the item due
   *  @param renewable Is the load renewable
   *  @param chargeable_units What are the chargable units
   *  @param cost What is the cost
   *  @param conditions What are the loan conditions
   *  @param extens And array of extension factories that will set up a shipped extension
   *                for each factory in the array.
   * 
   *  @return An ILL_APDU containing a Shipped message
   */
  public static ILL_APDU_type create(Session database_session, 
                                     LocationSymbol sender,
                                     LocationSymbol recipient,
                                     ILLTransaction transaction,
                                     long shipped_service_type,
                                     Date date_shipped,
                                     Date date_due,
                                     Boolean renewable,
                                     Long chargeable_units,
                                     String total_cost_currency,
                                     String total_cost_amount,
                                     Long conditions,
                                     String name,
                                     String extendedPostal,
                                     String streetAndNumber,
                                     String pobox,
                                     String city,
                                     String region,
                                     String country,
                                     String postcode,
                                     String responder_note,
                                     ISOExtensionFactory[] extens,
                                     ApplicationContext app_context) throws java.sql.SQLException, org.hibernate.HibernateException
  {
    Shipped_type shipped = new Shipped_type();
    ILL_APDU_type retval = new ILL_APDU_type( ILL_APDU_type.shipped_var_CID, shipped);

    // Fill out protocol version num,  transaction id
    ISOHeaderDetailsHelper.fillOutHeader(database_session, transaction, shipped);

    Date_Due_type dd = null;
    if ( date_due != null ) {
      log.debug("Creating new shiped message with due_date "+date_due+" renewable="+renewable);
      String date_due_str = Constants.formatDate(date_due);
      dd = new Date_Due_type(date_due_str, renewable != null ? renewable : Boolean.TRUE);
    }
    else {
      log.warn("Date Due and or renewable not set = no due date setting in shipped message");
    }

    if ( date_shipped == null )
      date_shipped = new Date(System.currentTimeMillis());

    String date_shipped_string = Constants.formatDate(date_shipped);

    Amount_type amount = null;
    if ( ( total_cost_currency != null ) && ( total_cost_amount != null ) ) {
      amount = new Amount_type(total_cost_currency,total_cost_amount);
    }
    BigInteger cu = null;
    if ( chargeable_units != null )
      cu = BigInteger.valueOf(chargeable_units.intValue());
    if ( responder_note != null )
      shipped.responder_note = ILLStringHelper.generalString(responder_note);
    BigInteger conds = null;
    if ( conditions != null )
      conds = BigInteger.valueOf(conditions.intValue());
    // TODO Get request header and take tran type from there
    shipped.transaction_type = BigInteger.valueOf(1);
    shipped.shipped_service_type = BigInteger.valueOf(shipped_service_type);
    shipped.supply_details = new Supply_Details_type (
                                date_shipped_string,
                                dd, // due date
                                cu,
                                amount, // Amount_type cost
                                conds,
                                null, // shipped via
                                null, // insured for
                                null, // return insurance required
                                null);// num units per med

    if ( name != null || extendedPostal != null || streetAndNumber != null || city != null || region != null || country != null || postcode != null ) {
      shipped.return_to_address = new Postal_Address_type(
        name != null ? new Name_Of_Person_Or_Institution_type(Name_Of_Person_Or_Institution_type.name_of_person_CID,ILLStringHelper.generalString(name)) : null,
        ILLStringHelper.generalString(extendedPostal),
        ILLStringHelper.generalString(streetAndNumber),
        ILLStringHelper.generalString(pobox),
        ILLStringHelper.generalString(city),
        ILLStringHelper.generalString(region),
        ILLStringHelper.generalString(country),
        ILLStringHelper.generalString(postcode)
      );
    }
   
    //
    // Construct any extensions
    //
    ArrayList extensions = new ArrayList();
    shipped.shipped_extensions = extensions;

    if ( ( extens != null ) && ( extens.length > 0 ) ) {
      for ( int i=0; i<extens.length; i++ ) {
        // Process exten i and add to vector
      }

    }

    // Tag on APDU Delivery info
    ISOHeaderDetailsHelper.addAPDUDeliveryInfo(database_session, sender, null, recipient, extensions, app_context);
    // ISOHeaderDetailsHelper.addAuthenticationInfo(database_session, sender, null, recipient, extensions);
    return retval;
  }
}
