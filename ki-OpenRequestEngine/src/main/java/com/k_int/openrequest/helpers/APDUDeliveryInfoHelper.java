/**
 * Title:       APDUDeliveryInfoHelper
 * @version:    $Id: APDUDeliveryInfoHelper.java,v 1.3 2005/07/07 17:57:19 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.*;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.ILL_APDU_Delivery_Info.*;
import com.k_int.openrequest.isoill.gen.Internal_Reference_Number.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import java.math.BigInteger;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import org.springframework.context.ApplicationContext;
import org.hibernate.*;

public class APDUDeliveryInfoHelper {

  private static Log log = LogFactory.getLog(APDUDeliveryInfoHelper.class.getName());
                                                                                                                                                                            
  private static final String[] extension_field_names = {
        "iLL_request_extensions",
        "forward_notification_extensions",
        "shipped_extensions",
        "ill_answer_extensions",
        "conditional_reply_extensions",
        "cancel_extensions",
        "cancel_reply_extensions",
        "received_extensions",
        "recall_extensions",
        "returned_extensions",
        "checked_in_extensions",
        "overdue_extensions",
        "renew_extensions",
        "renew_answer_extensions",
        "lost_extensions",
        "damaged_extensions",
        "message_extensions",
        "status_query_extensions",
        "status_or_error_report_extensions",
        "expired_extensions" };
                                                                                                                                                                            

  public static void addMissingAuthenticationInfo(ILLMessageEnvelope msg, Session session, OIDRegister reg) throws HelperException {
  }

  public static void addMissingDeliveryInfo(ILLMessageEnvelope msg, 
                                            Session session, 
                                            OIDRegister reg,
                                            ApplicationContext app_ctx) throws HelperException {
    try {
      log.debug("HandleGenericRequest tagging missing APDU Delivery Info onto message");
      ILL_APDU_type pdu = msg.getPDU();

      ArrayList extensions = getOrCreateExtensions(pdu);

      String sender_symbol =  msg.getSendingLocationSymbol();
      String recipient_symbol =  msg.getDestinationLocationSymbol();

      if ( ( extensions.size() == 0 ) || 
           ( extractDeliveryInfoIfPresent(extensions, reg) == null ) && 
           ( recipient_symbol != null ) ) {
        log.debug("Adding APDU Delivery Info...");
        LocationSymbol sender = DBHelper.lookupLocation(session, sender_symbol);
        LocationSymbol recipient = DBHelper.lookupLocation(session, recipient_symbol);
        ISOHeaderDetailsHelper.addAPDUDeliveryInfo(session,
                                                   sender,
                                                   null,
                                                   recipient,
                                                   extensions,
                                                   app_ctx);
      }
    }
    catch ( java.sql.SQLException sqle ) {
      throw new HelperException("Problem adding APDU Delivery Info",sqle);
    }
    catch ( org.hibernate.HibernateException he ) {
      throw new HelperException("Problem adding APDU Delivery Info",he);
    }
  }

  private static ArrayList getOrCreateExtensions(ILL_APDU_type pdu) throws HelperException {
    ArrayList extensions = null;
    try {
      String field_name = extension_field_names[pdu.which];

      // Use reflection to get hold of that field of pdu.o
      java.lang.reflect.Field extensions_field = pdu.o.getClass().getDeclaredField(field_name);
      extensions = (ArrayList) extensions_field.get(pdu.o);
      if ( extensions == null ) {
        extensions = new ArrayList();
        extensions_field.set(pdu.o, extensions);
      }
    }
    catch ( Exception e ) {
      throw new HelperException("Unable to locate extensions vector",e);
    }

    return extensions;
  }

  private static APDU_Delivery_Info_type extractDeliveryInfoIfPresent(ArrayList extensions, OIDRegister reg) {
    log.debug("Looking for APDUDeliveryInfo extension");
    APDU_Delivery_Info_type result = null;
    for ( Iterator i = extensions.iterator(); i.hasNext(); )
    {
      log.debug("Checking extension......");
      Extension_type et = (Extension_type)i.next();
      // we have et.identifier, et.critical, et.item (which is the any containing an External
      if ( ( et.identifier != null ) && ( et.identifier.equals(BigInteger.valueOf(1))) )
      {
        if ( et.item != null )
        {
          EXTERNAL_type ext = (EXTERNAL_type) et.item;
          int[] oid = ext.direct_reference;
          if ( oid != null )
          {
            OIDRegisterEntry ent = reg.lookupByOID(oid);
            if ( ent != null ) {
              if ( ent.getName().equals("ILL_APDU_Delivery_Info") ) {
                if ( ( ext.encoding != null ) &&
                     ( ext.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {
                  log.debug("Processing APDU Delivery Info");
                  result = (APDU_Delivery_Info_type) ext.encoding.o;
                }
              }
            }
          }
        }
      }
    }
    return result;
  }

}
