/**
 * Title:       DBHelper
 * @version:    $Id: ServiceLocationException.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

public class ServiceLocationException extends Exception
{
  public ServiceLocationException(String s)
  {
    super(s);
  }
}
