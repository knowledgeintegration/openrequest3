/**
 * Title:       TransactionGroupFactory
 * @version:    $Id: TransactionGroupFactory.java,v 1.3 2005/07/09 09:19:51 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import java.util.Properties;
import java.util.Vector;
import java.util.Date;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import java.math.BigInteger;
import org.w3c.dom.*;
import org.w3c.dom.traversal.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.xpath.*;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.Serializer;
import org.apache.xml.serialize.SerializerFactory;
import org.apache.xml.serialize.XMLSerializer;
import java.util.Hashtable;
import java.util.Map;
import java.io.StringWriter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;


public class TransactionGroupFactory
{
  public static Log log = LogFactory.getLog(TransactionGroupFactory.class);


  /**
   * Create a transaction group that will act as a header record for a request
   * which may be sent to multiple responders.
   */
  public static ILLTransactionGroup create(Session hibernate_session,
                                           Location location,
                                           String initial_requester_symbol,
                                           String requester_symbol,
                                           Document citation_doc,
                                           String role,
                                           Hashtable additional_attrs) throws java.sql.SQLException,
                                                                              org.hibernate.HibernateException
  {
    log.debug("create(...)");

    String xml_str = dumpRequestDoc(citation_doc);
    log.debug(xml_str);

    ILLTransactionGroup retval = null;
    String tgq = "OR:"+Constants.getNewUniqueId();

    log.debug("create transaction group. New tgq will be "+tgq);
    retval = new ILLTransactionGroup(location, initial_requester_symbol, tgq, role);

    log.debug("Setting item details");
    retval.setRequiredItemDetails( new RequiredItemDetails() );
    retval.setCurrentRotaPosition(-1);
    log.debug("Setting requester symbol "+requester_symbol);
    retval.setRequesterSymbol(requester_symbol);
    log.debug("Setting active state model");
    retval.setActiveStateModel(DBHelper.lookupStateModel(hibernate_session,"REQ/TG"));

    log.debug("Setting item details elements");
    // Extract the generic document elements from the dom tree
    retval.getRequiredItemDetails().setAuthor(extractItemDetail(citation_doc, "Author"));
    retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "Title"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "UniformTitle"));
    retval.getRequiredItemDetails().setSubtitle(extractItemDetail(citation_doc, "SubTitle"));
    retval.getRequiredItemDetails().setSponsoringBody(extractItemDetail(citation_doc, "SponsoringBody"));
    retval.getRequiredItemDetails().setPlaceOfPublication(extractItemDetail(citation_doc, "PlaceOfPublication"));
    retval.getRequiredItemDetails().setPublisher(extractItemDetail(citation_doc, "Publisher"));
    retval.getRequiredItemDetails().setPublicationDate(extractItemDetail(citation_doc, "PublicationDate"));
    retval.getRequiredItemDetails().setLanguage(extractItemDetail(citation_doc, "Language"));
    retval.getRequiredItemDetails().setSeriesTitleNumber(extractItemDetail(citation_doc, "SeriesTitleNumber"));
    retval.getRequiredItemDetails().setVolume(extractItemDetail(citation_doc, "Volume"));
    retval.getRequiredItemDetails().setIssue(extractItemDetail(citation_doc, "Issue"));
    retval.getRequiredItemDetails().setEdition(extractItemDetail(citation_doc, "Edition"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "ThisEditionOnly"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "ReportDocumentNumber"));
    retval.getRequiredItemDetails().setAuthorOfArticle(extractItemDetail(citation_doc, "ArticleAuthor"));
    retval.getRequiredItemDetails().setTitleOfArticle(extractItemDetail(citation_doc, "ArticleTitle"));
    retval.getRequiredItemDetails().setPagination(extractItemDetail(citation_doc, "Pagination"));
    retval.getRequiredItemDetails().setPublicationDateOfComponent(extractItemDetail(citation_doc, "ArticlePublicationDate"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "DissertationThesisInformation"));
    retval.getRequiredItemDetails().setCallNumber(extractItemDetail(citation_doc, "CallNumber"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "NationalBibliographyId/NationalBibliographyAuthority"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "NationalBibliographyId/NationalBibliographyNumber"));
    retval.getRequiredItemDetails().setISBN(extractItemDetail(citation_doc, "ISBN"));
    retval.getRequiredItemDetails().setISSN(extractItemDetail(citation_doc, "ISSN"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "SystemId/SystemAuthority"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "SystemId/SystemNumber"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "BibliographicSource"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "UniqueItemId"));
    retval.getRequiredItemDetails().setVerificationReferenceSource(extractItemDetail(citation_doc, "VerificationReferenceSource"));
    retval.getRequiredItemDetails().setAdditionalNoLetters(extractItemDetail(citation_doc, "AdditionalNumberLetters"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "RequestedMaterialFormat"));
    // retval.getRequiredItemDetails().setTitle(extractItemDetail(citation_doc, "RequestedMaterialLanguage"));
    // retval.getRequiredItemDetails().setSupplementalItemDescription(extractItemDetail(citation_doc, "SupplementalItemDescription"));

    log.debug("Looking for need before date");
    String need_by_comment = extract(citation_doc, "//SearchType/NeedBeforeDate");
    retval.setNeedByComment(need_by_comment);

    // If we can parse need_by_comment into a date, set need_by_date
    if ( need_by_comment != null )
    {
      try
      {
        Date need_by_date =  Constants.parseIsoDate(need_by_comment);
        if ( need_by_date != null )
          retval.setNeedByDate(need_by_date);
      }
      catch ( java.text.ParseException pe )
      {
        pe.printStackTrace();
      }
    }

    try
    {
      // Set up the delivery address information
      Node delivery_address =  XPathAPI.selectSingleNode(citation_doc.getDocumentElement(),"//RequestSubmissionMessage/Delivery/AddressInfo");
      log.debug("set current delivery address");
      retval.setCurrentDeliveryAddress(storeAddress(delivery_address, hibernate_session));
    }
    catch ( Exception e )
    {
      log.warn("Problem with address", e);
    }

    try
    {
      // Set up the billing address information
      Node billing_address =  XPathAPI.selectSingleNode(citation_doc.getDocumentElement(),"//RequestSubmissionMessage/Billing");
      log.debug("set current billing address");
      retval.setCurrentBillingAddress(storeAddress(billing_address, hibernate_session));
    }
    catch ( Exception e )
    {
      log.warn("Problem with address", e);
    }

    // Copy all additional attrs to the database
    if ( additional_attrs != null )
    {
      log.debug("Using putAll to copy additional attrs to database object");
      retval.getCustomAttrs().putAll(additional_attrs);
    }

    log.debug("Populate rota");
    // Extract the rota from the dom tree
    populateRota(citation_doc, retval);

    log.debug("SAVING.....");
    hibernate_session.save(retval);
    hibernate_session.flush();
    return retval;
  }

  public static void populateRota(Document d, ILLTransactionGroup tg)
  {
    log.debug("Populating rota.....");
    try
    {
      String the_path = "//RequestSubmissionMessage/PossibleSuppliers/PossibleSupplier/LibraryCodeOrSymbol/text()";
      NodeIterator i = XPathAPI.selectNodeIterator(d.getDocumentElement(),the_path);
      Node n = i.nextNode();
      while ( n != null )
      {
        String symbol = n.getNodeValue().toString();
        log.debug("Processing rota entry : "+symbol);
        tg.getRota().add(new RotaEntry(false, 
                                       Person_Or_Institution_Symbol_type.institution_symbol_CID, 
                                       symbol,
                                       null,
                                       0,      // Name type
                                       (String)null,   // name
                                       (String)null,   // Account number
                                       (String)null,   // Service Identifier
                                       (String)null)); // Service Address
        n=i.nextNode();
      }
    }
    catch ( javax.xml.transform.TransformerException te )
    {
      log.warn("Problem tring to extract XML doc field",te);
    }
  }

  public static String extractItemDetail(Document doc, String field)
  {
    try
    {
      String the_path = "//RequestSubmissionMessage/ItemInfo/"+field+"/text()";
      Node n = XPathAPI.selectSingleNode(doc.getDocumentElement(),the_path);

      if ( n != null )
      {
        log.debug("node found: "+n.getNodeValue());
        return n.getNodeValue().toString();
      }
    }
    catch ( javax.xml.transform.TransformerException te )
    {
      log.warn("Problem tring to extract XML doc field",te);
    }

    return null;
  }

  private static String dumpRequestDoc(Document d)
  {
   String result = null;

   try
    {
      OutputFormat format  = new OutputFormat( d );
      StringWriter stringOut = new StringWriter();
      XMLSerializer serial = new XMLSerializer( stringOut, format );
      serial.asDOMSerializer();
      serial.serialize( d.getDocumentElement() );
      result = stringOut.toString();
    }
    catch ( Exception e )
    {
      log.warn(e.toString(),e);
    }
    return result;
  }


  public static ISO10161Address storeAddress(Node address_node, Session sess) throws Exception
  {
    ISO10161Address address = null;

    log.debug("Store new address.....");

    if ( address_node != null )
    {
      address = new ISO10161Address();

      Node physical_address_info = XPathAPI.selectSingleNode(address_node,"PhysicalAddress");

      if ( physical_address_info != null )
      {
        //
        // Name of person or name of institution
        //
        Node person_name_node = XPathAPI.selectSingleNode( physical_address_info, "PersonName/text()" );
        if ( person_name_node != null )
        {
          address.setPostal_name_type(1); // TODO: Check
          if ( person_name_node.getNodeValue() != null )
            address.setPostal_name(person_name_node.getNodeValue().toString());
        }

        //
        // Name of person or name of institution
        //
        Node inst_name_node = XPathAPI.selectSingleNode( physical_address_info, "InstitutionName/text()" );
        if ( inst_name_node != null )
        {
          address.setPostal_name_type(2); // TODO: Check 
          if ( inst_name_node.getNodeValue() != null )
            address.setPostal_name(inst_name_node.getNodeValue().toString());
        }

        address.setPostal_extended_address(extract(physical_address_info, "ExtendedAddress"));
        address.setPostal_street_and_number(extract(physical_address_info, "StreetAndNumber"));
        address.setPostal_po_box(extract(physical_address_info, "PoBox"));
        address.setPostal_city(extract(physical_address_info, "City"));
        address.setPostal_region(extract(physical_address_info, "Region"));
        address.setPostal_country(extract(physical_address_info, "Country"));
        address.setPostal_postcode(extract(physical_address_info, "Postcode"));

      }

      // if (addr.electronic_address != null)
      // {
      //   if ( addr.electronic_address.telecom_service_identifier != null )
      //     address.setElectronic_service_identifier(
      //            addr.electronic_address.telecom_service_identifier.o.toString());
      //   if ( addr.electronic_address.telecom_service_address != null )
      //     address.setElectronic_service_address(addr.electronic_address.telecom_service_address.o.toString());
      // }
    }

    return address;
  }

  private static String extract(Node parent_node, String child_name)
  {
    try
    {
      Node child_node = XPathAPI.selectSingleNode(parent_node,child_name+"/text()");
      if ( child_node != null )
      {
        Object o = child_node.getNodeValue();
        if ( o != null )
        {
          return o.toString();
        }
        else
        {
          return null;
        }
      }
      else
      {
        return null;
      }
    }
    catch ( Exception e )
    {
      log.warn("Problem extracting field",e);
      return null;
    }
  }

}
