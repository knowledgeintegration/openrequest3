/**
 * Title:       Constants
 * @version:    $Id: APDUDeliveryInfoStorageHelper.java,v 1.3 2005/05/14 22:39:06 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */

package com.k_int.openrequest.helpers;

import java.io.Serializable;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.util.*;
import org.hibernate.*;
import org.hibernate.type.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.Internal_Reference_Number.*;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import com.k_int.openrequest.isoill.gen.ILL_APDU_Delivery_Info.*;
import java.math.BigInteger;
import org.springframework.context.ApplicationContext;
import org.apache.commons.logging.*;

public class APDUDeliveryInfoStorageHelper {

  private static Log log = LogFactory.getLog(APDUDeliveryInfoStorageHelper.class);

  public static void storeAPDUDeliveryInfo(Session session, 
                                           APDU_Delivery_Info_type delivery_info) throws java.sql.SQLException, org.hibernate.HibernateException {
    storeAPDUDeliveryInfo(session,delivery_info,null,null);
  }

  public static void storeAPDUDeliveryInfo(Session session, 
                                           APDU_Delivery_Info_type delivery_info,
                                           String sender_symbol,
                                           String recipient_symbol) throws java.sql.SQLException, org.hibernate.HibernateException {
    if ( delivery_info.sender_info != null ) {
      log.debug("Number of sender delivery info entries: "+delivery_info.sender_info.size());
      if ( delivery_info.sender_info.size() > 0 ) {
        if ( sender_symbol == null ) {
          sender_symbol = getFirstSymbol((APDU_Delivery_Parameters_type)(delivery_info.sender_info.get(0)));
        }
        checkPartner(session, 
                   (APDU_Delivery_Parameters_type)(delivery_info.sender_info.get(0)),
                   sender_symbol,
                   null);
      }
    }
    else {
      log.debug("sender info is null");
    }

    if ( delivery_info.recipient_info != null ) {
      log.debug("Number of recipient delivery info entries: "+delivery_info.recipient_info.size());
      if ( delivery_info.recipient_info.size() > 0 ) {
        if ( recipient_symbol == null ) {
          recipient_symbol = getFirstSymbol((APDU_Delivery_Parameters_type)(delivery_info.recipient_info.get(0)));
        }
        checkPartner(session, 
                   (APDU_Delivery_Parameters_type)(delivery_info.recipient_info.get(0)),
                   recipient_symbol,
                   null);
      }
    }
    else {
      log.debug("recipient info is null");
    }
  }

  public static String getFirstSymbol(APDU_Delivery_Parameters_type params) {
    ArrayList aliases = params.aliases;
    String result = null;
    if ( aliases != null ) {
      for ( Iterator alias_iterator = aliases.iterator(); ((alias_iterator.hasNext()) && (result==null)); ) {
        System_Id_type system_id = (System_Id_type) alias_iterator.next();
        Person_Or_Institution_Symbol_type person_or_institution_symbol = system_id.person_or_institution_symbol;
        Name_Of_Person_Or_Institution_type name_of_person_or_institution = system_id.name_of_person_or_institution;
  
        if ( ( person_or_institution_symbol != null ) && ( person_or_institution_symbol.which == Person_Or_Institution_Symbol_type.institution_symbol_CID ) ) {
          result = ILLStringHelper.extract((ILL_String_type)(person_or_institution_symbol.o));
        }
        else {
          log.debug("Looks like we have a person symbol instead of an institution");
        }
      }
    }

    return result;
  }

  public static void checkPartner(Session session,
                                   APDU_Delivery_Parameters_type params,
                                   String partner_symbol,
                                   String partner_name) throws java.sql.SQLException,
                                                               org.hibernate.HibernateException {

    log.debug("checkPartner(session, params,"+partner_symbol+")");

    System_Address_type transport = params.transport;
    String telecom_service_identifier = ILLStringHelper.extract(transport.telecom_service_identifier);
    String telecom_service_address = ILLStringHelper.extract(transport.telecom_service_address);

    ArrayList encodings_allowed_at_this_address = params.encoding;
    Service s = null;

    if ( encodings_allowed_at_this_address.contains( BigInteger.valueOf(2) ) ) {
        // BER in Mime
        log.debug("Create a new MIME address");
        s = DBHelper.lookupOrCreateService(session,
                                           telecom_service_identifier,
                                           telecom_service_address,
                                           "Mime System Address "+telecom_service_address,
                                           "ILL/MIME",
                                           "MIME",
                                           (long)0,
                                           2);
    }
    else if (  ( encodings_allowed_at_this_address.contains( BigInteger.valueOf(3) ) ) ) {
        // tcp
        log.debug("Create a new TCP address");
        s = DBHelper.lookupOrCreateService(session,
                                           telecom_service_identifier,
                                           telecom_service_address,
                                           "TCP System Address "+telecom_service_address,
                                           "ILL/DUPLEX",
                                           "DUPLEXTCP",
                                           (long)0,
                                           3);
    }
                                                                                                                                          
    if ( s != null ) {
      log.debug("Identified a service for message sender");
    }
    else {
      log.warn("Serious problem");
    }
    
                                                                                                                                          
    log.debug("Attempting to look up location from aliases");
    // 1. Can we manage to identify the canonical location base on any of the aliases?
    Location l = APDUDeliveryInfoStorageHelper.resolveFromAliases(session,params.aliases);

    if ( l == null ) {
      // Create a new location and import all aliases.
      log.debug("No location found, Creating a new location");
      l = new Location();
      l.setName(partner_name);
      l.setDefaultSymbol(partner_symbol);
      session.save(l);
    }

    // Add in an alias for the sending location itself (Some implementations don't list the
    // sending symbol in the aliases list
    log.debug("Adding an alias for the partner site");
    APDUDeliveryInfoStorageHelper.addAlias(session, l, partner_symbol, partner_name, s);

    log.debug("Importing any aliases from APDU Delivery Info");
    // Add in all aliases from APDU delivery info
    APDUDeliveryInfoStorageHelper.addMissingAliases(session, l, params.aliases, s);

  }

  public static Location resolveFromAliases(Session session, ArrayList aliases) throws java.sql.SQLException, org.hibernate.HibernateException {

    Location result = null;
    LocationSymbol identified_location = null;

    if ( aliases != null ) {
      for ( Iterator alias_iterator = aliases.iterator(); ((alias_iterator.hasNext()) && (identified_location==null)); ) {
        System_Id_type system_id = (System_Id_type) alias_iterator.next();
        Person_Or_Institution_Symbol_type person_or_institution_symbol = system_id.person_or_institution_symbol;
        Name_Of_Person_Or_Institution_type name_of_person_or_institution = system_id.name_of_person_or_institution;
  
        if ( ( person_or_institution_symbol != null ) &&
             ( person_or_institution_symbol.which == Person_Or_Institution_Symbol_type.institution_symbol_CID ) ) {
          if ( result == null ) {
            // See if we know anything about this symbol.
            String symbol_to_check = ILLStringHelper.extract((ILL_String_type)(person_or_institution_symbol.o));
            log.debug("resolve from aliases checking "+symbol_to_check);
            identified_location = DBHelper.lookupLocation(session, symbol_to_check);
          }
          log.debug("Done processing alias: "+ILLStringHelper.extract((ILL_String_type)(person_or_institution_symbol.o)));
        }
        else {
          log.debug("Looks like we have a person symbol instead of an institution");
        }
      }

      if ( identified_location != null )
        result = identified_location.getCanonical_location();
    }

    log.debug("resolve from aliases returns "+result);

    return result;
  }

  public static void addMissingAliases(Session session, Location l, ArrayList aliases, Service s) throws java.sql.SQLException, org.hibernate.HibernateException{
    if ( aliases != null ) {
      for ( Iterator alias_iterator = aliases.iterator(); alias_iterator.hasNext(); ) {
        log.debug("Processing alias...");
        System_Id_type system_id = (System_Id_type) alias_iterator.next();
        Person_Or_Institution_Symbol_type person_or_institution_symbol = system_id.person_or_institution_symbol;
        Name_Of_Person_Or_Institution_type name_of_person_or_institution = system_id.name_of_person_or_institution;

        if ( ( person_or_institution_symbol != null ) &&
             ( person_or_institution_symbol.which == Person_Or_Institution_Symbol_type.institution_symbol_CID ) ) {

            String symbol = ILLStringHelper.extract((ILL_String_type)(person_or_institution_symbol.o));
            String name = ILLStringHelper.extract((ILL_String_type)(person_or_institution_symbol.o));

            log.debug("Adding alias "+symbol+", "+name);
            addAlias(session, l, symbol, name, s);

            if ( l.getName() == null ) {
              l.setName(name);
              session.update(l);
            }

            if ( l.getDefaultSymbol() == null ) {
              l.setDefaultSymbol(symbol);
              session.update(symbol);
            }

        }

        log.debug("Done processing alias: "+ILLStringHelper.extract((ILL_String_type)(person_or_institution_symbol.o)));
      }
    }
    else {
      log.debug("No alaises");
    }
  }

  public static LocationSymbol addAlias(Session session,
                                        Location location,
                                        String symbol,
                                        String name,
                                        Service s) throws java.sql.SQLException, org.hibernate.HibernateException {

    LocationSymbol result = null;
    if ( symbol != null ) {
      result = DBHelper.lookupLocation(session, symbol);
      if ( result == null ) {
        int pos = symbol.indexOf(":");
        String alias_auth_part = symbol.substring(0,pos);
        String alias_symbol_part = symbol.substring(pos+1);
        NamingAuthority auth = DBHelper.lookupOrCreateAuthority(session,alias_auth_part,null);
        log.debug("Create new symbol "+symbol +" - "+name);
        result = new LocationSymbol(alias_symbol_part, auth, location, s, name);
        session.save(result);
      }
      else {
        log.debug("Alias already present with id "+result.getId());
      }
    }
    else {
      log.warn("Asked to add null symbol for location "+location);
    }
    return result;
  }
}
