/**
 * Title:       ISORequestFactory
 * @version:    $Id: SystemIdHelper.java,v 1.1 2005/06/10 15:38:04 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.Properties;
import java.util.Vector;
import java.util.Date;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class SystemIdHelper {

  public static System_Id_type toIsoSystemId(SystemId system_id) {
    System_Id_type result = new System_Id_type();
    if ( system_id.getSymbol() != null ) {
      result.person_or_institution_symbol = new Person_Or_Institution_Symbol_type();
      result.person_or_institution_symbol.o = ILLStringHelper.generalString(system_id.getSymbol().getSymbol());
      if ( system_id.getSymbol() instanceof PersonSymbol ) {
        result.person_or_institution_symbol.which = Person_Or_Institution_Symbol_type.person_symbol_CID;
      }
      else {
        result.person_or_institution_symbol.which = Person_Or_Institution_Symbol_type.institution_symbol_CID;
      }
    }

    if ( system_id.getName() != null ) {
      result.name_of_person_or_institution = new Name_Of_Person_Or_Institution_type();
      result.name_of_person_or_institution.o = ILLStringHelper.generalString(system_id.getName().getName());
      if ( system_id.getName() instanceof PersonName ) {
        result.name_of_person_or_institution.which = Name_Of_Person_Or_Institution_type.name_of_person_CID;
      }
      else {
        result.name_of_person_or_institution.which = Name_Of_Person_Or_Institution_type.name_of_institution_CID;
      }
    }

    return result;
  }
}
