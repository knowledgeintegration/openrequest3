/**
 * Title:       ISOShippedMessageFactory
 * @version:    $Id: ISOForwardNotificationMessageFactory.java,v 1.2 2005/05/13 17:24:40 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.Properties;
import java.util.ArrayList;
import java.util.Date;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 *  Create an ISO ILL Shipped Message.
 *  This class will construct an iso ill shipped message in a variety of different
 *  situations according to the supplied parameters.
 *  @author Ian Ibbotson
 *  @version $Id: ISOForwardNotificationMessageFactory.java,v 1.2 2005/05/13 17:24:40 ibbo Exp $
 */
public class ISOForwardNotificationMessageFactory {

  /**
   *  create a Shipped message.
   *
   *  Requires: An ILL Transaction in a state for which FWDreq is a valid action
   *
   *  Provides: A completed ILL Forward Notificatoin message with data derived as follows:
   *            N.B where a member could be set from the transaction store, speak with k-int
   *            about defaulting that member in, it's better than starting to track transaction data
   *            in databases outside the transaction store.
   *
   *  element name/type                        M/O	Source
   *  retval : ILL_APDU_type (Choice of pdu)	M       Filled in by factory
   *    .which : int				M       Filled in by factory
   *    .o : Shipped_type			M       Filled in by factory
   *      .protocol_version_num
   *      .transaction_id			M	Constructed field, populated by this method
   *        .initial_requester_id		M	
   *          .person_or_institution_symbol	O	Set by factory from database
   *          .name_of_person_or_institution    O	Not set
   *        .tgq				M	Set by factory from database
   *        .tq					M	Set by factory from database
   *        .stq				O	Set by factory from database
   *      .service_date_time			M	Constructed by factory
   *        .date_time_of_this_service          M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *        .date_time_of_original_service      M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *      .requester_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *      .responder_id				M	Set from new_responder param of create method
   *        .person_or_institution_symbol       O       Set from new_responder param of create method
   *        .person_or_institution_name         O       Set from new_responder param of create method
   *
   *                   -- End of common elements. Shipped specific follow --
   *
   *      .responder_address			O       Set from new_responder param of create method
   *        .telecom_service_identifier         O	Set from new_responder param of create method
   *        .telecom_service_address            O	Set from new_responder param of create method
   *      .intermediary_id			O       Set to sending location (original resp id)
   *        .person_or_institution_symbol       O       As above
   *        .person_or_institution_name         O       As above
   *      .notification_note			O       Not set
   *      .forward_notification_extensions      O       Not set
   *  Ends description of how forward notification PDU is populated by this factory method
   *
   *  @author Ian Ibbotson
   *  @param database_session The database session we will use to access the database
   *  @param request_id request id (internal number) of the transaction we are dealing with
   *  @param extens An array of extension factories that will set up a shipped extension
   *                for each factory in the array.
   * 
   *  @return An ILL_APDU containing a Shipped message
   */
  public static ILL_APDU_type create(Session database_session, 
                                     ILLTransaction transaction,
                                     LocationSymbol new_responder,
                                     String notification_note,
                                     ISOExtensionFactory[] extens) throws java.sql.SQLException,
                                                             org.hibernate.HibernateException
  {
    Forward_Notification_type fwd_notification = new Forward_Notification_type();
    ILL_APDU_type retval = new ILL_APDU_type( ILL_APDU_type.forward_notification_var_CID, fwd_notification);

    // Fill out protocol version num,  transaction id
    ISOHeaderDetailsHelper.fillOutHeader(database_session, transaction, fwd_notification);

    String new_responder_symbol = new_responder.toString();

    fwd_notification.intermediary_id = fwd_notification.responder_id;
    fwd_notification.responder_id =
            new System_Id_type(                                                //   o responder id
                new Person_Or_Institution_Symbol_type(                         //   o     Person or inst sym
                  Person_Or_Institution_Symbol_type.institution_symbol_CID,
                  ILLStringHelper.generalString(new_responder_symbol)),
                null);

    Service new_delivery_service = (Service) new_responder.getServiceForThisSymbol();
    String teleco_service_type = (String) new_delivery_service.getTelecomServiceIdentifier();
    String teleco_service_address = (String) new_delivery_service.getTelecomServiceAddress();

    fwd_notification.responder_address = new System_Address_type(
      ILLStringHelper.generalString(teleco_service_type),
      ILLStringHelper.generalString(teleco_service_address));

    if ( notification_note != null )
      fwd_notification.notification_note = ILLStringHelper.generalString(notification_note);

    if ( ( extens != null ) && ( extens.length > 0 ) )
    {
      ArrayList extensions = new ArrayList();
      for ( int i=0; i<extens.length; i++ )
      {
        // Process exten i and add to vector
      }

      fwd_notification.forward_notification_extensions = extensions;
    }

    return retval;
  }
}
