/**
 * Title:       ISOExtensionFactory
 * @version:    $Id: ISOExtensionFactory.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */

//

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.Properties;
import java.util.Vector;
import java.util.Date;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

import java.math.BigInteger;

public interface ISOExtensionFactory
{
  Extension_type create();
}
