/**
 * Title:       ISORequestFactory
 * @version:    $Id: ISOAnswerMessageFactory.java,v 1.4 2005/07/09 09:19:51 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.Properties;
import java.util.ArrayList;
import java.util.Date;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.context.ApplicationContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class ISOAnswerMessageFactory {

  public static Log log = LogFactory.getLog(ISOAnswerMessageFactory.class);

  private static ILL_APDU_type createBaseAnswer(Session hibernate_session,
                                                LocationSymbol sender,
                                                LocationSymbol recipient,
                                                ILLTransaction transaction,
                                                ApplicationContext app_context) throws java.sql.SQLException, org.hibernate.HibernateException {
    ILL_Answer_type answer = new ILL_Answer_type();
    ILL_APDU_type retval = new ILL_APDU_type( ILL_APDU_type.ill_answer_var_CID, answer);
    ISOHeaderDetailsHelper.fillOutHeader(hibernate_session, transaction, answer);
    // Tag on APDU Delivery info
    answer.ill_answer_extensions = new ArrayList();
    ISOHeaderDetailsHelper.addAPDUDeliveryInfo(hibernate_session, sender, null, recipient, answer.ill_answer_extensions, app_context);
    // ISOHeaderDetailsHelper.addAuthenticationInfo(hibernate_session, sender, null, recipient, answer.ill_answer_extensions);

    return retval;
  }

  /**
   * Create a conditional answer for the supplied transaction given our role.
   * @param hibernate_session The active database session
   * @param transaction The active ILL Transaction for which we are sending this message
   * 
   */
  public static ILL_APDU_type createConditionalAnswer(Session hibernate_session,
                                                      LocationSymbol sender,
                                                      LocationSymbol recipient,
                                                      ILLTransaction transaction,
                                                      int condition,
                                                      Date date_for_reply,
                                                      String responder_note,
                                                      ApplicationContext app_context) throws java.sql.SQLException, org.hibernate.HibernateException {
    // Conditions are 13=cost exceeds limit, 14=charges, 15=prepayment required,
    // 16=lacks copyright compliance, 22=library use only, 23=no reproduction, 24=client sig reqd,
    // 25=special collections supervision reqd, 27 other, 28 responder specific, 30 proposed deli. svc

    ILL_APDU_type retval = createBaseAnswer(hibernate_session, sender, recipient, transaction, app_context);
    ILL_Answer_type ans = (ILL_Answer_type)retval.o;
    ans.transaction_results = BigInteger.valueOf(1);
    String date_str = null;
    if ( date_for_reply != null )
      date_str = Constants.formatDate(date_for_reply);

    ans.results_explanation = 
      new results_explanation_inline5_type(results_explanation_inline5_type.conditional_results_CID,
                                           new Conditional_Results_type(
                                             BigInteger.valueOf(condition),
                                             date_str,
                                             null, // locations
                                             null  /* delivery service */ )
      );
    ans.responder_note = new ILL_String_type(ILL_String_type.generalstring_var_CID, responder_note);

    return retval;
  }


  /**
   * Create an ILL Answer that indicates the item is not currently available, but may be
   * available at some time in the future.
   * @param hibernate_session The active database session
   * @param transaction The active ILL Transaction for which we are sending this message
   * @param reason_not_available  Possible values for reason not available:
   * <ul>
   * <li>1 - In use on loan
   * <li>2 - In-Process
   * <li>6 - On Order
   * <li>7 - Volume/Issue not yet available
   * <li>8 - At bindery
   * <li>13 - Cost exceeds limit
   * <li>14 - Charges
   * <li>15 - Prepayment required
   * <li>16 - Lacks copyright compliance
   * <li>17 - Not found as cited
   * <li>19 - On Hold
   * <li>27 - Other
   * <li>28 - Responder Specific
   * </ul>
   * @param retry_date (Optional) The date on which the requester may re-submit the request
   * @param locations (Optional) A ArrayList of LocationSymbol objects that the responder may try. TODO:
   * this parameter is not currently acted on.
   */
  public static ILL_APDU_type createRetryAnswer(Session hibernate_session,
                                                LocationSymbol sender,
                                                LocationSymbol recipient,
                                                ILLTransaction transaction,
                                                int reason_not_available,
						Date retry_date,
						ArrayList locations,
                                                String responder_note,
                                                ApplicationContext app_context) throws java.sql.SQLException,
                                                                        org.hibernate.HibernateException
  {
    ILL_APDU_type retval = createBaseAnswer(hibernate_session, sender, recipient, transaction, app_context);
    ILL_Answer_type ans = (ILL_Answer_type)retval.o;
    ans.transaction_results = BigInteger.valueOf(2);

    String retry_date_str = null;
    if ( retry_date != null )
      retry_date_str = Constants.formatDate(retry_date);

    // TODO: Convert location symbols into actual entries in the locations list string.
    ArrayList location_list = null;

    ans.results_explanation = 
      new results_explanation_inline5_type(results_explanation_inline5_type.retry_results_CID,
                       new Retry_Results_type( BigInteger.valueOf(reason_not_available),
                                               retry_date_str,
                                               location_list));

    ans.responder_note = new ILL_String_type(ILL_String_type.generalstring_var_CID, responder_note);

    return retval;
  }

  /** 
   * Create an ILL Answer that indicates the request is unfilled.
   * @param hibernate_session The active database session
   * @param transaction The active ILL Transaction for which we are sending this message
   * @param reason_unfilled  Possible values for reason not available:
   * <ul>
   * <li> 1 - In use on loan
   * <li> 2 - In process
   * <li> 3 - Lost
   * <li> 4 - Non Circulating
   * <li> 5 - Not owned
   * <li> 6 - On Order
   * <li> 7 - Volume Issue Not Yet Available
   * <li> 8 - At Bindery
   * <li> 9 - Lacking
   * <li> 10 - Not on shelf
   * <li> 11 - On reserve 
   * <li> 12 - Poor condition
   * <li> 13 - Cost exceed limit
   * <li> 14 - Charges
   * <li> 15 - Prepayment required
   * <li> 16 - Lacks copyright compliance
   * <li> 17 - Not found as cited
   * <li> 18 - Locations not found
   * <li> 19 - On hold
   * <li> 20 - Policy problem
   * <li> 21 - Mandatory messaging not supported
   * <li> 22 - Expiry not supported
   * <li> 23 - Requested delivery service not supported
   * <li> 24 - Prefered delivery time not possible
   * <li> 27 - Other
   * <li> 28 - Responder specific
   * </ul>
   * @param locations ArrayList of LocationSymbol objects
   */
  public static ILL_APDU_type createUnfilledAnswer(Session hibernate_session,
                                                   LocationSymbol sender,
                                                   LocationSymbol recipient,
                                                   ILLTransaction transaction,
                                                   int reason_unfilled,
                                                   ArrayList locations,
                                                   String responder_note,
                                                   ApplicationContext app_context)  throws java.sql.SQLException,
                                                                         org.hibernate.HibernateException
  {
    ILL_APDU_type retval = createBaseAnswer(hibernate_session, sender, recipient, transaction, app_context);
    ILL_Answer_type ans = (ILL_Answer_type)retval.o;
    ans.transaction_results = BigInteger.valueOf(3);

    ArrayList location_list = null;

    ans.results_explanation = 
      new results_explanation_inline5_type(results_explanation_inline5_type.unfilled_results_CID,
                       new Unfilled_Results_type( BigInteger.valueOf(reason_unfilled),
                                                  location_list));

    ans.responder_note = new ILL_String_type(ILL_String_type.generalstring_var_CID, responder_note);

    return retval;
  }

  /**
   * Create an ILL Answer that indicates the responder is providing a list of alternate locations.
   * @param hibernate_session The active database session
   * @param transaction The active ILL Transaction for which we are sending this message
   * @param reason_locs_provided  Possible values for reason not available:
   * <ul>
   * <li> 1 - In use on loan
   * <li> 2 - In process
   * <li> 3 - Lost
   * <li> 4 - Non Circulating
   * <li> 5 - Not owned
   * <li> 6 - On Order
   * <li> 7 - Volume Issue Not Yet Available
   * <li> 8 - At Bindery
   * <li> 9 - Lacking
   * <li> 10 - Not on shelf
   * <li> 11 - On reserve 
   * <li> 12 - Poor condition
   * <li> 13 - Cost exceed limit
   * <li> 19 - On hold
   * <li> 27 - Other
   * <li> 28 - Responder specific
   * </ul>
   * @param locations ArrayList of LocationSymbol objects
   */
  public static ILL_APDU_type createLocationsProvidedAnswer(Session hibernate_session,
                                                            LocationSymbol sender,
                                                            LocationSymbol recipient,
                                                            ILLTransaction transaction,
                                                            int reason_locs_provided,
                                                            ArrayList locations,
                                                            String responder_note,
                                                            ApplicationContext app_context) 
                                                                   throws java.sql.SQLException,
                                                                          org.hibernate.HibernateException
  {
    ILL_APDU_type retval = createBaseAnswer(hibernate_session, sender, recipient, transaction, app_context);
    ILL_Answer_type ans = (ILL_Answer_type)retval.o;
    ans.transaction_results = BigInteger.valueOf(4);

    ArrayList location_list = null;

    BigInteger bi_reasons_provided = null;
    if ( reason_locs_provided > 0 )
      bi_reasons_provided = BigInteger.valueOf(reason_locs_provided);

    ans.results_explanation = 
      new results_explanation_inline5_type(results_explanation_inline5_type.locations_results_CID,
                       new Locations_Results_type(bi_reasons_provided,
                                                  location_list));

    ans.responder_note = new ILL_String_type(ILL_String_type.generalstring_var_CID, responder_note);

    return retval;
  }


  /**
   * Create an ILL Answer that indicates the responder will supply the item
   * @param hibernate_session The active database session
   * @param transaction The active ILL Transaction for which we are sending this message
   * @param reason_will_supply  Possible values for reason not available:
   * <ul>
   * <li> 1 - In use on loan
   * <li> 2 - In process
   * <li> 6 - On Order
   * <li> 8 - At Bindery
   * <li> 19 - On hold
   * <li> 26 - Being processed for supply
   * <li> 27 - Other
   * <li> 28 - Responder specific
   * <li> 30 - Electronic Delivery
   * </ul>
   * @param supply_date
   * @param return_to_address
   * @param locations ArrayList of LocationSymbol objects
   * @param electronic_delivery_service
   */
  public static ILL_APDU_type createWillSupplyAnswer(Session hibernate_session,
                                                     LocationSymbol sender,
                                                     LocationSymbol recipient,
                                                     ILLTransaction transaction,
                                                     int reason_will_supply,
                                                     Date supply_date,
                                                     Postal_Address_type return_to_address,
                                                     ArrayList locations,
                                                     Electronic_Delivery_Service_type electronic_delivery_service,
                                                     String responder_note,
                                                     ApplicationContext app_context)  
                                                                 throws java.sql.SQLException,
                                                                        org.hibernate.HibernateException
  {
    ILL_APDU_type retval = createBaseAnswer(hibernate_session, sender, recipient, transaction, app_context);
    ILL_Answer_type ans = (ILL_Answer_type)retval.o;
    ans.transaction_results = BigInteger.valueOf(5);

    String supply_date_string = null;
    if ( supply_date != null )
      supply_date_string = Constants.formatDate(supply_date);

    ArrayList locations_list = null;

    ans.results_explanation = 
      new results_explanation_inline5_type(results_explanation_inline5_type.will_supply_results_CID,
                       new Will_Supply_Results_type(BigInteger.valueOf(reason_will_supply),
                                                  supply_date_string,
                                                  return_to_address,
                                                  locations_list,
                                                  electronic_delivery_service));

    ans.responder_note = new ILL_String_type(ILL_String_type.generalstring_var_CID, responder_note);

    return retval;
  }


  /**
   * Create an ILL Answer that indicates the responder will place a hold on the item
   * @param hibernate_session The active database session
   * @param transaction The active ILL Transaction for which we are sending this message
   * @param estimated_date_available
   * @param hold_placed_medium_type
   * @param locations ArrayList of LocationSymbol objects
   */
  public static ILL_APDU_type createHoldPlacedAnswer(Session hibernate_session,
                                                     LocationSymbol sender,
                                                     LocationSymbol recipient,
                                                     ILLTransaction transaction,
                                                     Date estimated_date_available,
                                                     int hold_placed_medium_type,
                                                     ArrayList locations,
                                                     String responder_note,
                                                     ApplicationContext app_context)  
                                                              throws java.sql.SQLException,
                                                                     org.hibernate.HibernateException
  {
    ILL_APDU_type retval = createBaseAnswer(hibernate_session, sender, recipient, transaction, app_context);
    ILL_Answer_type ans = (ILL_Answer_type)retval.o;
    ans.transaction_results = BigInteger.valueOf(6);

    String estimated_date_available_str = null;
    if ( estimated_date_available != null )
      estimated_date_available_str = Constants.formatDate(estimated_date_available);

    ans.responder_note = new ILL_String_type(ILL_String_type.generalstring_var_CID, responder_note);

    ArrayList locations_list = null;

    ans.results_explanation = 
      new results_explanation_inline5_type(results_explanation_inline5_type.hold_placed_results_CID,
                       new Hold_Placed_Results_type(estimated_date_available_str,
                                                    BigInteger.valueOf(hold_placed_medium_type),
                                                    locations_list));

    ans.responder_note = new ILL_String_type(ILL_String_type.generalstring_var_CID, responder_note);

    return retval;
  }

  /**
   * Create an ILL estimate Answer 
   * @param hibernate_session The active database session
   * @param transaction The active ILL Transaction for which we are sending this message
   * @param estimate The Estimate
   * @param locations A list of locations
   * @return An ILL Answer message of type Estimate
   */
  public static ILL_APDU_type createEstimateAnswer(Session hibernate_session,
                                                   LocationSymbol sender,
                                                   LocationSymbol recipient,
                                                   ILLTransaction transaction,
                                                   String estimate,
                                                   ArrayList locations,
                                                   String responder_note,
                                                   ApplicationContext app_context)  
                                                                 throws java.sql.SQLException,
                                                                        org.hibernate.HibernateException
  {
    ILL_APDU_type retval = createBaseAnswer(hibernate_session, sender, recipient, transaction, app_context);
    ILL_Answer_type ans = (ILL_Answer_type)retval.o;
    ans.transaction_results = BigInteger.valueOf(7);

    ArrayList locations_list = null;

    ans.results_explanation = 
      new results_explanation_inline5_type(results_explanation_inline5_type.estimate_results_CID,
                       new Estimate_Results_type(ILLStringHelper.generalString(estimate),
                                                 locations_list));

    ans.responder_note = new ILL_String_type(ILL_String_type.generalstring_var_CID, responder_note);

    return retval;
  }
}
