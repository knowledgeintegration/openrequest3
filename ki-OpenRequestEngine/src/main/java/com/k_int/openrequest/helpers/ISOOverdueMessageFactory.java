/**
 * Title:       ISOOverdueMessageFactory
 * @version:    $Id: ISOOverdueMessageFactory.java,v 1.2 2005/07/09 09:19:51 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Recall PDU
 */

package com.k_int.openrequest.helpers;

import java.util.Properties;
import java.util.ArrayList;
import java.util.Date;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import java.math.BigInteger;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

/**
 *  Create an ISO ILL Overdue Message.
 *  This class will construct an iso ill recall message
 *  @author Ian Ibbotson
 *  @version $Id: ISOOverdueMessageFactory.java,v 1.2 2005/07/09 09:19:51 ibbo Exp $
 */
public class ISOOverdueMessageFactory {
   public static Log log = LogFactory.getLog(ISOOverdueMessageFactory.class);

  /**
   *  create an overdue message.
   *
   *  Requires: An ILL Transaction in a state for which DUEreq is a valid action
   *
   *  Provides: A completed ILL Recieved message with data derived as follows:
   *            N.B where a member could be set from the transaction store, speak with k-int
   *            about defaulting that member in, it's better than starting to track transaction data
   *            in databases outside the transaction store.
   *
   *  element name/type                        M/O	Source
   *  retval : ILL_APDU_type (Choice of pdu)	M       Filled in by factory
   *    .which : int				M       Filled in by factory
   *    .o : Recieved_type			M       Filled in by factory
   *      .protocol_version_num
   *      .transaction_id			M	Constructed field, populated by this method
   *        .initial_requester_id		M	
   *          .person_or_institution_symbol	O	Set by factory from database
   *          .name_of_person_or_institution    O	Not set
   *        .tgq				M	Set by factory from database
   *        .tq					M	Set by factory from database
   *        .stq				O	Set by factory from database
   *      .service_date_time			M	Constructed by factory
   *        .date_time_of_this_service          M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *        .date_time_of_original_service      M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *      .requester_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *      .responder_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *
   *                   -- End of common elements. Recieved specific follow --
   *
   *      .date_due				M	Structured type created by this factory
   *        .date_due_field			M	Set according to date_due param
   *        .renewable				M	Is the item renewable
   *      .responder_note			O	Not Set
   *	  .recall_extensions			O	Depends on Extension Factory
   *      
   *
   *
   *  Ends description of how shipped PDU is populated by this factory method
   *
   *  @author Ian Ibbotson
   *  @param database_session The database session we will use to access the database
   *  @param request_id request id (internal number) of the transaction we are dealing with
   *  @param date_due Date the item due 
   *  @param renewable : Can the item be renewed
   *  @param responder_note
   *  @param extens And array of extension factories that will set up a shipped extension
   *                for each factory in the array.
   * 
   *  @return An ILL_APDU containing a Recall message
   */
  public static ILL_APDU_type create(Session database_session, 
                                     ILLTransaction transaction,	
				     Date date_due,
				     boolean renewable,
                                     String responder_note,
                                     ISOExtensionFactory[] extens) throws java.sql.SQLException,
                                                             org.hibernate.HibernateException
  {
    Overdue_type overdue = new Overdue_type();
    ILL_APDU_type retval = new ILL_APDU_type( ILL_APDU_type.overdue_var_CID, overdue);

    // Fill out protocol version num,  transaction id
    ISOHeaderDetailsHelper.fillOutHeader(database_session, transaction, overdue);

    if ( date_due != null ) {
      overdue.date_due = new Date_Due_type(
        Constants.formatDate(date_due),
        new Boolean(renewable));
    }

    if ( responder_note != null )
      overdue.responder_note = ILLStringHelper.generalString(responder_note);

    if ( (extens != null) && ( extens.length > 0 ) )
    {
      ArrayList extensions = new ArrayList();
      for ( int i=0; i<extens.length; i++ )
      {
        // Process exten i and add to vector
      }

      overdue.overdue_extensions = extensions;
    }

    return retval;
  }
}
