package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.*;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.ILL_APDU_Delivery_Info.*;
import com.k_int.openrequest.isoill.gen.Internal_Reference_Number.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import org.jzkit.z3950.gen.v3.AccessControlFormat_prompt_1.*;
import java.math.BigInteger;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.context.ApplicationContext;
import org.hibernate.*;

public interface APDUDeliveryInfoPolicyProvider {
  public void process(Session hibernate_session,
                      LocationSymbol sender_location,
                      LocationSymbol transponder_location,
                      LocationSymbol recipient_location,
                      ArrayList extensions,
                      ApplicationContext app_context) throws java.sql.SQLException, org.hibernate.HibernateException;
}
