/**
 * Title:       ProposedDeliveryServiceHelper
 * @version:    $Id: ElectronicDeliveryServiceHelper.java,v 1.1 2005/06/11 12:35:48 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Recall PDU
 */

package com.k_int.openrequest.helpers;

import java.util.*;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.Intermediary_Control.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.k_int.openrequest.integration.iface.*;
import com.k_int.openrequest.integration.iface.extensions.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 *  Create an ISO ILL Status Or Error Report Message.
 *  This class will construct an iso ill recall message
 *  @author Ian Ibbotson
 *  @version $Id: ElectronicDeliveryServiceHelper.java,v 1.1 2005/06/11 12:35:48 ibbo Exp $
 */
public class ElectronicDeliveryServiceHelper {

  private static Log log = LogFactory.getLog(ElectronicDeliveryServiceHelper.class);

  public static Electronic_Delivery_Service_type toIso(ElectronicDeliveryServiceDTO electronic_delivery_dto) {
    Electronic_Delivery_Service_type result = null;

    if ( electronic_delivery_dto != null ) {
      result = new Electronic_Delivery_Service_type();
    }

    return result;
  }
}
