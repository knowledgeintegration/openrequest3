/**
 * Title:       ISOStatusOrErrorReportMessageFactory
 * @version:    $Id: ISOStatusOrErrorReportMessageFactory.java,v 1.12 2005/07/09 09:19:51 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Recall PDU
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.*;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.Intermediary_Control.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.k_int.openrequest.integration.iface.*;
import com.k_int.openrequest.integration.iface.extensions.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 *  Create an ISO ILL Status Or Error Report Message.
 *  This class will construct an iso ill recall message
 *  @author Ian Ibbotson
 *  @version $Id: ISOStatusOrErrorReportMessageFactory.java,v 1.12 2005/07/09 09:19:51 ibbo Exp $
 */
public class ISOStatusOrErrorReportMessageFactory
{
  private static Log log = LogFactory.getLog(ISOStatusOrErrorReportMessageFactory.class);

  /**
   *  create a status or error report message.
   *
   *  Requires: An ILL Transaction
   *
   *  Provides: A completed ILL Recieved message with data derived as follows:
   *            N.B where a member could be set from the transaction store, speak with k-int
   *            about defaulting that member in, it's better than starting to track transaction data
   *            in databases outside the transaction store.
   *
   *  element name/type                        M/O	Source
   *  retval : ILL_APDU_type (Choice of pdu)	M       Filled in by factory
   *    .which : int				M       Filled in by factory
   *    .o : Recieved_type			M       Filled in by factory
   *      .protocol_version_num
   *      .transaction_id			M	Constructed field, populated by this method
   *        .initial_requester_id		M	
   *          .person_or_institution_symbol	O	Set by factory from database
   *          .name_of_person_or_institution    O	Not set
   *        .tgq				M	Set by factory from database
   *        .tq					M	Set by factory from database
   *        .stq				O	Set by factory from database
   *      .service_date_time			M	Constructed by factory
   *        .date_time_of_this_service          M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *        .date_time_of_original_service      M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *      .requester_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *      .responder_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *
   *                   -- End of common elements. StatusOrErrorReport specific follow --
   *
   *
   *  Ends description of how shipped PDU is populated by this factory method
   *
   *  @author Ian Ibbotson
   *  @param database_session The database session we will use to access the database
   *  @param transaction
   *  @param messagedto
   *                for each factory in the array.
   * 
   *  @return An ILL_APDU containing a Recall message
   */
  public static ILL_APDU_type create(Session database_session, 
                                     ILLTransaction transaction,	
                                     StatusOrErrorReportDTO message) throws java.sql.SQLException, org.hibernate.HibernateException
  {
    Status_Or_Error_Report_type soer = new Status_Or_Error_Report_type();
    ILL_APDU_type retval = new ILL_APDU_type( ILL_APDU_type.status_or_error_report_var_CID, soer);

    // Fill out protocol version num,  transaction id
    ISOHeaderDetailsHelper.fillOutHeader(database_session, transaction, soer);

    BigInteger reason_no_report = null;
    Status_Report_type status_report = null;
    Error_Report_type error_report = null;

    if ( message.getReasonNoReport() != null ) {
      reason_no_report = BigInteger.valueOf(message.getReasonNoReport().longValue());
    }

    if ( message.getErrorReport() != null ) {
      error_report = new Error_Report_type();
      error_report.correlation_information = ILLStringHelper.generalString(message.getErrorReport().correlation_information);
      if ( message.getErrorReport().report_source != null )
        error_report.report_source = BigInteger.valueOf(message.getErrorReport().report_source.longValue());

      // Process UserErrorReport
      if (  message.getErrorReport().user_error_report != null ) {
        error_report.user_error_report = new User_Error_Report_type();
        if ( message.getErrorReport().user_error_report instanceof AlreadyForwardedDTO ) {
          error_report.user_error_report.which = User_Error_Report_type.already_forwarded_CID;
          Already_Forwarded_type aft = new Already_Forwarded_type();
          error_report.user_error_report.o = aft;
          aft.responder_id = SystemIdHelper.toIsoSystemId( ((AlreadyForwardedDTO)message.getErrorReport().user_error_report).getSystemId() );
          // aft.responder_address = null;
        }
        else if ( message.getErrorReport().user_error_report instanceof IntermediaryProblemDTO ) {
          error_report.user_error_report.which = User_Error_Report_type.intermediary_problem_CID;
          error_report.user_error_report.o = ((IntermediaryProblemDTO)message.getErrorReport().user_error_report).getProblemCode();
        }
        else if ( message.getErrorReport().user_error_report instanceof SecurityProblemDTO ) {
          error_report.user_error_report.which = User_Error_Report_type.security_problem_CID;
          error_report.user_error_report.o = ILLStringHelper.generalString(((SecurityProblemDTO)message.getErrorReport().user_error_report).getReport());
        }
        else if ( message.getErrorReport().user_error_report instanceof UnableToPerformDTO ) {
          error_report.user_error_report.which = User_Error_Report_type.unable_to_perform_CID;
          error_report.user_error_report.o = ((UnableToPerformDTO)message.getErrorReport().user_error_report).getProblemCode();
        }
      }

      // Provider Error Report
      if (  message.getErrorReport().provider_error_report != null ) {
        error_report.provider_error_report = new Provider_Error_Report_type();
        if ( message.getErrorReport().provider_error_report instanceof GeneralProblemDTO ) {
          error_report.provider_error_report.which = Provider_Error_Report_type.general_problem_CID;
          error_report.provider_error_report.o = ((GeneralProblemDTO)message.getErrorReport().provider_error_report).getProblemCode();
        }
        else if ( message.getErrorReport().provider_error_report instanceof TransactionIdProblemDTO ) {
          error_report.provider_error_report.which = Provider_Error_Report_type.transaction_id_problem_CID;
          error_report.provider_error_report.o = ((TransactionIdProblemDTO)message.getErrorReport().provider_error_report).getProblemCode();
        }
        else if ( message.getErrorReport().provider_error_report instanceof StateTransitionProhibitedDTO ) {
          StateTransitionProhibitedDTO dto = (StateTransitionProhibitedDTO)message.getErrorReport().provider_error_report;
          error_report.provider_error_report.which = Provider_Error_Report_type.state_transition_prohibited_CID;
          error_report.provider_error_report.o = new State_Transition_Prohibited_type(BigInteger.valueOf(dto.getAPDUType()),
                                                                                      BigInteger.valueOf(dto.getCurrentState()));
        }
      }
    }

    // ToDo: StatusReport.
    if ( message.getStatusReport() != null ) {
      // status_report = new Status_Report_type();
    }

    soer.reason_no_report = reason_no_report;
    soer.status_report = status_report;
    soer.error_report = error_report;

    if ( message.getNote() != null ) 
      soer.note = ILLStringHelper.generalString(message.getNote());

    if ( (message.getExtensionDTOs() != null) && ( message.getExtensionDTOs().size() > 0 ) ) {
      ArrayList extensions = new ArrayList();
      for ( Iterator i = message.getExtensionDTOs().iterator(); i.hasNext(); ) {
        ProtocolMessageExtensionDTO extension = (ProtocolMessageExtensionDTO)i.next();

        if ( extension instanceof IntermediaryControlDTO ) {
          extensions.add(createIntermediaryControlExtension((IntermediaryControlDTO)extension));
        }
        else {
          log.warn("** Unhandled extension of type "+extension.getClass().getName()+" for StatusOrErrorReport");
        }
      }

      soer.status_or_error_report_extensions = extensions;
    }

    return retval;
  }

  public static ILL_APDU_type create(Session database_session,
                                     ILLTransaction transaction,
                                     BigInteger reason_no_report,
                                     Status_Report_type status_report,
                                     Error_Report_type error_report,
                                     String note,
                                     ISOExtensionFactory[] extens) throws java.sql.SQLException, org.hibernate.HibernateException
  {
    Status_Or_Error_Report_type soer = new Status_Or_Error_Report_type();
    ILL_APDU_type retval = new ILL_APDU_type( ILL_APDU_type.status_or_error_report_var_CID, soer);

    // Fill out protocol version num,  transaction id
    ISOHeaderDetailsHelper.fillOutHeader(database_session, transaction, soer);

    soer.reason_no_report = reason_no_report;
    soer.status_report = status_report;
    soer.error_report = error_report;
    soer.note = ILLStringHelper.generalString(note);

    if ( (extens != null) && ( extens.length > 0 ) ) {
      ArrayList extensions = new ArrayList();
      for ( int i=0; i<extens.length; i++ ) {
        // Process exten i and add to vector
      }

      soer.status_or_error_report_extensions = extensions;
    }

    return retval;
  }

  private static Intermediary_Control_type createIntermediaryControlExtension(IntermediaryControlDTO ic_extension_dto) {
    Intermediary_Control_type result = new Intermediary_Control_type();

    result.actionFlag = BigInteger.valueOf(ic_extension_dto.intermediary_actions.getCode());

    if ( ic_extension_dto.last_responder != null ) {
      result.lastResponder = getIntermediaryResponderControl(ic_extension_dto.last_responder);
    }

    if ( ic_extension_dto.current_responder != null ) {
      result.currentResponderId = SystemIdHelper.toIsoSystemId(ic_extension_dto.current_responder);
    }

    return result;
  }

  private static Intermediary_Responder_Control_type getIntermediaryResponderControl(IntermediaryResponderControlDTO icr_dto) {
    Intermediary_Responder_Control_type result = new Intermediary_Responder_Control_type();

    result.transactionResults = BigInteger.valueOf(icr_dto.getTransactionResults());
    if ( icr_dto.results_explanation instanceof UnfilledResultsDTO ) {
      UnfilledResultsDTO unfilled_results_dto = (UnfilledResultsDTO)icr_dto.results_explanation;

      result.resultsExplanation = new resultsExplanation_inline57_type();
      result.resultsExplanation.which=resultsExplanation_inline57_type.unfilledresults_CID;
      Unfilled_Results_type unfilled_results = new Unfilled_Results_type();
      result.resultsExplanation.o=unfilled_results;
      
      unfilled_results.reason_unfilled = BigInteger.valueOf(unfilled_results_dto.getReasonUnfilled());

      // ToDo : Locations
    }
    else if ( icr_dto.results_explanation instanceof ConditionalResultsDTO) {
      ConditionalResultsDTO conditional_results_dto = (ConditionalResultsDTO)icr_dto.results_explanation;
      result.resultsExplanation = new resultsExplanation_inline57_type();
      result.resultsExplanation.which=resultsExplanation_inline57_type.conditionalresults_CID;
      Conditional_Results_type conditional_results = new Conditional_Results_type();
      result.resultsExplanation.o=conditional_results;
      conditional_results.conditions = BigInteger.valueOf(conditional_results_dto.getConditions());
      conditional_results.date_for_reply =  Constants.formatDate(conditional_results_dto.getDateForReply());
      // ToDo: Locations
      conditional_results.locations = null;
      conditional_results.proposed_delivery_service = ProposedDeliveryServiceHelper.toIso(conditional_results_dto.getProposedDeliveryService());
      log.warn(icr_dto.results_explanation.getClass().getName()+" not converted");
    }
    else if ( icr_dto.results_explanation instanceof EstimateResultsDTO) {
      EstimateResultsDTO estimate_results_dto = (EstimateResultsDTO)icr_dto.results_explanation;
      result.resultsExplanation = new resultsExplanation_inline57_type();
      result.resultsExplanation.which=resultsExplanation_inline57_type.estimateresults_CID;
      Estimate_Results_type estimate_results = new Estimate_Results_type();
      result.resultsExplanation.o=estimate_results;
      log.warn(icr_dto.results_explanation.getClass().getName()+" not converted");
    }
    else if ( icr_dto.results_explanation instanceof HoldPlacedResultsDTO) {
      HoldPlacedResultsDTO hold_placed_results_dto = (HoldPlacedResultsDTO)icr_dto.results_explanation;
      result.resultsExplanation = new resultsExplanation_inline57_type();
      result.resultsExplanation.which=resultsExplanation_inline57_type.holdplacedresults_CID;
      Hold_Placed_Results_type holdplaced_results = new Hold_Placed_Results_type();
      result.resultsExplanation.o=holdplaced_results;
      log.warn(icr_dto.results_explanation.getClass().getName()+" not converted");
    }
    else if ( icr_dto.results_explanation instanceof LocationsResultsDTO) {
      LocationsResultsDTO locations_results_dto = (LocationsResultsDTO)icr_dto.results_explanation;
      result.resultsExplanation = new resultsExplanation_inline57_type();
      result.resultsExplanation.which=resultsExplanation_inline57_type.locationsresults_CID;
      Locations_Results_type locations_results = new Locations_Results_type();
      result.resultsExplanation.o=locations_results;
      log.warn(icr_dto.results_explanation.getClass().getName()+" not converted");
    }
    else if ( icr_dto.results_explanation instanceof RetryResultsDTO) {
      RetryResultsDTO retry_results_dto = (RetryResultsDTO)icr_dto.results_explanation;
      result.resultsExplanation = new resultsExplanation_inline57_type();
      result.resultsExplanation.which=resultsExplanation_inline57_type.retryresults_CID;
      Retry_Results_type retry_results = new Retry_Results_type();
      result.resultsExplanation.o=retry_results;
      log.warn(icr_dto.results_explanation.getClass().getName()+" not converted");
    }
    else if ( icr_dto.results_explanation instanceof WillSupplyResultsDTO) {
      WillSupplyResultsDTO will_supply_results_dto = (WillSupplyResultsDTO)icr_dto.results_explanation;
      result.resultsExplanation = new resultsExplanation_inline57_type();
      result.resultsExplanation.which=resultsExplanation_inline57_type.willsupplyresults_CID;
      Will_Supply_Results_type willsupply_results = new Will_Supply_Results_type();
      result.resultsExplanation.o=willsupply_results;
      log.warn(icr_dto.results_explanation.getClass().getName()+" not converted");
    }

    return result;
  }


  public static ILL_APDU_type createSOERForDuplicateTransactionId(ILL_APDU_type offending_request, BigInteger problem_code, String correlation_info) {
    Status_Or_Error_Report_type soer = new Status_Or_Error_Report_type();
    ILL_APDU_type retval = new ILL_APDU_type( ILL_APDU_type.status_or_error_report_var_CID, soer);
    ISOHeaderDetailsHelper.copyHeader(offending_request.o, retval.o);
    Error_Report_type error_report = new Error_Report_type();
    error_report.correlation_information = ILLStringHelper.generalString(correlation_info);
    error_report.report_source = BigInteger.valueOf(2);
    error_report.provider_error_report = new Provider_Error_Report_type();
    error_report.provider_error_report.which = Provider_Error_Report_type.transaction_id_problem_CID;
    error_report.provider_error_report.o = problem_code;
    soer.error_report = error_report;
    return retval;
  }

  public static ILL_APDU_type createSOERForTransactionIdProblem(ILL_APDU_type offending_request, BigInteger problem_code, String correlation_info) {
    Status_Or_Error_Report_type soer = new Status_Or_Error_Report_type();
    ILL_APDU_type retval = new ILL_APDU_type( ILL_APDU_type.status_or_error_report_var_CID, soer);
    ISOHeaderDetailsHelper.copyHeader(offending_request.o, retval.o);
    Error_Report_type error_report = new Error_Report_type();
    error_report.correlation_information = ILLStringHelper.generalString(correlation_info);
    error_report.report_source = BigInteger.valueOf(2);
    error_report.provider_error_report = new Provider_Error_Report_type();
    error_report.provider_error_report.which = Provider_Error_Report_type.transaction_id_problem_CID;
    error_report.provider_error_report.o = problem_code;
    soer.error_report = error_report;
    return retval;
  }

}
