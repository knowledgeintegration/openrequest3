/**
 * Title:       Constants
 * @version:    $Id: Constants.java,v 1.3 2005/07/09 09:19:51 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */

// com.k_int.openrequest.helpers.Constants.ISO_DATE_FORMATTER

package com.k_int.openrequest.helpers;

import java.io.Serializable;
import java.util.Date;

public class Constants {

  private static final String ISO_DATE_FORMAT = "yyyyMMdd";
  private static final String ISO_TIME_FORMAT = "HHmmss";
  private static long counter = 0;

  public static String getNewUniqueId() throws java.sql.SQLException, org.hibernate.HibernateException {
    String result = ""+System.currentTimeMillis()+"."+(counter++);
    return result;
  }

  public static String formatDate(java.util.Date d) {
    return formatDate(d, ISO_DATE_FORMAT);
  }

  public static String formatTime(java.util.Date d) {
    return formatDate(d, ISO_TIME_FORMAT);
  }

  private static String formatDate(java.util.Date d, String pattern) {
    if ( d != null ) {
      java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(pattern);
      formatter.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
      return formatter.format(d).toString();
    }

    return null;
  }

  public static Date parseIsoDate(String iso_date) throws java.text.ParseException {
    if ( iso_date != null ) {
      java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(ISO_DATE_FORMAT);
      formatter.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
      return formatter.parse(iso_date);
    }

    return null;
  }
}
