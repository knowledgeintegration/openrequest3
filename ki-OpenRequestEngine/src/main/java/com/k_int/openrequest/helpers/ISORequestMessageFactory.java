/**
 * Title:       ISORequestFactory
 * @version:    $Id: ISORequestMessageFactory.java,v 1.26 2005/07/09 09:19:51 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.*;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import java.math.BigInteger;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.xpath.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import org.apache.commons.logging.*;
import org.springframework.context.ApplicationContext;


public class ISORequestMessageFactory
{
  private static Log log = LogFactory.getLog(ISORequestMessageFactory.class);

  /**
   *  Takes a session, patron, requesting location information and an XML Document using 
   *  our generic request schema to create an initial ISO 10161 Request message to ask for that item.
   *
   *  Requires: 
   *  Provides: A completed ILL Request message with data derived as follows:
   *            N.B where a member could be set from the transaction store, speak with k-int
   *            about defaulting that member in, it's better than starting to track transaction data
   *            in databases outside the transaction store.
   *
   *  element name/type                        M/O      Source
   *  retval : ILL_APDU_type (Choice of pdu)    M       Filled in by factory
   *    .which : int                            M       Filled in by factory
   *    .o : ILL_Request_type                   M       Filled in by factory
   *      .protocol_version_num
   *      .transaction_id                       M       Constructed field, populated by this method
   *        .initial_requester_id               M
   *          .person_or_institution_symbol     O       Set by factory from database
   *          .name_of_person_or_institution    O       Not set
   *        .tgq                                M       Value created if not passed in!
   *        .tq                                 M       Value created by factory
   *        .stq                                O       Set by factory from database
   *      .service_date_time                    M       Constructed by factory
   *        .date_time_of_this_service          M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *        .date_time_of_original_service      M       Set by factory from system clock
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *      .requester_id                         M       Set by factory
   *        .person_or_institution_symbol       O       Set by factory
   *        .person_or_institution_name         O       Not set
   *      .responder_id                         M       Set by factory
   *        .person_or_institution_symbol       O       Set by factory
   *        .person_or_institution_name         O       Not set
   *                   -- End of common elements. Request specific follow --
   *
   */
  public static ILL_APDU_type create(Session hibernate_session,
                                     String tgq,
                                     LocationSymbol requester,
                                     LocationSymbol responder,
                                     LocationSymbol initial_requester,
                                     String patron_symbol,
                                     String patron_status,
                                     String patron_name,
                                     Boolean allow_forwarding,
                                     Boolean allow_chaining,
                                     Boolean allow_partitioning,
                                     Boolean allow_change_send_to_list,
                                     long transaction_type,
                                     Document request_doc,
                                     ISOExtensionFactory[] extens,
                                     ApplicationContext app_context)
  {
    log.debug("ISORequestMessageFactory::create");
    ILL_APDU_type pdu = null;

    String requester_symbol = requester.toString();
    String responder_symbol = responder.toString();

    log.debug("requester_symbol will be "+requester_symbol);
    log.debug("responder_symbol will be "+responder_symbol);

    System_Id_type initial_requester_info = null;
    System_Address_type initial_requester_address = null;

    if ( initial_requester != null ) {
      String orig_requester_symbol = initial_requester.getAuthority().getIdentifier()+":"+
                                         initial_requester.getSymbol();
      log.debug("Setting original requester to "+orig_requester_symbol);

      initial_requester_info = new System_Id_type(
                   new Person_Or_Institution_Symbol_type(
                     Person_Or_Institution_Symbol_type.institution_symbol_CID,
                     new ILL_String_type(ILL_String_type.generalstring_var_CID,orig_requester_symbol)),
                   null);

      // Figure out a system address for the original requester.
      Service default_service = (Service) initial_requester.getServiceForThisSymbol();
      if ( default_service.getTelecomServiceIdentifier() != null ) {
        String service_type = default_service.getTelecomServiceIdentifier();
        String service_address = default_service.getTelecomServiceAddress();
        log.debug("Set orig service type/addr to "+service_type+" / "+service_address);
        initial_requester_address = new System_Address_type(
          new ILL_String_type(ILL_String_type.generalstring_var_CID,service_type),
          new ILL_String_type(ILL_String_type.generalstring_var_CID,service_address));
      }
    }

    try
    {
      // tgq = "OR:"+id_gen.generate(null,null);
      if ( tgq == null )
         tgq = "OR:"+Constants.getNewUniqueId();


      // String tq = "OR:"+id_gen.generate(null,null);
      String tq = "OR:"+Constants.getNewUniqueId();

      java.util.Date d = new java.util.Date( System.currentTimeMillis() );
      String date_str = Constants.formatDate(d);
      String time_str = Constants.formatTime(d);

      ArrayList extensions = ISOExtensionsHelper.create(extens);
      ISOHeaderDetailsHelper.addAPDUDeliveryInfo(hibernate_session, requester, null, responder, extensions, app_context);
      ISOHeaderDetailsHelper.addAuthenticationInfo(hibernate_session, requester, null, responder, extensions);

      pdu = 
        new ILL_APDU_type( ILL_APDU_type.ill_request_var_CID,
          new ILL_Request_type(                                                // ILL_Request
            BigInteger.valueOf(2),                                             //   m Protocol Version
            new Transaction_Id_type(                                           //   m Transaction ID
              initial_requester_info,                                          //   o   Init req Syst id
              new ILL_String_type(ILL_String_type.generalstring_var_CID,tgq),//m tgq
              new ILL_String_type(ILL_String_type.generalstring_var_CID,tq), //m tq
              (ILL_String_type)null), // stq
            new Service_Date_Time_type(                                        //   m service date time
              new date_time_of_this_service_inline35_type(date_str,time_str),
              null), // This is only needed for repeats. new date_time_of_original_service_inline36_type(date_str,time_str)),
            new System_Id_type(                                                //   o requester id
                new Person_Or_Institution_Symbol_type(                         //   o     Person or inst sym
                  Person_Or_Institution_Symbol_type.institution_symbol_CID,
                  new ILL_String_type(ILL_String_type.generalstring_var_CID,requester_symbol) ),
                null /* Don't bother setting name of person or instituion */), //   o     name of pers or inst
            new System_Id_type(                                                //   o responder id
                new Person_Or_Institution_Symbol_type(                         //   o     Person or inst sym
                  Person_Or_Institution_Symbol_type.institution_symbol_CID,
                  new ILL_String_type(ILL_String_type.generalstring_var_CID,responder_symbol) ),
                null /* Don't bother setting name of person or instituion */), //   o     name of pers or inst

            // End of generic attrs that come with every PDU

            BigInteger.valueOf(1),                                             //   m transaction type
                          // Simple, Chained or Partitioned 1,2 or 3
            null, // new Delivery_Address_type(),                              //   o delivery address
            null, // new Delivery_Service_type(),                              //   o delivery service
            null, // new Delivery_Address_type(),                              //   o billing address
            new ArrayList(),                                                      //   m service type
            null,                                                              //   o resp. specific. services
            new Requester_Optional_Messages_Type_type(                         //   m req. opt. msg
                Boolean.TRUE,
                Boolean.TRUE,
                BigInteger.valueOf(2), // 1=requires, 2=desires, 3=neither
                BigInteger.valueOf(2)  // 1=requires, 2=desires, 3=neither
                ),
            new Search_Type_type(                                              //   o search type
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"lvl"),
                "need-before-date",
                BigInteger.valueOf(2), // 1=need b4, 2=othr, 3=no expiry
                "expirydate"),
            new java.util.ArrayList(),                                            //   m supp. med. info. type
            BigInteger.valueOf(2),    // 1=yes, 2=no                           //   m place on hold
            new Client_Id_type(                                                //   o client id
		ILLStringHelper.generalString(patron_name),
		ILLStringHelper.generalString(patron_status),
		ILLStringHelper.generalString(patron_symbol)),
            new Item_Id_type(                                                  //   m item id
                BigInteger.valueOf(2),                                         //   o   item type
                BigInteger.valueOf(2),                                         //   o   held medium type
                extract(request_doc,"CallNumber"),
                extract(request_doc,"Author"),
                extract(request_doc,"Title"),
                extract(request_doc,"SubTitle"),
                extract(request_doc,"Sponsor"),
                extract(request_doc,"Place"),
                extract(request_doc,"Publisher"),
                extract(request_doc,"Series"),
                extract(request_doc,"Volume"),
                extract(request_doc,"Edition"),
                extract(request_doc,"PublicationDate"),
                extract(request_doc,"PublicationDateOfComponent"),
                extract(request_doc,"AuthorOfArticle"),
                extract(request_doc,"TitleOfArticle"),
                extract(request_doc,"Pagination"),
                null,
                extract(request_doc,"ISBN"),
                extract(request_doc,"ISSN"),
                null, // EXTERNAL_type
                extract(request_doc,"AdditionalNoLetters"),
                extract(request_doc,"VerificationReferenceSource") ),
            encodeSupplementalItemDescriptions(),                                        //   o supp. item desc.
            null, // new Cost_Info_Type_type(),                                //   o cost info type
             new ILL_String_type(ILL_String_type.generalstring_var_CID,"Signed forms avialable"),//  o copyright compliance
            new Third_Party_Info_Type_type(                                    //   o third_party_info_type
              allow_forwarding,
              allow_chaining,
              allow_partitioning,
              allow_change_send_to_list,
              initial_requester_address, // Opt initial requesterid Must be filled for partitioned subtrans
              BigInteger.valueOf(1), // Responder list is ordered (1), not unordered (2)
              null,  // Third party info type
              null),
            Boolean.FALSE,                                                     //   m retry flag
            Boolean.FALSE,                                                     //   m forward flag
            new ILL_String_type(ILL_String_type.generalstring_var_CID,"My note"), //o requester note
            new ILL_String_type(ILL_String_type.generalstring_var_CID,"My note"), //o forward note
            extensions)                                                           //   o vec req. extensions
        );
    }
    catch ( Exception e )
    {
      log.warn("Problem creating ILL Request message",e);
    }
    finally
    {
      log.debug("Done");
    }

    return pdu;
  }

  /**
   *  Create a new Request for the next entry on a rota within a transaction group.
   *
   *  Requires: 
   *  Provides: A completed ILL Request message with data derived as follows:
   *            N.B where a member could be set from the transaction store, speak with k-int
   *            about defaulting that member in, it's better than starting to track transaction data
   *            in databases outside the transaction store.
   *
   *  element name/type                        M/O      Source
   *  retval : ILL_APDU_type (Choice of pdu)    M       Filled in by factory
   *    .which : int                            M       Filled in by factory
   *    .o : ILL_Request_type                   M       Filled in by factory
   *      .protocol_version_num
   *      .transaction_id                       M       Constructed field, populated by this method
   *        .initial_requester_id               M
   *          .person_or_institution_symbol     O       Set by factory from database
   *          .name_of_person_or_institution    O       Not set
   *        .tgq                                M       Value created if not passed in!
   *        .tq                                 M       Value created by factory
   *        .stq                                O       Set by factory from database
   *      .service_date_time                    M       Constructed by factory
   *        .date_time_of_this_service          M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *        .date_time_of_original_service      M       Set by factory from system clock
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *      .requester_id                         M       Set by factory
   *        .person_or_institution_symbol       O       Set by factory
   *        .person_or_institution_name         O       Not set
   *      .responder_id                         M       Set by factory
   *        .person_or_institution_symbol       O       Set by factory
   *        .person_or_institution_name         O       Not set
   *                   -- End of common elements. Request specific follow --
   *
   */
  public static ILLMessageEnvelope nextRequestInRota( Session hibernate_session, 
                                                      ILLTransactionGroup tg, 
                                                      MessageDeliveryObserver mdo,
                                                      ApplicationContext app_ctx) throws org.hibernate.HibernateException, java.sql.SQLException {

    log.debug("ISORequestMessageFactory::nextRequestInRota");

    ILL_APDU_type pdu = null;
    ILLMessageEnvelope envelope = null;

    String requester_symbol = null;
    String responder_symbol = null;
    String initial_requester_symbol = null;
    LocationSymbol requester = null;
    LocationSymbol responder = null;

    try {
      hibernate_session.lock(tg,LockMode.UPGRADE);

      System_Id_type initial_requester_info = null;
      System_Address_type initial_requester_address = null;

      int current_rota_position = (int)(tg.getCurrentRotaPosition());

      if ( (current_rota_position+1) < tg.getRota().size() ) {
        current_rota_position++; 
        tg.setCurrentRotaPosition(current_rota_position);
        RotaEntry next_entry = (RotaEntry) (tg.getRota().get(current_rota_position));
  
        responder_symbol = next_entry.getSymbol();
        initial_requester_symbol = tg.getInitialRequesterSymbol();
        requester_symbol = tg.getRequesterSymbol();
  
        log.debug("Looking up initial requester "+initial_requester_symbol);
        LocationSymbol initial_requester = DBHelper.lookupLocation(hibernate_session, initial_requester_symbol);
        log.debug("Looking up requester "+requester_symbol);
        requester = DBHelper.lookupLocation(hibernate_session, requester_symbol);
        log.debug("Looking up [next in rota] respoder "+responder_symbol);
        responder = DBHelper.lookupLocation(hibernate_session, responder_symbol);
  
        Boolean allow_forwarding = Boolean.FALSE;
        Boolean allow_chaining = Boolean.FALSE;
        Boolean allow_partitioning = Boolean.FALSE;
        Boolean allow_change_send_to_list = Boolean.FALSE;
        long transaction_type = 1;
  
        log.debug("Next rota entry will be : "+responder_symbol);
        log.debug("requester_symbol will be "+requester_symbol);
        log.debug("responder_symbol will be "+responder_symbol);
  
  
        if ( initial_requester != null )
        {
          initial_requester_info = new System_Id_type(
                       new Person_Or_Institution_Symbol_type(
                         Person_Or_Institution_Symbol_type.institution_symbol_CID,
                         new ILL_String_type(ILL_String_type.generalstring_var_CID,initial_requester_symbol)),
                       null);
  
          // Figure out a system address for the original requester.
          // Service default_service = (Service) initial_requester.getCanonical_location().getServices().get(0);
          Service default_service = (Service) initial_requester.getServiceForThisSymbol();

          if ( default_service.getProperties().get("TelecoServiceType") != null ) { 
            String service_type = default_service.getProperties().get("TelecoServiceType").toString();
            String service_address = default_service.getProperties().get("TelecoServiceAddress").toString();
            log.debug("Set orig service type/addr to "+service_type+" / "+service_address);
            initial_requester_address = new System_Address_type(
              new ILL_String_type(ILL_String_type.generalstring_var_CID,service_type),
              new ILL_String_type(ILL_String_type.generalstring_var_CID,service_address));
          }
        }
  
        // String tq = "OR:"+id_gen.generate(null,null);

        String tq = null;
        if ( ( next_entry.getMandatoryTQ() != null ) && ( next_entry.getMandatoryTQ().length() > 0 ) )
          tq =  next_entry.getMandatoryTQ();
        else
          tq = "OR:"+Constants.getNewUniqueId();

        log.debug("New TQ will be "+tq);

        java.util.Date d = new java.util.Date( System.currentTimeMillis() );
  
        String date_str = com.k_int.openrequest.helpers.Constants.formatDate(d);
        String time_str = com.k_int.openrequest.helpers.Constants.formatTime(d);

        // Delivery_Address_type billing_address = null;
        Delivery_Address_type delivery_address = dbToAsnAddress(tg.getCurrentDeliveryAddress());
        Delivery_Address_type billing_address = dbToAsnAddress(tg.getCurrentBillingAddress());

        String need_by_str = null;
        if ( tg.getNeedByDate() != null )
          need_by_str = com.k_int.openrequest.helpers.Constants.formatDate(tg.getNeedByDate());

        ArrayList extensions = new ArrayList();

        ArrayList service_type_vector = new ArrayList();
        for ( Iterator i = tg.getRequiredItemDetails().getService_types().iterator(); i.hasNext(); ) {
          Integer st = (Integer)i.next();
          service_type_vector.add(BigInteger.valueOf(st.intValue()));
        }

        ISOHeaderDetailsHelper.addAPDUDeliveryInfo(hibernate_session, requester, null, responder, extensions, app_ctx);

        ISOHeaderDetailsHelper.addAuthenticationInfo(hibernate_session, requester, null, responder, extensions);

        ISOHeaderDetailsHelper.addInternalReferenceNumber(tg.getRequesterReferenceAuthority(),
                                                          tg.getRequesterReference(),
                                                          tg.getResponderReferenceAuthority(),
                                                          tg.getResponderReference(),
                                                          extensions);

        // Add IPIG ILL Request Extension
        if ( ( tg.getClientDepartment() != null ) ||
             ( tg.getPaymentMethod() != null ) ||
             ( tg.getAffiliations() != null ) ||
             ( tg.getRequiredItemDetails().getUniformTitle() != null ) ||
             ( tg.getRequiredItemDetails().getDissertation() != null ) ||
             ( tg.getRequiredItemDetails().getIssue() != null ) ||
             ( tg.getRequiredItemDetails().getVolume() != null ) ||
             ( tg.getRequiredItemDetails().getVerificationReferenceSource() != null ) ) {
          ISOHeaderDetailsHelper.addIPIGILLRequestExtenson(tg.getClientDepartment(),
                                                           tg.getPaymentMethod(),
                                                           tg.getAffiliations(),
                                                           tg.getRequiredItemDetails().getUniformTitle(),
                                                           tg.getRequiredItemDetails().getDissertation(),
                                                           tg.getRequiredItemDetails().getIssue(),
                                                           tg.getRequiredItemDetails().getVolume(),
                                                           tg.getRequiredItemDetails().getSource(),
                                                           extensions);
        }

        ArrayList supply_medium_info_types = null;
        if ( ( tg.getRequiredItemDetails().getSupplyMediumInfoType() != null ) && ( tg.getRequiredItemDetails().getSupplyMediumInfoType().size() > 0 ) ) {
          supply_medium_info_types = new java.util.ArrayList();
          for ( Iterator smi = tg.getRequiredItemDetails().getSupplyMediumInfoType().iterator(); smi.hasNext(); ) {
            MediumInfo mi = (MediumInfo) smi.next();
            // System.err.println("Adding supply medium info type to outgoing request : "+mi.getMediumType());
            supply_medium_info_types.add(
              new Supply_Medium_Info_Type_type(
                BigInteger.valueOf(mi.getMediumType()),
                ILLStringHelper.generalString(mi.getCharacteristics())));
          }
        }
        else {
          log.debug("getSupplyMediumInfoType() null or empty list");
        }
  
        String volissue = null;

        if ( tg.getRequiredItemDetails().getVolume() != null ) {
          if ( tg.getRequiredItemDetails().getIssue() != null ) {
            volissue = tg.getRequiredItemDetails().getVolume() + " " + tg.getRequiredItemDetails().getIssue();
          }
          else {
            volissue = tg.getRequiredItemDetails().getVolume();
          }
        }
        else {
          if ( tg.getRequiredItemDetails().getIssue() != null ) {
            volissue = tg.getRequiredItemDetails().getIssue();
          }
        }


        Cost_Info_Type_type cost_info = null;
        if ( ( tg.getRequiredItemDetails().getAccountNumber() != null ) ||
             ( tg.getRequiredItemDetails().getMaxCostString() != null ) ) {
          cost_info = new Cost_Info_Type_type(
            ILLStringHelper.generalString(tg.getRequiredItemDetails().getAccountNumber()),
            tg.getRequiredItemDetails().getMaxCostString() != null ?
                                            new Amount_type(tg.getRequiredItemDetails().getMaxCostCurrencyCode(), tg.getRequiredItemDetails().getMaxCostString()) : null,
            new Boolean(tg.getRequiredItemDetails().getReciprocalAgreement()),
            new Boolean(tg.getRequiredItemDetails().getWillPayFee()),
            new Boolean(tg.getRequiredItemDetails().getPaymentProvided()));
        }

        Requester_Optional_Messages_Type_type requester_opt_messages = null;
        if ( ( tg.getCanSendReceived() != null ) || ( tg.getCanSendReturned() != null ) ) {
          requester_opt_messages = new Requester_Optional_Messages_Type_type(
                  tg.getCanSendReceived(),
                  tg.getCanSendReturned(),
                  tg.getRequesterShipped() != null ? BigInteger.valueOf(tg.getRequesterShipped().longValue()) : null, // 1=requires, 2=desires, 3=neither
                  tg.getRequesterCheckedIn() != null ? BigInteger.valueOf(tg.getRequesterCheckedIn().longValue()) : null  // 1=requires, 2=desires, 3=neither
                  );
        }

        Client_Id_type client_id = null;
        if ( ( tg.getPatronName() != null ) || ( tg.getPatronStatus() != null ) || (tg.getPatronSymbol()!=null) ) {
          client_id = new Client_Id_type( ILLStringHelper.generalString(tg.getPatronName()),
                  ILLStringHelper.generalString(tg.getPatronStatus()),
                  ILLStringHelper.generalString(tg.getPatronSymbol()));
        }

     
        pdu = 
          new ILL_APDU_type( ILL_APDU_type.ill_request_var_CID,
            new ILL_Request_type(                                                // ILL_Request
              BigInteger.valueOf(2),                                             //   m Protocol Version
              new Transaction_Id_type(                                           //   m Transaction ID
                initial_requester_info,                                          //   o   Init req Syst id
                ILLStringHelper.generalString(tg.getTGQ()),
                ILLStringHelper.generalString(tq),
                (ILL_String_type)null), // stq
              new Service_Date_Time_type(                                        //   m service date time
                new date_time_of_this_service_inline35_type(date_str,time_str),
                null), // Only needed for repeats. new date_time_of_original_service_inline36_type(date_str,time_str)),
              new System_Id_type(                                                //   o requester id
                  new Person_Or_Institution_Symbol_type(                         //   o     Person or inst sym
                    Person_Or_Institution_Symbol_type.institution_symbol_CID,
                    ILLStringHelper.generalString(requester_symbol) ),
                  null /* Don't bother setting name of person or instituion */), //   o     name of pers or inst
              new System_Id_type(                                                //   o responder id
                  new Person_Or_Institution_Symbol_type(                         //   o     Person or inst sym
                    Person_Or_Institution_Symbol_type.institution_symbol_CID,
                    ILLStringHelper.generalString(responder_symbol) ),
                  null /* Don't bother setting name of person or instituion */), //   o     name of pers or inst
  
              // End of generic attrs that come with every PDU
  
              BigInteger.valueOf(1),                                             //   m transaction type
                            // Simple, Chained or Partitioned 1,2 or 3
              delivery_address,
              null, // new Delivery_Service_type(),                              //   o delivery service
              billing_address,
              service_type_vector,
              null,                                                              //   o resp. specific. services
              requester_opt_messages,
              new Search_Type_type(                                              //   o search type
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getLevelOfService()),
                  Constants.formatDate(tg.getRequiredItemDetails().getNeedBeforeDate()),
                  BigInteger.valueOf(tg.getRequiredItemDetails().getExpiryFlag()), // 1=need b4, 2=othr, 3=no expiry
                  Constants.formatDate(tg.getRequiredItemDetails().getExpiryDate())),
              supply_medium_info_types,                                            //   m supp. med. info. type
              BigInteger.valueOf(tg.getRequiredItemDetails().getPlaceOnHold()),
              client_id,                                                         //   o client id
              new Item_Id_type(
                  (tg.getRequiredItemDetails().getItemType() != 0 ? BigInteger.valueOf(tg.getRequiredItemDetails().getItemType()) : null),
                  (tg.getRequiredItemDetails().getHeldMediumType() != 0 ? BigInteger.valueOf(tg.getRequiredItemDetails().getHeldMediumType()) : null),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getCallNumber()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getAuthor()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getTitle()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getSubtitle()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getSponsoringBody()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getPlaceOfPublication()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getPublisher()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getSeriesTitleNumber()),
                  ILLStringHelper.generalString(volissue),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getEdition()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getPublicationDate()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getPublicationDateOfComponent()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getAuthorOfArticle()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getTitleOfArticle()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getPagination()),
                  generateIPIGSystemNumberExternal(tg.getRequiredItemDetails().getNatBibNo()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getISBN()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getISSN()),
                  generateIPIGSystemNumberExternal(tg.getRequiredItemDetails().getSystemNo()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getAdditionalNoLetters()),
                  ILLStringHelper.generalString(tg.getRequiredItemDetails().getVerificationReferenceSource())),
              encodeSupplementalItemDescriptions(),                                        //   o supp. item desc.
              cost_info, // new Cost_Info_Type_type(),                                //   o cost info type
              ILLStringHelper.generalString(tg.getRequiredItemDetails().getCopyrightCompliance()),
              new Third_Party_Info_Type_type(                                    //   o third_party_info_type
                allow_forwarding,
                allow_chaining,
                allow_partitioning,
                allow_change_send_to_list,
                initial_requester_address, // Opt initial requesterid Must be filled for partitioned subtrans
                BigInteger.valueOf(1), // Responder list is ordered (1), not unordered (2)
                null,  // Third party info type
                null),
              Boolean.FALSE,                                                      //   m retry flag
              Boolean.FALSE,                                                     //   m forward flag
              ILLStringHelper.generalString(tg.getRequesterNote()),
              ILLStringHelper.generalString(null),                               // "Forward note",
              extensions)                                                           //   o vec req. extensions
          );
          tg.setTransactionGroupStatusCode(DBHelper.lookupStatusCode(hibernate_session,"IN-PROCESS"));

        envelope = new ILLMessageEnvelope(pdu, requester_symbol, responder_symbol, mdo);
      }
      else {
        log.debug("End of rota");
        tg.setTransactionGroupStatusCode(DBHelper.lookupStatusCode(hibernate_session,"ENDROTA"));
      }

      hibernate_session.flush();
      hibernate_session.connection().commit();
      log.debug("Creation of new ILL Request complete");
    }
    finally {
      log.debug("Done");
    }
  
    return envelope;
  }
  
  public static ILL_String_type extract(Document doc, String field)
  {
    try
    {
      String the_path = "//DocumentRequest/"+field+"/text()";
      Node n = XPathAPI.selectSingleNode(doc.getDocumentElement(),the_path);

      if ( n != null )
      {
        log.debug("node found: "+n.getNodeValue());
        return ILLStringHelper.generalString(n.getNodeValue().toString());
      }
    }
    catch ( javax.xml.transform.TransformerException te )
    {
      log.warn("Problem tring to extract XML doc field",te);
    }
  
    return null;
  }
  
  private static Delivery_Address_type dbToAsnAddress(ISO10161Address db_addr)
  {
    Delivery_Address_type retval = null;

    if ( db_addr != null )
    {
      retval = new Delivery_Address_type();
      retval.postal_address = new Postal_Address_type();

      if ( db_addr.getPostal_name() != null ) {
        retval.postal_address.name_of_person_or_institution = new Name_Of_Person_Or_Institution_type();
        retval.postal_address.name_of_person_or_institution.which = (int) db_addr.getPostal_name_type();
        retval.postal_address.name_of_person_or_institution.o = ILLStringHelper.generalString(db_addr.getPostal_name());
      }

      retval.postal_address.extended_postal_delivery_address = 
                       ILLStringHelper.generalString(db_addr.getPostal_extended_address());
      retval.postal_address.street_and_number = ILLStringHelper.generalString(db_addr.getPostal_street_and_number());
      retval.postal_address.post_office_box = ILLStringHelper.generalString(db_addr.getPostal_po_box());
      retval.postal_address.city = ILLStringHelper.generalString(db_addr.getPostal_city());
      retval.postal_address.region = ILLStringHelper.generalString(db_addr.getPostal_region());
      retval.postal_address.country = ILLStringHelper.generalString(db_addr.getPostal_country());
      retval.postal_address.postal_code = ILLStringHelper.generalString(db_addr.getPostal_postcode());

      if ( db_addr.getElectronic_service_address() != null ) {
        retval.electronic_address = new System_Address_type();
        retval.electronic_address.telecom_service_identifier = 
                         ILLStringHelper.generalString(db_addr.getElectronic_service_identifier());
        retval.electronic_address.telecom_service_address =
                         ILLStringHelper.generalString(db_addr.getElectronic_service_address());
      }
    }

    return retval;

  }

  private static EXTERNAL_type generateIPIGSystemNumberExternal(IPIGSystemNumber data) {
    EXTERNAL_type result = null;

    if ( data != null ) {

      com.k_int.openrequest.isoill.gen.IPIG_System_Number.type_inline59_type sysno_type = 
        new com.k_int.openrequest.isoill.gen.IPIG_System_Number.type_inline59_type();

      if ( data.well_known_system_id == 0 ) {
        // not well known, must be other
        sysno_type.which = com.k_int.openrequest.isoill.gen.IPIG_System_Number.type_inline59_type.other_CID;
        // sysno_type.o = ILLStringHelper.generalString(data.other_system_id);
        sysno_type.o = new System_Id_type(
                         new Person_Or_Institution_Symbol_type(
                           Person_Or_Institution_Symbol_type.institution_symbol_CID,
                           new ILL_String_type(ILL_String_type.generalstring_var_CID,data.other_system_id)),
                         null);

      }
      else {
        // well known
        sysno_type.which = com.k_int.openrequest.isoill.gen.IPIG_System_Number.type_inline59_type.well_known_CID;
        sysno_type.o = BigInteger.valueOf(data.well_known_system_id);
      }
 
      ArrayList list = new ArrayList();
      list.add(
        new com.k_int.openrequest.isoill.gen.IPIG_System_Number.IPIG_system_numberItem58_type(
          sysno_type,
          ILLStringHelper.generalString(data.database_id),
          ILLStringHelper.generalString(data.system_no)));

      result =
        new EXTERNAL_type (
          new int[] {1,0,10161,6,1},
          null, // BigInteger indirect reference
          null, // String data value descriptor
          new encoding_inline0_type( encoding_inline0_type.single_asn1_type_CID, list)
        );
    }

    return result;
  }

  private static ArrayList encodeSupplementalItemDescriptions() {
    return null;
  }
}
