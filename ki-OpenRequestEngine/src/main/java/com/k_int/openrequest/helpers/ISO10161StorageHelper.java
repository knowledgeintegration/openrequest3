/**
 * Title:       ISO10161StorageHelper
 * @version:    $Id: ISO10161StorageHelper.java,v 1.22 2005/07/11 12:29:14 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Take an iso PDU and store it, returning a message id.
 */

package com.k_int.openrequest.helpers;

import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.hibernate.*;
import java.util.Properties;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import java.util.*;
import java.math.BigInteger;
import org.jzkit.a2j.gen.AsnUseful.*;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ILL_Supplemental_Client_Info.*;
import org.apache.commons.logging.*;

public class ISO10161StorageHelper
{
  private static Log log = LogFactory.getLog(ISO10161StorageHelper.class);

  public static ILLTransaction lookupTransaction(Session sess,
                                                 Location location,
                                                 Object pdu) throws HelperException
  {
    log.debug("lookupTransaction...");

    ILLTransaction retval = null;

    try
    {
      java.lang.reflect.Field requester_id_field = pdu.getClass().getDeclaredField("requester_id");
      java.lang.reflect.Field transaction_id_field = pdu.getClass().getDeclaredField("transaction_id");
      Transaction_Id_type transaction_id = (Transaction_Id_type) transaction_id_field.get(pdu);
      System_Id_type requester_id = (System_Id_type) requester_id_field.get(pdu);

      // Extract tgq, tq, stq and initial_requester_symbol from the PDU
      String tgq=ILLStringHelper.extract((ILL_String_type)transaction_id.transaction_group_qualifier);
      String tq=ILLStringHelper.extract((ILL_String_type)transaction_id.transaction_qualifier);
      String stq=ILLStringHelper.extract((ILL_String_type)transaction_id.sub_transaction_qualifier);
      String requester_symbol = null;

      System_Id_type initial_requester_id = transaction_id.initial_requester_id;

      if ( initial_requester_id != null )
      {
        requester_symbol = ILLStringHelper.extract(
                (ILL_String_type)initial_requester_id.person_or_institution_symbol.o);
      }
      else
      {
        requester_symbol = ILLStringHelper.extract(
                (ILL_String_type)requester_id.person_or_institution_symbol.o);
      }

      if ( stq != null )
      {
        retval = DBHelper.lookupTransaction(sess,
                                            location.getId(),
                                            requester_symbol,
                                            tgq,
                                            tq,
                                            stq);
      }
      else
      {
        retval = DBHelper.lookupTransaction(sess,
                                            location.getId(),
                                            requester_symbol,
                                            tgq,
                                            tq);
      }
    }
    catch ( Exception e )
    {
      log.warn("Problem storring message: ",e);
      throw new HelperException(e.toString());
    }
    finally
    {
    }

    return retval;
  }


  public static MessageHeader store(Session sess,
                                     Location location,
                                     ILL_APDU_type pdu,
                                     com.k_int.openrequest.isoill.ILLSystemAddress sender,
                                     com.k_int.openrequest.isoill.ILLSystemAddress recipient,
                                     OIDRegister reg) throws HelperException {

    MessageHeader retval = null;

    try {
      ILLTransaction parent = lookupTransaction(sess,location,pdu.o);

      if ( parent != null )
        retval = store(pdu,sess,parent,sender,recipient, reg);
      else
       throw new HelperException("unable to locate parent");
    }
    catch ( Exception e ) {
      log.warn("Problem storring message: ",e);
      throw new HelperException(e.toString());
    }
    finally {
    }

    return retval;
  }

  public static MessageHeader store(ILL_APDU_type pdu, 
                                     Session sess,
                                     ILLTransaction owner,
                                     com.k_int.openrequest.isoill.ILLSystemAddress sender,
                                     com.k_int.openrequest.isoill.ILLSystemAddress recipient,
                                     OIDRegister reg) throws java.sql.SQLException, org.hibernate.HibernateException {

    log.info("store message "+pdu.which+" from "+sender+" to "+recipient);
    MessageHeader retval = null;
    ArrayList extensions = null;

    switch ( pdu.which ) {
      case ILL_APDU_type.ill_request_var_CID:
        log.debug("ISO10161StorageHelper::store ill request");
        retval = storeRequest((ILL_Request_type) pdu.o, sess, owner);
        extensions = ((ILL_Request_type)pdu.o).iLL_request_extensions;
        break;
      case ILL_APDU_type.forward_notification_var_CID:
        log.debug("ISO10161StorageHelper::store ill forward notification");
        retval = storeForward((Forward_Notification_type) pdu.o, sess, owner);
        extensions = ((Forward_Notification_type)pdu.o).forward_notification_extensions;
        break;
      case ILL_APDU_type.shipped_var_CID:
        log.debug("ISO10161StorageHelper::store ill shipped");
        retval = storeShipped((Shipped_type) pdu.o, sess, owner);
        extensions = ((Shipped_type)pdu.o).shipped_extensions;
        break;
      case ILL_APDU_type.ill_answer_var_CID:
        log.debug("ISO10161StorageHelper::store ill answer");
        retval = storeAnswer((ILL_Answer_type) pdu.o, sess, owner);
        extensions = ((ILL_Answer_type)pdu.o).ill_answer_extensions;
        break;
      case ILL_APDU_type.conditional_reply_var_CID:
        log.debug("ISO10161StorageHelper::store ill conditional reply");
        retval = storeConditionalReply((Conditional_Reply_type) pdu.o, sess, owner);
        extensions = ((Conditional_Reply_type)pdu.o).conditional_reply_extensions;
        break;
      case ILL_APDU_type.cancel_var_CID:
        log.debug("ISO10161StorageHelper::store ill cancel");
        retval = storeCancel((Cancel_type) pdu.o, sess, owner);
        extensions = ((Cancel_type)pdu.o).cancel_extensions;
        break;
      case ILL_APDU_type.cancel_reply_var_CID:
        log.debug("ISO10161StorageHelper::store ill cancel reply");
        retval = storeCancelReply((Cancel_Reply_type) pdu.o, sess, owner);
        extensions = ((Cancel_Reply_type)pdu.o).cancel_reply_extensions;
        break;
      case ILL_APDU_type.received_var_CID:
        log.debug("ISO10161StorageHelper::store ill received");
        retval = storeReceived((Received_type) pdu.o, sess, owner);
        extensions = ((Received_type)pdu.o).received_extensions;
        break;
      case ILL_APDU_type.recall_var_CID:
        log.debug("ISO10161StorageHelper::store ill recall");
        retval = storeRecall((Recall_type) pdu.o, sess, owner);
        extensions = ((Recall_type)pdu.o).recall_extensions;
        break;
      case ILL_APDU_type.returned_var_CID:
        log.debug("ISO10161StorageHelper::store ill recall");
        retval = storeReturned((Returned_type) pdu.o, sess, owner);
        extensions = ((Returned_type)pdu.o).returned_extensions;
        break;
      case ILL_APDU_type.checked_in_var_CID:
        log.debug("ISO10161StorageHelper::store ill recall");
        retval = storeCheckedIn((Checked_In_type) pdu.o, sess, owner);
        extensions = ((Checked_In_type)pdu.o).checked_in_extensions;
        break;
      case ILL_APDU_type.overdue_var_CID:
        log.debug("ISO10161StorageHelper::store ill recall");
        retval = storeOverdue((Overdue_type) pdu.o, sess, owner);
        extensions = ((Overdue_type)pdu.o).overdue_extensions;
        break;
      case ILL_APDU_type.renew_var_CID:
        log.debug("ISO10161StorageHelper::store ill renew");
        retval = storeRenew((Renew_type) pdu.o, sess, owner);
        extensions = ((Renew_type)pdu.o).renew_extensions;
        break;
      case ILL_APDU_type.renew_answer_var_CID:
        log.debug("ISO10161StorageHelper::store ill renew answer");
        retval = storeRenewAnswer((Renew_Answer_type) pdu.o, sess, owner);
        extensions = ((Renew_Answer_type)pdu.o).renew_answer_extensions;
        break;
      case ILL_APDU_type.lost_var_CID:
        log.debug("ISO10161StorageHelper::store ill lost");
        retval = storeLost((Lost_type) pdu.o, sess, owner);
        extensions = ((Lost_type)pdu.o).lost_extensions;
        break;
      case ILL_APDU_type.damaged_var_CID:
        log.debug("ISO10161StorageHelper::store ill damaged");
        retval = storeDamaged((Damaged_type) pdu.o, sess, owner);
        extensions = ((Damaged_type)pdu.o).damaged_extensions;
        break;
      case ILL_APDU_type.message_var_CID:
        log.debug("ISO10161StorageHelper::store ill message");
        retval = storeMessage((Message_type) pdu.o, sess, owner);
        extensions = ((Message_type)pdu.o).message_extensions;
        break;
      case ILL_APDU_type.status_query_var_CID:
        log.debug("ISO10161StorageHelper::store ill status query");
        retval = storeStatusQuery((Status_Query_type) pdu.o, sess, owner);
        extensions = ((Status_Query_type)pdu.o).status_query_extensions;
        break;
      case ILL_APDU_type.status_or_error_report_var_CID:
        log.debug("ISO10161StorageHelper::store ill status or error report");
        retval = storeStatusOrErrorReport((Status_Or_Error_Report_type) pdu.o, sess, owner);
        extensions = ((Status_Or_Error_Report_type)pdu.o).status_or_error_report_extensions;
        break;
      case ILL_APDU_type.expired_var_CID:
        log.debug("ISO10161StorageHelper::store ill expired");
        retval = storeExpired((Expired_type) pdu.o, sess, owner);
        extensions = ((Expired_type)pdu.o).expired_extensions;
        break;
      default:
        log.debug("unhandled message type for storage");
        break;
    }

    if ( retval != null ) {
      storeExtensions(sess, owner, retval, extensions, reg);
      sess.update(retval);
      sess.update(owner);
    }

    log.debug("Leaving store message...");
    return retval;
  }

  public static void setGenericParams(Object pdu, 
                                      MessageHeader msg,
                                      Session sess) {
    try {
      msg.setTimestamp(new java.sql.Timestamp(System.currentTimeMillis()));

      java.lang.reflect.Field protocol_version_num_field = pdu.getClass().getDeclaredField("protocol_version_num");
      java.lang.reflect.Field transaction_id_field = pdu.getClass().getDeclaredField("transaction_id");
      java.lang.reflect.Field service_date_time_field = pdu.getClass().getDeclaredField("service_date_time");
      java.lang.reflect.Field requester_id_field = pdu.getClass().getDeclaredField("requester_id");
      java.lang.reflect.Field responder_id_field = pdu.getClass().getDeclaredField("responder_id");

      BigInteger protocol_version_num = (BigInteger) protocol_version_num_field.get(pdu);
      Transaction_Id_type transaction_id = (Transaction_Id_type) transaction_id_field.get(pdu);
      Service_Date_Time_type service_date_time = (Service_Date_Time_type) service_date_time_field.get(pdu);
      System_Id_type requester_id = (System_Id_type) requester_id_field.get(pdu);
      System_Id_type responder_id = (System_Id_type) responder_id_field.get(pdu);

      if ( protocol_version_num != null )
        msg.setProtocolVersionNumber(protocol_version_num.longValue());
    
      if ( transaction_id != null )
        storeTransactionId(msg, transaction_id, sess);

      if ( service_date_time != null )
        storeServiceDateTime(msg, service_date_time, sess);

      if ( requester_id != null )
        storeRequesterId(msg, requester_id, sess);

      if ( responder_id != null )
        storeResponderId(msg, responder_id, sess);
    }
    catch ( java.lang.NoSuchFieldException nsfe ) {
      log.warn("Problem",nsfe);
      nsfe.printStackTrace();
    }
    catch ( java.lang.IllegalAccessException iae ) {
      iae.printStackTrace();
      log.warn("Problem",iae);
    }
  }

      // OK.. Look inside the pdu and figure out what kind of message we are creating.

  public static ISO10161RequestMessageDetails storeRequest(ILL_Request_type request, 
                                                           Session sess,
                                                           ILLTransaction owner) throws java.sql.SQLException,
                                                                                        org.hibernate.HibernateException

  {
    // ISO10161RequestMessage msg = new ISO10161RequestMessage();

    ISO10161RequestMessageDetails details = new ISO10161RequestMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setIsoRequestMessageDetails(details);

    // Firstly, store any generic ILL message params.
    setGenericParams(request, details, sess);

    if ( request.service_date_time.date_time_of_original_service != null ) {
      owner.setOriginalServiceDate(request.service_date_time.date_time_of_original_service.date);
      owner.setOriginalServiceTime(request.service_date_time.date_time_of_original_service.time);
    }
    else {
      owner.setOriginalServiceDate(request.service_date_time.date_time_of_this_service.date);
      owner.setOriginalServiceTime(request.service_date_time.date_time_of_this_service.time);
    }
    
    if ( request.transaction_type != null )
      details.setTransactionType(request.transaction_type.longValue());

    RequiredItemDetails grd = new RequiredItemDetails();

    details.setRequiredItemDetails(grd);

    if ( request.delivery_address != null ) {
      details.setDeliveryAddress( storeAddress(request.delivery_address, sess) );
      owner.getTransactionGroup().setCurrentDeliveryAddress(details.getDeliveryAddress());
    }

    if ( request.delivery_service != null )
      storeDeliveryService(grd, request.delivery_service, sess);

    if ( request.billing_address != null ) {
      details.setBillingAddress( storeAddress(request.billing_address, sess) );
      owner.getTransactionGroup().setCurrentBillingAddress(details.getBillingAddress());
    }

    if ( ( request.iLL_service_type != null ) && ( request.iLL_service_type.size() > 0 ) ) {
      // ToDo: Need to get array in DB

      BigInteger first = (BigInteger) request.iLL_service_type.get(0);
      // If we get a received but no shipped, lets assume that we were shipped the required
      // service type... Our received service type should verity this?
      owner.setShippedServiceType(first.intValue());
      

      for ( Iterator i = request.iLL_service_type.iterator(); i.hasNext(); ) {
        BigInteger service_type = (BigInteger) i.next();
        grd.getService_types().add(new Integer(service_type.intValue()));
      }
    }

    // TODO
    if ( request.responder_specific_service != null )
      storeResponderSpecificService(details, request.responder_specific_service, sess);

    if ( request.requester_optional_messages != null )
      storeRequesterOptionalMessages(details, request.requester_optional_messages, sess);

    if ( request.search_type != null )
      storeSearchType(owner, grd, request.search_type, sess);

    if ( request.supply_medium_info_type != null )
    {
      List medium_info = grd.getSupplyMediumInfoType();

      for( Iterator i = request.supply_medium_info_type.iterator(); i.hasNext(); ) {
        Supply_Medium_Info_Type_type smit = (Supply_Medium_Info_Type_type)i.next();

        if ( smit.supply_medium_type != null ) {
          if ( smit.medium_characteristics != null )
            medium_info.add(new MediumInfo(smit.supply_medium_type.longValue(), smit.medium_characteristics.o.toString()));
          else
            medium_info.add(new MediumInfo(smit.supply_medium_type.longValue(), null ));
        }
        else {
          log.debug("Supply Medium Type was NULL");
        }
      }
    }

    if ( request.place_on_hold != null )
      grd.setPlaceOnHold(request.place_on_hold.longValue());

    if ( request.client_id != null )
      storeClientId(grd, request.client_id, sess);

    if ( request.item_id != null )
      storeItemId(grd, request.item_id, sess);

    // storeSupplementalItemDesc(msg, request.supplemental_item_description, sess);

    if ( request.cost_info_type != null )
      storeCostInfoType(grd, request.cost_info_type, sess);

    if ( ( request.copyright_compliance != null ) &&
         ( request.copyright_compliance.o != null ) )
      grd.setCopyrightCompliance(request.copyright_compliance.o.toString());

    if ( request.third_party_info_type != null )
      storeThirdPartyInfo(details, request.third_party_info_type, sess);

    if ( request.retry_flag != null )
      details.setRetryFlag(request.retry_flag.booleanValue());

    if ( request.forward_flag != null )
      details.setForwardFlag(request.forward_flag.booleanValue());

    if ( request.requester_note != null )
      details.setRequesterNote(request.requester_note.o.toString());

    if ( request.forward_note != null )
      details.setForwardNote(request.forward_note.o.toString());

    // storeExtensions(msg, request.iLL_request_extensions, sess);

    sess.save(details);

    return details;
  }

  public static ISO10161AnswerMessageDetails storeAnswer(ILL_Answer_type answer, 
                                                         Session sess,
                                                         ILLTransaction owner) throws java.sql.SQLException,
                                                                                      org.hibernate.HibernateException
  {
    // ISO10161AnswerMessage msg = new ISO10161AnswerMessage();

    ISO10161AnswerMessageDetails details = new ISO10161AnswerMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    // Firstly, store any generic ILL message params.
    setGenericParams(answer, details, sess);

    if ( answer.transaction_results != null )
      details.setTransactionResults(answer.transaction_results.intValue());

    if ( answer.results_explanation != null )
    {
      details.setResultsExplanationType(answer.results_explanation.which);
      switch(answer.results_explanation.which)
      {
        case results_explanation_inline5_type.conditional_results_CID:
          Conditional_Results_type cr = (Conditional_Results_type)answer.results_explanation.o;
          if ( cr.conditions != null )
            details.setConditionalConditions(cr.conditions.intValue());
          if ( cr.date_for_reply != null )
          {
            // details.setConditionalDateForReply(cr.date_for_reply);
            try { 
              details.setConditionalDateForReply(Constants.parseIsoDate(cr.date_for_reply)); 
              // owner.setExpiryTimer(details.getConditionalDateForReply());
            } catch ( java.text.ParseException pe ) {log.warn("Problem",pe);}
          }
          break;
        case results_explanation_inline5_type.retry_results_CID:
          log.debug("storing retry answer");
          Retry_Results_type rr = (Retry_Results_type)answer.results_explanation.o;
          if ( rr.reason_not_available != null ) {
            log.debug("Store Retry Answer Reason Not Available "+rr.reason_not_available.intValue()+" in db");
            details.setReasonNotAvailable(rr.reason_not_available.intValue());
          }
          break;
        case results_explanation_inline5_type.unfilled_results_CID:
          Unfilled_Results_type ur = (Unfilled_Results_type)answer.results_explanation.o;
          details.setReasonUnfilled(ur.reason_unfilled.intValue());
          break;
        case results_explanation_inline5_type.locations_results_CID:
          Locations_Results_type lr = (Locations_Results_type)answer.results_explanation.o;
          break;
        case results_explanation_inline5_type.will_supply_results_CID:
          Will_Supply_Results_type wsr = (Will_Supply_Results_type)answer.results_explanation.o;
          break;
        case results_explanation_inline5_type.hold_placed_results_CID:
          Hold_Placed_Results_type hpr = (Hold_Placed_Results_type)answer.results_explanation.o;
          break;
        case results_explanation_inline5_type.estimate_results_CID:
          Estimate_Results_type er = (Estimate_Results_type)answer.results_explanation.o;
          break;
      }
    }

    if ( answer.responder_note != null )
      details.setNote(answer.responder_note.o.toString());

    sess.save(details);
    return details;
  }


  public static void storeTransactionId(MessageHeader msg, Transaction_Id_type id, Session sess)
  {
    msg.setTgq(id.transaction_group_qualifier.o.toString());
    msg.setTq(id.transaction_qualifier.o.toString());

    if ( id.sub_transaction_qualifier != null )
      msg.setStq(id.sub_transaction_qualifier.o.toString());

    if ( id.initial_requester_id != null )
    {
      if ( id.initial_requester_id.person_or_institution_symbol != null )
      {
        msg.setInitial_requester_id_symbol_type(id.initial_requester_id.person_or_institution_symbol.which);
        msg.setInitial_requester_id_symbol(
                ((ILL_String_type)id.initial_requester_id.person_or_institution_symbol.o).o.toString());
      }
      if ( id.initial_requester_id.name_of_person_or_institution != null )
      {
        msg.setInitial_requester_name_type(id.initial_requester_id.name_of_person_or_institution.which);
        msg.setInitial_requester_name(
                ((ILL_String_type)id.initial_requester_id.name_of_person_or_institution.o).o.toString());
      }
    }
  }

  public static void storeServiceDateTime(MessageHeader msg, Service_Date_Time_type sdt, Session sess)
  {
    if ( sdt.date_time_of_this_service != null )
    {
      // log.debug("storeServiceDateTime:"+sdt.date_time_of_this_service.date+" "+sdt.date_time_of_this_service.time);

      msg.setStr_service_date(sdt.date_time_of_this_service.date);
      msg.setStr_service_time(sdt.date_time_of_this_service.time);
    }
    if ( sdt.date_time_of_original_service != null )
    {
      msg.setStr_original_service_date(sdt.date_time_of_original_service.date);
      msg.setStr_original_service_time(sdt.date_time_of_original_service.time);
    }
  }

  public static void storeRequesterId(MessageHeader msg, System_Id_type id, Session sess)
  {
    if ( id.person_or_institution_symbol != null )
    {
      msg.setRequester_id_symbol_type(id.person_or_institution_symbol.which);
      msg.setRequester_id_symbol(
               ((ILL_String_type)id.person_or_institution_symbol.o).o.toString());
    }
    if ( id.name_of_person_or_institution != null )
    {
      msg.setRequester_id_name_type(id.name_of_person_or_institution.which);
      msg.setRequester_id_name(
               ((ILL_String_type)id.name_of_person_or_institution.o).o.toString());
    }
  }

  public static void storeResponderId(MessageHeader msg, System_Id_type id, Session sess)
  {
    if ( id.person_or_institution_symbol != null )
    {
      msg.setResponder_id_symbol_type(id.person_or_institution_symbol.which);
      msg.setResponder_id_symbol(
               ((ILL_String_type)id.person_or_institution_symbol.o).o.toString());
    }
    if ( id.name_of_person_or_institution != null )
    {
      msg.setResponder_id_name_type(id.name_of_person_or_institution.which);
      msg.setResponder_id_name(
               ((ILL_String_type)id.name_of_person_or_institution.o).o.toString());
    }
  }

  // public static void storeAddress(msg, request.delivery_address, sess);
  public static ISO10161Address storeAddress(Delivery_Address_type addr, Session sess)
  {
    ISO10161Address address = new ISO10161Address();
    if (addr.postal_address != null)
    {
      if ( addr.postal_address.name_of_person_or_institution != null )
      {
       address.setPostal_name_type(addr.postal_address.name_of_person_or_institution.which);
       address.setPostal_name(ILLStringHelper.extract((ILL_String_type)addr.postal_address.name_of_person_or_institution.o));
      }

      if ( addr.postal_address.extended_postal_delivery_address != null )
        address.setPostal_extended_address(addr.postal_address.extended_postal_delivery_address.o.toString());

      if ( addr.postal_address.street_and_number != null )
        address.setPostal_street_and_number(addr.postal_address.street_and_number.o.toString());

      if ( addr.postal_address.post_office_box != null )
        address.setPostal_po_box(addr.postal_address.post_office_box.o.toString());

      if ( addr.postal_address.city != null )
        address.setPostal_city(addr.postal_address.city.o.toString());

      if ( addr.postal_address.region != null )
        address.setPostal_region(addr.postal_address.region.o.toString());

      if ( addr.postal_address.country != null )
        address.setPostal_country(addr.postal_address.country.o.toString());

      if ( addr.postal_address.postal_code != null )
        address.setPostal_postcode(addr.postal_address.postal_code.o.toString());
    }

    if (addr.electronic_address != null)
    {
      if ( addr.electronic_address.telecom_service_identifier != null )
        address.setElectronic_service_identifier(
               addr.electronic_address.telecom_service_identifier.o.toString());
      if ( addr.electronic_address.telecom_service_address != null )
        address.setElectronic_service_address(addr.electronic_address.telecom_service_address.o.toString());
    }

    return address;
  }

  public static void storeDeliveryService(RequiredItemDetails dets, 
                                          Delivery_Service_type service, 
                                          Session sess)
  {
    dets.setDeliveryServiceType(service.which);

    switch ( service.which )
    {
      case Delivery_Service_type.physical_delivery_CID:
        dets.setTransportationMode(((ILL_String_type)service.o).o.toString());
        break;
      case Delivery_Service_type.electronic_delivery_CID:
      {
        List electronic_delivery_info = (List)service.o;

        if ( ( electronic_delivery_info != null ) && ( electronic_delivery_info.size() > 0 ) ) {
          // TODO
          Electronic_Delivery_Service_type e = (Electronic_Delivery_Service_type) electronic_delivery_info.get(0);
          if ( e.e_delivery_service != null ) {
          }
  
          if ( e.document_type != null ) {
          }
  
          if ( e.e_delivery_description != null ) {
            dets.setEDeliveryDescripton(ILLStringHelper.extract(e.e_delivery_description));
          }
  
          if ( e.e_delivery_details != null ) {
            dets.setEDeliveryDetsType(e.e_delivery_details.which);
            if ( e.e_delivery_details.which == e_delivery_details_inline29_type.e_delivery_address_CID ) {
              System_Address_type addr = (System_Address_type) e.e_delivery_details.o;
              dets.setEDeliveryDetsID(ILLStringHelper.extract(addr.telecom_service_identifier));
                dets.setEDeliveryDetsAddr(ILLStringHelper.extract(addr.telecom_service_address));
            }
            else if ( e.e_delivery_details.which == e_delivery_details_inline29_type.e_delivery_id_CID ) {
              System_Id_type id = (System_Id_type) e.e_delivery_details.o;
            }
          }
  
          if ( e.name_or_code != null ) {
            dets.setEDeliveryNameOrCode(ILLStringHelper.extract(e.name_or_code));
          }
  
          if ( e.delivery_time != null ) {
            dets.setEDeliveryTime(e.delivery_time);
          }
        }
        break;
      }
    }
  }

  public static void storeRequesterOptionalMessages(ISO10161RequestMessageDetails dets, 
                                                    Requester_Optional_Messages_Type_type romt, 
                                                    Session session)
  {
    if ( romt.can_send_RECEIVED != null )
      dets.setCanSendReceived(romt.can_send_RECEIVED);
    
    if ( romt.can_send_RETURNED != null )
      dets.setCanSendReturned(romt.can_send_RETURNED);
      
    if ( romt.requester_SHIPPED != null )
      dets.setRequesterShipped(new Long(romt.requester_SHIPPED.longValue()));

    if ( romt.requester_CHECKED_IN != null )
      dets.setRequesterCheckedIn(new Long(romt.requester_CHECKED_IN.longValue()));
  }

  public static void storeSearchType(ILLTransaction owner,
                                     RequiredItemDetails dets, 
                                     Search_Type_type search_type,
                                     Session sess)
  {
    if ( ( search_type.level_of_service != null ) &&
         ( search_type.level_of_service.o != null ) )
      dets.setLevelOfService(search_type.level_of_service.o.toString());

    try
    {
      if ( search_type.need_before_date != null )
      {
        dets.setNeedBeforeDate(Constants.parseIsoDate(search_type.need_before_date));
        owner.getTransactionGroup().setNeedByDate(Constants.parseIsoDate(search_type.need_before_date));
      }

      if ( search_type.expiry_date != null )
        dets.setExpiryDate(Constants.parseIsoDate(search_type.expiry_date));
    }
    catch ( Exception e )
    {
      log.warn("Problem parsing dates...",e);
    }

    if ( search_type.expiry_flag != null )
    {
      dets.setExpiryFlag(search_type.expiry_flag.longValue());
      switch ( (int) (search_type.expiry_flag.longValue()) )
      {
        case 1:  // need-before-date
          owner.setExpiryTimer(dets.getNeedBeforeDate());
          break;
        case 2:  // other date
          owner.setExpiryTimer(dets.getExpiryDate());
          break;
        case 3:  // no expiry
        default:
          break;
      }
    }
  }

  public static void storeClientId(RequiredItemDetails dets, 
                                   Client_Id_type client_id,
                                   Session sess)
  {
    if ( client_id.client_name != null )
      dets.setClientName(client_id.client_name.o.toString());

    if ( client_id.client_status != null )
      dets.setClientStatus(client_id.client_status.o.toString());

    if ( client_id.client_identifier != null )
      dets.setClientIdentifier(client_id.client_identifier.o.toString());
  }

  public static void storeItemId(RequiredItemDetails dets, 
                                 Item_Id_type item_id,
                                 Session sess)
  {
    if ( item_id.item_type != null )
      dets.setItemType(item_id.item_type.longValue());

    if ( item_id.held_medium_type != null )
      dets.setHeldMediumType(item_id.held_medium_type.longValue());

    if ( item_id.call_number != null )
      dets.setCallNumber(item_id.call_number.o.toString());

    if ( item_id.author != null )
      dets.setAuthor(item_id.author.o.toString());

    if ( item_id.title != null )
      dets.setTitle(item_id.title.o.toString());

    if ( item_id.sub_title != null )
      dets.setSubtitle(item_id.sub_title.o.toString());

    if ( item_id.sponsoring_body != null )
      dets.setSponsoringBody(item_id.sponsoring_body.o.toString());

    if ( item_id.place_of_publication != null )
      dets.setPlaceOfPublication(item_id.place_of_publication.o.toString());

    if ( item_id.publisher != null )
      dets.setPublisher(item_id.publisher.o.toString());

    if ( item_id.series_title_number != null )
      dets.setSeriesTitleNumber(item_id.series_title_number.o.toString());

    if ( item_id.volume_issue != null ) {
      dets.setVolume(item_id.volume_issue.o.toString());
      dets.setIssue(item_id.volume_issue.o.toString());
    }

    if ( item_id.edition != null )
      dets.setEdition(item_id.edition.o.toString());

    if ( item_id.publication_date != null )
      dets.setPublicationDate(item_id.publication_date.o.toString());

    if ( item_id.publication_date_of_component != null )
      dets.setPublicationDateOfComponent(item_id.publication_date_of_component.o.toString());

    if ( item_id.author_of_article != null )
      dets.setAuthorOfArticle(item_id.author_of_article.o.toString());

    if ( item_id.title_of_article != null )
      dets.setTitleOfArticle(item_id.title_of_article.o.toString());

    if ( item_id.pagination != null )
      dets.setPagination(item_id.pagination.o.toString());

    if ( item_id.national_bibliography_no != null ) {
      ArrayList l = (ArrayList) item_id.national_bibliography_no.encoding.o;
      com.k_int.openrequest.isoill.gen.IPIG_System_Number.IPIG_system_numberItem58_type ipig_sysno = 
                    (com.k_int.openrequest.isoill.gen.IPIG_System_Number.IPIG_system_numberItem58_type) l.get(0);
      IPIGSystemNumber nat_bib_no = null;
      if ( ipig_sysno.type != null ) {
        switch ( ipig_sysno.type.which ) {
          case com.k_int.openrequest.isoill.gen.IPIG_System_Number.type_inline59_type.well_known_CID:
            int well_known_system_id = ((BigInteger)(ipig_sysno.type.o)).intValue();
            nat_bib_no = new IPIGSystemNumber(well_known_system_id,
                                              null,
                                              ILLStringHelper.extract(ipig_sysno.database_ID),
                                              ILLStringHelper.extract(ipig_sysno.number));

            break;
          case com.k_int.openrequest.isoill.gen.IPIG_System_Number.type_inline59_type.other_CID :
            String other_system_id = null;
            nat_bib_no = new IPIGSystemNumber(0,
                                              other_system_id,
                                              ILLStringHelper.extract(ipig_sysno.database_ID),
                                              ILLStringHelper.extract(ipig_sysno.number));
            break;
        }
        dets.setNatBibNo(nat_bib_no);
      }
    }

    if ( item_id.iSBN != null ) {
      if ( item_id.iSBN.o.toString().length() > 20 )
        dets.setISBN(item_id.iSBN.o.toString().substring(0,19));
      else
        dets.setISBN(item_id.iSBN.o.toString());
    }

    if ( item_id.iSSN != null ) {
      if ( item_id.iSSN.o.toString().length() > 20 )
        dets.setISBN(item_id.iSSN.o.toString().substring(0,19));
      else
        dets.setISBN(item_id.iSSN.o.toString());
    }

    if ( item_id.system_no != null ) {
      ArrayList l = (ArrayList) item_id.system_no.encoding.o;
      com.k_int.openrequest.isoill.gen.IPIG_System_Number.IPIG_system_numberItem58_type ipig_sysno =
                    (com.k_int.openrequest.isoill.gen.IPIG_System_Number.IPIG_system_numberItem58_type) l.get(0);
      IPIGSystemNumber system_no = null;
      if ( ipig_sysno.type != null ) {
        switch ( ipig_sysno.type.which ) {
          case com.k_int.openrequest.isoill.gen.IPIG_System_Number.type_inline59_type.well_known_CID:
            int well_known_system_id = ((BigInteger)(ipig_sysno.type.o)).intValue();
            system_no = new IPIGSystemNumber(well_known_system_id,
                                              null,
                                              ILLStringHelper.extract(ipig_sysno.database_ID),
                                              ILLStringHelper.extract(ipig_sysno.number));

            break;
          case com.k_int.openrequest.isoill.gen.IPIG_System_Number.type_inline59_type.other_CID :
            String other_system_id = null;
            system_no = new IPIGSystemNumber(0,
                                              other_system_id,
                                              ILLStringHelper.extract(ipig_sysno.database_ID),
                                              ILLStringHelper.extract(ipig_sysno.number));
            break;
        }
        dets.setSystemNo(system_no);
      }
    }

    if ( item_id.additional_no_letters != null )
      dets.setAdditionalNoLetters(item_id.additional_no_letters.o.toString());

    if ( item_id.verification_reference_source != null )
      dets.setVerificationReferenceSource(item_id.verification_reference_source.o.toString());
  }

  public static void storeCostInfoType(RequiredItemDetails dets, 
                                       Cost_Info_Type_type cost_info,
                                       Session sess)
  {
    if ( cost_info.account_number != null )
      dets.setAccountNumber(cost_info.account_number.o.toString());

    if ( cost_info.maximum_cost != null )
    {
      if ( cost_info.maximum_cost.currency_code != null )
        dets.setMaxCostCurrencyCode(cost_info.maximum_cost.currency_code);
      if ( cost_info.maximum_cost.monetary_value != null )
        dets.setMaxCostString(cost_info.maximum_cost.monetary_value);
    }

    if ( cost_info.reciprocal_agreement != null )
      dets.setReciprocalAgreement(cost_info.reciprocal_agreement.booleanValue());

    if ( cost_info.will_pay_fee != null )
      dets.setWillPayFee(cost_info.will_pay_fee.booleanValue());

    if ( cost_info.payment_provided != null )
      dets.setPaymentProvided(cost_info.payment_provided.booleanValue());
  }

  public static void storeThirdPartyInfo(ISO10161RequestMessageDetails details, 
                                         Third_Party_Info_Type_type info,
                                         Session sess)
  {
    if ( info.permission_to_forward != null )
      details.setPermToForward(info.permission_to_forward.booleanValue());

    if ( info.permission_to_chain != null )
      details.setPermToChain(info.permission_to_chain.booleanValue());

    if ( info.permission_to_partition != null )
      details.setPermToPartition(info.permission_to_partition.booleanValue());

    if ( info.permission_to_change_send_to_list != null )
      details.setPermToChangeSendToList(info.permission_to_change_send_to_list.booleanValue());

    if ( info.initial_requester_address != null )
    {
      if ( info.initial_requester_address.telecom_service_identifier != null )
        details.setInitReqServIdent(info.initial_requester_address.telecom_service_identifier.o.toString());
      if ( info.initial_requester_address.telecom_service_address != null )
        details.setInitReqServAddr(info.initial_requester_address.telecom_service_address.o.toString());
    }

    if ( info.preference != null )
      details.setPreference(info.preference.longValue());

    if ( info.already_tried_list != null ) {
      // details.setAlreadyTriedList(info.already_tried_list);
      for( Iterator i = info.already_tried_list.iterator(); i.hasNext(); ) {
        long symbol_type = 0;
        long name_type = 0;
        String symbol = null;
        String name = null;

        System_Id_type already_tried_entry = (System_Id_type)i.next();

        if ( already_tried_entry.person_or_institution_symbol != null ) {
          symbol_type = already_tried_entry.person_or_institution_symbol.which;
          symbol = ((ILL_String_type)already_tried_entry.person_or_institution_symbol.o).o.toString();
        }

        if ( already_tried_entry.name_of_person_or_institution != null ) {
          name_type = already_tried_entry.name_of_person_or_institution.which;
          name = ((ILL_String_type)already_tried_entry.name_of_person_or_institution.o).o.toString();
        }

         details.getRota().add( new RotaEntry(true,
                                              symbol_type,
                                              symbol,
                                              null,
                                              name_type,
                                              name,
                                              null,
                                              null,
                                              null) );
      }
    }

    if ( info.send_to_list != null ) {
      // details.setSendToList(info.send_to_list);
      for( Iterator i = info.send_to_list.iterator(); i.hasNext(); ) {
        long symbol_type = 0;
        long name_type = 0;
        String symbol = null;
        String name = null;
        String account_number = null;
        String service_identifier = null;
        String service_address = null;

        Send_To_List_TypeItem34_type send_to_entry = (Send_To_List_TypeItem34_type)i.next();

        if ( send_to_entry.system_id != null )
        {
          if ( send_to_entry.system_id.person_or_institution_symbol != null )
          {
            symbol_type = send_to_entry.system_id.person_or_institution_symbol.which;
            symbol = ((ILL_String_type)send_to_entry.system_id.person_or_institution_symbol.o).o.toString();
          }

          if ( send_to_entry.system_id.name_of_person_or_institution != null )
          {
            name_type = send_to_entry.system_id.name_of_person_or_institution.which;
            name = ((ILL_String_type)send_to_entry.system_id.name_of_person_or_institution.o).o.toString();
          }
        }

        if ( send_to_entry.account_number != null )
          account_number = send_to_entry.account_number.o.toString();

        if ( send_to_entry.system_address != null )
        {
          if ( send_to_entry.system_address.telecom_service_identifier != null )
            service_identifier = send_to_entry.system_address.telecom_service_identifier.o.toString();
          if ( send_to_entry.system_address.telecom_service_address != null )
            service_address = send_to_entry.system_address.telecom_service_address.o.toString();
        }

        details.getRota().add( new RotaEntry(false,
                                             symbol_type,
                                             symbol,
                                             null,
                                             name_type,
                                             name,
                                             account_number,
                                             service_identifier,
                                             service_address) );
      }
    }
  }

  public static ISO10161ForwardMessageDetails storeForward(Forward_Notification_type forward, 
                                                           Session sess,
                                                           ILLTransaction owner) throws java.sql.SQLException,
                                                                                 org.hibernate.HibernateException
  {
    // ISO10161ForwardMessage msg = new ISO10161ForwardMessage();

    ISO10161ForwardMessageDetails details = new ISO10161ForwardMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    setGenericParams(forward, details, sess);

    if ( forward.responder_address != null )
    {
      if ( forward.responder_address.telecom_service_identifier != null )
        details.setTelecomServiceIdentifier(forward.responder_address.telecom_service_identifier.o.toString());
      if ( forward.responder_address.telecom_service_address != null )
        details.setTelecomServiceAddress(forward.responder_address.telecom_service_address.o.toString());
    }

    if ( forward.intermediary_id != null )
    {
      if ( forward.intermediary_id.person_or_institution_symbol != null )
      {
        details.setIntermediaryIdSymbolType(forward.intermediary_id.person_or_institution_symbol.which);
        details.setIntermediaryIdSymbol(
                ((ILL_String_type)forward.intermediary_id.person_or_institution_symbol.o).o.toString());
      }

      if ( forward.intermediary_id.name_of_person_or_institution != null )
      {
        details.setIntermediaryIdNameType(forward.intermediary_id.name_of_person_or_institution.which);
        details.setIntermediaryIdName(
                ((ILL_String_type)forward.intermediary_id.name_of_person_or_institution.o).o.toString());
      }
    }

    sess.save(details);
    return details;
  }

  public static ISO10161ConditionalMessageDetails storeConditionalReply(Conditional_Reply_type cond_rep, 
                                                                        Session sess,
                                                                        ILLTransaction owner) throws java.sql.SQLException,
                                                                                              org.hibernate.HibernateException
  {
    // ISO10161ConditionalMessage msg = new ISO10161ConditionalMessage();

    ISO10161ConditionalMessageDetails details = new ISO10161ConditionalMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    setGenericParams(cond_rep, details, sess);

    if ( cond_rep.answer != null )
      details.setAnswer(cond_rep.answer);

    if ( cond_rep.requester_note != null )
      details.setNote(cond_rep.requester_note.o.toString());

    sess.save(details);
    return details;
  }

  public static ISO10161CancelMessageDetails storeCancel(Cancel_type cancel, 
                                                         Session sess,
                                                         ILLTransaction owner) throws java.sql.SQLException,
                                                                                 org.hibernate.HibernateException
  {
    // ISO10161CancelMessage msg = new ISO10161CancelMessage();

    ISO10161CancelMessageDetails details = new ISO10161CancelMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    if ( cancel.requester_note != null )
      details.setNote(cancel.requester_note.o.toString());

    setGenericParams(cancel, details, sess);
    sess.save(details);
    return details;
  }

  public static ISO10161CancelReplyMessageDetails storeCancelReply(Cancel_Reply_type cond_rep, 
                                                                   Session sess,
                                                                   ILLTransaction owner) throws java.sql.SQLException,
                                                                                           org.hibernate.HibernateException
  {
    // ISO10161CancelReplyMessage msg = new ISO10161CancelReplyMessage();
    ISO10161CancelReplyMessageDetails details = new ISO10161CancelReplyMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    details.setNote(ILLStringHelper.extract(cond_rep.responder_note));
    details.setAnswer(cond_rep.answer.booleanValue());

    setGenericParams(cond_rep, details, sess);
    sess.save(details);
    return details;
  }


  public static ISO10161ShippedMessageDetails storeShipped(Shipped_type shipped, 
                                                           Session sess,
                                                           ILLTransaction owner) throws java.sql.SQLException,
                                                                                 org.hibernate.HibernateException
  {
    log.debug("enter storeShipped");

    // ISO10161ShippedMessage msg = new ISO10161ShippedMessage();

    log.debug("create shipped details");

    ISO10161ShippedMessageDetails details = new ISO10161ShippedMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    log.debug("Set generic params");
    setGenericParams(shipped, details, sess);

    log.debug("Set shipped service type");
    // Set the ShippedServiceType property of the transaction
    owner.setShippedServiceType(shipped.shipped_service_type.intValue());

    log.debug("Processing responder address");
    if ( shipped.responder_address != null )
    {
      if ( shipped.responder_address.telecom_service_identifier != null )
        details.setResponderTelecomServiceIdentifier(shipped.responder_address.telecom_service_identifier.o.toString());
      if ( shipped.responder_address.telecom_service_address != null )
        details.setResponderTelecomServiceAddress(shipped.responder_address.telecom_service_address.o.toString());
    }

    log.debug("Processing intermediary id");
    if ( shipped.intermediary_id != null )
    {
      if ( shipped.intermediary_id.person_or_institution_symbol != null )
      {
        details.setIntermediaryIdSymbolType(shipped.intermediary_id.person_or_institution_symbol.which);
        details.setIntermediaryIdSymbol(
                 ((ILL_String_type)shipped.intermediary_id.person_or_institution_symbol.o).o.toString());
      }
      if ( shipped.intermediary_id.name_of_person_or_institution != null )
      {
        details.setIntermediaryIdNameType(shipped.intermediary_id.name_of_person_or_institution.which);
        details.setIntermediaryIdName(
                 ((ILL_String_type)shipped.intermediary_id.name_of_person_or_institution.o).o.toString());
      }
    }

    log.debug("Processing supplier id");
    if ( shipped.supplier_id != null )
    {
      if ( shipped.supplier_id.person_or_institution_symbol != null )
      {
        details.setSupplierIdSymbolType(shipped.supplier_id.person_or_institution_symbol.which);
        details.setSupplierIdSymbol(
                 ((ILL_String_type)shipped.supplier_id.person_or_institution_symbol.o).o.toString());
      }
      if ( shipped.supplier_id.name_of_person_or_institution != null )
      {
        details.setSupplierIdNameType(shipped.supplier_id.name_of_person_or_institution.which);
        details.setSupplierIdName(
                 ((ILL_String_type)shipped.supplier_id.name_of_person_or_institution.o).o.toString());
      }
    }


    log.debug("Processing client id");
    if ( shipped.client_id != null )
    {
      if ( shipped.client_id.client_name != null )
        details.setClientName(shipped.client_id.client_name.o.toString());

      if ( shipped.client_id.client_status != null )
        details.setClientStatus(shipped.client_id.client_status.o.toString());

      if ( shipped.client_id.client_identifier != null )
        details.setClientIdentifier(shipped.client_id.client_identifier.o.toString());
    }

    log.debug("Processing transaction type");
    if ( shipped.transaction_type != null )
      details.setTransactionType(shipped.transaction_type.intValue());

    log.debug("Processing shipped service type");
    if ( shipped.shipped_service_type != null )
      details.setShippedServiceType(shipped.shipped_service_type.intValue());

    if ( shipped.responder_optional_messages != null )
    {
      log.debug("Processing can send shipped");
      details.setCanSendShipped(shipped.responder_optional_messages.can_send_SHIPPED);

      log.debug("Processing can send checked_in");
      details.setCanSendCheckedIn(shipped.responder_optional_messages.can_send_CHECKED_IN);

      if ( shipped.responder_optional_messages.responder_RETURNED != null )
        details.setResponderReceived(shipped.responder_optional_messages.responder_RETURNED.intValue());

      if ( shipped.responder_optional_messages.responder_RECEIVED != null )
        details.setResponderReceived(shipped.responder_optional_messages.responder_RECEIVED.intValue());
    }

    log.debug("Processing Supply details");
    if ( shipped.supply_details != null ) {
      if ( shipped.supply_details.date_shipped != null )
      {
        try { 
          details.setDateShipped(Constants.parseIsoDate(shipped.supply_details.date_shipped)); 
        } catch ( java.text.ParseException pe ) {log.warn("Problem",pe);}
      }

      if ( shipped.supply_details.date_due != null )
      {
        try {
          log.debug("Extracting date due and renewable...."+shipped.supply_details.date_due);
          details.setDateDue(Constants.parseIsoDate(shipped.supply_details.date_due.date_due_field)); 
        } catch ( java.text.ParseException pe ) {log.warn("Problem",pe);}
        details.setRenewable(shipped.supply_details.date_due.renewable);
      }
      if ( shipped.supply_details.chargeable_units != null )
        details.setChargeableUnits(shipped.supply_details.chargeable_units.intValue());

      if ( shipped.supply_details.cost != null )
      {
        if ( shipped.supply_details.cost.currency_code != null )
          details.setCurrencyCode(shipped.supply_details.cost.currency_code);
        if ( shipped.supply_details.cost.monetary_value != null )
          details.setMonetaryValue(shipped.supply_details.cost.monetary_value);
      }

      log.debug("Processing Shipped Conditions");

      if ( shipped.supply_details.shipped_conditions != null )
        details.setShippedConditions(shipped.supply_details.shipped_conditions.intValue());

      log.debug("Processing Shipped Via");
      if ( shipped.supply_details.shipped_via != null )
      {
        details.setShippedViaType(shipped.supply_details.shipped_via.which);

        if ( shipped.supply_details.shipped_via.which == shipped_via_inline37_type.physical_delivery_CID )
        {
          details.setTransportationMode(((ILL_String_type)shipped.supply_details.shipped_via.o).o.toString());
        }
        else
        {
          // TODO
          log.debug("NOT_YET_IMPLEMENTED: Storage of Electronic Delivery Service Type");
          Electronic_Delivery_Service_type e = (Electronic_Delivery_Service_type) shipped.supply_details.shipped_via.o;
          if ( e.e_delivery_service != null );
          if ( e.document_type != null );
          if ( e.e_delivery_description != null );
          if ( e.e_delivery_details != null );
          if ( e.name_or_code != null );
          if ( e.delivery_time != null );
        }
      }
      else {
          log.debug("Shipped message did not contain supply details. No date due etc.");
      }


      log.debug("Processing Supply Details Insured For");
      if ( shipped.supply_details.insured_for != null )
      {
        details.setInsuredForCurrencyCode(shipped.supply_details.insured_for.currency_code);
        details.setInsuredForMonetaryValue(shipped.supply_details.insured_for.monetary_value);
      }

      if ( shipped.supply_details.return_insurance_require != null )
      {
        details.setReturnInsuredForCurrencyCode(shipped.supply_details.return_insurance_require.currency_code);
        details.setReturnInsuredForMonetaryValue(shipped.supply_details.return_insurance_require.monetary_value);
      }
    }

    log.debug("Processing Return Address");
    if ( shipped.return_to_address != null )
    {
      Postal_Address_type addr = shipped.return_to_address;

      if ( addr.name_of_person_or_institution != null )
      {
        details.setPostalNameType(addr.name_of_person_or_institution.which);
        details.setPostalName(((ILL_String_type)addr.name_of_person_or_institution.o).o.toString());
      }

      if ( addr.extended_postal_delivery_address != null )
        details.setPostalExtendedAddress(addr.extended_postal_delivery_address.o.toString());

      if ( addr.street_and_number != null )
        details.setPostalStreetAndNumber(addr.street_and_number.o.toString());

      if ( addr.post_office_box != null )
        details.setPostalPOBox(addr.post_office_box.o.toString());

      if ( addr.city != null )
        details.setPostalCity(addr.city.o.toString());

      if ( addr.region != null )
        details.setPostalRegion(addr.region.o.toString());

      if ( addr.country != null )
        details.setPostalCountry(addr.country.o.toString());

      if ( addr.postal_code != null )
        details.setPostalPostcode(addr.postal_code.o.toString());
    }

    if ( shipped.responder_note != null )
      details.setNote(shipped.responder_note.o.toString());

    log.debug("Saving....");
    sess.save(details);
    return details;
  }

  public static ISO10161ReceivedMessageDetails storeReceived(Received_type received, 
                                                             Session sess,
                                                             ILLTransaction owner) throws java.sql.SQLException,
                                                                                   org.hibernate.HibernateException
  {
    // ISO10161ReceivedMessage msg = new ISO10161ReceivedMessage();

    ISO10161ReceivedMessageDetails details = new ISO10161ReceivedMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    if ( received.supplier_id != null )
    {
      if ( received.supplier_id.person_or_institution_symbol != null )
      {
        details.setPersonOrInstitutionSymbolType(received.supplier_id.person_or_institution_symbol.which);
        details.setPersonOrInstitutionSymbol(received.supplier_id.person_or_institution_symbol.o.toString());
      }
      if ( received.supplier_id.name_of_person_or_institution != null )
      {
        details.setNameOfPersonOrInstitutionType(received.supplier_id.name_of_person_or_institution.which);
        details.setNameOfPersonOrInstitution(received.supplier_id.name_of_person_or_institution.o.toString());
      }
    }

    if ( received.date_received != null )
    {
      try { 
        details.setDateReceived(Constants.parseIsoDate(received.date_received)); 
      } catch ( java.text.ParseException pe ) {log.warn("Problem",pe);}
    }

    if ( received.shipped_service_type != null )
      details.setShippedServiceType(new Long(received.shipped_service_type.intValue()));

    if ( received.requester_note != null )
      details.setNote(received.requester_note.o.toString());

    setGenericParams(received, details, sess);
    sess.save(details);
    return details;
  }

  public static ISO10161RecallMessageDetails storeRecall(Recall_type recall, 
                                                         Session sess,
                                                         ILLTransaction owner) throws java.sql.SQLException,
                                                                                   org.hibernate.HibernateException
  {
    // ISO10161RecallMessage msg = new ISO10161RecallMessage();

    ISO10161RecallMessageDetails details = new ISO10161RecallMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    setGenericParams(recall, details, sess);

    if ( recall.responder_note != null )
      details.setNote(recall.responder_note.o.toString());

    sess.save(details);
    return details;
  }

  public static ISO10161ReturnedMessageDetails storeReturned(Returned_type returned, 
                                                             Session sess,
                                                             ILLTransaction owner) throws java.sql.SQLException,
                                                                                       org.hibernate.HibernateException
  {
    // ISO10161ReturnedMessage msg = new ISO10161ReturnedMessage();

    ISO10161ReturnedMessageDetails details = new ISO10161ReturnedMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    setGenericParams(returned, details, sess);

    if ( returned.insured_for != null ) {
      details.setInsuredForCurrencyCode(returned.insured_for.currency_code);
      details.setInsuredForMonetaryValue(returned.insured_for.monetary_value);
    }


    if ( returned.date_returned != null )
    {
      try { 
        details.setDateReturned(Constants.parseIsoDate(returned.date_returned)); 
      } catch ( java.text.ParseException pe ) {log.warn("Problem",pe);}
    }

    if ( returned.returned_via != null )
      details.setReturnedVia(returned.returned_via.o.toString());

    if ( returned.requester_note != null )
      details.setNote(returned.requester_note.o.toString());

    sess.save(details);
    return details;
  }

  public static ISO10161CheckedInMessageDetails storeCheckedIn(Checked_In_type checked_in, 
                                                               Session sess,
                                                               ILLTransaction owner) throws java.sql.SQLException,
                                                                                         org.hibernate.HibernateException
  {
    // ISO10161CheckedInMessage msg = new ISO10161CheckedInMessage();

    ISO10161CheckedInMessageDetails details = new ISO10161CheckedInMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    setGenericParams(checked_in, details, sess);

    if ( checked_in.date_checked_in != null )
    {
      try { 
        details.setDateCheckedIn(Constants.parseIsoDate(checked_in.date_checked_in)); 
      } catch ( java.text.ParseException pe ) {log.warn("Problem",pe);}
    }
    if ( checked_in.responder_note != null )
      details.setNote(checked_in.responder_note.o.toString());

    sess.save(details);
    return details;
  }

  public static ISO10161OverdueMessageDetails storeOverdue(Overdue_type overdue, 
                                                           Session sess,
                                                           ILLTransaction owner) throws java.sql.SQLException,
                                                                                     org.hibernate.HibernateException
  {
    // ISO10161OverdueMessage msg = new ISO10161OverdueMessage();

    ISO10161OverdueMessageDetails details = new ISO10161OverdueMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    setGenericParams(overdue, details, sess);

    if ( overdue.date_due != null )
    {
      if ( overdue.date_due.date_due_field != null )
      {
        try { 
          details.setDateDue(Constants.parseIsoDate(overdue.date_due.date_due_field)); 
        } catch ( java.text.ParseException pe ) {log.warn("Problem",pe);}
      }
      if ( overdue.date_due.renewable != null )
        details.setRenewable(overdue.date_due.renewable);
    }
    if ( overdue.responder_note != null )
      details.setNote(overdue.responder_note.o.toString());

    sess.save(details);
    return details;
  }

  public static ISO10161RenewMessageDetails storeRenew(Renew_type renew, 
                                                       Session sess,
                                                       ILLTransaction owner) throws java.sql.SQLException,
                                                                                    org.hibernate.HibernateException
  {
    // ISO10161RenewMessage msg = new ISO10161RenewMessage();

    ISO10161RenewMessageDetails details = new ISO10161RenewMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    if ( renew.desired_due_date != null )
    {
      try { 
        details.setDesiredDueDate(Constants.parseIsoDate(renew.desired_due_date)); 
      } catch ( java.text.ParseException pe ) {log.warn("Problem",pe);}
    }

    if ( renew.requester_note != null )
      details.setNote(renew.requester_note.o.toString());

    setGenericParams(renew, details, sess);
    sess.save(details);
    return details;
  }

  public static ISO10161RenewAnswerMessageDetails storeRenewAnswer(Renew_Answer_type renew_answer, 
                                                                   Session sess,
                                                                   ILLTransaction owner) throws java.sql.SQLException,
                                                                            org.hibernate.HibernateException
  {
    // ISO10161RenewAnswerMessage msg = new ISO10161RenewAnswerMessage();

    ISO10161RenewAnswerMessageDetails details = new ISO10161RenewAnswerMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    setGenericParams(renew_answer, details, sess);

    if ( renew_answer.answer != null )
      details.setAnswer(renew_answer.answer);

    if ( renew_answer.date_due != null )
    {
      if ( renew_answer.date_due.date_due_field != null )
      {
        try { 
          details.setDateDue(Constants.parseIsoDate(renew_answer.date_due.date_due_field)); 
        } catch ( java.text.ParseException pe ) {log.warn("Problem",pe);}
      }
      if ( renew_answer.date_due.renewable != null )
        details.setRenewable(renew_answer.date_due.renewable);
    }

    if ( renew_answer.responder_note != null )
      details.setNote(renew_answer.responder_note.o.toString());

    sess.save(details);
    return details;
  }

  public static ISO10161LostMessageDetails storeLost(Lost_type lost, 
                                                     Session sess,
                                                     ILLTransaction owner) throws java.sql.SQLException,
                                                                                  org.hibernate.HibernateException
  {
    // ISO10161LostMessage msg = new ISO10161LostMessage();

    ISO10161LostMessageDetails details = new ISO10161LostMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    setGenericParams(lost, details, sess);

    if ( lost.note != null )
      details.setNote(lost.note.o.toString());

    sess.save(details);
    return details;
  }

  public static ISO10161MessageMessageDetails storeMessage(Message_type message, 
                                                           Session sess,
                                                           ILLTransaction owner) throws java.sql.SQLException,
                                                                                         org.hibernate.HibernateException
  {
    // ISO10161MessageMessage msg = new ISO10161MessageMessage();

    ISO10161MessageMessageDetails details = new ISO10161MessageMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    setGenericParams(message, details, sess);

    if ( message.note != null )
      details.setNote(message.note.o.toString());

    sess.save(details);
    return details;
  }

  public static ISO10161DamagedMessageDetails storeDamaged(Damaged_type damaged, 
                                                           Session sess,
                                                           ILLTransaction owner) throws java.sql.SQLException,
                                                                                         org.hibernate.HibernateException
  {
    // ISO10161DamagedMessage msg = new ISO10161DamagedMessage();

    ISO10161DamagedMessageDetails details = new ISO10161DamagedMessageDetails();
    setGenericParams(damaged, details, sess);
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    if ( damaged.damaged_details != null )
    {
      details.setDocumentTypeId(damaged.damaged_details.document_type_id);
      if ( damaged.damaged_details.damaged_portion != null )
      {
        details.setDamagedPortion(damaged.damaged_details.damaged_portion.which);
        if (damaged.damaged_details.damaged_portion.which==damaged_portion_inline24_type.specific_units_CID)
          details.setDamagedSections((int[])damaged.damaged_details.damaged_portion.o);
      }
    }

    if ( damaged.note != null )
      details.setNote(damaged.note.o.toString());

    sess.save(details);
    return details;
  }

  public static ISO10161StatusQueryMessageDetails storeStatusQuery(Status_Query_type status_query, 
                                                                   Session sess,
                                                                   ILLTransaction owner) throws java.sql.SQLException,
                                                                           org.hibernate.HibernateException
  {
    // ISO10161StatusQueryMessage msg = new ISO10161StatusQueryMessage();

    ISO10161StatusQueryMessageDetails details = new ISO10161StatusQueryMessageDetails();
    details.setParentRequest(owner);
    // details.setParent(msg);
    // msg.setMessageDetails(details);

    setGenericParams(status_query, details, sess);

    if ( status_query.note != null )
      details.setNote(status_query.note.o.toString());

    sess.save(details);
    return details;
  }

  public static ISO10161StatusOrErrorReportMessageDetails storeStatusOrErrorReport(Status_Or_Error_Report_type soer, 
                                              Session sess,
                                              ILLTransaction owner) throws java.sql.SQLException,
                                                                            org.hibernate.HibernateException
  {
    // ISO10161StatusOrErrorReportMessage msg = new ISO10161StatusOrErrorReportMessage();
    ISO10161StatusOrErrorReportMessageDetails details = new ISO10161StatusOrErrorReportMessageDetails();
    details.setParentRequest(owner);

    // details.setParent(msg);
    // msg.setMessageDetails(details);

    if ( soer.reason_no_report != null )
      details.setReasonNoReport(new Long(soer.reason_no_report.intValue()));

    if ( soer.status_report != null )
    {
      if ( soer.status_report.user_status_report != null )
      {
        log.warn("History not saved...");
      }

      if ( soer.status_report.provider_status_report != null )
      {
        details.setProviderStatusReport(new Long(soer.status_report.provider_status_report.intValue()));
      }
    }

    if ( soer.error_report != null )
    {
      if ( soer.error_report.correlation_information != null )
        details.setCorrelationInfo(soer.error_report.correlation_information.o.toString());

      if ( soer.error_report.report_source != null )
        details.setReportSource(new Long(soer.error_report.report_source.intValue()));

      if ( soer.error_report.user_error_report != null )
      {
        details.setUserErrorReportType(soer.error_report.user_error_report.which);
      }

      if ( soer.error_report.provider_error_report != null )
      {
        details.setProviderErrorReportType(soer.error_report.provider_error_report.which);
      }
    }

    if ( soer.note != null )
      details.setNote(soer.note.o.toString());

    setGenericParams(soer, details, sess);


    sess.save(details);
    return details;
  }

  public static ISO10161ExpiredMessageDetails storeExpired(Expired_type expired, 
                                              Session sess,
                                              ILLTransaction owner) throws java.sql.SQLException,
                                                                            org.hibernate.HibernateException
  {
    ISO10161ExpiredMessageDetails msg = new ISO10161ExpiredMessageDetails();
    msg.setParentRequest(owner);
    setGenericParams(expired, msg, sess);
    sess.save(msg);
    return msg;
  }


  private static void storeResponderSpecificService(ISO10161RequestMessageDetails details, EXTERNAL_type responder_specific_external, Session sess) {
    if ( ( responder_specific_external != null ) &&
         ( responder_specific_external.encoding != null ) &&
         ( responder_specific_external.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {
      Object o = responder_specific_external.encoding.o;
      if ( o instanceof com.k_int.openrequest.isoill.gen.OCLCILLService.OCLCILLService_type ) {
        details.setOCLCILLService(new Long(((com.k_int.openrequest.isoill.gen.OCLCILLService.OCLCILLService_type)o).oCLCILLServiceType.longValue()));
      }
    }
  }

  public static void storeExtensions(Session sess, 
                                      ILLTransaction owner, 
                                      MessageHeader header, 
                                      List extensions, 
                                      OIDRegister reg) throws org.hibernate.HibernateException {

    log.info("Store extensions.....");

    if ( extensions != null ) {
      log.debug("Looking for APDUDeliveryInfo extension");
      for ( Iterator i = extensions.iterator(); i.hasNext(); )
      {
        log.debug("Checking extension......");
        Extension_type et = (Extension_type)i.next();

        // we have et.identifier, et.critical, et.item (which is the any containing an External
        if ( ( et.identifier != null ) && ( et.identifier.equals(BigInteger.valueOf(1))) ) {
          if ( et.item != null ) {
            log.info("Got EXTERNAL_type, class is "+et.item.getClass().getName());

            EXTERNAL_type ext = (EXTERNAL_type) et.item;
            int[] oid = ext.direct_reference;

            if ( oid != null ) {
              OIDRegisterEntry ent = reg.lookupByOID(oid);
              if ( ent != null ) {
                log.debug("Calling store extension for "+ent.getName());

                if ( ( ent.getName().equals("ILL_APDU_Delivery_Info") ) && 
                     ( ext.encoding != null ) && 
                     ( ext.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {
                    log.debug("Processing APDU Delivery Info");
                }
                else if ( ( ent.getName().equals("ILL_Internal_Reference_Number") ) && 
                     ( ext.encoding != null ) && 
                     ( ext.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {
                    log.debug("ILL Internal Reference Number");
                    storeInternalReferenceExtension(sess, 
                                                    owner, 
                                                    header, 
                                                    (com.k_int.openrequest.isoill.gen.Internal_Reference_Number.Internal_Reference_type)(ext.encoding.o));
                }
                else if ( ( ent.getName().equals("OCLCILLRequestExtension") ) && 
                     ( ext.encoding != null ) && 
                     ( ext.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {
                    log.debug("IPIG ILL Request Extension");
                    storeOCLCRequestExtensionExtension(sess, 
                                                       owner, 
                                                       header, 
                                                       (com.k_int.openrequest.isoill.gen.OCLCILLRequestExtension.OCLCILLRequestExtension_type)(ext.encoding.o));
                }
                else if ( ( ent.getName().equals("ILL_Supplemental_Client_Info") ) && 
                     ( ext.encoding != null ) && 
                     ( ext.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {
                    log.debug("ILL Supplemental Client Info");
                    storeSupplementalClientInfoExtension(sess, 
                                                         owner, 
                                                         header, 
                                                         (List)(ext.encoding.o));
                }
                else if ( ( ent.getName().equals("IPIG_ILL_Request_Extension") ) && 
                     ( ext.encoding != null ) && 
                     ( ext.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {
                }
                else if ( ( ent.getName().equals("OCLC_Shipped_Extension") ) && 
                     ( ext.encoding != null ) && 
                     ( ext.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {
                  storeOCLCShippedExtension(sess,owner,header,
                                            (com.k_int.openrequest.isoill.gen.OCLC_Shipped_Extension.OCLC_Shipped_Extension_type)(ext.encoding.o));
                }
                else {
                  log.info("No additional storage helper for "+ent.getName());
                }
              }
              else {
                log.warn("Unable to resolve extension OID in OID Register");
              }
            }
            else {
              log.warn("Null OID for extension type");
            }
          }
        }
      }
    }
  }

  private static void storeSupplementalClientInfoExtension(Session sess, 
                                                           ILLTransaction owner, 
                                                           MessageHeader header, 
                                                           List client_info_list) throws org.hibernate.HibernateException {

    // client_info is a sequence of ClientInfoData
    SupplementalClientDetailsExtension new_extn = new SupplementalClientDetailsExtension();
    sess.save(new_extn);
    header.getExtensions().add(new_extn);

    for ( Iterator i = client_info_list.iterator(); i.hasNext(); ) {
      com.k_int.openrequest.isoill.gen.ILL_Supplemental_Client_Info.ClientInfoData_type client_info = 
                  (com.k_int.openrequest.isoill.gen.ILL_Supplemental_Client_Info.ClientInfoData_type)i.next();
      if ( client_info.infoType.which == ClientInfoType_type.standard_CID ) {
        BigInteger bi = (BigInteger)client_info.infoType.o;
        switch ( bi.intValue() ) {
          case 30:    // Whole Namae
            if ( client_info.infoContent.size() > 0 ) {
              ClientInfoContent_type content = (ClientInfoContent_type) client_info.infoContent.get(0);
              if ( content.o instanceof ILL_String_type ) {
                new_extn.setPatronName(ILLStringHelper.extract((ILL_String_type)content.o));
                owner.getTransactionGroup().setPatronName(new_extn.getPatronName());
              }
            }
            break;
          case 101:    // Notes
            if ( client_info.infoContent.size() > 0 ) {
              ClientInfoContent_type content = (ClientInfoContent_type) client_info.infoContent.get(0);
              if ( content.o instanceof ILL_String_type ) {
                new_extn.setNotes(ILLStringHelper.extract((ILL_String_type)content.o));
              }
            }
            break;
        }
      }
    }
  }

  private static void storeInternalReferenceExtension(Session sess, 
                                                      ILLTransaction owner, 
                                                      MessageHeader header, 
                                                      com.k_int.openrequest.isoill.gen.Internal_Reference_Number.Internal_Reference_type ref_data) 
                                                      throws org.hibernate.HibernateException {
    String requester_ref_authority = null;
    String requester_ref = null;
    String responder_ref_authority = null;
    String responder_ref = null;

    if ( ref_data != null ) {

      if ( ref_data.requester_reference != null ) {
        requester_ref_authority = ILLStringHelper.extract(ref_data.requester_reference.reference_authority);
        requester_ref = ILLStringHelper.extract(ref_data.requester_reference.internal_reference);
      }

      if ( ref_data.responder_reference != null ) {
        responder_ref_authority = ILLStringHelper.extract(ref_data.responder_reference.reference_authority);
        responder_ref = ILLStringHelper.extract(ref_data.responder_reference.internal_reference);
      }
    }

    InternalReferenceNumber irn = new InternalReferenceNumber(requester_ref_authority,
                                                              requester_ref,
                                                              responder_ref_authority,
                                                              responder_ref);
    sess.save(irn);
    header.getExtensions().add(irn);
    if ( requester_ref_authority != null ) 
      owner.getTransactionGroup().setRequesterReferenceAuthority(requester_ref_authority);
    if ( requester_ref != null ) 
      owner.getTransactionGroup().setRequesterReference(requester_ref);
    if ( responder_ref_authority != null ) 
      owner.getTransactionGroup().setResponderReferenceAuthority(responder_ref_authority);
    if ( responder_ref != null ) 
      owner.getTransactionGroup().setResponderReferenceAuthority(responder_ref);
  }

  private static void storeOCLCRequestExtensionExtension(Session sess, 
                                                         ILLTransaction owner, 
                                                         MessageHeader header, 
                                                         com.k_int.openrequest.isoill.gen.OCLCILLRequestExtension.OCLCILLRequestExtension_type ref_data)
                                                         throws org.hibernate.HibernateException {
    log.info("Store OCLCRequestExtension");
    OCLCILLRequestExtension extn = new OCLCILLRequestExtension();
    extn.setClientDepartment(ILLStringHelper.extract(ref_data.clientDepartment));
    extn.setPaymentMethod(ILLStringHelper.extract(ref_data.paymentMethod));
    extn.setUniformTitle(ILLStringHelper.extract(ref_data.uniformTitle));
    extn.setDissertation(ILLStringHelper.extract(ref_data.dissertation));
    extn.setIssueNumber(ILLStringHelper.extract(ref_data.issueNumber));
    extn.setVolume(ILLStringHelper.extract(ref_data.volume));
    extn.setAffiliations(ILLStringHelper.extract(ref_data.affiliations));
    extn.setSource(ILLStringHelper.extract(ref_data.source));
    sess.save(extn);
    header.getExtensions().add(extn);
    owner.getTransactionGroup().setClientDepartment(ILLStringHelper.extract(ref_data.clientDepartment));
    owner.getTransactionGroup().setPaymentMethod(ILLStringHelper.extract(ref_data.paymentMethod));
    owner.getTransactionGroup().setAffiliations(ILLStringHelper.extract(ref_data.affiliations));
    owner.getTransactionGroup().getRequiredItemDetails().setUniformTitle(ILLStringHelper.extract(ref_data.uniformTitle));
    owner.getTransactionGroup().getRequiredItemDetails().setDissertation(ILLStringHelper.extract(ref_data.dissertation));
    owner.getTransactionGroup().getRequiredItemDetails().setIssue(ILLStringHelper.extract(ref_data.issueNumber));
    // owner.getTransactionGroup().getRequiredItemDetails().setVolume(ILLStringHelper.extract(ref_data.volume));
    // owner.getTransactionGroup().getRequiredItemDetails().setVerificationReferenceSource(ILLStringHelper.extract(ref_data.source));
  }

  private static void storeOCLCShippedExtension(Session sess, 
                                                ILLTransaction owner, 
                                                MessageHeader header, 
                                                com.k_int.openrequest.isoill.gen.OCLC_Shipped_Extension.OCLC_Shipped_Extension_type shipped_extn)
                                                         throws org.hibernate.HibernateException {
    owner.getTransactionGroup().setPaymentMethod(ILLStringHelper.extract(shipped_extn.paymentMethod));
  }
}
