/**
 * Title:       
 * @version:    $Id: ISO10161v2ExternalHelper.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.helpers;

import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import com.k_int.openrequest.isoill.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.ILL_APDU_Delivery_Info.*;
import java.math.BigInteger;
import java.util.*;

public class ISO10161v2ExternalHelper
{
  public static Object extractNamedExternal(ILL_APDU_type pdu, String external_name, OIDRegister reg) {

    Object result = null;
    java.util.ArrayList extensions = null;

    switch ( pdu.which )
    {
      case ILL_APDU_type.ill_request_var_CID:
        extensions = ((ILL_Request_type)pdu.o).iLL_request_extensions;
        break;
      case ILL_APDU_type.forward_notification_var_CID:
        extensions = ((Forward_Notification_type)pdu.o).forward_notification_extensions;
        break;
      case ILL_APDU_type.shipped_var_CID:
        extensions = ((Shipped_type)pdu.o).shipped_extensions;
        break;
      case ILL_APDU_type.ill_answer_var_CID:
        extensions = ((ILL_Answer_type)pdu.o).ill_answer_extensions;
        break;
      case ILL_APDU_type.conditional_reply_var_CID:
        extensions = ((Conditional_Reply_type)pdu.o).conditional_reply_extensions;
        break;
      case ILL_APDU_type.cancel_var_CID:
        extensions = ((Cancel_type)pdu.o).cancel_extensions;
        break;
      case ILL_APDU_type.cancel_reply_var_CID:
        extensions = ((Cancel_Reply_type)pdu.o).cancel_reply_extensions;
        break;
      case ILL_APDU_type.received_var_CID:
        extensions = ((Received_type)pdu.o).received_extensions;
        break;
      case ILL_APDU_type.recall_var_CID:
        extensions = ((Recall_type)pdu.o).recall_extensions;
        break;
      case ILL_APDU_type.returned_var_CID:
        extensions = ((Returned_type)pdu.o).returned_extensions;
        break;
      case ILL_APDU_type.checked_in_var_CID:
        extensions = ((Checked_In_type)pdu.o).checked_in_extensions;
        break;
      case ILL_APDU_type.overdue_var_CID:
        extensions = ((Overdue_type)pdu.o).overdue_extensions;
        break;
      case ILL_APDU_type.renew_var_CID:
        extensions = ((Renew_type)pdu.o).renew_extensions;
        break;
      case ILL_APDU_type.renew_answer_var_CID:
        extensions = ((Renew_Answer_type)pdu.o).renew_answer_extensions;
        break;
      case ILL_APDU_type.lost_var_CID:
        extensions = ((Lost_type)pdu.o).lost_extensions;
        break;
      case ILL_APDU_type.damaged_var_CID:
        extensions = ((Damaged_type)pdu.o).damaged_extensions;
        break;
      case ILL_APDU_type.message_var_CID:
        extensions = ((Message_type)pdu.o).message_extensions;
        break;
      case ILL_APDU_type.status_query_var_CID:
        extensions = ((Status_Query_type)pdu.o).status_query_extensions;
        break;
      case ILL_APDU_type.status_or_error_report_var_CID:
        extensions = ((Status_Or_Error_Report_type)pdu.o).status_or_error_report_extensions;
        break;
      case ILL_APDU_type.expired_var_CID:
        extensions = ((Expired_type)pdu.o).expired_extensions;
        break;
    }

    if ( extensions != null )
    {
      for ( java.util.Iterator i = extensions.iterator(); i.hasNext(); ) {
        Extension_type et = (Extension_type)i.next();

        // we have et.identifier, et.critical, et.item (which is the any containing an External
        // if ( ( et.identifier != null ) && ( et.identifier.equals(BigInteger.valueOf(1))) )
        if ( et.identifier != null )
        {
          if ( et.item != null )
          {
            EXTERNAL_type ext = (EXTERNAL_type) et.item;
            int[] oid = ext.direct_reference;
            if ( oid != null )
            {
              OIDRegisterEntry ent = reg.lookupByOID(oid);
              if ( ent != null ) {

                if ( ent.getName().equals(external_name) ) { 

                  if ( ( ext.encoding != null ) &&
                       ( ext.encoding.which == encoding_inline0_type.single_asn1_type_CID ) ) {

                    result = ext.encoding.o;
                  }
                }
              }
            }
          }
        }
      }
    }
    else {
    }

    return result;
  }
}
