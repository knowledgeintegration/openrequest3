/**
 * Title:       SystemHelper
 * @version:    $Id: SystemHelper.java,v 1.4 2005/05/14 22:39:06 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Request PDU in different situations
 */

package com.k_int.openrequest.helpers;

import java.util.*;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.db.folders.*;
import java.math.BigInteger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.jexl.*;
import org.hibernate.*;
import org.hibernate.type.*;
import com.k_int.openrequest.db.folders.FolderHeader;

public class SystemHelper
{
  private static Log log = LogFactory.getLog(SystemHelper.class);
  private static List global_rules = null;
  private static Object lock = new Object();

  public static FolderHeader locateFolder(org.hibernate.Session session,
                                          String loc,
                                          String folder_code) throws java.sql.SQLException, 
                                                                     org.hibernate.HibernateException
  {
    log.debug("locate folder for loc="+loc+" cold="+folder_code);

    FolderHeader retval = null;
    Object[] values = { loc, folder_code };
    Type[] types = { TypeFactory.basic("java.lang.String"), TypeFactory.basic("java.lang.String") };

    Query q = session.createQuery("from folder in class com.k_int.openrequest.db.folders.FolderHeader where folder.ownerId=? and folder.code=?");
    q.setParameter(0,loc,Hibernate.STRING);
    q.setParameter(1,folder_code,Hibernate.STRING);
    List matching_folders = q.list();

    if ( matching_folders.size() > 0 ) {
      retval = (FolderHeader) matching_folders.get(0);
    }
    else {
      retval = new FolderHeader("An automated folder","LOCATION",loc,folder_code);
      session.save(retval);
    }

    return retval;
  }

  public static List getGlobalRules(org.hibernate.Session session) throws java.sql.SQLException, org.hibernate.HibernateException
  {
    if ( global_rules == null )
    {
      synchronized(lock)
      {
        if ( global_rules == null )
        {
          log.debug("Loading static generic rule set from database");

          // Perform the fetch from the database
          List l = session.createQuery("from folder_rule in class com.k_int.openrequest.db.folders.GenericRule where folder_rule.scopeType='SYSTEM'").list();
          global_rules = l;

          log.debug("Size of global rules: "+l.size());
        }
      }
    }

    return global_rules;
  }

  public static void announceTGChanged(org.springframework.context.ApplicationContext ctx,
                                       Map vars,
                                       ILLTransactionGroup tg) {
    log.debug("Announce TGChanged:"+vars);
    // Look for any folder rules pertaining to this location_id and the supplied state model/values
    //log.debug("announceStateTransition from "+old_state_code+" to "+new_state_code);
    Session session = null;
    try {
      SessionFactory factory = (SessionFactory) ctx.getBean("OpenRequestSessionFactory");
      session = factory.openSession();

      JexlContext jc = JexlHelper.createContext();
      jc.setVars(vars);

      List global_rules = getGlobalRules(session);

      for ( Iterator i = global_rules.iterator(); i.hasNext(); ) {

        GenericRule gr = (GenericRule) i.next();
        // log.debug("Was rule appicable before? "+gr.getOldExpression());
        // log.debug("Is rule appicable now? "+gr.getExpression());

        try {

          Expression e1 = ExpressionFactory.createExpression(gr.getOldExpression());
          Object was_true = e1.evaluate(jc);
          Expression e2 = ExpressionFactory.createExpression(gr.getExpression());
          Object is_true = e2.evaluate(jc);

          String loc_id_as_str = ""+tg.getLocation().getId();

          if ( was_true == null ) was_true=Boolean.FALSE;
          if ( is_true == null ) is_true=Boolean.FALSE;

          // log.debug("Was True = "+was_true+", Is true "+is_true);

          if ( !was_true.equals(is_true) ) {

            log.debug("There has been a change");
  
            FolderHeader folder = FolderHeader.lookupOrCreateFolder(session, loc_id_as_str, gr.getTargetId());
            ILLTransactionGroup tg_folder_item = ILLTransactionGroup.lookupTGById(session, tg.getId());

            if ( was_true.equals(Boolean.TRUE) ) {
              // log.debug("Remove tg from folder");
              folder.getFolderItems().remove(tg_folder_item);
              folder.setRequestCount(folder.getFolderItems().size());
            }
            else {
              // log.debug("Add tg to folder");
              folder.getFolderItems().add(tg_folder_item);
              folder.setRequestCount(folder.getFolderItems().size());
            }
            session.flush();
            session.connection().commit();
          }
          else {
            // log.debug("No change");
          }
        }
        catch ( org.apache.commons.jexl.parser.TokenMgrError tme ) {
          log.warn("Problem",tme);
          log.warn(gr.getOldExpression());
          log.warn(gr.getExpression());
        }
        catch ( java.lang.Exception e ) {
          log.warn("Problem",e);
        }
      }
    }
    catch ( java.sql.SQLException sqle ) {
      log.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("Problem",he);
    }
    finally {
      if ( session != null ) {
        try { session.close(); } catch( Exception e ) {}
      }
    }
  }

  public static void announceVarChange(org.hibernate.Session session,
                                       String var_name,
                                       String old_value, 
                                       String new_value,
                                       Object object_changing)
  {
    log.debug("announceVarChange");
  }
}
