/**
 * Title:       ISOCheckedInMessageFactory
 * @version:    $Id: ISOCheckedInMessageFactory.java,v 1.2 2005/07/09 09:19:51 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an ISO ILL Recall PDU
 */

package com.k_int.openrequest.helpers;

import org.hibernate.*;
import java.util.Properties;
import java.util.ArrayList;
import java.util.Date;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;

import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 *  Create an ISO ILL Checked-In Message.
 *  This class will construct an iso ill Checked-In message
 *  @author Ian Ibbotson
 *  @version $Id: ISOCheckedInMessageFactory.java,v 1.2 2005/07/09 09:19:51 ibbo Exp $
 */
public class ISOCheckedInMessageFactory {

  /**
   *  create a Checked-In message.
   *
   *  Requires: An ILL Transaction in a state for which  is a valid action
   *
   *  Provides: A completed ILL Recieved message with data derived as follows:
   *            N.B where a member could be set from the transaction store, speak with k-int
   *            about defaulting that member in, it's better than starting to track transaction data
   *            in databases outside the transaction store.
   *
   *  element name/type                        M/O	Source
   *  retval : ILL_APDU_type (Choice of pdu)	M       Filled in by factory
   *    .which : int				M       Filled in by factory
   *    .o : Recieved_type			M       Filled in by factory
   *      .protocol_version_num
   *      .transaction_id			M	Constructed field, populated by this method
   *        .initial_requester_id		M	
   *          .person_or_institution_symbol	O	Set by factory from database
   *          .name_of_person_or_institution    O	Not set
   *        .tgq				M	Set by factory from database
   *        .tq					M	Set by factory from database
   *        .stq				O	Set by factory from database
   *      .service_date_time			M	Constructed by factory
   *        .date_time_of_this_service          M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *        .date_time_of_original_service      M       Constructed by factory
   *          .date                             M       Set by factory from system clock
   *          .time                             O       Set by factory from system clock
   *      .requester_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *      .responder_id				M	Set by factory from database
   *        .person_or_institution_symbol       O       Set by factory from database
   *        .person_or_institution_name         O       Not set
   *
   *                   -- End of common elements. Recieved specific follow --
   *
   * 	  .date_checked_in			M	Set according to date checked in param
   *      .responder_note			O	Not Set
   *	  .recall_extensions			O	Depends on Extension Factory
   *      
   *
   *
   *  Ends description of how shipped PDU is populated by this factory method
   *
   *  @author Ian Ibbotson
   *  @param database_session The database session we will use to access the database
   *  @param request_id request id (internal number) of the transaction we are dealing with
   *  @param responder_note
   *  @param extens And array of extension factories that will set up a shipped extension
   *                for each factory in the array.
   * 
   *  @return An ILL_APDU containing a Recall message
   */
  public static ILL_APDU_type create(Session database_session, 
                                     ILLTransaction transaction,
				     Date date_checked_in,
                                     String responder_note,
                                     ISOExtensionFactory[] extens) throws java.sql.SQLException,
                                                             org.hibernate.HibernateException
  {
    Checked_In_type checked_in = new Checked_In_type();
    ILL_APDU_type retval = new ILL_APDU_type( ILL_APDU_type.checked_in_var_CID, checked_in);

    // Fill out protocol version num,  transaction id
    ISOHeaderDetailsHelper.fillOutHeader(database_session, transaction, checked_in);

    if ( date_checked_in != null )
      checked_in.date_checked_in = Constants.formatDate(date_checked_in);

    if ( responder_note != null )
      checked_in.responder_note = ILLStringHelper.generalString(responder_note);

    if ( (extens != null) && ( extens.length > 0 ) )
    {
      ArrayList extensions = new ArrayList();
      for ( int i=0; i<extens.length; i++ )
      {
        // Process exten i and add to vector
      }

      checked_in.checked_in_extensions = extensions;
    }

    return retval;
  }
}
