/**
 * Server : Get incoming iso messages, send outgoing connection based messages
 *
 * @author Ian Ibbotson
 * @version $Id: CreateBSILLreq000.java,v 1.2 2005/07/07 17:57:20 ibbo Exp $
 */

package com.k_int.openrequest.test.isoill;

import com.k_int.openrequest.isoill.*;

import java.net.*;
import java.io.*;
import java.util.*;
import java.util.ArrayList;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import java.math.BigInteger;
import java.rmi.*;
import org.hibernate.*;
import org.w3c.dom.Document;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CreateBSILLreq000 {

  public static Log log = LogFactory.getLog(CreateBSILLreq000.class);

  public static ILL_APDU_type create(Session database_session, Properties props) throws Exception {
    // Properties must contain requester_symbol and responder_symbol.

    String requester_symbol = props.getProperty("requester_symbol");
    String responder_symbol = props.getProperty("responder_symbol");
    String test_case_id = props.getProperty("test_case_id");

    LocationSymbol requester = DBHelper.lookupLocation(database_session, requester_symbol);
    LocationSymbol responder = DBHelper.lookupLocation(database_session, responder_symbol);

    // Use the helper to create a base request
    ILL_APDU_type retval = ISORequestMessageFactory.create(database_session,
                                                           (String)null,
                                                           requester,
                                                           responder,
                                                           requester,
                                                           "ian.ibbotson@k-int.com",
                                                           "OK",
                                                           "Ian Ibbotson",
                                                           Boolean.FALSE,
                                                           Boolean.FALSE,
                                                           Boolean.FALSE,
                                                           Boolean.FALSE,
                                                           1,
                                                           (Document)null,
                                                           null,
                                                           null);

    ILL_Request_type req = (ILL_Request_type) retval.o;
    req.iLL_service_type = new ArrayList();
    req.iLL_service_type.add(BigInteger.valueOf(1));
    req.requester_note = new ILL_String_type(ILL_String_type.generalstring_var_CID,test_case_id);

    return retval;
  }
}
