/**
 * Title:       dbtest
 * @version:    $Id: SendShipped.java,v 1.2 2005/07/07 17:57:20 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Take an iso PDU and store it, returning a message id.
 */

package com.k_int.openrequest.test.isoill;

import org.hibernate.*;
import java.util.*;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.io.FileInputStream;
import java.math.BigInteger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class SendShipped {

  public static void main(String[] args) {
    System.err.println("Deprecated... ");
    System.exit(0);

    // We create a brokered request by creating a new BrokeredTransactionRequestMessage object
    if ( args.length < 1 ) {
      System.err.println("usage: java com.k_int.openrequest.services.Controller app_context_file.xml");
      System.exit(0);
    }
                                                                                                                                                                            
    try
    {
      String app_context_config_file = args[0];
      ClassPathXmlApplicationContext app_context = new ClassPathXmlApplicationContext( app_context_config_file );
                                                                                                                                                                            
      SessionFactory factory = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      Session sess = factory.openSession();

      Long id = new Long(Integer.parseInt(args[0]));
      ILLTransaction trans = DBHelper.lookupTransaction(sess,id);

      System.err.println("Result of looking up transaction "+id+": "+trans);

      ILL_APDU_type pdu = ISOShippedMessageFactory.create(sess,
                                                          null,
                                                          null,
                                                          trans,
                                                          1, // Shipped service type
                                                          new Date(System.currentTimeMillis()), //shipped date
                                                          new Date(System.currentTimeMillis()), //due date
                                                          Boolean.TRUE,
                                                          new Long(1), // Chargable units
                                                          "GBP",
                                                          "1.20",
                                                          new Long(1),
                                                          "return name",
                                                          "return extendedPostal",
                                                          "return streetAndNumber",
                                                          "return pobox",
                                                          "return city",
                                                          "return region",
                                                          "return country",
                                                          "return postcode",
                                                          "Responder note",
                                                          null,
                                                          app_context);
      
      try
      {
        ILLMessageEnvelope msg = 
               new ILLMessageEnvelope(pdu,
                                      trans.getLocation().getDefaultSymbol(),
                                      trans.getPVCurrentPartnerIdSymbol());

        MessageDispatcher mh = (MessageDispatcher) java.rmi.Naming.lookup("/OpenRequest/WorkflowController");
        // mh.handle("SHIreq",msg);
        mh.dispatch("System", "SHIreq",msg);
      }
      catch ( Exception e ) {
        e.printStackTrace();
      }

      sess.flush();
      sess.connection().commit();
      sess.close();
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      System.err.println("Done");
    }



    // We fill that out with a rota and an actual request structure

    // Then use the actioner interface to intiate the state model with that message.
    // That will cause the creation of a new BrokeredRequest object, fill in any
    // blank required params, etc.

    
  }
}
