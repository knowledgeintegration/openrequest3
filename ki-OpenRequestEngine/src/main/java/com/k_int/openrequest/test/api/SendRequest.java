/**
 * Title:       dbtest
 * @version:    $Id: SendRequest.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Take an iso PDU and store it, returning a message id.
 */

package com.k_int.openrequest.test.api;

import java.util.*;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import java.math.BigInteger;

import org.w3c.dom.*;
import javax.xml.parsers.*;

public class SendRequest
{
  public static void main(String[] args)
  {
    // We create a brokered request by creating a new BrokeredTransactionRequestMessage object
    try
    {
        // Look up the message dispatcher object using RMI naming.
        MessageDispatcher mh = (MessageDispatcher) java.rmi.Naming.lookup("/OpenRequest/WorkflowController");

        // Look up the factory object that will create a remote management object for us and
        // hand back a local proxy interface for that object.
        ManagerFactory manager_factory = 
             (ManagerFactory) java.rmi.Naming.lookup("/OpenRequest/ManagementInterfaceFactory");

        // Use the factory to log on to the OpenRequest server, using our location symbol ("KI:REQ")
        // the credentials (PAssword or digital certificate) object is null. IE we assume
        // the location symbol is valid.
        LocationBasedManagementInterface manager_interface = manager_factory.create("KI:REQ",null);

        // Manually construct an XML document describing the item we are requesting.
        Document details = null;
        DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
        dfactory.setNamespaceAware(true);
        DocumentBuilder docBuilder = dfactory.newDocumentBuilder();
        details = docBuilder.newDocument();

        // The root element
        Element root = details.createElement("DocumentRequest");
        details.appendChild(root);

        // The title of the item we are looking for
        Element title = details.createElement("Title");
        root.appendChild(title);
        title.appendChild( details.createTextNode("Brain of the firm") );

        // The author of the item we are looking for
        Element author = details.createElement("Author");
        root.appendChild(author);
        author.appendChild( details.createTextNode("Beer, Stafford") );

        // This call simulates an implementors administrative interface calling the manager
        // to create an ISO Ill request.
        // We pass in null for the tgq since we want the appication to assign a new tgq for us.
        // The Requester Symbol *Must* be one of our local system symbols (IE, an alias of this
        // "location") and the responder symbol should exist as an alias in the location_symbols
        // table. The patron symbol, status and name strings all come from a local information
        // system and the allow booleans releate to the ISO Protocol specification.
        // Initially, transaction_type should always be 1 and the XML document should describe
        // the item being described as in section N.N of the Programmers Guide to OpenRequest.
        ILLMessageEnvelope msg = manager_interface.createRequest(null, // No existing tgq
                                                            "KI:REQ", // requester symbol
                                                            "KI:RESP", // responder symbol
                                                            "KI:REQ", // initial requester symbol
                                                            "IBBO", // patron symbol
                                                            "OK", // patron status
                                                            "Ian Ibbotson", // patron name
                                                            Boolean.FALSE, // allow forwarding
                                                            Boolean.FALSE, // allow chaining
                                                            Boolean.FALSE, // allow partitioning
                                                            Boolean.FALSE, // allow change send to list
                                                            1, // Transaction Type : Simple
                                                            details);

        // Process the message
        // mh.handle("ILLreq",msg);
        mh.dispatch("System","ILLreq",msg);

        // Release the manager interface
        manager_interface.release();
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }
    finally
    {
      System.err.println("Done");
    }
  }
}
