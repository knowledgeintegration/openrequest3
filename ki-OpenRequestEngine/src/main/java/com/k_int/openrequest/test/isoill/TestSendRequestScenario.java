// Title:       Base Z3950 Server
// @version:    $Id: TestSendRequestScenario.java,v 1.2 2005/05/14 22:39:06 ibbo Exp $
// Copyright:   Copyright (C) 2001 Knowledge Integration Ltd.
// @author:     Ian Ibbotson (ibbo@k-int.com)
// Company:     KI
// Description: 
//
 

/**
 * Server : Get incoming iso messages, send outgoing connection based messages
 *
 * @author Ian Ibbotson
 * @version $Id: TestSendRequestScenario.java,v 1.2 2005/05/14 22:39:06 ibbo Exp $
 */

package com.k_int.openrequest.test.isoill;

import com.k_int.openrequest.isoill.*;

import java.net.*;
import java.io.*;
import java.util.*;
import java.util.ArrayList;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.helpers.*;
import java.math.BigInteger;
import java.rmi.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TestSendRequestScenario
{
  public static Log log = LogFactory.getLog(TestSendRequestScenario.class);

  public static void main (String args[]) throws IOException 
  {
      ILL_APDU_type pdu = 
        new ILL_APDU_type( ILL_APDU_type.ill_request_var_CID,
          new ILL_Request_type(                                                // ILL_Request
            BigInteger.valueOf(2),                                             //   m Protocol Version
            new Transaction_Id_type(                                           //   m Transaction ID
              new System_Id_type(                                              //   o   System ID
                new Person_Or_Institution_Symbol_type(                         //   o     Person or inst sym
                  Person_Or_Institution_Symbol_type.institution_symbol_CID,
                  new ILL_String_type(ILL_String_type.generalstring_var_CID,"k-int") ),
                null /* Don't bother setting name of person or instituion */), //   o     name of pers or inst
              new ILL_String_type(ILL_String_type.generalstring_var_CID,"tgq"),//   m   tgq
              new ILL_String_type(ILL_String_type.generalstring_var_CID,"tq"), //   m   tq
              new ILL_String_type(ILL_String_type.generalstring_var_CID,"s")), //   o   stq
            new Service_Date_Time_type(                                        //   m service date time
              new date_time_of_this_service_inline35_type("Date","Time"),
              new date_time_of_original_service_inline36_type("Date","Time")),
            new System_Id_type(                                                //   o requester id
                new Person_Or_Institution_Symbol_type(                         //   o     Person or inst sym
                  Person_Or_Institution_Symbol_type.institution_symbol_CID,
                  new ILL_String_type(ILL_String_type.generalstring_var_CID,"k-int") ),
                null /* Don't bother setting name of person or instituion */), //   o     name of pers or inst
            new System_Id_type(                                                //   o responder id
                new Person_Or_Institution_Symbol_type(                         //   o     Person or inst sym
                  Person_Or_Institution_Symbol_type.institution_symbol_CID,
                  new ILL_String_type(ILL_String_type.generalstring_var_CID,"k-int") ),
                null /* Don't bother setting name of person or instituion */), //   o     name of pers or inst

            // End of generic attrs that come with every PDU

            BigInteger.valueOf(1),                                             //   m transaction type
                          // Simple, Chained or Partitioned 1,2 or 3
            null, // new Delivery_Address_type(),                              //   o delivery address
            null, // new Delivery_Service_type(),                              //   o delivery service
            null, // new Delivery_Address_type(),                              //   o billing address
            new ArrayList(),                                                      //   m service type
            null,                                                              //   o resp. specific. services
            new Requester_Optional_Messages_Type_type(                         //   m req. opt. msg
                Boolean.TRUE,
                Boolean.TRUE,
                BigInteger.valueOf(2), // 1=requires, 2=desires, 3=neither
                BigInteger.valueOf(2)  // 1=requires, 2=desires, 3=neither
                ),
            new Search_Type_type(                                              //   o search type
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"lvl"),
                "need-before-date",
                BigInteger.valueOf(2), // 1=need b4, 2=othr, 3=no expiry
                "expirydate"),
            new java.util.ArrayList(),                                            //   m supp. med. info. type
            BigInteger.valueOf(2),    // 1=yes, 2=no                           //   m place on hold
            new Client_Id_type(),                                              //   o client id
            new Item_Id_type(                                                  //   m item id
                BigInteger.valueOf(2),                                         //   o   item type
                BigInteger.valueOf(2),                                         //   o   held medium type
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"calln"),            //   o   call number
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"autho"),            //   o   author
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"title"),            //   o   title
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"subti"),            //   o   sub title
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"spons"),            //   o   sponsoring body
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"place"),            //   o   place pub
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"publi"),            //   o   publisher
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"serie"),            //   o   series title num
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"volum"),            //   o   volume issue
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"editi"),            //   o   edition
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"pubda"),            //   o   pub date
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"pubda"),            //   o   pub date compo.
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"autho"),            //   o   author of article
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"title"),            //   o   title of article
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"pagin"),            //   o   pagination
                null, // EXTERNAL_type                                         //   o   nat bib num
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"isbn."),            //   o   isbn
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"issn."),            //   o   issn
                null, // EXTERNAL_type                                         //   o   system no
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"addno"),            //   o   add no let
                new ILL_String_type(ILL_String_type.generalstring_var_CID,"verif")),           //   o   verif ref source
            new ArrayList(),                                                      //   o supp. item desc.
            null, // new Cost_Info_Type_type(),                                //   o cost info type
            null, // new ILL_String_type(),                                    //   o copyright compliance
            null, // new Third_Party_Info_Type_type(),                         //   o third_party_info_type
            Boolean.TRUE,                                                      //   m retry flag
            Boolean.FALSE,                                                     //   m forward flag
            null, // new ILL_String_type(),                                    //   o requester note
            null, // new ILL_String_type(),                                    //   o forward note
            null)                                                              //   o vec req. extensions
          );

    Map properties = new HashMap();
    properties.put("HOST","localhost");
    properties.put("PORT",new Integer(1499));

    // ToDo: Use context to get hold of duplex protocol adapter
  }
}
