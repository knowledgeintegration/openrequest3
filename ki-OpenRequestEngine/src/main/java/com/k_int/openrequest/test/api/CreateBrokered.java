/**
 * Title:       dbtest
 * @version:    $Id: CreateBrokered.java,v 1.2 2005/07/07 17:57:20 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Take an iso PDU and store it, returning a message id.
 */

package com.k_int.openrequest.test.api;

import org.hibernate.*;
import java.util.Properties;
import java.util.Vector;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.math.BigInteger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CreateBrokered {
  public static Log log = LogFactory.getLog(CreateBrokered.class);

  public static void main(String[] args)
  {
    // We create a brokered request by creating a new BrokeredTransactionRequestMessage object
    if ( args.length != 1 )
    {
      System.out.println("Usgae: java com.k_int.openrequest.setup.AbstractSetup app_context.xml");
      System.exit(0);
    }
                                                                                                                                                                            
    try
    {
      System.err.println("Configure");
      String app_context_config_file = args[0];
      ClassPathXmlApplicationContext app_context = new ClassPathXmlApplicationContext( app_context_config_file );
      SessionFactory factory = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      Session sess = factory.openSession();
                                                                                                                                                                            
      org.w3c.dom.Document req_info = null;

      LocationSymbol responder = DBHelper.lookupLocation(sess, "KI", "fantasy:hogwarts");
      LocationSymbol requester = DBHelper.lookupLocation(sess, "KI", "fantasy:unseen");

      log.debug("requester: "+requester.getCanonical_location().toString());
      log.debug("responder: "+responder.getCanonical_location().toString());

      ILL_APDU_type pdu = ISORequestMessageFactory.create(sess,
                                                          null,
                                                          requester,
                                                          responder,
                                                          null, // Original Requester.
                                                          "ibbo", // Patron symbol
                                                          "OK", // Patron status
                                                          "Ian Ibbotson", // patron name
                                                          Boolean.FALSE, // Don't allow forwarding
                                                          Boolean.FALSE, // Don't allow chaining
                                                          Boolean.FALSE, // Don't Allow Partitioning
                                                          Boolean.FALSE, // Don't allow chg of send to list
                                                          1, // Simple transaction
                                                          req_info,
                                                          null,
                                                          app_context);
      
      try
      {
        ILLMessageEnvelope msg = new ILLMessageEnvelope(pdu, 
                                                        requester.toString(), 
                                                        responder.toString());

        MessageDispatcher mh = (MessageDispatcher) java.rmi.Naming.lookup("/OpenRequest/WorkflowController");
        // mh.handle("ILLreq",msg);
        mh.dispatch("System","ILLreq",msg);
      }
      catch ( Exception e )
      {
        e.printStackTrace();
      }

      sess.flush();
      sess.connection().commit();
      sess.close();
    }
    catch ( Exception e )
    {
      log.warn("Problem handling message",e);
    }
    finally
    {
      log.debug("Done");
    }



    // We fill that out with a rota and an actual request structure

    // Then use the actioner interface to intiate the state model with that message.
    // That will cause the creation of a new BrokeredRequest object, fill in any
    // blank required params, etc.

    
  }
}
