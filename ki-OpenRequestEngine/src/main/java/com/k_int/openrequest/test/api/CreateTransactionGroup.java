/**
 * Title:       dbtest
 * @version:    $Id: CreateTransactionGroup.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Take an iso PDU and store it, returning a message id.
 */

package com.k_int.openrequest.test.api;

import java.util.Properties;
import java.util.Vector;
import java.util.Hashtable;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.isoill.*;
import java.math.BigInteger;
import org.w3c.dom.*;
import javax.xml.parsers.*;


public class CreateTransactionGroup {

  public static void main(String[] args)
  {
    try
    {
      // Look up the factory object that will create a remote management object for us and
      // hand back a local proxy interface for that object.
      ManagerFactory manager_factory =
             (ManagerFactory) java.rmi.Naming.lookup("/OpenRequest/ManagementInterfaceFactory");

      // Use the factory to log on to the OpenRequest server, using our location symbol ("KI:REQ")
      // the credentials (PAssword or digital certificate) object is null. IE we assume
      // the location symbol is valid.
      LocationBasedManagementInterface manager_interface = manager_factory.create("KI:REQ",null);
    

      // Manually construct an XML document describing the item we are requesting.
      Document details = null;
      DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
      dfactory.setNamespaceAware(true);
      DocumentBuilder docBuilder = dfactory.newDocumentBuilder();
      details = docBuilder.newDocument();

      // The root element
      Element root = details.createElement("DocumentRequest");
      details.appendChild(root);

      // The title of the item we are looking for
      Element title = details.createElement("Title");
      root.appendChild(title);
      title.appendChild( details.createTextNode("Brain of the firm") );

      // The author of the item we are looking for
      Element author = details.createElement("Author");
      root.appendChild(author);
      author.appendChild( details.createTextNode("Beer, Stafford") );

      Element rota = details.createElement("ROTA");
      root.appendChild(rota);

      // Just one location to begin with...
      Element firstloc = details.createElement("LocationSymbol");
      rota.appendChild(firstloc);
      firstloc.appendChild(details.createTextNode("KI:RESP"));

      Long new_tg_id = manager_interface.createTransactionGroup("KI:REQ",
                                                                details,
                                                                new Hashtable());
      //                                                           "PatronName",
      //                                                           "PatronStatus",
      //                                                           "PatronSymbol",

      manager_interface.release();
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }

  }
}
