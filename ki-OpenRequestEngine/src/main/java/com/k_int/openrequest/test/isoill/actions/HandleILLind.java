/**
 * Title:       
 * @version:    $Id: HandleILLind.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.test.isoill.actions;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.util.*;
import org.hibernate.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.springframework.context.ApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class HandleILLind extends AbstractMessageHandler {

  public static Log log = LogFactory.getLog(HandleILLind.class);


  /**
   * Handle an incoming ISO 10161 indication event sent to a test location.
   *
   * Where possible, this handler should auto-respond to the incoming PDU if the test case
   * begin run is known and the next step is also known.
   *
   *  Preconditions: message parameter can be cast into the class expected by this handler
   *                 (ILLMessageEnvelope).
   *  Postconditions: 
   *
   *  @author Ian Ibbotson
   *  @param message An instance of ILL_APDU_type
   *  @param ctx Context for this handler
   *  @return Boolean object true
   *  @throws MessageHandlerException if an exceptional condition arose
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public Object handle(String event, 
                       Object message, 
                       MessageHandlerContext ctx,
                       ApplicationContext app_ctx) throws MessageHandlerException
  {
    log.debug("HandleILLind::handle("+event+",...,...)");
    // The trick is to look in the notes field to see if we can figure out what the response
    // should be and maybe auto respond...
    return Boolean.TRUE;
  }
}
