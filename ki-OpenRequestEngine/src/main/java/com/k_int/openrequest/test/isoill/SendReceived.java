/**
 * Title:       dbtest
 * @version:    $Id: SendReceived.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Take an iso PDU and store it, returning a message id.
 */

package com.k_int.openrequest.test.isoill;

import org.hibernate.*;
import java.util.*;

import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.io.FileInputStream;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.*;
import java.math.BigInteger;

public class SendReceived
{
  public static void main(String[] args)
  {
    // We create a brokered request by creating a new BrokeredTransactionRequestMessage object
    if ( args.length < 1 ) {
      System.err.println("usage: java com.k_int.openrequest.services.Controller app_context_file.xml");
      System.exit(0);
    }
                                                                                                                                                                            
                                                                                                                                                                            
    try
    {
      String app_context_config_file = args[0];
      ApplicationContext app_context = new ClassPathXmlApplicationContext( new String[] { app_context_config_file } );
      SessionFactory factory = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      Session sess = factory.openSession();

      Long id = new Long(Integer.parseInt(args[0]));
      ILLTransaction trans = DBHelper.lookupTransaction(sess,id);

      System.err.println("Result of looking up transaction "+id+": "+trans);

      ILL_APDU_type pdu = ISOReceivedMessageFactory.create(sess, trans, new Date(), null, null, null);

      try
      {
        ILLMessageEnvelope msg = 
               new ILLMessageEnvelope(pdu,
                                      trans.getLocation().getDefaultSymbol(),
                                      trans.getPVCurrentPartnerIdSymbol());

        MessageDispatcher mh = (MessageDispatcher) java.rmi.Naming.lookup("/OpenRequest/WorkflowController");
        // mh.handle("RCVreq",msg);
        mh.dispatch("System","RCVreq",msg);
      }
      catch ( Exception e )
      {
        System.err.println("Problem dispatching message");
        e.printStackTrace();
      }

      sess.flush();
      sess.connection().commit();
      sess.close();
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }
    finally
    {
      System.err.println("Done");
    }
  }
}
