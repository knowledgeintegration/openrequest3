/**
 * Title:       dbtest
 * @version:    $Id: SendReceived.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Take an iso PDU and store it, returning a message id.
 */

package com.k_int.openrequest.test.api;

import java.util.*;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import java.math.BigInteger;
import org.w3c.dom.*;
import javax.xml.parsers.*;

public class SendReceived {

  public static void main(String[] args) {

    Long internal_req_id = new Long(Integer.parseInt(args[0]));

    try {
        MessageDispatcher mh = (MessageDispatcher) java.rmi.Naming.lookup("/OpenRequest/WorkflowController");
        ManagerFactory manager_factory = 
             (ManagerFactory) java.rmi.Naming.lookup("/OpenRequest/ManagementInterfaceFactory");

        LocationBasedManagementInterface manager_interface = manager_factory.create("KI:REQ",null);

        ILLMessageEnvelope msg = manager_interface.createReceived(internal_req_id);

        // mh.handle("RCVreq",msg);
        mh.dispatch("System","RCVreq",msg);

        manager_interface.release();
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      System.err.println("Done");
    }
  }
}
