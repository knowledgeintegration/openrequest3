
/**
 * Server : Get incoming iso messages, send outgoing connection based messages
 *
 * @author Ian Ibbotson
 * @version $Id: ILL_APDU_Test_Factory.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 */

package com.k_int.openrequest.test.isoill;

import com.k_int.openrequest.isoill.*;

import java.net.*;
import java.io.*;
import java.util.*;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.helpers.*;
import java.math.BigInteger;

public interface ILL_APDU_Test_Factory
{
  public ILL_APDU_type create(org.hibernate.Session database_session,
                              Properties props) throws Exception;
}
