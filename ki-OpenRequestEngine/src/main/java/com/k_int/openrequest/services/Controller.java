/**
 * Title:       
 * @version:    $Id: Controller.java,v 1.7 2005/07/06 08:34:01 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.services;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tanukisoftware.wrapper.WrapperManager;
import org.tanukisoftware.wrapper.WrapperListener;


public class Controller implements WrapperListener {

  public static Log log = LogFactory.getLog(Controller.class);

  private boolean import_adi_symbols = true;
  public static java.util.Properties or_props = new java.util.Properties();

  public static void main (String args[]) {

    log.debug("OpenRequest startup");

    // Sanity Checks
    if ( com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.Extension_codec.HAND_MODIFIED_FLAG != 1 ) {
      log.warn("Incorrect version of Extension_codec. Cannot continue");
      System.exit(1);
    }
    else {
      log.debug("Packaing Sanity check OK");
    }

    try {
      java.io.InputStream is = Controller.class.getResourceAsStream("/or.properties");
      java.util.Properties or_props = new java.util.Properties();
      if ( is != null ) {
        or_props.load(is);
        log.info("OpenRequest "+or_props.get("app.fullversion")+" "+or_props.get("app.timestamp")+" Starting....");
      }
      else {
        log.warn("Warning: JAR file does not contain OpenRequest version information... probably an error in the build process");
      }
    }
    catch (Exception e) {
      log.warn("Message Dispatch Service exception",e);
    }

    // Added for NT Service integration
    WrapperManager.start( new Controller(), args );
  }

  private static void sanityCheck(ApplicationContext app_context) {
    log.debug("Performing application sanity checks");
  }

  public Controller() {
  }

  public void setImportADISymbols(boolean import_adi_symbols) {
    this.import_adi_symbols = import_adi_symbols;
  }

  public boolean getImportADISymbols() {
    return import_adi_symbols;
  }

  public Integer start( String[] args ) {
    try {
      log.info("Starting OpenRequest service");
      ClassPathXmlApplicationContext app_context = new ClassPathXmlApplicationContext( args );
      sanityCheck(app_context);
      log.info("OpenRequest server startup completed");
    }
    catch ( Exception e ) {
      log.error("Problem",e);
    }
    return null;
  }

  public int stop( int exitCode ) {
    return exitCode;
  }

  public void controlEvent( int event ) {
    if (WrapperManager.isControlledByNativeWrapper()) {
      // The Wrapper will take care of this event
      } else {
      // We are not being controlled by the Wrapper, so
      //  handle the event ourselves.
      if ((event == WrapperManager.WRAPPER_CTRL_C_EVENT) || (event == WrapperManager.WRAPPER_CTRL_CLOSE_EVENT) || (event == WrapperManager.WRAPPER_CTRL_SHUTDOWN_EVENT)){
        WrapperManager.stop(0);
      }
    }
  }
}
