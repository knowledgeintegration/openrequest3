/**
 * Title:       
 * @version:    $Id: Controller.java,v 1.7 2005/07/06 08:34:01 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.services;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.net.URL;
import java.io.File;
import java.io.InputStream;
import java.util.Properties;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.Context;
import org.apache.catalina.Engine;
import org.apache.catalina.Host;
import org.apache.catalina.startup.Embedded;
import org.apache.catalina.Container;
import org.apache.catalina.Wrapper;
import org.springframework.context.*;

public class EmbeddedTomcat implements ApplicationContextAware {

  public static Log log = LogFactory.getLog(EmbeddedTomcat.class);


  private static final String DEFAULT_ENGINE = "default";
  private static final String DEFAULT_HOST = "localhost";
  private static final String WEB_APPS_NAME = "webapps";
  private static final String DOC_BASE = "ROOT";
  private ApplicationContext ctx = null;

  private Embedded embedded;
  private String catalinaHome;
  private int port = 8080;

  public EmbeddedTomcat() {
    // Register a shutdown hook to do a clean shutdown
    Runtime.getRuntime().addShutdownHook(
      new Thread() {
        public void run() {
        stopServer();
      }
    });
  }

  public int setPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public void startServer() throws Exception {
    embedded.start();
  }

  public void stopServer() {
    if (embedded != null) {
      try {
        System.out.println("Shutting down OpenRequest embedded tomcat...");
        embedded.stop();
        System.out.println("OpenRequest embedded tomcat shutdown.");
      } catch (Exception e) {
        //No need to do anything
      }
    }
  }

  private void init() throws Exception {
    File home = (new File("..")).getCanonicalFile();
    catalinaHome = home.getAbsolutePath();

    embedded = new Embedded();
    embedded.setCatalinaHome(catalinaHome);

    // Create an Engine
    Engine engine = embedded.createEngine();
    engine.setName(DEFAULT_ENGINE);
    engine.setDefaultHost(DEFAULT_HOST);
    embedded.addEngine(engine);

    // Create a Host
    File webAppsLocation = new File(home, WEB_APPS_NAME);
    Host host = embedded.createHost( DEFAULT_HOST, webAppsLocation.getAbsolutePath());
    engine.addChild(host);


    // Add the context
    File docBase = new File(webAppsLocation, DOC_BASE);
    Context context = this.createContext("",docBase.getAbsolutePath());
    host.addChild(context);

    Properties props = new Properties();
    try {
      InputStream is = this.getClass().getResourceAsStream("/DefaultContext.properties");
      props.load(is);
    }
    catch(Exception e) {
    }

    // String prop_driver = "org.gjt.mm.mysql.Driver";
    // String prop_jdbc_url = "jdbc:mysql://localhost/OpenRequest?autoReconnect=true&amp;characterEncoding=utf8";
    // String prop_jdbc_dialect = "org.hibernate.dialect.MySQLDialect";
    // String prop_jdbc_user = "k-int";
    // String prop_jdbc_pass = "k-int";
    // String prop_auto_commit = "false";
    // String prop_query_substitutions = "true";
    // String prop_show_sql = "false";
    System.err.println("Web Context Props : "+props);
    String prop_driver = props.getProperty("com.k_int.openrequest.jdbc_driver");
    String prop_jdbc_url = props.getProperty("com.k_int.openrequest.url");
    String prop_jdbc_dialect = props.getProperty("com.k_int.openrequest.hiberate_dialect");
    String prop_jdbc_user = props.getProperty("com.k_int.openrequest.username");
    String prop_jdbc_pass = props.getProperty("com.k_int.openrequest.password");
    String prop_auto_commit = props.getProperty("com.k_int.openrequest.default_auto_commit");
    String prop_query_substitutions = props.getProperty("com.k_int.openrequest.query_substitutions");
    String prop_show_sql = props.getProperty("com.k_int.openrequest.show_sql");

    // now add the open request app
    Context or_context = this.createContext("/openrequest", webAppsLocation.getAbsolutePath()+"/openrequest");
    setContextParam(or_context,"com.k_int.openrequest.jdbc_driver",prop_driver);
    setContextParam(or_context,"com.k_int.openrequest.url",prop_jdbc_url);
    setContextParam(or_context,"com.k_int.openrequest.hiberate_dialect", prop_jdbc_dialect);
    setContextParam(or_context,"com.k_int.openrequest.username", prop_jdbc_user);
    setContextParam(or_context,"com.k_int.openrequest.password", prop_jdbc_pass);
    setContextParam(or_context,"com.k_int.openrequest.default_auto_commit", prop_auto_commit);
    setContextParam(or_context,"com.k_int.openrequest.query_substitutions", prop_query_substitutions);
    setContextParam(or_context,"com.k_int.openrequest.show_sql", prop_show_sql);

    org.apache.catalina.realm.JDBCRealm jdbc_realm = new org.apache.catalina.realm.JDBCRealm(); 
    jdbc_realm.setDriverName(prop_driver);
    jdbc_realm.setConnectionURL(prop_jdbc_url);
    jdbc_realm.setConnectionName(prop_jdbc_user);
    jdbc_realm.setConnectionPassword(prop_jdbc_pass);
    jdbc_realm.setUserTable("AI_AUTH_DETAILS");
    jdbc_realm.setUserNameCol("USERNAME");
    jdbc_realm.setUserCredCol("PASSWORD");
    jdbc_realm.setUserRoleTable("TC_ROLES");
    jdbc_realm.setRoleNameCol("ROLE");
    or_context.setRealm(jdbc_realm);

    host.addChild(or_context);

    // Create a connector that listens on all addresses port 8080
    Connector connector = embedded.createConnector( (String)null, port, false);
        
    // Wire up the connector
    embedded.addConnector(connector);

    startServer();
  }

  private Context createContext(String path, String docBase){
    // Create a Context
    Context context = embedded.createContext(path, docBase);
    context.setParentClassLoader(this.getClass().getClassLoader());

    // Create a default servlet
    Wrapper servlet = context.createWrapper();
    servlet.setName("default");
    servlet.setServletClass( "org.apache.catalina.servlets.DefaultServlet");
    servlet.setLoadOnStartup(1);
    servlet.addInitParameter("debug", "0");
    servlet.addInitParameter("listings", "false");
    context.addChild(servlet);
    context.addServletMapping("/", "default");

    // Create a handler for jsps
    Wrapper jspServlet = context.createWrapper();
    jspServlet.setName("jsp");
    jspServlet.setServletClass( "org.apache.jasper.servlet.JspServlet");
    jspServlet.addInitParameter("fork", "false");
    jspServlet.addInitParameter("xpoweredBy", "false");
    jspServlet.setLoadOnStartup(2);
    context.addChild(jspServlet);
    context.addServletMapping("*.jsp", "jsp");
    context.addServletMapping("*.jspx", "jsp");

    // Set seme default welcome files
    context.addWelcomeFile("index.html");
    context.addWelcomeFile("index.htm");
    context.addWelcomeFile("index.jsp");
    context.setSessionTimeout(30);

    // Add some mime mappings
    context.addMimeMapping("html", "text/html");
    context.addMimeMapping("htm", "text/html");
    context.addMimeMapping("gif", "image/gif");
    context.addMimeMapping("jpg", "image/jpeg");
    context.addMimeMapping("png", "image/png");
    context.addMimeMapping("js", "text/javascript");
    context.addMimeMapping("css", "text/css");
    context.addMimeMapping("pdf", "application/pdf");

    return context;
  }

  private void setContextParam(Context ctx, String name, String value) {
    org.apache.catalina.deploy.ApplicationParameter param = new org.apache.catalina.deploy.ApplicationParameter();
    param.setName(name);
    param.setValue(value);
    ctx.addApplicationParameter(param);
  }

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }

}
