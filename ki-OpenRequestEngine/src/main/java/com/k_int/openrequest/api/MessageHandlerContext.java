/**
 * Title:       
 * @version:    $Id: MessageHandlerContext.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.api;

import org.hibernate.*;
import java.util.*;

public class MessageHandlerContext
{
  protected Session session = null;
  protected MessageDispatcher dispatcher = null;
  protected String rmi_naming_context = null;

  public MessageHandlerContext(Session s, MessageDispatcher d, String rmi_naming_context)
  {
    this.session = s;
    this.dispatcher = d;
    this.rmi_naming_context = rmi_naming_context;
  }

  public Session getSession()
  {
    return session;
  }

  public MessageDispatcher getDispatcher()
  {
    return dispatcher;
  }

  public String getRMINamingContext()
  {
    return rmi_naming_context;
  }
}
