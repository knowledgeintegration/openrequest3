/**
 * Title:       
 * @version:    $Id: LocalMessageDispatcher.java,v 1.3 2005/06/11 18:42:50 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.api;

import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.DBHelper;
import org.hibernate.*;
import org.hibernate.type.*;
import java.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.lang.Thread; 
import java.lang.Runnable; 
import com.k_int.openrequest.services.*;
import org.springframework.context.*;
import java.util.concurrent.*;


/**
 * Essentially this is the "topic" for broadcasting OR events. Could be replaced with JMX topic or spring events.
 */
public class LocalMessageDispatcher  implements MessageDispatcher, ApplicationContextAware {

  private static Log log = LogFactory.getLog(LocalMessageDispatcher.class);

  private Map registration_by_channel = new HashMap();
  private Map registration_by_handlers = new HashMap();
  private List event_queue = new ArrayList();
  private boolean running = true;
  private ApplicationContext ctx = null;
  

  // Number of threads in dispatcher thread pool
  // private ThreadPool thread_pool = new ThreadPool(20);
  private ExecutorService thread_pool = null;

  private class HandlerTask implements Runnable {
    String event=null;
    Object message=null;
    MessageRecipient mh=null;
    public HandlerTask(MessageRecipient mh, String event, Object message) {
      this.event=event;
      this.message=message;
      this.mh=mh;
    }

    public void run() {
      log.debug("<<<<<<< run task, recipient="+mh.getClass().getName());
      try {
          mh.handle(event, message);
      }
      catch ( com.k_int.openrequest.api.MessageHandlerException mhe ) {
        log.error("Problem handling event", mhe);
        // deregisterObserver(mh);
      }
      log.debug(">>>>>>> run task complete...");
    }
  };

  Runnable dispatcher_thread = new Runnable() {
    public void run() {
      log.debug("Message dispatcher thread has started...");
      while ( running ) {
        while ( event_queue.size() > 0 ) {
          log.debug("Processing.... queue size = "+event_queue.size());

          try {
            HandlerTask ht = (HandlerTask) event_queue.remove(0);
            thread_pool.execute(ht);
          }
          finally {
          }
        }

        // Wait for something else to put tasks on the queue.
        log.debug("Sleeping on event queue (size="+event_queue.size()+")");
        synchronized ( event_queue ) {
          try {
            event_queue.wait();
          }
          catch ( java.lang.InterruptedException ie ) {
          }
          log.debug("MessageDispatcher thread waking up....");
        }
      }
    }
  };

  public LocalMessageDispatcher(int thread_pool_size) {
    super();
    log.debug("LocalMessageDispatcher::LocalMessageDispatcher("+thread_pool_size+")");
    thread_pool = Executors.newFixedThreadPool(thread_pool_size);
    startup();
  }

  private void startup() {
    try {
      thread_pool.execute(dispatcher_thread);
    }
    finally {
    }
  }

  /**
   * Take a message and see if we can locate any targets interested in that message.
   */
  public Object dispatch(String channel, 
                         String event, 
                         Object message) throws MessageHandlerException {

    log.info("dispatch channel="+channel+", event="+event+", message="+message);

    List l = (List) registration_by_channel.get(channel);

    if ( l != null ) {
      log.debug("Processing "+l.size()+" registrations for channel");

      for ( Iterator i = l.iterator(); i.hasNext(); ) {
        MessageRecipient mh = (MessageRecipient) i.next();
        log.debug("Adding handler task "+mh+" to event queue");
        event_queue.add(new HandlerTask(mh,event,message));
      }

      log.debug("Notify all....");
      synchronized(event_queue) {
        event_queue.notifyAll();
      }
    }
    else {
      log.debug("No registrations for channel: "+channel);
      log.debug("Current registrations:"+registration_by_channel);
    }

    log.debug("All done....");
    return Boolean.TRUE;
  }

  public void registerObserver(String channel, MessageRecipient h) {
    log.debug("registerObserver "+h+" in channel "+channel);
    List registrations_for_channel = getOrCreateChannelRegistrations(channel);
    List registrations_for_handler = getOrCreateHandlerRegistrations(h);
    log.debug("Adding handler to channel registrations ("+h+")");
    registrations_for_channel.add(h);
    log.debug("Adding channel to handler registrations ("+channel+")");
    registrations_for_handler.add(channel);
  }

  public void deregisterObserver(MessageRecipient h) {
    // Find all channels this handler is subscribed to and remove
    log.debug("deregisterObserver");
    // Find out which channels have this recipient attached to them
    List registrations_for_handler = getOrCreateHandlerRegistrations(h);
    for ( Iterator i = registrations_for_handler.iterator(); i.hasNext(); ) {
      String channel_id = (String) i.next();
      List registrations_for_channel = getOrCreateChannelRegistrations(channel_id);
      registrations_for_channel.remove(h);
    }
    registration_by_handlers.remove(h);

    log.debug("At end of deregisterObserver");
    log.debug("registration_by_handlers="+registration_by_handlers);
    log.debug("registrations_for_handler="+registrations_for_handler);
  }

  private List getOrCreateChannelRegistrations(String channel) {
    List channel_registrations = (List) registration_by_channel.get(channel);

    if ( channel_registrations == null ) {
      log.debug("Creating new channel registrations array for channel "+channel);
      channel_registrations = new ArrayList();
      registration_by_channel.put(channel, channel_registrations);
    }

    return channel_registrations;
  }

  private List getOrCreateHandlerRegistrations(MessageRecipient h) {
    List handler_registrations = (List) registration_by_handlers.get(h);

    if ( handler_registrations == null ) {
      handler_registrations = new ArrayList();
      registration_by_handlers.put(h, handler_registrations);
    }
    return handler_registrations;
  }

  public void init() {
    log.debug("init...");
  }

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }
}
