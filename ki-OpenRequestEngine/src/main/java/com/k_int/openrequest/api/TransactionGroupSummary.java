package com.k_int.openrequest.api;

import java.util.*;
import java.rmi.*;

/**
 * Title:       
 * @version:    $Id: TransactionGroupSummary.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class TransactionGroupSummary implements java.io.Serializable
{
  private String criteria_name;
  private String criteria_descriptions;
  private long count;

  public TransactionGroupSummary(String criteria_name,
                                 String criteria_descriptions,
                                 long count)
  {
    this.criteria_name = criteria_name;
    this.criteria_descriptions = criteria_descriptions;
    this.count = count;
  }

  public String getCriteriaName()
  {
    return criteria_name;
  }

  public String getCriteriaDescription()
  {
    return criteria_descriptions;
  }

  public long getCount()
  {
    return count;
  }
}
