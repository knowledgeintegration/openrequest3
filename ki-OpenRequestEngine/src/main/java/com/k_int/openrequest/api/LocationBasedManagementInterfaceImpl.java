/**
 * Title:       
 * @version:    $Id: LocationBasedManagementInterfaceImpl.java,v 1.4 2005/07/07 17:57:19 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.api;
import java.util.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.*;
import org.jzkit.a2j.codec.util.*;
import org.hibernate.*;
import org.w3c.dom.Document;
import com.k_int.openrequest.api.Internal.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import org.springframework.context.*;
import java.math.BigInteger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Title:       
 * @version:    $Id: LocationBasedManagementInterfaceImpl.java,v 1.4 2005/07/07 17:57:19 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class LocationBasedManagementInterfaceImpl implements LocationBasedManagementInterface {

  // Session the_session = null;
  String the_location_symbol = null;
  private SessionFactory sf = null;
  private Long internal_loc_id = null;
  private static int instance_counter = 0;
  private ApplicationContext ctx = null;

  public static Log cat = LogFactory.getLog(LocationBasedManagementInterfaceImpl.class);

  public LocationBasedManagementInterfaceImpl(ApplicationContext ctx,
                                              String location_symbol, 
                                              Object credentials) throws OpenRequestException {
    super();

    cat.debug("new LocationBasedManagementInterfaceImpl: "+(++instance_counter));
    the_location_symbol = location_symbol;
    this.sf = (SessionFactory) ctx.getBean("OpenRequestSessionFactory");
    this.ctx = ctx;

    try {
      Session the_session = getSession();
      LocationSymbol loc = DBHelper.lookupLocation(the_session,location_symbol);
      internal_loc_id = loc.getCanonical_location().getId();
      releaseSession(the_session);
    }
    catch ( Exception e ) {
      cat.warn("Problem",e);
    }
  }

  protected void finalize() {
    cat.debug("finalize LocationBasedManagementInterfaceImpl: "+(--instance_counter));
  }

  public void release() throws OpenRequestException {
    cat.debug("LocationBasedManagementInterfaceImpl::release");
  }

  private Session getSession() throws org.hibernate.HibernateException {
    return sf.openSession();
    // return the_session;
  }

  private void releaseSession(Session s) {
    try {
      s.flush();
      s.connection().commit();
      s.close();
    } 
    catch ( Exception e ) {
    }
  }

  /** Create a new transaction group for the location being managed by this instance.
   *  The TGQ returned is may only be unique within the context of a specific requester
   *  symbol.
   *
   *  @param patron_name A String containing the patron name (Or null)
   *  @param patron_status A String containing the patron status (Or null)
   *  @param patron_symbol A String containing the patron symbol
   *  @param request_doc An XML document containing details of the object to be requested and
   *                     A list of locations to try for this doc. 
   *                     If no such list is found, subsequent implementations may use JZKit to go out and
   *                     search configured holdings databases to try and discover a list of possible
   *                     locations.
   *  @return A string value containing the TGQ of the created transaction group.
   *  removed                        String patron_name,
   *  removed                        String patron_status,
   *  removed                        String patron_symbol,
   *
   */
  public Long createTransactionGroup(String requesting_symbol,
                                     Document request_details_doc,
                                     Hashtable additional_attrs) throws OpenRequestException {
    Session session = null;
    ILLTransactionGroup result = null;

    String required_requesting_symbol = null;
    if ( requesting_symbol != null )
      required_requesting_symbol = requesting_symbol;
    else
      required_requesting_symbol = the_location_symbol;

    try
    {
      session = getSession();
      LocationSymbol requester_loc = DBHelper.lookupLocation(session,required_requesting_symbol);

      result = TransactionGroupFactory.create(session,
                                              requester_loc.getCanonical_location(),
                                              requester_loc.toString(),
                                              requester_loc.toString(),
                                              request_details_doc,
                                              "REQ",
                                              additional_attrs);
      session.flush();
      session.connection().commit();
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return result.getId();
  }

  public ILLMessageEnvelope createRequest (String tgq,
                                      String requester_symbol,
                                      String responder_symbol,
                                      String initial_requester_symbol,
                                      String patron_symbol,
                                      String patron_status,
                                      String patron_name,
                                      Boolean allow_forwarding,
                                      Boolean allow_chaining,
                                      Boolean allow_partitioning,
                                      Boolean allow_change_send_to_list,
                                      long transaction_type,
                                      Document request_doc) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();
        LocationSymbol requester = DBHelper.lookupLocation(session,requester_symbol);
        LocationSymbol responder = DBHelper.lookupLocation(session,responder_symbol);

        Object[] apdu_deliv_params = { requester, responder, null };
        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISORequestMessageFactory.create(session,
                                                 tgq, // No existing tgq
                                                 requester, // requester symbol
                                                 responder, // responder symbol
                                                 requester, // initial requester symbol
                                                 patron_symbol, // patron symbol
                                                 patron_status, // patron status
                                                 patron_name, // patron name
                                                 allow_forwarding, // allow forwarding
                                                 allow_chaining, // allow chaining
                                                 allow_partitioning, // allow partitioning
                                                 allow_change_send_to_list, // allow chg snd to lst
                                                 transaction_type, // Transaction Type : Simple
                                                 request_doc,
                                                 extensions,
                                                 ctx);

      retval = new ILLMessageEnvelope(pdu, requester_symbol, responder_symbol);
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }

  public ILLMessageEnvelope createForwardIndication(Long internal_request_id,
                                               String new_responder_symbol,
                                               String note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol new_responder = 
               DBHelper.lookupLocation(session,new_responder_symbol);
        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOForwardNotificationMessageFactory.create(session,
                                                 transaction,
                                                 new_responder,
                                                 note,
                                                 extensions);
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }


  public ILLMessageEnvelope createShipped (Long internal_request_id,
                                           long shipped_service_type,
                                           Date date_shipped,
                                           Date date_due,
                                           Boolean renewable,
                                           Long chargeable_units,
                                           String total_cost_currency,
                                           String total_cost_value,
                                           Long conditions,
                                           String note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
      session = getSession();

      cat.debug("Looking up transaction "+internal_request_id);
      ILLTransaction trans = DBHelper.lookupTransaction(session,internal_request_id);

      LocationSymbol sender = 
               DBHelper.lookupLocation(session,trans.getLocation().getDefaultSymbol());
      LocationSymbol recipient = 
               DBHelper.lookupLocation(session,trans.getPVCurrentPartnerIdSymbol());

      // Sender, Recipient, Transponder
      Object[] apdu_deliv_params = { sender, recipient, null };

      ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

      pdu = ISOShippedMessageFactory.create(session,
                                            sender,
                                            recipient,
                                            trans,
                                            shipped_service_type, // Shipped service type
                                            date_shipped, //shipped date
                                            date_due, //due date
                                            renewable,
                                            chargeable_units, // Chargable units
                                            total_cost_currency,
                                            total_cost_value,
                                            conditions,
                                            null, // return to address name 
                                            null, // return to extended postal
                                            null, // return to street and number
                                            null, // return to pobox
                                            null, // return to city
                                            null, // return to region
                                            null, // return to country
                                            null, // return to postcode
                                            note,
                                            extensions,
                                            ctx);

      retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createConditionalAnswer(Long internal_request_id,
                                               int condition,
                                               Date date_for_reply,
                                               String responder_note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOAnswerMessageFactory.createConditionalAnswer(session,
                                                              sender,
                                                              recipient,
                                                 transaction,
                                                 condition,
                                                 date_for_reply,
                                                 responder_note,ctx);
                                                 // TODO: extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createRetryAnswer(Long internal_request_id,
                                         int reason_not_available,
                                         Date retry_date,
                                         ArrayList location_symbols) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();
        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOAnswerMessageFactory.createRetryAnswer(session,
                                                        sender,
                                                        recipient,
                                                 transaction,
                                                 reason_not_available,
                                                 retry_date,
                                                 location_symbols,null,ctx);
                                                 // TODO: extensions);
        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createUnfilledAnswer(Long internal_request_id,
                                            int reason_unfilled,
                                            ArrayList location_symbols) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOAnswerMessageFactory.createUnfilledAnswer(session,
                                                           sender,
                                                           recipient,
                                                 transaction,
                                                 reason_unfilled,
                                                 location_symbols,
                                                 null, // responder_note
                                                 ctx);
                                                 // TODO: extensions);
        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createLocationsProvidedAnswer(Long internal_request_id,
                                            int reason_locations_provided,
                                            ArrayList location_symbols) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOAnswerMessageFactory.createLocationsProvidedAnswer(session,
                                                           sender,
                                                           recipient,
                                                 transaction,
                                                 reason_locations_provided,
                                                 location_symbols,null,ctx);
                                                 // TODO: extensions);
        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createWillSupplyAnswer(Long internal_request_id,
                                              int reason_will_supply,
                                              Date supply_date,
                                              Postal_Address_type return_to_address,
                                              ArrayList locations) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOAnswerMessageFactory.createWillSupplyAnswer(session,
                                                           sender,
                                                           recipient,
                                                 transaction,
                                                 reason_will_supply,
                                                 supply_date,
                                                 return_to_address,
                                                 locations,
                                                 null,
                                                 null,
                                                 ctx);
                                                 // TODO: extensions);
        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }


  
  public ILLMessageEnvelope createHoldPlacedAnswer(Long internal_request_id,
                                              Date estimated_date_available,
                                              int hold_placed_medium_type,
                                              ArrayList locations)  throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOAnswerMessageFactory.createHoldPlacedAnswer(session,
                                                             sender,
                                                             recipient,
                                                             transaction,
                                                             estimated_date_available,
                                                             hold_placed_medium_type,
                                                             locations,
                                                             null, // responder_note
                                                             ctx);

                                                 // TODO: extensions);
        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createEstimateAnswer(Long internal_request_id,
                                            String estimate,
                                            ArrayList locations) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOAnswerMessageFactory.createEstimateAnswer(session,
                                                           sender,
                                                           recipient,
                                                 transaction,
                                                 estimate,
                                                 locations,
                                                 null, // responder_note
                                                 ctx);
                                                 // TODO: extensions);
        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createConditionalReply(Long internal_request_id,
                                              Boolean answer,
                                              String note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOConditionalReplyMessageFactory.create(session,
                                                 transaction,
                                                 answer,
                                                 note,
                                                 extensions);
        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createCancel(Long internal_request_id,
                                    String requester_note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOCancelMessageFactory.create(session,
                                                transaction,
                                                requester_note,
                                                extensions);
        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createCancelReply(Long internal_request_id,
                                         Boolean answer,
                                         String note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOCancelReplyMessageFactory.create(session,
                                                transaction,
                                                answer,
                                                note,
                                                extensions);
        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
    // throw new NotImplementedException("Error");
  }



  public ILLMessageEnvelope createReceived (Long internal_request_id) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
      session = getSession();

      ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

      LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
      LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

      pdu = ISOReceivedMessageFactory.create(session, transaction, new Date(), null, null, null);

      retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createRecall(Long internal_request_id,
                                    String requester_note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISORecallMessageFactory.create(session,
                                                transaction,
                                                requester_note,
                                                extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createReturned(Long internal_request_id,
                                      Date date_returned,
                                      String returned_via,
                                      String requester_note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOReturnedMessageFactory.create(session,
                                                transaction,
                                                null,
                                                date_returned,
                                                returned_via,
                                                null,
                                                requester_note,
                                                extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createCheckedIn(Long internal_request_id,
                                       Date date_checked_in,
                                       String responder_note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOCheckedInMessageFactory.create(session,
                                                   transaction,
                                                   date_checked_in,
                                                   responder_note,
                                                   extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }

  public ILLMessageEnvelope createOverdue(Long internal_request_id,
                                     Date date_due,
                                     boolean renewable,
                                     String responder_note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOOverdueMessageFactory.create(session,
                                                transaction,
                                                date_due,
                                                renewable,
                                                responder_note,
                                                extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createRenew(Long internal_request_id,
                                   Date desired_due_date,
                                   String requester_note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISORenewMessageFactory.create(session,
                                               transaction,
                                               desired_due_date,
                                               requester_note,
                                               extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createRenewAnswer(Long internal_request_id,
                                         boolean answer,
                                         Date date_due,
                                         boolean renewable,
                                         String responder_note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISORenewAnswerMessageFactory.create(session,
                                                     transaction,
                                                     answer,
                                                     date_due,
                                                     renewable,
                                                     responder_note,
                                                     extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createLost(Long internal_request_id,
                                  String note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOLostMessageFactory.create(session,
                                              transaction,
                                              note,
                                              extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createDamaged(Long internal_request_id,
                                     String note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISODamagedMessageFactory.create(session,
                                              transaction,
                                              note,
                                              extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createMessage(Long internal_request_id,
                                     String note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOMessageMessageFactory.create(session,
                                              transaction,
                                              note,
                                              extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createStatusQuery(Long internal_request_id,
                                         String note) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOStatusQueryMessageFactory.create(session,
                                              transaction,
                                              note,
                                              extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createStatusOrErrorReport(Long internal_request_id) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = 
               DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = 
               DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOStatusOrErrorReportMessageFactory.create(session,
                                              transaction,
                                              BigInteger.valueOf(0),null,null,null,
                                              extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }



  public ILLMessageEnvelope createExpired(Long internal_request_id) throws OpenRequestException {
    Session session = null;
    ILL_APDU_type pdu = null;
    ILLMessageEnvelope retval = null;

    try
    {
        session = getSession();

        ILLTransaction transaction = DBHelper.lookupTransaction(session, internal_request_id);

        LocationSymbol sender = DBHelper.lookupLocation(session,transaction.getLocation().getDefaultSymbol());
        LocationSymbol recipient = DBHelper.lookupLocation(session,transaction.getPVCurrentPartnerIdSymbol());

        // Sender, Recipient, Transponder
        Object[] apdu_deliv_params = { sender, recipient, null };

        ISOExtensionFactory[] extensions = { new ISOAPDUDeliveryInfoExtension(session, apdu_deliv_params) };

        pdu = ISOExpiredMessageFactory.create(session,
                                              transaction,
                                              extensions);

        retval = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }

  public ILLTransactionGroupInfo lookupTransactionGroup(Long tgq_id) throws OpenRequestException {
    Session session = null;
    ILLTransactionGroupInfo retval = null;

    try
    {
      session = getSession();
      ILLTransactionGroup tg = DBHelper.lookupGAR(session, tgq_id);
      retval = new ILLTransactionGroupInfoImpl(tg);
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    finally
    {
      releaseSession(session);
    }


    return retval;
  }


  public ILLTransactionGroupInfo lookupTransactionGroup(String requester_id, String tgq) throws OpenRequestException {
    Session session = null;
    ILLTransactionGroupInfo retval = null;

    // try
    // {
    //   session = getSession();
      // ILLTransactionGroup tg = DBHelper.findExistingTG(session, tgq_id);

      // retval = new ILLTransactionGroupInfoImpl(tg);
    //   retval = null;
    // }
    // catch ( java.sql.SQLException sqle )
    // {
    //   cat.warn("Problem",sqle);
    // }
    // catch ( org.hibernate.HibernateException he )
    // {
    //   cat.warn("Problem",he);
    // }
    // finally
    // {
    //   releaseSession(session);
    // }

    // return retval;
    throw new NotImplementedException("Error");
  }

  public LocationSymbolInfo lookupAlias(String alias) throws OpenRequestException {
    throw new NotImplementedException("Error");
  }

  public ILLTransactionInfo lookupTransaction(Long internal_transaction_id) throws OpenRequestException {
    throw new NotImplementedException("Error");
  }

  public TransactionGroupSummary[] getSummary(String report_name) throws OpenRequestException {
    throw new NotImplementedException("Error");
  }

  public TransactionGroupSet findTransactionGroups(String named_criteria) throws OpenRequestException {
    throw new NotImplementedException("Error");
  }

  public TransactionGroupSet findTransactionGroups(String role,
                                                   Boolean only_with_unread_messages,
                                                   String required_live_transaction_status_code,
                                                   String required_transaction_group_status
                                                   ) throws OpenRequestException {
    throw new NotImplementedException("Error");
  }

  public Long resolveTransactionID(String initial_requester,
                                   String tgq, 
                                   String tq, 
                                   String stq) throws OpenRequestException {
    Long retval = null;
    Session session = null;

    try
    {
      session = getSession();
      LocationSymbol this_loc = DBHelper.lookupLocation(session,the_location_symbol);

      ILLTransaction transaction = DBHelper.lookupTransaction(session,
                                                 this_loc.getCanonical_location().getId(),
                                                 initial_requester,
                                                 tgq,
                                                 tq,
                                                 stq);
      if ( transaction != null )
        retval = transaction.getId();
    }
    catch ( java.sql.SQLException sqle )
    {
      cat.warn("Problem",sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      cat.warn("Problem",he);
    }
    catch ( com.k_int.openrequest.helpers.HelperException helper_excep )
    {
      cat.warn("Problem",helper_excep);
    }
    finally
    {
      releaseSession(session);
    }

    return retval;
  }

  public Long getInternalLocId() {
    return internal_loc_id;
  }
}
