/**
 * Title:       
 * @version:    $Id: MessageIndicationDetails.java,v 1.4 2005/02/25 16:10:00 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.api;

import java.io.Serializable;
import java.util.Map;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import com.k_int.openrequest.db.MessageHeader;

public class MessageIndicationDetails implements Serializable {
  private Long transaction_group_id = null;
  private Long transaction_id = null;
  private Long message_id = null;
  private ILLMessageEnvelope message_envelope;

  /** WARNING: message_as_stored_in_db: This object cannot be accessed over the remote interface.. it's deeply nested in the database */
  private transient MessageHeader message_as_stored_in_db;

  private Map context;

  public MessageIndicationDetails(Long transaction_group_id,
                                  Long transaction_id,
                                  Long message_id) {
    this(transaction_group_id,transaction_id,message_id,null,null,null);
  }

  public MessageIndicationDetails(Long transaction_group_id,
                                  Long transaction_id,
                                  Long message_id,
                                  ILLMessageEnvelope message_envelope,
                                  MessageHeader message_as_stored_in_db,
                                  Map context) {
    this.transaction_group_id = transaction_group_id;
    this.transaction_id = transaction_id;
    this.message_id = message_id;
    this.message_envelope = message_envelope;
    this.context = context;
    this.message_as_stored_in_db = message_as_stored_in_db;
  }


  public Long getTransactionGroupId() {
    return transaction_group_id;
  }

  public Long getTransactionId() {
    return transaction_id;
  }

  public Long getMessageId() {
    return message_id;
  }

  public ILLMessageEnvelope getMessage() {
    return message_envelope;
  }

  public Map getContext() {
    return context;
  }

  public MessageHeader getMessageAsStoredInDB() {
    return message_as_stored_in_db;
  }

  public String toString() {
    return "MessageIndicationDetails("+ transaction_group_id +","+ transaction_id +","+ message_id + ","+ message_envelope+", DBObj)";
  }
}
