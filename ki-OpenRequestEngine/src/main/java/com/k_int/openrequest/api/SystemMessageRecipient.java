/**
 * Title:       
 * @version:    $Id: SystemMessageRecipient.java,v 1.3 2005/06/09 13:43:25 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.api;

import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.DBHelper;
import java.util.*;
import org.hibernate.*;
import org.hibernate.type.*;
import com.k_int.openrequest.services.*;
import org.springframework.context.ApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * This is the top level pivot point for protocol messages in OpenRequest. Messages from many different protocols and sources
 * eventally end up here where a suitable handler is selected and invoked.
 */
public class SystemMessageRecipient extends AbstractMessageRecipient
{
  private MessageDispatcher dispatcher = null;
  private String rmi_server_url = null;
  private SessionFactory sf = null;
  private Log log = LogFactory.getLog(SystemMessageRecipient.class);

  public SystemMessageRecipient() {
    super();
  }

  public Object handle(String event, Object message) throws MessageHandlerException {
    Object result = null;
    String location_type = "SYSTEM";

    log.debug("handle "+location_type+","+event+","+message);

    Session handler_session = null;
    try {

      // Each handler gets it's own connection to the database
      handler_session = sf.openSession();

      log.debug("Looking up location type "+location_type);
      LocationType system_location_type = DBHelper.lookupLocationType(handler_session,location_type);

      log.debug("Looking up event handler for event type "+event+" location type "+location_type);
      EventHandler eh = (EventHandler) system_location_type.getLocationEventHandlers().get(event);

      if ( eh != null ) {

        log.debug("Found matching event handler : "+eh.getHandlerType());

        // ActionHandlerParams should contain the class name of the handler
        MessageHandlerContext msg_ctx = new MessageHandlerContext(handler_session, dispatcher, rmi_server_url);

        String handler_class_name = eh.getHandlerParams();

        log.debug("Handling with "+handler_class_name);

        Class handler_class = Class.forName(handler_class_name);
        AbstractMessageHandler amh = (AbstractMessageHandler) handler_class.newInstance();
        result = amh.handle(event,message,msg_ctx,ctx);

        log.debug("Flush and commit session");
        handler_session.flush();
        handler_session.connection().commit();
        handler_session.clear();
      }
      else {
        log.warn("No system handler for event "+event);
      }
    }
    catch ( org.hibernate.MappingException me )
    {
      log.warn("Hibernate mapping Problem in handler",me);
      throw new MessageHandlerException(me.toString(),me);
    }
    catch ( java.sql.SQLException sqle )
    {
      log.warn("SQL Problem handler",sqle);
      throw new MessageHandlerException(sqle.toString(),sqle);
    }
    catch ( org.hibernate.HibernateException he )
    {
      log.warn("Hibernate Problem handler",he);
      throw new MessageHandlerException(he.toString(),he);
    }
    catch ( java.lang.ClassNotFoundException cnfe )
    {
      log.warn("Problem trying to instansiate handler",cnfe);
      throw new MessageHandlerException(cnfe.toString(), cnfe);
    }
    catch ( java.lang.InstantiationException ie )
    {
      log.warn("Problem trying to instansiate handler",ie);
      throw new MessageHandlerException(ie.toString(), ie);
    }
    catch ( java.lang.IllegalAccessException iae )
    {
      log.warn("Problem trying to instansiate handler",iae);
      throw new MessageHandlerException(iae.toString(),iae);
    }
    catch ( Exception e )
    {
      log.warn("Problem trying to instansiate handler",e);
      throw new MessageHandlerException("Problem trying to instansiate handler",e);
    }
    finally
    {
      if ( handler_session != null ) {
        try {
          handler_session.close();
        }
        catch ( Exception e ) {
        }
      }
    }

    return result;
  }

  public void init() {
    log.debug("Initialise SystemMessageRecipient");
    sf = (SessionFactory) ctx.getBean("OpenRequestSessionFactory");

    log.debug("Getting hold of message dispatcher");
    dispatcher = (MessageDispatcher) ctx.getBean("DISPATCHER");

    log.debug("Registering \"System\" message recipient"); 
    dispatcher.registerObserver("System", this);

    log.debug("SystemMessageRecipient init completed");
  }

  public void stop()
  {
    log.debug("Stop SystemMessageRecipient");
  }
}
