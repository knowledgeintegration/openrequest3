/**
 * Title:       
 * @version:    $Id: MessageDispatcher.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.api;

/**
 * Title:       
 * @version:    $Id: MessageDispatcher.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Locate and invoke message specific handlers given specific application events.
 *              Handlers are looked up from the location_event_handler table by finding the
 *              location details for the event recipient and then looking up a handler for that
 *              locations generic type. The "System" location is used to hold all generic system
 *              level events.
 */
public interface MessageDispatcher {
  /**
   *  Handle an application event.
   *  This class is responsible for finding a handler class capable of dealing with
   *  and the call will be made by that component.
   *
   *  Requires<ul><li>A valid event_code. Event codes are defined in the database.
   *                 An object for the message parameter which matches the preconditions
   *                 for the supplied event code.
   *  </ul>
   *  Provides<ul><li> A handler specific object from the task specific realisation of the
   *                  AbstractMessageHandler object or null if no value was returned.
   *  <ul>
   *
   *  @author Ian Ibbotson
   *  @param channel A string indicating what channel the event is broadcast to
   *  @param event_code A string indicating what the event is.
   *  @param message Application supplied parameter containing information needed to
   *                 handle the event
   *  @return Whatever object the specific realisation of the handler base class requires.
   *  @throws MessageHandlerException if an exceptional condition arose
   */

  public Object dispatch(String channel,
                         String event_code,
                         Object message) throws MessageHandlerException;

  public void registerObserver(String channel, MessageRecipient h);
  public void deregisterObserver(MessageRecipient h);
}
