package com.k_int.openrequest.api.Internal;

import java.util.*;
import java.rmi.*;
import java.rmi.server.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;

/**
 * 
 * @version:    $Id: ILLTransactionGroupInfoImpl.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class ILLTransactionGroupInfoImpl implements ILLTransactionGroupInfo
{
  private ILLTransactionGroup local_tg = null;

  public ILLTransactionGroupInfoImpl() {
  }

  public ILLTransactionGroupInfoImpl(ILLTransactionGroup local_tg) {
    this.local_tg = local_tg;
  }
}
