package com.k_int.openrequest.api;

import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.w3c.dom.Document;
import java.util.*;
import org.jzkit.a2j.codec.util.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;

/**
 * Provides the primary interface that an ILL System developer will use to create, track
 * and modify ill transactions and related objects.
 * @version:    $Id: LocationBasedManagementInterface.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: This is the primary class for end users of the OpenRequest engine. It provides access
 *              to all message creation and tracking facilities. The message creation functions are used
 *              to create messages which will be later processsed by instances of MessageDispatcher.
 *              <br>ToDo:<br><ul>
 *              <li>Question: We need some kind of api for list-of-values on things like conditional codes.
 *              <li>Good generic address object!
 *              </ul>
 *              LocationAgnostic is a marker interface that indicates we may want to use this interface
 *              across arbritrary RPC mechanisms (Including RMI, SOAP). When accessed remotely, any class
 *              implementing LocationAgnostic will be wrapped with a SmartProxy object.
 */
public interface LocationBasedManagementInterface {
  /**
   *  Disconnect from the request management service.
   *
   *  <br>REQUIRES: <ul>
   *  <li>A valid instance of this LocationBasedManagementInterface which has not already been released.
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li>A clean session shutdown that will 
   *                  Commit any open transactions, clean up any cached objects and release any
   *                  held jdbc connections. After a call to this method the released instance should
   *                  never be reused.
   *  <ul>
   *
   *  @author Ian Ibbotson
   *  @since 1.0
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   */
  public void release() throws OpenRequestException;

  /**
   *  Create a new interlending transaction group for the item cited in the 
   *  request_item_details_doc XML document. <br>
   *  Use this method when you would like to create a request that may be sent to many
   *  potential responders. If you are only sending a request to one location, use the @see #createRequest
   *  method which will create a default transaction group for you.
   *
   *  <br>REQUIRES: <ul><li>A valid request document conforming to the ORRequestDetails schema 
   *  which at least provides some basic bibliographic data for the requested item.
   *  </ul>
   *  PROVIDES: <ul><li>A new transaction group created in the database with the supplied details as defaults.
   *  <ul>
   *
   *  @author Ian Ibbotson
   *  @since 1.0
   *  @param patron_name Name of the person requesting the item or null.
   *  @param patron_status A String representing the status of the institution Borrower/Patron or null.
   *         This string will be returned to you in some subsequent messages. There is
   *         currently no shared enumeration of patron status. Use whatever local string
   *         you wish.
   *  @param patron_symbol Symbol (For example borrower barcode) of the end-user or patron who is
   *         requesting this item or null.
   *  @param request_item_details_doc An instance of org.w3c.dom.Document conforming to the 
   *         ORRequestDetails schema. 
   *         This document must have the root element "DocumentRequest" allowable child elements are
   *  <ul>
   *  <li>"CallNumber"
   *  <li>"Author"
   *  <li>"Title"
   *  <li>"SubTitle"
   *  <li>"Sponsor"
   *  <li>"Place"
   *  <li>"Publisher"
   *  <li>"Series"
   *  <li>"Volume"
   *  <li>"Edition"
   *  <li>"PublicationDate" in the format DDMMYYYY
   *  <li>"PublicationDateOfComponent" in the format DDMMYYYY
   *  <li>"AuthorOfArticle"
   *  <li>"TitleOfArticle"
   *  <li>"Paginiation"
   *  <li>"ISBN"
   *  <li>"ISSN"
   *  <li>"AdditionalNoLetters"
   *  <li>"VerificationReferenceSource"
   *  <li>"ROTA" : The list of locations which this request can be sent to. In Priority Order.
   *    <ul>
   *      <li>"LocationSymbol" : The symbol ("NamingAuthority:Symbol") to which this request should
   *          be sent.
   *    </ul>
   *  </ul>
   *  A simple example document might be :<br>
   *  &lt;DocumentRequest>&lt;Title>Brain of the firm&lt;Title>&lt;DocumentRequest>
   *  @return A string containing the Transaction Group Qualifier (tgq) of the new transaction group.
   *  @throws OpenRequestException if an exceptional condition arose
   *  removed                         String patron_name,
   *  removed                         String patron_status,
   *  removed                         String patron_symbol,
   */
  public Long createTransactionGroup(String requesting_symbol,
                                     Document request_details_doc,
                                     Hashtable additional_attrs) throws OpenRequestException;


  /**
   *  Creates a new ISO ILL Request message which is sufficiently well populated to be
   *  sent to a remote system party without modification. However, users will normally use this
   *  method to create a template request given certain bib and request defaults which can then be
   *  displayed and modified by some system user before being "approved" and sent.
   *
   *  <br>REQUIRES: <ul>
   *  <li>A Valid Requester Symbol <b>lookupAlias(requester_symbol) != null</b>
   *  <li>A Valid Responder Symbol <b>lookupAlias(responder_symbol) != null</b>
   *  <li>If Initial requester is supplied, A Valid Initial requester Symbol 
   *         <b>lookupAlias(initial_requester_symbol) != null</b>
   *  <li>If a tgq is supplied, <b> lookupTransactionGroup(initial_requester_symbol,tgq) != null</b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Request message which is sufficiently well populated to be
   *  sent without modification to the remote ILL responder system identified by responder_symbol.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param tgq The transaction group qualifier to use for this request. If none is specified then a
   *         new one will be created and assigned. If a tgq is specified, a Transaction Group with that
   *         tgq must have been created using the createTransactionGroup method.
   *  @param requester_symbol The requester symbol. A location can be known by many symbols (Although
   *         each location has a default symbol. This string must be of the form "Authority:Alias" for example
   *         "BL:DEVN". The symbol used must be a valid alias.
   *  @param responder_symbol The Responder Symbol. 
   *         This string must be of the form "Authority:Alias" for example
   *         "BL:DEVN". The symbol used must be a valid alias.
   *  @param initial_requester_symbol The initial requester symbol or null. Only needs to be supplied if
   *         we are acting as an intermediary and forwarding a request. If null is specified, the 
   *         requester_symbol will be used as the initial_requester_symbol.
   *  @param patron_symbol An arbritrary string representing the id of the end user requesting the item.
   *         For example, the value of a borrower barcode in your local circulation system.
   *  @param patron_status An arbritrary string representing the status of the end user requesting the item
   *  @param patron_name An arbritrary string representing the name of the end user requesting the item
   *  @param allow_forwarding Set to true if you wish to allow forwarding. If null is provided,
   *         the default Boolean.FALSE will be used.
   *  @param allow_chaining Set to true if you wish to allow chaining. If null is provided,
   *         the default Boolean.FALSE will be used.
   *  @param allow_partitioning Set to true if you wish to allow partitioning. If null is provided,
   *         the default Boolean.FALSE will be used.
   *  @param allow_change_send_to_list Set to true if you wish to allow the responder to change the
   *         send-to list used provided with this request. If null is provided, 
   *         the default Boolean.FALSE will be used.
   *  @param transaction_type The type of transaction, 1=Simple, 2=Chained or 3=Partitioned. Always set
   *         to 1 when creating a new request.
   *  @param request_doc An XML document as described in the createTransactionGroup method which contains
   *         bibloigraphic details of the item to be requested and information about possible locations
   *         holding that item.
   *  @throws OpenRequestException if there was a communications problem.
   *  @see #createTransactionGroup
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createRequest (String tgq,
                                           String requester_symbol,
                                           String responder_symbol,
                                           String initial_requester_symbol,
                                           String patron_symbol,
                                           String patron_status,
                                           String patron_name,
                                           Boolean allow_forwarding,
                                           Boolean allow_chaining,
                                           Boolean allow_partitioning,
                                           Boolean allow_change_send_to_list,
                                           long transaction_type,
                                           Document request_doc) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Forward Inidication message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  <li>The new responder symbol must be known to the system. 
   *       <b> lookupAlias(new_responder_symbol) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Forward Indication message which is sufficiently well populated to be
   *  sent without modification to the remote ILL system identified by original_requester_symbol.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param new_responder_symbol The symbol (In the form "AUTHORITY:ALIAS") we are forwarding to
   *  @param note A text note to accompany the message or null.
   *  @return an ILL Forward Indication APDU ready to be sent to the remote system.
   *  @see #lookupAlias(String)
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createForwardIndication(Long internal_transaction_id,
                                               String new_responder_symbol,
                                               String note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Shipped message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Shipped message which is sufficiently well populated to be
   *  sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param shipped_service_type One of { 1=Loan, 2=Copy-Non-Returnable }
   *  @param date_shipped The date on which the item was shipped
   *  @param date_due The date by which the returned item should be returned to the responder,
   *         or null if not returnable
   *  @param renewable Is the item renewable ( or null if not returnable )
   *  @param chargeable_units The number of units supplied for which there is a charge
   *  @param cost The amount asked, taken or billed by the responder
   *  @param total_cost_currency The currency of the amount asked, taken or billed by the responder
   *  @param total_cost_value The amount asked, taken or billed by the responder
   *  @param conditions one of { 22=Library use only, 23=No reproduction, 24=Client signature required
   *         25=special collections supervision required, 27=other }
   *  @return an ILL Shipped APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createShipped (Long internal_transaction_id,
                                      long shipped_service_type,
                                      Date date_shipped,
                                      Date date_due,
                                      Boolean renewable,
                                      Long chargeable_units,
                                      String total_cost_currency,
                                      String total_cost_value,
                                      Long conditions,
                                      String note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Answer : Conditional message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Answer : Conditional message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param condition one of { 13=Cost exceeds limit, 14=charges, 15=prepayment required,
   *         16=lacks copyright compliance, 22=library use only, 23=no reproduction,
   *         24=client signature required, 25=special collections supervision required,
   *         27=other, 28=responder specific, 30=proposed delivery service
   *  @param date_for_reply The date by which a conditional reply is needed
   *  @param responder_note A note to attach to the message
   *  @return an ILL Answer APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createConditionalAnswer(Long internal_transaction_id,
                                               int condition,
                                               Date date_for_reply,
                                               String responder_note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Answer : Retry message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  <li>If not null, the location_symbols parameter must be a vector of strings which are
   *      system aliases known to the system. For each Object o in the vector,
   *      <b> ( o instanceof String ) AND ( lookupAlias((String)o) != null ) </b>
   *  <li>A valid reason not available as defined below.
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Answer : Retry message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param reason_not_available one of { 1=In use on loan, 2=In Process, 6=On order, 
   *         7=Volume, Issue not yet available, 8=At Bindery, 13=Cost exceeds limit, 14=Charges
   *         15=Prepayment required, 16=Lacks copyright compliance, 17=Not found as cited,
   *         19=On hold, 27=Other, 28=Responder specific }
   *  @param retry_date The date after which a request may be retried
   *  @param location_symbols A ArrayList of String aliases for other locations to try
   *  @return an ILL Answer APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createRetryAnswer(Long internal_request_id,
                                         int reason_not_available,
                                         Date retry_date,
                                         ArrayList location_symbols) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Answer : Unfilled message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  <li>If not null, the location_symbols parameter must be a vector of strings which are
   *      system aliases known to the system. For each Object o in the vector,
   *      <b> ( o instanceof String ) AND ( lookupAlias((String)o) != null ) </b>
   *  <li>A valid reason not available as defined below.
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Answer : Unfilled
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param reason_unfilled one of { 1=In use on loan, 2=In Process, 3=lost, 4=Non circulating,
   *         5=Not owned, 6=On order, 7=Volume, Issue not yet available, 8=At Bindery, 
   *         9=Lacking, 10=Not on shelf, 11=On-Reserve, 12=Poor condtion, 13=Cost exceeds limit, 14=Charges
   *         15=Prepayment required, 16=Lacks copyright compliance, 17=Not found as cited,
   *         18=locations not found, 19=On hold, 20=policy problem, 21=mandatory messageing not supported,
   *         22=Expiry not supported, 23=Requested delivery service not supported, 
   *         24=preferred delivery time not possible, 27=Other, 28=Responder specific }
   *  @param location_symbols A ArrayList of String aliases for other locations to try
   *  @return an ILL Answer APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createUnfilledAnswer(Long internal_request_id,
                                            int reason_unfilled,
                                            ArrayList location_symbols) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Answer : Locations Provided message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  <li>The location_symbols parameter must be a vector of strings which are
   *      ill system aliases known in the OpenRequest database. For each Object o in the vector,
   *      <b> ( o instanceof String ) AND ( lookupAlias((String)o) != null ) </b>
   *  <li>A valid reason locations provided as defined below.
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Answer : Locations provided message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param reason_locations_provided one of { 1=In use on loan, 2=In Process, 3=lost, 4=Non circulating,
   *         5=Not owned, 6=On order, 7=Volume, Issue not yet available, 8=At Bindery, 
   *         9=Lacking, 10=Not on shelf, 11=On-Reserve, 12=Poor condtion, 13=Cost exceeds limit,
   *         19=On hold, 27=Other, 28=Responder specific }
   *  @param location_symbols A ArrayList of String aliases for other locations to try
   *  @return an ILL Answer APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   *
   */
  public ILLMessageEnvelope createLocationsProvidedAnswer(Long internal_request_id,
                                            int reason_locations_provided,
                                            ArrayList location_symbols) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Answer : Will Supply message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  <li>The location_symbols parameter must be a vector of strings which are
   *      ill system aliases known in the OpenRequest database. For each Object o in the vector,
   *      <b> ( o instanceof String ) AND ( lookupAlias((String)o) != null ) </b>
   *  <li>A valid reason will supply provided as defined below.
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Answer : Will Supply message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param reason_will_supply one of { 1=In use on loan, 2=In Process,
   *         6=On order, 8=At Bindery, 
   *         19=On hold, 26=Being processed for supply, 27=Other, 28=Responder specific,
   *         30=electronic delivery }
   *  @param supply_date
   *  @param return_to_address Address to which the item should be returned.
   *  @param locations A ArrayList of String aliases for other locations to try
   *  @return an ILL Answer APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see #lookupAlias(String)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createWillSupplyAnswer(Long internal_request_id,
                                              int reason_will_supply,
                                              Date supply_date,
                                              Postal_Address_type return_to_address,
                                              ArrayList locations) throws OpenRequestException;
  
  /**
   *  Creates a new ISO ILL Answer : Hold Placed message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  <li>The location_symbols parameter must be a vector of strings which are
   *      ill system aliases known in the OpenRequest database. For each Object o in the vector,
   *      <b> ( o instanceof String ) AND ( lookupAlias((String)o) != null ) </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Answer : Will Supply message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param estimated_date_available Date the item is expected to become available
   *  @param hold_placed_medium_type One of { 1=Printed, 2=Microform, 4=Film or Video Recording,
   *         5=audio recording, 6=machine readable, 7=other }
   *  @param locations A ArrayList of String aliases for other locations to try
   *  @return an ILL Answer APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see #lookupAlias(String)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createHoldPlacedAnswer(Long internal_transaction_id,
                                              Date estimated_date_available,
                                              int hold_placed_medium_type,
                                              ArrayList locations)  throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Answer : Estimate message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  <li>The locations parameter must be a vector of strings which are
   *      ill system aliases known in the OpenRequest database. For each Object o in the vector,
   *      <b> ( o instanceof String ) AND ( lookupAlias((String)o) != null ) </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Answer : Estimate message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param estimate
   *  @param locations A ArrayList of String aliases for other locations to try
   *  @return an ILL Answer APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see #lookupAlias(String)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createEstimateAnswer(Long internal_transaction_id,
                                            String estimate,
                                            ArrayList locations) throws OpenRequestException;

  /**
   *   Creates a new ISO ILL Conditional Reply message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Conditional Reply message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param answer Boolean.TRUE or Boolean.FALSE
   *  @param note A note sent with the message
   *  @return an ILL Conditional Reply APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createConditionalReply(Long internal_transaction_id,
                                              Boolean answer,
                                              String note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Cancel message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Cancel message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param note A note sent with the message
   *  @return an ILL Conditional Reply APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createCancel(Long internal_transaction_id,
                                    String requester_note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Cancel Reply message which is sufficiently 
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Cancel Reply message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param answer Boolean.TRUE or Boolean.FALSE
   *  @param note A note sent with the message
   *  @return an ILL Cancel Reply APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createCancelReply(Long internal_transaction_id,
                                         Boolean answer,
                                         String note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Received message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Received message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @return an ILL Cancel Reply APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createReceived (Long internal_transaction_id) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Recall message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Recall message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param note
   *  @return an ILL Recall APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createRecall(Long internal_request_id,
                                    String requester_note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Returned message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Returned message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param date_returned
   *  @param returned_via
   *  @param requester_note
   *  @return an ILL Returned APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createReturned(Long internal_transaction_id,
                                      Date date_returned,
                                      String returned_via,
                                      String requester_note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL CheckedIn message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL CheckedIn message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param date_checked_in Date the item was checked in
   *  @param responder_note
   *  @return an ILL CheckedIn APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createCheckedIn(Long internal_transaction_id,
                                       Date date_checked_in,
                                       String responder_note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Overdue message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Overdue message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param date_due
   *  @param renewable
   *  @param responder_note
   *  @return an ILL Overdue APDU ready to be sent to the remote system.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see #lookupTransaction(long)
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   *
   */
  public ILLMessageEnvelope createOverdue(Long internal_transaction_id,
                                     Date date_due,
                                     boolean renewable,
                                     String responder_note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Renew message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Renew message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param desired_date_due
   *  @param requester_note
   *  @return an ILL Renew APDU ready to be sent to the remote system.
   *  @see #lookupTransaction(long)
   *  @throws OpenRequestException
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   *
   */
  public ILLMessageEnvelope createRenew(Long internal_transaction_id,
                                   Date desired_due_date,
                                   String requester_note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Renew Answer message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Renew Answer message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param answer Boolean.TRUE if the renew was OK, Boolean.FALSE Otherwise.
   *  @param date_due If answer==Boolean.TRUE the date the item is expected to be returned
   *  @param renewable If answer==Boolean.TRUE an indication if the item will be renewable again
   *  @param responder_note A note
   *  @return an ILL Renew Answer APDU ready to be sent to the remote system.
   *  @see #lookupTransaction(long)
   *  @throws OpenRequestException
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   *
   */
  public ILLMessageEnvelope createRenewAnswer(Long internal_transaction_id,
                                         boolean answer,
                                         Date date_due,
                                         boolean renewable,
                                         String responder_note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Lost message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Lost message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param note A note
   *  @return an ILL Lost APDU ready to be sent to the remote system.
   *  @see #lookupTransaction(long)
   *  @throws OpenRequestException
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   *
   */
  public ILLMessageEnvelope createLost(Long internal_transaction_id,
                                  String note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Damaged message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Damaged message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param note A note
   *  @return an ILL Damaged APDU ready to be sent to the remote system.
   *  @see #lookupTransaction(long)
   *  @throws OpenRequestException
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   *
   */
  public ILLMessageEnvelope createDamaged(Long internal_transaction_id,
                                     String note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Message message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Message message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param note A note
   *  @return an ILL Message APDU ready to be sent to the remote system.
   *  @see #lookupTransaction(long)
   *  @throws OpenRequestException
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   *
   */
  public ILLMessageEnvelope createMessage(Long internal_transaction_id,
                                     String note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Status Query message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Status Query message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @param note A note
   *  @return an ILL Status Query APDU ready to be sent to the remote system.
   *  @see #lookupTransaction(long)
   *  @throws OpenRequestException
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   *
   */
  public ILLMessageEnvelope createStatusQuery(Long internal_transaction_id,
                                         String note) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL StatusOrErrorReport message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL StatusOrErrorReport message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @return an ILL StatusOrErrorReport APDU ready to be sent to the remote system.
   *  @see #lookupTransaction(long)
   *  @throws OpenRequestException
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   *
   */
  public ILLMessageEnvelope createStatusOrErrorReport(Long internal_request_id) throws OpenRequestException;

  /**
   *  Creates a new ISO ILL Expired message which is sufficiently
   *  well populated to be sent to a remote system without modification.
   *
   *  <br>REQUIRES: <ul>
   *  <li>The internal request id of a transaction being managed at this location.
   *       <b> lookupTransaction(internal_transaction_id) != null </b>
   *  </ul>
   *
   *  PROVIDES: <ul>
   *  <li> A new ISO ILL Expired message
   *       which is sufficiently well populated to be sent without modification to a remote ILL system.
   *  </ul>
   *  @author Ian Ibbotson
   *  @param internal_transaction_id The id transaction for which we wish to create this message
   *  @return an ILL Expired APDU ready to be sent to the remote system.
   *  @see #lookupTransaction(long)
   *  @throws OpenRequestException
   *  @see com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type
   */
  public ILLMessageEnvelope createExpired(Long internal_request_id) throws OpenRequestException;

  /* Data handling and lookup functions */

  /**
   *  Lookup a TransactionGroup using the specified transaction group id<br>
   *  <br>REQUIRES:
   *  <br>
   *  PROVIDES:
   *  An instance of the ILLTransactionGroup interface which can be used to discover information
   *  about a specific transaction group.
   *  <br>
   *  
   *  @author Ian Ibbotson
   *  @since 1.0
   *  @return the ILLTransactionGroup for the supplied id or null.
   *  @param tgq_id The internal ID of the required transaction group.
   *  @see com.k_int.openrequest.api.ILLTransactionGroupInfo
   */
  public ILLTransactionGroupInfo lookupTransactionGroup(Long tgq_id) throws OpenRequestException;

  /**
   *  Lookup a TransactionGroup using the supplied requester, tgq and our location.<br>
   *  A TGQ Is unique only witin the context of a specific requester symbol at a specific location.
   *  Beware that in a hub system there may be many Transaction Group records with the same
   *  requester and tgq, one for the initial requester and one for each intermediary and 
   *  one for the responder.
   *  <br>REQUIRES:
   *  <br>
   *  PROVIDES:
   *  An instance of the ILLTransactionGroup interface for which the initial requester = the supplied
   *  symbol, the tgq = the supplied tgq and the location is the location being managed by this interface.
   *  <br>
   *  
   *  @author Ian Ibbotson
   *  @since 1.0
   *  @param requester_id The ILL requester id ("Authority:Symbol" form)
   *  @param tgq The Transaction Group Qualifier we are looking for.
   *  @return the ILLTransactionGroup for the supplied requester and tgq or null if not found.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see com.k_int.openrequest.api.LocationSymbolInfo
   */
  public ILLTransactionGroupInfo lookupTransactionGroup(String requester_id, String tgq) throws OpenRequestException;

  /**
   *  Search the database for a LocationSymbol object matching the supplied String
   *  and return an instance of LocationSymbol.<br>
   *  
   *  <br>REQUIRES:<ul>
   *  <li>A string of the form "AUTHORITY:ALIAS" for example "BL:DEVON"
   *  </ul>
   *
   *  PROVIDES:<ul>
   *  <li>An instance of LocationSymbol for which the authority field matches the text before the
   *      colon in the alias parameter and the symbol matches the text after the colon in the alias.
   *  <li>Null if no such location symbol can be found.
   *  </ul>
   *  
   *  @author Ian Ibbotson
   *  @since 1.0
   *  @param alias A String in the form "AUTHORITY:ALIAS"
   *  @return an instance of LocationSymbol for the provided alias or null if no matching symbol is found.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see com.k_int.openrequest.api.LocationSymbolInfo
   */
  public LocationSymbolInfo lookupAlias(String alias) throws OpenRequestException;

  /**
   *  Search the database for an ILLTransaction object with the provided id.
   *
   *  <br>REQUIRES:<br>
   *
   *  PROVIDES:<ul>
   *  <li>An instance of ILLTransaction for which the id field matches the supplied id
   *  <li>Null if no such ILLTransaction symbol can be found for this location.
   *  </ul>
   *
   *  @author Ian Ibbotson
   *  @since 1.0
   *  @param internal_transaction_id the internal transaction id we are looking for
   *  @return an instance of ILLTransaction for the provided id or null if no matching transaction is found.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   *  @see com.k_int.openrequest.api.ILLTransactionInfo
   */
  public ILLTransactionInfo lookupTransaction(Long internal_transaction_id) throws OpenRequestException;

  /**
   *  Try to resolve the supplied ILL Transaction ID into an internal_transaction_id.
   *
   *  <br>REQUIRES:<br>
   *
   *  PROVIDES:<ul>
   *  <li>The unique internal key for a transacton or -1 if not found.
   *  </ul>
   *
   *  @author Ian Ibbotson
   *  @since 1.0
   *  @param initial_requester The initial requester symbol
   *  @param tgq The transaction group qualifier from a request message
   *  @param tq The transaction qualifier from a request message
   *  @param stq The sub transaction qualifier from a request message
   *  @return The integer internal transaction ID or -1 if not found.
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   */
  public Long resolveTransactionID(String initial_requester,
                                   String tgq,
                                   String tq,
                                   String stq) throws OpenRequestException;

  /**
   *  Provide a the identified "summary" of the transactions held at this location.
   *  The requests at a given location may be summarised in many different ways, such as
   *  All the requests at this location where the role is "Responder", all the requests at
   *  this location where the role is "Responder" and the transaction group status is "Unfilled"
   *  etc. These summaries are useful when grouped into reports which deliver a specific picture
   *  of the situation at a location, for example the "Requester" view and the "Responder" view.A
   *  Use this method to provide a summary of all the requests at this location. The details of
   *  the summary will be determined by the name of the summary report
   *  <br>Requires:
   *  <ul>
   *  <li>A valid report name. Initially "RESPONDER" and "REQUESTER"</li>
   *  </ul>
   *  Provides:
   *  <ul>
   *  <li>An array of TransactionGroupSummary objects which provide the name of a 
   *  specific statistic (For example, ConditionalQuery), a count of the number of
   *  transactions matching that condition, and a method to obtain all the transactions 
   *  matching that criteria</li>
   *  </ul>
   *  @author Ian Ibbotson
   *  @since 1.0
   *  @param report_name A name identifiying the set of statistics making up this report
   *  @return An array containing details of the numbers of TransactionGroups matching each statistic
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   */
  public TransactionGroupSummary[] getSummary(String report_name) throws OpenRequestException;

  /**
   *  Return a set of transaction groups matching the named criteria.
   *  Return a TransactionGruopSet which allows access to all the TransactionGroupInfo
   *  objects matching the named criteria.
   *  
   *  <br>Requires:
   *  <ul>
   *  <li>A valid report name. Initially "RESPONDER" and "REQUESTER"</li>
   *  </ul>
   *  Provides:
   *  <ul>
   *  <li>A TransactionGroupSet that allows access to each of the TransactionGroupInfo objects
   *  meeting the named criteria.
   *  </li>
   *  </ul>
   *  @author Ian Ibbotson
   *  @since 1.0
   *  @param role "RESP" for Responder or "REQ" for Requester.
   *  @param named_criteria The named criteria, e.g. "ALL_RESPONDER_TRANSACTION_GROUPS"
   *  @return An array containing details of the numbers of TransactionGroups matching each statistic
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   */
  public TransactionGroupSet findTransactionGroups(String named_criteria) throws OpenRequestException;

  /**
   *  Return a set of transaction groups matching the named criteria.
   *  Return a TransactionGruopSet which allows access to all the TransactionGroupInfo
   *  objects matching the named criteria.
   *
   *  <br>Requires:
   *  <ul>
   *  <li>A valid report name. Initially "RESPONDER" and "REQUESTER"</li>
   *  </ul>
   *  Provides:
   *  <ul>
   *  <li>A TransactionGroupSet that allows access to each of the TransactionGroupInfo objects
   *  meeting the named criteria. </li>
   *  </ul>
   *  @author Ian Ibbotson
   *  @since 1.0
   *  @param role "RESP" for Responder or "REQ" for Requester.
   *  @param only_with_unread_messages Boolean.TRUE if you only want transaction groups for which
   *         there are unread messages.
   *  @param required_live_transaction_status_code Set to the status code you require for the
   *         currently live transaction. See the status_code table in the database. null if any
   *         status code is required.
   *  @param required_transaction_group_status Set to the required status of the transaction group
   *         rather than the live transaction.
   *  @return An array containing details of the numbers of TransactionGroups matching each statistic
   *  @throws OpenRequestException if there was a problem talking with the remote object.
   */
  public TransactionGroupSet findTransactionGroups(String role,
                                                   Boolean only_with_unread_messages,
                                                   String required_live_transaction_status_code,
                                                   String required_transaction_group_status
                                                   ) throws OpenRequestException;

  public Long getInternalLocId();
}
