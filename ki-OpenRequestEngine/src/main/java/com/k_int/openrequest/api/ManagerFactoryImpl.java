/**
 * Title:       ManagerFactory
 * @version:    $Id: ManagerFactoryImpl.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.api;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.w3c.dom.Document;
import java.util.*;
import org.jzkit.a2j.codec.util.*;
import org.hibernate.*;
import com.k_int.openrequest.services.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.*;

/**
 * Title:       ManagerFactory
 * @version:    $Id: ManagerFactoryImpl.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: Create an instance of an object implementing LocationBasedManagementInterface
 */
public class ManagerFactoryImpl implements ManagerFactory, ApplicationContextAware {

  private Log log = LogFactory.getLog(this.getClass());
  private ApplicationContext ctx = null;

  public ManagerFactoryImpl() {
    super();
  }

  /**
   *  Initailise the management interface by identifiying which location we are currently
   *  managing requests for.
   */
  public LocationBasedManagementInterface create(String location_symbol, Object credentials) throws OpenRequestException {
    return new LocationBasedManagementInterfaceImpl(ctx, location_symbol, credentials);
  }

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }

  public void init() {
  }

}
