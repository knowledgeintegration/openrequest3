/**
 * Title:       
 * @version:    $Id: OpenRequestSession.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.api;

import java.util.logging.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.helpers.*;
import java.util.*;
import org.hibernate.*;
import org.hibernate.type.*;
import java.rmi.*;
import java.rmi.server.*;

public class OpenRequestSession
{
  private static Logger log = Logger.getLogger(OpenRequestSession.class.getName());

  private Session session = null;
  private org.springframework.beans.factory.BeanFactory app_context = null;
  private long institution_id;

  private OpenRequestSession() {
  }

  public OpenRequestSession(long institution_id, org.springframework.beans.factory.BeanFactory app_context)
  {
    this.institution_id = institution_id;
    this.app_context = app_context;
    try
    {
      SessionFactory factory = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      session = factory.openSession();
    }
    catch ( org.hibernate.HibernateException he )
    {
      log.log(Level.SEVERE,"Problem finding connection factory ",he);
    }
  }

  public void close()
  {
    try
    {
      session.flush();
      session.connection().commit();
      session.close();
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }
  }

  public Session getDBSession()
  {
    return session;
  }
}
