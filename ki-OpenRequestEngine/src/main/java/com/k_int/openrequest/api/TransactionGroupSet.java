package com.k_int.openrequest.api;

import java.util.*;
import java.rmi.*;

/**
 * Title:       
 * @version:    $Id: TransactionGroupSet.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public interface TransactionGroupSet extends Remote
{
  public long size() throws RemoteException;
  public ILLTransactionGroupInfo getTransactionGroupInfo(long index) throws RemoteException;
}
