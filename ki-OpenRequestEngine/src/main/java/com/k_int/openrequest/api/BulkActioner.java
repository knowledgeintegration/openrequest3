/**
 * Title:       
 * @version:    $Id: BulkActioner.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.api;

import com.k_int.openrequest.db.*;
import java.util.Set;
import java.util.HashSet;

public class BulkActioner
{
  public class RequestActionInfo
  {
    String description;
    int action_code;
    String action_message_class_name;
  };

  /**
   *
   */
  public RequestActionInfo[] getValidActions(long[] request_ids)
  {
    return null;
  }

  public boolean performBulkAnswerUnfilled(long[] request_ids,
                                   String a_bulk_answer_unfilled_param)
  {
    return true;
  }
}
