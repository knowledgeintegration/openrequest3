package com.k_int.openrequest.api.Internal;

import java.util.*;
import java.rmi.*;
import java.rmi.server.*;
import com.k_int.openrequest.api.*;

/**
 * 
 * @version:    $Id: TransactionGroupSetImpl.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class TransactionGroupSetImpl extends UnicastRemoteObject implements TransactionGroupSet
{
  public TransactionGroupSetImpl() throws RemoteException
  {
  }

  public long size() throws RemoteException
  {
    return 0;
  }

  public ILLTransactionGroupInfo getTransactionGroupInfo(long index) throws RemoteException
  {
    return null;
  }
}
