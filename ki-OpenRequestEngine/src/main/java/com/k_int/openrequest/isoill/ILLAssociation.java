/**
 * Title:       ISO ILL Assoc
 * @version:    $Id: ILLAssociation.java,v 1.9 2005/06/22 21:57:50 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson (ian.ibbotson@k-int.com)
 * Company:     KI
 * Description: 
 */

/*
 * $Log: ILLAssociation.java,v $
 * Revision 1.9  2005/06/22 21:57:50  ibbo
 * Updated enums for item type, new a2j jar
 *
 * Revision 1.8  2005/06/11 18:04:11  ibbo
 * PDU Logging extension
 *
 * Revision 1.7  2005/06/11 16:27:04  ibbo
 * Added test location import class
 *
 * Revision 1.6  2005/05/12 15:15:41  ibbo
 * Updated
 *
 * Revision 1.5  2005/03/22 16:49:57  ibbo
 * Updated
 *
 * Revision 1.4  2005/03/22 13:20:40  ibbo
 * retry flag now defaults to false.
 *
 * Revision 1.3  2005/02/24 19:16:49  ibbo
 * updated
 *
 * Revision 1.2  2005/02/22 14:49:01  ibbo
 * Added code to allow an association context to pass up from protocol adapters all the
 * way to custom handlers. This lets (for example) custom handlers deal with authentication
 * extensions (A-la OCLC) and set a login variable that subsequent request objects can
 * refer to. The context lasts the duration of the ILL Association in the case of TCP, or
 * a single mime message (Possibly with multiple parts) from mail.
 *
 * Also added missing OID and codec lines from oid register. Added jzkit_z3950_plugin jar
 * for authentication external.
 *
 * Revision 1.1.1.1  2005/02/14 11:46:29  ibbo
 * Import
 *
 * Revision 1.2  2004/07/26 15:37:34  ibbo
 * Updated
 *
 * Revision 1.1.1.1  2004/01/15 15:04:18  ibbo
 * Import
 *
 * Revision 1.2  2003/04/24 16:12:30  ibbo
 * Major changes to add a controller object that will be used to start
 * each system component instead of using the command line. src/java/com.k_int.openrequest/api/OpenRequestServer
 * now controlls these components and the configuration is in the database instead of system files.
 *
 * Revision 1.1.1.1  2003/03/17 11:53:48  ibbo
 * Initial import
 *
 * Revision 1.2  2002/12/23 20:50:38  ianibbo
 * Updates to schema and classes
 *
 * Revision 1.1.1.1  2002/10/20 10:24:02  ianibbo
 * Initial upload
 *
 * Revision 1.1.1.1  2002/10/18 07:13:01  ibbo
 * beta 1
 *
 * Revision 1.1  2002/08/04 11:56:32  ianibbo
 * Added in correct place :-)
 *
 * Revision 1.3  2002/08/01 21:08:35  ianibbo
 * Phase one working
 *
 * Revision 1.2  2002/08/01 14:41:39  ianibbo
 * Updated
 *
 * Revision 1.1  2002/08/01 09:54:48  ianibbo
 * Added bits
 *
 */

package com.k_int.openrequest.isoill;

import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import org.jzkit.a2j.codec.runtime.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;

import java.net.*;
import java.io.*;
import java.util.*;
import java.math.BigInteger;

// For XML components
import  org.w3c.dom.Document;
import  org.apache.xml.serialize.OutputFormat;
import  org.apache.xml.serialize.Serializer;
import  org.apache.xml.serialize.SerializerFactory;
import  org.apache.xml.serialize.XMLSerializer;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class ILLAssociation extends Thread
{
  private Log log = LogFactory.getLog(ILLAssociation.class);

  private PDUNotificationTarget handler = null;
  private int dbg_counter=0;

  private Socket socket = null;
  private InputStream incoming_data = null;
  private OutputStream outgoing_data = null;
  private boolean running = false;

  public static final String US_ASCII_ENCODING = "US-ASCII";
  public static final String UTF_8_ENCODING = "UTF-8";
  public static final String UTF_16_ENCODING = "UTF-16";

  private ILL_APDU_codec codec = ILL_APDU_codec.getCodec();

  private String charset_encoding = US_ASCII_ENCODING;

  private String remote_ip = null;
  private int portno = -1;

  private OIDRegister reg = null;
  private PDULogger logger = null;

  public ILLAssociation(Socket s, 
                        PDUNotificationTarget handler, 
                        String remote_ip,
                        int port,
                        OIDRegister reg,
                        PDULogger logger)
  {
    socket = s;
    this.remote_ip = remote_ip;
    this.portno = port;
    this.handler = handler;
    this.reg = reg;
    this.logger = logger;

    try
    {
      incoming_data = s.getInputStream();
      outgoing_data = s.getOutputStream();
    }
    catch( java.io.IOException e )
    {
      log.error("Error constructing Protocol Association",e);
    }
  }

  public void run() {
    running = true;
    Map ill_association_context = new HashMap();


    ChunkingBERInputStream chunker = new ChunkingBERInputStream(incoming_data);


    while(running) {

      byte[] complete_apdu = null;
      int pdu_type=0;

      try {
        log.debug("Waiting for pdu on input stream");
        complete_apdu = chunker.getNextCompleteAPDU();

        if ( logger != null ) {
          logger.logAPDU("IN","-",complete_apdu);
        }

        BERInputStream bds = new BERInputStream(new ByteArrayInputStream(complete_apdu),charset_encoding,reg);
        ILL_APDU_type pdu = null;
        pdu = (ILL_APDU_type)codec.serialize(bds, pdu, false, "PDU");

        if ( pdu != null ) {
          pdu_type = pdu.which;
          String telecom_service_address = remote_ip+":"+portno;

          log.info("Incoming TCP APDU from "+telecom_service_address+" Sending notifications");

          handler.notifyILLPDU(pdu,
                               "TCP",
                               telecom_service_address,
                               System.currentTimeMillis(),
                               ill_association_context);
          yield();
        }
        else {
          log.warn("Socket returned a null APDU");
        }
      }
      catch ( java.io.InterruptedIOException iioe )
      {
        // Socket timeout, clean up and stop this thread. N.B. this exception
        // will only be caused if you have used setSoTimeout on the socket
        log.error("Processing java.io.InterruptedIOException, shut down association",iioe);

        running = false;
      }
      catch ( java.net.SocketException se)
      {
        if ( se.getMessage().equals("Socket closed") )
          log.debug("Connection Closed");
        else
          log.error("Processing java.io.IOException, shut down association", se);
        running = false;
      }
      catch ( java.io.IOException ioe )
      {
        // Client snapped connection somehow...
        if(ioe.getMessage().equals("Connection Closed"))
          log.debug("Connection Closed");
        else
          log.debug("Processing java.io.IOException, shut down association", ioe);
        running = false;
      }
      catch ( Exception e )
      {
        log.debug("Processing exception : ",e);
        running=false;
      }
      finally {
      }
    }

    try
    {
      incoming_data.close();
      outgoing_data.close();
      socket.close();
    }
    catch ( Exception e )
    {
      log.warn("Exception closing open sockets", e);
    }

    incoming_data = null;
    outgoing_data = null;
    socket = null;
  }

  public boolean encodeAndSendPDU(ILL_APDU_type the_pdu) throws java.io.IOException {

    log.debug("encodeAndSend...");
    BEROutputStream encoder = new BEROutputStream(charset_encoding,reg);
    encoder.setDefaultLengthOfLengthEncoding(3);

    codec.serialize(encoder, the_pdu, false, "ISOILL_APDU");
    encoder.flush();

    // Grab a copy of the outgoing APDU for logging purposes.
    byte[] outgoing_pdu = encoder.toByteArray();

    if ( logger != null ) {
      logger.logAPDU("OUT",""+the_pdu.which,outgoing_pdu);
    }

    encoder.writeTo(outgoing_data);
    outgoing_data.flush();
    yield();
    return true;
  }
}
