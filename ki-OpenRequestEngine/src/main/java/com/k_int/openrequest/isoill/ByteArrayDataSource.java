package com.k_int.openrequest.isoill;

import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.awt.datatransfer.*;
import java.io.*;

public class ByteArrayDataSource implements DataSource
{
  private byte[] contents = null;
  private String type = null;
  private ByteArrayOutputStream baos = null;

  public ByteArrayDataSource(byte[] contents, String type) 
  {
    this.contents = contents;
    this.type = type;
  }

  public ByteArrayDataSource()
  {
    baos = new ByteArrayOutputStream();
  }
  
  public String getContentType()
  {
    return type;
  }

  public byte[] getData() 
  {
    if ( contents != null )
      return contents;
    else if ( baos != null )
      return baos.toByteArray();
    else
      throw new RuntimeException("Unable to get data");
  }

  public InputStream getInputStream() 
  {
    return new ByteArrayInputStream(contents);
  }

  public java.lang.String getName() 
  {
    return "ByteArrayDataSource";
  }

  public OutputStream getOutputStream()
  {
    return baos;
  }
}
