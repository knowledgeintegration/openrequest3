package com.k_int.openrequest.isoill;

import java.rmi.*;
import java.rmi.server.*;
import java.net.*;
import java.io.*;
import java.util.*;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import org.jzkit.a2j.codec.runtime.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.k_int.openrequest.services.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class MimeProtocolAdapter extends AbstractILLProtocolAdapter
{
  private Log log = LogFactory.getLog(this.getClass());
  private ILL_APDU_codec codec = ILL_APDU_codec.getCodec();
  private String charset_encoding = "US-ASCII";
  private String from_address = null;
  private String outgoing_mail_server = null;
  private String smtp_user = null;
  private String smtp_pass = null;
  private String mail_debug = null;
  private String pop_user = null;
  private String pop_pass = null;
  private String pop_server = null;
  private String pop_folder = null;
  private int sleep_time;
  private OIDRegister reg = null;

  private org.springframework.beans.factory.BeanFactory app_context;

  public MimeProtocolAdapter() {
    super();
  }

  public int getSleepTime() {
    return sleep_time;
  }

  public void setSleepTime(int sleep_time) {
    this.sleep_time = sleep_time;
  }

  public String getFromAddress() {
    return from_address;
  }

  public void setFromAddress(String from_address) {
    this.from_address = from_address;
  }

  public String getOutgoingMailServer() {
    return outgoing_mail_server;
  }

  public void setOutgoingMailServer(String outgoing_mail_server) {
    this.outgoing_mail_server = outgoing_mail_server;
  }

  public String getOutgoingUsername() {
    return smtp_user;
  }

  public void setOutgoingUsername(String smtp_user) {
    this.smtp_user = smtp_user;
  }

  public String getOutgoingPassword() {
    return smtp_pass;
  }

  public void setOutgoingPassword(String smtp_pass) {
    this.smtp_pass = smtp_pass;
  }

  public String getMailDebug() {
    return mail_debug;
  }

  public void setMailDebug(String mail_debug) {
    this.mail_debug = mail_debug;
  }

  public String getPopUser() {
    return pop_user;
  }

  public void setPopUser(String pop_user) {
    this.pop_user = pop_user;
  }

  public String getPopPassword() {
    return pop_pass;
  }

  public void setPopPassword(String pop_pass) {
    this.pop_pass = pop_pass;
  }

  public String getPopServer() {
    return pop_server;
  }

  public void setPopServer(String pop_server) {
    this.pop_server = pop_server;
  }

  public String getPopFolder() {
    return pop_folder;
  }

  public void setPopFolder(String pop_folder) {
    this.pop_folder = pop_folder;
  }

  public boolean sendProtocolMessage(InternalIso10161Message msg) {
    try {
      String service_address = msg.getPartnerTelecomServiceAddress();
      // service_address should contain send-to address
      log.debug("Email to "+service_address+", from="+from_address);

      // create some properties and get the default Session
      Properties props = new Properties(System.getProperties());
      props.put("mail.smtp.host", outgoing_mail_server);

      if ( ( smtp_user != null ) && ( smtp_user.length() > 0 ) ) 
        props.put("mail.smtp.auth","true");

      if ( ( mail_debug != null ) && ( mail_debug.length() > 0 ) )
        props.put("mail.debug", mail_debug);

      Session session = Session.getInstance(props, null);

      if ( ( mail_debug != null ) && ( mail_debug.equals("true") ) ) {
        session.setDebug(true);
        log.debug("Mailer props :"+props);
      }

      // create a message
      MimeMessage mime_msg = new MimeMessage(session);
      mime_msg.setFrom(new InternetAddress(from_address));
      InternetAddress[] address = {new InternetAddress(service_address)};
      mime_msg.setRecipients(Message.RecipientType.TO, address);
      mime_msg.setSubject("ISO ILL APDU from "+from_address);

      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      BEROutputStream encoder = new BEROutputStream(charset_encoding,reg);
      codec.serialize(encoder, msg.getPDU(), false, "ISOILL_APDU");
      encoder.flush();
      encoder.writeTo(baos);

      MimeBodyPart mbp1 = new MimeBodyPart();
      DataSource ds = new ByteArrayDataSource (baos.toByteArray(), "application/iso-10161-ill-1");
      DataHandler dh = new DataHandler(ds);
      dh.setCommandMap(null); // Revert to getDefaultCommandMap
      mbp1.setHeader("transfer-encoding","iso-8825-ber");
      mbp1.setDataHandler(dh);

      // Need to set transfer-encoding to iso-8825-ber

      Multipart mp = new MimeMultipart();
      mp.addBodyPart(mbp1);
      mime_msg.setContent(mp);
      mime_msg.setSentDate(new Date());

      mime_msg.setContent(mp);

      // set the Date: header
      mime_msg.setSentDate(new Date());

      if ( ( smtp_user != null ) && ( smtp_user.length() > 0 ) ) {
        log.debug("Sending MIME message via authenticated SMTP service : "+outgoing_mail_server+","+smtp_user+","+smtp_pass);

        Transport tr = session.getTransport("smtp");
        log.debug("Transport connection status : "+tr.isConnected());

        tr.connect(outgoing_mail_server, smtp_user, smtp_pass);
        log.debug("Attempting to send");
        mime_msg.saveChanges();
        tr.sendMessage(mime_msg, address);
        log.debug("Transport connection status after send : "+tr.isConnected());
        tr.close();
      }
      else {
        // send the message
        log.debug("Sending MIME message via open SMTP service at "+outgoing_mail_server);
        Transport.send(mime_msg);
      }
    }
    catch (MessagingException mex) {
      log.warn("Problem sending message: "+mex);
      Exception ex = null;
      if ((ex = mex.getNextException()) != null) {
        log.warn("Mail Inner exception",ex);
      }
    }
    catch ( Exception e ) {
      log.warn("Problem sending message",e);
    }
    finally {
      log.debug("All done sending mail\n");
    }

    return true;
  }

  public void init() {
    log.debug("Initialise MimeProtocolAdapter");
    reg = (OIDRegister)ctx.getBean("OIDRegister"); // .getRegister();
    com.k_int.openrequest.util.EngineStatusService status_service = null;
    if ( ctx.containsBean("EngineStatusService") ) 
      status_service = (com.k_int.openrequest.util.EngineStatusService) ctx.getBean("EngineStatusService");

    try {
      log.debug("Starting pop mailbox poller...");
      PopMailboxPoller server = new PopMailboxPoller(pop_server,pop_user,pop_pass,pop_folder,handler,reg,status_service);
      server.start();
    }
    catch ( Exception e ) {
      log.warn("Problem",e);
    }
  }

  public void stop()
  {
    log.debug("Stop MimeProtocolAdapter");
  }
}
