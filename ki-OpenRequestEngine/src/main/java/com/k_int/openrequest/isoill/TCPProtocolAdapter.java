package com.k_int.openrequest.isoill;

import java.rmi.*;
import java.rmi.server.*;
import java.net.*;
import java.io.*;
import java.util.*;
import org.jzkit.a2j.codec.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.k_int.openrequest.services.*;
import org.hibernate.*;
import com.k_int.openrequest.db.MessageQueueItem;
import com.k_int.openrequest.helpers.DBHelper;

public class TCPProtocolAdapter extends AbstractILLProtocolAdapter {

  private Log log = LogFactory.getLog(this.getClass());
  private int port = 1499;
  // timeout in milliseconds
  private int timeout = 30000;
  // try to resend after....
  private int retry_after = 300000;
  private OIDRegister reg = null;
  private Object queue_notification = new Object();
  private boolean running = true;

  private Thread message_send_thread = new Thread() {
    public void run() {
      messageSendLoop();
    }
  };

  public TCPProtocolAdapter() {
    super();
  }

  public boolean sendProtocolMessage(InternalIso10161Message msg) {
    log.info("enqueue message for sending : "+msg.hashCode()+" to "+msg.getPartnerTelecomServiceAddress());
    synchronized(queue_notification) {
      Session sess = null;
      try {
        // Get database connection
        SessionFactory sf = (SessionFactory) ctx.getBean("OpenRequestSessionFactory");
        sess = sf.openSession();
        // enqueue message
        MessageQueueItem queue_item = new MessageQueueItem("DUPLEXTCP",msg.getPartnerTelecomServiceAddress(),msg);
        sess.save(queue_item);
        sess.flush();
        sess.connection().commit();
        sess.clear();
        log.info("Message "+msg.hashCode()+" added to pending message queue. msg="+msg);
      }
      catch ( org.hibernate.HibernateException he ) {
        log.warn("Problem with enqueue message",he);
      }
      catch ( java.sql.SQLException sqle ) {
        log.warn("Problem with enqueue message",sqle);
      }
      finally {
        if ( sess != null ) {
          try { sess.close(); } catch ( Exception e ) {}
        }
      }
     
      // notify observers
      queue_notification.notifyAll();
    }
    return true;
  }

  public void messageSendLoop() {
    log.info("TCP Protocol Adapter - messageSendLoop started");

    SessionFactory sf = (SessionFactory) ctx.getBean("OpenRequestSessionFactory");

    while ( running ) {
      synchronized(queue_notification) {
        try {
          queue_notification.wait(retry_after);  // Timeout every 5 mins and try to send any pending messages
        }
        catch ( java.lang.InterruptedException ie ) {
        }
      }

      log.info("Message queue woken up");
      Session sess = null;
      try {
        // Get database connection
        sess = sf.openSession();

        List pending_outgoing_messages = DBHelper.getQueuedMessages(sess,"DUPLEXTCP");

        for ( Iterator i = pending_outgoing_messages.iterator(); i.hasNext(); ) {
          MessageQueueItem mqi = (MessageQueueItem) i.next();
          InternalIso10161Message m = (InternalIso10161Message) mqi.getMessage();
          if ( m != null ) {
            log.info("Attempting to deliver queued (at "+mqi.getDateAdded()+") message to "+m.getPartnerTelecomServiceAddress());
            if (attemptToSend(m)) {
              // if the send attempt was OK, remove the message from the queue.
              log.info("Delivered OK, delete mqi object");
              sess.delete(mqi);
              sess.flush();
              sess.connection().commit();
            }
            else {
              log.info("Message sending to "+m.getPartnerTelecomServiceAddress()+" failed Message will remain in queue");
              mqi.setNote("Message sending failed");
              mqi.setLastTry(new java.util.Date());
              sess.update(mqi);
              sess.flush();
              sess.connection().commit();
              sess.clear();
            }
          }
          else {
            log.error("\n\n***\nMessage was null\n***\n\n");
          }
        }
      }
      catch ( org.hibernate.HibernateException he ) {
        log.warn("Problem getting queued messages",he);
      }
      catch ( java.sql.SQLException sqle ) {
        log.warn("Problem getting queued messages",sqle);
      }
      finally {
        if ( sess != null ) {
          try { sess.close(); } catch ( Exception e ) {}
        }
      }
    }
  }
  
  public boolean attemptToSend(InternalIso10161Message msg) {
    boolean result = false;
    try {
      String service_address = msg.getPartnerTelecomServiceAddress();
      int colon_pos = service_address.indexOf(':');
      String remote_host = null;
      int remote_port = -1;

      if ( colon_pos > 0 ) {
        String port_str = service_address.substring(colon_pos+1,service_address.length());
        remote_host = service_address.substring(0,colon_pos);
        Integer remote_port_integer = new Integer(Integer.parseInt(port_str));
        remote_port = remote_port_integer.intValue();
      }
      else {
        throw new Exception("No connect information (service_address="+service_address+")");
      }

      log.debug("Open Socket to "+remote_host+":"+remote_port+" timeout="+timeout);
      
      Socket s = new Socket();
      s.connect(new InetSocketAddress(remote_host,remote_port),timeout);
      
      ILLAssociation assoc = new ILLAssociation(s, 
                                                handler, 
                                                remote_host, 
                                                remote_port,
                                                reg,
                                                logger);
      assoc.start();
      assoc.encodeAndSendPDU(msg.getPDU());
      log.debug("PDU sent");
      s.close();
      result = true;
    }
    catch ( java.io.IOException ioe ) {
      if(ioe.getMessage().equals("Connection Closed"))
        log.debug("Connection Closed");
      else {
        log.warn("Problem sending message",ioe);
      }
    }
    catch ( Exception e ) {
      log.warn("Problem sending message",e);
    }
    return result;
  }

  public void init() {
    log.debug("Initialise TCPProtocolAdapter");

    try {
      reg = (OIDRegister)ctx.getBean("OIDRegister");
      ListeningServer server = new ListeningServer(port, timeout, handler, reg, logger);
      log.debug("Staring listening TCP server");
      server.start();
      log.debug("Staring message send thread");
      message_send_thread.start();
    }
    catch ( Exception e ) {
      log.warn("Problem",e);
    }
  }

  public void stop() {
    log.debug("Stop TCPProtocolAdapter");
    running=false;
  }

  /**
   * Set the port on which this adapter should listen.
   * @param port
   */
  public void setPort(int port) {
    this.port = port;
  }

  public int getPort() {
    return port;
  }

  /**
   * set the default timeout for socket connections
   * @param timeout in milliseconds
   */
  public void setTimeout(int timeout) {
    this.timeout = timeout;
  }

  public int getTimeout() {
    return timeout;
  }

  /**
   * set the default timeout for socket connections
   * @param retry in milliseconds
   */
  public void setRetryAfter(int retry_after) {
    this.retry_after = retry_after;
  }

  public int getRetryAfter() {
    return retry_after;
  }
}
