package com.k_int.openrequest.isoill;

import org.springframework.context.*;

public abstract class AbstractILLProtocolAdapter implements ILLProtocolAdapter, ApplicationContextAware {

  protected ApplicationContext ctx = null;
  protected PDUNotificationTarget handler = null;
  protected PDULogger logger = null;

  public AbstractILLProtocolAdapter() {
    super();
  }

  public abstract boolean sendProtocolMessage(InternalIso10161Message msg);

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }

  public void setHandler(PDUNotificationTarget handler) {
    this.handler = handler;
  }

  public PDUNotificationTarget getHandler() {
    return handler;
  }

  public void setLogger(PDULogger logger) {
    this.logger = logger;
  }

  public PDULogger getLogger() {
    return logger;
  }
}
