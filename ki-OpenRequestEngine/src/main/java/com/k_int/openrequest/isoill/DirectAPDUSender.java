/**
 * Title:       
 * @version:    $Id: DirectAPDUSender.java,v 1.2 2005/07/07 17:57:20 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.isoill;

import com.k_int.openrequest.api.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.isoill.*;
import java.util.*;
import org.hibernate.*;
import org.hibernate.type.*;
import com.k_int.openrequest.isoill.ILLProtocolAdapter;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.gen.ILL_APDU_Delivery_Info.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.k_int.openrequest.util.WorkflowLogger;
import org.springframework.context.ApplicationContext;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;

public abstract class DirectAPDUSender {

  private static Log log = LogFactory.getLog(DirectAPDUSender.class);

  public static void send(ILLMessageEnvelope message, Session session, OIDRegister reg, ApplicationContext app_ctx) {
    try {
      ILLMessageEnvelope msg = (ILLMessageEnvelope)message;
      ILL_APDU_type pdu = msg.getPDU();
      String from_symbol =  msg.getSendingLocationSymbol();
      String to_symbol =  msg.getDestinationLocationSymbol();

      log.debug("DIRECT APDU"+from_symbol+" to "+to_symbol);

      APDUDeliveryInfoHelper.addMissingDeliveryInfo(msg,session,reg,app_ctx);

      LocationSymbol this_location = DBHelper.lookupLocation(session,"internal",from_symbol);

      LocationSymbol partner_info = DBHelper.lookupLocation(session,"internal",to_symbol);
      Service service_to_use = partner_info.getServiceForThisSymbol();

      String adapter_bean_name = service_to_use.getHandlerServiceName();
      log.debug("The service we will use is "+service_to_use.getHandlerServiceName());
      log.debug("Service props are "+service_to_use.getProperties());

      String queue_name = service_to_use.getHandlerServiceName();

      ILLProtocolAdapter adapter = (ILLProtocolAdapter) app_ctx.getBean(service_to_use.getHandlerServiceName());

      // We now use the current partner addressing information as found in the APDU Delivery info and
      // stored against the transacton. This allows us to change the recipient for a message by changing the
      // delivery info. We used to use service_to_use.getTelecomServiceIdentifier() and service_to_use.getTelecomServiceAddress()
      InternalIso10161Message reqmsg = new InternalIso10161Message(pdu,
                                                                   service_to_use.getTelecomServiceIdentifier(),
                                                                   service_to_use.getTelecomServiceAddress(),
                                                                   service_to_use.getProperties(),
                                                                   System.currentTimeMillis(),
                                                                   from_symbol,
                                                                   to_symbol);

      adapter.sendProtocolMessage(reqmsg);
    }
    catch ( java.sql.SQLException e ) {
      log.warn("Problem sending message",e);
    }
    catch ( org.hibernate.HibernateException he ) {
      log.warn("Problem sending message",he);
    }
    catch ( com.k_int.openrequest.helpers.HelperException he ) {
      log.warn("Problem sending message",he);
    }
  }
}
