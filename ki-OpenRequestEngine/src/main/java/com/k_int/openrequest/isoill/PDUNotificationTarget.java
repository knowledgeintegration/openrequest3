/**
 * Title:       PDUNotificationTarget
 * @version:    $Id: PDUNotificationTarget.java,v 1.3 2005/02/22 14:49:01 ibbo Exp $
 * Copyright:   Copyright (C) 2001 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson (ibbo@k-int.com)
 * Company:     KI
 * Description: 
 *
 */
 
/**
 * 
 *
 * @author Ian Ibbotson
 * @version $Id: PDUNotificationTarget.java,v 1.3 2005/02/22 14:49:01 ibbo Exp $
 */

package com.k_int.openrequest.isoill;

import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type;
import java.util.Map;

public interface PDUNotificationTarget
{
  /**
   * @param pdu - The protocol data unit
   * @param telecom_service_identifier - notification of the listening service identifier
   * @param telecom_service_address - notification of the listening service address
   * @param event_time - Exactly when
   * @param ill_association_context - A map of contextual items for the lifetime of the association (Used initially for auth)
   */
  public void notifyILLPDU(ILL_APDU_type pdu,
                           String telecom_service_identifier,
                           String telecom_service_address,
                           long event_time,
                           Map ill_association_context);
}
