package com.k_int.openrequest.isoill;

import java.io.Serializable;

 
/**
 * Title:       ILLSystemAddress
 * @version:    $Id: ILLSystemAddress.java,v 1.1 2005/05/02 09:24:47 ibbo Exp $
 * Copyright:   Copyright (C) 2001 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson (ibbo@k-int.com)
 * Company:     KI
 * Description: 
 *
 */
public class ILLSystemAddress implements Serializable {
  private String symbol;
  private String telecom_service_identifier;
  private String telecom_service_address;

  public ILLSystemAddress(String symbol, String telecom_service_identifier, String telecom_service_address) {
    this.symbol = symbol;
    this.telecom_service_identifier = telecom_service_identifier;
    this.telecom_service_address = telecom_service_address;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setTelecomServiceIdentifier(String telecom_service_identifier) {
    this.telecom_service_identifier = telecom_service_identifier;
  }

  public String getTelecomServiceIdentifier() {
    return telecom_service_identifier;
  }

  public void setTelecomServiceAddress(String telecom_service_address) {
    this.telecom_service_address = telecom_service_address;
  }

  public String getTelecomServiceAddress() {
    return telecom_service_address;
  }
}
