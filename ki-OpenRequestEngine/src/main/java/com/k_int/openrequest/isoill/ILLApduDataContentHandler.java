package com.k_int.openrequest.isoill;

import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.awt.datatransfer.*;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.commons.logging.*;

public class ILLApduDataContentHandler implements DataContentHandler
{
  private static Log log = LogFactory.getLog(ILLApduDataContentHandler.class);

  public static DataFlavor BER_ENCODED_FLAVOR = new DataFlavor(byte[].class,"BER Encoded Octet Stream");
  public static DataFlavor DECODED_FLAVOR = new DataFlavor(com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type.class, "ILL APDU");

  public ILLApduDataContentHandler() {
    log.debug("new ILLApduDataContentHandler");
  }

  public DataFlavor[] getTransferDataFlavors() {
    log.debug("1");
    DataFlavor[] result = { BER_ENCODED_FLAVOR };
    return result;
  }

  public Object getTransferData(DataFlavor df, DataSource ds) throws UnsupportedFlavorException, IOException
  {
    log.debug("2");
    if ( df == BER_ENCODED_FLAVOR )
    {
      int i = 0;
    }
    else
      throw new UnsupportedFlavorException(df);

    return null;
  }

  public Object getContent(DataSource ds) throws IOException
  {
    log.debug("3");
    // We use ds.getInputStream to get the input stream and create a new ByteArray
    int available = ds.getInputStream().available();
    byte[] result = new byte[available];
    ds.getInputStream().read(result);
    return result;
  }

  public void writeTo(Object obj, String mime_type, OutputStream os) throws IOException
  {
    log.debug("4");
    log.debug("Requesting "+mime_type);
    ByteArrayDataSource bads = (ByteArrayDataSource)obj;
    os.write(bads.getData());
  }
}
