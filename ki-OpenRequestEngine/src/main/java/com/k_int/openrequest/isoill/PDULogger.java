/**
 * Title:       PDUNotificationTarget
 * @version:    $Id: PDULogger.java,v 1.1 2005/06/11 18:04:11 ibbo Exp $
 * Copyright:   Copyright (C) 2001 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson (ibbo@k-int.com)
 * Company:     KI
 * Description: 
 *
 */
 
/**
 * 
 *
 * @author Ian Ibbotson
 * @version $Id: PDULogger.java,v 1.1 2005/06/11 18:04:11 ibbo Exp $
 */

package com.k_int.openrequest.isoill;

import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type;

public interface PDULogger
{
  /**
   *
   */
  public void logAPDU(String direction,
                      String type,
                      byte[] pdu);
}
