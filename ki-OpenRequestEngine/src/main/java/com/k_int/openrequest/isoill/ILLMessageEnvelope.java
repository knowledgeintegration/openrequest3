/**
 * Title:       ISO10161EventMessage
 * @version:    $Id: ILLMessageEnvelope.java,v 1.4 2005/04/10 15:28:56 ibbo Exp $
 * Copyright:   Copyright (C) 2001 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson (ibbo@k-int.com)
 * Company:     KI
 * Description: 
 *
 */
 
/**
 * 
 *
 * @author Ian Ibbotson
 * @version $Id: ILLMessageEnvelope.java,v 1.4 2005/04/10 15:28:56 ibbo Exp $
 */

package com.k_int.openrequest.isoill;

import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type;
import com.k_int.openrequest.api.MessageDeliveryObserver;
import java.util.*;
import java.io.Serializable;

public class ILLMessageEnvelope implements Serializable
{
  private ILL_APDU_type message = null;
  private String destination_location_symbol = null;
  private String sending_location_symbol = null;
  private String subject = null;
  private MessageDeliveryObserver mdo = null;

  public ILLMessageEnvelope(ILL_APDU_type message,
                            String sending_location_symbol,
                            String destination_location_symbol) {
    this.message = message;
    this.sending_location_symbol = sending_location_symbol;
    this.destination_location_symbol = destination_location_symbol;
  }

  public ILLMessageEnvelope(ILL_APDU_type message, 
                            String sending_location_symbol,
                            String destination_location_symbol,
                            MessageDeliveryObserver mdo) {
    this.message = message;
    this.destination_location_symbol = destination_location_symbol;
    this.sending_location_symbol = sending_location_symbol;
    this.mdo = mdo;
  }

  public ILL_APDU_type getPDU() {
    return message;
  }

  public String getDestinationLocationSymbol() {
    return destination_location_symbol;
  }

  public String getSendingLocationSymbol() {
    return sending_location_symbol;
  }

  public MessageDeliveryObserver getMessageDeliveryObserver() {
    return mdo;
  }

  public String toString() {
    return("ILLMessageEnvelope(...,dest="+destination_location_symbol+",src="+sending_location_symbol+",mdo="+mdo+")");
  }
}
