/**
 * Title:       ISOILLWorkflowAgent
 * @version:    $Id: ISOILLWorkflowAgent.java,v 1.3 2005/02/22 14:49:01 ibbo Exp $
 * Copyright:   Copyright (C) 2001 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson (ibbo@k-int.com)
 * Company:     KI
 * Description: Act as an interface between our generic Workflow framework and
 *              the ISO 10161 Protocol, sending and publishing PDU's
 */
 
package com.k_int.openrequest.isoill;

import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type;
import com.k_int.openrequest.helpers.*;
import com.k_int.openrequest.api.*;
import java.util.*;
import org.springframework.context.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * This class is the first handler invoked when any new protocol message is recieved. In essence,
 * This handler is given the responsibility of checking the validitiy of the message and it's
 * destination location. If all ok, this handler wraps the protocol message in an InternalIso10161Message object and
 * passes the new object along to the next processor in the chain.
 *
 * @author Ian Ibbotson
 * @version $Id: ISOILLWorkflowAgent.java,v 1.3 2005/02/22 14:49:01 ibbo Exp $
 */
public class ISOILLWorkflowAgent implements PDUNotificationTarget, ApplicationContextAware {

  public static Log log = LogFactory.getLog(ISOILLWorkflowAgent.class);

  private ApplicationContext ctx = null;

  public ISOILLWorkflowAgent() {
    log.debug("ISOILLWorkflowAgent::ISOILLWorkflowAgent()");
  }

  public void notifyILLPDU(ILL_APDU_type pdu,
                           String telecom_service_identifier,
                           String telecom_service_address,
                           long event_time,
                           Map ill_association_context) {
    log.debug("notifyILLPDU(..pdu..,"+telecom_service_identifier+","+telecom_service_address+","+event_time+","+ill_association_context+")");
    try {
      log.debug("Creating internal ISO Message to wrap apdu");
      InternalIso10161Message the_message = new InternalIso10161Message(pdu, 
                                                                        telecom_service_identifier,
                                                                        telecom_service_address,
                                                                        ill_association_context, 
                                                                        event_time,
                                                                        null,
                                                                        null);

      log.debug("Forwarding on to OpenRequest Message dispatcher");
      MessageDispatcher dispatcher = (MessageDispatcher) ctx.getBean("DISPATCHER");
      dispatcher.dispatch("System","internal:IncomingPDU",the_message);
    }
    catch ( Exception e )
    {
      log.warn("Problem sending notification "+e);
    }
  }

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }

}
