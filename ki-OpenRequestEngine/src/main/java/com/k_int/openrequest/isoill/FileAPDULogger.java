package com.k_int.openrequest.isoill;

import org.springframework.context.*;
import java.io.FileOutputStream;

public class FileAPDULogger implements PDULogger, ApplicationContextAware {

  protected String base_dir = ".";
  protected String file_sep = java.lang.System.getProperty("file.separator");
  protected ApplicationContext ctx = null;

  public static final String TIMESTAMP_FORMAT = "yyyyMMdd:HHmmss";
  public static final java.text.SimpleDateFormat TIMESTAMP_FORMATTER = new java.text.SimpleDateFormat(TIMESTAMP_FORMAT);

  public FileAPDULogger() {
    // file_sep = java.lang.System.getProperty("file.separator");
  }

  /**
   *
   */
  public void logAPDU(String direction,
                      String type,
                      byte[] pdu) {
    try {
      String timestamp = TIMESTAMP_FORMATTER.format(new java.util.Date()).toString();
      String filename = base_dir+file_sep+"ILLPDU:"+direction+":"+type+":"+timestamp;
      FileOutputStream fos = new FileOutputStream(filename+".ber");
      fos.write(pdu);
      fos.flush();
      fos.close();
    }
    catch ( java.io.FileNotFoundException fnfe ) {
      fnfe.printStackTrace();
    }
    catch ( java.io.IOException ioe ) {
      ioe.printStackTrace();
    }
  }

  public String getBaseDir() {
    return base_dir;
  }

  public void setBaseDir(String base_dir) { 
    this.base_dir = base_dir;
  }

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }

}
