// Title:       Base Z3950 Server
// @version:    $Id: ListeningServer.java,v 1.3 2005/06/11 18:04:11 ibbo Exp $
// Copyright:   Copyright (C) 2001 Knowledge Integration Ltd.
// @author:     Ian Ibbotson (ibbo@k-int.com)
// Company:     KI
// Description: 
//
 
 
/**
 * Server : Get incoming iso messages, send outgoing connection based messages
 *
 * @author Ian Ibbotson
 * @version $Id: ListeningServer.java,v 1.3 2005/06/11 18:04:11 ibbo Exp $
 */

package com.k_int.openrequest.isoill;

import java.net.*;
import java.io.*;
import java.util.*;
import org.jzkit.a2j.codec.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ListeningServer extends Thread {

  private static Log log = LogFactory.getLog(ListeningServer.class);

  private int socket_timeout = 300000;  // 300 second default timeout
  private boolean running = true;
  private ServerSocket server_socket = null;
  private int listening_port = -1;
  private PDUNotificationTarget handler = null;
  private OIDRegister reg = null;
  private PDULogger logger = null;

  public ListeningServer(int port, 
                         int timeout, 
                         PDUNotificationTarget handler, 
                         OIDRegister reg,
                         PDULogger logger) throws java.io.IOException {
    this.handler = handler;
    socket_timeout = timeout;
    listening_port = port;
    this.reg = reg;
    this.logger = logger;

    log.info("Creating ILL endpoint on port "+listening_port+ " (timeout="+socket_timeout+")");
    server_socket = new ServerSocket(listening_port);
  }       

  public void run() {
    try {
      while ( running ) {
        log.debug("Waiting for connection");
        Socket socket = (Socket)server_socket.accept();
        socket.setSoTimeout(socket_timeout);

        ILLAssociation ill_assoc = new ILLAssociation(socket, 
                                                      handler, 
                                                      socket.getInetAddress().getHostAddress(),
                                                      listening_port,
                                                      reg,
                                                      logger);
        ill_assoc.start();
      }

      server_socket.close();
    }
    catch (java.io.IOException e) {
      e.printStackTrace();
    }
  }

  public void shutdown(int shutdown_type) {
    this.running = false;

    switch ( shutdown_type ) {
      default:
        // Currently no special processing to join with or shutdown active associations.
	try {
	  server_socket.close();
	}
	catch ( java.io.IOException ioe ) {
          // No special action
	}
	break;
    }

    try {
      this.join();
    }
    catch ( java.lang.InterruptedException ie ) {
      // No action
    }
  }
}      

