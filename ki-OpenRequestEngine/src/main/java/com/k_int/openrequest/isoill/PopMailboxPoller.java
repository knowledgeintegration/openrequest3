// Title:       PopMailboxPoller
// @version:    $Id: PopMailboxPoller.java,v 1.2 2005/02/22 14:49:01 ibbo Exp $
// Copyright:   Copyright (C) 2001 Knowledge Integration Ltd.
// @author:     Ian Ibbotson (ibbo@k-int.com)
// Company:     KI
// Description: 
//
 
 
/**
 * Server : Get incoming mime encoded iso messages
 *
 * @author Ian Ibbotson
 * @version $Id: PopMailboxPoller.java,v 1.2 2005/02/22 14:49:01 ibbo Exp $
 */

package com.k_int.openrequest.isoill;

import java.net.*;
import java.io.*;
import java.util.*;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.codec.runtime.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import javax.mail.*;
import javax.mail.event.*;
import javax.activation.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import java.net.*;
import java.io.*;
import java.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import java.math.BigInteger;
                                                                                                                                                                            
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PopMailboxPoller extends Thread
{
  public static Log cat = LogFactory.getLog(PopMailboxPoller.class);


  private boolean running = true;
  private PDUNotificationTarget handler = null;
  private String incoming_mail_server = null;
  private String incoming_user = null;
  private String incoming_password = null;
  private String incoming_folder = null;
  private String incoming_provider = "pop3";

  public static final String US_ASCII_ENCODING = "US-ASCII";
  public static final String UTF_8_ENCODING = "UTF-8";
  public static final String UTF_16_ENCODING = "UTF-16";
  private ILL_APDU_codec codec = ILL_APDU_codec.getCodec();
                                                                                                                                                                            
  private String charset_encoding = US_ASCII_ENCODING;

  private static int pdu_counter=0;
  private OIDRegister reg = null;
  private com.k_int.openrequest.util.EngineStatusService status_service;


  public PopMailboxPoller(String incoming_mail_server,
                          String incoming_user,
                          String incoming_password,
                          String incoming_folder,
                          PDUNotificationTarget handler,
                          OIDRegister reg,
                          com.k_int.openrequest.util.EngineStatusService status_service) throws java.io.IOException
  {
    this.handler = handler;
    this.incoming_mail_server = incoming_mail_server;
    this.incoming_user = incoming_user;
    this.incoming_password = incoming_password;
    this.incoming_folder = incoming_folder;
    this.reg = reg;
    this.status_service = status_service;

    cat.debug("pop mailbox poller : "+incoming_mail_server+", "+incoming_user+", "+incoming_password+", "+incoming_folder);
  }       

  public void processMessages(Message[] msgs) {
    // Just dump out the new messages
    Map ill_association_context = new HashMap();

    for (int i = 0; i < msgs.length; i++) 
    {
      try 
      {
        cat.debug("Processing.... "+msgs[i].getSubject()+" from "+msgs[i].getFrom());
        cat.debug("content type of top level message is "+msgs[i].getContentType() );

        DataHandler dh = msgs[i].getDataHandler();
        Object content = dh.getContent();

        if (content instanceof Multipart) {
          Multipart mp = (Multipart)content;
          boolean has_message = false;
          for(int mpi = 0; mpi < mp.getCount(); mpi++) {
            has_message = has_message || checkPartForIsoMessage(mp.getBodyPart(mpi), ill_association_context);
          }
          if ( has_message ) {
            msgs[i].setFlag(Flags.Flag.DELETED, true);
          }
        }
        else {
          if ( checkPartForIsoMessage(msgs[i], ill_association_context) ) {
            msgs[i].setFlag(Flags.Flag.DELETED, true);
          }
        }
      } 
      catch (IOException ioex) 
      {
        ioex.printStackTrace();
        cat.warn("Problem polling mailbox",ioex);
      } 
      catch (MessagingException mex) 
      {
        mex.printStackTrace();
        cat.warn("Problem polling mailbox",mex);
      }
    }
  }

  public boolean checkPartForIsoMessage(Part part, Map ill_association_context) throws javax.mail.MessagingException, java.io.IOException {
    cat.debug("Checking part "+part.getContentType());
    if ( part.getContentType().toLowerCase().startsWith("application/iso-10161-ill-1") ) {
      cat.debug("Processing ill mail message");
      DataHandler part_dh = part.getDataHandler();
      // saveMessage(part_dh.getInputStream());
      BERInputStream bds = new BERInputStream(part_dh.getInputStream(),charset_encoding,reg);
      ILL_APDU_type pdu = null;
      pdu = (ILL_APDU_type)codec.serialize(bds, pdu, false, "PDU");
      cat.debug("Got pdu, send notifications");
      String telecom_service_address = "email_address";
      handler.notifyILLPDU(pdu,"MIME",telecom_service_address,System.currentTimeMillis(),ill_association_context);

      cat.debug("Deleting mail from inbox");
      return true;
    }
    else {
      cat.debug("Mime part Not application/iso-10161-ill-1, its "+part.getContentType());
    }

    return false;
  }

  public void run() {

    Properties props = System.getProperties();

    try {
      // Get a Session object
      while ( running ) {
        try {
          cat.debug("Sleeping");
          Thread.sleep(60000);
          cat.debug("Polling");

          cat.debug("Checking for new messages on POP Mailbox "+incoming_mail_server+":"+incoming_user+":"+incoming_password);
          Session session = Session.getDefaultInstance(props, null);
          // Store store = session.getStore("imap");
          // Store store = session.getStore("pop3");
          Store store = session.getStore(incoming_provider);
          store.connect(incoming_mail_server, incoming_user, incoming_password);

          Folder folder = store.getFolder(incoming_folder);
          if (folder == null || !folder.exists()) {
            cat.warn("Invalid folder ("+incoming_folder+"="+folder+")");
          }
          else {
            cat.debug("Registering folder monitor...... folder="+folder.toString());

            folder.open(Folder.READ_WRITE);
            processMessages(folder.getMessages());
            folder.close(true);
          } 

          store.close();
          status_service.setProperty("openrequest.messaging.mime.lastsuccessfulcheck",new java.util.Date().toString());
        }
        catch ( Exception e ) {
          e.printStackTrace();
          cat.warn("Problem polling mailbox",e);
        }
      }
    }
    finally {
      cat.debug("Pop mailbox poller exiting");
    }
  }

  public void shutdown(int shutdown_type) {
    this.running = false;

    cat.debug("Shutdown");

    switch ( shutdown_type ) {
      default:
        // No special action
	break;
    }

    try {
      this.join();
    }
    catch ( java.lang.InterruptedException ie ) {
      // No action
    }
  }

  public void saveMessage(InputStream in) throws java.io.IOException {
    File dst = new File("/tmp/pdu."+pdu_counter++);
    OutputStream out = new FileOutputStream(dst);
    
    // Transfer bytes from in to out
    byte[] buf = new byte[1024];
    int len;
    while ((len = in.read(buf)) > 0) {
      out.write(buf, 0, len);
    }
    in.close();
    out.close();
  }
}      



