package com.k_int.openrequest.isoill;

import java.rmi.*;
import java.rmi.server.*;
import java.util.*;
import java.io.*;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.services.*;
import org.apache.commons.logging.*;



public class LoopbackProtocolAdapter extends AbstractILLProtocolAdapter {

  private static Log log = LogFactory.getLog(LoopbackProtocolAdapter.class);

  private List message_queue = Collections.synchronizedList(new LinkedList());
  private boolean running = true;
  private MessageDispatcher dispatcher = null;

  private Thread message_consumer = new Thread() {


    public void run() {
      log.info("Waiting for messages to send");
      while (running) {
        while ( message_queue.size() > 0 ) {

          InternalIso10161Message msg = (InternalIso10161Message)message_queue.remove(0);
          try {
            log.debug("Sending message");
            dispatcher.dispatch("System", "internal:IncomingValidPDU",msg);
          }
          catch ( Exception e ) {
            log.warn("Problem sending message",e);
          }
        }

        try {
          synchronized(message_queue) {
            log.debug("Waiting for messages....");
            message_queue.wait();
          }
        }
        catch( java.lang.InterruptedException ie ) {
          log.debug("Interrupted");
        }
        finally {
        }
      }
    }
  };

  public LoopbackProtocolAdapter() {
  }

  public boolean sendProtocolMessage(InternalIso10161Message msg) {

    synchronized(message_queue) {
      message_queue.add(msg);
      message_queue.notifyAll();
    }
    return true;
  }

  public void init() {
    log.info("Initialise LoopbackProtocolAdapter");
    message_consumer.start();
    dispatcher = (MessageDispatcher) ctx.getBean("DISPATCHER");
  }

  public void stop() {
    log.info("Stop LoopbackProtocolAdapter");
  }

}
