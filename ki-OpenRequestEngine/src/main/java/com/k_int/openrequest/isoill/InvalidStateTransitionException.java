/**
 * Title:       
 * @version:    $Id: InvalidStateTransitionException.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.isoill;


public class InvalidStateTransitionException extends Exception
{
  public InvalidStateTransitionException(String s)
  {
    super(s);
  }
}
