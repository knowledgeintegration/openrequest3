package com.k_int.openrequest.isoill;

import java.rmi.*;
import java.rmi.server.*;
import java.net.*;
import java.io.*;
import java.util.*;
import org.jzkit.a2j.codec.util.*;
import org.jzkit.a2j.gen.AsnUseful.*;
import org.jzkit.a2j.codec.runtime.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.k_int.openrequest.services.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class TestMime
{
  private static Log log = LogFactory.getLog(TestMime.class);
  private String charset_encoding = "US-ASCII";

  // public MimeProtocolAdapter(PDUNotificationTarget handler) throws RemoteException
  public static void main(String[] args)
  {
    try
    {
      // MailcapCommandMap cmap = (MailcapCommandMap)javax.activation.CommandMap.getDefaultCommandMap();
      // cmap.addMailcap("application/iso-10161-ill-1;; x-java-content-handler=com.k_int.openrequest.isoill.ILLApduDataContentHandler");
      // CommandMap.setDefaultCommandMap(cmap);
     
      String service_address = "ibbo@k-int.com";
      log.debug("Email to "+service_address);
      Properties props = new Properties(System.getProperties());
      props.put("mail.smtp.host", "mailhost");
      Session session = Session.getDefaultInstance(props, null);
      session.setDebug(true);
      MimeMessage mime_msg = new MimeMessage(session);
      mime_msg.setFrom(new InternetAddress("ill@k-int.com"));
      InternetAddress[] address = {new InternetAddress(service_address)};
      mime_msg.setRecipients(Message.RecipientType.TO, address);
      mime_msg.setSubject("ISO ILL APDU");

      // ByteArrayOutputStream baos = new ByteArrayOutputStream();
      // baos.write(new String("Hello").getBytes());
      MimeBodyPart mbp1 = new MimeBodyPart();
      // mbp1.setContent(new String("ILLAPDU").getBytes(),"application/iso-10161-ill-1");
      // mbp1.setContent("HelloWorld","text/plain");
      // mbp1.setDataHandler(new DataHandler("HelloWorldnnn","text/html"));
      DataSource ds = new ByteArrayDataSource (new String("Hellov4").getBytes(), "application/iso-10161-ill-1");
      // DataHandler dh = new DataHandler(new String("Hellov4").getBytes(),"application/iso-10161-ill-1");
      DataHandler dh = new DataHandler(ds);
      dh.setCommandMap(null); // Revert to getDefaultCommandMap
      mbp1.setDataHandler(dh);
      // mbp1.setDataHandler(new DataHandler("Hellov4","application/iso-10161-ill-1"));
      // mbp1.setText("A test attachment");
      Multipart mp = new MimeMultipart();
      mp.addBodyPart(mbp1);
      mime_msg.setContent(mp);
      mime_msg.setSentDate(new Date());

      Transport.send(mime_msg);
    }
    catch (MessagingException mex) 
    {
      mex.printStackTrace();
      Exception ex = null;
      if ((ex = mex.getNextException()) != null) {
        ex.printStackTrace();
      }
    }
    catch ( Exception e )
    {
      log.warn("Problem sending message",e);
    }
  }
}
