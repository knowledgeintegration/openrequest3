package com.k_int.openrequest.isoill;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ILLProtocolAdapter {

  boolean sendProtocolMessage(InternalIso10161Message msg);

}
