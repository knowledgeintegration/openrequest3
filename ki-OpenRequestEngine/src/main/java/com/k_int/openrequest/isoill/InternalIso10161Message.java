/**
 * Title:       PDUNotificationTarget
 * @version:    $Id: InternalIso10161Message.java,v 1.3 2005/05/02 09:57:47 ibbo Exp $
 * Copyright:   Copyright (C) 2001 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson (ibbo@k-int.com)
 * Company:     KI
 * Description: 
 *
 */
 

/**
 * 
 *
 * @author Ian Ibbotson
 * @version $Id: InternalIso10161Message.java,v 1.3 2005/05/02 09:57:47 ibbo Exp $
 */

package com.k_int.openrequest.isoill;

import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.ILL_APDU_type;
import java.util.*;
import java.io.Serializable;

public class InternalIso10161Message implements Serializable
{
  private ILL_APDU_type message = null;
  private transient Map ill_assoc_context = null;
  private long event_time;
  private String from_symbol = null;
  private String to_symbol = null;
  private String partner_telecom_service_identifier = null;
  private String partner_telecom_service_address = null;


  public InternalIso10161Message (ILL_APDU_type message,
                                  String partner_telecom_service_identifier,
                                  String partner_telecom_service_address,
                                  Map ill_assoc_context,
                                  long event_time,
                                  String from_symbol,
                                  String to_symbol) {
    this.message = message;
    this.partner_telecom_service_identifier = partner_telecom_service_identifier;
    this.partner_telecom_service_address = partner_telecom_service_address;
    this.ill_assoc_context = ill_assoc_context;
    this.event_time = event_time;
    this.from_symbol = from_symbol;
    this.to_symbol = to_symbol;
  }

  public Map getILLAssociationContext() {
    return ill_assoc_context;
  }

  public ILL_APDU_type getPDU() {
    return message;
  }

  public long getEventTime() {
    return event_time;
  }

  public String getFromSymbol() {
    return from_symbol;
  }

  public void setFromSymbol(String from_symbol) {
    this.from_symbol = from_symbol;
  }

  public String getToSymbol() {
    return to_symbol;
  }

  public void setToSymbol(String to_symbol) {
    this.to_symbol = to_symbol;
  }

  public String getPartnerTelecomServiceIdentifier() {
    return partner_telecom_service_identifier;
  }

  public void setPartnerTelecomServiceIdentifier(String partner_telecom_service_identifier) {
    this.partner_telecom_service_identifier=partner_telecom_service_identifier;
  }

  public String getPartnerTelecomServiceAddress() {
    return partner_telecom_service_address;
  }

  public void setPartnerTelecomServiceAddress(String partner_telecom_service_address) {
    this.partner_telecom_service_address = partner_telecom_service_address;
  }
}
