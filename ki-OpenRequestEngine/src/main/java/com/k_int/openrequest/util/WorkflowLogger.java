/**
 * Title:       WorkflowLogger
 * @version:    $Id: WorkflowLogger.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */

package com.k_int.openrequest.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class WorkflowLogger
{
  private static Log log = LogFactory.getLog("Workflow");

  public static void log(String location_id,
                         String event,
                         String action)
  {
    log.info("Location: "+location_id+" event: "+event+" action: "+action);
  }
}
