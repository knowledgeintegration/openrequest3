package com.k_int.openrequest.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EngineStatusService {

  public static Log cat = LogFactory.getLog(EngineStatusService.class);
  
  java.util.List message_queue = Collections.synchronizedList(new LinkedList());
  java.util.Map<String,String> system_properties = new java.util.HashMap<String,String>();

  public EngineStatusService() {
  }

  public void init() {
  }

  public void logEvent(Object message) {
    cat.debug("logEvent:"+message);
    message_queue.add(0,message);
  }

  public List<String> getStatus(long max_items) {
    long ctr = 0;
    List<String> result = new java.util.ArrayList<String>();
    for ( java.util.Iterator i = message_queue.iterator(); ( ( i.hasNext() ) && ( ctr++ < max_items ) ); ) {
      result.add(i.next().toString());
    } 
    return result;
  }

  public String getProperty(String name) {
    String result = system_properties.get(name);

    if ( result == null ) 
      result = "";

    return result;
  }

  public void setProperty(String name, String value) {
    cat.debug("setProperty:"+name+"="+value);
    system_properties.put(name,value);
  }
}
