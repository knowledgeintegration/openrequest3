/**
 * Title:       WorkflowLogger
 * @version:    $Id: APDUSender.java,v 1.1 2005/05/14 22:39:06 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */

package com.k_int.openrequest.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.io.*;
import java.net.Socket;

public class APDUSender
{
  private static Log log = LogFactory.getLog(APDUSender.class);

  public static void main(String[] args) {
    String apdu_file_name = args[0];
    String hostname = args[1];
    int port = 1199;

    if ( hostname.lastIndexOf(':') != -1 ) {
      String[] components = hostname.split(":");
      hostname=components[0];
      port=Integer.parseInt(components[1]);
    }

    log.debug("Sending apdu defined in file "+apdu_file_name+" to "+hostname+":"+port);

    try {
      File apdu_file = new File(apdu_file_name);
      FileInputStream fis = new FileInputStream(apdu_file);
      Socket socket = new Socket(hostname,port);
      OutputStream socket_output_stream = socket.getOutputStream();
  
      byte[] buffer = new byte[4096];
      int byte_count = fis.read(buffer);
      while ( byte_count != -1 ) {
        log.debug("Processing "+byte_count+" bytes");
        socket_output_stream.write(buffer,0,byte_count);
        byte_count = fis.read(buffer);
      }

      fis.close();
      socket_output_stream.close();
      socket.close();
    }
    catch ( java.io.FileNotFoundException fnfe ) {
      log.warn("Problem",fnfe);
    }
    catch ( java.io.IOException ioe ) {
      log.warn("Problem",ioe);
    }
  }
}
