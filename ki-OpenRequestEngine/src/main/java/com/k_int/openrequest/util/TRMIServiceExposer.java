package com.k_int.openrequest.util;

import java.util.*;
import java.io.InputStream;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.*;

import java.rmi.*;
import java.rmi.registry.*;

import trmi.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 *
 */
public class TRMIServiceExposer implements ApplicationContextAware, org.springframework.beans.factory.InitializingBean {

  public static Log log = LogFactory.getLog(TRMIServiceExposer.class);

  private String service_url=null;
  private String service_interface=null;
  private String service_impl_bean_id=null;
  private int service_port;
  private ApplicationContext ctx;

  public TRMIServiceExposer() {
  }

  public void setServiceURL(String service_url) {
    this.service_url = service_url;
  }

  public String getServiceURL() {
    return service_url;
  }

  public void setServicePort(int service_port) {
    this.service_port = service_port;
  }

  public int getServicePort() {
    return service_port;
  }

  public void setServiceInterface(String service_interface) {
    this.service_interface = service_interface;
  }

  public String getServiceInterface() {
    return service_interface;
  }

  public void setServiceImplBeanId(String service_impl_bean_id) {
    this.service_impl_bean_id = service_impl_bean_id;
  }

  public String getServiceImplBeanId() {
    return service_impl_bean_id;
  }

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }

  /**
   * Locate or create the RMI registry for this exporter.
   * @param registryPort the registry port to use
   * @return the RMI registry
   * @throws RemoteException if the registry couldn't be located or created
   */
  protected Registry getRegistry(int registryPort) throws RemoteException {
    log.debug("Looking for RMI registry at port '" + registryPort + "'");
    Registry registry;
    try {
      // retrieve registry
      registry = LocateRegistry.getRegistry(registryPort);
      registry.list();
    }
    catch (RemoteException ex) {
      log.debug("Could not detect RMI registry - creating new one");
      // assume no registry found -> create new one
      registry = LocateRegistry.createRegistry(registryPort);
    }
    return registry;
  }

  public void afterPropertiesSet() throws Exception {
    System.err.println("Rebind component "+service_url);

    Object impl = ctx.getBean(service_impl_bean_id);
    try {
      getRegistry(service_port);
      trmi.Naming.rebind(service_url, impl, new Class[] {Class.forName(service_interface)});
    }
    catch ( java.lang.ClassNotFoundException cnfe ) {
      cnfe.printStackTrace();
    }

  }
}
