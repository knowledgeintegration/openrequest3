/**
 * Title:       
 * @version:    $Id: AbstractLocationImporter.java,v 1.4 2005/06/13 21:35:37 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.util;

import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.db.folders.*;
import com.k_int.openrequest.helpers.DBHelper;
import java.util.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import org.hibernate.*;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.*;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;
import org.apache.commons.logging.*;

public abstract class AbstractLocationImporter {
  public int counter=0;
  public int commit_counter=0;
  public int commit_interval=50;
  private static Log log = LogFactory.getLog(AbstractLocationImporter.class);

  public AbstractLocationImporter() {
  }

  public void runImp(String[] args) {
    runImp(args, 50);
  }

  public void runImp(String[] args, int commit_interval) {

    log.debug("runImp..... commit every "+commit_interval+" Locations");
    this.commit_interval = commit_interval;

    try {
      log.debug("Setting up classpath.....");

      // Set up the application context
      ApplicationContext app_context = new ClassPathXmlApplicationContext( new String[] {"BaseApplicationContext.xml"} );

      log.debug("Setting Database/Hibernate Session (Updating database schema as needed).....");

      SessionFactory factory = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      Session sess = factory.openSession();

      log.debug("Calling iterateLocations");
      iterateLocations(app_context,sess);

      log.debug("Flushing all new location data to database & commit");
      sess.flush();
      sess.connection().commit();
      log.debug("Close session");
      sess.close();
    }
    catch ( Exception e ) {
      e.printStackTrace();
      log.error("Problem importing location data",e);
    }
    finally {
      log.debug("All Done");
    }
  }

  public void runImp(ApplicationContext app_context, int commit_interval) {

    log.debug("runImp..... commit every "+commit_interval+" Locations");
    this.commit_interval = commit_interval;

    try {
      log.debug("Setting up classpath.....");
      SessionFactory factory = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      Session sess = factory.openSession();

      log.debug("Calling iterateLocations");
      iterateLocations(app_context,sess);

      log.debug("Flushing all new location data to database & commit");
      sess.flush();
      sess.connection().commit();
      log.debug("Close session");
      sess.close();
    }
    catch ( Exception e ) {
      e.printStackTrace();
      log.error("Problem importing location data",e);
    }
    finally {
      log.debug("All Done");
    }
  }


  public abstract void iterateLocations(ApplicationContext app_context, Session sess);

  public Location impRemoteTCPSymbol(Session sess,
                                     String naming_authority,
                                     String symbol,
                                     String location_name,
                                     String telecom_system_address,
                                     String app_specific_1,
                                     int auth_method,
                                     String user,
                                     String password) throws Exception {


    String full_symbol = naming_authority+":"+symbol;
    log.debug("Import TCP Symbol for "+location_name+" ("+full_symbol+")");

    LocationType normal_location_type = DBHelper.lookupLocationType(sess,"NORM");

    log.debug("Using location  type: "+normal_location_type);

    //ToDo: We need to check lengths to avoid database problems.

    // Create a new location
    log.debug("Create new location record for "+location_name);
    Location new_loc = new Location(location_name, normal_location_type, full_symbol);
    new_loc.getProperties().put("RequiresAPDUDeliveryInfo","t");

    // Set any app specific value (eg Role for OCLC)
    new_loc.setAppSpecific1(app_specific_1);
    Long new_loc_id = (Long) sess.save(new_loc);
    log.debug("New loc id = "+new_loc_id);

    log.debug("lookup or create service: "+telecom_system_address);
    Service service = DBHelper.lookupOrCreateService(sess, 
                                                     "TCP", 
                                                     telecom_system_address, 
                                                     "TCP:"+telecom_system_address, 
                                                     "ILL/DUPLEX", 
                                                     "DUPLEXTCP", 0, 3, auth_method, user, password); 

    log.debug("lookup or create authority: "+naming_authority);
    NamingAuthority auth = DBHelper.lookupOrCreateAuthority(sess,naming_authority,null);

    sess.flush();
    sess.connection().commit();
    LocationSymbol test = DBHelper.lookupLocation(sess, full_symbol);

    if ( test == null ) {
      log.debug(full_symbol+" not already present in directory, adding....");
      LocationSymbol sym = new LocationSymbol(symbol, auth, new_loc, service);
      sess.save(sym);
    }
    else {
      log.debug("Symbol not created - Duplicate");
    }

    log.debug("Done Importing "+naming_authority+":"+symbol+" Counter#"+(++counter));

    if ( (++commit_counter) >= commit_interval ) {
      log.debug("Commit");
      sess.flush();
      sess.connection().commit();
      commit_counter=0;
    }

    return new_loc;
  }

  public Location impRemoteTCPSymbol(Session sess,
                                     String naming_authority,
                                     String symbol,
                                     String location_name,
                                     String telecom_system_address,
                                     String app_specific_1) throws Exception {
    return impRemoteTCPSymbol(sess,naming_authority,symbol,location_name,telecom_system_address,app_specific_1,0,null,null);
  }
}
