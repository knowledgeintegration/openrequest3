package com.k_int.openrequest.util;

import java.util.logging.*;
import java.util.*;
import java.io.InputStream;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.*;

import java.rmi.*;
import java.rmi.registry.*;

import trmi.*;

/**
 * Use RMI to obtain an instance of a remote object using the supplied service url.
 */
public class TRMIServiceFactory implements ApplicationContextAware {

  private static Logger log = Logger.getLogger(TRMIServiceFactory.class.getName());

  private String service_url=null;
  private ApplicationContext ctx;

  public TRMIServiceFactory(ApplicationContext ctx, String service_url) {
    this.ctx=ctx;
    this.service_url=service_url;
  }

  public TRMIServiceFactory() {
    System.out.println("New TRMIServiceFactory...");
  }

  public void setServiceURL(String service_url) {
    this.service_url = service_url;
  }

  public String getServiceURL() {
    return service_url;
  }

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }

  public Object getInstance() throws NotBoundException,RemoteException,java.net.MalformedURLException {
    System.out.println("newInstance of service interface...");
    return trmi.Naming.lookup(service_url);
  }
}
