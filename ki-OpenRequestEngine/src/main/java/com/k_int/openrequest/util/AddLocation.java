/**
 * Title:       
 * @version:    $Id: AddLocation.java,v 1.2 2005/05/14 22:39:06 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.util;

import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.db.folders.*;
import com.k_int.openrequest.helpers.DBHelper;
import java.util.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import org.hibernate.*;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;

/* TODO: Check that protocol adapters dont use the services list hanging off loc.. but instead uses service attached to symbol */

public class AddLocation
{
  public AddLocation()
  {
  }

  public Object createLocationSymbol(String full_alias,
                                     String symbol,
                                     NamingAuthority authority,
                                     Location location,
                                     Service service_for_this_symbol,
                                     Session sess) throws java.sql.SQLException,
                                                          org.hibernate.HibernateException
  {
    LocationSymbol test = DBHelper.lookupLocation(sess, full_alias);

    if ( test == null ) {
      LocationSymbol sym = new LocationSymbol(symbol, authority, location, service_for_this_symbol);
      return sess.save(sym);
    }
    else {
      System.err.println("Symbol not created - Duplicate");
      return test;
    }
  }

  public static void main(String[] args)
  {
    AddLocation a = new AddLocation();
    a.setup(args);
  }

  public void setup(String[] args) {

    try
    {
      BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

      // Set up the application context
      ClassPathXmlApplicationContext app_context = new ClassPathXmlApplicationContext( new String[] {"DefaultApplicationContext.xml"} );

      // LocalSessionFactoryBean lsfb = (LocalSessionFactoryBean) app_context.getBean("&OpenRequestSessionFactory");
      // lsfb.updateDatabaseSchema();

      SessionFactory factory = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      Session sess = factory.openSession();


      LocationType normal_location_type = DBHelper.lookupLocationType(sess,"NORM");



      System.out.print("New Location Name (Or enter to quit) : ");
      String location_name = in.readLine();

      while ( ( location_name != null ) && ( location_name.length() > 0 ) ) {
        System.out.print("  Default Symbol (Auth:Symbol) : ");
        String default_symbol = in.readLine();

        // Create a new location 
        Location new_loc = new Location(location_name, normal_location_type, default_symbol );
        new_loc.getProperties().put("RequiresAPDUDeliveryInfo","t");
        sess.save(new_loc);

        System.out.print("Add Service [M]ail or [T]cp : ");
        String service_type = in.readLine();
 
        Service service = null;

        while ( ( service_type != null ) && ( service_type.length() > 0 ) ) {
          if ( service_type.equalsIgnoreCase("M") ) {
            System.err.print("Service Email Address: ");
            String network_address = in.readLine();
            service = new Service("Mime System Address "+network_address, "ILL/MIME", "MIME", 0, 2);
            service.setTelecomServiceIdentifier("SMTP");
            service.setTelecomServiceAddress(network_address);
            sess.save(service);
          }
          else if ( service_type.equalsIgnoreCase("T") ) {
            System.err.print("Service TCP Address (host:port) : ");
            String network_address = in.readLine();
            service = new Service("TCP System Address "+network_address, "ILL/DUPLEX", "DUPLEXTCP", 0, 3);
            service.setTelecomServiceIdentifier("TCP");
            service.setTelecomServiceAddress(network_address);
            sess.save(service);
          }
          else {
            System.out.println("Unknown type");
          }

          System.out.println("Alias for this location: ");
          String alias = in.readLine();
          while ( ( alias != null ) && ( alias.length() > 0 ) ) {
            int pos = alias.indexOf(":");
            String alias_auth_part = alias.substring(0,pos);
            String alias_name_part = alias.substring(pos+1);
            NamingAuthority auth = DBHelper.lookupOrCreateAuthority(sess,alias_auth_part,null);
            createLocationSymbol(alias, alias_name_part,auth,new_loc,service,sess);
            System.out.println("Addiotnal aliases for this location (or enter when finished): ");
            alias = in.readLine();
          }

          System.out.print("Additional Service [M]ail or [T]cp (or enter when finished: ");
          service_type = in.readLine();
        }
     
        System.out.print("Next Location Name (Or enter to quit) : ");
        location_name = in.readLine();
      }

      sess.flush();
      sess.connection().commit();
      sess.close();
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }
    finally
    {
      System.err.println("Done");
    }
  }
}
