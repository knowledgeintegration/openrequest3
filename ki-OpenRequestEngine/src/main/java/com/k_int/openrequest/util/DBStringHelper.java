/**
 * Title:       DBStringHelper
 * @version:    $Id: DBStringHelper.java,v 1.1 2005/07/01 16:11:12 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */

package com.k_int.openrequest.util;

public class DBStringHelper {
  public static String set(String value, int maxlen) {
    String result = value;
    if ( ( result != null ) && ( result.length() > maxlen ) ) {
      result = value.substring(0,maxlen-1);
    }
    return result;
  }
}
