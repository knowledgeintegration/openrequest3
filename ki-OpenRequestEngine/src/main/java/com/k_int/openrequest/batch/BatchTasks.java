/**
 * Title:       
 * @version:    $Id: BatchTasks.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.batch;

import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.DBHelper;
import java.util.*;
import org.hibernate.*;
import org.hibernate.type.*;
import com.k_int.openrequest.services.*;
import com.k_int.openrequest.api.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.*;

public class BatchTasks implements ApplicationContextAware {

  private SessionFactory sf = null;
  private MessageDispatcher dispatcher = null;
  private Log log = LogFactory.getLog(this.getClass());
  private String rmi_server_url = null;
  private Timer timer = new Timer(true);
  private org.springframework.beans.factory.BeanFactory app_context;
  private int delay;
  private int period;
  protected ApplicationContext ctx = null;

  public BatchTasks() throws java.rmi.RemoteException {
    super();
  }

  public void init() {
    log.debug("Initialise BatchTasks");

    sf = (SessionFactory) ctx.getBean("OpenRequestSessionFactory");
    dispatcher = (MessageDispatcher) ctx.getBean("DISPATCHER");
    timer.scheduleAtFixedRate(new OpenRequestBackgroundTask(sf, dispatcher), 300000, 300000);
  }

  public void stop() {
    log.debug("Stop BatchTasks");
  }

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }

  public int getDelay() {
    return delay;
  }

  public void setDelay(int delay) {
    this.delay = delay;
  }

  public int getPeriod() {
    return period;
  }

  public void setPeriod(int period) {
    this.period = period;
  }
}
