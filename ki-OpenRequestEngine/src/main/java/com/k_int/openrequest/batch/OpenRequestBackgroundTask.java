/**
 * Title:       
 * @version:    $Id: OpenRequestBackgroundTask.java,v 1.2 2005/06/12 18:58:13 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.batch;

import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.helpers.DBHelper;
import java.util.*;
import java.rmi.*;
import java.rmi.server.*;
import com.k_int.openrequest.services.*;
import com.k_int.openrequest.api.*;
import com.k_int.openrequest.isoill.gen.ISO_10161_ILL_1.*;
import com.k_int.openrequest.isoill.ILLMessageEnvelope;
import com.k_int.openrequest.helpers.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import org.hibernate.type.*;

public class OpenRequestBackgroundTask extends TimerTask
{
  private SessionFactory sf = null;
  private MessageDispatcher dispatcher = null;
  private Log log = LogFactory.getLog(this.getClass());

  public OpenRequestBackgroundTask(SessionFactory sf,
                                   MessageDispatcher dispatcher)
  {
    this.sf = sf;
    this.dispatcher = dispatcher;
  }

  public void run() {
    log.debug("Starting background task");
    Session sess = null;

    try {
      sess = sf.openSession();
      Date now = new Date();

      Query q = sess.createQuery("select t from com.k_int.openrequest.db.ILLTransaction t where t.requestStatusCode.code in (?,?) and t.expiryTimer > ? and t.role = ?");
      q.setParameter(0,"IN-PROCESS",Hibernate.STRING);
      q.setParameter(1,"CONDITIONAL",Hibernate.STRING);
      q.setParameter(2,now,Hibernate.DATE);
      q.setParameter(3,"RESP",Hibernate.STRING);

      for ( Iterator i = q.iterate(); i.hasNext(); ) {
        ILLTransaction trans = (ILLTransaction)i.next();
        log.info("Send expiry for trans#"+trans.getId());
        log.debug("Looking up sender");
        LocationSymbol sender = DBHelper.lookupLocation(sess,trans.getLocation().getDefaultSymbol());
        log.debug("Looking up recipient");
        LocationSymbol recipient = DBHelper.lookupLocation(sess,trans.getPVCurrentPartnerIdSymbol());

        ILL_APDU_type pdu = ISOExpiredMessageFactory.create(sess, trans, null);
        ILLMessageEnvelope e = new ILLMessageEnvelope(pdu, sender.toString(), recipient.toString());
        dispatcher.dispatch("System", "EXPIRYtimeout", e);
      }
    }
    catch (Exception e) {
      log.warn("Problem running expiry query:",e);
    }
    finally
    {
      if ( sess != null )
        try { sess.close(); } catch ( Exception e ) {}
    }

  }
}
