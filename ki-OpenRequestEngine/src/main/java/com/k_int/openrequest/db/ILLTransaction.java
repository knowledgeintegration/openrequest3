package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.api.*;
import org.hibernate.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.k_int.openrequest.helpers.SystemHelper;
import com.k_int.openrequest.util.DBStringHelper;
import org.springframework.context.*;

/**
 * Title:       ILLTransaction
 * @version:    $Id: ILLTransaction.java,v 1.7 2005/07/01 16:11:12 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_ILL_TRANSACTION" dynmic-update="true" dynamic-insert="true" discriminator-value="GENERIC"
 * @hibernate.discriminator column="REQUEST_TYPE" type="string"
 */
public class ILLTransaction {

  private Log log = LogFactory.getLog(this.getClass());

  public static final long SIMPLE_TRANSACTION_TYPE = 1;
  public static final long CHAINED_TRANSACTION_TYPE = 2;
  public static final long PARTITIONED_TRANSACTION_TYPE = 3;

  private Long id;
  private String request_type = null;
  private long transaction_type;
  private boolean retry_flag;
  private boolean forward_flag;
  private String requester_note;
  private String forward_note;
  private MessageHeader initial_message;
  private MessageHeader last_repeatable_message;
  private ILLTransactionGroup transaction_group;
  private StateModel active_state_model;
  protected StatusCode active_status_code;
  protected StatusCode previous_status_code;
  private String role;
  private Location location;
  private List transaction_alerts = new ArrayList();

  // TODO: Add "NeedsAttention" flag

  // Protocol variables
  private boolean pv_return;
  private boolean pv_fwd;
  private boolean pv_part;
  private boolean pv_chain;
  private java.util.Date pv_sequence_time_stamp;
  private java.util.Date pv_repeat_time_stamp;
  private String pv_current_partner_id_symbol;
  private String pv_current_partner_id_name;

  private String responder_symbol;
  private String requester_symbol;
  private String initial_requester_symbol;

  private String tgq;
  private String tq;
  private String stq;

  private String original_service_date;
  private String original_service_time;
  private Date original_service_timestamp;
  private int shipped_service_type;
  private boolean is_live;
  private String accession_number;
  private Set messages = new HashSet();
  private long unread_message_count;
  private long previous_unread_message_count;
  private Date last_message_date;
  private Date expiry_timer;

  private String partner_telecom_service_identifier;
  private String partner_telecom_service_address;

  private String protocol;

  public ILLTransaction() {
    is_live = true;
  }

  public ILLTransaction(String initial_requester_symbol,
                        String requester_symbol,
                        String tgq,
                        String tq,
                        String stq,
                        String role) {

    log.debug("new ILL Transaction tgq="+tgq+" tq="+tq+" stq="+stq+" role="+role);

    this.initial_requester_symbol = DBStringHelper.set(initial_requester_symbol,25);
    this.requester_symbol = DBStringHelper.set(requester_symbol,25);
    this.tgq = DBStringHelper.set(tgq,25);
    this.tq = DBStringHelper.set(tq,25);
    this.stq = DBStringHelper.set(stq,25);
    this.role = DBStringHelper.set(role,10);
  }

  /**
   * @hibernate.id  generator-class="native" column="REQUEST_ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="ILL_ROLE" length="10"
   */
  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = DBStringHelper.set(role,10);
  }

  /**
   * @hibernate.property column="TRANSACTION_TYPE"
   */
  public long getTransactionType() {
    return transaction_type;
  }

  public void setTransactionType(long transaction_type) {
    this.transaction_type = transaction_type;
  }

  public String getRequestType()
  {
    return request_type;
  }

  public void setRequestType(String request_type)
  {
    this.request_type = request_type;
  }

  /**
   * @hibernate.property column="RETRY_FLAG"
   */
  public boolean getRetryFlag() {
    return retry_flag;
  }

  public void setRetryFlag(boolean retry_flag) {
    this.retry_flag = retry_flag;
  }

  /**
   * @hibernate.property column="FORWARD_FLAG"
   */
  public boolean getForwardFlag() {
    return forward_flag;
  }

  public void setForwardFlag(boolean forward_flag) {
    this.forward_flag = forward_flag;
  }

  /**
   * @hibernate.property column="REQUESTER_NOTE" length="2000"
   */
  public String getRequesterNote() {
    return requester_note;
  }

  public void setRequesterNote(String requester_note) {
    this.requester_note = DBStringHelper.set(requester_note,2000);
  }

  /**
   * @hibernate.property column="FORWARD_NOTE" length="2000"
   */
  public String getForwardNote() {
    return forward_note;
  }

  public void setForwardNote(String forward_note) {
    this.forward_note = DBStringHelper.set(forward_note,2000);
  }

  /**
   * @hibernate.many-to-one  column="INITIAL_REQUEST_MESSAGE" class="com.k_int.openrequest.db.MessageHeader" outer-join="false"
   */
  public MessageHeader getInitialRequestMessage() {
    return initial_message;
  }

  public void setInitialRequestMessage(MessageHeader  initial_message) {
    this.initial_message = initial_message;
  }

  /**
   * @hibernate.many-to-one  column="LAST_REPEATABLE_MESSAGE" class="com.k_int.openrequest.db.MessageHeader" outer-join="false"
   */
  public MessageHeader getLastRepeatableMessage() {
    return last_repeatable_message;
  }

  public void setLastRepeatableMessage(MessageHeader last_repeatable_message) {
    this.last_repeatable_message = last_repeatable_message;
  }

  /**
   * @hibernate.many-to-one  column="TRANSACTION_GROUP" class="com.k_int.openrequest.db.ILLTransactionGroup" cascade="save-update" outer-join="false"
   */
  public ILLTransactionGroup getTransactionGroup() {
    return transaction_group;
  }

  public void setTransactionGroup(ILLTransactionGroup transaction_group) {
    this.transaction_group = transaction_group;
  }

  /**
   * @hibernate.many-to-one  column="ACTIVE_STATE_MODEL" class="com.k_int.openrequest.db.StateModel" outer-join="false"
   */
  public StateModel getActiveStateModel() {
    return active_state_model;
  }

  public void setActiveStateModel(StateModel active_state_model) {
    this.active_state_model = active_state_model;
  }

  /**
   * @hibernate.many-to-one  column="REQUEST_STATUS_CODE" class="com.k_int.openrequest.db.StatusCode" outer-join="false"
   */
  public StatusCode getRequestStatusCode() {
    return active_status_code;
  }

  public void setRequestStatusCode(StatusCode active_status_code) {
    this.previous_status_code = this.active_status_code;
    this.active_status_code = active_status_code;
  }

  // Methods implemented for Stateful interface

  public void setStateModel(StateModel model)
  {
    this.active_state_model = model;
  }

  public void setState(StatusCode code)
  {
    this.active_status_code = code;
  }

  public StateModel getCurrentStateModel()
  {
    return active_state_model;
  }

  public StatusCode getCurrentStatusCode()
  {
    return active_status_code;
  }

  /**
   * @hibernate.property column="PV_RETURN"
   */
  public boolean getPVReturn() {
    return pv_return;
  }

  public void setPVReturn(boolean pv_return) {
    this.pv_return = pv_return;
  }
 
  /**
   * @hibernate.property column="PV_FWD"
   */
  public boolean getPVFwd() {
    return pv_fwd;
  }

  public void setPVFwd(boolean pv_fwd) {
    this.pv_fwd = pv_fwd;
  }
 
  /**
   * @hibernate.property column="PV_PART"
   */
  public boolean getPVPart() {
    return pv_part;
  }

  public void setPVPart(boolean pv_part) {
    this.pv_part = pv_part;
  }
 
  /**
   * @hibernate.property column="PV_CHAIN"
   */
  public boolean getPVChain()
  {
    return pv_chain;
  }

  public void setPVChain(boolean pv_chain)
  {
    this.pv_chain = pv_chain;
  }
 
  /**
   * @hibernate.property column="PV_SEQUENCE_TIME_STAMP"
   */
  public java.util.Date getPVSequenceTimeStamp() {
    return pv_sequence_time_stamp;
  }

  public void setPVSequenceTimeStamp(java.util.Date pv_sequence_time_stamp) {
    this.pv_sequence_time_stamp = pv_sequence_time_stamp;
  }
 
  /**
   * @hibernate.property column="PV_REPEAT_TIME_STAMP"
   */
  public java.util.Date getPVRepeatTimeStamp() {
    return pv_repeat_time_stamp;
  }

  public void setPVRepeatTimeStamp(java.util.Date pv_repeat_time_stamp) {
    this.pv_repeat_time_stamp = pv_repeat_time_stamp;
  }

  /**
   * @hibernate.property column="PV_CURRENT_PARTNER_ID_SYMBOL" length="25"
   */
  public String getPVCurrentPartnerIdSymbol() {
    return pv_current_partner_id_symbol;
  }

  public void setPVCurrentPartnerIdSymbol(String pv_current_partner_id_symbol) {
    this.pv_current_partner_id_symbol = DBStringHelper.set(pv_current_partner_id_symbol,25);
  }
 
  /**
   * @hibernate.property column="PV_CURRENT_PARTNER_ID_NAME" length="100"
   */
  public String getPVCurrentPartnerIdName() {
    return pv_current_partner_id_name;
  }

  public void setPVCurrentPartnerIdName(String pv_current_partner_id_name) {
    this.pv_current_partner_id_name = DBStringHelper.set(pv_current_partner_id_name,100);
  }

  /**
   * @hibernate.property column="RESPONDER_SYMBOL" length="25"
   */
  public String getResponderSymbol() {
    return responder_symbol;
  }

  public void setResponderSymbol(String responder_symbol) {
    this.responder_symbol = DBStringHelper.set(responder_symbol,25);
  }

  /**
   * @hibernate.property column="REQUESTER_SYMBOL" length="25"
   */
  public String getRequesterSymbol() {
    return requester_symbol;
  }

  public void setRequesterSymbol(String requester_symbol) {
    this.requester_symbol = DBStringHelper.set(requester_symbol,25);
  }

  /**
   * @hibernate.property column="INITIAL_REQUESTER_SYMBOL" length="25"
   */
  public String getInitialRequesterSymbol() {
    return initial_requester_symbol;
  }

  public void setInitialRequesterSymbol(String initial_requester_symbol) {
    this.initial_requester_symbol = DBStringHelper.set(initial_requester_symbol,25);
  }

  /**
   * @hibernate.property column="TGQ" length="25"
   */
  public String getTGQ() {
    return tgq;
  }

  public void setTGQ(String value) { 
    this.tgq = DBStringHelper.set(value,25);
  }

  /**
   * @hibernate.property column="TQ" length="25"
   */
  public String getTQ() {
    return tq;
  }

  public void setTQ(String value) {
    this.tq = DBStringHelper.set(value,25);
  }

  /**
   * @hibernate.property column="STQ" length="25"
   */
  public String getSTQ() {
    return stq;
  }

  public void setSTQ(String value) {
    this.stq = DBStringHelper.set(value,25);
  }

  /**
   * @hibernate.many-to-one  column="LOCATION" class="com.k_int.openrequest.db.Location.Location" outer-join="false"
   */
  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  /**
   * @hibernate.property column="ORIGINIAL_SERVICE_DATE" length="10"
   */
  public String getOriginalServiceDate() {
    return original_service_date;
  }

  public void setOriginalServiceDate(String value) {
    this.original_service_date = DBStringHelper.set(value,10);
  }

  /**
   * @hibernate.property column="ORIGINIAL_SERVICE_TIME" length="10"
   */
  public String getOriginalServiceTime() {
    return original_service_time;
  }

  public void setOriginalServiceTime(String value) {
    this.original_service_time = DBStringHelper.set(value,10);
  }

  /**
   * @hibernate.property column="ORIGINIAL_SERVICE_TIMESTAMP"
   */
  public Date getOriginalServiceTimestamp() {
    return original_service_timestamp;
  }

  public void setOriginalServiceTimestamp(Date value) {
    this.original_service_timestamp = value;
  }

  /**
   * @hibernate.property column="SHIPPED_SERVICE_TYPE"
   */
  public int getShippedServiceType() {
    return shipped_service_type;
  }

  public void setShippedServiceType(int shipped_service_type) {
    this.shipped_service_type = shipped_service_type;
  }

  /**
   * @hibernate.property column="IS_LIVE"
   */
  public boolean getIsLive() {
    return is_live;
  }

  public void setIsLive(boolean is_live) {
    this.is_live=is_live;
  }

  public String toString()
  {
    return "ILLTransaction id:"+getId()+
           ", owner loc:"+getLocation().getName()+
           ", active state model:"+getActiveStateModel().getName()+
           ", state:"+getRequestStatusCode().getDescription()+
           ", isLive:"+getIsLive();
  }
 

  /**
   * @hibernate.set cascade="all" lazy="true" table="OR_MESSAGE_HEADER" order-by="MESSAGE_ID ASC"
   * @hibernate.collection-key column="PARENT_REQUEST"
   * @hibernate.collection-one-to-many class="com.k_int.openrequest.db.MessageHeader"
   */
  public Set getMessages() {
    return messages;
  }

  public void setMessages(Set messages) {
    this.messages = messages;
  }

  public List getTransactionAlerts()
  {
    return transaction_alerts;
  }

  public void setTransactionAlerts(List transaction_alerts)
  {
    this.transaction_alerts = transaction_alerts;
  }

  /**
   * @hibernate.property column="UNREAD_MESSAGE_COUNT"
   */
  public long getUnreadMessageCount() {
    return unread_message_count;
  }

  public void setUnreadMessageCount(long unread_message_count) {
    this.previous_unread_message_count = this.unread_message_count;
    this.unread_message_count = unread_message_count;
  }

  /**
   * @hibernate.property column="LAST_MESSAGE_DATE"
   */
  public Date getLastMessageDate() {
    return last_message_date;
  }

  public void setLastMessageDate(Date last_message_date) {
    this.last_message_date = last_message_date;
  }

  // Non property methods....
  public void incrementUnreadMessageCount()
  {
    unread_message_count++;
    setUnreadMessageCount(unread_message_count);
  }

  public void decrementUnreadMessageCounter()
  {
    unread_message_count--;
    setUnreadMessageCount(unread_message_count);
  }

  /**
   * @hibernate.property column="PROTOCOL" length="20"
   */
  public String getProtocol() {
    return protocol;
  }

  public void setProtocol(String protocol) {
    this.protocol = DBStringHelper.set(protocol,20);
  }

  /**
   * @hibernate.property column="EXPIRY_TIMER"
   */
  public Date getExpiryTimer() {
    return expiry_timer;
  }

  public void setExpiryTimer(Date expiry_timer) {
    this.expiry_timer = expiry_timer;
  }

  /**
   * @hibernate.property column="PARTNER_TCI" length="20"
   */
  public String getPartnerTelecomServiceIdentifier() {
    return partner_telecom_service_identifier;
  }

  public void setPartnerTelecomServiceIdentifier(String partner_telecom_service_identifier) {
    this.partner_telecom_service_identifier = DBStringHelper.set(partner_telecom_service_identifier,20);
  }

  /**
   * @hibernate.property column="PARTNER_TCA" length="100"
   */
  public String getPartnerTelecomServiceAddress() {
    return partner_telecom_service_address;
  }

  public void setPartnerTelecomServiceAddress(String partner_telecom_service_address) {
    this.partner_telecom_service_address = DBStringHelper.set(partner_telecom_service_address,100);
  }

  private void checkForChanges(ApplicationContext ctx) {
    log.debug("Comparing "+this.active_status_code+" and "+this.previous_status_code );

    if ( this.active_status_code != this.previous_status_code ) {
      Map vars = new HashMap();
      vars.put("role",role);
      vars.put("type","TRANS_CHANGE");

      if ( active_status_code != null )
        vars.put("trans_state", active_status_code.getCode());
      else
        vars.put("trans_state", "NULL");

      if ( previous_status_code != null )
        vars.put("old_trans_state", previous_status_code.getCode());
      else
        vars.put("old_trans_state", "NULL");

      if ( transaction_group.active_status_code != null )
        vars.put("tg_state", transaction_group.active_status_code.getCode());
      else
        vars.put("tg_state", "NULL");

      if ( transaction_group.previous_status_code != null )
        vars.put("old_tg_state", transaction_group.previous_status_code.getCode());
      else
        vars.put("old_tg_state", "NULL");

      vars.put("tg_unread", new Integer((int)unread_message_count));
      vars.put("old_tg_unread", new Integer((int)previous_unread_message_count));

      SystemHelper.announceTGChanged(ctx, vars, this.transaction_group);

      this.previous_status_code = this.active_status_code;
      this.previous_unread_message_count = this.unread_message_count;
    }
  }

  public void notifyLoad(ApplicationContext ctx) {
    previous_status_code = active_status_code;
    previous_unread_message_count = unread_message_count;
    log.debug("notifyLoad() previous_status_code="+previous_status_code+", previous_unread_message_count="+previous_unread_message_count);
    checkForChanges(ctx);
  }

  public void notifyPreUpdate(ApplicationContext ctx) {
    log.debug("notifyPreUpdate()");
  }


}
