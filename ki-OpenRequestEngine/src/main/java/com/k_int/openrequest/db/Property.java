/**
 * Title:       Property
 * @version:    $Id: Property.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.db;

import com.k_int.openrequest.db.Location.*;

public class Property
{
  private String property_name;
  private String property_value;

  public Property(String property_name, String property_value)
  {
    this.property_name = property_name;
    this.property_value = property_value;
  }

  public Property()
  {
  }

  public String getPropName()
  {
    return property_name;
  }

  public void setPropertyName(String property_name)
  {
    this.property_name = property_name;
  }

  public String getValue()
  {
    return property_value;
  }

  public void setValue(String property_value)
  {
    this.property_value=property_value;
  }
}
