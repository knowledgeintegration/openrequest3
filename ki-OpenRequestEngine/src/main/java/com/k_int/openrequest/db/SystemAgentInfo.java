package com.k_int.openrequest.db;

import java.util.Map;
import java.util.HashMap;

/**
 * Title:       SystemAgentInfo
 * @version:    $Id: SystemAgentInfo.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_SYSTEM_AGENT_INFO" dynmic-update="true" dynamic-insert="true"
 */
public class SystemAgentInfo {

  private Long id;
  private String desc;
  private String code;
  private String class_name;
  private String status;
  private String service_address;
  private int priority;

  private Map properties = new HashMap();

  public SystemAgentInfo() {
  }

  public SystemAgentInfo(String desc, String code, String class_name, String service_address, int priority) {
    this.desc = desc;
    this.code = code;
    this.class_name = class_name;
    this.service_address = service_address;
    this.priority = priority;
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="CODE" length="20"
   */
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  /**
   * @hibernate.property column="CLASS_NAME" length="100"
   */
  public String getClassName() {
    return class_name;
  }

  public void setClassName(String class_name) {
    this.class_name = class_name;
  }

  /**
   * @hibernate.property column="DESCRIPTION" length="100"
   */
  public String getDescription() {
    return desc;
  }

  public void setDescription(String desc) {
    this.desc = desc;
  }

  /**
   * @hibernate.property column="STATUS" length="10"
   */
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @hibernate.property column="SERVICE_ADDRESS" length="100"
   */
  public String getServiceAddress() {
    return service_address;
  }

  public void setServiceAddress(String service_address) {
    this.service_address = service_address;
  }

  /**
   * @hibernate.map table="OR_SYSTEM_AGENT_PROPERTIES" lazy="true" cascade="all"
   * @hibernate.collection-key  column="AGENT_ID"
   * @hibernate.collection-index  column="PROP_NAME" type="java.lang.String" length="50"
   * @hibernate.collection-element type="java.lang.String" column="PROP_VALUE"
   */
  public Map getProperties() {
    return properties;
  }

  public void setProperties(Map properties) {
    this.properties = properties;
  }

  /**
   * @hibernate.property column="SVC_PRIORITY"
   */
  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }
}
