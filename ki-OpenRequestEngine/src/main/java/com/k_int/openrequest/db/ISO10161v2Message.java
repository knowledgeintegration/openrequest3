/**
 * Title:       ISO10161v2Message
 * @version:    $Id: ISO10161v2Message.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.db;

import com.k_int.openrequest.util.DBStringHelper;

/**
  * @author Ian Ibbotson
  * @since 1.0
  * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2"
  */
public class ISO10161v2Message extends MessageHeader {
}
