package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161ReceivedMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:REC"
 */
public class ISO10161ReceivedMessageDetails extends ISO10161v2Message {

  private long person_or_institution_symbol_type;
  private String person_or_institution_symbol;
  private long name_of_person_or_institution_type;
  private String name_of_person_or_institution;
  private Date date_received;
  private Long shipped_service_type;

  /**
   * @hibernate.property column="PERS_OR_INST_SYMBOL_TYPE"
   */
  public long getPersonOrInstitutionSymbolType() {
    return person_or_institution_symbol_type;
  }

  public void setPersonOrInstitutionSymbolType(long person_or_institution_symbol_type) {
    this.person_or_institution_symbol_type = person_or_institution_symbol_type;
  }

  /**
   * @hibernate.property column="PERS_OR_INST_SYMBOL" length="100"
   */
  public String getPersonOrInstitutionSymbol() {
    return person_or_institution_symbol;
  }

  public void setPersonOrInstitutionSymbol(String person_or_institution_symbol) {
    this.person_or_institution_symbol = DBStringHelper.set(person_or_institution_symbol,100);
  }

  /**
   * @hibernate.property column="NAME_OF_PERS_OR_INST_TYPE"
   */
  public long getNameOfPersonOrInstitutionType() {
    return name_of_person_or_institution_type;
  }

  public void setNameOfPersonOrInstitutionType(long name_of_person_or_institution_type) {
    this.name_of_person_or_institution_type = name_of_person_or_institution_type;
  }

  /**
   * @hibernate.property column="NAME_OF_PERS_OR_INST" length="100"
   */
  public String getNameOfPersonOrInstitution() {
    return name_of_person_or_institution;
  }

  public void setNameOfPersonOrInstitution(String name_of_person_or_institution) {
    this.name_of_person_or_institution = DBStringHelper.set(name_of_person_or_institution,100);
  }

  /**
   * @hibernate.property column="STR_DATE_RECEIVED"
   */
  public Date getDateReceived() {
    return date_received;
  }

  public void setDateReceived(Date date_received) {
    this.date_received = date_received;
  }

  /**
   * @hibernate.property column="SHIPPED_SERVICE_TYPE"
   */
  public Long getShippedServiceType() {
    return shipped_service_type;
  }

  public void setShippedServiceType(Long shipped_service_type) {
    this.shipped_service_type = shipped_service_type;
  }
}
