package com.k_int.openrequest.db;

/**
 * Title:       StatusCode
 * @version:    $Id: StateTransition.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_STATE_TRANSITION" dynmic-update="true" dynamic-insert="true"
 */
public class StateTransition {

  private Long id;
  private String desc;
  private String predicate;
  private StatusCode from_state;
  private StatusCode to_state;
  private StateModel model;
  private String transition_code;
  private int transition_action_type;

  private String action_handler_type;
  private String handler_params;
  private String message_type;
  private String message_params;
  private Boolean user_selectable;

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="DESCRIPTION" length="30"
   */
  public String getDescription() {
    return desc;
  }

  public void setDescription(String desc) {
    this.desc = desc;
  }

  /**
   * @hibernate.property column="PREDICATE" length="100"
   */
  public String getPredicate() {
    return predicate;
  }

  public void setPredicate(String predicate) {
    this.predicate = predicate;
  }

  /**
   * @hibernate.many-to-one  column="FROM_STATE" class="com.k_int.openrequest.db.StatusCode"
   */
  public StatusCode getFrom_state() {
    return from_state;
  }

  public void setFrom_state(StatusCode from_state) {
    this.from_state=from_state;
  }

  /**
   * @hibernate.many-to-one  column="TO_STATE" class="com.k_int.openrequest.db.StatusCode"
   */
  public StatusCode getTo_state() {
    return to_state;
  }

  public void setTo_state(StatusCode to_state) {
    this.to_state=to_state;
  }

  /**
   * @hibernate.many-to-one  column="MODEL" class="com.k_int.openrequest.db.StateModel" cascade="all"
   */
  public StateModel getModel() {
    return model;
  }

  public void setModel(StateModel model) {
    this.model=model;
  }

  public String getMessageType()
  {
    return message_type;
  }
 
  public void setMessageType(String message_type)
  {
    this.message_type = message_type;
  }

  public String getMessageParams() {
    return message_params;
  }

  public void setMessageParams(String message_params) {
    this.message_params = message_params;
  }

  /**
   * @hibernate.property column="USER_SELECTABLE"
   */
  public Boolean getUserSelectable() {
    return user_selectable;
  }

  public void setUserSelectable(Boolean user_selectable) {
    this.user_selectable = user_selectable;
  }


  public String getActionHandlerType() {
    return action_handler_type;
  }
 
  public void setActionHandlerType(String action_handler_type) {
    this.action_handler_type = action_handler_type;
  }

  public String getActionHandlerParams() {
    return handler_params;
  }

  public void setActionHandlerParams(String handler_params) {
    this.handler_params = handler_params;
  }

  /**
   * @hibernate.property column="TRANSITION_CODE" length="15"
   */
  public String getTransitionCode() {
    return transition_code;
  }

  public void setTransitionCode(String transition_code) {
    this.transition_code = transition_code;
  }

  /** 
   * Return the "type" of action associated with this transiton, 0=action, 1=indication
   * @hibernate.property column="TRANSITION_ACTION_TYPE"
   */
  public int getTransitionActionType() {
    return this.transition_action_type;
  }

  /** 
   * set the "type" of action associated with this transiton, 0=action, 1=indication
   */
  public void setTransitionActionType(int transition_action_type) {
    this.transition_action_type = transition_action_type;
  }
}
