
package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;

/**
 * Title:       Extension
 * @version:    $Id: SupplementalClientDetailsExtension.java,v 1.4 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynmic-update="true" dynamic-insert="true" discriminator-value="SUPP_CLI_INFO"
 */
public class SupplementalClientDetailsExtension extends Extension {

  private String patron_name;
  private String notes;
  
  public SupplementalClientDetailsExtension() {
  }

  /**
   * @hibernate.property
   * @hibernate.column name="PATRON_NAME" size=50
   */
  public String getPatronName() {
    return patron_name;
  }

  public void setPatronName(String patron_name) {
    this.patron_name = DBStringHelper.set(patron_name,50);
  }

  /**
   * @hibernate.property
   * @hibernate.column name="NOTES" size=2000
   */
  public String getNotes() {
    return patron_name;
  }

  public void setNotes(String notes) {
    this.notes = DBStringHelper.set(notes,2000);
  }
}
