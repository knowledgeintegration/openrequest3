package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161StatusOrErrorReportMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:SOR"
 */
public class ISO10161StatusOrErrorReportMessageDetails extends ISO10161v2Message {

  private Long reason_no_report;
  private Long provider_status_report;
  private String correlation_info;
  private Long report_source;
  private int user_error_report_type;
  private int provider_error_report_type;

  /**
   * @hibernate.property column="REASON_NO_REPORT"
   */
  public Long getReasonNoReport() {
    return reason_no_report;
  }

  public void setReasonNoReport(Long reason_no_report) {
    this.reason_no_report = reason_no_report;
  }

  /**
   * @hibernate.property column="CORRELATION_INFO" length="100"
   */
  public String getCorrelationInfo() {
    return correlation_info;
  }

  public void setCorrelationInfo(String correlation_info) {
    this.correlation_info = DBStringHelper.set(correlation_info,100);
  }

  /**
   * @hibernate.property column="REPORT_SOURCE"
   */
  public Long getReportSource() {
    return report_source;
  }

  public void setReportSource(Long report_source) {
    this.report_source = report_source;
  }

  /**
   * @hibernate.property column="USER_ERROR_REPORT_TYPE"
   */
  public int getUserErrorReportType() {
    return user_error_report_type;
  }

  public void setUserErrorReportType(int user_error_report_type) {
    this.user_error_report_type = user_error_report_type;
  }

  /**
   * @hibernate.property column="PROVIDER_ERROR_REPORT_TYPE"
   */
  public int getProviderErrorReportType() {
    return provider_error_report_type;
  }

  public void setProviderErrorReportType(int provider_error_report_type) {
    this.provider_error_report_type = provider_error_report_type;
  }

  /**
   * @hibernate.property column="PROVIDER_STATUS_REPORT"
   */
  public Long getProviderStatusReport() {
    return provider_status_report;
  }

  public void setProviderStatusReport(Long provider_status_report) {
    this.provider_status_report = provider_status_report;
  }
}
