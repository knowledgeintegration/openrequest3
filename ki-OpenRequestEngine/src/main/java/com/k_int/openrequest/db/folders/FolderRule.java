package com.k_int.openrequest.db.folders;

/**
 * Title:       FolderRule
 * @version:    $Id: FolderRule.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_FOLDER_RULE" dynmic-update="true" dynamic-insert="true" discriminator-value="BASE" lazy="false"
 * @hibernate.discriminator column="VAR_TYPE" type="string" length="25"
 */
public class FolderRule {

  private long id;
  private String desc;
  private String code;
  private String scope_type;
  private String target_type;
  private String target_id;

  public FolderRule() {
  }

  public FolderRule(String desc, String code, String scope_type, String target_type, String target_id) {
    this.desc = desc;
    this.code = code;
    this.scope_type = scope_type;
    this.target_type = target_type;
    this.target_id = target_id;
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="CODE" length="20"
   */
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  /**
   * @hibernate.property column="DESCRIPTION" length="200"
   */
  public String getDescription() {
    return desc;
  }

  public void setDescription(String desc) {
    this.desc = desc;
  }

  /**
   * @hibernate.property column="SCOPE_TYPE" length="20"
   * Current possible values: "Global", "CURRENT_LOC"
   */
  public String getScopeType() {
     return scope_type;
  }

  public void setScopeType(String scope_type) {
    this.scope_type = scope_type;
  }

  /**
   * @hibernate.property column="TARGET_TYPE" length="20"
   * 1==SYSTEM owned folder identified by code in t-id, 
   * 2==CURRENT_LOC folder idenfied by code in target id
   * 3==SPECIFIC folder identified by id in
   */
  public String getTargetType() {
     return target_type;
  }

  public void setTargetType(String target_type) {
    this.target_type = target_type;
  }

  /**
   * @hibernate.property column="TARGET_ID" length="20"
   * 1==SYSTEM, 2==CURRENT_LOC, 3=PERSON 
   */
  public String getTargetId() {
     return target_id;
  }

  public void setTargetId(String target_id) {
    this.target_id = target_id;
  }
}
