/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161DamagedMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:DMG"
 */

package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


public class ISO10161DamagedMessageDetails extends ISO10161v2Message {

  private int[] document_type_id;
  private int damaged_portion;
  private int[] damaged_sections;

  /**
   * @hibernate.property column="DOCUMENT_TYPE_ID"
   */
  public int[] getDocumentTypeId() {
    return document_type_id;
  }

  public void setDocumentTypeId(int[] document_type_id) {
    this.document_type_id = document_type_id;
  }

  /**
   * @hibernate.property column="DAMAGED_PORTION"
   */
  public int[] getDamagedSections() {
    return damaged_sections;
  }

  public void setDamagedSections(int[] damaged_sections) {
    this.damaged_sections = damaged_sections;
  }

  /**
   * @hibernate.property column="DAMAGED_SECTIONS"
   */
  public int getDamagedPortion() {
    return damaged_portion;
  }

  public void setDamagedPortion(int damaged_portion) {
    this.damaged_portion = damaged_portion;
  }
}
