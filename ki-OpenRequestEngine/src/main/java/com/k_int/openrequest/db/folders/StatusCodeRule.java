package com.k_int.openrequest.db.folders;

/**
 * Title:       StatusCodeRule
 * @version:    $Id: StatusCodeRule.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="STATECODE" lazy="false"
 */
public class StatusCodeRule extends FolderRule {



  private String role;
  private String state_model_to_monitor;
  private String state_code_to_monitor;

  public StatusCodeRule() {
  }

  public StatusCodeRule(String desc, 
                        String code, 
                        String scope_type, 
                        String target_type, 
                        String target_id,
                        String role,
                        String state_model_to_monitor,
                        String state_code_to_monitor) {
    super(desc,code,scope_type,target_type,target_id);
    this.role=role;
    this.state_model_to_monitor = state_model_to_monitor;
    this.state_code_to_monitor = state_code_to_monitor;
  }

  /**
   * @hibernate.property column="FOLDER_ROLE" length="20"
   * Which role does this rule apply to ? 
   */
  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  /**
   * @hibernate.property column="STATE_MODEL" length="20"
   * Here we identify what thing we are actually checking, for example we
   * might only be interested in state changes to the Currently live transaction in
   * a transaction group, or we may only want to track state changes to a TransactionGroup
   */
  public String getStateModelToMonitor() {
    return state_model_to_monitor;
  }

  public void setStateModelToMonitor(String state_model_to_monitor) {
    this.state_model_to_monitor = state_model_to_monitor;
  }

  /**
   * @hibernate.property column="STATE_CODE" length="20"
   * When the stats monitor sees a state change for an object moving into the
   * identified state, increment the counter, when from the indicated state, decrement
   */
  public String getStateCodeToMonitor() {
    return state_code_to_monitor;
  }

  public void setStateCodeToMonitor(String state_code_to_monitor) {
    this.state_code_to_monitor = state_code_to_monitor;
  }
}
