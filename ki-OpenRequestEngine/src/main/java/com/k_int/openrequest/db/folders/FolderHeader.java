package com.k_int.openrequest.db.folders;

import java.util.*;

/**
 * Title:       FolderHeader
 * @version:    $Id: FolderHeader.java,v 1.2 2005/05/12 15:15:41 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_FOLDER_HEADER" dynmic-update="true" dynamic-insert="true"
 */
public class FolderHeader {

  private long id;
  private String desc;
  private String owner_type;
  private String owner_id;
  private String code;
  private long request_count;

  private Set folder_items = new HashSet();

  public FolderHeader() {
  }

  public FolderHeader(String desc, String owner_type, String owner_id, String code) {
    this.desc=desc;
    this.owner_type=owner_type;
    this.owner_id=owner_id;
    this.code=code;
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="DESCRIPTION" length="200"
   */
  public String getDescription() {
    return desc;
  }

  public void setDescription(String desc) {
    this.desc = desc;
  }

  /**
   * @hibernate.property column="OWNER_TYPE" length="20"
   */
  public String getOwnerType() {
    return owner_type;
  }

  public void setOwnerType(String owner_type) {
    this.owner_type = owner_type;
  }

  /**
   * @hibernate.property column="OWNER_ID" length="20"
   */
  public String getOwnerId() {
    return owner_id;
  }

  public void setOwnerId(String owner_id) {
    this.owner_id = owner_id;
  }

  /**
   * @hibernate.property column="CODE" length="20"
   */
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  /**
   * @hibernate.set cascade="none" lazy="true" table="OR_FOLDER_ITEM"
   * @hibernate.collection-key column="FOLDER_ID"
   * @hibernate.collection-many-to-many class="com.k_int.openrequest.db.ILLTransactionGroup" column="TRANS_ID" cascade="none"
   */
  public Set getFolderItems() {
    return folder_items;
  }

  public void setFolderItems(Set folder_items) {
    this.folder_items = folder_items;
  }

  /**
   * @hibernate.property column="REQUEST_COUNT"
   */
  public long getRequestCount() {
    return request_count;
  }

  public void setRequestCount(long request_count) {
    this.request_count = request_count;
  }

  public static FolderHeader lookupOrCreateFolder(org.hibernate.Session session,
                                                  String loc,
                                                  String folder_code) throws java.sql.SQLException,
                                                                             org.hibernate.HibernateException
  {
    // log.debug("locate folder for loc="+loc+" cold="+folder_code);

    FolderHeader retval = null;
    Object[] values = { loc, folder_code };
    org.hibernate.type.Type[] types = { org.hibernate.Hibernate.STRING, org.hibernate.Hibernate.STRING };

    org.hibernate.Query q = session.createQuery("from folder in class com.k_int.openrequest.db.folders.FolderHeader where folder.ownerId=? and folder.code=?");
    q.setParameter(0,loc,org.hibernate.Hibernate.STRING);
    q.setParameter(1,folder_code,org.hibernate.Hibernate.STRING);
    List matching_folders = q.list();

    if ( matching_folders.size() > 0 ) {
      retval = (FolderHeader) matching_folders.get(0);
    }
    else {
      retval = new FolderHeader("An automated folder","LOCATION",loc,folder_code);
      session.save(retval);
    }

    return retval;
  }

}
