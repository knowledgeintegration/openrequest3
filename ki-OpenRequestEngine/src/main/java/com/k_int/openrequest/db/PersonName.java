package com.k_int.openrequest.db;

/**
 * Title:       PersonSymbol
 * @version:    $Id: PersonName.java,v 1.2 2005/06/17 18:52:15 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 * @hibernate.subclass discriminator-value="PERS_NAME"
 */
public class PersonName extends PartyName implements java.io.Serializable {
  public PersonName(String name) {
    this.name = name;
  }
}
