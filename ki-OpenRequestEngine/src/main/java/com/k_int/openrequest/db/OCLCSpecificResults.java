
package com.k_int.openrequest.db;

import java.util.*;

/**
 * Title:       Extension
 * @version:    $Id: OCLCSpecificResults.java,v 1.2 2005/07/01 08:59:11 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynmic-update="true" dynamic-insert="true" discriminator-value="OCLC:SR"
 */
public class OCLCSpecificResults extends Extension {

  public OCLCSpecificResults() {
  }
}
