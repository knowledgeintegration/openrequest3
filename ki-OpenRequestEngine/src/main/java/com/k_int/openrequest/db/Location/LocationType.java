package com.k_int.openrequest.db.Location;

import java.util.*;

/**
 * Title:       LocationType
 * @version:    $Id: LocationType.java,v 1.2 2005/04/07 22:24:14 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_LOCATION_TYPE" dynmic-update="true" dynamic-insert="true"
 */
public class LocationType {

  private Long id;
  private String code;
  private String description;
  private Map event_handlers = new HashMap();

  public LocationType() {
  }

  public LocationType(String code) {
    this.code = code;
  }

  public LocationType(String code, String description) {
    this.code = code;
    this.description = description;
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="LOCATION_TYPE" length="10"
   */
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  /**
   * @hibernate.property column="DESCRIPTION" length="40"
   */
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Map getLocationEventHandlers() {
    return event_handlers;
  }

  public void setLocationEventHandlers(Map event_handlers) {
    this.event_handlers = event_handlers;
  }
}
