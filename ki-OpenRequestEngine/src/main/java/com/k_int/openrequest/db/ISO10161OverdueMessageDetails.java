package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161OverdueMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:ODUE"
 */
public class ISO10161OverdueMessageDetails extends ISO10161v2Message {

  private Date date_due;
  private Boolean renewable;

  /**
   * @hibernate.property column="DATE_DUE"
   */
  public Date getDateDue() {
    return date_due;
  }

  public void setDateDue(Date date_due) {
    this.date_due = date_due;
  }

  /**
   * @hibernate.property column="RENEWABLE"
   */
  public Boolean getRenewable() {
    return renewable;
  }

  public void setRenewable(Boolean renewable) {
    this.renewable = renewable;
  }
}
