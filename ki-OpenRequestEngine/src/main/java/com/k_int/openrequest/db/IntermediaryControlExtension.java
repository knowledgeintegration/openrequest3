
package com.k_int.openrequest.db;

import java.util.*;

/**
 * Title:       Extension
 * @version:    $Id: IntermediaryControlExtension.java,v 1.1 2005/02/25 16:10:00 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_MESSAGE_EXTENSION" dynmic-update="true" dynamic-insert="true" discriminator-value="ICE"
 */
public class IntermediaryControlExtension extends Extension {

  public IntermediaryControlExtension() {
  }
}
