package com.k_int.openrequest.db;

/**
 * Title:       MessageQueueItem
 * @version:    $Id: MessageQueueItem.java,v 1.2 2005/06/30 12:33:40 ibbo Exp $
 * Copyright:   Copyright (C) 2005 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_MESSAGE_QUEUE" dynmic-update="true" dynamic-insert="true"
 */
public class MessageQueueItem {

  private Long id;
  private java.util.Date date_added;
  private java.util.Date last_try;
  private String queue_name;
  private String destination;
  private String note;
  private java.io.Serializable message;

  public MessageQueueItem() {
  }

  public MessageQueueItem(String queue_name,
                          String destination,
                          java.io.Serializable message) {
    this.queue_name = queue_name;
    this.destination = destination;
    this.message = message;
    this.date_added = new java.util.Date();
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="DATE_ADDED"
   */
  public java.util.Date getDateAdded() {
    return date_added;
  }

  public void setDateAdded(java.util.Date date_added) {
    this.date_added = date_added;
  }

  /**
   * @hibernate.property column="LAST_TRY"
   */
  public java.util.Date getLastTry() {
    return last_try;
  }

  public void setLastTry(java.util.Date last_try) {
    this.last_try = last_try;
  }

  /**
   * @hibernate.property
   * @hibernate.column name="QUEUE_NAME" length="40"
   */
  public String getQueueName() {
    return queue_name;
  }

  public void setQueueName(String queue_name) {
    this.queue_name = queue_name;
  }

  /**
   * @hibernate.property
   * @hibernate.column name="DESTINATION" length="500"
   */
  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  /**
   * @hibernate.property 
   * @hibernate.column name="NOTE" length="500"
   */
  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  /**
   * trying... hibernate.property type="blob"
   * @hibernate.property
   * @hibernate.column name="MESSAGE" length="32600"
   * used to have additional length="32700"
   */
  public java.io.Serializable getMessage() {
    return message;
  }

  public void setMessage(java.io.Serializable message) {
    this.message = message;
  }
}
