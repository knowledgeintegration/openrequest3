package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161CheckedInMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:CHK"
 */
public class ISO10161CheckedInMessageDetails extends ISO10161v2Message
{
  private Date date_checked_in;

  /**
   * @hibernate.property column="DATE_CHECKED_IN"
   */
  public Date getDateCheckedIn() {
    return date_checked_in;
  }

  public void setDateCheckedIn(Date date_checked_in) {
    this.date_checked_in = date_checked_in;
  }
}
