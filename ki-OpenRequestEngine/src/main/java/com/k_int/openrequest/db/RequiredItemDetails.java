package com.k_int.openrequest.db;

import java.util.Vector;
import java.util.*;
import java.math.BigInteger;
import com.k_int.openrequest.util.DBStringHelper;

/**
 * Title:       
 * @version:    $Id: RequiredItemDetails.java,v 1.7 2005/07/02 07:24:43 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_REQUIRED_ITEM_DETAILS" dynmic-update="true" dynamic-insert="true"
 */
public class RequiredItemDetails implements Cloneable {

  public static transient BigInteger ST_LOAN=BigInteger.valueOf(1);
  public static transient BigInteger ST_COPY_NON_RETURNABLE=BigInteger.valueOf(2);
  public static transient BigInteger ST_LOCATIONS=BigInteger.valueOf(3);
  public static transient BigInteger ST_ESTIMATE=BigInteger.valueOf(4);
  public static transient BigInteger ST_RESPONDER_SPECIFIC=BigInteger.valueOf(5);

  public static transient long PHYSICAL_DELIVERY=0;
  public static transient long ELECTRONIC_DELIVERY=1;

  public static transient long IT_MONOGRAPH=1;
  public static transient long IT_SERIAL=2;
  public static transient long IT_OTHER=3;

  public static transient Hashtable service_codes = null;

  public static final transient String DELIVERY_SERVICE_TYPE_PHYSICAL = "PHYSICAL";
  public static final transient String DELIVERY_SERVICE_TYPE_ELECTRONIC = "ELECTRONIC";

  private Long id;
  private List service_types = new ArrayList();

  private String level_of_service;
  private Date need_before_date;
  private long expiry_flag;
  private Date expiry_date;
  private List supply_medium_info_type = new ArrayList();
  private long place_on_hold;
  private String client_name;
  private String client_status;
  private String client_identifier;
  private long delivery_service_type;
  private String transportation_mode;
  private String telecom_service_identifier;
  private String telecom_service_address;

  private long item_type;
  private long held_medium_type;
  private String call_number;
  private String author;
  private String title;
  private String subtitle;
  private String sponsoring_body;
  private String place_of_publication;
  private String publisher;
  private String series_title_number;
  private String volume;
  private String issue;
  private String edition;
  private String publication_date;
  private String publication_date_of_component;
  private String author_of_article;
  private String title_of_article;
  private String pagination;
  private String isbn;
  private String issn;
  private String additional_no_letters;
  private String verification_reference_source;
  private String account_number;
  private String max_cost_currency_code;
  private String max_cost_string;
  private boolean reciprocal_agreement;
  private boolean will_pay_fee;
  private boolean payment_provided;
  private String copyright_compliance;
  private String accession_number;
  private String shelfmark;
  private String local_notes;
  private String language;

  private IPIGSystemNumber sysno;
  private IPIGSystemNumber natbib;

  public String uniform_title = null;
  public String dissertation = null;

  private String e_delivery_description;
  private String e_delivery_name_or_code;
  private String e_delivery_time;
  private int e_delivery_dets_type;
  private String e_delivery_dets_id;
  private String e_delivery_dets_addr;
  private String source;

  public void RequiredItemDetails() {
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.list cascade="all" lazy="true" table="OR_SERVICE_TYPES"
   * @hibernate.collection-key column="REQUEST_MESSAGE_ID"
   * @hibernate.collection-index column="IDX_POSITION"
   * @hibernate.collection-element column="SERVICE_TYPE" type="java.lang.Integer"
   */
  public List getService_types() {
    return service_types;
  }

  public void setService_types(List service_types) {
    this.service_types = service_types;
  }

  /**
   * @hibernate.property column="LEVEL_OF_SERVICE" length="20"
   */
  public String getLevelOfService() {
    return level_of_service;
  }

  public void setLevelOfService(String level_of_service) {
    this.level_of_service = DBStringHelper.set(level_of_service,20);
  }

  /**
   * @hibernate.property column="NEED_BEFORE_DATE"
   */
  public Date getNeedBeforeDate() {
    return need_before_date;
  }

  public void setNeedBeforeDate(Date need_before_date) {
    this.need_before_date = need_before_date;
  }

  /**
   * @hibernate.property column="EXPIRY_FLAG"
   */
  public long getExpiryFlag() {
    return expiry_flag;
  }

  public void setExpiryFlag(long expiry_flag) {
    this.expiry_flag = expiry_flag;
  }

  /**
   * @hibernate.property column="EXPIRY_DATE"
   */
  public Date getExpiryDate() {
    return expiry_date;
  }

  public void setExpiryDate(Date expiry_date) {
    this.expiry_date = expiry_date;
  }

  public List getSupplyMediumInfoType() {
    return supply_medium_info_type;
  }

  public void setSupplyMediumInfoType(List supply_medium_info_type) {
    this.supply_medium_info_type = supply_medium_info_type;
  }

  /**
   * @hibernate.property column="PLACE_ON_HOLD"
   */
  public long getPlaceOnHold() {
    return place_on_hold;
  }

  public void setPlaceOnHold(long place_on_hold) {
    this.place_on_hold = place_on_hold;
  }

  /**
   * @hibernate.property column="CLIENT_NAME" length="50"
   */
  public String getClientName() {
    return client_name;
  }

  public void setClientName(String client_name) {
    this.client_name = DBStringHelper.set(client_name,50);
  }

  /**
   * @hibernate.property column="CLIENT_STATUS" length="20"
   */
  public String getClientStatus() {
    return client_status;
  }

  public void setClientStatus(String client_status) {
    this.client_status = DBStringHelper.set(client_status,20);
  }


  /**
   * @hibernate.property column="CLIENT_IDENTIFIER" length="50"
   */
  public String getClientIdentifier() {
    return client_identifier;
  }

  public void setClientIdentifier(String client_identifier) {
    this.client_identifier = DBStringHelper.set(client_identifier,50);
  }

  /**
   * @hibernate.property column="DELIVERY_SERVICE_TYPE"
   */
  public long getDeliveryServiceType() {
    return delivery_service_type;
  }

  public void setDeliveryServiceType(long delivery_service_type) {
    this.delivery_service_type = delivery_service_type;
  }

  /**
   * @hibernate.property column="TRANSPORTATION_MODE" length="50"
   * Physical or electronic
   * If physical, set transportation mode
   */
  public String getTransportationMode() {
    return transportation_mode;
  }

  public void setTransportationMode(String transportation_mode) {
    this.transportation_mode = DBStringHelper.set(transportation_mode,50);
  }

 /**
   * @hibernate.property column="TELECOM_SERVICE_IDENTIFIER" length="15"
   */
  public String getTelecomServiceIdentifier() {
    return telecom_service_identifier;
  }

  public void setTelecomServiceIdentifier(String telecom_service_identifier) {
    this.telecom_service_identifier = DBStringHelper.set(telecom_service_identifier,15);
  }

 /**
   * @hibernate.property column="TELECOM_SERVICE_ADDRESS" length="50"
   */
  public String getTelecomServiceAddress() {
    return telecom_service_address;
  }

  public void setTelecomServiceAddress(String telecom_service_address) {
    this.telecom_service_address = DBStringHelper.set(telecom_service_address,50);
  }

  /**
   * @hibernate.property column="ITEM_TYPE"
   * Item
   */
  public long getItemType() {
    return item_type;
  }

  public void setItemType(long item_type) {
    this.item_type = item_type;
  }

  /**
   * @hibernate.property column="HELD_MEDIUM_TYPE"
   */
  public long getHeldMediumType() {
    return held_medium_type;
  }

  public void setHeldMediumType(long held_medium_type) {
    this.held_medium_type = held_medium_type;
  }

  /**
   * @hibernate.property column="CALL_NUMBER"
   */
  public String getCallNumber() {
    return call_number;
  }

  public void setCallNumber(String call_number) {
    this.call_number = call_number;
  }

  /**
   * @hibernate.property column="AUTHOR" length="200"
   */
  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = DBStringHelper.set(author,200);
  }

  /**
   * @hibernate.property column="TITLE" length="200"
   */
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = DBStringHelper.set(title,200);
  }

  /**
   * @hibernate.property column="SUBTITLE" length="200"
   */
  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = DBStringHelper.set(subtitle,200);
  }

  /**
   * @hibernate.property column="SPONSORING_BODY" length="200"
   */
  public String getSponsoringBody() {
    return sponsoring_body;
  }

  public void setSponsoringBody(String sponsoring_body) {
    this.sponsoring_body = DBStringHelper.set(sponsoring_body,200);
  }

  /**
   * @hibernate.property column="PLACE_OF_PUBLICATION" length="200"
   */
  public String getPlaceOfPublication() {
    return place_of_publication;
  }

  public void setPlaceOfPublication(String place_of_publication) {
    this.place_of_publication = DBStringHelper.set(place_of_publication,200);
  }

  /**
   * @hibernate.property column="PUBLISHER" length="200"
   */
  public String getPublisher() {
    return publisher;
  }

  public void setPublisher(String publisher) {
    this.publisher = DBStringHelper.set(publisher,200);
  }

  /**
   * @hibernate.property column="SERIES_TITLE_NUMBER" length="200"
   */
  public String getSeriesTitleNumber() {
    return series_title_number;
  }

  public void setSeriesTitleNumber(String series_title_number) {
    this.series_title_number = DBStringHelper.set(series_title_number,200);
  }

  /**
   * @hibernate.property column="ITEM_VOLUME" length="200"
   */
  public String getVolume() {
    return volume;
  }

  public void setVolume(String volume) {
    this.volume = DBStringHelper.set(volume,200);
  }

  /**
   * @hibernate.property column="ISSUE" length="200"
   */
  public String getIssue() {
    return issue;
  }

  public void setIssue(String issue) {
    this.issue = DBStringHelper.set(issue,200);
  }

  /**
   * @hibernate.property column="EDITION" length="200"
   */
  public String getEdition() {
    return edition;
  }

  public void setEdition(String edition) {
    this.edition = DBStringHelper.set(edition,200);
  }

  /**
   * @hibernate.property column="PUBLICATION_DATE" length="200"
   */
  public String getPublicationDate() {
    return publication_date;
  }

  public void setPublicationDate(String publication_date) {
    this.publication_date = DBStringHelper.set(publication_date,200);
  }

  /**
   * @hibernate.property column="PUBLICATION_DATE_OF_COMPONENT" length="200"
   */
  public String getPublicationDateOfComponent() {
    return publication_date_of_component;
  }

  public void setPublicationDateOfComponent(String publication_date_of_component) {
    this.publication_date_of_component = DBStringHelper.set(publication_date_of_component,200);
  }

  /**
   * @hibernate.property column="AUTHOR_OF_ARTICLE" length="200"
   */
  public String getAuthorOfArticle() {
    return author_of_article;
  }

  public void setAuthorOfArticle(String author_of_article) {
    this.author_of_article = DBStringHelper.set(author_of_article,200);
  }

  /**
   * @hibernate.property column="TITLE_OF_ARTICLE" length="200"
   */
  public String getTitleOfArticle() {
    return title_of_article;
  }

  public void setTitleOfArticle(String title_of_article) {
    this.title_of_article = DBStringHelper.set(title_of_article,200);
  }

  /**
   * @hibernate.property column="PAGINATION" length="200"
   */
  public String getPagination() {
    return pagination;
  }

  public void setPagination(String pagination) {
    this.pagination = DBStringHelper.set(pagination,200);
  }

  // National bib no
  /**
   * @hibernate.property column="ISBN" length="20"
   */
  public String getISBN() {
    return isbn;
  }

  public void setISBN(String isbn) {
    this.isbn = DBStringHelper.set(isbn,20);
  }

  /**
   * @hibernate.property column="ISSN" length="20"
   */
  public String getISSN() {
    return issn;
  }

  public void setISSN(String issn) {
    this.issn = DBStringHelper.set(issn,20);
  }

  // System No

  /**
   * @hibernate.property column="ADDITIONAL_NO_LETTERS" length="50"
   */
  public String getAdditionalNoLetters() {
    return additional_no_letters;
  }

  public void setAdditionalNoLetters(String additional_no_letters) {
    this.additional_no_letters = DBStringHelper.set(additional_no_letters,50);
  }

  /**
   * @hibernate.property column="VERIFICATION_REFERENCE_SOURCE" length="200"
   */
  public String getVerificationReferenceSource() {
    return verification_reference_source;
  }

  public void setVerificationReferenceSource(String verification_reference_source) {
    this.verification_reference_source = DBStringHelper.set(verification_reference_source,200);
  }

  /**
   * @hibernate.property column="ACCOUNT_NUMBER" length="50"
   */
  public String getAccountNumber() {
    return account_number;
  }

  public void setAccountNumber(String account_number) {
    this.account_number = DBStringHelper.set(account_number,50);
  }

  /**
   * @hibernate.property column="MAX_COST_CURRENCY_CODE" length="10"
   */
  public String getMaxCostCurrencyCode() {
    return max_cost_currency_code;
  }

  public void setMaxCostCurrencyCode(String max_cost_currency_code) {
    this.max_cost_currency_code = DBStringHelper.set(max_cost_currency_code,10);
  }

  /**
   * @hibernate.property column="MAX_COST_STRING" length="10"
   */
  public String getMaxCostString() {
    return max_cost_string;
  }

  public void setMaxCostString(String max_cost_string) {
    this.max_cost_string = DBStringHelper.set(max_cost_string,10);
  }

  /**
   * @hibernate.property column="RECIPROCAL_AGREEMENT"
   */
  public boolean getReciprocalAgreement() {
    return reciprocal_agreement;
  }

  public void setReciprocalAgreement(boolean reciprocal_agreement) {
    this.reciprocal_agreement = reciprocal_agreement;
  }

  /**
   * @hibernate.property column="WILL_PAY_FEE"
   */
  public boolean getWillPayFee() {
    return will_pay_fee;
  }

  public void setWillPayFee(boolean will_pay_fee) {
    this.will_pay_fee = will_pay_fee;
  }

  /**
   * @hibernate.property column="PAYMENT_PROVIDED"
   */
  public boolean getPaymentProvided() {
    return payment_provided;
  }

  public void setPaymentProvided(boolean payment_provided) {
    this.payment_provided = payment_provided;
  }

  /**
   * @hibernate.property column="COPYRIGHT_COMPILIANCE" length="200"
   */
  public String getCopyrightCompliance() {
    return copyright_compliance;
  }

  public void setCopyrightCompliance(String copyright_compliance) {
    this.copyright_compliance = DBStringHelper.set(copyright_compliance,200);
  }

  public IPIGSystemNumber getSystemNo()
  {
    return sysno;
  }

  public void setSystemNo(IPIGSystemNumber sysno)
  {  
    this.sysno = sysno;
  }

  public IPIGSystemNumber getNatBibNo()
  {
    return natbib;
  }

  public void setNatBibNo(IPIGSystemNumber natbib)
  {  
    this.natbib = natbib;
  }

  /**
   * @hibernate.property column="UNIFORM_TITLE" length="200"
   */
  public String getUniformTitle() {
    return uniform_title;
  }

  public void setUniformTitle(String uniform_title) {
    this.uniform_title = DBStringHelper.set(uniform_title,200);
  }

  /**
   * @hibernate.property column="DISSERTATION" length="200"
   */
  public String getDissertation() {
    return dissertation;
  }

  public void setDissertation(String dissertation) {
    this.dissertation = DBStringHelper.set(dissertation,200);
  }

  /**
   * @hibernate.property column="ACCESSION_NUMBER" length="20"
   */
  public String getAccessionNumber() {
    return accession_number;
  }

  public void setAccessionNumber(String accession_number) {  
    this.accession_number = DBStringHelper.set(accession_number,20);
  }

  /**
   * @hibernate.property column="SHELFMARK" length="20"
   */
  public String getShelfmark() {
    return shelfmark;
  }

  public void setShelfmark(String shelfmark) {  
    this.shelfmark = DBStringHelper.set(shelfmark,20);
  }

  /**
   * @hibernate.property column="LOCAL_NOTES" length="200"
   */
  public String getLocalNotes() {
    return local_notes;
  }

  public void setLocalNotes(String local_notes) {  
    this.local_notes = DBStringHelper.set(local_notes,200);
  }

  /**
   * @hibernate.property column="ITEM_LANGUAGE" length="20"
   */
  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {  
    this.language = DBStringHelper.set(language,20);
  }

  /**
   * @hibernate.property column="E_DELIV_DESC" length="100"
   */
  public String getEDeliveryDescripton() {
    return e_delivery_description;
  }

  public void setEDeliveryDescripton(String e_delivery_description) {  
    this.e_delivery_description = DBStringHelper.set(e_delivery_description,100);
  }

  /**
   * @hibernate.property column="E_DELIV_NAME_OR_CODE" length="100"
   */
  public String getEDeliveryNameOrCode() {
    return e_delivery_name_or_code;
  }

  public void setEDeliveryNameOrCode(String e_delivery_name_or_code) {  
    this.e_delivery_name_or_code = DBStringHelper.set(e_delivery_name_or_code,100);
  }

  /**
   * @hibernate.property column="E_DELIV_TIME" length="20"
   */
  public String getEDeliveryTime() {
    return e_delivery_time;
  }

  public void setEDeliveryTime(String e_delivery_time) {  
    this.e_delivery_time = DBStringHelper.set(e_delivery_time,20);
  }

  /**
   * @hibernate.property column="E_DELIV_DETS_TYPE"
   */
  public int getEDeliveryDetsType() {
    return e_delivery_dets_type;
  }

  public void setEDeliveryDetsType(int e_delivery_dets_type) {  
    this.e_delivery_dets_type = e_delivery_dets_type;
  }

  /**
   * @hibernate.property column="E_DELIV_DETS_ID" length="20"
   */
  public String getEDeliveryDetsID() {
    return e_delivery_dets_id;
  }

  public void setEDeliveryDetsID(String e_delivery_dets_id) {  
    this.e_delivery_dets_id = DBStringHelper.set(e_delivery_dets_id,20);
  }

  /**
   * @hibernate.property column="E_DELIV_DETS_ADDR" length="100"
   */
  public String getEDeliveryDetsAddr() {
    return e_delivery_dets_addr;
  }

  public void setEDeliveryDetsAddr(String e_delivery_dets_addr) {  
    this.e_delivery_dets_addr = DBStringHelper.set(e_delivery_dets_addr,100);
  }

  /**
   * @hibernate.property column="SOURCE" length="100"
   */
  public String getSource() {
    return source;
  }

  public void setSource(String source) {  
    this.source = DBStringHelper.set(source,100);
  }

  public Object clone()
  {
    RequiredItemDetails new_details = new RequiredItemDetails();
    if ( this.service_types != null )
      new_details.setService_types((List) ((ArrayList)this.service_types).clone());
    new_details.setLevelOfService(this.level_of_service);
    new_details.setNeedBeforeDate(this.need_before_date);
    new_details.setExpiryFlag(this.expiry_flag);
    new_details.setExpiryDate(this.expiry_date);
    if ( this.supply_medium_info_type != null )
      new_details.setSupplyMediumInfoType((List) ((ArrayList)this.supply_medium_info_type).clone());
    new_details.setPlaceOnHold(this.place_on_hold);
    new_details.setClientName(this.client_name);
    new_details.setClientStatus(this.client_status);
    new_details.setClientIdentifier(this.client_identifier);
    // if ( this.delivery_address != null )
    //   new_details.setDeliveryAddress((ISO10161Address) this.delivery_address.clone());
    new_details.setDeliveryServiceType(this.delivery_service_type);
    new_details.setTransportationMode(this.transportation_mode);
    new_details.setTelecomServiceIdentifier(this.telecom_service_identifier);
    new_details.setTelecomServiceAddress(this.telecom_service_address);
    // if ( this.billing_address != null )
    //   new_details.setBillingAddress((ISO10161Address) this.billing_address.clone());
    new_details.setItemType(this.item_type);
    new_details.setHeldMediumType(this.held_medium_type);
    new_details.setCallNumber(this.call_number);
    new_details.setAuthor(this.author);
    new_details.setTitle(this.title);
    new_details. setSubtitle(this.subtitle);
    new_details.setSponsoringBody(this.sponsoring_body);
    new_details.setPlaceOfPublication(this.place_of_publication);
    new_details.setPublisher(this.publisher);
    new_details.setSeriesTitleNumber(this.series_title_number);
    new_details.setVolume(this.volume);
    new_details.setIssue(this.issue);
    new_details.setEdition(this.edition);
    new_details.setPublicationDate(this.publication_date);
    new_details.setPublicationDateOfComponent(this.publication_date_of_component);
    new_details.setAuthorOfArticle(this.author_of_article);
    new_details.setTitleOfArticle(this.title_of_article);
    new_details.setPagination(this.pagination);
    new_details.setISBN(this.isbn);
    new_details.setISSN(this.issn);
    new_details.setAdditionalNoLetters(this.additional_no_letters);
    new_details.setVerificationReferenceSource(this.verification_reference_source);
    new_details.setAccountNumber(this.account_number);
    new_details.setMaxCostCurrencyCode(this.max_cost_currency_code);
    new_details.setMaxCostString(this.max_cost_string);
    new_details.setReciprocalAgreement(this.reciprocal_agreement);
    new_details.setWillPayFee(this.will_pay_fee);
    new_details.setPaymentProvided(this.payment_provided);
    new_details.setCopyrightCompliance(this.copyright_compliance);
    if ( this.sysno != null )
      new_details.setSystemNo((IPIGSystemNumber)this.sysno.clone());
    if ( this.natbib != null )
      new_details.setNatBibNo((IPIGSystemNumber)this.natbib.clone());
    new_details.setAccessionNumber(this.accession_number);
    new_details.setShelfmark(this.shelfmark);
    new_details.setLocalNotes(this.local_notes);
    return new_details;
  }
}
