package com.k_int.openrequest.db.Location;

import java.util.*;
import org.hibernate.*;
import org.hibernate.type.*;


/**
 * Title:       Location
 * @version:    $Id: Location.java,v 1.4 2005/05/13 17:24:40 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_LOCATION" dynmic-update="true" dynamic-insert="true"
 */
public class Location {

  private Long id;
  private String name;
  private LocationType type;
  private String default_symbol;
  private Map properties = new HashMap();
  private Set aliases = new HashSet();

  private String app_specific_1;


  public static String TYPE_NORMAL = "NORM";
  public static String TYPE_INTERMEDIARY = "INT";

  public Location() {
  }
  
  public Location(String name,LocationType type,String default_symbol) {
    this.name = name;
    this.type = type;
    this.default_symbol = default_symbol;
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="LOC_NAME" length="100"
   */
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  /**
   * @hibernate.many-to-one  column="LOC_TYPE" class="com.k_int.openrequest.db.Location.LocationType"
   */
  public LocationType getType() {
    return type;
  }

  public void setType(LocationType type) {
    this.type = type;
  }

  public String toString() {
    return name+" ("+default_symbol+")";
  }

  /**
   * @hibernate.map table="OR_LOCATION_PROPERTIES" lazy="true" cascade="all"
   * @hibernate.collection-key  column="LOCATION_ID"
   * @hibernate.collection-index  column="PROP_NAME" type="java.lang.String" length="50"
   * @hibernate.collection-element type="java.lang.String" column="PROP_VALUE"
   */
  public Map getProperties() {
    return properties;
  }

  public void setProperties(Map properties) {
    this.properties = properties;
  }

  /**
   * @hibernate.property column="DEFAULT_SYMBOL" length="50"
   */
  public String getDefaultSymbol() {
    return default_symbol;
  }

  public void setDefaultSymbol(String default_symbol) {
    this.default_symbol = default_symbol;
  }

  /**
   * @hibernate.set cascade="all" lazy="true" table="OR_LOCATION_SYMBOL"
   * @hibernate.collection-key column="CANONICAL_LOCATION"
   * @hibernate.collection-one-to-many class="com.k_int.openrequest.db.Location.LocationSymbol"
   */
  public Set getAliases() {
    return aliases;
  }

  public void setAliases(Set aliases) {
    this.aliases = aliases;
  }

  /**
   * @hibernate.property column="APP_SPECIFIC_1" length="30"
   */
  public String getAppSpecific1() {
    return app_specific_1;
  }

  public void setAppSpecific1(String app_specific_1) {
    this.app_specific_1 = app_specific_1;
  }

  public static Location lookupOrCreateByName(Session session, String location_name, String default_symbol) throws java.sql.SQLException {
    Location result = null;
    Query q = session.createQuery("select l from com.k_int.openrequest.db.Location.Location l where l.name = ?");
    q.setParameter(0,location_name,Hibernate.STRING);
    result = (Location) q.uniqueResult();

    if ( result == null ) {
      result = new Location();
      result.setName(location_name);
      result.setType(com.k_int.openrequest.helpers.DBHelper.lookupLocationType(session,"NORM"));
      result.setDefaultSymbol(default_symbol);
      Long new_id = (Long) session.save(result);
      result.setId(new_id);
    }

    return result;
  }
}
