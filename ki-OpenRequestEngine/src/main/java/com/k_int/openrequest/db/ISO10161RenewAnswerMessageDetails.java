/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161RenewAnswerMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:REA"
 */

package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;

/**
 * Title:       ISO10161RenewMessageDetails
 * @version:    $Id: ISO10161RenewMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:REA"
 */
public class ISO10161RenewAnswerMessageDetails extends ISO10161v2Message {

  private Boolean answer;
  private Date date_due;
  private Boolean renewable;

  /**
   * @hibernate.property column="ANSWER"
   */
  public Boolean getAnswer() {
    return answer;
  }

  public void setAnswer(Boolean answer) {
    this.answer = answer;
  }

  /**
   * @hibernate.property column="DATE_DUE"
   */
  public Date getDateDue() {
    return date_due;
  }

  public void setDateDue(Date date_due) {
    this.date_due = date_due;
  }

  /**
   * @hibernate.property column="RENEWABLE"
   */
  public Boolean getRenewable() {
    return renewable;
  }

  public void setRenewable(Boolean renewable) {
    this.renewable = renewable;
  }
}
