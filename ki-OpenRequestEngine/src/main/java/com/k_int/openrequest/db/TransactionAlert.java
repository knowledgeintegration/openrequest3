/**
 * Title:       TransactionAlert
 * @version:    $Id: TransactionAlert.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.db;
import java.util.Date;

public class TransactionAlert
{
  private Date alert_timestamp;
  private String alert_message;

  public Date getAlertTimestamp()
  {
    return alert_timestamp;
  }

  public void setAlertTimestamp(Date alert_timestamp)
  {
    this.alert_timestamp = alert_timestamp;
  }

  public String getAlertMessage()
  {
    return alert_message;
  }

  public void setAlertMessage(String alert_message)
  {
    this.alert_message = alert_message;
  }
}
