
package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;

/**
 * Title:       Extension
 * @version:    $Id: Extension.java,v 1.3 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_MESSAGE_EXTENSION" dynmic-update="true" dynamic-insert="true" discriminator-value="0"
 * @hibernate.discriminator column="EXTENSION_TYPE" type="string" length="20"
 */
public class Extension {
  private Long id;
  private String extension_name;

  public Extension() {
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="EXTENSION_NAME" length="50"
   */
  public String getExtensionName() {
    return extension_name;
  }

  public void setExtensionName(String extension_name) {
    this.extension_name = DBStringHelper.set(extension_name,50);
  }
}
