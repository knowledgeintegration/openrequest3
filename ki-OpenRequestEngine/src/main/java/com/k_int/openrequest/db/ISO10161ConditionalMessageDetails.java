package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161ConditionalMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:COND"
 */
public class ISO10161ConditionalMessageDetails extends ISO10161v2Message
{
  private Boolean answer;

  /**
   * @hibernate.property column="ANSWER"
   */
  public Boolean getAnswer() {
    return answer;
  }

  public void setAnswer(Boolean answer) {
    this.answer = answer;
  }
}
