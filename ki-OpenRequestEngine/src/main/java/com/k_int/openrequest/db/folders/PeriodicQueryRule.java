/**
 * Title:       PeriodicQueryRule
 * @version:    $Id: PeriodicQueryRule.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="QUERY" lazy="false"
 */

package com.k_int.openrequest.db.folders;

public class PeriodicQueryRule extends FolderRule {

  private String query;

  /**
   * @hibernate.property column="QUERY"
   */
  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }
}
