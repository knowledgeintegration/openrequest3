package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161ShippedMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:SHI"
 */

public class ISO10161ShippedMessageDetails extends ISO10161v2Message {

  private String responder_telecom_service_identifier;
  private String responder_telecom_service_address;
  private long intermediary_symbol_type;
  private String intermediary_symbol;
  private long intermediary_name_type;
  private String intermediary_name;
  private long supplier_symbol_type;
  private String supplier_symbol;
  private long supplier_name_type;
  private String supplier_name;
  private String client_name;
  private String client_id;
  private String client_status;
  private long transaction_type;
  private long shipped_service_type;
  private Boolean can_send_shipped;
  private Boolean can_send_checked_in;
  private long responder_received;
  private long responder_returned;
  private Date date_shipped;
  private Date date_due;
  private Boolean renewable;
  private long chargeable_units;
  private String currency_code;
  private String monetary_value;
  private long shipped_conditions;
  private long shipped_via_type;
  private String transportation_mode;
  private String insured_for_currency_code;
  private String insured_for_monetary_value;
  private String return_insured_for_currency_code;
  private String return_insured_for_monetary_value;
  private long postal_name_type;
  private String postal_name;
  private String postal_extended_address;
  private String postal_street_and_number;
  private String postal_po_box;
  private String postal_city;
  private String postal_region;
  private String postal_country;
  private String postal_postcode;

  /**
   * @hibernate.property column="RESP_TELECO_SERV_ID" length="40"
   */
  public String getResponderTelecomServiceIdentifier() {
    return responder_telecom_service_identifier;
  }

  public void setResponderTelecomServiceIdentifier(String responder_telecom_service_type) {
    this.responder_telecom_service_identifier = DBStringHelper.set(responder_telecom_service_identifier,40);
  }

  /**
   * @hibernate.property column="RESP_TELECO_SERV_ADDR" length="100"
   */
  public String getResponderTelecomServiceAddress() {
    return responder_telecom_service_address;
  }

  public void setResponderTelecomServiceAddress(String responder_telecom_service_address) {
    this.responder_telecom_service_address = DBStringHelper.set(responder_telecom_service_address,100);
  }

  /**
   * @hibernate.property column="INT_ID_SYM_TYPE"
   */
  public long getIntermediaryIdSymbolType() {
    return intermediary_symbol_type;
  }

  public void setIntermediaryIdSymbolType(long intermediary_symbol_type) {
    this.intermediary_symbol_type = intermediary_symbol_type;
  }

  /**
   * @hibernate.property column="INT_ID_SYM" length="40"
   */
  public String getIntermediaryIdSymbol() {
    return intermediary_symbol;
  }

  public void setIntermediaryIdSymbol(String intermediary_symbol) {
    this.intermediary_symbol = DBStringHelper.set(intermediary_symbol,40);
  }

  /**
   * @hibernate.property column="INT_ID_NAME_TYPE"
   */
  public long getIntermediaryIdNameType() {
    return intermediary_name_type;
  }

  public void setIntermediaryIdNameType(long intermediary_name_type) {
    this.intermediary_name_type = intermediary_name_type;
  }

  /**
   * @hibernate.property column="INT_ID_NAME" length="100"
   */
  public String getIntermediaryIdName() {
    return intermediary_name;
  }

  public void setIntermediaryIdName(String intermediary_name) {
    this.intermediary_name = DBStringHelper.set(intermediary_name,100);
  }

  /**
   * @hibernate.property column="SUPPLIER_ID_SYMBOL_TYPE"
   */
  public long getSupplierIdSymbolType() {
    return supplier_symbol_type;
  }

  public void setSupplierIdSymbolType(long supplier_symbol_type) {
    this.supplier_symbol_type = supplier_symbol_type;
  }

  /**
   * @hibernate.property column="SUPPLIER_ID_SYMBOL" length="40"
   */
  public String getSupplierIdSymbol() {
    return supplier_symbol;
  }

  public void setSupplierIdSymbol(String supplier_symbol) {
    this.supplier_symbol = DBStringHelper.set(supplier_symbol,40);
  }

  /**
   * @hibernate.property column="SUPPLIER_ID_NAME_TYPE"
   */
  public long getSupplierIdNameType() {
    return supplier_name_type;
  }

  public void setSupplierIdNameType(long supplier_name_type) {
    this.supplier_name_type = supplier_name_type;
  }

  /**
   * @hibernate.property column="SUPPLIER_ID_NAME" length="100"
   */
  public String getSupplierIdName() {
    return supplier_name;
  }

  public void setSupplierIdName(String supplier_name) {
    this.supplier_name = DBStringHelper.set(supplier_name,100);
  }

  /**
   * @hibernate.property column="CLIENT_NAME" length="100"
   */
  public String getClientName() {
    return client_name;
  }

  public void setClientName(String client_name) {
    this.client_name = DBStringHelper.set(client_name,100);
  }

  /**
   * @hibernate.property column="CLIENT_IDENTIFIER" length="100"
   */
  public String getClientIdentifier() {
    return client_id;
  }

  public void setClientIdentifier(String client_id) {
    this.client_id = DBStringHelper.set(client_id,100);
  }

  /**
   * @hibernate.property column="CLIENT_STATUS" length="20"
   */
  public String getClientStatus() {
    return client_status;
  }

  public void setClientStatus(String client_status) {
    this.client_status = DBStringHelper.set(client_status,20);
  }

  /**
   * @hibernate.property column="TRANSACTION_TYPE"
   */
  public long getTransactionType() {
    return transaction_type;
  }

  public void setTransactionType(long transaction_type) {
    this.transaction_type = transaction_type;
  }

  /**
   * @hibernate.property column="SHIPPED_SERVICE_TYPE"
   */
  public long getShippedServiceType() {
    return shipped_service_type;
  }

  public void setShippedServiceType(long shipped_service_type) {
    this.shipped_service_type = shipped_service_type;
  }

  /**
   * @hibernate.property column="CAN_SEND_SHIPPED"
   */
  public Boolean getCanSendShipped() {
    return can_send_shipped;
  }

  public void setCanSendShipped(Boolean can_send_shipped) {
    this.can_send_shipped = can_send_shipped;
  }

  /**
   * @hibernate.property column="CAN_SEND_CHECKED_IN"
   */
  public Boolean getCanSendCheckedIn() {
    return can_send_checked_in;
  }

  public void setCanSendCheckedIn(Boolean can_send_checked_in) {
    this.can_send_checked_in = can_send_checked_in;
  }

  /**
   * @hibernate.property column="RESPONDER_RECEIVED"
   */
  public long getResponderReceived() {
    return responder_received;
  }

  public void setResponderReceived(long responder_received) {
    this.responder_received = responder_received;
  }

  /**
   * @hibernate.property column="RESPONDER_RETURNED"
   */
  public long getResponderReturned() {
    return responder_returned;
  }

  public void setResponderReturned(long responder_returned) {
    this.responder_returned = responder_returned;
  }

  /**
   * @hibernate.property column="DATE_SHIPPED"
   */
  public Date getDateShipped() {
    return date_shipped;
  }

  public void setDateShipped(Date date_shipped) {
    this.date_shipped = date_shipped;
  }

  /**
   * @hibernate.property column="DATE_DUE"
   */
  public Date getDateDue() {
    return date_due;
  }

  public void setDateDue(Date date_due) {
    this.date_due = date_due;
  }

  /**
   * @hibernate.property column="RENEWABLE"
   */
  public Boolean getRenewable() {
    return renewable;
  }

  public void setRenewable(Boolean renewable) {
    this.renewable = renewable;
  }

  /**
   * @hibernate.property column="CHARGEABLE_UNITS"
   */
  public long getChargeableUnits() {
    return chargeable_units;
  }

  public void setChargeableUnits(long chargeable_units) {
    this.chargeable_units = chargeable_units;
  }

  /**
   * @hibernate.property column="CURRENCY_CODE" length="10"
   */
  public String getCurrencyCode() {
    return currency_code;
  }

  public void setCurrencyCode(String currency_code) {
    this.currency_code = DBStringHelper.set(currency_code,10);
  }

  /**
   * @hibernate.property column="MONETARY_VALUE" length="20"
   */
  public String getMonetaryValue() {
    return monetary_value;
  }

  public void setMonetaryValue(String monetary_value) {
    this.monetary_value = DBStringHelper.set(monetary_value,20);
  }

  /**
   * @hibernate.property column="SHIPPED_CONDITIONS"
   */
  public long getShippedConditions() {
    return shipped_conditions;
  }

  public void setShippedConditions(long shipped_conditions) {
    this.shipped_conditions = shipped_conditions;
  }

  /**
   * @hibernate.property column="SHIPPED_VIA_TYPE"
   */
  public long getShippedViaType() {
    return shipped_via_type;
  }

  public void setShippedViaType(long shipped_via_type) {
    this.shipped_via_type = shipped_via_type;
  }

  /**
   * @hibernate.property column="TRANSPORTATION_MODE" length="100"
   */
  public String getTransportationMode() {
    return transportation_mode;
  }

  public void setTransportationMode(String transportation_mode) {
    this.transportation_mode = DBStringHelper.set(transportation_mode,100);
  }

  /**
   * @hibernate.property column="INSURED_FOR_CURRENCY_CODE" length="20"
   */
  public String getInsuredForCurrencyCode() {
    return insured_for_currency_code;
  }

  public void setInsuredForCurrencyCode(String insured_for_currency_code) {
    this.insured_for_currency_code = DBStringHelper.set(insured_for_currency_code,20);
  }

  /**
   * @hibernate.property column="INSURED_FOR_MONETARY_VALUE" length="20"
   */
  public String getInsuredForMonetaryValue() {
    return insured_for_monetary_value;
  }

  public void setInsuredForMonetaryValue(String insured_for_monetary_value) {
    this.insured_for_monetary_value = DBStringHelper.set(insured_for_monetary_value,20);
  }

  /**
   * @hibernate.property column="RET_INS_FOR_CURR_CODE" length="10"
   */
  public String getReturnInsuredForCurrencyCode() {
    return return_insured_for_currency_code;
  }

  public void setReturnInsuredForCurrencyCode(String return_insured_for_currency_code) {
    this.return_insured_for_currency_code = DBStringHelper.set(return_insured_for_currency_code,10);
  }

  /**
   * @hibernate.property column="RET_INS_FOR_VALUE" length="20"
   */
  public String getReturnInsuredForMonetaryValue() {
    return return_insured_for_monetary_value;
  }

  public void setReturnInsuredForMonetaryValue(String return_insured_for_monetary_value) {
    this.return_insured_for_monetary_value = DBStringHelper.set(return_insured_for_monetary_value,20);
  }

  /**
   * @hibernate.property column="POSTAL_NAME_TYPE"
   */
  public long getPostalNameType() {
    return postal_name_type;
  }

  public void setPostalNameType(long postal_name_type) {
    this.postal_name_type = postal_name_type;
  }

  /**
   * @hibernate.property column="POSTAL_NAME" length="100"
   */
  public String getPostalName() {
    return postal_name;
  }

  public void setPostalName(String postal_name) {
    this.postal_name= DBStringHelper.set(postal_name,100);
  }

  /**
   * @hibernate.property column="POSTAL_EXTENDED_ADDRESS" length="100"
   */
  public String getPostalExtendedAddress() {
    return postal_extended_address;
  }

  public void setPostalExtendedAddress(String postal_extended_address) {
    this.postal_extended_address= DBStringHelper.set(postal_extended_address,100);
  }

  /**
   * @hibernate.property column="POSTAL_STREET_AND_NUMBER" length="100"
   */
  public String getPostalStreetAndNumber() {
    return postal_street_and_number;
  }

  public void setPostalStreetAndNumber(String postal_street_and_number) {
    this.postal_street_and_number=DBStringHelper.set(postal_street_and_number,100);
  }

  /**
   * @hibernate.property column="POSTAL_PO_BOX" length="100"
   */
  public String getPostalPOBox() {
    return postal_po_box;
  }

  public void setPostalPOBox(String postal_po_box) {
    this.postal_po_box=DBStringHelper.set(postal_po_box,100);
  }

  /**
   * @hibernate.property column="POSTAL_CITY" length="100"
   */
  public String getPostalCity() {
    return postal_city;
  }

  public void setPostalCity(String postal_city) {
    this.postal_city=DBStringHelper.set(postal_city,100);
  }

  /**
   * @hibernate.property column="POSTAL_REGION" length="100"
   */
  public String getPostalRegion() {
    return postal_region;
  }

  public void setPostalRegion(String postal_region) {
    this.postal_region=DBStringHelper.set(postal_region,100);
  }

  /**
   * @hibernate.property column="POSTAL_COUNTRY" length="100"
   */
  public String getPostalCountry() {
    return postal_country;
  }

  public void setPostalCountry(String postal_country) {
    this.postal_country=DBStringHelper.set(postal_country,100);
  }

  /**
   * @hibernate.property column="POSTAL_POST_CODE" length="100"
   */
  public String getPostalPostcode() {
    return postal_postcode;
  }

  public void setPostalPostcode(String postal_postcode) {
    this.postal_postcode=DBStringHelper.set(postal_postcode,100);
  }
}
