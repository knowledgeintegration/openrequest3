package com.k_int.openrequest.db;

/**
 * Title:       PartyName
 * @version:    $Id: SystemId.java,v 1.2 2005/06/17 18:52:15 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 */
public class SystemId implements java.io.Serializable {

  protected PartySymbol symbol;
  protected PartyName name;

  public SystemId() {
  }

  public SystemId(PartySymbol symbol, PartyName name) {
    this.symbol = symbol;
    this.name = name;
  }

  /**
   * @hibernate.component
   */
  public PartySymbol getSymbol() {
    return symbol;
  }

  public void setSymbol(PartySymbol symbol) {
    this.symbol = symbol;
  }

  /**
   * @hibernate.component
   */
  public PartyName getName() {
    return name;
  }

  public void setName(PartyName name) {
    this.name = name;
  }
}
