/**
 * Title:       StatusCode
 * @version:    $Id: EventHandler.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.db;

public class EventHandler
{
  // private long id;
  // private String event_precondition;
  private String handler_type;
  private String handler_params;
  private String message_type;
  private String message_params;

  public EventHandler()
  {
  }

  // public EventHandler(String event_precondition,
  public EventHandler(String message_type,
                      String message_params,
                      String handler_type,
                      String handler_params)
  {
    // this.event_precondition = event_precondition;
    this.message_type = message_type;
    this.message_params = message_params;
    this.handler_type = handler_type;
    this.handler_params = handler_params;
  }

  // public long getId()
  // {
  //   return id;
  // }
  // public void setId(long id)
  // {
  //   this.id = id;
  // }

  // public String getEventPrecondition()
  // {
  //   return event_precondition;
  // }

  // public void setEventPrecondition(String event_precondition)
  // {
  //   this.event_precondition = event_precondition;
  // }

  public String getMessageType()
  {
    return message_type;
  }
 
  public void setMessageType(String message_type)
  {
    this.message_type = message_type;
  }

  public String getMessageParams()
  {
    return message_params;
  }

  public void setMessageParams(String message_params)
  {
    this.message_params = message_params;
  }

  public String getHandlerType()
  {
    return handler_type;
  }
 
  public void setHandlerType(String handler_type)
  {
    this.handler_type = handler_type;
  }

  public String getHandlerParams()
  {
    return handler_params;
  }

  public void setHandlerParams(String handler_params)
  {
    this.handler_params = handler_params;
  }
}
