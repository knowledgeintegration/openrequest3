
package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


/**
 * Title:       Extension
 * @version:    $Id: OCLCShippedExtension.java,v 1.3 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynmic-update="true" dynamic-insert="true" discriminator-value="OCLC:SHIPPEDEXTN"
 */
public class OCLCShippedExtension extends Extension {

  private String payment_method;

  public OCLCShippedExtension() {
  }

  /**
   * @hibernate.property column="PAYMENT_METHOD" length="40"
   */
  public String getPaymentMethod() {
    return payment_method;
  }

  public void setPaymentMethod(String payment_method) {
    this.payment_method =  DBStringHelper.set(payment_method,40);
  }
}
