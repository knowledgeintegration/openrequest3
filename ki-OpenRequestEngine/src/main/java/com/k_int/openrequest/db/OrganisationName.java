package com.k_int.openrequest.db;

/**
 * Title:       OrganisationName
 * @version:    $Id: OrganisationName.java,v 1.2 2005/06/17 18:52:15 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * @hibernate.subclass discriminator-value="ORG_NAME"
 * Description: 
 */
public class OrganisationName extends PartyName implements java.io.Serializable {
  public OrganisationName(String name) {
    this.name = name;
  }
}
