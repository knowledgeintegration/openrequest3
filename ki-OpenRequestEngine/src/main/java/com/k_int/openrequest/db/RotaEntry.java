/**
 * Title:       RecordFormatSpecification
 * @version:    $Id: RotaEntry.java,v 1.3 2005/02/24 21:18:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.db;

import com.k_int.openrequest.db.Location.*;

public class RotaEntry
{
  private boolean already_tried;
  private long symbol_type;
  private String symbol;
  private String mandatory_tq;
  private long name_type;
  private String name;
  private String account_number;
  private String service_identifier;
  private String service_address;

  public RotaEntry() {
  }

  public RotaEntry(boolean already_tried,
                   long symbol_type,
                   String symbol,
                   String mandatory_tq,
                   long name_type,
                   String name,
                   String account_number,
                   String service_identifier,
                   String service_address) {
    this.already_tried = already_tried;
    this.symbol_type = symbol_type;
    this.symbol = symbol;
    this.mandatory_tq = mandatory_tq;
    this.name_type = name_type;
    this.name = name;
    this.account_number = account_number;
    this.service_identifier = service_identifier; 
    this.service_address = service_address;
  }

  public boolean getAlreadyTried() {
    return already_tried;
  }

  public void setAlreadyTried(boolean already_tried) {
    this.already_tried = already_tried;
  }

  public long getSymbolType() {
    return symbol_type;
  }

  public void setSymbolType(long symbol_type) {
    this.symbol_type = symbol_type;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public long getNameType() {
    return name_type;
  }

  public void setNameType(long name_type) {
    this.name_type = name_type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAccountNumber() {
    return account_number;
  }

  public void setAccountNumber(String account_number) {
    this.account_number = account_number;
  }

  public String getServiceIdentifier() {
    return service_identifier;
  }

  public void setServiceIdentifier(String service_identifier) {
    this.service_identifier = service_identifier;
  }

  public String getServiceAddress() {
    return service_address;
  }

  public void setServiceAddress(String service_address) {
    this.service_address = service_address;
  }

  public String getMandatoryTQ() {
    return mandatory_tq;
  }

  public void setMandatoryTQ(String mandatory_tq) {
    this.mandatory_tq = mandatory_tq;
  }

}
