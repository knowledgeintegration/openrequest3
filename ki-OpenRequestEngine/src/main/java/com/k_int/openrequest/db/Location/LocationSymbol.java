package com.k_int.openrequest.db.Location;

/**
 * Title:       Location
 * @version:    $Id: LocationSymbol.java,v 1.4 2005/06/11 14:02:25 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_LOCATION_SYMBOL" dynmic-update="true" dynamic-insert="true"
 */
public class LocationSymbol {

  private Long id;
  private String name;
  private String symbol;
  private String upper_symbol;
  private NamingAuthority naming_authority;
  private Location canonical_location;
  private Service service_for_this_symbol;

  public LocationSymbol() {
  }

  public LocationSymbol(String symbol,
                        NamingAuthority naming_authority,
                        Location canonical_location,
                        Service service_for_this_symbol) {
    this.symbol = symbol;
    this.naming_authority = naming_authority;
    this.canonical_location = canonical_location;
    this.service_for_this_symbol = service_for_this_symbol;
    if ( symbol != null )
      this.upper_symbol = symbol.toUpperCase();
  }

  public LocationSymbol(String symbol,
                        NamingAuthority naming_authority,
                        Location canonical_location,
                        Service service_for_this_symbol,
                        String name) {
    this.symbol = symbol;
    this.naming_authority = naming_authority;
    this.canonical_location = canonical_location;
    this.service_for_this_symbol = service_for_this_symbol;
    this.name = name;
    if ( symbol != null )
      this.upper_symbol = symbol.toUpperCase();
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  /**
   * @hibernate.property column="SYMBOL_NAME" length="255"
   */
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  /**
   * @hibernate.property column="SYMBOL" index="SYMBOL_IDX" length="50"
   */
  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
    if ( symbol != null )
      this.upper_symbol = symbol.toUpperCase();
    else
      this.upper_symbol = null;
  }

  /**
   * @hibernate.many-to-one  column="AUTHORITY" class="com.k_int.openrequest.db.Location.NamingAuthority"
   */
  public NamingAuthority getAuthority() {
    return naming_authority;
  }

  public void setAuthority(NamingAuthority authority) {
    this.naming_authority = authority;
  }

  /**
   * @hibernate.many-to-one  column="CANONICAL_LOCATION" class="com.k_int.openrequest.db.Location.Location"
   */
  public Location getCanonical_location() {
    return canonical_location;
  }

  public void setCanonical_location(Location canonical_location) {
    this.canonical_location = canonical_location;
  }

  /**
   * @hibernate.many-to-one  column="SERVICE_FOR_THIS_SYMBOL" class="com.k_int.openrequest.db.Location.Service"
   */
  public Service getServiceForThisSymbol() {
    return service_for_this_symbol;
  }

  public void setServiceForThisSymbol(Service service_for_this_symbol) {
    this.service_for_this_symbol = service_for_this_symbol;
  }

  public String toString() {
    return naming_authority.getIdentifier()+":"+symbol;
  }

  /**
   * @hibernate.property 
   * @hibernate.column name="UC_SYMBOL" length="50" index="UpperSymbolIdx"
   */
  public String getUpperSymbol() {
    return upper_symbol;
  }

  public void setUpperSymbol(String upper_symbol) {
    if ( upper_symbol != null )
      this.upper_symbol = upper_symbol.toUpperCase();
  }

}
