package com.k_int.openrequest.db;

/**
 * Title:       CustomAttrDefinition
 * @version:    $Id: CustomAttrDefinition.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002,2003 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_CUSTOM_ATTR_DEFINITION" dynmic-update="true" dynamic-insert="true"
 */

public class CustomAttrDefinition {
  private Long id;
  private String attr_name;
  private String attr_group;

  /** 
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="CUSTOM_ATTR_NAME" length="25"
   */
  public String getAttrName() {
    return attr_name;
  }

  public void setAttrName(String code) {
    this.attr_name = attr_name;
  }

  /**
   * @hibernate.property column="CUSTOM_ATTR_GROUP" length="25"
   */
  public String getAttrGroup() {
    return attr_group;
  }

  public void setAttrGroup(String attr_group) {
    this.attr_group = attr_group;
  }
}
