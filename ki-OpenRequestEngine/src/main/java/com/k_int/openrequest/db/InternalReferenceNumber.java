
package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;

/**
 * Title:       Extension
 * @version:    $Id: InternalReferenceNumber.java,v 1.3 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="INTREFNO"
 */
public class InternalReferenceNumber extends Extension {

  private String requester_ref_authority = null;
  private String requester_ref = null;
  private String responder_ref_authority = null;
  private String responder_ref = null;

  public InternalReferenceNumber() {
  }

  public InternalReferenceNumber(String requester_ref_authority,
                                 String requester_ref,
                                 String responder_ref_authority,
                                 String responder_ref) {
    this.requester_ref_authority =  DBStringHelper.set(requester_ref_authority,100);
    this.requester_ref =  DBStringHelper.set(requester_ref,100);
    this.responder_ref_authority =  DBStringHelper.set(responder_ref_authority,100);
    this.responder_ref =  DBStringHelper.set(responder_ref,100);
  }

  /**
   * @hibernate.property
   * @hibernate.column name="REQ_REF_AUTH" length="100"
   */
  public String getRequesterRefAuthority() {
    return requester_ref_authority;
  }

  public void setRequesterRefAuthority(String requester_ref_authority) {
    this.requester_ref_authority =  DBStringHelper.set(requester_ref_authority,100);
  }

  /**
   * @hibernate.property
   * @hibernate.column name="REQ_REF" length="100"
   */
  public String getRequesterRef() {
    return requester_ref;
  }

  public void setRequesterRef(String requester_ref) {
    this.requester_ref=  DBStringHelper.set(requester_ref,100);
  }

  /**
   * @hibernate.property
   * @hibernate.column name="RESP_REF_AUTH" length="100"
   */
  public String getResponderRefAuthority() {
    return responder_ref_authority;
  }

  public void setResponderRefAuthority(String responder_ref_authority) {
    this.responder_ref_authority =  DBStringHelper.set(responder_ref_authority,100);
  }

  /**
   * @hibernate.property
   * @hibernate.column name="RESP_REF" length="100"
   */
  public String getResponderRef() {
    return responder_ref;
  }

  public void setResponderRef(String responder_ref) {
    this.responder_ref=DBStringHelper.set(responder_ref,100);
  }
}
