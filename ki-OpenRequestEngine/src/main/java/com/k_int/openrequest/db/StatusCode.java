package com.k_int.openrequest.db;

/**
 * Title:       StatusCode
 * @version:    $Id: StatusCode.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_STATUS_CODE" dynmic-update="true" dynamic-insert="true"
 */
public class StatusCode {

  private Long id;
  private String desc;
  private String code;

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="CODE" length="15"
   */
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  /**
   * @hibernate.property column="DESCRIPTION" length="40"
   */
  public String getDescription() {
    return desc;
  }

  public void setDescription(String desc) {
    this.desc = desc;
  }
}
