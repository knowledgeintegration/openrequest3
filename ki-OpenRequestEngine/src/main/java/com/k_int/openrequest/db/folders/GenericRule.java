package com.k_int.openrequest.db.folders;

/**
 * Title:       PeriodicQueryRule
 * @version:    $Id: GenericRule.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="GENERIC" lazy="false"
 */
public class GenericRule extends FolderRule {

  private String old_expression;
  private String expression;

  public GenericRule() {
  } 

  public GenericRule(String desc,
                     String code,
                     String scope_type,
                     String target_type,
                     String target_id,
                     String old_expression,
                     String new_expression) {
    super(desc,code,scope_type,target_type,target_id);
    this.old_expression = old_expression;
    this.expression = new_expression;
  }

  /**
   * @hibernate.property column="EXPRESSION"
   */
  public String getExpression() {
    return expression;
  }

  public void setExpression(String expression) {
    this.expression = expression;
  }

  /**
   * @hibernate.property column="OLD_EXPRESSION"
   * An optimisation that allows us to determine if a transaction needs to be
   * removed from a category without having to load the category
   */
  public String getOldExpression() {
    return old_expression;
  }

  public void setOldExpression(String old_expression) {
    this.old_expression = old_expression;
  }
}
