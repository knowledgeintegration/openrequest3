package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;

/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161AnswerMessageDetails.java,v 1.3 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:ANS"
 */
public class ISO10161AnswerMessageDetails extends ISO10161v2Message {

  private int results_explanation_type;
  private int transaction_results;

  // For Conditional answer
  private int conditional_conditions;

  // private String conditional_date_for_reply;
  private Date conditional_date_for_reply;
  //TODO: Vector for Locations
  //TODO: proposed delivery service

  // For retry answer
  private int reason_not_available;

  // For unfilled answer
  private int reason_unfilled;
  private String locations;
  // For locations answer
  // For will supply answer
  // For hold placed answer
  // For estimate answer

  /**
   * @hibernate.property column="TRANSACTION_RESULTS"
   */
  public int getTransactionResults() {
    return transaction_results;
  }

  public void setTransactionResults(int transaction_results) {
    this.transaction_results = transaction_results;
  }

  /**
   * @hibernate.property column="RESULTS_EXPLANATION_TYPE"
   */
  public int getResultsExplanationType() {
    return results_explanation_type;
  }

  public void setResultsExplanationType(int results_explanation_type) {
    this.results_explanation_type = results_explanation_type;
  }

  /**
   * @hibernate.property column="CONDITIONAL_CONDITIONS"
   */
  public int getConditionalConditions() {
    return conditional_conditions;
  }

  public void setConditionalConditions(int conditional_conditions) {
    this.conditional_conditions = conditional_conditions;
  }

  /**
   * @hibernate.property column="CONDITIONAL_DATE_FOR_REPLY"
   */
  public Date getConditionalDateForReply() {
    return conditional_date_for_reply;
  }

  public void setConditionalDateForReply(Date conditional_date_for_reply) {
    this.conditional_date_for_reply = conditional_date_for_reply;
  }

  /**
   * @hibernate.property column="REASON_UNFILLED"
   */
  public int getReasonUnfilled() {
    return reason_unfilled;
  }

  public void setReasonUnfilled(int reason_unfilled) {
    this.reason_unfilled = reason_unfilled;
  }

  /**
   * @hibernate.property column="REASON_NOT_AVAILABLE"
   */
  public int getReasonNotAvailable() {
    return reason_not_available;
  }

  public void setReasonNotAvailable(int reason_not_available) {
    this.reason_not_available = reason_not_available;
  }

  /**
   * @hibernate.property column="LOCATIONS" length="200"
   */
  public String getLocations() {
    return locations;
  }

  public void setLocations(String locations) {
    this.locations = locations;
  }

}
