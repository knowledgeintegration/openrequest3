/**
 * Title:       RecordFormatSpecification
 * @version:    $Id: MediumInfo.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.db;

import com.k_int.openrequest.db.Location.*;

public class MediumInfo
{
  private long medium_type;
  private String characteristics;

  public MediumInfo()
  {
  }

  public MediumInfo(long medium_type,
                    String characteristics)
  {
    this.medium_type = medium_type;
    this.characteristics = characteristics;
  }

  public long getMediumType()
  {
    return medium_type;
  }

  public void setMediumType(long medium_type)
  {
    this.medium_type = medium_type;
  }

  public String getCharacteristics()
  {
    return characteristics;
  }

  public void setCharacteristics(String characteristics)
  {
    this.characteristics = characteristics;
  }
}
