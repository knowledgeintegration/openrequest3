/**
 * Title:       IPIGSystemNumber
 * @version:    $Id: IPIGSystemNumber.java,v 1.3 2005/07/01 16:11:12 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.db;

import com.k_int.openrequest.util.DBStringHelper;

public class IPIGSystemNumber implements java.io.Serializable
{
  public int well_known_system_id=0;
  public String other_system_id;
  public String database_id = null;
  public String system_no = null;

  public IPIGSystemNumber()
  {
  }

  public IPIGSystemNumber(int well_known_system_id,
                          String other_system_id,
                          String database_id,
                          String system_no)
  {
    this.well_known_system_id = well_known_system_id;
    this.other_system_id = DBStringHelper.set(other_system_id,32);
    this.database_id = DBStringHelper.set(database_id,32);
    this.system_no = DBStringHelper.set(system_no,32);
  }

  public void setWellKnownSystemId(int id)
  {
    this.well_known_system_id = id;
  }

  public int getWellKnownSystemId()
  {
    return well_known_system_id;
  }

  public void setOtherSystemId(String id)
  {
    this.other_system_id = DBStringHelper.set(id,32);
  }

  public String getOtherSystemId()
  {
    return other_system_id;
  }

  public void setDatabaseId(String id)
  {
    this.database_id = DBStringHelper.set(id,32);
  }

  public String getDatabaseId()
  {
    return database_id;
  }

  public void setSystemNo(String system_no)
  {
    this.system_no = DBStringHelper.set(system_no,32);
  }

  public String getSystemNo()
  {
    return system_no;
  }

  public Object clone()
  {
    return new IPIGSystemNumber(well_known_system_id, other_system_id, database_id, system_no);
  }
}
