package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161MessageMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:MSG"
 */
public class ISO10161MessageMessageDetails extends ISO10161v2Message {
  private int dummy;

  /**
   * @hibernate.property column="DUMMY"
   */
  public int getDummy() {
    return dummy;
  }

  public void setDummy(int dummy) {
    this.dummy = dummy;
  }
}
