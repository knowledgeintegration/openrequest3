package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;

/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161RequestMessageDetails.java,v 1.4 2005/07/01 16:11:12 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * @since 1.0
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:REQ"
 */

public class ISO10161RequestMessageDetails extends ISO10161v2Message {

  // private MessageHeader parent;
  private long transaction_type;
  private Boolean can_send_received;
  private Boolean can_send_returned;
  private Long requester_shipped;
  private Long requester_checked_in;
  private boolean retry_flag;
  private boolean forward_flag;
  private String requester_note;
  private String forward_note;
  private boolean perm_to_forward;
  private boolean perm_to_chain;
  private boolean perm_to_partition;
  private boolean perm_to_change_send_to_list;
  private String init_req_service_ident;
  private String init_req_service_addr;
  private long preference;

  private List rota = new ArrayList();

  private RequiredItemDetails generic_details;
  private ISO10161Address delivery_address;
  private ISO10161Address billing_address;

  private Long oclc_ill_service;

  /**
   * @hibernate.property column="TRANSACTION_TYPE"
   */
  public long getTransactionType() {
    return transaction_type;
  }

  public void setTransactionType(long transaction_type) {
    this.transaction_type = transaction_type;
  }

  /**
   * @hibernate.property column="CAN_SEND_RECEIVED"
   */
  public Boolean getCanSendReceived() {
    return can_send_received;
  }

  public void setCanSendReceived(Boolean can_send_received) {
    this.can_send_received = can_send_received;
  }

  /**
   * @hibernate.property column="CAN_SEND_RETURNED"
   */
  public Boolean getCanSendReturned() {
    return can_send_returned;
  }

  public void setCanSendReturned(Boolean can_send_returned) {
    this.can_send_returned = can_send_returned;
  }

  /**
   * @hibernate.property column="REQUESTER_SHIPPED"
   */
  public Long getRequesterShipped() {
    return requester_shipped;
  }

  public void setRequesterShipped(Long requester_shipped) {
    this.requester_shipped = requester_shipped;
  }

  /**
   * @hibernate.property column="REQUESTER_CHECKED_IN"
   */
  public Long getRequesterCheckedIn() {
    return requester_checked_in;
  }

  public void setRequesterCheckedIn(Long responder_received) {
    this.requester_checked_in = requester_checked_in;
  }


  /**
   * @hibernate.property column="RETRY_FLAG"
   */
  public boolean getRetryFlag() {
    return retry_flag;
  }

  public void setRetryFlag(boolean retry_flag) {
    this.retry_flag = retry_flag;
  }

  /**
   * @hibernate.property column="FORWARD_FLAG"
   */
  public boolean getForwardFlag() {
    return forward_flag;
  }

  public void setForwardFlag(boolean forward_flag) {
    this.forward_flag = forward_flag;
  }

  /**
   * @hibernate.property column="REQUESTER_NOTE" length="2000"
   */
  public String getRequesterNote() {
    return requester_note;
  }

  public void setRequesterNote(String requester_note) {
    this.requester_note = DBStringHelper.set(requester_note,2000);
  }

  /**
   * @hibernate.property column="FORWARD_NOTE" length="2000"
   */
  public String getForwardNote() {
    return forward_note;
  }

  public void setForwardNote(String forward_node) {
    this.forward_note = DBStringHelper.set(forward_note,2000);
  }

  /**
   * @hibernate.many-to-one  column="REQUIRED_ITEM_DETAILS" class="com.k_int.openrequest.db.RequiredItemDetails" outer-join="false" cascade="all"
   */
  public RequiredItemDetails getRequiredItemDetails() {
    return generic_details;
  }

  public void setRequiredItemDetails(RequiredItemDetails generic_details) {
    this.generic_details = generic_details;
  }

  /**
   * @hibernate.property column="PERM_TO_FORWARD"
   */
  public boolean getPermToForward() {
    return perm_to_forward;
  }

  public void setPermToForward(boolean perm_to_forward) {
    this.perm_to_forward = perm_to_forward;
  }

  /**
   * @hibernate.property column="PERM_TO_CHAIN"
   */
  public boolean getPermToChain() {
    return perm_to_chain;
  }

  public void setPermToChain(boolean perm_to_chain) {
    this.perm_to_chain = perm_to_chain;
  }

  /**
   * @hibernate.property column="PERM_TO_PARTITION"
   */
  public boolean getPermToPartition() {
    return perm_to_partition;
  }

  public void setPermToPartition(boolean perm_to_partition) {
    this.perm_to_partition = perm_to_partition;
  }

  /**
   * @hibernate.property column="PERM_TO_CHANGE_SEND_TO_LIST"
   */
  public boolean getPermToChangeSendToList() {
    return perm_to_change_send_to_list;
  }

  public void setPermToChangeSendToList(boolean perm_to_change_send_to_list) {
    this.perm_to_change_send_to_list = perm_to_change_send_to_list;
  }

  /**
   * @hibernate.property column="INIT_REQ_SERV_IDENT"
   */
  public String getInitReqServIdent() {
    return init_req_service_ident;
  }

  public void setInitReqServIdent(String init_req_service_ident) {
    this.init_req_service_ident = init_req_service_ident;
  }

  /**
   * @hibernate.property column="INIT_REQ_SERV_ADDR"
   */
  public String getInitReqServAddr() {
    return init_req_service_addr;
  }

  public void setInitReqServAddr(String init_req_service_addr) {
    this.init_req_service_addr = init_req_service_addr;
  }

  /**
   * @hibernate.property column="PREFERENCE"
   */
  public long getPreference() {
    return preference;
  }

  public void setPreference(long preference) {
    this.preference = preference;
  }

  public List getRota()
  {
    return rota;
  }

  public void setRota(List rota)
  {
    this.rota = rota;
  }

  /**
   * @hibernate.many-to-one  column="DELIVERY_ADDRESS" class="com.k_int.openrequest.db.ISO10161Address" outer-join="false" cascade="all"
   */
  public ISO10161Address getDeliveryAddress() {
    return delivery_address;
  }

  public void setDeliveryAddress(ISO10161Address delivery_address) {
    this.delivery_address = delivery_address;
  }

  /**
   * @hibernate.many-to-one  column="BILLING_ADDRESS" class="com.k_int.openrequest.db.ISO10161Address" outer-join="false" cascade="all"
   */
  public ISO10161Address getBillingAddress() {
    return billing_address;
  }

  public void setBillingAddress(ISO10161Address billing_address) {
    this.billing_address = billing_address;
  }

  /**
   * @hibernate.property column="OCLCILLService"
   */
  public Long getOCLCILLService() {
    return oclc_ill_service;
  }

  public void setOCLCILLService(Long oclc_ill_service) {
    this.oclc_ill_service = oclc_ill_service;
  }

}
