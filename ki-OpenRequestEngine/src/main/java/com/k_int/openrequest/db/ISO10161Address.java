package com.k_int.openrequest.db;

import com.k_int.openrequest.util.DBStringHelper;

/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161Address.java,v 1.2 2005/07/01 16:11:12 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_ISO10161_ADDRESS" dynmic-update="true" dynamic-insert="true"
 */
public class ISO10161Address implements Cloneable {

  public static final long NAME_OF_PERSON = 1;
  public static final long NAME_OF_ORGANISATION = 2;
  // private long id;
  private Long id;
  private long postal_name_type;  // 0=person,1=inst.
  private String postal_name;
  private String postal_extended_address;
  private String postal_street_and_number;
  private String postal_po_box;
  private String postal_city;
  private String postal_region;
  private String postal_country;
  private String postal_postcode;
  private String electronic_service_identifier;
  private String electronic_service_address;
  private String email;
  private String telephone;
  private String fax;

  public ISO10161Address() {
  }

  public ISO10161Address(long postal_name_type,
                         String postal_name,
                         String postal_extended_address,
                         String postal_street_and_number,
                         String postal_po_box,
                         String postal_city,
                         String postal_region,
                         String postal_country,
                         String postal_postcode,
                         String electronic_service_identifier,
                         String electronic_service_address)
  {
    this.postal_name_type = postal_name_type;
    this.postal_name = DBStringHelper.set(postal_name,255);
    this.postal_extended_address = DBStringHelper.set(postal_extended_address,255);
    this.postal_street_and_number = DBStringHelper.set(postal_street_and_number,255);
    this.postal_po_box = DBStringHelper.set(postal_po_box,255);
    this.postal_city = DBStringHelper.set(postal_city,255);
    this.postal_region = DBStringHelper.set(postal_region,255);
    this.postal_country = DBStringHelper.set(postal_country,255);
    this.postal_postcode = DBStringHelper.set(postal_postcode,255);
    this.electronic_service_identifier = DBStringHelper.set(electronic_service_identifier,255);
    this.electronic_service_address = DBStringHelper.set(electronic_service_address,255);
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="POSTAL_NAME_TYPE" 
   */
  public long getPostal_name_type() {
    return postal_name_type;
  }

  public void setPostal_name_type(long postal_name_type) {
    this.postal_name_type = postal_name_type;
  }

  /**
   * @hibernate.property column="POSTAL_NAME" length="255"
   */
  public String getPostal_name() {
    return postal_name;
  }

  public void setPostal_name(String postal_name) {
    this.postal_name=DBStringHelper.set(postal_name,255);
  }

  /**
   * @hibernate.property column="POSTAL_EXTENDED_ADDRESS" length="255"
   */
  public String getPostal_extended_address() {
    return postal_extended_address;
  }

  public void setPostal_extended_address(String postal_extended_address) {
    this.postal_extended_address=DBStringHelper.set(postal_extended_address,255);
  }

  /**
   * @hibernate.property column="POSTAL_STREET_AND_NUMBER" length="255"
   */
  public String getPostal_street_and_number() {
    return postal_street_and_number;
  }

  public void setPostal_street_and_number(String postal_street_and_number) {
    this.postal_street_and_number=DBStringHelper.set(postal_street_and_number,255);
  }

  /**
   * @hibernate.property column="POSTAL_PO_BOX" length="255"
   */
  public String getPostal_po_box() {
    return postal_po_box;
  }

  public void setPostal_po_box(String postal_po_box) {
    this.postal_po_box=DBStringHelper.set(postal_po_box,255);
  }

  /**
   * @hibernate.property column="POSTAL_CITY" length="255"
   */
  public String getPostal_city() {
    return postal_city;
  }

  public void setPostal_city(String postal_city) {
    this.postal_city=DBStringHelper.set(postal_city,255);
  }

  /**
   * @hibernate.property column="POSTAL_REGION" length="255"
   */
  public String getPostal_region() {
    return postal_region;
  }

  public void setPostal_region(String postal_region) {
    this.postal_region=DBStringHelper.set(postal_region,255);
  }

  /**
   * @hibernate.property column="POSTAL_COUNTRY" length="255"
   */
  public String getPostal_country() {
    return postal_country;
  }

  public void setPostal_country(String postal_country) {
    this.postal_country=DBStringHelper.set(postal_country,255);
  }

  /**
   * @hibernate.property column="POSTAL_POSTCODE" length="255"
   */
  public String getPostal_postcode() {
    return postal_postcode;
  }

  public void setPostal_postcode(String postal_postcode) {
    this.postal_postcode=DBStringHelper.set(postal_postcode,255);
  }

  /**
   * @hibernate.property column="ELECTRONIC_SERVICE_IDENTIFIER" length="255"
   */
  public String getElectronic_service_identifier() {
    return electronic_service_identifier;
  }

  public void setElectronic_service_identifier(String electronic_service_identifier) {
    this.electronic_service_identifier=DBStringHelper.set(electronic_service_identifier,255);
  }

  /**
   * @hibernate.property column="ELECTRONIC_SERVICE_ADDRESS" length="255"
   */
  public String getElectronic_service_address() {
    return electronic_service_address;
  }

  public void setElectronic_service_address(String electronic_service_address) {
    this.electronic_service_address=DBStringHelper.set(electronic_service_address,255);
  }

  /**
   * @hibernate.property column="EMAIL" length="255"
   */
  public String getEMail() {
    return email;
  }

  public void setEMail(String email) {
    this.email=DBStringHelper.set(email,255);
  }

  /**
   * @hibernate.property column="TELEPHONE" length="255"
   */
  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone=DBStringHelper.set(telephone,255);
  }

  /**
   * @hibernate.property column="FAX" length="255"
   */
  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax=DBStringHelper.set(fax,255);
  }

  public Object clone()
  {
    return new ISO10161Address(postal_name_type,
                               postal_name,
                               postal_extended_address,
                               postal_street_and_number,
                               postal_po_box,
                               postal_city,
                               postal_region,
                               postal_country,
                               postal_postcode,
                               electronic_service_identifier,
                               electronic_service_address);
  }

}
