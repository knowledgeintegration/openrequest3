package com.k_int.openrequest.db;

/**
 * Title:       StateModel
 * @version:    $Id: StateModel.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_STATE_MODEL" dynmic-update="true" dynamic-insert="true"
 */
public class StateModel {

  private Long id;
  private String code;
  private String name;

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="NAME"
   */
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  /**
   * @hibernate.property column="CODE"
   */
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
