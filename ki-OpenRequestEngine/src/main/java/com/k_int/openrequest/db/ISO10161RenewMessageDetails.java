package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


/**
 * Title:       ISO10161RenewMessageDetails
 * @version:    $Id: ISO10161RenewMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:REN"
 */
public class ISO10161RenewMessageDetails extends ISO10161v2Message {

  private Date desired_due_date;

  /**
   * @hibernate.property column="DESIRED_DUE_DATE"
   */
  public Date getDesiredDueDate() {
    return desired_due_date;
  }

  public void setDesiredDueDate(Date desired_due_date) {
    this.desired_due_date = desired_due_date;
  }
}
