package com.k_int.openrequest.db.Location;

/**
 * Title:       NamingAuthority
 * @version:    $Id: NamingAuthority.java,v 1.3 2005/06/11 14:02:25 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_NAMING_AUTHORITY" dynmic-update="true" dynamic-insert="true"
 */
public class NamingAuthority {

  private Long id;
  private String name;
  private String identifier;
  private String upper_id;

  public NamingAuthority() {
  }
  
  public NamingAuthority(String identifier, String name) {
    this.identifier = identifier;
    this.name = name;
    if ( identifier != null )
      this.upper_id = identifier.toUpperCase();
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="AUTHORITY_NAME"
   */
  public String getAuthorityName() {
    return name;
  }

  public void setAuthorityName(String name) {
    this.name = name;
  }

  /**
   * @hibernate.property column="IDENTIFIER" length="50"
   */
  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
    if ( identifier != null )
      this.upper_id = identifier.toUpperCase();
    else
      this.upper_id = null;
  }

  /**
   * @hibernate.property 
   * @hibernate.column name="UC_IDENTIFIER" length="50" index="UC_AUTH_ID_IDX"
   */
  public String getUpperIdentifier() {
    return upper_id;
  }

  public void setUpperIdentifier(String upper_id) {
    if ( upper_id != null )
      this.upper_id = upper_id.toUpperCase();
    else
      this.upper_id = null;
  }
}
