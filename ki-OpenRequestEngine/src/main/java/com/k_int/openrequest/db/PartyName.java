package com.k_int.openrequest.db;

/**
 * Title:       PartyName
 * @version:    $Id: PartyName.java,v 1.2 2005/06/17 18:52:15 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 * @hibernate.discriminator column="NAME_TYPE" type="string"
 */
public abstract class PartyName implements java.io.Serializable {

  protected String name;

  /**
   * @hibernate.property
   * @hibernate.column name="PARTY_NAME"
   */
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
