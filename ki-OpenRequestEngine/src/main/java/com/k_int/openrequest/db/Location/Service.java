package com.k_int.openrequest.db.Location;
import org.hibernate.*;

import java.util.*;

/**
 * Title:       Location
 * @version:    $Id: Service.java,v 1.3 2005/06/17 18:52:15 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_ILL_SYSTEM" dynmic-update="true" dynamic-insert="true"
 */
public class Service {

  private Long id;
  private String description;
  private String service_type;
  private long priority;
  private Map properties = new HashMap();
  private String handler_service_name;
  private String telecom_service_identifier;
  private String telecom_service_address;
  private int encoding;
  private int auth_method=0;
  private String username;
  private String password;

  public Service(String description, 
                 String service_type, 
                 String handler_service_name,
                 long priority,
                 int encoding) {
    this.description = description;
    this.service_type = service_type;
    this.handler_service_name = handler_service_name;
    this.priority = priority;
    this.encoding = encoding;
  }

  public Service() {
  }
  
  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="DESCRIPTION"
   */
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @hibernate.property column="SERVICE_TYPE"
   */
  public String getServiceType() {
    return service_type;
  }

  public void setServiceType(String service_type) {
    this.service_type = service_type;
  }

  /**
   * @hibernate.map table="OR_SERVICE_PROPERTIES" lazy="true" cascade="all"
   * @hibernate.collection-key  column="SERVICE_ID"
   * @hibernate.collection-index  column="PROP_NAME" type="java.lang.String" length="50"
   * @hibernate.collection-element type="java.lang.String" column="PROP_VALUE"
   */
  public Map getProperties() {
    return properties;
  }

  public void setProperties(Map properties) {
    this.properties = properties;
  }

  /**
   * @hibernate.property column="HANDLER_SERVICE_NAME"
   */
  public String getHandlerServiceName() {
    return handler_service_name;
  }

  public void setHandlerServiceName(String handler_service_name) {
    this.handler_service_name = handler_service_name;
  }

  /**
   * @hibernate.property column="PRIORITY"
   */
  public long getPriority() {
    return priority;
  }

  public void setPriority(long priority) {
    this.priority = priority;
  }

  /**
   * @hibernate.property column="TELECOM_SERVICE_IDENTIFIER"
   */
  public String getTelecomServiceIdentifier() {
    return telecom_service_identifier;
  }

  public void setTelecomServiceIdentifier(String telecom_service_identifier) {
    this.telecom_service_identifier = telecom_service_identifier;
  }

  /**
   * @hibernate.property column="TELECOM_SERVICE_ADDRESS"
   */
  public String getTelecomServiceAddress() {
    return telecom_service_address;
  }

  public void setTelecomServiceAddress(String telecom_service_address) {
    this.telecom_service_address = telecom_service_address;
  }

  /**
   * @hibernate.property column="ENCODING"
   */
  public int getEncoding() {
    return encoding;
  }

  public void setEncoding(int encoding) {
    this.encoding = encoding;
  }

  /**
   * @hibernate.property column="AUTH_METHOD"
   */
  public int getAuthMethod() {
    return auth_method;
  }

  public void setAuthMethod(int auth_method) {
    this.auth_method = auth_method;
  }

  /**
   * @hibernate.property column="USERNAME"
   */
  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * @hibernate.property column="PASSWORD"
   */
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public static Service lookupOrCreate(Session session, String addr_type, String addr) {
    Service result = null;

    Query q = session.createQuery("select s from com.k_int.openrequest.db.Location.Service s where s.telecomServiceAddress = ?");
    q.setParameter(0,addr_type,Hibernate.STRING);
    result = (Service) q.uniqueResult();

    if ( result == null ) {
      if ( addr_type.equalsIgnoreCase("M") ) {
        result = new Service("Mime System Address "+addr, "ILL/MIME", "MIME", 0, 2);
        result.setTelecomServiceIdentifier("SMTP");
        result.setTelecomServiceAddress(addr);
        session.save(result);
      }
      else if ( addr_type.equalsIgnoreCase("T") ) {
        System.err.print("Service TCP Address (host:port) : ");
        result = new Service("TCP System Address "+addr, "ILL/DUPLEX", "DUPLEXTCP", 0, 3);
        result.setTelecomServiceIdentifier("TCP");
        result.setTelecomServiceAddress(addr);
        session.save(result);
      }
      else {
        System.out.println("Unknown type: "+addr_type);
      }
    }

    return result;
  }
}
