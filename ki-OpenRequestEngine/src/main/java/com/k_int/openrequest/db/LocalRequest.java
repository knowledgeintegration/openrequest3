/**
 * Title:       LocalRequest
 * @version:    $Id: LocalRequest.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.db;

import java.util.Hashtable;
import com.k_int.openrequest.db.*;
import com.k_int.openrequest.api.*;
import java.util.Date;
import com.k_int.openrequest.util.DBStringHelper;


public class LocalRequest
{
  private Long id;
  private Date creation_date;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Date getCreationDate()
  {
    return creation_date;
  }

  public void setCreationDate(Date creation_date)
  {
    this.creation_date = creation_date;
  }
}
