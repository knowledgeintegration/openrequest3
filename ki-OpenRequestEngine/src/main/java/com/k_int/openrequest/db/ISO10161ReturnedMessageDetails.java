package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


/**
 * Title:       ISO10161RequestMessageDetails
 * @version:    $Id: ISO10161ReturnedMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:RET"
 */
public class ISO10161ReturnedMessageDetails extends ISO10161v2Message {

  private Date date_returned;
  private String returned_via;
  private String insured_for_monetary_value;
  private String insured_for_currency_code;

  /**
   * @hibernate.property column="DATE_RETURNED"
   */
  public Date getDateReturned() {
    return date_returned;
  }

  public void setDateReturned(Date date_returned) {
    this.date_returned = date_returned;
  }

  /**
   * @hibernate.property column="RETURNED_VIA" length="300"
   */
  public String getReturnedVia() {
    return returned_via;
  }

  public void setReturnedVia(String returned_via) {
    this.returned_via = DBStringHelper.set(returned_via,300);
  }

  /**
   * @hibernate.property column="INSURED_FOR_CURRENCY_CODE" length="20"
   */
  public String getInsuredForCurrencyCode() {
    return insured_for_currency_code;
  }

  public void setInsuredForCurrencyCode(String insured_for_currency_code) {
    this.insured_for_currency_code = DBStringHelper.set(insured_for_currency_code,20);
  }

  /**
   * @hibernate.property column="INSURED_FOR_MONETARY_VALUE" length="20"
   */
  public String getInsuredForMonetaryValue() {
    return insured_for_monetary_value;
  }

  public void setInsuredForMonetaryValue(String insured_for_monetary_value) {
    this.insured_for_monetary_value = DBStringHelper.set(insured_for_monetary_value,20);
  }
}
