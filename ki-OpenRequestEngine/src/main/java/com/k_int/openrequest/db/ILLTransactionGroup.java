package com.k_int.openrequest.db;

import com.k_int.openrequest.db.Location.*;
import java.util.*;
import org.hibernate.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.k_int.openrequest.helpers.SystemHelper;
import com.k_int.openrequest.util.DBStringHelper;
import org.springframework.context.*;

/**
 * Title:       TransactionGroup
 * @version:    $Id: ILLTransactionGroup.java,v 1.8 2005/07/01 16:11:12 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_ILL_TRANSACTION_GROUP" dynmic-update="true" dynamic-insert="true"
 */

public class ILLTransactionGroup {

  private Log log = LogFactory.getLog(this.getClass());

  private Long id;
  private String requester_symbol;
  private String initial_requester_symbol;
  private String tgq;
  private List rota = new ArrayList();
  private long current_rota_position;
  private Location location;
  private Boolean active;
  private RequiredItemDetails generic_request_details;
  private String requester_reference_authority;
  private String requester_reference;
  private String responder_reference_authority;
  private String responder_reference;
  private Date date_created;
  private String role;
  private Map custom_attrs = new HashMap();
  private ILLTransaction last_transaction = null;
  private StateModel active_state_model = null;
  protected StatusCode active_status_code = null;
  protected StatusCode previous_status_code = null;

  private Date need_by_date;
  private String need_by_comment;

  private ISO10161Address current_delivery_address;
  private ISO10161Address current_billing_address;

  private String patron_name;
  private String patron_status;
  private String patron_symbol;

  private long previous_unread_message_count;
  private long unread_message_count;
  private String requester_note;
  private Date last_message_date;

  private String initiating_branch_code;
  private Set transactions = new HashSet();

  private int close_reason = 0;

  // Override values for Requester optional messages
  private Boolean can_send_received;
  private Boolean can_send_returned;
  private Long requester_shipped;
  private Long requester_checked_in;

  public String client_department = null;
  public String payment_method = null;
  public String affiliations = null;

  public ILLTransactionGroup() {
  }

  public ILLTransactionGroup(Location location,
                             String initial_requester_symbol,
                             String tgq,
                             String role) {
    this.location=location;
    this.initial_requester_symbol = DBStringHelper.set(initial_requester_symbol,25);
    this.tgq = DBStringHelper.set(tgq,25);
    this.role = DBStringHelper.set(role,10);
  }

  /**
   * @hibernate.id  generator-class="native" column="ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.property column="REQUESTER_SYMBOL" length="25"
   */
  public String getRequesterSymbol() {
    return requester_symbol;
  }

  public void setRequesterSymbol(String requester_symbol) {
    this.requester_symbol = DBStringHelper.set(requester_symbol,25);
  }

  /**
   * @hibernate.property column="INITIAL_REQUESTER_SYMBOL" length="25"
   */
  public String getInitialRequesterSymbol() {
    return initial_requester_symbol;
  }

  public void setInitialRequesterSymbol(String initial_requester_symbol) {
    this.initial_requester_symbol = initial_requester_symbol;
  }

  /**
   * @hibernate.property column="TGQ" length="25"
   */
  public String getTGQ() {
    return tgq;
  }

  public void setTGQ(String tgq) {
    this.tgq=DBStringHelper.set(tgq,25);
  }

  public List getRota()
  {
    return rota;
  }

  public void setRota(List rota)
  {
    this.rota = rota;
  }

  /**
   * @hibernate.property column="CURRENT_ROTA_POSITION"
   */
  public long getCurrentRotaPosition() {
    return current_rota_position;
  }

  public void setCurrentRotaPosition(long current_rota_position) {
    this.current_rota_position = current_rota_position;
  }

  /**
   * @hibernate.many-to-one  column="LOCATION" class="com.k_int.openrequest.db.Location.Location" cascade="save-update"
   */
  public Location getLocation()
  {
    return location;
  }

  public void setLocation(Location location)
  {
    this.location = location;
  }

  /**
   * @hibernate.property column="ACTIVE"
   */
  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  /**
   * @hibernate.many-to-one  column="REQUIRED_ITEM_DETAILS" class="com.k_int.openrequest.db.RequiredItemDetails" cascade="all"
   */
  public RequiredItemDetails getRequiredItemDetails() {
    return generic_request_details;
  }

  public void setRequiredItemDetails(RequiredItemDetails generic_request_details) {
    this.generic_request_details = generic_request_details;
  }

  /**
   * @hibernate.property column="REQUESTER_REFERENCE_AUTHORITY" length="100"
   */
  public String getRequesterReferenceAuthority() {
    return requester_reference_authority;
  }

  public void setRequesterReferenceAuthority(String requester_reference_authority) {
    this.requester_reference_authority = DBStringHelper.set(requester_reference_authority,100);
  }

  /**
   * @hibernate.property column="REQUESTER_REFERENCE" length="100"
   */
  public String getRequesterReference() {
    return requester_reference;
  }

  public void setRequesterReference(String requester_reference) {
    this.requester_reference = requester_reference;
  }

  /**
   * @hibernate.property column="RESPONDER_REFERENCE_AUTHORITY" length="100"
   */
  public String getResponderReferenceAuthority() {
    return responder_reference_authority;
  }

  public void setResponderReferenceAuthority(String responder_reference_authority) {
    this.responder_reference_authority = DBStringHelper.set(responder_reference_authority,100);
  }

  /**
   * @hibernate.property column="RESPONDER_REFERENCE" length="100"
   */
  public String getResponderReference() {
    return responder_reference;
  }

  public void setResponderReference(String responder_reference) {
    this.responder_reference = DBStringHelper.set(responder_reference,100);
  }

  /**
   * @hibernate.property column="DATE_CREATED"
   */
  public Date getDateCreated() {
    return date_created;
  }

  public void setDateCreated(Date date_created) {
    this.date_created = date_created;
  }

  /**
   * @hibernate.property column="ILL_ROLE" length="10"
   */
  public String getRole()
  {
    return role;
  }

  public void setRole(String role) {
    this.role = DBStringHelper.set(role,10);
  }

  /**
   * @hibernate.map table="OR_TG_CUSTOM_ATTRS" lazy="true" cascade="all"
   * @hibernate.collection-key  column="TG_ID"
   * @hibernate.collection-index  column="CUSTOM_ATTR_NAME" type="java.lang.String" length="25"
   * @hibernate.collection-element type="java.lang.String" column="CUSTOM_ATTR_VALUE"
   */
  public Map getCustomAttrs() {
    return custom_attrs;
  }

  public void setCustomAttrs(Map custom_attrs) {
    this.custom_attrs = custom_attrs;
  }

  /**
   * @hibernate.many-to-one  column="LAST_TRANSACTION" class="com.k_int.openrequest.db.ILLTransaction" cascade="save-update" outer-join="true"
   */
  public ILLTransaction getLastTransaction() {
    return last_transaction;
  }

  public void setLastTransaction(ILLTransaction last_transaction) {
    this.last_transaction = last_transaction;
  }

  /**
   * @hibernate.many-to-one  column="TG_STATE_MODEL" class="com.k_int.openrequest.db.StateModel"
   */
  public StateModel getActiveStateModel() {
    return active_state_model;
  }

  public void setActiveStateModel(StateModel active_state_model) {
    this.active_state_model = active_state_model;
  }

  /**
   * @hibernate.many-to-one  column="TG_STATUS_CODE" class="com.k_int.openrequest.db.StatusCode"
   */
  public StatusCode getTransactionGroupStatusCode()
  {
    return active_status_code;
  }

  public void setTransactionGroupStatusCode(StatusCode active_status_code)
  {
    this.previous_status_code = this.active_status_code;
    this.active_status_code = active_status_code;
  }

  /**
   * @hibernate.many-to-one  column="CURRENT_DELIVERY_ADDRESS" class="com.k_int.openrequest.db.ISO10161Address" cascade="all"
   */
  public ISO10161Address getCurrentDeliveryAddress() {
    return current_delivery_address;
  }

  public void setCurrentDeliveryAddress(ISO10161Address current_delivery_address) {
    this.current_delivery_address = current_delivery_address;
  }

  /**
   * @hibernate.many-to-one  column="CURRENT_BILLING_ADDRESS" class="com.k_int.openrequest.db.ISO10161Address" cascade="all"
   */
  public ISO10161Address getCurrentBillingAddress() {
    return current_billing_address;
  }

  public void setCurrentBillingAddress(ISO10161Address current_billing_address) {
    this.current_billing_address = current_billing_address;
  }

  /**
   * @hibernate.property column="NEED_BY_DATE"
   */
  public Date getNeedByDate() {
    return need_by_date;
  }
    
  public void setNeedByDate(Date need_by_date) {
    this.need_by_date=need_by_date;
  }

  /**
   * @hibernate.property column="NEED_BY_COMMENT" length="100"
   */
  public String getNeedByComment() {
    return need_by_comment;
  }
    
  public void setNeedByComment(String need_by_comment) {
    this.need_by_comment=DBStringHelper.set(need_by_comment,100);
  }

  /**
   * @hibernate.property column="PATRON_NAME" length="100"
   */
  public String getPatronName() {
    return patron_name;
  }
    
  public void setPatronName(String patron_name) {
    this.patron_name=DBStringHelper.set(patron_name,100);
  }

  /**
   * @hibernate.property column="PATRON_STATUS" length="40"
   */
  public String getPatronStatus()
  {
    return patron_status;
  }
    
  public void setPatronStatus(String patron_status)
  {
    this.patron_status=DBStringHelper.set(patron_status,40);
  }

  /**
   * @hibernate.property column="PATRON_SYMBOL" length="50"
   */
  public String getPatronSymbol() {
    return patron_symbol;
  }
    
  public void setPatronSymbol(String patron_symbol) {
    this.patron_symbol=DBStringHelper.set(patron_symbol,50);
  }

  /**
   * @hibernate.property column="UNREAD_MESSAGE_COUNT"
   */
  public long getUnreadMessageCount() {
    return unread_message_count;
  }

  public void setUnreadMessageCount(long unread_message_count) {
    this.previous_unread_message_count = this.unread_message_count;
    this.unread_message_count = unread_message_count;
  }

  /**
   * @hibernate.property column="REQUESTER_NOTE" length="3000"
   */
  public String getRequesterNote() {
    return requester_note;
  }
    
  public void setRequesterNote(String requester_note) {
    this.requester_note=DBStringHelper.set(requester_note,3000);
  }

  /**
   * @hibernate.property column="LAST_MESSAGE_DATE"
   */
  public Date getLastMessageDate() {
    return last_message_date;
  }

  public void setLastMessageDate(Date last_message_date) {
    this.last_message_date = last_message_date;
  }

  /**
   * @hibernate.property column="INITIATING_BRANCH_CODE" length="100"
   */
  public String getInitiatingBranchCode()
  {
    return initiating_branch_code;
  }
    
  public void setInitiatingBranchCode(String initiating_branch_code)
  {
    this.initiating_branch_code=DBStringHelper.set(initiating_branch_code,100);
  }


  /**
   * @hibernate.set cascade="all" lazy="true" table="OR_ILL_TRANSACTION" order-by="REQUEST_ID ASC"
   * @hibernate.collection-key column="TRANSACTION_GROUP"
   * @hibernate.collection-one-to-many class="com.k_int.openrequest.db.ILLTransaction"
   */
  public Set getTransactions() {
    return transactions;
  }

  public void setTransactions(Set transactions) {
    this.transactions = transactions;
  }

  /**
   * @hibernate.property column="CLOSE_REASON"
   */
  public int getCloseReason() {
    return close_reason;
  }

  public void setCloseReason(int close_reason) {
    this.close_reason = close_reason;
  }

  /**
   * @hibernate.property column="CAN_SEND_RECEIVED"
   */
  public Boolean getCanSendReceived() {
    return can_send_received;
  }

  public void setCanSendReceived(Boolean can_send_received) {
    this.can_send_received = can_send_received;
  }

  /**
   * @hibernate.property column="CAN_SEND_RETURNED"
   */
  public Boolean getCanSendReturned() {
    return can_send_returned;
  }

  public void setCanSendReturned(Boolean can_send_returned) {
    this.can_send_returned = can_send_returned;
  }

  /**
   * @hibernate.property column="REQUESTER_SHIPPED"
   */
  public Long getRequesterShipped() {
    return requester_shipped;
  }

  public void setRequesterShipped(Long requester_shipped) {
    this.requester_shipped = requester_shipped;
  }

  /**
   * @hibernate.property column="REQUESTER_CHECKED_IN"
   */
  public Long getRequesterCheckedIn() {
    return requester_checked_in;
  }

  public void setRequesterCheckedIn(Long requester_checked_in) {
    this.requester_checked_in = requester_checked_in;
  }

  /**
   * @hibernate.property 
   * @hibernate.column name="CLIENT_DEPT" length="100"
   */
  public String getClientDepartment() {
    return client_department;
  }
    
  public void setClientDepartment(String client_department) {
    this.client_department=DBStringHelper.set(client_department,100);
  }

  /**
   * @hibernate.property
   * @hibernate.column name="PAYMENT_METHOD" length="100"
   */
  public String getPaymentMethod() {
    return payment_method;
  }

  public void setPaymentMethod(String payment_method) {
    this.payment_method=DBStringHelper.set(payment_method,100);
  }

  /**
   * @hibernate.property
   * @hibernate.column name="AFFILIATIONS" length="100"
   */
  public String getAffiliations() {
    return affiliations;
  }

  public void setAffiliations(String affiliations) {
    this.affiliations=DBStringHelper.set(affiliations,100);
  }


  // Non property methods....
  public void incrementUnreadMessageCount()
  {
    unread_message_count++;
    setUnreadMessageCount(unread_message_count);
  }

  public void decrementUnreadMessageCounter()
  {
    unread_message_count--;
    setUnreadMessageCount(unread_message_count);
  }

  // public void onLoad(org.hibernate.Session s, java.io.Serializable id)
  // {
  //   this.previous_status_code = this.active_status_code;
  //   this.previous_unread_message_count = this.previous_unread_message_count;
  // }

  private void checkForChanges(ApplicationContext ctx) {
    log.debug("Comparing "+this.active_status_code+" and "+this.previous_status_code );

    if ( ( this.active_status_code != this.previous_status_code ) ||
         ( this.unread_message_count != this.previous_unread_message_count ) )
    {
      Map vars = new HashMap();
      vars.put("type","TG_CHANGE");
      vars.put("role",role);

      if ( ( last_transaction != null ) && ( last_transaction.active_status_code != null ) )
        vars.put("trans_state", last_transaction.active_status_code.getCode());

      if ( ( last_transaction != null ) && ( last_transaction.previous_status_code != null ) )
        vars.put("old_trans_state", last_transaction.previous_status_code.getCode());

      if ( active_status_code != null )
        vars.put("tg_state", active_status_code.getCode());

      if ( previous_status_code != null )
        vars.put("old_tg_state", previous_status_code.getCode());

      vars.put("tg_unread", new Integer((int)unread_message_count));
      vars.put("old_tg_unread", new Integer((int)previous_unread_message_count));

      log.debug("update prev status code and message count");
      this.previous_status_code = this.active_status_code;
      this.unread_message_count = this.previous_unread_message_count;

      SystemHelper.announceTGChanged(ctx, vars, this);
    }
  }

  public void notifyLoad(ApplicationContext ctx) {
    this.previous_status_code = this.active_status_code;
    this.previous_unread_message_count = this.previous_unread_message_count;
    log.debug("notifyLoad() - prev_status_code set to "+this.active_status_code+" pref_umc set to "+this.previous_unread_message_count);
  }

  public void notifyPreUpdate(ApplicationContext ctx) {
    log.debug("notifyPreUpdate()");
    checkForChanges(ctx);
  }

  public static ILLTransactionGroup lookupTGById(Session s, Long id) {
    return (ILLTransactionGroup) s.createQuery("Select tg from com.k_int.openrequest.db.ILLTransactionGroup tg where tg.id=?")
                                     .setParameter(0,id,Hibernate.LONG)
                                     .uniqueResult();
  }
}
