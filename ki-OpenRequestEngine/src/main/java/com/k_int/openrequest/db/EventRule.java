/**
 * Title:       StatusCode
 * @version:    $Id: EventRule.java,v 1.1.1.1 2005/02/14 11:46:29 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.db;

public class EventRule {
  private Long id;
  private String desc;
  private StateModel model;
  private StatusCode from_state;
  private String transition_code;

  private String action_handler_type;
  private String handler_params;
  private String message_type;
  private String message_params;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getDescription()
  {
    return desc;
  }

  public void setDescription(String desc)
  {
    this.desc = desc;
  }

  public StatusCode getFrom_state()
  {
    return from_state;
  }

  public void setFrom_state(StatusCode from_state)
  {
    this.from_state=from_state;
  }

  public StateModel getModel()
  {
    return model;
  }

  public void setModel(StateModel model)
  {
    this.model=model;
  }

  public String getMessageType()
  {
    return message_type;
  }
 
  public void setMessageType(String message_type)
  {
    this.message_type = message_type;
  }

  // public Object[] getMessageParams()
  public String getMessageParams()
  {
    return message_params;
  }

  // public void setMessageParams(Object[] message_params)
  public void setMessageParams(String message_params)
  {
    this.message_params = message_params;
  }

  public String getActionHandlerType()
  {
    return action_handler_type;
  }
 
  public void setActionHandlerType(String action_handler_type)
  {
    this.action_handler_type = action_handler_type;
  }

  // public Object[] getActionHandlerParams()
  public String getActionHandlerParams()
  {
    return handler_params;
  }

  // public void setActionHandlerParams(Object[] handler_params)
  public void setActionHandlerParams(String handler_params)
  {
    this.handler_params = handler_params;
  }

  public String getTransitionCode()
  {
    return transition_code;
  }

  public void setTransitionCode(String transition_code)
  {
    this.transition_code = transition_code;
  }
}
