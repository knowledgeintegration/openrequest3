package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;

/**
 * Title:       Extension
 * @version:    $Id: IPIGILLRequestExtension.java,v 1.3 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002-2005 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynmic-update="true" dynamic-insert="true" discriminator-value="IPIG:IRE"
 */
public class IPIGILLRequestExtension extends Extension {

  public String client_department;
  public String payment_method;
  public String uniform_title;
  public String dissertation;
  public String issue_number;
  public String volume;
  public String affiliations;
  public String source;

  public IPIGILLRequestExtension() {
  }

  public IPIGILLRequestExtension(String client_department,
                                 String payment_method,
                                 String uniform_title,
                                 String dissertation,
                                 String issue_number,
                                 String volume,
                                 String affiliations,
                                 String source) {
    this.client_department= DBStringHelper.set(client_department,100);
    this.payment_method= DBStringHelper.set(payment_method,100);
    this.uniform_title= DBStringHelper.set(uniform_title,100);
    this.dissertation= DBStringHelper.set(dissertation,100);
    this.issue_number= DBStringHelper.set(issue_number,100);
    this.volume= DBStringHelper.set(volume,100);
    this.affiliations= DBStringHelper.set(affiliations,100);
    this.source= DBStringHelper.set(source,100);
  }

  /**
   * @hibernate.property column="CLIENT_DEPARTMENT" length="100"
   */
  public String getClientDepartment() {
    return client_department;
  }

  public void setClientDepartment(String client_department) {
    this.client_department=DBStringHelper.set(client_department,100);
  }

  /**
   * @hibernate.property column="PAYMENT_METHOD" length="100"
   */
  public String getPaymentMethod() {
    return payment_method;
  }

  public void setPaymentMethod(String payment_method) {
    this.payment_method=DBStringHelper.set(payment_method,100);
  }

  /**
   * @hibernate.property column="UNIFORM_TITLE" length="100"
   */
  public String getUniformTitle() {
    return uniform_title;
  }

  public void setUniformTitle(String uniform_title) {
    this.uniform_title=DBStringHelper.set(uniform_title,100);
  }

  /**
   * @hibernate.property column="DISSERTATION" length="100"
   */
  public String getDissertation() {
    return dissertation;
  }

  public void setDissertation(String dissertation) {
    this.dissertation=DBStringHelper.set(dissertation,100);
  }

  /**
   * @hibernate.property column="ISSUE_NUMBER" length="100"
   */
  public String getIssueNumber() {
    return issue_number;
  }

  public void setIssueNumber(String issue_number) {
    this.issue_number=DBStringHelper.set(issue_number,100);
  }

  /**
   * @hibernate.property column="VOLUME" length="100"
   */
  public String getVolume() {
    return volume;
  }

  public void setVolume(String volume) {
    this.volume=DBStringHelper.set(volume,100);
  }

  /**
   * @hibernate.property column="AFFILIATIONS" length="100"
   */
  public String getAffiliations() {
    return affiliations;
  }

  public void setAffiliations(String affiliations) {
    this.affiliations=DBStringHelper.set(affiliations,100);
  }

  /**
   * @hibernate.property column="SOURCE" length="100"
   */
  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source=DBStringHelper.set(source,100);
  }
}
