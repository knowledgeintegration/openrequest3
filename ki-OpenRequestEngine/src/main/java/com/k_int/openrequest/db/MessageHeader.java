package com.k_int.openrequest.db;

import java.util.*;
import java.sql.Timestamp;
import com.k_int.openrequest.util.DBStringHelper;

/**
 * Title:       MessageHeader
 * @version:    $Id: MessageHeader.java,v 1.5 2005/07/01 16:11:12 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.class  table="OR_MESSAGE" dynmic-update="true" dynamic-insert="true" discriminator-value="HEADER" lazy="false"
 * @hibernate.discriminator column="MESSAGE_TYPE" type="string"
 */
public class MessageHeader {

  private Long id;
  private ILLTransaction parent;
  private String message_type = null;

  private static final long PERSON_TYPE=0;
  private static final long INSTITUTION_TYPE=1;

  // ISO ILL Application Messages all share a common set of elements... They are:
  // Protocol Version
  // Transaction ID
  //   Initial Requester id ( System Id )
  //     PersonOrInstitutionSymbol
  //     Name of Person or Institution
  //   tgq
  //   tq
  //   stq
  //   Service Date Time
  //   Requester Id (System Id)
  //     Person or Instition Symbol
  //     Name of person or institution
  //   Responder Id (System Id)
  //     Person or Instition Symbol
  //     Name of person or institution
  private long protocol_version_num;
  private long initial_requester_id_symbol_type; // 0:Person or 1:Institution
  private String initial_requester_id_symbol;
  private long initial_requester_name_type; // 0:Person or 1:Institution
  private String initial_requester_name;
  private String tgq;
  private String tq;
  private String stq;
  private String str_service_date;
  private String str_service_time;
  private String str_original_service_date;
  private String str_original_service_time;

  private long requester_id_symbol_type;
  private String requester_id_symbol;
  private long requester_id_name_type;
  private String requester_id_name;

  private long responder_id_symbol_type;
  private String responder_id_symbol;
  private long responder_id_name_type;
  private String responder_id_name;
  private char message_status = 'U';

  private com.k_int.openrequest.db.StatusCode from_state;
  private com.k_int.openrequest.db.StatusCode to_state;
  private String event_code;

  private Timestamp timestamp;

  private List extensions = new ArrayList();

  private boolean read;
  private Boolean repeat = Boolean.FALSE;

  private String note;
  private java.io.Serializable message_data;

  public MessageHeader() {
  }

  /**
   * @hibernate.id  generator-class="native" column="MESSAGE_ID"
   */
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @hibernate.many-to-one  column="PARENT_REQUEST" class="com.k_int.openrequest.db.ILLTransaction" outer-join="false"
   */
  public ILLTransaction getParentRequest() {
    return parent;
  }

  public void setParentRequest(ILLTransaction parent) {
    this.parent = parent;
  }

  public String getMessageType() {
    return message_type;
  }

  public void setMessageType(String message_type) {
    this.message_type = message_type;
  }

  public long getProtocolVersionNumber()
  {
    return protocol_version_num;
  }

  public void setProtocolVersionNumber(long protocol_version_num)
  {
    this.protocol_version_num=protocol_version_num;
  }

  /**
   * @hibernate.property column="INIT_REQ_ID_SYM_TYPE"
   */
  public long getInitial_requester_id_symbol_type() {
    return initial_requester_id_symbol_type;
  }

  public void setInitial_requester_id_symbol_type(long value) {
    this.initial_requester_id_symbol_type = value;
  }

  /**
   * @hibernate.property column="INIT_REQ_ID_SYM" length="25"
   */
  public String getInitial_requester_id_symbol() {
    return initial_requester_id_symbol;
  }

  public void setInitial_requester_id_symbol(String value) {
    this.initial_requester_id_symbol = DBStringHelper.set(value,25);
  }

  /**
   * @hibernate.property column="INIT_REQ_NAME_TYPE"
   */
  public long getInitial_requester_name_type() {
    return initial_requester_name_type;
  }

  public void setInitial_requester_name_type(long value) {
    this.initial_requester_name_type = value;
  }

  /**
   * @hibernate.property column="INIT_REQ_NAME" length="25"
   */
  public String getInitial_requester_name() {
    return initial_requester_name;
  }

  public void setInitial_requester_name(String value) {
    this.initial_requester_name = DBStringHelper.set(value,25);
  }

  /**
   * @hibernate.property column="TGQ" length="25"
   */
  public String getTgq() {
    return tgq;
  }

  public void setTgq(String value) { 
    this.tgq = DBStringHelper.set(value,25);
  }

  /**
   * @hibernate.property column="TQ" length="25"
   */
  public String getTq() {
    return tq;
  }

  public void setTq(String value) {
    this.tq = DBStringHelper.set(value,25);
  }

  /**
   * @hibernate.property column="STQ" length="25"
   */
  public String getStq() {
    return stq;
  }

  public void setStq(String value) {
    this.stq = DBStringHelper.set(value,25);
  }

  /**
   * @hibernate.property column="STR_SERVICE_DATE" length="10"
   */
  public String getStr_service_date() {
    return str_service_date;
  }

  public void setStr_service_date(String value) {
    this.str_service_date = DBStringHelper.set(value,10);
  }

  /**
   * @hibernate.property column="STR_SERVICE_TIME" length="6"
   */
  public String getStr_service_time() {
    return str_service_time;
  }

  public void setStr_service_time(String value) {
    this.str_service_time = DBStringHelper.set(value,6);
  }

  /**
   * @hibernate.property column="STR_ORIGINIAL_SERVICE_DATE" length="10"
   */
  public String getStr_original_service_date() {
    return str_original_service_date;
  }

  public void setStr_original_service_date(String value) {
    this.str_original_service_date= DBStringHelper.set(value,10);
  }

  /**
   * @hibernate.property column="STR_ORIGINIAL_SERVICE_TIME" length="6"
   */
  public String getStr_original_service_time() {
    return str_original_service_time;
  }

  public void setStr_original_service_time(String value) {
    this.str_original_service_time= DBStringHelper.set(value,6);
  }

  /**
   * @hibernate.property column="REQUESTER_ID_SYMBOL_TYPE"
   */
  public long getRequester_id_symbol_type() {
    return requester_id_symbol_type;
  }

  public void setRequester_id_symbol_type(long value) {
    this.requester_id_symbol_type = value;
  }

  /**
   * @hibernate.property column="REQUESTER_ID_SYMBOL" length="25"
   */
  public String getRequester_id_symbol() {
    return requester_id_symbol;
  }

  public void setRequester_id_symbol(String value) {
    this.requester_id_symbol = DBStringHelper.set(value,25);
  }

  /**
   * @hibernate.property column="REQUESTER_ID_NAME_TYPE"
   */
  public long getRequester_id_name_type() {
    return requester_id_name_type;
  }

  public void setRequester_id_name_type(long value) {
    this.requester_id_name_type = value;
  }

  /**
   * @hibernate.property column="REQUESTER_ID_NAME" length="25"
   */
  public String getRequester_id_name() {
    return requester_id_name;
  }

  public void setRequester_id_name(String value) {
    this.requester_id_name = DBStringHelper.set(value,25);
  }

  /**
   * @hibernate.property column="RESPONDER_ID_SYMBOL_TYPE"
   */
  public long getResponder_id_symbol_type() {
    return responder_id_symbol_type;
  }

  public void setResponder_id_symbol_type(long value) {
    this.responder_id_symbol_type = value;
  }

  /**
   * @hibernate.property column="RESPONDER_ID_SYMBOL" length="25"
   */
  public String getResponder_id_symbol() {
    return responder_id_symbol;
  }

  public void setResponder_id_symbol(String value) {
    this.responder_id_symbol = DBStringHelper.set(value,25);
  }

  /**
   * @hibernate.property column="RESPONDER_ID_NAME_TYPE"
   */
  public long getResponder_id_name_type() {
    return responder_id_name_type;
  }

  public void setResponder_id_name_type(long value) {
    this.responder_id_name_type = value;
  }

  /**
   * @hibernate.property column="RESPONDER_ID_NAME" length="25"
   */
  public String getResponder_id_name() {
    return responder_id_name;
  }

  public void setResponder_id_name(String value) {
    this.responder_id_name = DBStringHelper.set(value,25);
  }

  /**
   * don't bother with this now : table="MESSAGE_EXTENSIONS"
   * @hibernate.list cascade="all" lazy="true"
   * @hibernate.collection-key column="MESSAGE_ID"
   * @hibernate.collection-index column="POS"
   * @hibernate.collection-one-to-many class="com.k_int.openrequest.db.Extension"
   */
  public List getExtensions() {
    return extensions;
  }

  public void setExtensions(List extensions) {
    this.extensions = extensions;
  }

  /**
   * @hibernate.property column="MESSAGE_STATUS" length="1"
   *
   * D:Draft, S:Sent, R:Received, A:Acknowledged
   */
  public char getMessageStatus() {
    return message_status;
  }

  public void setMessageStatus(char message_status) {
    this.message_status = message_status;
  }

  /**
   * @hibernate.many-to-one  column="FROM_STATE" class="com.k_int.openrequest.db.StatusCode" outer-join="false"
   */
  public com.k_int.openrequest.db.StatusCode getFromState() {
    return from_state;
  }

  public void setFromState(com.k_int.openrequest.db.StatusCode from_state) {
    this.from_state = from_state;
  }

  /**
   * @hibernate.many-to-one  column="TO_STATE" class="com.k_int.openrequest.db.StatusCode" outer-join="false"
   */
  public com.k_int.openrequest.db.StatusCode getToState() {
    return to_state;
  }

  public void setToState(com.k_int.openrequest.db.StatusCode to_state) {
    this.to_state = to_state;
  }

  /**
   * @hibernate.property column="MSG_TIMESTAMP"
   */
  public Timestamp getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Timestamp timestamp) {
    this.timestamp = timestamp;
  }

  /**
   * @hibernate.property column="EVENT_CODE" length="20"
   */
  public String getEventCode() {
    return event_code;
  }

  public void setEventCode(String event_code) {
    this.event_code = DBStringHelper.set(event_code,20);
  }

  /**
   * @hibernate.property column="IS_READ"
   */
  public boolean getRead() {
    return read;
  }

  public void setRead(boolean read) {
    this.read = read;
  }

  /**
   * @hibernate.property column="IS_REPEAT"
   */
  public Boolean getRepeat() {
    return repeat;
  }

  public void setRepeat(Boolean repeat) {
    this.repeat = repeat;
  }

  // non property methods
  public void markAsRead()
  {
    this.read = true;
    this.getParentRequest().decrementUnreadMessageCounter();
    this.getParentRequest().getTransactionGroup().decrementUnreadMessageCounter();
  }
                                                                                                                                          
  /**
   * @hibernate.property column="NOTE" length="3000"
   */
  public String getNote() {
    return note;
  }
                                                                                                                                          
  public void setNote(String note) {
    this.note = DBStringHelper.set(note,3000);
  }

  /**
   * @hibernate.property column="MESSAGE_DATA" length="10000"
   */
  public java.io.Serializable getMessageDate() {
    return message_data;
  }
                                                                                                                                          
  public void setMessageDate(java.io.Serializable message_data) {
    this.message_data = message_data;
  }
}
