package com.k_int.openrequest.db;

import java.util.*;
import com.k_int.openrequest.util.DBStringHelper;


/**
 * Title:       ISO10161ForwardMessageDetails
 * @version:    $Id: ISO10161ForwardMessageDetails.java,v 1.2 2005/07/04 16:03:44 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 * @hibernate.subclass dynamic-update="true" dynamic-insert="true" discriminator-value="ISO2:FWD"
 */
public class ISO10161ForwardMessageDetails extends ISO10161v2Message {

  private String responder_service_identifier;
  private String responder_service_address;
  private long intermediary_symbol_type;
  private String intermediary_symbol;
  private long intermediary_name_type;
  private String intermediary_name;

  /**
   * @hibernate.property column="TELECOM_SERVICE_IDENTIFIER" length="20"
   */
  public String getTelecomServiceIdentifier() {
    return responder_service_identifier;
  }

  public void setTelecomServiceIdentifier(String responder_service_identifier) {
    this.responder_service_identifier = DBStringHelper.set(responder_service_identifier,20);
  }

  /**
   * @hibernate.property column="TELECOM_SERVICE_ADDRESS" length="100"
   */
  public String getTelecomServiceAddress() {
    return responder_service_address;
  }

  public void setTelecomServiceAddress(String responder_service_address) {
    this.responder_service_address = DBStringHelper.set(responder_service_address,100);
  }

  /**
   * @hibernate.property column="INTERMEDIARY_ID_SYMBOL_TYPE"
   */
  public long getIntermediaryIdSymbolType() {
    return intermediary_symbol_type;
  }

  public void setIntermediaryIdSymbolType(long intermediary_symbol_type) {
    this.intermediary_symbol_type = intermediary_symbol_type;
  }

  /**
   * @hibernate.property column="INTERMEDIARY_ID_SYMBOL" length="40"
   */
  public String getIntermediaryIdSymbol() {
    return intermediary_symbol;
  }

  public void setIntermediaryIdSymbol(String intermediary_symbol) {
    this.intermediary_symbol = DBStringHelper.set(intermediary_symbol,40);
  }

  /**
   * @hibernate.property column="INTERMEDIARY_ID_NAME_TYPE"
   */
  public long getIntermediaryIdNameType() {
    return intermediary_name_type;
  }

  public void setIntermediaryIdNameType(long intermediary_name_type) {
    this.intermediary_name_type = intermediary_name_type;
  }

  /**
   * @hibernate.property column="INTERMEDIARY_ID_NAME" length="100"
   */
  public String getIntermediaryIdName() {
    return intermediary_name;
  }

  public void setIntermediaryIdName(String intermediary_name) {
    this.intermediary_name = DBStringHelper.set(intermediary_name,100);
  }
}
