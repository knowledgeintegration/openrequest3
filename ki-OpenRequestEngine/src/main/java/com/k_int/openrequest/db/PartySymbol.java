package com.k_int.openrequest.db;

/**
 * Title:       PartySymbolDTO
 * @version:    $Id: PartySymbol.java,v 1.2 2005/06/17 18:52:15 ibbo Exp $
 * Copyright:   Copyright (C) 2003,2004 Knowledge Integration Ltd
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description: 
 * @hibernate.discriminator column="SYMBOL_TYPE" type="string"
 */
public abstract class PartySymbol implements java.io.Serializable {

  protected String symbol;

  /**
   * @hibernate.property
   * @hibernate.column name="PARTY_SYMBOL"
   */
  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }
}
