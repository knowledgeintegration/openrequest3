/**
 * Title:       
 * @version:    $Id: ORInstallServiceCmd.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.cmdline;

import org.hibernate.*;
import java.rmi.*;
import com.k_int.openrequest.db.*;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.*;

public class ORInstallServiceCmd
{
  public static void main (String args[])
  {
    if ( args.length != 6 )
    {
      System.out.println("Usgae: Install \"Desc\" \"ServiceIdentifier\" \"Class\" \"ServiceName\" Priority app_context_file.xml");
      System.exit(0);
    }

    try
    {
      String app_context_config_file = args[5];
      ApplicationContext app_context = new ClassPathXmlApplicationContext( new String[] { app_context_config_file } );

      SessionFactory factory = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      Session sess = factory.openSession();
     
      for ( int i=0; i<args.length;i++)
      {
        System.err.println("Arg: "+args[i]+"\n");
      }

      SystemAgentInfo new_service = new SystemAgentInfo(args[0], args[1], args[2], args[3], Integer.parseInt(args[4]));
      sess.save(new_service);
      sess.flush();
      sess.connection().commit();

      sess.close();
    }
    catch (Exception e)
    {
      System.err.println("Message Dispatch Service exception");
      e.printStackTrace();
    }
  }
}
