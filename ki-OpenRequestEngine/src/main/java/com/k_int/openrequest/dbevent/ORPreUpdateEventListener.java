/**
 * Title:       ORSaveOrUpdateEventListener
 * @version:    
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.dbevent;
import org.hibernate.event.*;
import org.hibernate.event.def.*;
import org.hibernate.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.*;

public class ORPreUpdateEventListener implements PreUpdateEventListener, ApplicationContextAware {

  public static Log log = LogFactory.getLog(ORPreUpdateEventListener.class);

  private ApplicationContext ctx = null;

  public void setApplicationContext(ApplicationContext ctx) {
    log.debug("setApplicationContext");
    this.ctx = ctx;
  }

  public ORPreUpdateEventListener() {
    super();
    log.debug("ORPreUpdateEventListener()");
  }

  public boolean onPreUpdate(PreUpdateEvent event) throws HibernateException {

    if ( event.getEntity() instanceof com.k_int.openrequest.db.ILLTransaction ) {
      log.debug("ILLTransaction onPreUpdate Event");
      com.k_int.openrequest.db.ILLTransaction trans = (com.k_int.openrequest.db.ILLTransaction) event.getEntity();
      trans.notifyPreUpdate(ctx);
    }
    else if ( event.getEntity() instanceof com.k_int.openrequest.db.ILLTransactionGroup ) {
      log.debug("ILLTransactionGroup onPreUpdate Event");
      com.k_int.openrequest.db.ILLTransactionGroup tg = (com.k_int.openrequest.db.ILLTransactionGroup) event.getEntity();
      tg.notifyPreUpdate(ctx);
    }

    return false;
  }

}
