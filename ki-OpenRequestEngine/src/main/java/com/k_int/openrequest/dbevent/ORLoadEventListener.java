/**
 * Title:       ORLoadEventListener
 * @version:    
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

package com.k_int.openrequest.dbevent;
import org.hibernate.event.*;
import org.hibernate.event.def.*;
import org.hibernate.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.*;

public class ORLoadEventListener extends DefaultLoadEventListener implements ApplicationContextAware {

  public static Log log = LogFactory.getLog(ORLoadEventListener.class);

  private ApplicationContext ctx = null;

  public void setApplicationContext(ApplicationContext ctx) {
    log.debug("setApplicationContext");
    this.ctx = ctx;
  }

  public ORLoadEventListener() {
    super();
    log.debug("ORLoadEventListener()");
  }

  public void onLoad(LoadEvent event, LoadEventListener.LoadType loadType) throws HibernateException {

    // log.debug("OR Load Event Listener");
    super.onLoad(event,loadType);

    if ( event.getResult() instanceof com.k_int.openrequest.db.ILLTransaction ) {
      log.debug("ILLTransaction onLoad Event");
      com.k_int.openrequest.db.ILLTransaction trans = (com.k_int.openrequest.db.ILLTransaction) event.getResult();
      trans.notifyLoad(ctx);
    }
    else if ( event.getResult() instanceof com.k_int.openrequest.db.ILLTransactionGroup ) {
      log.debug("ILLTransactionGroup onLoad Event");
      com.k_int.openrequest.db.ILLTransactionGroup tg = (com.k_int.openrequest.db.ILLTransactionGroup) event.getResult();
      tg.notifyLoad(ctx);
    }

    
  }
}
