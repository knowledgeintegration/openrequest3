/**
 * Title:       
 * @version:    $Id: ImportTestingLocationData.java,v 1.5 2005/12/01 18:37:42 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.setup;

import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.db.folders.*;
import java.util.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.springframework.context.*;
import org.hibernate.*;

public class ImportTestingLocationData extends com.k_int.openrequest.util.AbstractLocationImporter {

  public static void main(String[] args) {
    com.k_int.openrequest.util.AbstractLocationImporter importer = new ImportTestingLocationData();
    importer.runImp(args);
  }

  public void iterateLocations(ApplicationContext app_context, Session sess) {
    try {
      impRemoteTCPSymbol(sess, "KI", "loopback-1", "Loopback System 1 On Localhost", "localhost:1499", null);
      impRemoteTCPSymbol(sess, "KI", "loopback-2", "Loopback System 2 On Localhost", "localhost:1499", null);
      impRemoteTCPSymbol(sess, "KI", "loopback-3", "Loopback System 3 On Localhost", "localhost:1499", null);
      impRemoteTCPSymbol(sess, "KI", "ill.k-int.com-1", "K-Int test System", "ill.k-int.com:7090", null);
      // impRemoteTCPSymbol(sess, "OCLC", "ISOAA", "System", "wsdemo.oclc.org:1611", null, 1, "100294377", "isoaatest");
      // impRemoteTCPSymbol(sess, "OCLC", "ISOBB", "System", "wsdemo.oclc.org:1611", null, 1, "100294378", "isobbtest");
      // impRemoteTCPSymbol(sess, "OCLC", "ISOKK", "System", "wsdemo.oclc.org:1611", null, 1, "100294378", "isobbtest");
      // impRemoteTCPSymbol(sess, "OCLC", "ILL@OCLC", "System", "wsdemo.oclc.org:1611", null, 1, "100294378", "isobbtest");
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
  }
}
