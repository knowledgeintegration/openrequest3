/**
 * Title:       
 * @version:    $Id: AbstractSetup.java,v 1.5 2005/06/11 18:04:11 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd (See the COPYING file for details.)
 * @author:     Ian Ibbotson ( ian.ibbotson@k-int.com )
 * Company:     Knowledge Integration Ltd.
 * Description:
 */

//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the license, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite
// 330, Boston, MA  02111-1307, USA.
//

package com.k_int.openrequest.setup;

import com.k_int.openrequest.db.*;
import com.k_int.openrequest.db.Location.*;
import com.k_int.openrequest.db.folders.*;
import java.util.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import org.hibernate.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;
import org.apache.commons.logging.*;

/* TODO: Check that protocol adapters dont use the services list hanging off loc.. but instead uses service attached to symbol */

public abstract class AbstractSetup {

  private static Log log = LogFactory.getLog(AbstractSetup.class);

  public static String CLASS_KEY = "Class";

  public AbstractSetup() {
  }

  public void registerEventHandler(LocationType lt,
                                   String precondition,
                                   String message_type,
                                   String message_params,
                                   String handler_type,
                                   String handler_params,
                                   Session sess) throws java.sql.SQLException,
                                                        org.hibernate.HibernateException {
    Map m = lt.getLocationEventHandlers();
    m.put(precondition, new EventHandler(message_type,
                                         message_params,
                                         handler_type,
                                         handler_params));
  }

  public Object createLocationSymbol(String symbol,
                                     NamingAuthority authority,
                                     Location location,
                                     Service service_for_this_symbol,
                                     Session sess) throws java.sql.SQLException,
                                                          org.hibernate.HibernateException {
    LocationSymbol sym = new LocationSymbol(symbol, authority, location, service_for_this_symbol);
    return sess.save(sym);
  }

  public StateModel createStateModel(String description, 
                                            String code,
                                            Session sess) throws java.sql.SQLException,
                                                          org.hibernate.HibernateException {
    StateModel sm = new StateModel();
    sm.setName(description);
    sm.setCode(code);
    sess.save(sm);
    return sm;
  }

  public StatusCode createStatusCode(String description,
                                            String code,
                                            Session sess) throws java.sql.SQLException,
                                                          org.hibernate.HibernateException {
    StatusCode sc = new StatusCode();
    sc.setDescription(description);
    sc.setCode(code);
    sess.save(sc);
    return sc;
  }

  public StateTransition createST(StateModel model,
                                  String transition_code,
                                  StatusCode from,
                                  StatusCode to,
                                  String predicate,
                                  String description,
                                  Session sess,
                                  Boolean user_selectable) throws java.sql.SQLException, org.hibernate.HibernateException {
    StateTransition st = new StateTransition();
    st.setModel(model);
    st.setTransitionCode(transition_code);
    st.setDescription(description);
    st.setPredicate(predicate);
    st.setFrom_state(from);
    st.setTo_state(to);
    st.setUserSelectable(user_selectable);
    sess.save(st);
    return st;
  }


  public StateTransition createST(StateModel model,
                                  String transition_code,
                                  StatusCode from,
                                  StatusCode to,
                                  String predicate,
                                  String description,
                                  Session sess) throws java.sql.SQLException,
                                                       org.hibernate.HibernateException {
    StateTransition st = new StateTransition();
    st.setModel(model);
    st.setTransitionCode(transition_code);
    st.setDescription(description);
    st.setPredicate(predicate);
    st.setFrom_state(from);
    st.setTo_state(to);
    st.setUserSelectable(Boolean.FALSE);
    sess.save(st);
    return st;
  }


  public void setup(String[] args) {
    log.debug("Configure");
    ClassPathXmlApplicationContext app_context = new ClassPathXmlApplicationContext( args );
    setup(app_context);
  }

  public void setup(ApplicationContext app_context) {

    try {
      // LocalSessionFactoryBean lsfb = (LocalSessionFactoryBean) app_context.getBean("&OpenRequestSessionFactory");
      // lsfb.updateDatabaseSchema();

      SessionFactory factory = (SessionFactory) app_context.getBean("OpenRequestSessionFactory");
      Session sess = factory.openSession();

      BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

      log.debug("\n\nSetting up database\n\n");

      // Generate a globally unique system id for this OpenRequest installation
      String system_id = new java.rmi.dgc.VMID().toString();

      log.debug("Auto Generated globally unique system id is "+system_id);
      com.k_int.commons.util.PropSetHDO or_global_settings = com.k_int.commons.util.PropSetHDO.lookupOrCreate(sess,"com.k_int.openrequest.GlobalProps");
      or_global_settings.getProperties().put("SYSTEM_ID",system_id);
      sess.save(or_global_settings);

      // log.debug("\n\nSystem Administrator\n\n");
      // com.k_int.commons.auth.User sysadmin = new com.k_int.commons.auth.User("Administrator","password","Admin",null,null);
      // sess.save(sysadmin);

      // Grant all permissions on all locations to the admin user.
      // com.k_int.commons.auth.Permission perm = new com.k_int.commons.auth.Permission(
      //                                                        "ALL",          // perm id
      //                                                        system_id,      // auth service
      //                                                        "Administrator",// grantee
      //                                                        "com.k_int.openrequest.db.Location.Location",          // resource auth
      //                                                        "ALL");         // resource id
      // sess.save(perm);

      log.debug("Setting up state models");
      // Set up the ISO ILL State models:
      StateModel sm_req_proc = createStateModel("ISO 10161 v2 Requester processing phase", "REQ/PRO", sess);
      StateModel sm_res_proc = createStateModel("ISO 10161 v2 Responder processing phase", "RES/PRO",sess);
      StateModel sm_req_track = createStateModel("ISO 10161 v2 Requester tracking phase", "REQ/TRACK", sess);
      StateModel sm_res_track = createStateModel("ISO 10161 v2 Responder tracking phase", "RES/TRACK", sess);
      StateModel sm_int = createStateModel("ISO 10161 v2 State Model for Intermediary", "INT",sess);
      StateModel sm_req_tg = createStateModel("Transaction Group Requester State Model", "REQ/TG", sess);
      StateModel sm_res_tg = createStateModel("Transaction Group Responder State Model", "RES/TG", sess);
     
      StatusCode sc_idle = createStatusCode("Idle","IDLE",sess);
      StatusCode sc_pending = createStatusCode("Pending","PENDING",sess);
      StatusCode sc_not_supplied = createStatusCode("Not Supplied", "NOTSUPPLIED",sess);
      StatusCode sc_conditional = createStatusCode("Conditional","CONDITIONAL",sess);
      StatusCode sc_cancel_pending = createStatusCode("Cancel Pending","CANCELPENDING",sess);
      StatusCode sc_cancelled = createStatusCode("Cancelled","CANCELLED",sess);
      StatusCode sc_shipped = createStatusCode("Shipped","SHIPPED",sess);
      StatusCode sc_received = createStatusCode("Received","RECEIVED",sess);
      StatusCode sc_renew_pending = createStatusCode("Renew Pending","RENEWPENDING",sess);
      StatusCode sc_renew_overdue = createStatusCode("Renew Overdue","RENEWOVERDUE",sess);
      StatusCode sc_overdue = createStatusCode("Overdue","OVERDUE",sess);
      StatusCode sc_not_rcvd_overdue = createStatusCode("Not Received/Overdue","NOTREC/OVERDUE",sess);
      StatusCode sc_recall = createStatusCode("Recall","RECALL",sess);
      StatusCode sc_returned = createStatusCode("Returned","RETURNED",sess);
      StatusCode sc_lost = createStatusCode("Lost","LOST",sess);
      StatusCode sc_in_process = createStatusCode("In Process","IN-PROCESS",sess);
      StatusCode sc_forward = createStatusCode("Forward","FORWARD",sess);
      StatusCode sc_checked_in = createStatusCode("Checked In","CHECKEDIN",sess);

      StatusCode sc_end_of_rota = createStatusCode("End of rota","ENDROTA",sess);
      StatusCode sc_satisfied = createStatusCode("Satisfied","SATISFIED",sess);
      StatusCode sc_tracking = createStatusCode("Tracking","TRACKING",sess);
      StatusCode sc_closed = createStatusCode("Closed","CLOSED",sess);


      // State model for Requester processing phase

      createST(sm_req_proc, "ILLreq", sc_idle, sc_pending, "", "", sess, Boolean.TRUE); // Make it so users can select the ILLreq action
      createST(sm_req_proc, "ILLreq", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "C-REPreq+", sc_conditional, sc_pending, "", "", sess);
      createST(sm_req_proc, "C-REPreq+", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "C-REPreq-", sc_conditional, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "C-REPreq-", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "CANreq", sc_pending, sc_cancel_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "CANreq", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "RCVreq", sc_pending, sc_received, "", "set RETURN var", sess, Boolean.TRUE);
      createST(sm_req_proc, "RCVreq", sc_cancel_pending, sc_received, "", "set RETURN var", sess, Boolean.TRUE);
      createST(sm_req_proc, "RCVreq", sc_shipped, sc_received, "", "set RETURN var", sess, Boolean.TRUE);
      createST(sm_req_proc, "LSTreq", sc_pending, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "LSTreq", sc_cancel_pending, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "LSTreq", sc_shipped, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "MSGreq", sc_pending, sc_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "MSGreq", sc_not_supplied, sc_not_supplied, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "MSGreq", sc_conditional, sc_conditional, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "MSGreq", sc_cancel_pending, sc_cancel_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "MSGreq", sc_cancelled, sc_cancelled, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "MSGreq", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STQreq", sc_pending, sc_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STQreq", sc_not_supplied, sc_not_supplied, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STQreq", sc_conditional, sc_conditional, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STQreq", sc_cancel_pending, sc_cancel_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STQreq", sc_cancelled, sc_cancelled, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STQreq", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STRreq", sc_pending, sc_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STRreq", sc_not_supplied, sc_not_supplied, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STRreq", sc_conditional, sc_conditional, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STRreq", sc_cancel_pending, sc_cancel_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STRreq", sc_cancelled, sc_cancelled, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "STRreq", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_req_proc, "FWD", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "FWD", sc_cancel_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "ANS-CO", sc_pending, sc_conditional, "", "", sess); // p7, ANSind-CO
      createST(sm_req_proc, "ANS-CO", sc_not_supplied, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-CO", sc_conditional, sc_conditional, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-CO", sc_cancel_pending, sc_cancel_pending, "", "", sess); // p7, ANSind-CO
      createST(sm_req_proc, "ANS-CO", sc_cancelled, sc_cancelled, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-RY", sc_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-RY", sc_not_supplied, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-RY", sc_cancel_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-UN", sc_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-UN", sc_not_supplied, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-UN", sc_cancel_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-LP", sc_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-LP", sc_not_supplied, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-LP", sc_cancel_pending, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-WS", sc_pending, sc_pending, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-WS", sc_not_supplied, sc_not_supplied, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-WS", sc_conditional, sc_conditional, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-WS", sc_cancel_pending, sc_cancel_pending, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-WS", sc_shipped, sc_shipped, "", "", sess); // ANSind-CO
      createST(sm_req_proc, "ANS-HP", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "ANS-HP", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "ANS-HP", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_req_proc, "ANS-HP", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "ANS-HP", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "ANS-ES", sc_pending, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "ANS-ES", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "ANS-ES", sc_cancel_pending, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "CAR+", sc_cancel_pending, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "CAR+", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "CAR-", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "CAR-", sc_cancel_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "CAR-", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "SHI", sc_pending, sc_shipped, "", "", sess);
      createST(sm_req_proc, "SHI", sc_cancel_pending, sc_shipped, "", "", sess);
      createST(sm_req_proc, "SHI", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "MSG", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "MSG", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "MSG", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_req_proc, "MSG", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "MSG", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "MSG", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "STQ", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "STQ", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "STQ", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_req_proc, "STQ", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "STQ", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "STQ", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "STR", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_proc, "STR", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "STR", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_req_proc, "STR", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_proc, "STR", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_req_proc, "STR", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_proc, "EXP", sc_pending, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "EXP", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "EXP", sc_conditional, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "EXP", sc_cancel_pending, sc_not_supplied, "", "", sess);
      createST(sm_req_proc, "LST", sc_pending, sc_lost, "", "", sess);
      createST(sm_req_proc, "LST", sc_cancel_pending, sc_lost, "", "", sess);
      createST(sm_req_proc, "LST", sc_shipped, sc_lost, "", "", sess);

      // State model for Requester tracking phase
      createST(sm_req_track, "RCVreq", sc_pending, sc_received, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "RCVreq", sc_cancel_pending, sc_received, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "RCVreq", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "RCVreq", sc_shipped, sc_received, "", "", sess, Boolean.TRUE);

      createST(sm_req_track, "RET", sc_received, sc_returned, "p5", "", sess);
      createST(sm_req_track, "RET", sc_renew_pending, sc_returned, "", "", sess);
      createST(sm_req_track, "RET", sc_renew_overdue, sc_returned, "", "", sess);

      createST(sm_req_track, "RENreq", sc_received, sc_renew_pending, "p5", "", sess, Boolean.TRUE);
      createST(sm_req_track, "RENreq", sc_renew_overdue, sc_renew_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "RENreq", sc_renew_pending, sc_renew_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "RENreq", sc_overdue, sc_renew_overdue, "", "", sess, Boolean.TRUE);

      createST(sm_req_track, "LSTreq", sc_pending, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "LSTreq", sc_cancel_pending, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "LSTreq", sc_received, sc_lost, "p5", "", sess, Boolean.TRUE);
      createST(sm_req_track, "LSTreq", sc_renew_pending, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "LSTreq", sc_renew_overdue, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "LSTreq", sc_shipped, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "LSTreq", sc_not_rcvd_overdue, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "LSTreq", sc_overdue, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "LSTreq", sc_returned, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "LSTreq", sc_lost, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "LSTreq", sc_recall, sc_lost, "", "", sess, Boolean.TRUE);

      createST(sm_req_track, "DAMreq", sc_received, sc_received, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "DAMreq", sc_renew_pending, sc_renew_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "DAMreq", sc_renew_overdue, sc_renew_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "DAMreq", sc_overdue, sc_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "DAMreq", sc_returned, sc_returned, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "DAMreq", sc_recall, sc_recall, "", "", sess, Boolean.TRUE);

      createST(sm_req_track, "MSGreq", sc_pending, sc_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "MSGreq", sc_cancel_pending, sc_cancel_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "MSGreq", sc_received, sc_received, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "MSGreq", sc_renew_pending, sc_renew_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "MSGreq", sc_renew_overdue, sc_renew_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "MSGreq", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "MSGreq", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "MSGreq", sc_overdue, sc_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "MSGreq", sc_returned, sc_returned, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "MSGreq", sc_lost, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "MSGreq", sc_recall, sc_recall, "", "", sess, Boolean.TRUE);

      createST(sm_req_track, "STQreq", sc_pending, sc_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STQreq", sc_cancel_pending, sc_cancel_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STQreq", sc_received, sc_received, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STQreq", sc_renew_pending, sc_renew_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STQreq", sc_renew_overdue, sc_renew_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STQreq", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STQreq", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STQreq", sc_overdue, sc_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STQreq", sc_returned, sc_returned, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STQreq", sc_lost, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STQreq", sc_recall, sc_recall, "", "", sess, Boolean.TRUE);

      createST(sm_req_track, "STRreq", sc_pending, sc_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STRreq", sc_cancel_pending, sc_cancel_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STRreq", sc_received, sc_received, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STRreq", sc_renew_pending, sc_renew_pending, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STRreq", sc_renew_overdue, sc_renew_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STRreq", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STRreq", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STRreq", sc_overdue, sc_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STRreq", sc_returned, sc_returned, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STRreq", sc_lost, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "STRreq", sc_recall, sc_recall, "", "", sess, Boolean.TRUE);

      createST(sm_req_track, "ANS-WS", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "ANS-WS", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "ANS-HP", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "ANS-HP", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "CAR-", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "CAR-", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "CAR-", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "CAR-", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "CAR-", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "CAR-", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "CAR-", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "CAR-", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "CAR-", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "CAR-", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "CAR-", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "SHI", sc_pending, sc_shipped, "", "", sess);
      createST(sm_req_track, "SHI", sc_cancel_pending, sc_shipped, "", "", sess);
      createST(sm_req_track, "SHI", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "SHI", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "SHI", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "SHI", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "SHI", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "SHI", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "SHI", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "SHI", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "SHI", sc_recall, sc_recall, "", "", sess);
    
      createST(sm_req_track, "RCL", sc_pending, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_cancel_pending, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_received, sc_recall, "p5", "", sess);
      createST(sm_req_track, "RCL", sc_renew_pending, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_renew_overdue, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_shipped, sc_recall, "p5", "", sess);
      createST(sm_req_track, "RCL", sc_not_rcvd_overdue, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_overdue, sc_recall, "", "", sess);
      createST(sm_req_track, "RCL", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "RCL", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "RCL", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "DUE", sc_pending, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "DUE", sc_cancel_pending, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "DUE", sc_received, sc_overdue, "p5", "", sess);
      createST(sm_req_track, "DUE", sc_renew_pending, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "DUE", sc_renew_overdue, sc_overdue, "p7 and p8", "", sess);
      createST(sm_req_track, "DUE", sc_shipped, sc_not_rcvd_overdue, "p5", "", sess);
      createST(sm_req_track, "DUE", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "DUE", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "DUE", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "DUE", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "LST", sc_pending, sc_lost, "", "", sess);
      createST(sm_req_track, "LST", sc_cancel_pending, sc_lost, "", "", sess);

      createST(sm_req_track, "MSG", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "MSG", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "MSG", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "MSG", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "MSG", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "MSG", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "MSG", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "MSG", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "MSG", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "MSG", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "MSG", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "STQ", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "STQ", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "STQ", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "STQ", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "STQ", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "STQ", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "STQ", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "STQ", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "STQ", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "STQ", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "STQ", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "STR", sc_pending, sc_pending, "", "", sess);
      createST(sm_req_track, "STR", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_req_track, "STR", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "STR", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_req_track, "STR", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_req_track, "STR", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_req_track, "STR", sc_not_rcvd_overdue, sc_not_rcvd_overdue, "", "", sess);
      createST(sm_req_track, "STR", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "STR", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "STR", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "STR", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "REA+", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "REA+", sc_renew_pending, sc_received, "", "", sess);
      createST(sm_req_track, "REA+", sc_renew_overdue, sc_received, "", "", sess);
      createST(sm_req_track, "REA+", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "REA+", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "REA+", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "REA-", sc_received, sc_received, "", "", sess);
      createST(sm_req_track, "REA-", sc_renew_pending, sc_received, "", "", sess);
      createST(sm_req_track, "REA-", sc_renew_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "REA-", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_req_track, "REA-", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "REA-", sc_lost, sc_lost, "", "", sess);
      createST(sm_req_track, "REA-", sc_recall, sc_recall, "", "", sess);

      createST(sm_req_track, "CHK", sc_pending, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_cancel_pending, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_received, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_renew_pending, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_renew_overdue, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_shipped, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_not_rcvd_overdue, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_overdue, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_returned, sc_returned, "", "", sess);
      createST(sm_req_track, "CHK", sc_recall, sc_returned, "", "", sess);

      createST(sm_req_track, "RCVreq", sc_not_rcvd_overdue, sc_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "RCVreq", sc_recall, sc_recall, "", "", sess, Boolean.TRUE);

      createST(sm_req_track, "RETreq", sc_received, sc_returned, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "RETreq", sc_overdue, sc_returned, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "RETreq", sc_returned, sc_returned, "", "", sess, Boolean.TRUE);
      createST(sm_req_track, "RETreq", sc_recall, sc_returned, "", "", sess, Boolean.TRUE);

      createST(sm_req_track, "DAM", sc_returned, sc_returned, "", "", sess);

      // Responder processing phase
      createST(sm_res_proc, "ILL", sc_idle, sc_in_process, "", "", sess);
      createST(sm_res_proc, "ILL", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "ILL", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "ILL", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_res_proc, "ILL", sc_forward, sc_forward, "", "", sess);

      createST(sm_res_proc, "FWDreq", sc_in_process, sc_forward, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "FWDreq", sc_forward, sc_forward, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "ANSreq-CO", sc_in_process, sc_conditional, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "ANSreq-CO", sc_conditional, sc_conditional, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "ANSreq-RY", sc_in_process, sc_not_supplied, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "ANSreq-RY", sc_not_supplied, sc_not_supplied, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "ANSreq-UN", sc_in_process, sc_not_supplied, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "ANSreq-UN", sc_not_supplied, sc_not_supplied, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "ANSreq-PL", sc_in_process, sc_not_supplied, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "ANSreq-PL", sc_not_supplied, sc_not_supplied, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "ANSreq-WS", sc_in_process, sc_in_process, "", "", sess, Boolean.TRUE);
      // createST(sm_res_proc, "ANSreq-WS", sc_not_supplied, sc_not_supplied, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "ANSreq-HP", sc_in_process, sc_in_process, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "ANSreq-HP", sc_not_supplied, sc_in_process, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "ANSreq-ES", sc_in_process, sc_not_supplied, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "ANSreq-ES", sc_not_supplied, sc_not_supplied, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "CARreq+", sc_cancel_pending, sc_cancelled, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "CARreq+", sc_cancelled, sc_cancelled, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "CARreq-", sc_cancel_pending, sc_in_process, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "CARreq-", sc_in_process, sc_in_process, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "SHIreq", sc_in_process, sc_shipped, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "MSGreq", sc_in_process, sc_in_process, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "MSGreq", sc_not_supplied, sc_not_supplied, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "MSGreq", sc_conditional, sc_conditional, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "MSGreq", sc_cancel_pending, sc_cancel_pending, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "MSGreq", sc_cancelled, sc_cancelled, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "MSGreq", sc_forward, sc_forward, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "STQreq", sc_in_process, sc_in_process, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "STQreq", sc_not_supplied, sc_not_supplied, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "STQreq", sc_conditional, sc_conditional, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "STQreq", sc_cancel_pending, sc_cancel_pending, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "STQreq", sc_cancelled, sc_cancelled, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "STQreq", sc_forward, sc_forward, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "STRreq", sc_in_process, sc_in_process, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "STRreq", sc_not_supplied, sc_not_supplied, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "STRreq", sc_conditional, sc_conditional, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "STRreq", sc_cancel_pending, sc_cancel_pending, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "STRreq", sc_cancelled, sc_cancelled, "", "", sess, Boolean.TRUE);
      createST(sm_res_proc, "STRreq", sc_forward, sc_forward, "", "", sess, Boolean.TRUE);

      createST(sm_res_proc, "C-REP+", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "C-REP+", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "C-REP+", sc_conditional, sc_in_process, "", "", sess);

      createST(sm_res_proc, "C-REP-", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "C-REP-", sc_conditional, sc_not_supplied, "", "", sess);

      createST(sm_res_proc, "CAN", sc_in_process, sc_cancel_pending, "p7", "", sess);
      createST(sm_res_proc, "CAN", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "CAN", sc_conditional, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "CAN", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "CAN", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "CAN", sc_forward, sc_cancelled, "", "", sess);

      createST(sm_res_proc, "MSG", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "MSG", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "MSG", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_res_proc, "MSG", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "MSG", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "MSG", sc_forward, sc_forward, "", "", sess);

      createST(sm_res_proc, "STQ", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "STQ", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "STQ", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_res_proc, "STQ", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "STQ", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "STQ", sc_forward, sc_forward, "", "", sess);

      createST(sm_res_proc, "STR", sc_in_process, sc_in_process, "", "", sess);
      createST(sm_res_proc, "STR", sc_not_supplied, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "STR", sc_conditional, sc_conditional, "", "", sess);
      createST(sm_res_proc, "STR", sc_cancel_pending, sc_cancel_pending, "", "", sess);
      createST(sm_res_proc, "STR", sc_cancelled, sc_cancelled, "", "", sess);
      createST(sm_res_proc, "STR", sc_forward, sc_forward, "", "", sess);

      createST(sm_res_proc, "EXPIRYtimeout", sc_in_process, sc_not_supplied, "", "", sess);
      createST(sm_res_proc, "EXPIRYtimeout", sc_conditional, sc_not_supplied, "", "", sess);

      // Responder tracking phase
      createST(sm_res_track, "SHIreq", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);

      createST(sm_res_track, "CHKreq", sc_shipped, sc_checked_in, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "CHKreq", sc_renew_pending, sc_checked_in, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "CHKreq", sc_renew_overdue, sc_checked_in, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "CHKreq", sc_overdue, sc_checked_in, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "CHKreq", sc_recall, sc_checked_in, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "CHKreq", sc_checked_in, sc_checked_in, "", "", sess, Boolean.TRUE);

      createST(sm_res_track, "RCLreq", sc_shipped, sc_recall, "p5", "", sess, Boolean.TRUE);
      createST(sm_res_track, "RCLreq", sc_renew_pending, sc_recall, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "RCLreq", sc_renew_overdue, sc_recall, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "RCLreq", sc_overdue, sc_recall, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "RCLreq", sc_recall, sc_recall, "", "", sess, Boolean.TRUE);

      createST(sm_res_track, "DUEreq", sc_shipped, sc_overdue, "p5", "", sess, Boolean.TRUE);
      createST(sm_res_track, "DUEreq", sc_renew_pending, sc_renew_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "DUEreq", sc_overdue, sc_overdue, "p8", "", sess, Boolean.TRUE);
      createST(sm_res_track, "DUEreq", sc_renew_overdue, sc_renew_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "DUEreq", sc_overdue, sc_overdue, "", "", sess, Boolean.TRUE);

      createST(sm_res_track, "LSTreq", sc_shipped, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "LSTreq", sc_renew_pending, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "LSTreq", sc_renew_overdue, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "LSTreq", sc_overdue, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "LSTreq", sc_recall, sc_lost, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "LSTreq", sc_lost, sc_lost, "", "", sess, Boolean.TRUE);

      createST(sm_res_track, "DAMreq", sc_checked_in, sc_checked_in, "", "", sess, Boolean.TRUE);

      createST(sm_res_track, "MSGreq", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "MSGreq", sc_renew_pending, sc_renew_pending, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "MSGreq", sc_renew_overdue, sc_renew_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "MSGreq", sc_overdue, sc_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "MSGreq", sc_recall, sc_recall, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "MSGreq", sc_checked_in, sc_checked_in, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "MSGreq", sc_lost, sc_lost, "", "", sess, Boolean.TRUE);

      createST(sm_res_track, "STQreq", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STQreq", sc_renew_pending, sc_renew_pending, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STQreq", sc_renew_overdue, sc_renew_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STQreq", sc_overdue, sc_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STQreq", sc_recall, sc_recall, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STQreq", sc_checked_in, sc_checked_in, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STQreq", sc_lost, sc_lost, "", "", sess, Boolean.TRUE);

      createST(sm_res_track, "STRreq", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STRreq", sc_renew_pending, sc_renew_pending, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STRreq", sc_renew_overdue, sc_renew_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STRreq", sc_overdue, sc_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STRreq", sc_recall, sc_recall, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STRreq", sc_checked_in, sc_checked_in, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "STRreq", sc_lost, sc_lost, "", "", sess, Boolean.TRUE);

      createST(sm_res_track, "REAreq+", sc_renew_pending, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "REAreq+", sc_renew_overdue, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "REAreq+", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);

      createST(sm_res_track, "REAreq-", sc_renew_pending, sc_shipped, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "REAreq-", sc_renew_overdue, sc_overdue, "", "", sess, Boolean.TRUE);
      createST(sm_res_track, "REAreq-", sc_shipped, sc_shipped, "", "", sess, Boolean.TRUE);

      createST(sm_res_track, "ILL", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "ILL", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "ILL", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "ILL", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "ILL", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "CAN", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "CAN", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "CAN", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "CAN", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "CAN", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "CAN", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "CAN", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "RCV", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "RCV", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "RCV", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "RCV", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "RCV", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "RCV", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "RCV", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "RET", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "RET", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "RET", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "RET", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "RET", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "RET", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "RET", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "REN", sc_shipped, sc_shipped, "p7", "", sess);
      createST(sm_res_track, "REN", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "REN", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "REN", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "REN", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "REN", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "REN", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "LST", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "LST", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "LST", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "LST", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "LST", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "LST", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "LST", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "DAM", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "DAM", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "DAM", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "DAM", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "DAM", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "DAM", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "DAM", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "MSG", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "MSG", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "MSG", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "MSG", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "MSG", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "MSG", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "MSG", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "STQ", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "STQ", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "STQ", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "STQ", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "STQ", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "STQ", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "STQ", sc_lost, sc_lost, "", "", sess);

      createST(sm_res_track, "STR", sc_shipped, sc_shipped, "", "", sess);
      createST(sm_res_track, "STR", sc_renew_pending, sc_renew_pending, "", "", sess);
      createST(sm_res_track, "STR", sc_renew_overdue, sc_renew_overdue, "", "", sess);
      createST(sm_res_track, "STR", sc_overdue, sc_overdue, "", "", sess);
      createST(sm_res_track, "STR", sc_recall, sc_recall, "", "", sess);
      createST(sm_res_track, "STR", sc_checked_in, sc_checked_in, "", "", sess);
      createST(sm_res_track, "STR", sc_lost, sc_lost, "", "", sess);


      //
      // Transaction group - Requester - state model
      //
      createST(sm_req_tg, "TG_START",    sc_idle,             sc_in_process, "", "", sess, Boolean.TRUE);

      createST(sm_req_tg, "TG_NEXT",     sc_in_process,       sc_in_process, "", "", sess);
      createST(sm_req_tg, "TG_EOR",      sc_in_process,       sc_end_of_rota, "", "", sess);
      createST(sm_req_tg, "TG_RECEIVED", sc_in_process,       sc_satisfied, "", "", sess); // Shipped non-ret
      createST(sm_req_tg, "TG_SHIP_NR",  sc_in_process,       sc_satisfied, "", "", sess); // Shipped non-ret
      createST(sm_req_tg, "TG_SHIP_RET", sc_in_process,       sc_tracking, "", "", sess); // Shipped returnable
      createST(sm_req_tg, "TG_LOST",     sc_in_process,       sc_tracking, "", "", sess); //
      createST(sm_req_tg, "TG_CAN_REQ",  sc_in_process,       sc_cancel_pending, "", "", sess);

      createST(sm_req_tg, "TG_RESTART",  sc_end_of_rota,      sc_in_process, "", "", sess);
      createST(sm_req_tg, "TG_CLOSE",    sc_end_of_rota,      sc_closed, "", "", sess);

      createST(sm_req_tg, "TG_SHIP_NR",  sc_cancel_pending,   sc_tracking, "", "", sess);
      createST(sm_req_tg, "TG_SHIP_RET", sc_cancel_pending,   sc_satisfied, "", "", sess);
      createST(sm_req_tg, "TG_CAN_OK",   sc_cancel_pending,   sc_cancelled, "", "", sess);

      createST(sm_req_tg, "TG_CLOSE",    sc_satisfied,        sc_closed, "", "", sess);
      createST(sm_req_tg, "TG_CLOSE",    sc_tracking,         sc_closed, "", "", sess);
      createST(sm_req_tg, "TG_CLOSE",    sc_cancelled,        sc_closed, "", "", sess);

      //
      // Transaction group - Responder - state model
      //
      createST(sm_res_tg, "TG_START",    sc_idle,             sc_in_process, "", "", sess);

      createST(sm_res_tg, "TG_NEXT",     sc_in_process,       sc_in_process, "", "", sess);
      createST(sm_res_tg, "TG_EOR",      sc_in_process,       sc_end_of_rota, "", "", sess);
      createST(sm_res_tg, "TG_SHIP_NR",  sc_in_process,       sc_satisfied, "", "", sess); // Shipped non-ret
      createST(sm_res_tg, "TG_SHIP_RET", sc_in_process,       sc_tracking, "", "", sess); // Shipped returnable
      createST(sm_res_tg, "TG_CAN_REQ",  sc_in_process,       sc_cancel_pending, "", "", sess);

      createST(sm_res_tg, "TG_RESTART",  sc_end_of_rota,      sc_in_process, "", "", sess);
      createST(sm_res_tg, "TG_CLOSE",    sc_end_of_rota,      sc_closed, "", "", sess);

      createST(sm_res_tg, "TG_SHIP_NR",  sc_cancel_pending,   sc_tracking, "", "", sess);
      createST(sm_res_tg, "TG_SHIP_RET", sc_cancel_pending,   sc_satisfied, "", "", sess);
      createST(sm_res_tg, "TG_CAN_OK",   sc_cancel_pending,   sc_cancelled, "", "", sess);

      createST(sm_res_tg, "TG_CLOSE",    sc_satisfied,        sc_closed, "", "", sess);
      createST(sm_res_tg, "TG_CLOSE",    sc_tracking,         sc_closed, "", "", sess);
      createST(sm_res_tg, "TG_CLOSE",    sc_cancelled,        sc_closed, "", "", sess);

      // ILL FWD ANS-CO ANS-RY ANS-UN ANS-LP ANS-WS ANS-HP ANS-ES C-REP+ C-REP- CAN CAR+
      // CAR-  SHI RCV RCL DUE RET REN REA+ REA- CHK LST DAM MSG STQ STR EXP

      log.debug("Setting up Location Types");
      LocationType system_location_type = new LocationType("SYSTEM", "System Location");  // The System
      LocationType normal_location_type = new LocationType("NORM", "Standard ILL Site");    // Standard location
      LocationType intermediary_location_type = new LocationType("INT", "Intermediary");     // Intermediary

      // Register some handlers
      log.debug("Setting up Event Handlers");

      // Request that an ILL be sent
      registerEventHandler(system_location_type,
                           "ILLreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ILLreq",
                           sess);

      registerEventHandler(system_location_type,
                           "FWDreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_FWDreq",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-CO",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSreqCO",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-RY",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSreqRY",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-UN",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSreqUN",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-LP",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSreqLP",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-WS",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSreqWS",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-HP",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSreqHP",
                           sess);

      registerEventHandler(system_location_type,
                           "ANSreq-ES",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSreqES",
                           sess);

      registerEventHandler(system_location_type,
                           "C-REPreq+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CREPreqYES",
                           sess);

      registerEventHandler(system_location_type,
                           "C-REPreq-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CREPreqNO",
                           sess);

      registerEventHandler(system_location_type,
                           "CANreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CANreq",
                           sess);

      registerEventHandler(system_location_type,
                           "CARreq+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CARreqYES",
                           sess);

      registerEventHandler(system_location_type,
                           "CARreq-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CARreqNO",
                           sess);

      registerEventHandler(system_location_type,
                           "SHIreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_SHIreq",
                           sess);

      registerEventHandler(system_location_type,
                           "RCVreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_RCVreq",
                           sess);

      registerEventHandler(system_location_type,
                           "RCLreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_RCLreq",
                           sess);

      registerEventHandler(system_location_type,
                           "DUEreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_DUEreq",
                           sess);

      registerEventHandler(system_location_type,
                           "RETreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_RETreq",
                           sess);

      registerEventHandler(system_location_type,
                           "RENreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_RENreq",
                           sess);

      registerEventHandler(system_location_type,
                           "REAreq+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_REAreqYES",
                           sess);

      registerEventHandler(system_location_type,
                           "REAreq-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_REAreqNO",
                           sess);

      registerEventHandler(system_location_type,
                           "CHKreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CHKreq",
                           sess);

      registerEventHandler(system_location_type,
                           "LSTreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_LSTreq",
                           sess);

      registerEventHandler(system_location_type,
                           "DAMreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_DAMreq",
                           sess);

      registerEventHandler(system_location_type,
                           "MSGreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_MSGreq",
                           sess);

      registerEventHandler(system_location_type,
                           "STQreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_STQreq",
                           sess);

      registerEventHandler(system_location_type,
                           "STRreq",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_STRreq",
                           sess);

      // Handle an incoming ILL message
      registerEventHandler(system_location_type,
                           "ILL",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ILL",
                           sess);

      registerEventHandler(system_location_type,
                           "FWD",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_FWD",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-CO",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSCO",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-RY",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSRY",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-UN",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSUN",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-LP",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSLP",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-WS",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSWS",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-HP",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSHP",
                           sess);

      registerEventHandler(system_location_type,
                           "ANS-ES",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_ANSES",
                           sess);

      registerEventHandler(system_location_type,
                           "C-REP+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CREPYES",
                           sess);

      registerEventHandler(system_location_type,
                           "C-REP-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CREPNO",
                           sess);

      registerEventHandler(system_location_type,
                           "CAN",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CAN",
                           sess);

      registerEventHandler(system_location_type,
                           "CAR+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CARYES",
                           sess);

      registerEventHandler(system_location_type,
                           "CAR-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CARNO",
                           sess);

      registerEventHandler(system_location_type,
                           "SHI",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_SHI",
                           sess);

      registerEventHandler(system_location_type,
                           "RCV",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_RCV",
                           sess);

      registerEventHandler(system_location_type,
                           "RCL",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_RCL",
                           sess);

      registerEventHandler(system_location_type,
                           "DUE",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_DUE",
                           sess);

      registerEventHandler(system_location_type,
                           "RET",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_RET",
                           sess);

      registerEventHandler(system_location_type,
                           "REN",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_REN",
                           sess);

      registerEventHandler(system_location_type,
                           "REA+",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_REAYES",
                           sess);

      registerEventHandler(system_location_type,
                           "REA-",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_REANO",
                           sess);

      registerEventHandler(system_location_type,
                           "CHK",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_CHK",
                           sess);

      registerEventHandler(system_location_type,
                           "LST",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_LST",
                           sess);

      registerEventHandler(system_location_type,
                           "DAM",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_DAM",
                           sess);

      registerEventHandler(system_location_type,
                           "MSG",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_MSG",
                           sess);

      registerEventHandler(system_location_type,
                           "STQ",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_STQ",
                           sess);

      registerEventHandler(system_location_type,
                           "STR",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_STR",
                           sess);

      registerEventHandler(system_location_type,
                           "EXP",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_EXP",
                           sess);

      registerEventHandler(system_location_type,
                           "EXPIRYtimeout",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleILL_EXPIRYtimeout",
                           sess);

      // Got this far re-working state tables.

      registerEventHandler(system_location_type,
                           "TG_NEXT",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.workflow.HandleNextRotaResponderRequest",
                           sess);

      registerEventHandler(system_location_type,
                           "TG_SHIP_RET",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.workflow.HandleShippedReturnable",
                           sess);

      registerEventHandler(system_location_type,
                           "TG_SHIP_NR",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.workflow.HandleShippedNonReturnable",
                           sess);

      registerEventHandler(system_location_type,
                           "TG_LOST",
                           CLASS_KEY, null,
                           CLASS_KEY, "com.k_int.openrequest.actions.workflow.HandleLost",
                           sess);

      registerEventHandler(system_location_type,
                           "internal:IncomingPDU",
                           CLASS_KEY, "com.k_int.openrequest.isoill.ILLMessageEnvelope",
                           CLASS_KEY, "com.k_int.openrequest.actions.iso10161.HandleIncomingProtocolMessage",
                           sess);


      /*
       * Call the correct handler for a specific event
       */
      registerEventHandler(system_location_type,
                           "internal:IncomingValidPDU",
                           CLASS_KEY,
                           "com.k_int.openrequest.helpers.InternalIso10161Message",
                           CLASS_KEY,
                           "com.k_int.openrequest.actions.iso10161.IncomingPDUDispatcher",
                           sess);


      log.debug("Saving / Flushing");
      sess.save(system_location_type);
      sess.save(normal_location_type);
      sess.save(intermediary_location_type);

      log.debug("Setting up naming authorities");
      // Create some location information
      // from maintenance agency
      NamingAuthority a_g_authority = new NamingAuthority("a_g", "Auto-Graphics, Inc");
      sess.save(a_g_authority);

      NamingAuthority ait_authority = new NamingAuthority("ait", "Aurora Information Technology");
      sess.save(ait_authority);

      NamingAuthority als_authority = new NamingAuthority("als", "Ameritech Library Services");
      sess.save(als_authority);

      NamingAuthority bl_authority = new NamingAuthority("bl", "British Library");
      sess.save(bl_authority);

      NamingAuthority btp_authority = new NamingAuthority("btp", "Big 12 Plus Libraries Consortium");
      sess.save(btp_authority);

      NamingAuthority calis_authority = new NamingAuthority("calis", "China Academic Library and Information");
      sess.save(calis_authority);

      NamingAuthority carl_authority = new NamingAuthority("carl", "CARL Corporation");
      sess.save(carl_authority);

      NamingAuthority cisti_authority = new NamingAuthority("cisti", 
                         "Canada Institute for Scientific and Technical Information (CISTI)");
      sess.save(cisti_authority);

      NamingAuthority dra_authority = new NamingAuthority("dra", "Data Research Associates, Inc");
      sess.save(dra_authority);

      NamingAuthority elias_authority = new NamingAuthority("elias", "ELiAS nv");
      sess.save(elias_authority);

      NamingAuthority exl_authority = new NamingAuthority("exl", "Ex Libris (USA)");
      sess.save(exl_authority);

      NamingAuthority ilib_authority = new NamingAuthority("ilib", "CMC Co. Ltd");
      sess.save(ilib_authority);

      NamingAuthority irc_authority = new NamingAuthority("irc", 
                                                          "Information Resource Center of Da Nang University (Viet Nam)");
      sess.save(irc_authority);

      NamingAuthority lvep_authority = new NamingAuthority("lveb", "Lac Viet Corp.");
      sess.save(lvep_authority);

      NamingAuthority mnlink_authority = new NamingAuthority("mnlink", "Minnesota Library Information Network (MnLINK)");
      sess.save(mnlink_authority);

      NamingAuthority mnscu_authority = new NamingAuthority("MnSCU/PALS", 
                   "Minnesota State Colleges and Universities Project for Automated Library Systems Authority:");
      sess.save(mnscu_authority);

      NamingAuthority nacsis_authority = new NamingAuthority("nacsis", 
                                                             "National Center for Science Information Systems (Japan");
      sess.save(nacsis_authority);

      NamingAuthority nla_authority = new NamingAuthority("nla", "National Library of Australia");
      sess.save(nla_authority);

      NamingAuthority nlc_authority = new NamingAuthority("nlc-bnc", "National Library of Canada");
      sess.save(nlc_authority);

      NamingAuthority nlm_authority = new NamingAuthority("nlm", "National Library of Medicine");
      sess.save(nlm_authority);

      NamingAuthority nlnz_authority = new NamingAuthority("nlnz", "National Library of New Zealand");
      sess.save(nlnz_authority);

      NamingAuthority oclc_authority = new NamingAuthority("oclc", "OCLC");
      sess.save(oclc_authority);

      NamingAuthority psi_authority = new NamingAuthority("psi", "Pigasus Software, Inc.");
      sess.save(psi_authority);

      NamingAuthority rct_authority = new NamingAuthority("rct", "The RCT Digital Library");
      sess.save(rct_authority);

      NamingAuthority relais_authority = new NamingAuthority("relais", "Relais International Inc.");
      sess.save(relais_authority);

      NamingAuthority rlg_authority = new NamingAuthority("rlg", "Research Libraries Group");
      sess.save(rlg_authority);

      NamingAuthority sirsi_authority = new NamingAuthority("sirsi", "SIRSI Corporation");
      sess.save(sirsi_authority);

      NamingAuthority tlc_authority = new NamingAuthority("tlc", "The Library Corporation");
      sess.save(tlc_authority);

      NamingAuthority usnuc_authority = new NamingAuthority("usnuc", "Library of Congress");
      sess.save(usnuc_authority);

      NamingAuthority vlts_authority = new NamingAuthority("vlts", "VLTS Inc");
      sess.save(vlts_authority);

      // made up stuff
      NamingAuthority dn_authority = new NamingAuthority("dn", "X.500/LDAP DN");
      sess.save(dn_authority);

      NamingAuthority k_int_authority = new NamingAuthority("KI", "Knowledge Integration Ltd");
      sess.save(k_int_authority);

      NamingAuthority talis_authority = new NamingAuthority("talis", "Talis Information Ltd");
      sess.save(talis_authority);

      log.debug("Setting up Services");

      Service loopback = new Service("Loopback",
                                     "LOOPBACK",
                                     "LOOPBACK",0,0);
      sess.save(loopback);

      Map common_props = new HashMap();

      log.debug("Setting up folder rules");

      installFolderRules(in, sess);

      log.debug("Custom Setup");
      customSetup(sess,common_props);

      log.debug("DB Setup complete... flush");
      sess.flush();
      log.debug("DB Setup complete... commit");
      sess.connection().commit();
      log.debug("DB Setup complete... close");
      sess.close();
    }
    catch ( Exception e ) {
      e.printStackTrace();
      log.warn("Problem setting up db",e);
    }
    finally {
      log.debug("Done");
    }
  }

  private void installFolderRules(BufferedReader in, 
                                  Session sess) throws java.sql.SQLException, org.hibernate.HibernateException
  {
    // Create the special system rules that will maintain a set of workflow folders for each location
    // registered in the system
  
    try {
      // Add requests in state PENDING for a RESPONDER to the NEW_REQUESTS folder
      log.debug("New Responder Requests");
      sess.save( new GenericRule("New Responder Requests",
                                 "RESP_NEW_REQ",
                                 "SYSTEM",       // A Globally applicable rule
                                 "CURRENT_LOC",  // Next param in the context of the CURRENT_LOC
                                 "RESP_NEW_REQUESTS", // Put matching transactions in the NEW_REQUESTS folder
                                 "old_role=='RESP' && old_trans_state=='IN-PROCESS'", // Old rule
                                 "role=='RESP' && trans_state=='IN-PROCESS'")); // New Rule
  
      log.debug("Requester Unread Messages");
      sess.save( new GenericRule("Requester Unread Messages",
                                 "REQ_UNREAD",
                                 "SYSTEM",       // A Globally applicable rule
                                 "CURRENT_LOC",  // Next param in the context of the CURRENT_LOC
                                 "REQ_UNREAD", // Put matching transactions in the REQ_UNREAD folder
                                 "old_role=='REQ' && old_tg_unread > 0", // Old rule
                                 "role=='REQ' && tg_unread > 0")); // New Rule
  
      log.debug("Requester End Of Rota");
      sess.save( new GenericRule("Requester End Of Rota",
                                 "REQ_EOR",
                                 "SYSTEM",       // A Globally applicable rule
                                 "CURRENT_LOC",  // Next param in the context of the CURRENT_LOC
                                 "REQ_EOR", // Put matching transactions in the RESP_COND folder
                                 "old_role=='REQ' && old_tg_state=='ENDROTA'", // Old rule
                                 "role=='REQ' && tg_state=='ENDROTA'")); // New Rule
  
      sess.save( new GenericRule("Requester Conditional Requests",
                                 "REQ_COND_REQ",
                                 "SYSTEM",       // A Globally applicable rule
                                 "CURRENT_LOC",  // Next param in the context of the CURRENT_LOC
                                 "REQ_COND", // Put matching transactions in the RESP_COND folder
                                 "old_role=='REQ' && old_trans_state=='CONDITIONAL'", // Old rule
                                 "role=='REQ' && trans_state=='CONDITIONAL'")); // New Rule
  
      sess.save( new GenericRule("Requester Overdue Requests",
                                 "REQ_OVERDUE_REQ",
                                 "SYSTEM",       // A Globally applicable rule
                                 "CURRENT_LOC",  // Next param in the context of the CURRENT_LOC
                                 "REQ_OVERDUE", // Put matching transactions in the RESP_COND folder
                                 "old_role=='REQ' && ( old_trans_state=='OVERDUE' || trans_state=='RENEWOVERDUE' ) ", // Old
                                 "role=='REQ' && ( trans_state=='OVERDUE' || trans_state=='RENEWOVERDUE' ) ")); // New Rule
  
      sess.save( new GenericRule("Requester Recall Requests",
                                 "REQ_RECALL_REQ",
                                 "SYSTEM",       // A Globally applicable rule
                                 "CURRENT_LOC",  // Next param in the context of the CURRENT_LOC
                                 "REQ_RECALL", 
                                 "old_role=='REQ' && old_trans_state=='RECALL'", // Old
                                 "role=='REQ' && trans_state=='RECALL'")); // New Rule
  
      sess.save( new GenericRule("Responder Unread Messages",
                                 "RESP_UNREAD",
                                 "SYSTEM",       // A Globally applicable rule
                                 "CURRENT_LOC",  // Next param in the context of the CURRENT_LOC
                                 "RESP_UNREAD", // Put matching transactions in the RESP_UNREAD folder
                                 "old_role=='RESP' && old_tg_unread > 0", // Old rule
                                 "role=='RESP' && tg_unread > 0")); // New Rule
  
      sess.save( new GenericRule("Responder Cancel Messages",
                                 "RESP_CANCEL",
                                 "SYSTEM",       // A Globally applicable rule
                                 "CURRENT_LOC",  // Next param in the context of the CURRENT_LOC
                                 "RESP_CANCEL", // Put matching transactions in the RESP_UNREAD folder
                                 "old_role=='RESP' && trans_state=='CANCEL'", // Old rule
                                 "role=='RESP' && trans_state=='CANCEL'")); // New Rule
  
      sess.save( new GenericRule("Responder Renew Messages",
                                 "RESP_RENEWAL",
                                 "SYSTEM",       // A Globally applicable rule
                                 "CURRENT_LOC",  // Next param in the context of the CURRENT_LOC
                                 "RESP_RENEWAL", // Put matching transactions in the RESP_UNREAD folder
                                 "old_role=='RESP' && ( trans_state=='RENEW/PENDING' || trans_state=='RENEWOVERDUE' )",
                                 "role=='RESP' && ( trans_state=='RENEW/PENDING' || trans_state=='RENEWOVERDUE' )") );
  
      sess.save( new GenericRule("Responder Overdue Messages",
                                 "RESP_OVERDUE",
                                 "SYSTEM",       // A Globally applicable rule
                                 "CURRENT_LOC",  // Next param in the context of the CURRENT_LOC
                                 "RESP_OVERDUE", // Put matching transactions in the RESP_UNREAD folder
                                 "old_role=='RESP' && ( trans_state=='OVERDUE' || trans_state=='RENEWPENDING' )",
                                 "role=='RESP' && ( trans_state=='OVERDUE' || trans_state=='RENEWPENDING' )") );
  
    }
    finally {
      log.debug("Done Folder Rules");
    }
  }

  public abstract void customSetup(Session session, Map default_props);
}
