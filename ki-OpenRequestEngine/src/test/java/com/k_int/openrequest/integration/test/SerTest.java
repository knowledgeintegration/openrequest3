package com.k_int.openrequest.integration.test;

import java.io.*;
import java.util.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.commons.logging.*;
import com.k_int.openrequest.integration.iface.*;
import com.k_int.openrequest.db.IPIGSystemNumber;
import java.net.*;

/**
 * Title:       OQLTest
 * @version:    $Id: SerTest.java,v 1.15 2005/07/01 19:53:22 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson (ian.ibbotson@k-int.com)
 * Company:     Knowledge Integration Ltd.
 * Description: Test the programmatic configuration of the persistence framework.
 * License:     A LICENSE.TXT file can be found in the root directory of this project.
 *              All current and derivative works are subject to that license.
 */

public class SerTest extends TestCase {

  private static Log log = LogFactory.getLog(SerTest.class);
 
  /**
   * Assembles and returns a test suite for all the test methods of this test case.
   * @return A non-null test suite.
   */
  public static Test suite() {
    TestSuite suite = new TestSuite(SerTest.class);
    return suite;
  }

  /**
   * Runs the test case.
   */
  public static void main(String args[]) {
    junit.textui.TestRunner.run(suite());
  }

  public SerTest(String name) {
    super(name);
  }

  protected void setUp() {
  }

  protected void tearDown() {
  }

  public void testSer1() {
    TransGroupInfoDTO test1 = 
      new TransGroupInfoDTO(
        new TransactionGroupIdDTO(new Long(0)),
        new StatusDTO(State.IDLE),
        new RequestView(
          new ILLRequestMessageDTO(),
          "A string"),
        new TransactionInfoDTO[] {},
        new RotaElementDTO[] {},
        Role.REQUESTER,
        new Long(0)
      );

    ByteArrayOutputStream fos = null;
    ObjectOutputStream out = null;
    try {
      fos = new ByteArrayOutputStream();
      out = new ObjectOutputStream(fos);
      out.writeObject(test1);
      out.close();
    }
    catch(IOException ex) {
       ex.printStackTrace();
    }
  }
}
