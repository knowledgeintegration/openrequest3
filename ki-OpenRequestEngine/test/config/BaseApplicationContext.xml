<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE beans PUBLIC "-//SPRING//DTD BEAN//EN" "http://www.springframework.org/dtd/spring-beans.dtd">

<!--
  CHANGEGLOG:
    II 07/08/2004 - Now uses variables instead of hard coded property names. You will find the 
                    values for these variables in the .properties file in the same directory.
-->

<beans>

  <bean id="placeholderConfig" class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
    <property name="location"><value>DefaultContext.properties</value></property>
  </bean>

  <!-- ========================= GENERAL DEFINITIONS ========================= -->

  <!--
    - The message source for this context, loaded from localized "messages_xx" files
    - in the classpath, i.e. "/WEB-INF/classes/messages.properties" or
    - "/WEB-INF/classes/messages_de.properties".
    -
    - "getMessage" calls to this context will use this source.
    - Child contexts can have their own message sources, inheriting all messages from this
    - source, being able to define new messages and override ones defined in this source.
    -->
  <bean id="messageSource" class="org.springframework.context.support.ResourceBundleMessageSource">
    <property name="basename"><value>messages</value></property>
  </bean>


  <!-- ========================= PERSISTENCE DEFINITIONS ========================= -->

  <!--
    - Makes a JNDI DataSource available as bean reference, assuming a J2EE environment.
    - By default, "java:comp/env/" will be prepended if not already specified.
    -
    - If giving this DataSource to LocalSessionFactoryBean, an alternative DataSource
    - could be used here too, e.g. for non-J2EE environments (see "webapp-typical").
    - Alternatively, one can omit the context-level DataSource definition, and let
    - Hibernate use its own connection provider, specified via Hibernate properties.
    -->
  <bean id="OpenRequestDataSource" class="org.apache.commons.dbcp.BasicDataSource">
    <property name="driverClassName"><value>${com.k_int.openrequest.jdbc_driver}</value></property>
    <property name="url"><value>${com.k_int.openrequest.url}</value></property>
    <property name="username"><value>${com.k_int.openrequest.username}</value></property>
    <property name="password"><value>${com.k_int.openrequest.password}</value></property>
    <property name="defaultAutoCommit"><value>${com.k_int.openrequest.default_auto_commit}</value></property>
  </bean>

  <!--
    - Builds a Hibernate SessionFactory and makes it available as bean reference.
    - All necessary settings get specified here, without relying on an external
    - Hibernate config file like "WEB-INF/classes/hibernate.cfg.xml".
    -
    - The SessionFactory will use the locally defined DataSource in this context.
    - This allows for convenient sharing of transactions with plain JDBC access
    - code, if HibernateTransactionManager is explictly given the same DataSource.
    -
    - This definition could also use a JDBC DataSource specified via Hibernate
    - properties. Note that to be able to share transactions with plain JDBC access
    - code then, that DataSource would need to be a JNDI one that should match the
    - one given to HibernateTransactionManager and data access objects.
    -
    - This definition can be replaced by a JndiObjectFactoryBean definition for fetching
    - a JNDI-bound SessionFactory, but this only makes sense when using Hibernate's
    - JCA Connector (not recommended for most types of applications).
    -->
  <bean id="OpenRequestSessionFactory" class="org.springframework.orm.hibernate.LocalSessionFactoryBean">
    <property name="dataSource"><ref local="OpenRequestDataSource"/></property>
    <property name="mappingResources">
      <list>
        <value>com/k_int/openrequest/db/CustomAttrDefinition.hbm.xml</value>
        <value>com/k_int/openrequest/db/ILLTransactionGroup.hbm.xml</value>
        <value>com/k_int/openrequest/db/ILLTransaction.hbm.xml</value>
        <value>com/k_int/openrequest/db/MessageHeader.hbm.xml</value>
        <value>com/k_int/openrequest/db/ISO10161Address.hbm.xml</value>
        <value>com/k_int/openrequest/db/StateModel.hbm.xml</value>
        <value>com/k_int/openrequest/db/StatusCode.hbm.xml</value>
        <value>com/k_int/openrequest/db/StateTransition.hbm.xml</value>
        <value>com/k_int/openrequest/db/Location/LocationType.hbm.xml</value>
        <value>com/k_int/openrequest/db/Location/Location.hbm.xml</value>
        <value>com/k_int/openrequest/db/Location/Service.hbm.xml</value>
        <value>com/k_int/openrequest/db/Location/LocationSymbol.hbm.xml</value>
        <value>com/k_int/openrequest/db/Location/NamingAuthority.hbm.xml</value>
        <value>com/k_int/openrequest/db/RequiredItemDetails.hbm.xml</value>
        <value>com/k_int/openrequest/db/Extension.hbm.xml</value>
        <value>com/k_int/openrequest/db/SystemAgentInfo.hbm.xml</value>
        <value>com/k_int/openrequest/db/folders/FolderHeader.hbm.xml</value>
        <value>com/k_int/openrequest/db/folders/FolderRule.hbm.xml</value>
        <value>com/k_int/commons/auth/User.hbm.xml</value>
        <value>com/k_int/commons/auth/Permission.hbm.xml</value>
        <value>com/k_int/commons/settings/GlobalProperty.hbm.xml</value>
      </list>
    </property>
    <property name="hibernateProperties">
      <props>
        <prop key="hibernate.dialect">${com.k_int.openrequest.hiberate_dialect}</prop>
        <prop key="hibernate.query.substitutions">${com.k_int.openrequest.query_substitutions}</prop>
        <prop key="hibernate.show_sql">${com.k_int.openrequest.show_sql}</prop>
        <prop key="hibernate.hbm2ddl.auto">update</prop>
      </props>
    </property>
  </bean>

  <!--
    - Defines a transaction manager for usage in business or data access objects.
    - No special treatment by the context, just a bean instance available as reference
    - for business objects that want to handle transactions, e.g. via TransactionTemplate.
    -
    - Needs a SessionFactory reference that it should handle transactions for.
    - Can optionally take a DataSource reference that it should use export Hibernate
    - transactions for, to be able to share transactions with plain JDBC access code
    - on the same database (using the same DataSource as the Hibernate SessionFactory).
    -->
  <bean id="myTransactionManager" class="org.springframework.orm.hibernate.HibernateTransactionManager">
    <property name="sessionFactory"><ref local="OpenRequestSessionFactory"/></property>
    <!--
    <property name="dataSource"><ref local="myDataSource"/></property>
    -->
  </bean>

  <bean id="OIDRegister" class="org.jzkit.a2j.codec.util.OIDRegister">
    <constructor-arg index="0"><value>/a2j.properties</value></constructor-arg>
  </bean>


  <!-- Get hold of an instance of the remote manager session -->
  <bean id="RemoteManagerFactory" class="com.k_int.openrequest.util.TRMIServiceFactory">
    <property name="serviceURL"><value>rmi://localhost:1199/RequestManagerFactory</value></property>
  </bean>

  <bean id="RemoteManagerSession"
        factory-bean="RemoteManagerFactory"
        factory-method="getInstance"
        singleton="false"/>

</beans>
