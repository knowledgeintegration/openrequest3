select MESSAGE_ID, 
       PARENT_REQUEST, 
       sc1.CODE as from_state,
       EVENT_CODE as event_code,
       sc2.CODE as to_state,
       MSG_TIMESTAMP 
from MESSAGE_HEADER, 
     STATUS_CODE sc1, 
     STATUS_CODE sc2
where sc1.ID=FROM_STATE 
  and sc2.ID=TO_STATE
order by PARENT_REQUEST,MESSAGE_ID;
  --     MESSAGE_TYPE, 
