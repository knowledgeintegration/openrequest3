select REQUEST_ID as id, 
       LOCATION as loc , 
       INITIAL_REQUESTER_SYMBOL as req, 
       TGQ, 
       TQ, 
       ILL_ROLE, 
       TRANSACTION_GROUP as tg, 
       CODE 
  from ILL_TRANSACTION, 
       STATUS_CODE 
 where REQUEST_STATUS_CODE=STATUS_CODE.ID;
