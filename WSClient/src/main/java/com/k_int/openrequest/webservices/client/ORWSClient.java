package com.k_int.openrequest.webservices.client;

import java.net.URL;
import com.k_int.openrequest.webservices.generated.*;

public class ORWSClient {

  private final String def_base_url = "http://10.4.36.102:8080/openrequest/services/StatelessRequestManagerPort";

  public static void main(String[] args) {

    if ( args.length > 0 ) {
    }

    try {
      ORWSClient cli = new ORWSClient();

      try {
        // long result = cli.createLocation(args[0]);
        String result = cli.cancelRequest(args[0]);
        System.err.println("Result "+result);
      } 
      catch ( Exception e ) {
        e.printStackTrace();
      }

      try {
        System.err.println("GetVersion "+cli.getVersion(args[0]));
      } 
      catch ( Exception e ) {
        e.printStackTrace();
      }
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
  }

  public ORWSClient() {
  }

  public long createLocation(String base_url) throws java.net.MalformedURLException, org.apache.axis.AxisFault, java.rmi.RemoteException {
    long result = -1;
    URL url = new URL(base_url);
    System.err.println("Connecting to "+url);
    StatelessRequestManagerPort stateless_request_manager_port_type = new StatelessRequestManagerBindingStub(url,new org.apache.axis.client.Service());
    com.k_int.openrequest.webservices.generated.LocationInfoDTO loc_info = new com.k_int.openrequest.webservices.generated.LocationInfoDTO();
    loc_info.setLocationName("Ian's new test location 3");
    loc_info.setSymbol("ki:Test7");
    loc_info.setAddressType("M");
    loc_info.setAddress("ill@k-int.com");
    result = stateless_request_manager_port_type.createLocation(loc_info);
    return result;
  }

  public String cancelRequest(String base_url)  throws java.net.MalformedURLException, org.apache.axis.AxisFault, java.rmi.RemoteException {
    String result = null;
    URL url = new URL(base_url);
    System.err.println("Connecting to "+url);
    StatelessRequestManagerPort stateless_request_manager_port_type = new StatelessRequestManagerBindingStub(url,new org.apache.axis.client.Service());
    long loc_id = 0;
    long[] trans_ids = new long[] {0,1,2};
    com.k_int.openrequest.webservices.generated.ActionMessageDTO message = new com.k_int.openrequest.webservices.generated.ActionMessageDTO();
    com.k_int.openrequest.webservices.generated.CancelActionDTO cancel_msg = new com.k_int.openrequest.webservices.generated.CancelActionDTO();
    cancel_msg.setNote("Hello World!");
    message.setCancel(cancel_msg);
    result = stateless_request_manager_port_type.actionRequests(loc_id,trans_ids,message);
    return result;
  }

  public String getVersion(String base_url) throws java.net.MalformedURLException, org.apache.axis.AxisFault, java.rmi.RemoteException {
    String result = null;
    URL url = new URL(base_url);
    System.err.println("Connecting to "+url);
    StatelessRequestManagerPort stateless_request_manager_port_type = new StatelessRequestManagerBindingStub(url,new org.apache.axis.client.Service());
    result = stateless_request_manager_port_type.getVersion();
    return result;
  }
}
