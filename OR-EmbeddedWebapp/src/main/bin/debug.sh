export CLASSPATH=../etc
for lib in `ls ../dist/*.jar ../../lib/*.jar`
do
  export CLASSPATH=$CLASSPATH:$lib
done

export CLASSPATH=.:$CLASSPATH


# echo Starting RMI registry
# $JAVA_HOME/bin/rmiregistry &
# 
# sleep 4
# 
echo Starting controller...
OPTS="-launch -verbose"
jdb $OPTS com.k_int.openrequest.services.Controller ./BaseApplicationContext.xml ./ServerApplicationContext.xml
