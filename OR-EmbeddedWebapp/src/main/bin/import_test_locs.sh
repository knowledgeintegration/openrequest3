export CLASSPATH=../etc
for lib in `ls ../dist/*.jar ../../lib/*.jar`
do
  export CLASSPATH=$CLASSPATH:$lib
done

export CLASSPATH=.:$CLASSPATH

export OPTS="-Xmx256m -Xms256m"

# echo Starting RMI registry
# $JAVA_HOME/bin/rmiregistry &
# 
# sleep 4
# 
echo Starting controller...
java $OPTS com.k_int.openrequest.setup.ImportTestingLocationData ./BaseApplicationContext.xml
