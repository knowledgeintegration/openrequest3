export CLASSPATH=../etc
for lib in `ls ../dist/*.jar ../../lib/*.jar`
do
  export CLASSPATH=$CLASSPATH:$lib
done

export OPTS="-Xmx256m -Xms128m"
export CLASSPATH=.:$CLASSPATH


echo Starting controller...
# java $OPTS com.k_int.openrequest.integration.dataload.oclc.OCLCImport ./imp_216.ber /tmp/badfile
java $OPTS com.k_int.openrequest.integration.dataload.oclc.OCLCImport /tmp/idata2 /tmp/badfile
