#!/bin/bash

mysql -uk-int -pk-int <<theend
use OpenCRM;
drop table CASE_PARTICIPANT;
drop table COMMUNICATION_EVENT;
drop table OPENCRM_CASE;
drop table OPENCRM_USER;
drop table PARTY;
drop table PARTY_ORGANISATION;
drop table PARTY_PERSON;
drop table PARTY_RELATIONSHIP;
drop table PARTY_ROLE;
drop table PARTY_ROLE_TYPE;
drop table STATE_MODEL;
drop table STATE_TRANSITION;
drop table STATUS_CODE;
theend
