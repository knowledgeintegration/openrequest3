#!

export CLASSPATH=
for jar in `ls ../lib/*.jar ../dist/*.jar`
do
  export CLASSPATH=$CLASSPATH:$jar
done
export CLASSPATH=../etc:$CLASSPATH:../classes

java -Dlog4j.configuration=file:../etc/log4j.properties com.k_int.OpenCRM.setup.Setup $*
