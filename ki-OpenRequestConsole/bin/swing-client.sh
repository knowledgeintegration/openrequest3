#!/bin/bash

export CLASSPATH=../etc
for jar in `ls ../../lib/*.jar ../../ki-OpenRequestEngine/dist/*.jar`
do
  export CLASSPATH=$CLASSPATH:$jar
done
export CLASSPATH=../etc:$CLASSPATH:../classes

JAVA_OPTS="-Djava.util.logging.config.file=../etc/logging.properties -Dlog4j.configuration=file:../etc/log4j.properties"

java $JAVA_OPTS com.k_int.openrequest.console.main.OpenRequestConsole $*
