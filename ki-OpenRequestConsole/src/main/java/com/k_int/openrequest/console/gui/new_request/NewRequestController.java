/**
 * Title:
 * @version:    $Id: NewRequestController.java,v 1.2 2005/02/17 16:02:28 ibbo Exp $
 * Copyright:   Copyright (C) 2003 Ian Ibbotson
 * @author:     Ian Ibbotson
 * Company:
 * Description:
 */

package com.k_int.openrequest.console.gui.new_request;

import java.io.BufferedInputStream;
import java.beans.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import com.k_int.AdminApp.config.*;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.ListSelectionModel;
import net.sf.hibernate.*;
import net.sf.hibernate.type.*;
import com.k_int.AdminApp.gui.Controller;
import com.k_int.AdminApp.gui.AdminControllerComponent;
import com.k_int.AdminApp.gui.ControllerOwner;
import com.k_int.AdminApp.gui.ExplorerView.AdminController;
import com.k_int.AdminApp.models.Search.SearchModel;
import com.k_int.AdminApp.gui.SearchView.DBSearchTableModel;
import com.k_int.AdminApp.gui.RecordView.*;
import com.k_int.commons.statemodel.*;
import java.util.logging.*;
import org.springframework.context.ApplicationContext;
import javax.swing.table.TableModel;
import com.k_int.openrequest.integration.iface.*;

import java.util.*;

public class NewRequestController extends Controller implements ActionListener, ControllerOwner
{
  private static Logger cat = Logger.getLogger(NewRequestController.class.getName());
  protected ConfigHolder config;
  protected String name;
  protected ControllerOwner owner;
  protected AdminController top;
  protected boolean is_closeable;
  protected JPanel view;

  protected ApplicationContext ctx;

  // Controller specific stuff
  private NewRequestModel model = new NewRequestModel();
  // RotaElementDTO[] rota
  private ArrayList rota = new ArrayList();



  public NewRequestController(ApplicationContext ctx,
                              String name,
                              ControllerOwner owner,
                              AdminController top,
                              boolean is_closeable) {
    super(top, ctx);

    System.err.println("Start...");
    try {
      this.ctx = ctx;
      this.config = (ConfigHolder)ctx.getBean("XDAdminConfig");
      this.name = name;
      this.owner = owner;
      this.top = top;
      this.is_closeable = is_closeable;

      // com.k_int.swing.databinding.SimpleDocumentModel test = new com.k_int.swing.databinding.SimpleDocumentModel(this,"book");

      System.err.println("get sf");
      SessionFactory sf = (SessionFactory) ctx.getBean("OpenRequestSessionFactory");
      // this.session = sf.openSession();

      // model = new ManageRequestsModel(sf, this);

      System.err.println("get view");
      URL view_definition = ClassLoader.getSystemResource("com/k_int/openrequest/console/gui/new_request/view.xml");
      XMLDecoder d = new XMLDecoder( new BufferedInputStream( view_definition.openStream() ), this );
      view = (JPanel) d.readObject();
      d.close();

      System.err.println("Notify model changed");
      this.modelChanged(model);

      System.err.println("done");
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }
  }

  public Object getComponentId() {
    return new Integer(this.hashCode());
  }

  public String getName() {
    return "Manage Requests";
  }

  public java.awt.Component getView() {
    return view;
  }

  public boolean isCloseable() {
    return is_closeable;
  }

  public void setIsCloseable(boolean is_closeable) {
    this.is_closeable = is_closeable;
  }

  public ConfigHolder getConfig() {
    return config;
  }
                                                                                                                                        
  public void actionPerformed(java.awt.event.ActionEvent e) {
    String a = e.getActionCommand();
    System.err.println("Action: "+a);

    Object source = e.getSource();
    if ( a.equals("comboBoxChanged") ) {
    }
  }

  public void componentCloseNotification(AdminControllerComponent component) {
  }

  // Adapters
  /*
  private ListSelectionListener transaction_list_listener = new ListSelectionListener() {
    public void valueChanged(ListSelectionEvent e) {
        //Ignore extra messages.
        if (e.getValueIsAdjusting()) return;
                                                                                                                                          
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        if (lsm.isSelectionEmpty()) {
            //no rows are selected
        } else {
            int selected_client_row = lsm.getMinSelectionIndex();
            cat.finest("Row selection change : "+selected_client_row);
            // showResultRowDetailRecord(selected_row);
            // notifyClientRowSelected(selected_client_row);
        }
    } 
  };
  */
}


    
