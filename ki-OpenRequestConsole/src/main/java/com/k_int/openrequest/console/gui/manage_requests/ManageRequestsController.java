/**
 * Title:
 * @version:    $Id: ManageRequestsController.java,v 1.1.1.1 2005/02/14 11:46:30 ibbo Exp $
 * Copyright:   Copyright (C) 2003 Ian Ibbotson
 * @author:     Ian Ibbotson
 * Company:
 * Description:
 */

package com.k_int.openrequest.console.gui.manage_requests;

import java.io.BufferedInputStream;
import java.beans.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import com.k_int.AdminApp.config.*;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.ListSelectionModel;
import net.sf.hibernate.*;
import net.sf.hibernate.type.*;
import com.k_int.AdminApp.gui.AdminControllerComponent;
import com.k_int.AdminApp.gui.ControllerOwner;
import com.k_int.AdminApp.gui.ExplorerView.AdminController;
import com.k_int.AdminApp.models.Search.SearchModel;
import com.k_int.AdminApp.gui.SearchView.DBSearchTableModel;
import com.k_int.AdminApp.gui.RecordView.*;
import com.k_int.commons.statemodel.*;
import java.util.logging.*;
import org.springframework.context.ApplicationContext;
import javax.swing.table.TableModel;
                                                                                                                                          
import java.util.*;
import com.k_int.openrequest.console.gui.new_request.NewRequestController;
                                                                                                                                          


public class ManageRequestsController implements AdminControllerComponent, ActionListener, ControllerOwner
{
  private static Logger cat = Logger.getLogger(ManageRequestsController.class.getName());
  protected ConfigHolder config;
  protected String name;
  protected ControllerOwner owner;
  protected AdminController top;
  protected boolean is_closeable;
  protected JPanel view;
  protected ManageRequestsModel model;
  protected ApplicationContext ctx;

  protected RequestSearchDataModel search_model = new RequestSearchDataModel();
                                                                                                                                          
  private javax.swing.DefaultComboBoxModel location_combo_model = new javax.swing.DefaultComboBoxModel();
                                                                                                                                          
  private ListSelectionListener transaction_list_listener = new ListSelectionListener() {
    public void valueChanged(ListSelectionEvent e) {
        //Ignore extra messages.
        if (e.getValueIsAdjusting()) return;
                                                                                                                                          
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
                                                                                                                                          
        if (lsm.isSelectionEmpty()) {
            //no rows are selected
        } else {
            int selected_client_row = lsm.getMinSelectionIndex();
            cat.finest("Row selection change : "+selected_client_row);
            // showResultRowDetailRecord(selected_row);
            // notifyClientRowSelected(selected_client_row);
        }
    } };

  public ManageRequestsController(ApplicationContext ctx,
                                  String name,
                                  ControllerOwner owner,
                                  AdminController top,
                                  boolean is_closeable)
  {
    try
    {
      this.ctx = ctx;
      this.config = (ConfigHolder)ctx.getBean("XDAdminConfig");
      this.name = name;
      this.owner = owner;
      this.top = top;
      this.is_closeable = is_closeable;


      SessionFactory sf = (SessionFactory) ctx.getBean("OpenRequestSessionFactory");
      // this.session = sf.openSession();

      model = new ManageRequestsModel(sf, this, ctx);

      URL view_definition = ClassLoader.getSystemResource("com/k_int/openrequest/console/gui/manage_requests/view.xml");
      XMLDecoder d = new XMLDecoder( new BufferedInputStream( view_definition.openStream() ), this );
      view = (JPanel) d.readObject();
      d.close();

      String logged_in_userid = top.getUser().getUserID();
      List avail_locs = model.getAvailableLocations(logged_in_userid);

      for ( Iterator i = avail_locs.iterator(); i.hasNext(); ) {
        location_combo_model.addElement( i.next() );
      }
    }
    catch ( Exception e )
    {
      e.printStackTrace();
    }
  }

  public Object getComponentId()
  {
    return new Integer(this.hashCode());
  }

  public String getName()
  {
    return "Manage Requests";
  }

  public java.awt.Component getView()
  {
    return view;
  }

  public boolean isCloseable()
  {
    return is_closeable;
  }

  public void setIsCloseable(boolean is_closeable)
  {
    this.is_closeable = is_closeable;
  }

  public ConfigHolder getConfig()
  {
    return config;
  }
                                                                                                                                        
  public AdminController getRootController()
  {
    return top;
  }
  
  
  /**
  private class CasesMouseAdapter extends MouseAdapter
  {
      private Controller controller;
        CasesMouseAdapter(Controller controller)
        {
            this.controller=controller;
        }
      public void mouseClicked(MouseEvent e) 
      {
        if ( e.getClickCount() == 2 ) 
        {
            controller.openSelectedCase();
        }
      }   
             
  }
  */

  public void actionPerformed(java.awt.event.ActionEvent e)
  {
    String a = e.getActionCommand();
    System.err.println("Action: "+a);

    Object source = e.getSource();
    if ( a.equals("comboBoxChanged") ) {
      Object o = ((javax.swing.JComboBox)source).getSelectedItem();
      if ( o instanceof com.k_int.openrequest.db.Location.Location ) {
        model.selectLocation((com.k_int.openrequest.db.Location.Location) o);
      }
    }
    else if ( a.equals("New Request") ) {
      // Request to create a new iso ill request
      createNewRequest("LOC_CODE");
    }
  }
                                                                                                                                          
  public void createNewRequest(String loc_id) {
    NewRequestController new_request_controller = new NewRequestController(ctx,
                                                                           "New Request",
                                                                           this,
                                                                           top,
                                                                           true);
    top.addComponent(new_request_controller);
  }

  public ApplicationContext getApplicationContext() {
    return ctx;
  }

  public void componentCloseNotification(AdminControllerComponent component) {
  }
                                                                                                                                          
  public void registerLocationCombo(JComboBox combo) {
    combo.setModel(location_combo_model);
  }
  public ListSelectionListener getTransactionTableSelectionListener() {
    return transaction_list_listener;
  }
                                                                                                                                          
  public TableModel getTransactionSearchDataModel() {
    return search_model;
  }
                                                                                                                                          
  public void notifyTransactionListChanged(List transactions) {
    search_model.setResults(transactions);
  }

}


    
