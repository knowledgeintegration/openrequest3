package com.k_int.openrequest.console.main;

import org.springframework.context.ApplicationContext;

public class OpenRequestConsole extends com.k_int.AdminApp.gui.GUIMain {

  public static void main(String[] args) {
    OpenRequestConsole app = new OpenRequestConsole();
    app.launch();
  }

  public OpenRequestConsole() {
    super("OpenRequestConsoleAppCtx.xml");
  }

  public void checkVersion(ApplicationContext app_context) {
    // Do it
    // Step 1 : Ask for any new properties that are required
    // Step 2 : Upgrade the database schema
    // Step 3 : Perform any upgrade tasks from a previous version, et
    // if ( installed_version < 1 ) {
    //   code for fresh install }
    // if ( installed_version < 2 ) {
    //   upgrade 1 -> 2 }
    // if ( installed_version < 3 ) {
    //   upgrade 2 -> 3 }
    // etc
  }

  public boolean validateProperties(java.util.Properties props) {
    return true;
  }

}
