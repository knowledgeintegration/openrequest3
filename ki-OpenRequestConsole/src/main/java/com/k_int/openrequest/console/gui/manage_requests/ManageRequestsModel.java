/**
 * Title:
 * @version:    $Id: ManageRequestsModel.java,v 1.2 2005/02/21 10:02:58 ibbo Exp $
 * Copyright:   Copyright (C) 2003 Ian Ibbotson
 * @author:     Ian Ibbotson
 * Company:
 * Description:
 */

package com.k_int.openrequest.console.gui.manage_requests;

import net.sf.hibernate.*;
import java.util.*;
import com.k_int.openrequest.integration.enums.*;
import com.k_int.openrequest.integration.iface.*;
import org.springframework.context.ApplicationContext;

public class ManageRequestsModel
{
  public static final String AVAIL_LOCS_OQL = " select loc from com.k_int.openrequest.db.Location.Location loc, com.k_int.commons.auth.Permission perm where ( perm.ResourceAuthority = 'com.k_int.openrequest.db.Location.Location' ) and ( perm.ResourceId = loc.id OR perm.ResourceId = 'ALL' ) and ( perm.Grantee = ? ) and ( perm.PermissionId = 'or.ManageRequests' OR perm.PermissionId = 'ALL' )";

  private SessionFactory sf = null;
  private Long current_loc_id = null;
  private String current_symbol = null;
  private List transactions = null;
  private RequestManager manager = null;
  private RequestManagerFactory factory = null;
  private ManageRequestsController controller = null;
  private ApplicationContext ctx = null;

  public ManageRequestsModel(SessionFactory sf, ManageRequestsController controller, ApplicationContext ctx) {
    this.sf = sf;
    this.controller = controller;
    this.ctx = ctx;
    
    System.err.println("Looking up factory");
    this.factory = (RequestManagerFactory) ctx.getBean("RemoteManagerSession");
    System.err.println("got factory");

  }

  /**
   *  Work out which locations the currently logged in user has permission to
   *  manage requests for.
   */
  public List getAvailableLocations(String user_id) {
    List result = null;

    try {
      // We use the permissions system to select all locations where manage requests has 
      // been granted to the user.
      Session s = sf.openSession();
      result = s.find(AVAIL_LOCS_OQL,user_id,Hibernate.STRING);
      s.close();
    }
    catch ( net.sf.hibernate.HibernateException he ) {
      he.printStackTrace();
    }

    return result;
  }


  /**
   *  Select a specific location to manage requests for
   */
  public void selectLocation(com.k_int.openrequest.db.Location.Location location) {
    System.err.println("selectLocation"+location.getDefaultSymbol());

    try {
      if ( location.getId() != current_loc_id ) {
        current_loc_id = location.getId();
        current_symbol = location.getDefaultSymbol();
        System.out.println("Switching to location "+location);
  
        if ( manager != null ) {
          System.err.println("Release manager");
          manager.release();
        }
  
        System.err.println("Asking factory for new RemoteManagerSessoin : "+current_symbol);
        manager = factory.getRequestManager(current_symbol);
        System.err.println("gotManager...");
  
        if ( manager != null ) {
          System.err.println("Refresh transaction list");
          refreshTransactionLists();
          manager.release();
          manager = null;
        }
        else {
          System.out.println("No manager");
        }
      }
    }
    catch ( com.k_int.openrequest.integration.iface.RequestManagerException rme ) {
      rme.printStackTrace();
    }
  }

  public void refreshTransactionLists() throws  com.k_int.openrequest.integration.iface.RequestManagerException {
    if ( manager != null ) {
      transactions = doQuery(null);
      controller.notifyTransactionListChanged(transactions);
    }
  }
  
  public List doQuery(Role role) throws com.k_int.openrequest.integration.iface.RequestManagerException {

    List result = new ArrayList();

    RequestManagerQueryDTO query = new RequestManagerQueryDTO(role,null,null,null,null,null);
    SearchResultDTO search_result = manager.searchQuery(query);
    System.err.println("Result of search:");
    System.err.println("   handle:"+search_result.getHandle());
    System.err.println("   status:"+search_result.getStatus().toString());
    System.err.println("   hit count:"+search_result.getRowCount());
                                                                                                                                          
    System.err.println("Calling retrieveResultSet....");
                                                                                                                                          
    // Get the actual rows
    ResultSetDTO res_records = manager.retrieveResultSet(search_result, search_result.getRowCount(), 0);
                                                                                                                                          
    System.err.println("result contains "+res_records.count+" rows");
    for ( int row_ctr = 0; row_ctr < res_records.results.length; row_ctr++ ) {
      result.add(res_records.results[row_ctr]);
      System.err.println("Row-> "+res_records.results[row_ctr]);
    }
                                                                                                                                          
    manager.releaseSearchResult(search_result);

    return result;
  }
}
