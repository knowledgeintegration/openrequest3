/**
 * Title:
 * @version:    $Id: NewRequestModel.java,v 1.2 2005/02/22 14:49:01 ibbo Exp $
 * Copyright:   Copyright (C) 2003 Ian Ibbotson
 * @author:     Ian Ibbotson
 * Company:
 * Description:
 */

package com.k_int.openrequest.console.gui.new_request;

import com.k_int.openrequest.integration.iface.*;
import java.util.*;

public class NewRequestModel {

  private RequestView request_details = null;
  private List rota = null;

  public NewRequestModel() {
    request_details = new RequestView();
    request_details.setILLRequest(new ILLRequestMessageDTO());
    request_details.getILLRequest().setItemId(new ItemIdDTO());
    request_details.getILLRequest().setDeliveryAddress(new PostalAddressDTO());
    request_details.getILLRequest().setBillingAddress(new PostalAddressDTO());
    request_details.getILLRequest().setRequestedServiceType(new ArrayList(com.k_int.openrequest.integration.enums.ServiceType.VALUES));
    rota = new ArrayList();
  }

  public RequestView getRequestDetails() {
    return request_details;
  }

  public void setRequestDetails(RequestView request_details) {
    this.request_details = request_details;
  }

  public List getRota() {
    return rota;
  }

  public void setRota(List rota) {
    this.rota = rota;
  }
}
