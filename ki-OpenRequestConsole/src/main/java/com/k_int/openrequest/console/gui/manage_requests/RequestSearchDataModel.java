/**
 * Title:
 * @version:    $Id: RequestSearchDataModel.java,v 1.3 2005/06/10 13:09:42 ibbo Exp $
 * Copyright:   Copyright (C) 2003 Ian Ibbotson
 * @author:     Ian Ibbotson
 * Company:
 * Description:
 */

package com.k_int.openrequest.console.gui.manage_requests;

import javax.swing.table.*;
import java.util.List;
import com.k_int.openrequest.integration.enums.*;
import com.k_int.openrequest.integration.iface.*;

public class RequestSearchDataModel extends AbstractTableModel {

  private List transactions;
  private String[] col_heads;
  private int row_count=0;

  public RequestSearchDataModel() {
    col_heads = new String[] { "ID", 
                               "TGQ", 
                               "Req Ref", 
                               "role",
                               "Title",
                               "Author",
                               "PubDate",
                               "Current Partner",
                               "Last Message",
                               "State",
                               "Phase"};
  }

  public int getRowCount() {
    if ( transactions != null )
      return transactions.size();
    return 0;
  }

  public int getColumnCount() {
    return col_heads.length;
  }

  public Object getValueAt(int row_num, int col) {

    if ( transactions == null )
      return null;

    SearchResultEntryDTO e = (SearchResultEntryDTO) transactions.get(row_num);
    switch(col) {
      case 0: return e.getTransGroupId();
      case 1: return e.getTGQ();
      case 2: return e.getRequesterRef();
      case 3: return e.getRole();
      case 4: return e.getTitle();
      case 5: return e.getAuthor();
      case 6: return e.getPublication();
      case 7: return e.getCurrentPartner();
      case 8: return e.getLastMsgDate();
      case 9: return e.getState();
      case 10: return e.getPhase();
    }

    return null;
  }

  public String getColumnName(int col) {
    return col_heads[col];
  }

  public void setResults(List transactions) {
    this.transactions = transactions;
    this.fireTableDataChanged();
  }

}
