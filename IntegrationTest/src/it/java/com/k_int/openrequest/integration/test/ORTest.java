package com.k_int.openrequest.integration.test;

import java.io.*;
import java.util.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.commons.logging.*;
import com.k_int.openrequest.integration.iface.*;
import com.k_int.openrequest.db.IPIGSystemNumber;

/**
 * Title:       OQLTest
 * @version:    $Id: ORTest.java,v 1.15 2005/07/01 19:53:22 ibbo Exp $
 * Copyright:   Copyright (C) 2002 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson (ian.ibbotson@k-int.com)
 * Company:     Knowledge Integration Ltd.
 * Description: Test the programmatic configuration of the persistence framework.
 * License:     A LICENSE.TXT file can be found in the root directory of this project.
 *              All current and derivative works are subject to that license.
 */

public class ORTest extends TestCase {

  private static String[] loc_data = null;
  private static String[] ctx_args = null;
  private ApplicationContext ctx = null;
  private static Log log = LogFactory.getLog(ORTest.class);
 
  /**
   * Assembles and returns a test suite for all the test methods of this test case.
   * @return A non-null test suite.
   */
  public static Test suite() {
    TestSuite suite = new TestSuite(ORTest.class);
    return suite;
  }

  /**
   * Runs the test case.
   */
  public static void main(String args[]) {
    ctx_args=args;
    if ( args.length < 2 ) {
      log.debug("Usage : java ORTest CtxDefFiles.xml....");
      System.exit(0);
    }
   
    junit.textui.TestRunner.run(suite());
  }

  public ORTest(String name) {
    super(name);
  }

  protected void setUp() {
    log.debug("_Service_ App CTX, starts service too, initially with no data");
    ctx = new ClassPathXmlApplicationContext( new String[] {"/BaseApplicationContext.xml","/ServerApplicationContext.xml"} );

    log.debug("Database Reference Data");
    com.k_int.openrequest.setup.Setup setup_instance = new com.k_int.openrequest.setup.Setup();
    setup_instance.setup(ctx);

    log.debug("Setup test locations");
    com.k_int.openrequest.setup.ImportTestingLocationData loc_importer = new com.k_int.openrequest.setup.ImportTestingLocationData();
    loc_importer.runImp(ctx, 10);

    if ( loc_data == null ) {
      loc_data = new String[] { "KI:loopback-1", "KI:loopback-2", "KI:loopback-3" };
    }

    log.debug("Setup Complete");
  }

  protected void tearDown() {
  }

  public void testFactory() {

    log.debug("testFactory......");

    RequestManager manager = null;

    try {

      log.debug("Looking up factory");
      RequestManagerFactory factory = (RequestManagerFactory) ctx.getBean("RemoteManagerSession");
      StatelessRequestManager srm = (StatelessRequestManager) ctx.getBean("RemoteStatelessManager");
      log.debug("Got factory");
     
      // Going to send a request from location bl:QS/Q28
      // RequestManager manager = factory.getRequestManager("bl:QS/Q28");
      manager = factory.getRequestManager(loc_data[0]);
      log.debug("Got Manager... Calling create tg");
      ArrayList service_types = new ArrayList();
      service_types.add(ServiceType.LOAN);


      RequesterViewDTO new_req_details = new RequesterViewDTO(
                                     TransactionType.SIMPLE,
                                     // new DeliveryAddressDTO(  // Delivery
                                       new PostalAddressDTO(
                                         "name",
                                         "extended",
                                         "street",
                                         "post office",
                                         "city",
                                         "region",
                                         "country",
                                         "postcode"),
                                       // new SystemAddressDTO(
                                       //   "ftp",
                                       //   "ftp://un:pw@www.gobblajuke.com")
                                     // ),
                                     // new DeliveryServiceDTO(0, TransportationMode.POSTAL),
                                     new PhysicalDeliveryDTO(BeTransportationMode.HAYS),
                                     // new DeliveryAddressDTO(  // Billing
                                       new PostalAddressDTO(
                                         "name",
                                         "extended",
                                         "street",
                                         "post office",
                                         "city",
                                         "region",
                                         "country",
                                         "postcode"),
                                       // new SystemAddressDTO(
                                       //   "ftp",
                                       //   "ftp://un:pw@www.gobblajuke.com")
                                     // ),
                                     service_types,
                                     new RequesterOptionalMessagesDTO(
                                       true,
                                       true,
                                       MessageOption.DESIRES, 
                                       MessageOption.DESIRES),
                                     new SearchTypeDTO(LevelOfService.LOCAL,
                                                        new java.util.Date(),
                                                        ExpiryFlag.NO_EXPIRY,
                                                        new java.util.Date()),
                                     new SupplyMediumType[] { SupplyMediumType.PRINTED,SupplyMediumType.PHOTOCOPY,SupplyMediumType.MACHINE },
                                     PlaceOnHoldType.PREF,
                                     new ItemIdDTO(
                                       ItemTypeExtended.MONO,
                                       MediumType.PRINTED,
                                       "Brain of the firm",
                                       "Managerial Cybernetics",
                                       "Beer, Stafford",
                                       null, // Sponsoring body
                                       "Wiley",
                                       "London, England",
                                       null, // pubdate
                                       null, // isbn
                                       null, // issn
                                       null, // title of article
                                       null, // author of article
                                       "volume/issue", // volume/issue
                                       null, // pubdate of component
                                       null, // paginiation
                                       new IPIGSystemNumber(0,"ii-num","db-id","db-system-no"), // system no
                                       null, // callno
                                       new IPIGSystemNumber(1,null,"db-id","db-system-no"), // nat bib no
                                       null, // series title number
                                       "source", // verif ref src
                                       null),// edition
                                     new CostInfoTypeDTO(
                                       "acctno",
                                       "CAD",
                                       "value",
                                       false),
                                     false, // permission to forward
                                     "A requester note",
                                     "ReqRefAuth",
                                     "IMI:"+System.currentTimeMillis(), // User request number
                                     "Client Id",
                                     "CCS",
                                     null);

      TransactionGroupIdDTO tg_id1 = manager.createTransGroup(new_req_details, new RotaElementDTO[] { new RotaElementDTO(loc_data[1],ServiceProtocol.ISO) });
      TransactionGroupIdDTO tg_id2 = manager.createTransGroup(new_req_details, new RotaElementDTO[] { new RotaElementDTO(loc_data[1],ServiceProtocol.ISO) });
      TransactionGroupIdDTO tg_id3 = manager.createTransGroup(new_req_details, new RotaElementDTO[] { new RotaElementDTO(loc_data[1],ServiceProtocol.ISO) });
      TransactionGroupIdDTO tg_id4 = manager.createTransGroup(new_req_details, new RotaElementDTO[] { new RotaElementDTO(loc_data[1],ServiceProtocol.ISO) });
      TransactionGroupIdDTO tg_id5 = manager.createTransGroup(new_req_details, new RotaElementDTO[] { new RotaElementDTO(loc_data[1],ServiceProtocol.ISO) });
      log.debug("New TG : "+tg_id1);
      log.debug("New TG : "+tg_id2);
      log.debug("New TG : "+tg_id3);
      log.debug("New TG : "+tg_id4);
      log.debug("New TG : "+tg_id5);
      // Do a yield to give the engine chance to process the request
      log.debug("Calling startMessagingSync.... tg1");
      Long new_trans_id1 = manager.startMessagingSync(tg_id1);
      log.debug("Calling startMessagingSync.... tg2");
      Long new_trans_id2 = manager.startMessagingSync(tg_id2);
      log.debug("Calling startMessagingSync.... tg3");
      Long new_trans_id3 = manager.startMessagingSync(tg_id3);
      log.debug("Calling startMessagingSync.... tg4");
      Long new_trans_id4 = manager.startMessagingSync(tg_id4);
      log.debug("Calling startMessagingSync.... tg5");
      Long new_trans_id5 = manager.startMessagingSync(tg_id5);

      log.debug("New Transaction ID: "+new_trans_id1);
      log.debug("New Transaction ID: "+new_trans_id2);
      log.debug("New Transaction ID: "+new_trans_id3);
      log.debug("New Transaction ID: "+new_trans_id4);
      log.debug("New Transaction ID: "+new_trans_id5);

      log.debug("\n\n\n\n\n\n\n\nSleeping for 10000");
      // Sleep for a little while
      try {
        Thread.sleep(10000);
      }
      catch ( Exception e ) {
        e.printStackTrace();
      }

      TransGroupInfoDTO tg_info = manager.retrieveTransGroup(tg_id1);
      log.debug("\n\nCurrent Transaction ID : "+(tg_info.getCurrentTransactionId()));

      log.debug("\n\n\n\n\n\n\nCalling search.... all transactions where I am the requester");
      RequestManagerQueryDTO query = new RequestManagerQueryDTO("REQ",null,null,null,null,null,null,null,null,null,null);
      SearchResultDTO search_result = manager.searchQuery(query);
      log.debug("Result of search:");
      log.debug("   handle:"+search_result.getHandle());
      log.debug("   status:"+search_result.getStatus().toString());
      log.debug("   hit count:"+search_result.getRowCount());

      log.debug("Calling retrieveResultSet....");

      ResultSetDTO res_records = manager.retrieveResultSet(search_result, search_result.getRowCount(), 0);

      log.debug("result contains "+res_records.count+" rows");
      for ( int row_ctr = 0; row_ctr < res_records.results.length; row_ctr++ )
        System.out.println("Row-> "+res_records.results[row_ctr]);
    
      manager.releaseSearchResult(search_result);

      log.debug("\n\n\n\n\n\n\n\ncall getRequestCounts..");
      RequestCountDTO[] counts = manager.retrieveRequestCounts();
      log.debug("Got "+counts.length+" folders...");
      for ( int count_ctr = 0; count_ctr < counts.length; count_ctr++ )
        System.out.println("Count->"+counts[count_ctr]);
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
    finally {
      if ( manager != null ) {
        log.debug("Calling release....");
        try { manager.release(); } catch(Exception e) {}
        log.debug("Done");
      }
    }

    log.debug("All Done");
  }
}
